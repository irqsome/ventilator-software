Spintris
========

## Video
[![](http://img.youtube.com/vi/EyXufPBdlIQ/0.jpg)](http://www.youtube.com/watch?v=EyXufPBdlIQ "Video")

## Info
- Type: Game
- Genre: Puzzle
- Author(s): JT Cook
- First release: 2007
- Improved version: **Yes** (2020)
- Players: 1 or 2
- Special requirements: None
- Video formats: NTSC, PAL50 (selected in source code)
- Inputs: 2x Gamepad, Keyboard

## Description
A simple _Tetris_ clone.

The improved version featured here includes improved controls, music, new sound effects,
a somewhat _TGM3_-style bag randomizer (instead of a naive randomizer) and a scoring system based on the GameBoy version.


## Controls
|Gamepad|Player 1's Keyboard|Player 2's Keyboard|Action|
|------|--------------------|-------------------|------|
|Y|Right CTRL|Left CTRL|Rotate counter-clockwise|
|B|Right ALT|Left ALT|Rotate clockwise|
|Left/Right|Left/Right|F/H|Move piece|
|Down|Down|G|Soft drop|
|Start|Enter||Pause|

## TODOs
- Add runtime NTSC/PAL switching
- Add Highscore table
- Add additional scoring mechanics, such as combo and back-to-back bonuses
- Add singleplayer elimination mode
- Further randomization tweaks - ~~it currently likes producing big droughts and floods of Z pieces if one wasn't dealt in the first couple "turns"~~ (actually, this is fixed now)
- Add an image of St. Basil's Cathedral somewhere :)
- ~~Add music?~~ (DONE!)

## Misc
- Uncomment the `drawdebug` calls in bag_random.spin to visualize the randomizer's state (only works properly in 1P mode)
