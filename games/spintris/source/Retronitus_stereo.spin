
{{
+------------------------------------------------------------------------------------------------------------------------------+
|         Retronitus DX and BlasterShot - (C) 2009-16 Johannes Ahlebrand, (C) 2019 IRQsome Software/Ada Gottensträter          |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|                                    TERMS OF USE: Parallax Object Exchange License                                            |                                                            
+------------------------------------------------------------------------------------------------------------------------------+
|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    | 
|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
|is furnished to do so, subject to the following conditions:                                                                   |
|                                                                                                                              |
|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
|                                                                                                                              |
|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
+------------------------------------------------------------------------------------------------------------------------------+
}}
CON
  SAMPLE_RATE = 64_000.0 'Hz

  nf  = 0.9438743967 
  MSN = 2_000_000_000.0 * (67878.79 / SAMPLE_RATE)  
  f12 = MSN,      f11 = f12 * nf, f10 = f11 * nf, f09 = f10 * nf, f08 = f09 * nf, f07 = f08 * nf
  f06 = f07 * nf, f05 = f06 * nf, f04 = f05 * nf, f03 = f04 * nf, f02 = f03 * nf, f01 = f02 * nf

VAR                  
                    word  triggers[8] 
                    word  initPatterns[8] 
                    word  instruments[16]
                    byte  cog
                    byte  pause
PUB start(right, left) | i
  stop
  init
  pausePointer     := @pause
  triggerPointer   := @triggers
  instrumenPointer := @instruments
  tempValue1       := $18000000 | left
  tempValue2       := $18000000 | right
  tempValue3       := ((1 << right) | (1 << left)) & !1
  sampleRate       := clkfreq/trunc(SAMPLE_RATE)
  cog              := cognew(@retronitus, @initPatterns) + 1

PUB stop
  if cog
    cogstop(cog~ -1)
    cog := 0

PUB playMusic(musicPointer) | i
  pauseMusic(false)
  stop
  init   
  initMusic(musicPointer)   
  cog := cognew(@retronitus, @initPatterns) + 1

PUB pauseMusic(p)
  pause := p
  muteAll

PUB playSoundEffect(channel, FX_Address)
  triggers[channel] := FX_Address

PUB muteChannel(channel)
  triggers[channel] := @muted  

PUB muteAll
  wordfill(@triggers, @muted, 8)  

PRI init
  wordfill(@instruments, @muted, 16)
  wordfill(@triggers, @muted, 8)
  wordfill(@initPatterns, @nullPattern, 8) 

PRI initMusic(musicPointer) | i, j, addVal ' The init routine simply adds the "runtime address offset" to the music data in memory
  musicPointer += 2
  i := 0
  repeat j from 0 to 7                                   ' Init start pattern for each channel  
    ifnot word[musicPointer][i] == 0
      initPatterns[j] := word[musicPointer][i] + musicPointer
    i++             
  j~             
  repeat until word[musicPointer][i] == 0                ' Init instrument data  
    instruments[j++] := word[musicPointer][i++] + musicPointer
  i++
  'i >>= 1
  if word[musicPointer][-1] & $8000 == 0
    repeat until word[musicPointer][i] == $FFFE      ' Init pattern sequencies      
      ifnot word[musicPointer][i] & 1
        word[musicPointer][i] += musicPointer<<1 
      i++
    word[musicPointer][-1] |= $8000
  

DAT           org 0
retronitus
outC          jmp #initialize
pausePointer        long 0
triggerPointer      long 0  ' This points to the first trigger register in hub memory
instrumenPointer    long 0  ' This points to the first instrument position in hub memory
sampleRate          long 0 '<- These  
tempValue1          long 1 '<- four
tempValue2          long 1 '<- variables
tempValue3          long 1 '<- are used to initialize retronitus
tempValue4  ' Aliased over first init instruction
initialize
mov ctra, tempValue1                     
mov ctrb, tempValue2  
mov frqa, val31bit
mov frqb, val31bit                  
mov dira, tempValue3

'-----------------------------------------------------------
selectedTrigPointer ' variable overwrites this instruction
              rdbyte    tempValue1,pausePointer wz  ' initialization is stalled while pause is active
c1_noteOn   ' following 8 init instructions are reused (current instrument pointers)
        if_nz jmp       #$-1          
reStart       mov       tempValue1, par              
              movd      :par, #c1_patternPointer         
              mov       tempValue2, #8       
:par          rdword    0-0, tempValue1        
              add       :par, val512
              add       tempValue1, #2
              djnz      tempValue2, #:par
'-----------------------------------------------------------  
              mov       cnt, cnt
              add       cnt, sampleRate
'-----------------------------------------------------------
'                     Task scheduler
'-----------------------------------------------------------
mainLoop      cmpsub    tablePointerOffset, #1           wc  '
        if_nc mov       tablePointerOffset, #10              ' currentTableP = #waveTablePointer1 + tablePointerOffset
              cmp       tablePointerOffset, #8           wz  
        if_z  jmp       #trigger                             ' On frame 8/10 , jump to the trigger subroutine 
              cmp       tablePointerOffset, #9       wz, wc   
enableM if_z  jmp       #music                               ' On frame 9/10 , jump to the music subroutine
if_nz_and_nc  jmp       #prepMusic                           ' On frame 10/10, jump to the prepare music subroutine

'-----------------------------------------------------------
' Read and interpret instructions/commands from hub memory   ' On frame 0 to 7, handle instruction execution for channel 0 - 7
'-----------------------------------------------------------
              mov       tempValue3, #waveTablePointer1        
              add       tempValue3, tablePointerOffset        
              movs      readCommand, tempValue3              ' Read instruction from HUB memory for channel X
              movd      incCurrTableP, tempValue3            '        
readCommand   rdlong    tempValue2, 0-0                       
              mov       tempValue1, tempValue2               
'-----------------------------------------------------------
incCurrTableP add       0-0, #4                              ' If not in repeat mode, point to next long (data)
              movd      incCurrTblP2, tempValue3              
'-----------------------------------------------------------
              mov       tempValue4, #c1_freq                  
              add       tempValue4, tablePointerOffset       ' currentRegPointer = ch1_freq + regOffsetPointer + registerSelect 
              and       tempValue2, #$7                       
              shl       tempValue2, #3
              add       tempValue4, tempValue2                
'-----------------------------------------------------------
              movs      setHubAddress,  tempValue3           '
              'movd     jump, tempValue3                     ' Read in data from the hub address contained in "currentTableP"
'-----------------------------------------------------------
              cmp       channelUpdate, #0                wz  '
       if_nz  jmp       #setHubAddress                       '
              rdbyte    tempValue2, pausePointer         wz  ' Handle pausing
       if_nz  movs      enableM, #phaseAcc                   '  
       if_z   movs      enableM, #music                      '
'----------------------------------------------------------- 
setHubAddress rdlong    tempValue2, 0-0                      '
'-----------------------------------------------------------                                                           '
              movd      set, tempValue4                      ' Prepare destination registers
              movd      modify, tempValue4                   ' 
              test      tempValue1, #$8                  wz  '
set     if_z  mov       0-0, tempValue2                      ' If command = 0: currentRegPointer  = dataValue  
modify  if_nz add       0-0, tempValue2                      ' If command = 1: currentRegPointer += dataValue
'-----------------------------------------------------------
              andn      tempValue1, #15                  wz  '  
incCurrTblP2  sumnz     0-0, #4                              ' If not in repeat mode, point to next long (command)

'-----------------------------------------------------------
'                     Sound synthesis            
'-----------------------------------------------------------
phaseAcc
ch1_Square
      add c1_phase,c1_freq
      test c1_modspeed,#511 wz
if_nz movi c1_modphase,c1_modspeed
if_z  add  c1_modphase,c1_modspeed
      add  c1_envelope,c1_ASD
      test c1_envelope,volumeBits wz
if_z  movi c1_envelope,c1_ASD
      max  c1_envelope,c1_vol

      cmp  c1_modphase,c1_phase wc
      mov  tempValue1,c1_envelope
      shr  tempValue1,#4
      sumc outC,tempValue1
ch2_Square
      add c2_phase,c2_freq
      test c2_modspeed,#511 wz
if_nz movi c2_modphase,c2_modspeed
if_z  add  c2_modphase,c2_modspeed
      add  c2_envelope,c2_ASD
      test c2_envelope,volumeBits wz
if_z  movi c2_envelope,c2_ASD
      max  c2_envelope,c2_vol

      cmp  c2_modphase,c2_phase wc
      mov  tempValue1,c2_envelope
      shr  tempValue1,#4
      sumc outC,tempValue1
ch3_Sawtooth
add c3_phase,c3_freq
      test c3_modspeed,#511 wz
if_nz movi c3_modphase,c3_modspeed
if_z  add  c3_modphase,c3_modspeed
      add  c3_envelope,c3_ASD
      test c3_envelope,volumeBits wz
if_z  movi c3_envelope,c3_ASD
      max  c3_envelope,c3_vol

      mov  tempValue1,c3_phase wc
      abs  tempValue2,c3_modphase
      sumc tempValue1,tempValue2 wc
      mov  tempValue2,c3_envelope
      call #sar9Multiply
      add  outS,tempValue3
ch4_Sawtooth
add c4_phase,c4_freq
      test c4_modspeed,#511 wz
if_nz movi c4_modphase,c4_modspeed
if_z  add  c4_modphase,c4_modspeed
      add  c4_envelope,c4_ASD
      test c4_envelope,volumeBits wz
if_z  movi c4_envelope,c4_ASD
      max  c4_envelope,c4_vol

      mov  tempValue1,c4_phase wc
      abs  tempValue2,c4_modphase
      sumc tempValue1,tempValue2 wc
      mov  tempValue2,c4_envelope
      call #sar9Multiply
      add  outS,tempValue3
ch5_Sawtooth
add c5_phase,c5_freq
      test c5_modspeed,#511 wz
if_nz movi c5_modphase,c5_modspeed
if_z  add  c5_modphase,c5_modspeed
      add  c5_envelope,c5_ASD
      test c5_envelope,volumeBits wz
if_z  movi c5_envelope,c5_ASD
      max  c5_envelope,c5_vol

      mov  tempValue1,c5_phase wc
      abs  tempValue2,c5_modphase
      sumc tempValue1,tempValue2 wc
      mov  tempValue2,c5_envelope
      call #sar9Multiply
      add  outS,tempValue3
ch6_Sawtooth
add c6_phase,c6_freq
      test c6_modspeed,#511 wz
if_nz movi c6_modphase,c6_modspeed
if_z  add  c6_modphase,c6_modspeed
      add  c6_envelope,c6_ASD
      test c6_envelope,volumeBits wz
if_z  movi c6_envelope,c6_ASD
      max  c6_envelope,c6_vol

      mov  tempValue1,c6_phase wc
      abs  tempValue2,c6_modphase
      sumc tempValue1,tempValue2 wc
      mov  tempValue2,c6_envelope
      call #sar9Multiply
      add  outC,tempValue3
ch7_Triangle
add c7_phase,c7_freq

      add  c7_envelope,c7_ASD
      test c7_envelope,volumeBits wz
if_z  movi c7_envelope,c7_ASD
      max  c7_envelope,c7_vol

      abs  tempValue1,c7_phase
      and  tempValue1,c7_modspeed
      sub  tempValue1,val30bit
      sar  tempValue1,#6
      mov  tempValue2,c7_envelope
      call #multiply
      add  outC,tempValue3
ch8_LFSR
add c8_phase,c8_freq wc

      muxc :selfmod8,#1
      add  c8_envelope,c8_ASD
      test c8_envelope,volumeBits wz
if_z  movi c8_envelope,c8_ASD
      max  c8_envelope,c8_vol

      test c8_modphase,c8_modspeed wc
:selfmod8
      rcr c8_modphase,#0-0 wc
      mov  tempValue1,c8_envelope
      shr  tempValue1,#3
      sumc outC,tempValue1
mixer
    add outL,outC
    add outR,outC
    add outL,outS
    sub outR,outS
    waitcnt cnt, sampleRate
    mov frqa,outL
    mov frqb,outR
    mov outL,val31bit
    mov outR,val31bit
    mov outC,#0
    mov outS,#0
    jmp #mainLoop
outL long 0
outR long 0
outS long 0

'-----------------------------------------------------------
'               Handle sound triggering
'-----------------------------------------------------------
trigger       cmpsub    triggerOffset, #1                wc '
        if_nc mov       triggerOffset, #7                   ' 
              mov       selectedTrigPointer, triggerOffset  ' tempValue = iterates through the hub addresses of trigger1 - trigger8
              shl       selectedTrigPointer, #1             '
              add       selectedTrigPointer, triggerPointer '
              mov       tempValue3, #waveTablePointer1      '  
              rdword    tempValue2, selectedTrigPointer  wz '
'-----------------------------------------------------------
              add       tempValue3, triggerOffset           ' Sets the tablePointer to be affected
              movd      wrToWavT, tempValue3                '   
        if_nz wrword    zero, selectedTrigPointer           '
wrToWavT if_nz mov      0-0, tempValue2                     ' set the address in the selected waveTablePointer
'-----------------------------------------------------------
'               Instruction repeat handling
'-----------------------------------------------------------
              movs      readRepAmount, tempValue3           ' 
              movd      incCT, tempValue3                   ' Reads in the repetation amount value
'-----------------------------------------------------------
readRepAmount rdlong    tempValue3, 0-0                    
              shr       tempValue3, #4                          
'-----------------------------------------------------------
              mov       tempValue1, #repeatCounter1         '
              add       tempValue1, triggerOffset           ' Sets which repeatCounter to affect    
              movd      src1, tempValue1               
              movd      src2, tempValue1 
              movd      src3, tempValue1 
'-----------------------------------------------------------
src1   if_nz  mov       0-0, #0              
src2          cmpsub    0-0, #1                      wc, wz ' Decrement the selected repeatCounter and check if zero(nc) or one(z)
src3   if_nc  mov       0-0, tempValue3                     ' If zero, set the the selected repeatCounter to command
incCT  if_z   add       0-0, #8                             ' If one, increment the selected waveTablePointer
              jmp       #phaseAcc

'-----------------------------------------------------------
' Point variables in the music routine to the next channel (round robin)
'-----------------------------------------------------------
prepMusic     mov       tempValue1, #c1_freq               ' Freq
              add       tempValue1, triggerOffset
              movd      setNote, tempValue1
              movd      F3, tempValue1
              sub       tempValue1, #c1_freq - c1_stepWait ' stepWait
              movd      A1, tempValue1 
              movd      A2, tempValue1 
              movd      A3, tempValue1 
              sub       tempValue1, #c1_stepWait - c1_noteOn ' noteOn
              movd      C1, tempValue1 
              movd      C2, tempValue1
              movs      C3, tempValue1
              add       tempValue1, #c1_note - c1_noteOn   ' note
              movd      E1, tempValue1
              movd      E2, tempValue1
              movd      E3, tempValue1
              movd      E4, tempValue1
              movs      E5, tempValue1
              movd      E6, tempValue1
              movd      E7, tempValue1
              movd      E8, tempValue1
              add       tempValue1, #c1_octave - c1_note   ' octave
              movd      F1, tempValue1
              movd      F2, tempValue1
              movs      F3, tempValue1
              movd      F4, tempValue1
              movd      F5, tempValue1
              add       tempValue1, #c1_patternPointer - c1_octave ' patternPointer
              movs      G1, tempValue1
              movd      G2, tempValue1 
              sub       tempValue1, #c1_patternPointer - c1_stepPointer ' stepPointer
              movs      pattern, tempValue1  
              movd      B1, tempValue1     
              movs      B3, tempValue1     
              movd      B4, tempValue1     
              jmp       #phaseAcc

'-----------------------------------------------------------
'                     Music routine
'-----------------------------------------------------------
music         add       tempoAccumulator, tempo          wc  
 if_c         mov       channelUpdate, #255      
              shr       channelUpdate, #1                wc
 if_nc        jmp       #patternHandler
'-----------------------------------------------------------
A1            cmpsub    c1_stepWait, #1                  wc
        if_c  jmp       #phaseAcc
'-----------------------------------------------------------  Handle pattern stepping and note triggering
pattern       rdbyte    tempValue1, c1_stepPointer       wz '
        if_z  jmp       #phaseAcc
B1            add       c1_stepPointer, #1                  ' If note equals zero, don't increment the step pointer
A2            mov       c1_stepWait, tempValue1             '
A3            and       c1_stepWait, #7                     ' Gets "note shift" and "step wait"
'-----------------------------------------------------------
              shr       tempValue1, #3                      '  
              cmpsub    tempValue1, #25              wz, wc '
        if_c  shl       tempValue1, #3                      '
C3            mov       tempValue2, c1_noteOn               '
              sub       tempValue2, tempValue1              '  
 if_c_and_nz  wrword    tempValue2, selectedTrigPointer     '
        if_c  jmp       #phaseAcc                           '
C1      if_nc wrword    c1_noteOn, selectedTrigPointer      '                           '
'-----------------------------------------------------------           
              sub       tempValue1, #12                     ' Note -> -12 - +12  
E1            add       c1_note, tempValue1              wc  
E2            cmps      c1_note, #0                      wc ' If currentNote < 0  
E3      if_c  add       c1_note, #12                        '   currentNote += 12 
F1      if_c  add       c1_octave, #1                       '   octave += 1
E4            cmpsub    c1_note, #12                     wc ' If currentNote > 11 | currentNote -= 12
'----------------------------------------------------------- 
              mov       tempValue1, #fTable                 
E5            add       tempValue1, c1_note                  
              movs      setNote, tempValue1                 ' Sets the current frequency
'----------------------------------------------------------- 
F2      if_c  sub       c1_octave, #1                       '   octave -= 1
'-----------------------------------------------------------                        '
setNote       mov       c1_freq, 0-0                         
F3            shr       c1_freq, c1_octave                  
              jmp       #phaseAcc
'-----------------------------------------------------------
'              Handle loading of new patterns
'-----------------------------------------------------------
patternHandler                                             
B3            rdbyte    tempValue1, c1_stepPointer       wz
        'if_nz jmp       #phaseAcc
             'nop
G1      if_z rdword   tempValue1, c1_patternPointer 

         if_z shr       tempValue1, #1                  wc  ' check bit 0
B4 if_nc_and_z mov      c1_stepPointer, tempValue1          ' run pattern
  if_nc_and_z jmp       #patternNext

        if_z shr        tempValue1,#1                   wc  ' check bit 1
   if_c_and_z jmp       #patternSetStuff                    ' if set, set instrument, octave, note
        if_z shr        tempValue1,#1                   wc  ' check bit 2
  if_c_or_nz jmp        #patternJump
'----------------------------------------------------------- 
              mov       tempo, tempValue1                     
              shl       tempo, #14                          ' set tempo                      
              jmp       #patternNext                         
              
patternJump
        if_z  shr       tempValue1,#1                   wc ' use bit 3 as sign
              jmp       #patternSum                                                                
'-----------------------------------------------------------
patternSetStuff
F4            mov       c1_octave, tempValue1                
F5            shr       c1_octave, #10                       
E6            mov       c1_note, tempValue1                 ' This takes care of "octave" and "note" init values
E7            shr       c1_note, #6                          
E8            and       c1_note, #15              
'-----------------------------------------------------------                                                             
              and       tempValue1, #63                     ' Note = noteOn address 
              add       tempValue1, instrumenPointer              '
C2            rdword    c1_noteOn, tempValue1

patternNext   mov       tempValue1, #2 wc
patternSum
G2      if_z  sumc      c1_patternPointer, tempValue1
              jmp       #phaseAcc

'-----------------------------------------------------------
'    Multiplication     r1(I32) = arg1(I32) * arg2(I6)
'-----------------------------------------------------------
sar9Multiply  sar       tempValue1, #9          
multiply      mov       tempValue3, #0
              test      tempValue2, val26bit             wc
        if_c  add       tempValue3, tempValue1
              shl       tempValue1, #1
              test      tempValue2, val27bit             wc
        if_c  add       tempValue3, tempValue1
              shl       tempValue1, #1
              test      tempValue2, val28bit             wc
        if_c  add       tempValue3, tempValue1
              shl       tempValue1, #1
              test      tempValue2, val29bit             wc
        if_c  add       tempValue3, tempValue1
              shl       tempValue1, #1
              test      tempValue2, val30bit             wc
        if_c  add       tempValue3, tempValue1
              shl       tempValue1, #1
              test      tempValue2, val31bit             wc
        if_c  add       tempValue3, tempValue1
sar9Multiply_ret
multiply_ret  ret

'-----------------------------------------------------------
'                    Frequency table  
'-----------------------------------------------------------
fTable              long trunc(f01)
                    long trunc(f02)
                    long trunc(f03)
                    long trunc(f04)
                    long trunc(f05)
                    long trunc(f06)
                    long trunc(f07)
                    long trunc(f08)
                    long trunc(f09)
                    long trunc(f10)
                    long trunc(f11)
                    long trunc(f12)

'-----------------------------------------------------------
'    Pre-initialized masks and reference values
'-----------------------------------------------------------
val31bit            long $80000000
val30bit            long $40000000
val29bit            long $20000000
val28bit            long $10000000
val27bit            long $8000000
val26bit            long $4000000
val15bit            long $8000
tempoBits           long $ffff0000
volumeBits          long $fc000000 
patternEnd          long $ffffffff

'-----------------------------------------------------------
'                  Variables and pointers
'-----------------------------------------------------------          
channelUpdate       long 0 
zero                long 0   
tablePointerOffset  long 1 
triggerOffset       long 1 
val512              long 1 << 9
tempo               long 1_000_000_000
tempoAccumulator    long 0

c1_stepPointer      long $8080[8] ' Address to the current pattern step

c1_stepWait         long 0
c2_stepWait         long 0
c3_stepWait         long 0
c4_stepWait         long 0
c5_stepWait         long 0
c6_stepWait         long 0
c7_stepWait         long 0
c8_stepWait         long 0

'-----------------------------------------------------------
'               Channel registers
'-----------------------------------------------------------
c1_freq             long 0 ' bit 31-0:  Signed frequency
c2_freq             long 0 
c3_freq             long 0
c4_freq             long 0
c5_freq             long 0
c6_freq             long 0
c7_freq             long 0
c8_freq             long 0
c1_phase            long 0
c2_phase            long 0
c3_phase            long 0
c4_phase            long 0
c5_phase            long 0
c6_phase            long 0
c7_phase            long 0
c8_phase            long 0
c1_modspeed         long 0  ' bit 31-10: Waveform modulation rate, bit 9-0: Set fixed wafeform
c2_modspeed         long 0
c3_modspeed         long 0
c4_modspeed         long 0
c5_modspeed         long 0
c6_modspeed         long 0
c7_modspeed         long 0
c8_modspeed         long 0
c1_modphase         long 0
c2_modphase         long 0
c3_modphase         long 0
c4_modphase         long 0
c5_modphase         long 0
c6_modphase         long 0
c7_modphase         long 0
c8_modphase         long 0
c1_ASD              long 0  ' bit 31-5:  Attack/Decay/AM rate, bit 4-0:  Amplitude modulation amount
c2_ASD              long 0
c3_ASD              long 0
c4_ASD              long 0
c5_ASD              long 0
c6_ASD              long 0
c7_ASD              long 0
c8_ASD              long 0
c1_vol              long 0  ' bit 31-27: Sustain level
c2_vol              long 0
c3_vol              long 0
c4_vol              long 0
c5_vol              long 0
c6_vol              long 0
c7_vol              long 0
c8_vol              long 0
c1_envelope         long 0
c2_envelope         long 0
c3_envelope         long 0
c4_envelope         long 0
c5_envelope         long 0
c6_envelope         long 0
c7_envelope         long 0
c8_envelope         long 0
waveTablePointer1   long $8080
waveTablePointer2   long $8080      
waveTablePointer3   long $8080      
waveTablePointer4   long $8080      
waveTablePointer5   long $8080  
waveTablePointer6   long $8080  
waveTablePointer7   long $8080  
waveTablePointer8   long $8080

'-----------------------------------------------------------
'               Internal Retronitus registers
'-----------------------------------------------------------
repeatCounter1      res  1
repeatCounter2      res  1
repeatCounter3      res  1
repeatCounter4      res  1
repeatCounter5      res  1
repeatCounter6      res  1
repeatCounter7      res  1
repeatCounter8      res  1                                       
c1_note             res  8  ' Holds the current note (0 - 11)
c1_octave           res  8  ' Holds the current octave 
c1_patternPointer   res  8  ' Address to the current pattern init 
                    fit 496
DAT                        
nullPattern         long  13
muted               'long  0+0,  $0000_0000 ' ch1_freq  - bit 31-0:  Signed frequency 
                    long  0+5,  $0000_0000 ' ch1_vol   - bit 31-27: Sustain level
                    long  8+7, -(4<<3)     ' jump -1 steps
