OBJ
retrosnd: "Retronitus_stereo.spin"

CON

#0,TRACK_NONE,TRACK_KOROBEINIKI,TRACK_BRADINSKY

#0,SFX_LOCK,SFX_MOVE,SFX_SPIN,SFX_LINE,SFX_LEVEL

VAR
long playing

PUB init(left,right)
  retrosnd.start(left,right)
  play(TRACK_NONE)

PUB play(music)
  playing := music
  set_tempo(false)
  retrosnd.playMusic(lookupz(music:@NULL_DAT,@KOROBEINIKI_DAT,@BRADINSKY_DAT))  


PUB set_tempo(high) : t

case playing
  TRACK_NONE:
    t:=((((BPM*100)*4)>>15)&$FFF0)|1
  TRACK_KOROBEINIKI:
    ifnot high 
      t:=((((BPM*150)*32)>>15)&$FFF0)|1
    else
      t:=((((BPM*150)*56)>>15)&$FFF0)|1 
      
  TRACK_BRADINSKY:
    ifnot high 
      t:=((((BPM*160)*96)>>15)&$FFF0)|1
    else
      t:=((((BPM*160)*144)>>15)&$FFF0)|1
word[@tempochan] := t 

PUB sfx(which)

retrosnd.PlaySoundEffect(7,lookupz(which:@sfxdata_lock,@sfxdata_move,@sfxdata_spin,@sfxdata_line,@sfxdata_level))

DAT

sfxdata_lock
        long    SET|FREQUENCY,                  1070*Hz*4
        long    SET|PHASE,                      0
        long    SET|MODULATION,                 $CCC0_00C0
        long    SET|MODPHASE,                   $DA45_4156
        long    SET|ENVELOPE,                   $00500000|$008
        long    SET|VOLUME,                     $AE000000
        long    MODIFY|FREQUENCY|(10*wait_ms),  -12*(Hz/88)
        long    SET|ENVELOPE|(60*wait_ms),      $FFF68000|$008
        long    SET|VOLUME,                     $00000000    
        long    JUMP,                           -1*STEPS

sfxdata_move     
        long    SET|VOLUME,                     $00000000
        long    SET|FREQUENCY,                  440*Hz*16  
        long    SET|PHASE,                      0 
        long    SET|MODULATION,                 $0000_0001
        long    SET|MODPHASE,                   $E000_E000
        long    SET|ENVELOPE,                   $001FF000|$008
        long    SET|ENVELOPE_NOW,               $00000000
        long    SET|VOLUME|(30*wait_ms),        $7E000000
        long    MODIFY|MODPHASE,                $C000_C000
        long    SET|VOLUME,                     $66600000      
        long    SET|ENVELOPE|(40*wait_ms),    -$002FF000|$008
        long    SET|VOLUME,                     $00000000    
        long    JUMP,                           -1*STEPS

sfxdata_spin     
        long    SET|VOLUME,                     $00000000
        long    SET|PHASE,                      0 
        long    SET|MODULATION,                 $0000_0001
        long    SET|MODPHASE,                   $C0C0_C0C0
        long    SET|ENVELOPE,                  -$000030000|$008
        long    SET|VOLUME,                     $96700000
        long    SET|ENVELOPE_NOW,               $FF000000 
        long    SET|FREQUENCY|(12*wait_ms),     740*Hz*8
        long    SET|FREQUENCY|(12*wait_ms),     660*Hz*8
        long    SET|FREQUENCY|(12*wait_ms),     440*Hz*8
        long    SET|FREQUENCY|(12*wait_ms),     660*Hz*8
        long    SET|FREQUENCY|(12*wait_ms),     440*Hz*8     
        long    SET|VOLUME,                     $00000000    
        long    JUMP,                           -1*STEPS

sfxdata_line
        long    SET|PHASE,                      0 
        long    SET|MODULATION,                 $0000_0001
        long    SET|MODPHASE,                   $F0F0_F0F0
        long    SET|ENVELOPE,                  -$000008000|$008
        long    SET|VOLUME,                     $98000000
        long    SET|ENVELOPE_NOW,               $FF000000 
        long    SET|FREQUENCY|(30*wait_ms),     110*Hz*8 
        long    SET|FREQUENCY|(40*wait_ms),      83*Hz*8 
        long    SET|FREQUENCY|(30*wait_ms),     220*Hz*8 
        long    SET|FREQUENCY|(40*wait_ms),     165*Hz*8
        long    SET|FREQUENCY|(30*wait_ms),     440*Hz*8
        long    SET|FREQUENCY|(40*wait_ms),     330*Hz*8
        long    SET|FREQUENCY|(30*wait_ms),     660*Hz*8
        long    SET|FREQUENCY|(40*wait_ms),     440*Hz*8
        long    SET|FREQUENCY|(30*wait_ms),     880*Hz*8  
        long    SET|FREQUENCY|(40*wait_ms),     440*Hz*8
        long    SET|FREQUENCY|(30*wait_ms),    1320*Hz*8   
             
        long    SET|VOLUME,                     $00000000    
        long    JUMP,                           -1*STEPS

sfxdata_level
        long    SET|FREQUENCY,                  392*Hz*10*4 
        long    SET|PHASE,                      0 
        long    SET|MODULATION,                 $0000_0001
        long    SET|MODPHASE,                   $8811_77EE
        long    SET|ENVELOPE,                   $002C1000|$008
        long    SET|ENVELOPE_NOW,               $FF000000
        long    SET|VOLUME|(10*wait_ms),        $A0000000 
        long    SET|MODPHASE,                   $8811_77EE
        long    SET|ENVELOPE|(80*wait_ms),     -$00041000|$008
        long    SET|ENVELOPE_NOW,               $FF000000
        long    SET|FREQUENCY|(80*wait_ms),     494*Hz*10*4
        long    SET|ENVELOPE_NOW,               $FF000000
        long    SET|FREQUENCY|(180*wait_ms),    587*Hz*10*4  
        long    SET|VOLUME,                     $00000000    
        long    JUMP,                          -1*STEPS

DAT
NULL_DAT      word 0
B0 '' song base
SEQ_INIT0
              word @nullchan   -(@B0-RLOC)
              word @nullchan   -(@B0-RLOC)
              word @nullchan   -(@B0-RLOC)   
              word @nullchan   -(@B0-RLOC)   
              word @nullchan   -(@B0-RLOC)   
              word @nullchan   -(@B0-RLOC)
              word @nullchan   -(@B0-RLOC)
              word @nullchan   -(@B0-RLOC) 
'──────────────────────────────────────────────────────────────────────────────────────────
INS_INIT0
              word @instr_dummy  -(@B0-RLOC)
              word  END

word $FFFE

DAT
  
KOROBEINIKI_DAT word  0
'──────────────────────────────────────────────────────────────────────────────────────────
B1 '' song base
SEQ_INIT1
              word @koro_chan02_0    -(@B1-RLOC)
              word @koro_chan03_0    -(@B1-RLOC)
              word @koro_chan04_0    -(@B1-RLOC)   
              word @koro_chan04_1    -(@B1-RLOC)   
              word @koro_chan04_2    -(@B1-RLOC)   
              word @koro_chan01_0    -(@B1-RLOC)
              word @koro_chan05_0    -(@B1-RLOC)
              word @tempochan  -(@B1-RLOC) 
'──────────────────────────────────────────────────────────────────────────────────────────
INS_INIT1
              word @instr_dummy            -(@B1-RLOC)'
              word @instr_sawbass          -(@B1-RLOC)
              word @instr_korochorus       -(@B1-RLOC)
              word @instr_korobridge_low   -(@B1-RLOC)
              word @instr_korobridge_high  -(@B1-RLOC)
              word @instr_koroharmony      -(@B1-RLOC)
              word @instr_drumkit          -(@B1-RLOC)

              word  END 

DAT
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
'                               Pattern sequency data
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────

koro_chan01_0      
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 0) |3,    (@patt_init  -(@B1-RLOC))<<1
  
  word (OCTAVE * 9) | (NOTE * 4)               | (INSTR * 1) |3,    (@koro_patt_c01_001_part0  -(@B1-RLOC))<<1
  word (OCTAVE *10) | (NOTE *11)               | (INSTR * 1) |3,    (@koro_patt_c01_001_part1  -(@B1-RLOC))<<1
  word (OCTAVE *10) | (NOTE *11)               | (INSTR * 1) |3,    (@koro_patt_c01_001_part2  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE * 4)               | (INSTR * 1) |3,    (@koro_patt_c01_001_part3  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE * 4)               | (INSTR * 1) |3,    (@koro_patt_c01_001_part4  -(@B1-RLOC))<<1  
  word (OCTAVE * 9) | (NOTE * 8)               | (INSTR * 1) |3,    (@koro_patt_c01_002_part0  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE * 3)               | (INSTR * 1) |3,    (@koro_patt_c01_002_part1  -(@B1-RLOC))<<1
  word (OCTAVE *10) | (NOTE *11)               | (INSTR * 1) |3,    (@koro_patt_c01_002_part2  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE * 4)               | (INSTR * 1) |3,    (@koro_patt_c01_002_part3  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 2)               | (INSTR * 1) |3,    (@koro_patt_c01_003_part0  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 2)               | (INSTR * 1) |3,    (@koro_patt_c01_003_part1  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 0)               | (INSTR * 1) |3,    (@koro_patt_c01_003_part2  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 0)               | (INSTR * 1) |3,    (@koro_patt_c01_003_part3  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE *11)               | (INSTR * 1) |3,    (@koro_patt_c01_004_part0  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 6)               | (INSTR * 1) |3,    (@koro_patt_c01_004_part1  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 8)               | (INSTR * 1) |3,    (@koro_patt_c01_004_part2  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 9)               | (INSTR * 1) |3,    (@koro_patt_c01_004_part3  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 9)               | (INSTR * 1) |3,    (@koro_patt_c01_004_part4  -(@B1-RLOC))<<1 
koro_chan01_0_loop
  word (OCTAVE * 9) | (NOTE * 4)               | (INSTR * 1) |3,    (@koro_patt_c01_001_part0  -(@B1-RLOC))<<1
  word (OCTAVE *10) | (NOTE *11)               | (INSTR * 1) |3,    (@koro_patt_c01_001_part1  -(@B1-RLOC))<<1
  word (OCTAVE *10) | (NOTE *11)               | (INSTR * 1) |3,    (@koro_patt_c01_001_part2  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE * 4)               | (INSTR * 1) |3,    (@koro_patt_c01_001_part3  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE * 4)               | (INSTR * 1) |3,    (@koro_patt_c01_001_part4  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE * 8)               | (INSTR * 1) |3,    (@koro_patt_c01_002_part0  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE * 3)               | (INSTR * 1) |3,    (@koro_patt_c01_002_part1  -(@B1-RLOC))<<1
  word (OCTAVE *10) | (NOTE *11)               | (INSTR * 1) |3,    (@koro_patt_c01_002_part2  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE * 4)               | (INSTR * 1) |3,    (@koro_patt_c01_002_part3  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 2)               | (INSTR * 1) |3,    (@koro_patt_c01_003_part0  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 2)               | (INSTR * 1) |3,    (@koro_patt_c01_003_part1  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 0)               | (INSTR * 1) |3,    (@koro_patt_c01_003_part2  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 0)               | (INSTR * 1) |3,    (@koro_patt_c01_003_part3  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE *11)               | (INSTR * 1) |3,    (@koro_patt_c01_004_part0  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 6)               | (INSTR * 1) |3,    (@koro_patt_c01_004_part1  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 8)               | (INSTR * 1) |3,    (@koro_patt_c01_004_part2  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 9)               | (INSTR * 1) |3,    (@koro_patt_c01_004_part3  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 9)               | (INSTR * 1) |3,    (@koro_patt_c01_004_part4  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c01_005  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE * 9)               | (INSTR * 1) |3,    (@koro_patt_c01_006  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE * 9)               | (INSTR * 1) |3,    (@koro_patt_c01_005  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE * 9)               | (INSTR * 1) |3,    (@koro_patt_c01_006  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c01_001_part0  -(@B1-RLOC))<<1
  word (OCTAVE *10) | (NOTE *11)               | (INSTR * 1) |3,    (@koro_patt_c01_001_part1  -(@B1-RLOC))<<1
  word (OCTAVE *10) | (NOTE *11)               | (INSTR * 1) |3,    (@koro_patt_c01_001_part2  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE * 4)               | (INSTR * 1) |3,    (@koro_patt_c01_001_part3  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE * 4)               | (INSTR * 1) |3,    (@koro_patt_c01_001_part4  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE * 8)               | (INSTR * 1) |3,    (@koro_patt_c01_002_part0  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE * 3)               | (INSTR * 1) |3,    (@koro_patt_c01_002_part1  -(@B1-RLOC))<<1
  word (OCTAVE *10) | (NOTE *11)               | (INSTR * 1) |3,    (@koro_patt_c01_002_part2  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE * 4)               | (INSTR * 1) |3,    (@koro_patt_c01_002_part3  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 2)               | (INSTR * 1) |3,    (@koro_patt_c01_003_part0  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 2)               | (INSTR * 1) |3,    (@koro_patt_c01_003_part1  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 0)               | (INSTR * 1) |3,    (@koro_patt_c01_003_part2  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 0)               | (INSTR * 1) |3,    (@koro_patt_c01_003_part3  -(@B1-RLOC))<<1
  word (OCTAVE * 9) | (NOTE *11)               | (INSTR * 1) |3,    (@koro_patt_c01_004_part0  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 6)               | (INSTR * 1) |3,    (@koro_patt_c01_004_part1  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 8)               | (INSTR * 1) |3,    (@koro_patt_c01_004_part2  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 9)               | (INSTR * 1) |3,    (@koro_patt_c01_004_part3  -(@B1-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 9)               | (INSTR * 1) |3,    (@koro_patt_c01_004_part4  -(@B1-RLOC))<<1
koro_chan01_0_end
  word ((@koro_chan01_0_end-@koro_chan01_0_loop)<<4)|13

koro_chan02_0    
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 0) |3,    (@patt_init  -(@B1-RLOC))<<1
                                     
  word (OCTAVE * 5) | (NOTE * 4)               | (INSTR * 2) |3,    (@koro_patt_c02_001to004  -(@B1-RLOC))<<1   
koro_chan02_0_loop 
  word (OCTAVE * 5) | (NOTE * 4)               | (INSTR * 2) |3,    (@koro_patt_c02_001to004  -(@B1-RLOC))<<1 
  word (OCTAVE * 6) | (NOTE * 4)               | (INSTR * 3) |3,    (@koro_patt_c02_005  -(@B1-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 3) |3,    (@koro_patt_c02_006  -(@B1-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 4)               | (INSTR * 3) |3,    (@koro_patt_c02_005  -(@B1-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 3) |3,    (@koro_patt_c02_007  -(@B1-RLOC))<<1
  word (OCTAVE * 5) | (NOTE * 4)               | (INSTR * 2) |3,    (@koro_patt_c02_001to004  -(@B1-RLOC))<<1 
koro_chan02_0_end
  word ((@koro_chan02_0_end-@koro_chan02_0_loop)<<4)|13


koro_chan03_0    
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 0) |3,    (@patt_init  -(@B1-RLOC))<<1
  
  word (OCTAVE * 6) | (NOTE *11)               | (INSTR * 2) |3,    (@koro_patt_c03_001to004  -(@B1-RLOC))<<1  
koro_chan03_0_loop
  word (OCTAVE * 6) | (NOTE *11)               | (INSTR * 2) |3,    (@koro_patt_c03_001to004  -(@B1-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 4) |3,    (@koro_patt_c03_005  -(@B1-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 9)               | (INSTR * 4) |3,    (@koro_patt_c03_006  -(@B1-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 4) |3,    (@koro_patt_c03_005  -(@B1-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 9)               | (INSTR * 4) |3,    (@koro_patt_c03_007  -(@B1-RLOC))<<1
  word (OCTAVE * 6) | (NOTE *11)               | (INSTR * 2) |3,    (@koro_patt_c03_001to004  -(@B1-RLOC))<<1
koro_chan03_0_end
  word ((@koro_chan03_0_end-@koro_chan03_0_loop)<<4)|13

koro_chan04_0    
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 0) |3,    (@patt_init  -(@B1-RLOC))<<1
  
  word                                                              (@koro_patt_c04_empty_32  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_empty_32  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_empty_32  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_empty_32  -(@B1-RLOC))<<1
koro_chan04_0_loop  
  word (OCTAVE * 7) | (NOTE * 8)               | (INSTR * 5) |3,    (@koro_patt_c04_001_s0  -(@B1-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 8)               | (INSTR * 5) |3,    (@koro_patt_c04_002_s0  -(@B1-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 5)               | (INSTR * 5) |3,    (@koro_patt_c04_003_s0  -(@B1-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 8)               | (INSTR * 5) |3,    (@koro_patt_c04_004_s0  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_005_s0  -(@B1-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 9)               | (INSTR * 5) |3,    (@koro_patt_c04_006_s0  -(@B1-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 9)               | (INSTR * 5) |3,    (@koro_patt_c04_005_s0  -(@B1-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 9)               | (INSTR * 5) |3,    (@koro_patt_c04_006_s0  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_001_s0  -(@B1-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 8)               | (INSTR * 5) |3,    (@koro_patt_c04_002_s0  -(@B1-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 5)               | (INSTR * 5) |3,    (@koro_patt_c04_003_s0  -(@B1-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 8)               | (INSTR * 5) |3,    (@koro_patt_c04_004_s0  -(@B1-RLOC))<<1
koro_chan04_0_end
  word ((@koro_chan04_0_end-@koro_chan04_0_loop)<<4)|13


koro_chan04_1   
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 0) |3,    (@patt_init  -(@B1-RLOC))<<1
  
  word                                                              (@koro_patt_c04_empty_32  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_empty_32  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_empty_32  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_empty_32  -(@B1-RLOC))<<1
koro_chan04_1_loop
  word (OCTAVE * 7) | (NOTE *11)               | (INSTR * 5) |3,    (@koro_patt_c04_001_s1  -(@B1-RLOC))<<1
  word (OCTAVE * 7) | (NOTE *11)               | (INSTR * 5) |3,    (@koro_patt_c04_002_s1  -(@B1-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 9)               | (INSTR * 5) |3,    (@koro_patt_c04_003_s1  -(@B1-RLOC))<<1
  word (OCTAVE * 7) | (NOTE *11)               | (INSTR * 5) |3,    (@koro_patt_c04_004_s1  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_005_s1  -(@B1-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 5) |3,    (@koro_patt_c04_006_s1  -(@B1-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 5) |3,    (@koro_patt_c04_005_s1  -(@B1-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 5) |3,    (@koro_patt_c04_006_s1  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_001_s1  -(@B1-RLOC))<<1
  word (OCTAVE * 7) | (NOTE *11)               | (INSTR * 5) |3,    (@koro_patt_c04_002_s1  -(@B1-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 9)               | (INSTR * 5) |3,    (@koro_patt_c04_003_s1  -(@B1-RLOC))<<1
  word (OCTAVE * 7) | (NOTE *11)               | (INSTR * 5) |3,    (@koro_patt_c04_004_s1  -(@B1-RLOC))<<1
koro_chan04_1_end
  word ((@koro_chan04_1_end-@koro_chan04_1_loop)<<4)|13


koro_chan04_2   
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 0) |3,    (@patt_init  -(@B1-RLOC))<<1
  
  word                                                              (@koro_patt_c04_empty_32  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_empty_32  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_empty_32  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_empty_32  -(@B1-RLOC))<<1 
koro_chan04_2_loop
  word (OCTAVE * 6) | (NOTE * 4)               | (INSTR * 5) |3,    (@koro_patt_c04_001_s2  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_002_s2  -(@B1-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 2)               | (INSTR * 5) |3,    (@koro_patt_c04_003_s2  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_004_s2  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_005_s2  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_006_s2  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_005_s2  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_006_s2  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_001_s2  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_002_s2  -(@B1-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 2)               | (INSTR * 5) |3,    (@koro_patt_c04_003_s2  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c04_004_s2  -(@B1-RLOC))<<1
koro_chan04_2_end
  word ((@koro_chan04_2_end-@koro_chan04_2_loop)<<4)|13

koro_chan05_0  
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 0) |3,    (@patt_init  -(@B1-RLOC))<<1         
  
  word                             DUMMY_NOTESET | (INSTR * 6) |3,  (@koro_patt_c05_001_s0  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c05_001_s0  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c05_001_s0  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c05_002_s0  -(@B1-RLOC))<<1  
koro_chan05_0_loop 
  word                           DUMMY_NOTESET | (INSTR * 6) |3,    (@koro_patt_c05_003_s0  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c05_004_s0  -(@B1-RLOC))<<1 
  word                                                              (@koro_patt_c05_003_s0  -(@B1-RLOC))<<1 
  word                                                              (@koro_patt_c05_004_s0  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c05_001_s0  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c05_002_s0  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c05_001_s0  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c05_002_s0  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c05_003_s0  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c05_003_s0  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c05_003_s0  -(@B1-RLOC))<<1
  word                                                              (@koro_patt_c05_004_s0  -(@B1-RLOC))<<1
koro_chan05_0_end
  word ((@koro_chan05_0_end-@koro_chan05_0_loop)<<4)|13

tempochan
  word ((((BPM*150)*32{56})>>15)&$FFF0)|1
  word 16|13
nullchan
  word 13

word $fffe ' end of pattseqs

DAT
'──────────────────────────────────────────────────────────────────────────────────────────
'                                  Pattern data
'──────────────────────────────────────────────────────────────────────────────────────────

patt_init
                        byte TRG_0           | 2
                        byte END

koro_patt_c00_empty_32
koro_patt_c04_empty_32
koro_patt_c05_empty_32
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte END

koro_patt_c01_001_part0
koro_patt_c01_001_part4
koro_patt_c01_002_part0
                        byte ((12    ) << 3) | 1
                        byte ((12 +12) << 3) | 1
                        byte END

koro_patt_c01_001_part1
koro_patt_c01_001_part3
                        byte ((12    ) << 3) | 1
                        byte ((12 +12) << 3) | 1
                        byte ((12 - 7) << 3) | 1
                        byte ((12 +12) << 3) | 1
                        byte END

koro_patt_c01_001_part2
koro_patt_c01_002_part2
                        byte ((12    ) << 3) | 1
                        byte ((12 +12) << 3) | 1
                        byte ((12 - 2) << 3) | 1
                        byte ((12 +12) << 3) | 1
                        byte END

koro_patt_c01_002_part1
                        byte ((12    ) << 3) | 1
                        byte ((12 +12) << 3) | 1
                        byte ((12 -11) << 3) | 1
                        byte ((12 +12) << 3) | 1
                        byte END

koro_patt_c01_002_part3
                        byte ((12    ) << 3) | 1
                        byte ((12 +12) << 3) | 1
                        byte ((12 - 7) << 3) | 1
                        byte ((12 +12) << 3) | 1
                        byte ((12 -10) << 3) | 1
                        byte ((12 + 1) << 3) | 1
                        byte END

koro_patt_c01_003_part0
koro_patt_c01_003_part1
koro_patt_c01_004_part3
                        byte ((12    ) << 3) | 1
                        byte ((12 -12) << 3) | 1
                        byte ((12 + 7) << 3) | 1
                        byte ((12 -12) << 3) | 1
                        byte END

koro_patt_c01_003_part2
koro_patt_c01_003_part3
                        byte ((12    ) << 3) | 1
                        byte ((12 -12) << 3) | 1
                        byte ((12 + 7) << 3) | 1
                        byte ((12 -10) << 3) | 1
                        byte END

koro_patt_c01_004_part0
koro_patt_c01_004_part2
                        byte ((12    ) << 3) | 1
                        byte ((12 -12) << 3) | 1
                        byte END

koro_patt_c01_004_part1
                        byte ((12    ) << 3) | 1
                        byte ((12 -12) << 3) | 1
                        byte ((12 +10) << 3) | 1
                        byte ((12 -12) << 3) | 1
                        byte END

koro_patt_c01_004_part4
                        byte ((12    ) << 3) | 1
                        byte ((12 -12) << 3) | 1
                        byte TRG_1{:noteoff} | 3
                        byte END

koro_patt_c01_005
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 - 5) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte END

koro_patt_c01_006
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12 - 5) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte ((12    ) << 3) | 1
                        byte TRG_1{:noteoff} | 3
                        byte END

koro_patt_c02_001to004
                        byte ((12    ) << 3) | 3
                        byte ((12 - 5) << 3) | 1
                        byte ((12 + 1) << 3) | 1
                        byte ((12 + 2) << 3) | 1
                        byte ((12 + 2) << 3) | 0
                        byte ((12 - 2) << 3) | 0
                        byte ((12 - 2) << 3) | 1
                        byte ((12 - 1) << 3) | 1
                        byte ((12 - 2) << 3) | 3
                        byte ((12    ) << 3) | 1
                        byte ((12 + 3) << 3) | 1
                        byte ((12 + 4) << 3) | 3
                        byte ((12 - 2) << 3) | 1
                        byte ((12 - 2) << 3) | 1
                        {byte END

koro_patt_c02_002}
                        byte ((12 - 1) << 3) | 5
                        byte ((12 + 1) << 3) | 1
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 - 4) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12    ) << 3) | 7
                        {byte END

koro_patt_c02_003}
                        byte TRG_1{:noteoff} | 1
                        byte ((12 + 5) << 3) | 3
                        byte ((12 + 3) << 3) | 1
                        byte ((12 + 4) << 3) | 3
                        byte ((12 - 2) << 3) | 1
                        byte ((12 - 2) << 3) | 1
                        byte ((12 - 1) << 3) | 5
                        byte ((12 - 4) << 3) | 1
                        byte ((12 + 4) << 3) | 3
                        byte ((12 - 2) << 3) | 1
                        byte ((12 - 2) << 3) | 1
                        {byte END

koro_patt_c02_004}
                        byte ((12 - 1) << 3) | 3
                        byte ((12    ) << 3) | 1
                        byte ((12 + 1) << 3) | 1
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 - 4) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte TRG_1{:noteoff} | 3
                        byte END

koro_patt_c02_005
                        byte ((12    ) << 3) | 7
                        byte ((12 - 4) << 3) | 7
                        byte ((12 + 2) << 3) | 7
                        byte ((12 - 3) << 3) | 7
                        byte END

koro_patt_c02_006
                        byte ((12    ) << 3) | 7
                        byte ((12 - 3) << 3) | 7
                        byte ((12 - 1) << 3) | 7
                        byte ((12 + 3) << 3) | 3
                        byte TRG_1{:noteoff} | 3
                        byte END

koro_patt_c02_007
                        byte ((12    ) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 + 5) << 3) | 7
                        byte ((12 - 1) << 3) | 7
                        byte ___             | 3
                        byte TRG_1{:noteoff} | 3
                        byte END

koro_patt_c03_001to004
                        byte ((12    ) << 3) | 3
                        byte ((12 - 3) << 3) | 1
                        byte ((12 + 1) << 3) | 1
                        byte ((12 + 2) << 3) | 3
                        byte ((12 - 2) << 3) | 1
                        byte ((12 - 1) << 3) | 1
                        byte ((12 - 4) << 3) | 3
                        byte ((12    ) << 3) | 1
                        byte ((12 + 5) << 3) | 1
                        byte ((12 + 3) << 3) | 3
                        byte ((12 - 1) << 3) | 1
                        byte ((12 - 2) << 3) | 1
                        {byte END

koro_patt_c03_002}
                        byte ((12 - 1) << 3) | 1
                        byte ((12 - 4) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 + 1) << 3) | 1
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12 - 5) << 3) | 3
                        byte ((12    ) << 3) | 7
                        {byte END

koro_patt_c03_003}
                        byte TRG_1{:noteoff} | 1
                        byte ((12 + 1) << 3) | 3
                        byte ((12 + 4) << 3) | 1
                        byte ((12 + 3) << 3) | 1
                        byte ((12    ) << 3) | 0
                        byte ((12    ) << 3) | 0
                        byte ((12 - 1) << 3) | 1
                        byte ((12 - 2) << 3) | 1
                        byte ((12 - 2) << 3) | 5
                        byte ((12 - 3) << 3) | 1
                        byte ((12 + 3) << 3) | 1
                        byte ((12 + 2) << 3) | 0
                        byte ((12 - 2) << 3) | 0
                        byte ((12 - 2) << 3) | 1
                        byte ((12 - 1) << 3) | 1
                        {byte END

koro_patt_c03_004}
                        byte ((12 + 4) << 3) | 1
                        byte ((12 - 4) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 + 1) << 3) | 1
                        byte ((12 + 2) << 3) | 1
                        byte ((12 - 3) << 3) | 1
                        byte ((12 + 4) << 3) | 1
                        byte ((12 - 4) << 3) | 1
                        byte ((12 + 1) << 3) | 3
                        byte ((12 - 5) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte TRG_1{:noteoff} | 3
                        byte END

koro_patt_c03_005
                        byte ((12    ) << 3) | 7
                        byte ((12 - 3) << 3) | 7
                        byte ((12 + 2) << 3) | 7
                        byte ((12 - 3) << 3) | 7
                        byte END

koro_patt_c03_006
                        byte ((12    ) << 3) | 7
                        byte ((12 - 5) << 3) | 7
                        byte ((12    ) << 3) | 7
                        byte ((12 + 4) << 3) | 3
                        byte TRG_1{:noteoff} | 3
                        byte END

koro_patt_c03_007
                        byte ((12    ) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 + 4) << 3) | 7
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte TRG_1{:noteoff} | 3
                        byte END

koro_patt_c04_001_s0
koro_patt_c04_002_s0
koro_patt_c04_001_s1
koro_patt_c04_002_s1
                        byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte ((12 + 1) << 3) | 7
                        byte ___             | 7
                        byte END

koro_patt_c04_003_s0
koro_patt_c04_003_s2
                        byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte ((12 + 2) << 3) | 7
                        byte ___             | 7
                        byte END

koro_patt_c04_004_s0
koro_patt_c04_004_s1
                        byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte ((12 + 1) << 3) | 7
                        byte ___             | 3
                        byte TRG_1{:noteoff} | 3
                        byte END

koro_patt_c04_005_s0
koro_patt_c04_005_s1
                        byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte ((12 - 1) << 3) | 7
                        byte ___             | 7
                        byte END

koro_patt_c04_006_s0
koro_patt_c04_006_s1
                        byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte ((12 - 1) << 3) | 7
                        byte ___             | 3
                        byte TRG_1{:noteoff} | 3
                        byte END

koro_patt_c04_003_s1
                        byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte ((12 + 3) << 3) | 7
                        byte ___             | 7
                        byte END

koro_patt_c04_001_s2
koro_patt_c04_002_s2
koro_patt_c04_005_s2
                        byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte END

koro_patt_c04_004_s2
koro_patt_c04_006_s2
                        byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte TRG_1{:noteoff} | 3
                        byte END

koro_patt_c05_001_s0
                        byte TRG_0{percussion 21} | 1
                        byte TRG_1{percussion 29} | 3
                        byte TRG_1{percussion 29} | 1
                        byte TRG_0{percussion 21} | 1
                        byte TRG_1{percussion 29} | 0
                        byte TRG_1{percussion 29} | 2
                        byte TRG_1{percussion 29} | 1
                        byte TRG_0{percussion 21} | 1
                        byte TRG_1{percussion 29} | 3
                        byte TRG_1{percussion 29} | 1
                        byte TRG_0{percussion 21} | 1
                        byte TRG_1{percussion 29} | 1
                        byte TRG_1{percussion 29} | 1
                        byte TRG_1{percussion 29} | 1
                        byte END

koro_patt_c05_002_s0
                        byte TRG_0{percussion 21} | 1
                        byte TRG_1{percussion 29} | 3
                        byte TRG_1{percussion 29} | 1
                        byte TRG_0{percussion 21} | 1
                        byte TRG_1{percussion 29} | 0
                        byte TRG_1{percussion 29} | 2
                        byte TRG_1{percussion 29} | 1
                        byte TRG_0{percussion 21} | 1
                        byte TRG_1{percussion 29} | 3
                        byte TRG_1{percussion 29} | 1
                        byte TRG_0{percussion 21} | 1
                        byte TRG_1{percussion 29} | 1
                        byte TRG_3{percussion 26} | 0
                        byte TRG_1{percussion 29} | 0
                        byte TRG_3{percussion 26} | 0
                        byte TRG_3{percussion 26} | 0
                        byte END

koro_patt_c05_003_s0
                        byte TRG_0{percussion 21} | 1
                        byte TRG_1{percussion 29} | 1
                        byte TRG_3{percussion 26} | 1
                        byte TRG_1{percussion 29} | 1
                        byte TRG_0{percussion 21} | 1
                        byte TRG_1{percussion 29} | 0
                        byte TRG_1{percussion 29} | 0
                        byte TRG_3{percussion 26} | 1
                        byte TRG_1{percussion 29} | 1
                        byte TRG_0{percussion 21} | 1
                        byte TRG_1{percussion 29} | 1
                        byte TRG_3{percussion 26} | 1
                        byte TRG_1{percussion 29} | 1
                        byte TRG_0{percussion 21} | 1
                        byte TRG_1{percussion 29} | 1
                        byte TRG_3{percussion 26} | 1
                        byte TRG_1{percussion 29} | 1
                        byte END

koro_patt_c05_004_s0         
                        byte TRG_0{percussion 21} | 1
                        byte TRG_1{percussion 29} | 1
                        byte TRG_3{percussion 26} | 1
                        byte TRG_1{percussion 29} | 1
                        byte TRG_0{percussion 21} | 1
                        byte TRG_1{percussion 29} | 0
                        byte TRG_1{percussion 29} | 0
                        byte TRG_3{percussion 26} | 1
                        byte TRG_1{percussion 29} | 1
                        byte TRG_0{percussion 21} | 1
                        byte TRG_1{percussion 29} | 1
                        byte TRG_3{percussion 26} | 1
                        byte TRG_1{percussion 29} | 1
                        byte TRG_0{percussion 21} | 1
                        byte TRG_1{percussion 29} | 1
                        byte TRG_3{percussion 26} | 0
                        byte TRG_1{percussion 29} | 0' volume 67
                        byte TRG_3{percussion 26} | 0
                        byte TRG_3{percussion 26} | 0
                        byte END                        

koro_patt_c05_001_s1
koro_patt_c05_002_s1
koro_patt_c05_003_s1
koro_patt_c05_004_s1
koro_patt_c05_001_s2
koro_patt_c05_002_s2
koro_patt_c05_003_s2
koro_patt_c05_004_s2
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte END
                        
DAT
BRADINSKY_DAT word  0
'──────────────────────────────────────────────────────────────────────────────────────────
B2 '' song base
SEQ_INIT2
              word @brad_chan03_0     -(@B2-RLOC)
              word @brad_chan01_0     -(@B2-RLOC)
              word @brad_chan02_1     -(@B2-RLOC)
              word @brad_chan02_0     -(@B2-RLOC)
              word @brad_chan00_0     -(@B2-RLOC)
              word @brad_chan01_1     -(@B2-RLOC)
              word @brad_chan04_0     -(@B2-RLOC)
              word @tempochan    -(@B2-RLOC)
'──────────────────────────────────────────────────────────────────────────────────────────
INS_INIT2
              word @instr_dummy  -(@B2-RLOC)
              word @instr_sqwbass  -(@B2-RLOC)
              word @instr_bradharmony_low  -(@B2-RLOC)
              word @instr_bradharmony_high  -(@B2-RLOC)
              word @instr_bradlead  -(@B2-RLOC)
              word @instr_drumkit  -(@B2-RLOC)

              word  END 

DAT
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
'                               Pattern sequency data
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────

brad_chan00_0      
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 0) |3,    (@patt_init  -(@B2-RLOC))<<1
brad_chan00_0_loop  
  word (OCTAVE * 7) | (NOTE * 4)               | (INSTR * 1) |3,    (@brad_patt_c00_001  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c00_001  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c00_002  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 4)               | (INSTR * 1) |3,    (@brad_patt_c00_001  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c00_003  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 4)               | (INSTR * 1) |3,    (@brad_patt_c00_004  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c00_003  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 4)               | (INSTR * 1) |3,    (@brad_patt_c00_004  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c00_001  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c00_001  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c00_002  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 4)               | (INSTR * 1) |3,    (@brad_patt_c00_001  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c00_005  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 4)               | (INSTR * 1) |3,    (@brad_patt_c00_006  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 4)               | (INSTR * 1) |3,    (@brad_patt_c00_007  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 4)               | (INSTR * 1) |3,    (@brad_patt_c00_008  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c00_009  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 3)               | (INSTR * 1) |3,    (@brad_patt_c00_010  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 4)               | (INSTR * 1) |3,    (@brad_patt_c00_011  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 5)               | (INSTR * 1) |3,    (@brad_patt_c00_012  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c00_013  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 5)               | (INSTR * 1) |3,    (@brad_patt_c00_014  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 4)               | (INSTR * 1) |3,    (@brad_patt_c00_015  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 4)               | (INSTR * 1) |3,    (@brad_patt_c00_016  -(@B2-RLOC))<<1
brad_chan00_0_end
  word ((@brad_chan00_0_end-@brad_chan00_0_loop)<<4)|13  


brad_chan01_0      
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 0) |3,    (@patt_init  -(@B2-RLOC))<<1
brad_chan01_0_loop  
  word (OCTAVE * 7) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_001_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_001_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_002_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_001_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_003_s0  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c01_004_s0_part0  -(@B2-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_004_s0_part1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_003_s0  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c01_004_s0_part0  -(@B2-RLOC))<<1
  word (OCTAVE * 8) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_004_s0_part1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_001_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_001_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_002_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_001_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 4)               | (INSTR * 2) |3,    (@brad_patt_c01_005to016_s0  -(@B2-RLOC))<<1{
  word (OCTAVE * 7) | (NOTE * 4)               | (INSTR * 2) |3,    (@brad_patt_c01_006_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 4)               | (INSTR * 2) |3,    (@brad_patt_c01_007_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 4)               | (INSTR * 2) |3,    (@brad_patt_c01_008_s0  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c01_009_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 3)               | (INSTR * 2) |3,    (@brad_patt_c01_010_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 2)               | (INSTR * 2) |3,    (@brad_patt_c01_011_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 9)               | (INSTR * 2) |3,    (@brad_patt_c01_012_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 3)               | (INSTR * 2) |3,    (@brad_patt_c01_013_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 9)               | (INSTR * 2) |3,    (@brad_patt_c01_014_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_015_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_016_s0  -(@B2-RLOC))<<1}
brad_chan01_0_end
  word ((@brad_chan01_0_end-@brad_chan01_0_loop)<<4)|13  

brad_chan01_1      
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 0) |3,    (@patt_init  -(@B2-RLOC))<<1
brad_chan01_1_loop  
  word (OCTAVE * 7) | (NOTE *11)               | (INSTR * 2) |3,    (@brad_patt_c01_001_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE *11)               | (INSTR * 2) |3,    (@brad_patt_c01_001_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE *11)               | (INSTR * 2) |3,    (@brad_patt_c01_002_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE *11)               | (INSTR * 2) |3,    (@brad_patt_c01_001_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE *11)               | (INSTR * 2) |3,    (@brad_patt_c01_003_s1  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c01_004_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE *11)               | (INSTR * 2) |3,    (@brad_patt_c01_003_s1  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c01_004_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE *11)               | (INSTR * 2) |3,    (@brad_patt_c01_001_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE *11)               | (INSTR * 2) |3,    (@brad_patt_c01_001_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE *11)               | (INSTR * 2) |3,    (@brad_patt_c01_002_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE *11)               | (INSTR * 2) |3,    (@brad_patt_c01_001_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_005to016_s1  -(@B2-RLOC))<<1{
  word (OCTAVE * 7) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_006_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_007_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_008_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 7)               | (INSTR * 2) |3,    (@brad_patt_c01_009_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 6)               | (INSTR * 2) |3,    (@brad_patt_c01_010_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE *11)               | (INSTR * 2) |3,    (@brad_patt_c01_011_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 2) |3,    (@brad_patt_c01_012_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 9)               | (INSTR * 2) |3,    (@brad_patt_c01_013_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 2) |3,    (@brad_patt_c01_014_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE *11)               | (INSTR * 2) |3,    (@brad_patt_c01_015_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 8) | (NOTE *11)               | (INSTR * 2) |3,    (@brad_patt_c01_016_s1  -(@B2-RLOC))<<1}
brad_chan01_1_end
  word ((@brad_chan01_1_end-@brad_chan01_1_loop)<<4)|13  

brad_chan02_0      
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 0) |3,    (@patt_init  -(@B2-RLOC))<<1
brad_chan02_0_loop  
  word (OCTAVE * 6) | (NOTE * 7)               | (INSTR * 3) |3,    (@brad_patt_c02_001_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 7)               | (INSTR * 3) |3,    (@brad_patt_c02_001_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 7)               | (INSTR * 3) |3,    (@brad_patt_c02_002_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 7)               | (INSTR * 3) |3,    (@brad_patt_c02_001_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 7)               | (INSTR * 3) |3,    (@brad_patt_c02_003_s0  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c02_004_s0_part0  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 8)               | (INSTR * 3) |3,    (@brad_patt_c02_004_s0_part1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 7)               | (INSTR * 3) |3,    (@brad_patt_c02_003_s0  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c02_004_s0_part0  -(@B2-RLOC))<<1
  word (OCTAVE * 7) | (NOTE * 8)               | (INSTR * 3) |3,    (@brad_patt_c02_004_s0_part1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 7)               | (INSTR * 3) |3,    (@brad_patt_c02_001_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 7)               | (INSTR * 3) |3,    (@brad_patt_c02_001_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 7)               | (INSTR * 3) |3,    (@brad_patt_c02_002_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 7)               | (INSTR * 3) |3,    (@brad_patt_c02_001_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 4)               | (INSTR * 3) |3,    (@brad_patt_c02_005to016_s0  -(@B2-RLOC))<<1{
  word (OCTAVE * 6) | (NOTE * 4)               | (INSTR * 3) |3,    (@brad_patt_c02_006_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 4)               | (INSTR * 3) |3,    (@brad_patt_c02_007_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 4)               | (INSTR * 3) |3,    (@brad_patt_c02_008_s0  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c02_009_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE * 3)               | (INSTR * 3) |3,    (@brad_patt_c02_010_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE * 2)               | (INSTR * 3) |3,    (@brad_patt_c02_011_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 9)               | (INSTR * 3) |3,    (@brad_patt_c02_012_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 3)               | (INSTR * 3) |3,    (@brad_patt_c02_013_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 9)               | (INSTR * 3) |3,    (@brad_patt_c02_014_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 7)               | (INSTR * 3) |3,    (@brad_patt_c02_015_s0  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 7)               | (INSTR * 3) |3,    (@brad_patt_c02_016_s0  -(@B2-RLOC))<<1}
brad_chan02_0_end
  word ((@brad_chan02_0_end-@brad_chan02_0_loop)<<4)|13  

brad_chan02_1      
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 0) |3,    (@patt_init  -(@B2-RLOC))<<1
brad_chan02_1_loop  
  word (OCTAVE * 6) | (NOTE *11)               | (INSTR * 3) |3,    (@brad_patt_c02_001_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE *11)               | (INSTR * 3) |3,    (@brad_patt_c02_001_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE *11)               | (INSTR * 3) |3,    (@brad_patt_c02_002_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE *11)               | (INSTR * 3) |3,    (@brad_patt_c02_001_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE *11)               | (INSTR * 3) |3,    (@brad_patt_c02_003_s1  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c02_004_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE *11)               | (INSTR * 3) |3,    (@brad_patt_c02_003_s1  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c02_004_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE *11)               | (INSTR * 3) |3,    (@brad_patt_c02_001_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE *11)               | (INSTR * 3) |3,    (@brad_patt_c02_001_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE *11)               | (INSTR * 3) |3,    (@brad_patt_c02_002_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE *11)               | (INSTR * 3) |3,    (@brad_patt_c02_001_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 7)               | (INSTR * 3) |3,    (@brad_patt_c02_005to016_s1  -(@B2-RLOC))<<1{
  word (OCTAVE * 6) | (NOTE * 7)               | (INSTR * 3) |3,    (@brad_patt_c02_006_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 7)               | (INSTR * 3) |3,    (@brad_patt_c02_007_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 7)               | (INSTR * 3) |3,    (@brad_patt_c02_008_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 7)               | (INSTR * 3) |3,    (@brad_patt_c02_009_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE * 6)               | (INSTR * 3) |3,    (@brad_patt_c02_010_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE *11)               | (INSTR * 3) |3,    (@brad_patt_c02_011_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE * 0)               | (INSTR * 3) |3,    (@brad_patt_c02_012_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 9)               | (INSTR * 3) |3,    (@brad_patt_c02_013_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE * 0)               | (INSTR * 3) |3,    (@brad_patt_c02_014_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE *11)               | (INSTR * 3) |3,    (@brad_patt_c02_015_s1  -(@B2-RLOC))<<1
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 3) |3,    (@brad_patt_c02_016_s1  -(@B2-RLOC))<<1 }
brad_chan02_1_end
  word ((@brad_chan02_1_end-@brad_chan02_1_loop)<<4)|13  

brad_chan03_0      
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 0) |3,    (@patt_init  -(@B2-RLOC))<<1
brad_chan03_0_loop  
  word (OCTAVE * 5) | (NOTE *11)               | (INSTR * 4) |3,    (@brad_patt_c03_001  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE *11)               | (INSTR * 4) |3,    (@brad_patt_c03_001  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE *11)               | (INSTR * 4) |3,    (@brad_patt_c03_002  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE *11)               | (INSTR * 4) |3,    (@brad_patt_c03_001  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE *11)               | (INSTR * 4) |3,    (@brad_patt_c03_003  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c03_004  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE *11)               | (INSTR * 4) |3,    (@brad_patt_c03_003  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c03_005  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE *11)               | (INSTR * 4) |3,    (@brad_patt_c03_001  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE *11)               | (INSTR * 4) |3,    (@brad_patt_c03_001  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE *11)               | (INSTR * 4) |3,    (@brad_patt_c03_002  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE *11)               | (INSTR * 4) |3,    (@brad_patt_c03_001  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE *11)               | (INSTR * 4) |3,    (@brad_patt_c03_006  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE * 4)               | (INSTR * 4) |3,    (@brad_patt_c03_007  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE *11)               | (INSTR * 4) |3,    (@brad_patt_c03_008  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE * 7)               | (INSTR * 4) |3,    (@brad_patt_c03_009  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE * 9)               | (INSTR * 4) |3,    (@brad_patt_c03_010  -(@B2-RLOC))<<1
  word (OCTAVE * 4) | (NOTE * 2)               | (INSTR * 4) |3,    (@brad_patt_c03_011  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE * 7)               | (INSTR * 4) |3,    (@brad_patt_c03_012  -(@B2-RLOC))<<1
  word (OCTAVE * 4) | (NOTE * 0)               | (INSTR * 4) |3,    (@brad_patt_c03_013  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE * 9)               | (INSTR * 4) |3,    (@brad_patt_c03_014  -(@B2-RLOC))<<1
  word (OCTAVE * 4) | (NOTE * 0)               | (INSTR * 4) |3,    (@brad_patt_c03_015  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE * 4)               | (INSTR * 4) |3,    (@brad_patt_c03_016  -(@B2-RLOC))<<1
  word (OCTAVE * 5) | (NOTE * 4)               | (INSTR * 4) |3,    (@brad_patt_c03_017  -(@B2-RLOC))<<1
brad_chan03_0_end
  word ((@brad_chan03_0_end-@brad_chan03_0_loop)<<4)|13  

brad_chan04_0      
  word (OCTAVE * 6) | (NOTE * 0)               | (INSTR * 0) |3,    (@patt_init  -(@B2-RLOC))<<1
brad_chan04_0_loop  
  word                             DUMMY_NOTESET | (INSTR * 5) |3,  (@brad_patt_c04_001  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_001  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_002  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_001  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_003  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_004  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_003  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_004  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_001  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_001  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_002  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_001  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_005  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_005  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_005  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_005  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_005  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_005  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_006  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_007  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_007  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_007  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_007  -(@B2-RLOC))<<1
  word                                                              (@brad_patt_c04_008  -(@B2-RLOC))<<1
brad_chan04_0_end
  word ((@brad_chan04_0_end-@brad_chan04_0_loop)<<4)|13

word $fffe ' end of pattseqs

DAT
'──────────────────────────────────────────────────────────────────────────────────────────
'                                  Pattern data
'──────────────────────────────────────────────────────────────────────────────────────────

brad_patt_c00_001
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 + 7) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 6) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 1) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 +10) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte END

brad_patt_c00_002
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 2) << 3) | 7
                        byte ___             | 3
                        byte ((12 + 7) << 3) | 7
                        byte ___             | 3
                        byte ((12 + 2) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 6) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 1) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte END

brad_patt_c00_003
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 + 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 +10) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte END

brad_patt_c00_004
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 + 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 +10) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte END

brad_patt_c00_005
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 1) << 3) | 7
                        byte ___             | 3
                        byte END

brad_patt_c00_006
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 1) << 3) | 7
                        byte ___             | 3
                        byte ((12 + 1) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 1) << 3) | 7
                        byte ___             | 3
                        byte END

brad_patt_c00_007
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 1) << 3) | 7
                        byte ___             | 3
                        byte END

brad_patt_c00_008
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte END

brad_patt_c00_009
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 1) << 3) | 7
                        byte ___             | 3
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 3
                        byte ((12 + 6) << 3) | 7
                        byte ___             | 3
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 3
                        byte ((12 + 3) << 3) | 7
                        byte ___             | 3
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 3
                        byte END

brad_patt_c00_010
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 3
                        byte ((12 + 6) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 - 2) << 3) | 7
                        byte ___             | 3
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 3
                        byte END

brad_patt_c00_011
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 + 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte END

brad_patt_c00_012
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 + 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 + 4) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 + 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte END

brad_patt_c00_013
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 + 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 +10) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 + 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte END

brad_patt_c00_014
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 + 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 + 4) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte END

brad_patt_c00_015
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12 +10) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte ((12 - 5) << 3) | 7
                        byte ___             | 3
                        byte END

brad_patt_c00_016
                        byte ((12    ) << 3) | 7
                        byte ___             | 3
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 3
                        byte ((12 - 1) << 3) | 7
                        byte ___             | 3
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 3
                        byte ((12 + 1) << 3) | 7
                        byte ___             | 3
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 3
                        byte ((12 -12) << 3) | 7
                        byte ___             | 3
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 3
                        byte END

brad_patt_c01_001_s0
brad_patt_c02_001_s0
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 6) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte ((12 + 4) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 -12) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte END

brad_patt_c01_002_s0
brad_patt_c02_002_s0
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 6) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte END

brad_patt_c01_003_s0
brad_patt_c02_003_s0
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 4) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 4) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte END

brad_patt_c01_004_s0_part0
brad_patt_c02_004_s0_part0
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 4) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte ((12 + 8) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte END

brad_patt_c01_004_s0_part1
brad_patt_c02_004_s0_part1
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte END

brad_patt_c01_005to016_s0
brad_patt_c02_005to016_s0
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 1) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        {byte END

brad_patt_c01_006_s0
brad_patt_c02_006_s0}
                        byte ((12 + 1) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 1) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 1) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 1) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        {byte END

brad_patt_c01_007_s0
brad_patt_c02_007_s0}
                        byte ((12 + 1) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 1) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        {byte END

brad_patt_c01_008_s0
brad_patt_c02_008_s0}
                        byte ((12 + 1) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        {byte END

brad_patt_c01_009_s0
brad_patt_c02_009_s0}
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 1) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 + 6) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 + 3) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        {byte END

brad_patt_c01_010_s0
brad_patt_c02_010_s0}
                        byte ((12 + 3) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 + 6) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 1) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 - 4) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        {byte END

brad_patt_c01_011_s0
brad_patt_c02_011_s0}
                        byte ((12 - 1) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ((12 - 7) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        {byte END

brad_patt_c01_012_s0
brad_patt_c02_012_s0}
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        {byte END

brad_patt_c01_013_s0
brad_patt_c02_013_s0}
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 4) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 4) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        {byte END

brad_patt_c01_014_s0
brad_patt_c02_014_s0}
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 4) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        {byte END

brad_patt_c01_015_s0
brad_patt_c02_015_s0}
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 4) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 4) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 4) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 4) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        {byte END

brad_patt_c01_016_s0
brad_patt_c02_016_s0}
                        byte ((12 + 4) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 -12) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte END

brad_patt_c01_001_s1
brad_patt_c02_001_s1 
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 1) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 3) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 3
                        byte ((12 -10) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte END

brad_patt_c01_002_s1
brad_patt_c02_002_s1
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 1) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 3) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte END

brad_patt_c01_003_s1
brad_patt_c02_003_s1
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte END

brad_patt_c01_004_s1
brad_patt_c02_004_s1
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 3
                        byte ((12 -10) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte END

brad_patt_c01_005to016_s1
brad_patt_c02_005to016_s1
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
{                        byte END

brad_patt_c01_006_s1
brad_patt_c02_006_s1}
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        {byte END

brad_patt_c01_007_s1
brad_patt_c02_007_s1   }
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        {byte END

brad_patt_c01_008_s1
brad_patt_c02_008_s1}
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        {byte END

brad_patt_c01_009_s1
brad_patt_c02_009_s1}
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 + 3) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 + 3) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        {byte END

brad_patt_c01_010_s1
brad_patt_c02_010_s1}
                        byte ((12 + 3) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 1
                        {byte END

brad_patt_c01_011_s1
brad_patt_c02_011_s1}
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 5
                        byte ((12 - 7) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        {byte END

brad_patt_c01_012_s1
brad_patt_c02_012_s1}
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 1) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 1) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
{                        byte END

brad_patt_c01_013_s1
brad_patt_c02_013_s1 }
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                       { byte END

brad_patt_c01_014_s1
brad_patt_c02_014_s1}
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 1) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 1) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        {byte END

brad_patt_c01_015_s1
brad_patt_c02_015_s1}
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        {byte END

brad_patt_c01_016_s1
brad_patt_c02_016_s1}
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ((12 -10) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte END

{brad_patt_c02_001_s0
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 6) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte ((12 + 4) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 -11) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte END

brad_patt_c02_016_s0
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 -11) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte END

brad_patt_c02_001_s1
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 1) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 3) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 3
                        byte ((12 - 9) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte END

brad_patt_c02_004_s1
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 3
                        byte ((12 - 9) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte END
}
brad_patt_c03_001
                        byte ((12    ) << 3) | 5
                        byte ((12    ) << 3) | 5
                        byte ((12 + 1) << 3) | 2
                        byte ((12 - 1) << 3) | 2
                        byte ((12 - 2) << 3) | 2
                        byte ((12 + 3) << 3) | 2
                        byte ((12 - 1) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12 + 2) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12    ) << 3) | 5
                        byte ((12 + 2) << 3) | 2
                        byte ((12 - 2) << 3) | 2
                        byte ((12 - 1) << 3) | 2
                        byte ((12 + 1) << 3) | 2
                        byte ((12 - 3) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 5) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte END

brad_patt_c03_002
                        byte ((12    ) << 3) | 5
                        byte ((12    ) << 3) | 5
                        byte ((12 + 1) << 3) | 2
                        byte ((12 - 1) << 3) | 2
                        byte ((12 - 2) << 3) | 2
                        byte ((12 + 3) << 3) | 2
                        byte ((12 - 3) << 3) | 5
                        byte ((12    ) << 3) | 5
                        byte ((12 + 2) << 3) | 2
                        byte ((12 - 2) << 3) | 2
                        byte ((12 - 2) << 3) | 2
                        byte ((12 + 4) << 3) | 2
                        byte ((12 - 4) << 3) | 5
                        byte ((12    ) << 3) | 5
                        byte ((12 + 2) << 3) | 2
                        byte ((12 - 2) << 3) | 2
                        byte ((12 - 1) << 3) | 2
                        byte ((12 + 3) << 3) | 2
                        byte ((12 - 3) << 3) | 5
                        byte ((12    ) << 3) | 5
                        byte ((12 + 1) << 3) | 5
                        byte ((12 + 2) << 3) | 5
                        byte END

brad_patt_c03_003
                        byte ((12    ) << 3) | 5
                        byte ((12    ) << 3) | 5
                        byte ((12 + 1) << 3) | 5
                        byte ((12 + 2) << 3) | 5
                        byte ((12 + 2) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12 - 1) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12    ) << 3) | 5
                        byte ((12 + 2) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12    ) << 3) | 5
                        byte ((12 + 2) << 3) | 5
                        byte ((12 + 2) << 3) | 5
                        byte END

brad_patt_c03_004
                        byte ((12    ) << 3) | 5
                        byte ((12    ) << 3) | 5
                        byte ((12 + 1) << 3) | 5
                        byte ((12 + 2) << 3) | 5
                        byte ((12 + 2) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12 - 1) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12    ) << 3) | 5
                        byte ((12 + 2) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 8) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte END

brad_patt_c03_005
                        byte ((12    ) << 3) | 5
                        byte ((12    ) << 3) | 5
                        byte ((12 + 1) << 3) | 5
                        byte ((12 + 2) << 3) | 5
                        byte ((12 + 2) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12 - 1) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12    ) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte ((12 - 1) << 3) | 5
                        byte ((12 - 2) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12 - 5) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte END

brad_patt_c03_006
                        byte ((12    ) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 - 1) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 5) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 1) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 1) << 3) | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 - 5) << 3) | 3
                        byte END

brad_patt_c03_007
                        byte ((12    ) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 + 7) << 3) | 3
                        byte ((12 - 1) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 1) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 1) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte END

brad_patt_c03_008
                        byte ((12    ) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 1) << 3) | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 - 5) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 - 1) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 - 4) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 1) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte END

brad_patt_c03_009
                        byte ((12    ) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 1) << 3) | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 - 5) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 + 5) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 1) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 1) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte END

brad_patt_c03_010
                        byte ((12    ) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 - 1) << 3) | 3
                        byte ((12 + 1) << 3) | 3
                        byte ((12 - 1) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 - 4) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 - 4) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 7) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 6) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 3) << 3) | 3
                        byte ((12 - 3) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte END

brad_patt_c03_011
                        byte ((12    ) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 2) << 3) | 3
                        byte ((12 - 2) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 6) << 3) | 3
                        byte ((12 - 4) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 4) << 3) | 3
                        byte ((12 - 4) << 3) | 3
                        byte ((12    ) << 3) | 3
                        byte ((12 + 2) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 - 7) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte END

brad_patt_c03_012
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 + 4) << 3) | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 - 4) << 3) | 5
                        byte ((12 - 3) << 3) | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte TRG_1{:noteoff} | 5
                        byte END

brad_patt_c03_013
                        byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 - 3) << 3) | 5
                        byte ((12 - 4) << 3) | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte TRG_1{:noteoff} | 5
                        byte ((12 + 6) << 3) | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 - 4) << 3) | 5
                        byte ((12 - 3) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte END

brad_patt_c03_014
                        byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 - 3) << 3) | 5
                        byte ((12 - 4) << 3) | 5
                        byte ((12 + 4) << 3) | 5
                        byte ((12 + 3) << 3) | 5
                        byte ((12 - 3) << 3) | 5
                        byte ((12 + 5) << 3) | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 - 4) << 3) | 5
                        byte ((12 - 3) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 5
                        byte END

brad_patt_c03_015
                        byte ((12    ) << 3) | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 - 3) << 3) | 5
                        byte ((12 - 4) << 3) | 5
                        byte ((12 + 4) << 3) | 5
                        byte ((12 + 3) << 3) | 5
                        byte ((12 - 3) << 3) | 5
                        byte ((12 + 2) << 3) | 5
                        byte ((12 - 4) << 3) | 5
                        byte ((12 - 3) << 3) | 5
                        byte ((12 + 3) << 3) | 5
                        byte ((12 - 5) << 3) | 5
                        byte ((12 + 4) << 3) | 5
                        byte ((12 + 3) << 3) | 5
                        byte ((12 - 3) << 3) | 5
                        byte END

brad_patt_c03_016
                        byte ((12    ) << 3) | 5
                        byte ((12 + 3) << 3) | 5
                        byte ((12 + 4) << 3) | 5
                        byte ((12 - 4) << 3) | 5
                        byte ((12 + 2) << 3) | 5
                        byte ((12 - 3) << 3) | 5
                        byte ((12 - 3) << 3) | 5
                        byte ((12 + 3) << 3) | 5
                        byte ((12 + 5) << 3) | 5
                        byte ((12 - 4) << 3) | 5
                        byte ((12 - 3) << 3) | 5
                        byte ((12 + 3) << 3) | 5
                        byte ((12 - 4) << 3) | 5
                        byte ((12 + 3) << 3) | 5
                        byte ((12 + 3) << 3) | 5
                        byte ((12 - 7) << 3) | 5
                        byte END

brad_patt_c03_017
                        byte ((12    ) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 + 7) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 + 5) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte ((12 -12) << 3) | 5
                        byte TRG_1{:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte END

brad_patt_c04_001
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_2{percussion 23} | 5
                        byte ___  {:noteoff} | 5
                        byte TRG_3{percussion 24} | 5
                        byte ___  {:noteoff} | 5
                        byte END

brad_patt_c04_002
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 2
                        byte TRG_2{percussion 23} | 2
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte END

brad_patt_c04_003
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 2
                        byte TRG_2{percussion 23} | 2
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 2
                        byte TRG_2{percussion 23} | 2
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 2
                        byte TRG_2{percussion 23} | 2
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 2
                        byte TRG_2{percussion 23} | 2
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 2
                        byte TRG_2{percussion 23} | 2
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 2
                        byte TRG_2{percussion 23} | 2
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 2
                        byte TRG_2{percussion 23} | 2
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 2
                        byte TRG_2{percussion 23} | 2
                        byte END

brad_patt_c04_004
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 2
                        byte TRG_2{percussion 23} | 2
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 2
                        byte TRG_2{percussion 23} | 2
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 2
                        byte TRG_2{percussion 23} | 2
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 2
                        byte TRG_2{percussion 23} | 2
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 2
                        byte TRG_2{percussion 23} | 2
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_2{percussion 23} | 5
                        byte ___  {:noteoff} | 5
                        byte TRG_3{percussion 24} | 5
                        byte ___  {:noteoff} | 5
                        byte END

brad_patt_c04_005
                        byte TRG_3{percussion 24} | 5
                        byte ___  {:noteoff} | 5
                        byte TRG_3{percussion 24} | 5
                        byte ___  {:noteoff} | 5
                        byte TRG_3{percussion 24} | 5
                        byte ___  {:noteoff} | 5
                        byte TRG_3{percussion 24} | 5
                        byte ___  {:noteoff} | 5
                        byte TRG_3{percussion 24} | 5
                        byte ___  {:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte TRG_3{percussion 24} | 5
                        byte ___   {:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte END

brad_patt_c04_006
                        byte TRG_2{percussion 23} | 5
                        byte ___  {:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte TRG_3{percussion 24} | 5
                        byte TRG_3{percussion 24} | 5
                        byte TRG_3{percussion 24} | 5
                        byte TRG_3{percussion 24} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte END

brad_patt_c04_007
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 2
                        byte TRG_2{percussion 23} | 2
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte TRG_0{percussion 19} | 5
                        byte TRG_2{percussion 23} | 5
                        byte END

brad_patt_c04_008
                        byte TRG_0{percussion 19} | 5
                        byte ___  {:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte TRG_0{percussion 19} | 5
                        byte ___  {:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 1
                        byte TRG_2{percussion 23} | 5
                        byte ___  {:noteoff} | 7
                        byte ___             | 7
                        byte ___             | 7
                        byte ___             | 5
                        byte TRG_3{percussion 24} | 5
                        byte ___  {:noteoff} | 5
                        byte END


DAT
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
'                                 Instrument data
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
org

instr_dummy
    long    SET|VOLUME|(wait_ms*-1),        $0000_0000     
    long    JUMP,                           -1*STEPS

' Simple BASS
    long    SET|VOLUME|(wait_ms*-1),            $00000000 ' TRG1: el cheapo noteoff
instr_sawbass                                                
    long    SET|MODULATION,                     $0000_0000|$100  
    long    SET|ENVELOPE,                       $0000_0000|$000
    long    SET|VOLUME,                         $F000_0000  
    long    SET|ENVELOPE_NOW,                   $FF00_0000 
    long    JUMP,                               -1*STEPS

' CHORUS BLEEP
    long    SET|ENVELOPE|(wait_ms*-1),         -$0006_0000|$001 ' TRG1: el cheapo noteoff 
instr_korochorus
'instr_c03_00
    long    SET|ENVELOPE|(wait_ms*2),          -$00A2_0000|$008                                                                                   
    long    SET|MODULATION,                     $0000_0000|$080  
    long    SET|ENVELOPE,                       $004E_0000|$008
    long    SET|VOLUME|(wait_ms*30),            $DB00_0000 
    long    SET|ENVELOPE|(wait_ms*17),         -$000A_2200|$008  
    long    SET|ENVELOPE,                       $0000_0000|$008 
    long    JUMP,                               -1*STEPS

' BRIDGE BLEEP LOW
    long    SET|VOLUME|(wait_ms*-1),            $00000000 ' TRG1: el cheapo noteoff
instr_korobridge_low                                                
    long    SET|MODULATION,                     $0000_0000|$100  
    long    SET|ENVELOPE,                       $0000_0000|$000
    long    SET|VOLUME,                         $B000_0000  
    long    SET|ENVELOPE_NOW,                   $FF00_0000 
    long    JUMP,                               -1*STEPS

' BRIDGE BLEEP HIGH
    long    SET|VOLUME|(wait_ms*-1),            $00000000 ' TRG1: el cheapo noteoff
instr_korobridge_high                                                
    long    SET|MODULATION,                     $0000_0000|$100  
    long    SET|ENVELOPE,                       $0000_0000|$000
    long    SET|VOLUME,                         $B000_0000  
    long    SET|ENVELOPE_NOW,                   $FF00_0000 
    long    JUMP,                               -1*STEPS

' Harmony
    long    SET|ENVELOPE|(wait_ms*-1),         -$0009_0000|$001 ' TRG1: noteoff           
instr_koroharmony                                                
    long    SET|MODULATION,                     $0000_0000|$0A0
    long    SET|MODULATION,                     $0000_0800|$000
    long    SET|ENVELOPE,                       $0000_0000|$000
    long    SET|VOLUME,                         $7C00_0000  
    long    SET|ENVELOPE_NOW,                   $FF00_0000 
    long    JUMP,                               -1*STEPS

' SPINTRIS DRUM KIT
TRI_PREC_trg3    long    JUMP,     @TRI_PREC_CLAP-@TRI_PREC_trg3 -STEPS ''TRG_3 (Clap)
TRI_PREC_trg2    long    JUMP,     @TRI_PREC_SNARE-@TRI_PREC_trg2 -STEPS ''TRG_2 (Snare)  
TRI_PERC_trg1    long    JUMP,     @TRI_PREC_SHAKE-@TRI_PERC_trg1 -STEPS ''TRG_1 (Shaker/Hihat)
instr_drumkit
TRI_PREC_trg0    ''TRG_0 (Kick)

                                                     
TRI_PREC_KICK
    ''Kick/Base
    long    SET|MODULATION,                 $FFFFFFFF
    long    SET|VOLUME,                     $E4000000
    long    SET|FREQUENCY,                  $F96F96F9
    long    SET|ENVELOPE,                   $00F80A00|$080
    long    MODIFY|FREQUENCY|(5 *wait_ms),  $B2CF9020
    long    SET|FREQUENCY,                  $009A02A0
    long    SET|VOLUME,                     $FF000000
    long    SET|ENVELOPE,                   $FFFCA200|$007
    long    MODIFY|FREQUENCY|(100 *wait_ms), $FFFFD900
    'long    SET|VOLUME,                     $00000000
    long    JUMP,                           -1 *STEPS
TRI_PREC_SHAKE
    ''Shakery something i don't know
    long    SET|MODULATION,                 $FFFFFFFF
    long    SET|VOLUME,                     $E4000000
    long    SET|ENVELOPE,                   $FFD7F400|$08
    long    SET|ENVELOPE_NOW,               $FFFFFFFF
    long    SET|FREQUENCY,                  $04444444
    long    MODIFY|FREQUENCY|(90 *wait_ms), $B2CF9020
    long    SET|ENVELOPE,                   $FF57F400|$007
    long    MODIFY|FREQUENCY|(10 *wait_ms), $83CF9020
    long    SET|VOLUME,                     $00000000
    long    JUMP,                           -1 *STEPS
TRI_PREC_CLAP
    '' clap
    long    SET|VOLUME|(2 *wait_ms),        $00000000    
    long    SET|PHASE,                      $FFCCCCCC  
    long    SET|MODULATION,                 $FFFFFFFF
    long    SET|VOLUME,                     $FF000000
    long    SET|FREQUENCY,                  $024C22E4'$04444444'$01F81DE8'600*Hz
    long    SET|ENVELOPE,                   $18000000|$100
    long    SET|ENVELOPE_NOW,               $18000000  
    long    MODIFY|FREQUENCY|(30 *wait_ms), $04165020
    long    SET|ENVELOPE,                   $008F0A00|$1F0             
    'long    SET|VOLUME,                     $98000000
    long    SET|ENVELOPE,                   (-(ms/100) &ILSB9) | 1           
    long    MODIFY|FREQUENCY|(30 *wait_ms), $A2CF9020
    long    MODIFY|FREQUENCY|(70 *wait_ms), $A2CF9020
    long    SET|VOLUME,                     $00000000
    long    JUMP,                           -1 *STEPS
TRI_PREC_SNARE
    '' snare
    long    SET|VOLUME|(1 *wait_ms),        $00000000   
    long    SET|PHASE,                      $70000000
    long    SET|MODULATION,                 $CCCCCCCC
    long    SET|VOLUME,                     $E4000000
    long    SET|FREQUENCY,                  $024C22E4'$04444444'$01F81DE8'600*Hz
    long    SET|ENVELOPE,                   $00F80A00|$008   
    long    MODIFY|FREQUENCY|(3 *wait_ms),  $B2CF9020
    long    SET|FREQUENCY,                  $025A31D8'1000 *Hz
    long    MODIFY|FREQUENCY|(1 *wait_ms),  -1_363_000'$FFEB33C8 '- ($035A31D8/(3*8))
    long    MODIFY|FREQUENCY|(5 *wait_ms),  -168_000
    long    MODIFY|FREQUENCY|(7 *wait_ms),  -7_000         
    long    SET|VOLUME,                     $90000000
    long    SET|ENVELOPE,                   (-(ms/150) &ILSB9) | 1           
    long    MODIFY|FREQUENCY|(30 *wait_ms), $83CF9020
    long    MODIFY|FREQUENCY|(170 *wait_ms), $B2CF9020
    long    SET|VOLUME,                     $00000000
    long    JUMP,                           -1 *STEPS


' Bass BLEEP   
    long    SET|VOLUME|(wait_ms*-1),            $0000_0000 ' TRG1: noteoff           
instr_sqwbass                                                
    long    SET|MODULATION,                     $0000_0000|$100
    'long   SET|MODULATION,                     $0000_0800|$000
    long    SET|ENVELOPE,                      -$0000_A000|$000
    long    SET|VOLUME,                         $8000_0000  
    long    SET|ENVELOPE_NOW,                   $FF00_0000 
    long    JUMP,                               -1*STEPS

' Harmony low
    long    SET|ENVELOPE|(wait_ms*-1),         -$0009_0000|$001 ' TRG1: noteoff           
instr_bradharmony_low                                                
    long    SET|MODULATION,                     $0000_0000|$0A0
    long    SET|MODULATION,                     $0000_0800|$000
    long    SET|ENVELOPE,                       $0000_0000|$000
    long    SET|VOLUME,                         $8C00_0000  
    long    SET|ENVELOPE_NOW,                   $FF00_0000 
    long    JUMP,                               -1*STEPS
    
' Harmony high
    long    SET|ENVELOPE|(wait_ms*-1),         -$0009_0000|$001 ' TRG1: noteoff           
instr_bradharmony_high                                                
    long    SET|MODULATION,                     $0000_0000|$100
    'long   SET|MODULATION,                     $0000_0800|$000
    long    SET|ENVELOPE,                       $0000_0000|$000
    long    SET|VOLUME,                         $7C00_0000  
    long    SET|ENVELOPE_NOW,                   $FF00_0000 
    long    JUMP,                               -1*STEPS

' Melody BLEEP
    long    SET|ENVELOPE|(wait_ms*-1),         -$0006_0000|$001 ' TRG1: el cheapo noteoff
instr_bradlead
    long    SET|ENVELOPE|(wait_ms*2),          -$00A2_0000|$008                                                                              
    long    SET|MODULATION,                     $0000_0000|$100                                                                                   
    'long    SET|MODULATION,                     $0000_1400|$000  
    long    SET|ENVELOPE,                       $0030_0000|$008
    long    SET|VOLUME|(wait_ms*12),            $FF00_0000                                                                                        
    'long    SET|MODULATION,                     $0000_F000|$000 
    long    SET|ENVELOPE|(wait_ms*17),         -$000A_2200|$008  
    long    SET|ENVELOPE,                      -$0000_C200|$008 
    long    JUMP,                               -1*STEPS 

DAT
'byte "-RETROSNDFOOT-"

CON
    END         = $0
    OCTAVE      = 1<<12
    NOTE        = 1<<8
    INSTR       = 1<<3 
    ___         = 25 << 3      ' Void note
    TRG_0       = 12 << 3
    TRG_1       = 26 << 3
    TRG_2       = 27 << 3  
    TRG_3       = 28 << 3  
    TRG_4       = 29 << 3  
    TRG_5       = 30 << 3  
    TRG_6       = 31 << 3  
    ms          = 67108864
    Hz          = ms / 1000
    seconds     = Hz   
    repeats     = $10 ' Not actually a single repeat, but 8(?) repeats
    delta       = 12
    wait_ms     = repeats ' Not actually a millisecond
    STEPS       = 8
    VOL         = 1 << 24
    BPM         = 366_048 / 16 ' (2^32 / 64_000 / 11) * 16

    DUMMY_NOTESET = (OCTAVE *6) | (NOTE *9) ' A4
    
    ' Instructions
    JUMP        = MODIFY|PROGRAM_CNT
    SET         = %0000
    MODIFY      = %1000
    WAIT        = MODIFY
    
    ' Registers
    FREQUENCY   = $0
    PHASE       = $1
    MODULATION  = $2
    MODPHASE    = $3
    ENVELOPE    = $4
    VOLUME      = $5
    ENVELOPE_NOW= $6
    PROGRAM_CNT = $7
        

    ' Masks
    LSB9         = %00000000_00000000_00000001_11111111
    ILSB9        = %11111111_11111111_11111110_00000000

    ' Gunk
    TODO_INSTR1 = JUMP
    TODO_INSTR2 = -1 * STEPS
    TODO = ___
    TODO_CHANNEL = 0

    RLOC = 0 ' TODO: relocate