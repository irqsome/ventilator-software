'' Spectrum-Graphics routines for Manic Miner
'' Setup Originally modified Graphics Driver
'' ──────────────────────────────────────────────────────────────────────────────────
''

CON

  #1, _setup,_loop,_draw16x16,_put16x16,_get16x16,_or16x16,_or16x16_noattr,_scrolltext

VAR

  long  cog
  long  command
  long  bitmap_base                                     'bitmap data


PUB start : okay

'' Start graphics driver - starts a cog
'' returns false if no cog available

'  fontptr := @font                                      'set font pointer (same for all instances)

  stop
  okay := cog := cognew(@loop, @command) + 1


PUB stop

'' Stop graphics driver - frees a cog

  if cog
    cogstop(cog~ - 1)

  command~


PUB setup(scrptr)

  setcommand(_loop, 0)                                  'make sure last command finished

  setcommand(_setup, @scrptr)

  bitmap_base := scrptr                               'retain high-level bitmap data


PUB GfxDraw16x16(data, x, y, attr)
  Setcommand(_loop, 0)
  Setcommand(_draw16x16, @data)  

PUB GfxPut16x16(data, x, y, attr)
  Setcommand(_loop, 0)
  Setcommand(_put16x16, @data)  

PUB GfxGet16x16(data, x, y, attr)
  Setcommand(_loop, 0)
  Setcommand(_get16x16, @data)  

PUB GfxOr16x16(data, x, y, attr)
  Setcommand(_loop, 0)
  Setcommand(_or16x16, @data)  

PUB GfxOr16x16noattr(data, x, y, attr)
  Setcommand(_loop, 0)
  Setcommand(_or16x16_noattr, @data)  

PUB GfxScroll(y)
  Setcommand(_loop, 0)
  Setcommand(_scrolltext, @y)  

PRI setcommand(cmd, argptr)

  command := cmd << 16 + argptr                         'write command and pointer
  repeat while command                                  'wait for command to be cleared, signifying receipt


DAT

'*************************************
'* Assembly language graphics driver *
'*************************************

                        org
'
'
' Graphics driver - main loop
'
loop                    rdlong  t1,par          wz      'wait for command
        if_z            jmp     #loop

                        movd    :arg,#arg0              'get 8 arguments
                        mov     t2,t1
                        mov     t3,#8
:arg                    rdlong  arg0,t2
                        add     :arg,d0
                        add     t2,#4
                        djnz    t3,#:arg

                        wrlong  zero,par                'zero command to signify received

                        ror     t1,#16+2                'lookup command address
                        add     t1,#jumps
                        movs    :table,t1
                        rol     t1,#2
                        shl     t1,#3
:table                  mov     t2,0
                        shr     t2,t1
                        and     t2,#$FF
                        jmp     t2                      'jump to command


jumps                   byte    0                       '0
                        byte    setup_                  '1
                        byte    loop                    '2
                        byte    draw16x16_              '3
                        byte    put16x16_               '4
                        byte    get16x16_               '5
                        byte    or16x16_                '6
                        byte    or16x16_noattr          '7
                        byte    scroll_                 '8

scroll_                 mov     t1,arg0
                        mov     t2,#255
:lp                     add     t1,#1
                        rdbyte  t3,t1
                        sub     t1,#1
                        nop
                        wrbyte  t3,t1
                        add     t1,#1
                        djnz    t2,#:lp

                        jmp     #loop                          
                                                
draw16x16_              mov     t1,arg2
                        shl     t1,#5           'y*32
                        add     t1,myscrbase
                        add     t1,arg1
                        mov     t3,#16
:dxlp                   rdword  t2,arg0
                        add     arg0,#2
                        wrbyte  t2,t1
                        ror     t2,#8
                        add     t1,#1
                        wrbyte  t2,t1
                        add     t1,#31
                        djnz    t3,#:dxlp

                        mov     t1,arg2
                        shr     t1,#3
                        shl     t1,#5
                        add     t1,myatrbase
                        add     t1,arg1

                        wrbyte  arg3,t1
                        add     t1,#1
                        nop
                        wrbyte  arg3,t1
                        add     t1,#31
                        nop
                        wrbyte  arg3,t1
                        add     t1,#1
                        nop
                        wrbyte  arg3,t1

                        jmp     #loop

put16x16_               mov     t1,arg2
                        shl     t1,#5           'y*32
                        add     t1,myscrbase
                        add     t1,arg1
                        mov     t3,#16
:dxlp                   rdword  t2,arg0
                        add     arg0,#2
                        wrbyte  t2,t1
                        ror     t2,#8
                        add     t1,#1
                        wrbyte  t2,t1
                        add     t1,#31
                        djnz    t3,#:dxlp

                        jmp     #loop

get16x16_               mov     t1,arg2
                        shl     t1,#5           'y*32
                        add     t1,myscrbase
                        add     t1,arg1
                        mov     t3,#16
:dxlp                   rdbyte  t2,t1
                        add     t1,#1
                        nop
                        rdbyte  t4,t1
                        rol     t4,#8
                        or      t4,t2
                        wrword  t4,arg0
                        add     arg0, #2
                        add     t1,#31
                        djnz    t3,#:dxlp

                        jmp     #loop

or16x16_noattr          mov     t1,arg2
                        shl     t1,#5           'y*32
                        add     t1,myscrbase
                        add     t1,arg1
                        mov     t3,#16
:dxlp                   rdword  t2,arg0
                        add     arg0,#2
                        nop
                        rdbyte  t4,t1
                        or      t4,t2
                        nop                        
                        wrbyte  t4,t1
                        ror     t2,#8
                        add     t1,#1
                        rdbyte  t4,t1
                        or      t4,t2
                        nop
                        wrbyte  t4,t1
                        add     t1,#31
                        djnz    t3,#:dxlp
                        
                        jmp     #loop

or16x16_                mov     t1,arg2
                        shl     t1,#5           'y*32
                        add     t1,myscrbase
                        add     t1,arg1
                        mov     t3,#16
:dxlp                   rdword  t2,arg0
                        add     arg0,#2
                        nop
                        rdbyte  t4,t1
                        or      t4,t2
                        nop                        
                        wrbyte  t4,t1
                        ror     t2,#8
                        add     t1,#1
                        rdbyte  t4,t1
                        or      t4,t2
                        nop
                        wrbyte  t4,t1
                        add     t1,#31
                        djnz    t3,#:dxlp

                        mov     t1,arg2
                        shr     t1,#3
                        shl     t1,#5
                        add     t1,myatrbase
                        add     t1,arg1

                        and     arg3,#7         'remove paper and bright and flash and just leave sprites ink colour

                        rdbyte  t2,t1           'read attribute
                        and     t2,#$f8         'remove ink colour
                        or      t2,arg3         'add sprite ink colour
                        wrbyte  t2,t1           'put back in attrs
                        add     t1,#1           'goto top right attribute in 16x16
                        nop                     'pad 2 cycles per hum ram access
                        
                        rdbyte  t2,t1           'read attribute
                        and     t2,#$f8         'remove ink colour
                        or      t2,arg3         'add sprite ink colour
                        wrbyte  t2,t1           'put back in attrs
                        add     t1,#31          'goto bottom left attribute in 16x16
                        nop                     'pad 2 cycles per hum ram access
                        
                        rdbyte  t2,t1           'read attribute
                        and     t2,#$f8         'remove ink colour
                        or      t2,arg3         'add sprite ink colour
                        wrbyte  t2,t1           'put back in attrs
                        add     t1,#1           'goto bottom right attribute in 16x16
                        nop                     'pad 2 cycles per hum ram access
                        
                        rdbyte  t2,t1           'read attribute
                        and     t2,#$f8         'remove ink colour
                        or      t2,arg3         'add sprite ink colour
                        wrbyte  t2,t1           'put back in attrs

                        and     arg2,#7 wz
              if_z      jmp     #loop 

                        add     t1,#31          'goto bottom left attribute in 16x24

                        rdbyte  t2,t1           'read attribute
                        and     t2,#$f8         'remove ink colour
                        or      t2,arg3         'add sprite ink colour
                        wrbyte  t2,t1           'put back in attrs
                        add     t1,#1           'goto bottom right attribute in 16x24
                        nop                     'pad 2 cycles per hum ram access
                        
                        rdbyte  t2,t1           'read attribute
                        and     t2,#$f8         'remove ink colour
                        or      t2,arg3         'add sprite ink colour
                        wrbyte  t2,t1           'put back in attrs

                        jmp     #loop           'wait for next command

'
' setup(scrptr)
'
setup_                  mov     myscrbase,arg0
                        mov     myatrbase,arg0
                        add     myatrbase,attroffset
                        jmp     #loop
'
'
' Multiply
'
'   in:         t1 = 16-bit multiplicand (t1[31..16] must be 0)
'               t2 = 16-bit multiplier
'
'   out:        t1 = 32-bit product
'
multiply                mov     t3,#16
                        shl     t2,#16
                        shr     t1,#1           wc

:loop   if_c            add     t1,t2           wc
                        rcr     t1,#1           wc
                        djnz    t3,#:loop

multiply_ret            ret
'
'
' Defined data
'
zero                    long    0                       'constants
d0                      long    $200
attroffset              long    6144

'
'
' Undefined data
'
t1                      res     1       'temps
t2                      res     1
t3                      res     1
t4                      res     1
t5                      res     1
t6                      res     1
t7                      res     1

arg0                    res     1       'arguments passed from high-level
arg1                    res     1
arg2                    res     1
arg3                    res     1
arg4                    res     1
arg5                    res     1
arg6                    res     1
arg7                    res     1

myscrbase               res     1
myatrbase               res     1