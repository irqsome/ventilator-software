  ' Manic Miner - ported to Propeller Chip By Jim Bagley 
'

CON

'uncomment the '{ block which suites your system.
{ Hydra Defines
        _CLKMODE                = xtal1 + pll8x
        _XINFREQ                = 10_000_000 + 0000

USE_SDCARD_INSTEAD_OF_HIEEPROM = 0      ' 0 for level files taken from HIEEPROM or 1 for level files taken from SD Card root
USE_SOUND_BITS_FOR_STEREO      = 1      ' usually 3 for Proto/Demo boards and 1 for Hydra ... 1 for mono and 3 for stereo 
USE_SOUND_PINS                 = 7      ' usually 10 for Proto/Demo boards and 7 for Hydra
USE_HYDRA_KEYBAORD             = 1      ' usually 0 for Proto/Demo boards ( key.start(26,27) ) and 1 for Hydra ( keyhydra.start(3) )
USE_TV_ON_PIN_24               = 1      ' usually 0 for Proto/Demo boards ( JBSpecGfx.Start(12) ) and 1 for Hydra ( JBSpecGfx.Start(24) ) 
'}
{ Proto/Demoboard Defines - No SD card installed
        _CLKMODE                = xtal1 + pll16x
        _XINFREQ                = 5_000_000 + 0000

USE_SDCARD_INSTEAD_OF_HIEEPROM = 0      ' 0 for level files taken from HIEEPROM or 1 for level files taken from SD Card root
USE_SOUND_BITS_FOR_STEREO      = 3      ' usually 3 for Proto/Demo boards and 1 for Hydra ... 1 for mono and 3 for stereo 
USE_SOUND_PINS                 = 10     ' usually 10 for Proto/Demo boards and 7 for Hydra
USE_HYDRA_KEYBAORD             = 0      ' usually 0 for Proto/Demo boards ( key.start(26,27) ) and 1 for Hydra ( keyhydra.start(3) ) 
USE_TV_ON_PIN_24               = 0      ' usually 0 for Proto/Demo boards ( JBSpecGfx.Start(12) ) and 1 for Hydra ( JBSpecGfx.Start(24) ) 
'}
'{ Proto/Demoboard Defines - SD card installed on pin 0-3
        _CLKMODE                = xtal1 + pll16x
        _XINFREQ                = 5_000_000 + 0000

USE_SDCARD_INSTEAD_OF_HIEEPROM = 1      ' 0 for level files taken from HIEEPROM or 1 for level files taken from SD Card root
USE_SOUND_BITS_FOR_STEREO      = 3      ' usually 3 for Proto/Demo boards and 1 for Hydra ... 1 for mono and 3 for stereo 
USE_SOUND_PINS                 = 10     ' usually 10 for Proto/Demo boards and 7 for Hydra
USE_HYDRA_KEYBAORD             = 0      ' usually 0 for Proto/Demo boards ( key.start(26,27) ) and 1 for Hydra ( keyhydra.start(3) ) 
USE_TV_ON_PIN_24               = 0      ' usually 0 for Proto/Demo boards ( JBSpecGfx.Start(12) ) and 1 for Hydra ( JBSpecGfx.Start(24) )
  spiDO     = 21
  spiClk    = 24
  spiDI     = 20
  spiCS     = 25
  JOY_CLK = 16
  JOY_LCH = 17
  JOY_DATAOUT0 = 19
  JOY_DATAOUT1 = 18 
'}

OBJ
  Spectrum      : "JB_002_JLC_Spectrum_TV_010"
  JBSpecGfx     : "JB_Spectrum_Graphics_001"
  key           : "Keyboard"                          
  'keyhydra      : "keyboard_iso_010.spin"  ' instantiate a keyboard object       
  'i2c           : "Basic_I2C_Driver"
  'sdfat         : "fsrw"
  kyefat        : "SD-MMC_FATEngine"

VAR
  long  VSync

  long  eepromAddress
  
  long  joypadbits
  
  long  level_ptr
  long  ptr
  long  level_num
  long  txtx,txty,txtcol  

  long  gameframe

  long  livesframe
  long  willyframe
  long  willyx
  long  willyy
  long  willydir
  long  willyydir
  long  willyjumpcnt
  long  willyjumping
  long  willyjumpdir
  long  willylastdir
  long  willylives
  long  willyjumporigin

  long  gameloopexittype
  long  levelbits
  long  airleft
  long  airbits

  long  booty

  long  conveyorPtr
  long  conveyorLen
  long  conveyorX
  long  conveyorY

  long  pickups[5]
  byte  pickupbuf[8] 'only need 5 but use 8 for padding

  long  HoriBad[4*badsize]
  long  VertBad[4*badsize]

  long  sprites[9*8]
  byte  spritebuffer[9*36]

  byte  Screen[6144]
  byte  Attr[768]

  byte  level_buf[1024]

  byte  swordfish_sprite[32]
  byte  swordfish_attr[4]
  byte  plinth_sprite[32]
  byte  plinth_attr[4]
  byte  boot_sprite[32]
  byte  boot_attr[4]
  byte  eugene_sprite[32]
  byte  eugene_attr[4]

  long  title_tune_pos
  long  title_tune_delay
  long  title_tune_note1
  long  title_tune_note2
  long  muscog
  long  musicon

  long  start_scrolling
  long  demo
  
CON
  badattr       = 0
  badx          = 1
  bady          = 2
  baddir        = 3
  badmin        = 4
  badmax        = 5
  badspd        = 6
  badrealx      = 7
  badsize       = 8

'gameloopexittypes
  LEVEL_DONE    = 1
  LIFE_LOST     = 2
  BACK_TO_MENU  = 3

 ' NES bit encodings for NES gamepad 0
  NES_RIGHT  = %00000000_00000001
  NES_LEFT   = %00000000_00000010
  NES_DOWN   = %00000000_00000100
  NES_UP     = %00000000_00001000
  NES_START  = %00000000_00010000
  NES_SELECT = %00000000_00100000
  NES_B      = %00000000_01000000
  NES_A      = %00000000_10000000

  ' control keys
  KB_LEFT_ARROW  = $C0
  KB_RIGHT_ARROW = $C1
  KB_UP_ARROW    = $C2
  KB_DOWN_ARROW  = $C3
  KB_ESC         = $CB
  KB_SPACE       = $20
  KB_ENTER       = $0D

PUB go | x2
   x2 := \start
   print(@sderrortext,0,192-8,%11010110)

PUB Start | x,y,i,j,k

  LongFill(@Screen,0,6144/4)
  LongFill(@Attr,$47474747,768/4)

  Spectrum.Start(@Screen, 0, @VSync, USE_TV_ON_PIN_24)
  
  JBSpecGfx.Start
  JBSpecGfx.Setup(@Screen)

  {ifnot USE_HYDRA_KEYBAORD 
    key.Start(26,27)
  else
    keyhydra.Start(3)}
  key.Start(8,9)  

'start eeprom driver
  'i2c.Initialize(28)

  {if USE_SDCARD_INSTEAD_OF_HIEEPROM
    sdfat.mount(0)}
  kyefat.FATEngineStart(spiDO,spiClk,spiDI,spiCS,-1,-1,-1,-1,-1)
  kyefat.mountPartition(0)
  kyefat.changeDirectory(string("MINER"))
  

  repeat y from 0 to 23
    repeat x from 0 to 31
      Attr[(y<<5) + x] := %01000111

  txtx:=0
  txty:=0
  txtcol:=%01000111

  {ifnot USE_SDCARD_INSTEAD_OF_HIEEPROM
    eepromAddress:=$8000+(0*1024)+736
    read32(@swordfish_sprite[0])
    eepromAddress:=$8000+(1*1024)+736
    read32(@plinth_sprite[0])
    eepromAddress:=$8000+(2*1024)+736
    read32(@boot_sprite[0])
    eepromAddress:=$8000+(4*1024)+736
    read32(@eugene_sprite[0])
  else}
    levelfilename[5]:="0"
    levelfilename[6]:="1"
    kyefat.openFile(@levelfilename,"r")
    kyefat.fileSeek(736)
    kyefat.readData(@swordfish_sprite,32)
    
    levelfilename[6]:="2"
    kyefat.openFile(@levelfilename,"r") 
    kyefat.fileSeek(736)
    kyefat.readData(@plinth_sprite,32)
    
    levelfilename[6]:="3"
    kyefat.openFile(@levelfilename,"r") 
    kyefat.fileSeek(736)
    kyefat.readData(@boot_sprite,32)
    
    levelfilename[6]:="5"
    kyefat.openFile(@levelfilename,"r") 
    kyefat.readData(@level_buf,736)
    kyefat.readData(@eugene_sprite,32)
    kyefat.closeFile
  long[@swordfish_attr[0]][0]:=$07070707
  long[@plinth_attr[0]][0]:=$07070707
  long[@boot_attr[0]][0]:=$07070707
  long[@eugene_attr[0]][0]:=$07070707

  musicon:=1
    
  repeat

'   TITLE SCREEN  
    {ifnot USE_SDCARD_INSTEAD_OF_HIEEPROM
      LongFill(@Attr,0,768/4)
      LongFill(@Screen,0,6144/4)
      repeat y from 0 to 127
        eepromAddress:=$D000+(y<<5)
        read32(@Screen+((y&7)<<8)+((y&$38)<<2)+((y&64)<<5))
      repeat y from 0 to 15
        eepromAddress:=$e000+(y<<5)
        read32(@Attr+(y<<5))
    else}
      kyefat.openFile(String("titlescr.bin"),"r")
      repeat y from 0 to 127
        kyefat.readData(@Screen+((y&7)<<8)+((y&$38)<<2)+((y&64)<<5),32)
      'sdfat.pclose
      kyefat.openFile(String("titleatr.bin"),"r")
      kyefat.readData(@Attr,512)
      kyefat.closeFile

    y:=16
    repeat x from 0 to 31
      Attr[y<<5 + x] := %01110000
    y:=17
    repeat x from 0 to 9
      Attr[y<<5 + x] := %01010111
    repeat x from 10 to 31
      Attr[y<<5 + x] := %01100111
    longfill(@Attr+512+96,$06060606,32/4)
 
    start_scrolling:=0
    demo:=0
    init_title_tune
    x:=0
    repeat while x == 0
      gameframe++
      VSync:=0
      repeat while VSync==0
'      Spectrum.SetBorderColor(1)
      joypadbits:=NES_Read_Gamepad
      if joypadbits&NES_START
        x := 1
      handle_title_tune
      
      if start_scrolling<>0
        JBSpecGfx.GfxDraw16x16(@MANDAT+((gameframe&(6<<3))<<2),29,9<<3,$17)
        if (gameframe&7)==0
          JBSpecGfx.GfxScroll(@Screen+(19<<8))
          txtx:=31
          txty:=19
          txtcol:=$06
          print_char(titlemsg[start_scrolling])
          start_scrolling++
          if titlemsg[start_scrolling]==0
            start_scrolling:=0
            demo:=255
            x:=1
           
'      Spectrum.SetBorderColor(0)

    stop_title_tune

    level_num:=0
    willylives:=2

    bytefill(@score,"0",6)

    LongFill(@Screen,0,6144/4)
    LongFill(@Attr,0,512/4)

    gameloopexittype:=0
    repeat while gameloopexittype==0

      repeat i from 0 to 8
        sprites[i<<3+0]:=0
        sprites[i<<3+7]:=0

      showlevel

      init_ingame_tune

      gameloopexittype:=0
      repeat while gameloopexittype==0

'        wobble_ingame_tune
        handle_ingame_tune
    
        joypadbits:=NES_Read_Gamepad

        gameframe++
        VSync:=0
        repeat while VSync==0

'        handle_ingame_tune
'        wobble_ingame_tune

'        Spectrum.SetBorderColor(1)
 
        GameDrawStuff
                   
'        Spectrum.SetBorderColor(2)
    
        if(gameframe&3)==0
    
          i:=joypadbits
          if getmapblock(willyx,willyy+16)==4 or getmapblock(willyx+1,willyy+16)==4
            if (byte[level_ptr][623]==0) and ( (i&NES_RIGHT) or ((i&(NES_RIGHT+NES_LEFT))==0) )
              i:=willylastdir        
              if (not(i&(NES_LEFT+NES_RIGHT))) and ((willyframe&128)==0)
                i:=(i&(!NES_RIGHT))|NES_LEFT         
            if (byte[level_ptr][623]==1) and ( (i&NES_LEFT) or ((i&(NES_RIGHT+NES_LEFT))==0) ) 
              i:=willylastdir
              if (not(i&(NES_LEFT+NES_RIGHT))) and ((willyframe&128))
                i:=(i&(!NES_LEFT))|NES_RIGHT         
          if(willyjumping)
            willylastdir:=willyjumpdir
            i:=willyjumpdir
         
          if i&NES_LEFT
            willylastdir:=NES_LEFT
            willyjumpdir:=NES_LEFT
            if willyframe&128
              willyframe-=32
              if (willyframe&$60)==$60 and testmapblock(willyx-1,willyy)<>1 and testmapblock(willyx-1,willyy+8)<>1 and testmapblock(willyx-1,willyy+15)<>1
                willyx--
                willyframe+=128
                if willyx<1
                  willyx++
                  willyframe-=128-32
              else
                if (willyframe&$60)==$60
                  willyframe+=32
            else
              i&=NES_A
              willyframe+=$80
            
          if i&NES_RIGHT
            willylastdir:=NES_RIGHT
            willyjumpdir:=NES_RIGHT
            if willyframe&128
              willyframe-=$80
              i&=NES_A
            else
              willyframe+=32
              if (willyframe&$60)==$0 and testmapblock(willyx+2,willyy)<>1 and testmapblock(willyx+2,willyy+8)<>1 and testmapblock(willyx+2,willyy+15)<>1
                willyx++
                willyframe-=128
                if willyx>29
                  willyx--
                  willyframe+=128-32
              else
                if (willyframe&$60)==$0
                  willyframe-=32
     
          if joypadbits&NES_A
            if willyjumping==0
              willyjumping:=1
              willyjumporigin:=willyy
              willyydir:=-4
              willyjumpdir:=i&(NES_LEFT+NES_RIGHT)
              willyjumpcnt:=0
      
          if willyjumping<>0
            willyjumping++
            willyy+=willyydir
            if willyjumping&1
              if willyydir<4
                willyydir++
            if willyydir>0
              j:=getmapblock(willyx,willyy+16)
              if (j>0 and j<5) or (j==7)
                willyydir:=0
                willyjumping:=0
                willyy&=$78
                if(willyy>willyjumporigin+24)
                  gameloopexittype:=LIFE_LOST
              j:=getmapblock(willyx+1,willyy+16)
              if (j>0 and j<5) or (j==7)
                willyydir:=0
                willyjumping:=0
                willyy&=$78
                if(willyy>willyjumporigin+24)
                  gameloopexittype:=LIFE_LOST
            else
              if testmapblock(willyx,willyy-1)==1 or testmapblock(willyx+1,willyy-1)==1
                willyydir:=0
                willyjumpdir:=0
                willyy:=((willyy-1) & $78)+8
            willyjumpcnt++
            if willyjumpcnt>16
              willyjumpdir:=0
          else
            j:=getmapblock(willyx,willyy+16)
            ifnot ((j>0 and j<5) or (j==7))
              j:=getmapblock(willyx+1,willyy+16)
              ifnot ((j>0 and j<5) or (j==7))
                willyydir:=0
                willyjumping:=1
                willyjumporigin:=willyy+8
                willyjumpdir:=0

        sprites[0<<3+0]:=@MANDAT+willyframe
        sprites[0<<3+1]:=willyx
        sprites[0<<3+2]:=willyy
        sprites[0<<3+3]:=$07
        sprites[0<<3+6]:=@spritebuffer+0*36
    
        repeat x from 0 to 3
          if (HoriBad[x*badsize+badattr]<>0)
            j:=(x+1)<<3
            sprites[j+0]:=level_ptr+768+(HoriBad[x*badsize+baddir]<<5)
            if VertBad[0*badsize+badattr]<>0 or level_num==7 or level_num==11  
              sprites[j+0]:=level_ptr+768+((HoriBad[x*badsize+baddir]|4)<<5)
            sprites[j+1]:=HoriBad[x*badsize+badx]
            sprites[j+2]:=HoriBad[x*badsize+bady]
            sprites[j+3]:=(HoriBad[x*badsize+badattr]&7)+byte[level_ptr][544]&$f8
            sprites[j+6]:=@spritebuffer+((x+1)*36)

            if ( (HoriBad[x*badsize+badattr]&128) and ((gameframe&7)==x) ) or ( (not(HoriBad[x*badsize+badattr]&128)) and ((gameframe&3)==x) ) 
              if HoriBad[x*badsize+baddir]>3
                HoriBad[x*badsize+baddir]--
                if(HoriBad[x*badsize+baddir]&3)==3
                  HoriBad[x*badsize+baddir]+=4
                  HoriBad[x*badsize+badx]--
                if(HoriBad[x*badsize+baddir]&3)==3 and HoriBad[x*badsize+badx]<HoriBad[x*badsize+badmin]
                  HoriBad[x*badsize+badx]++
                  HoriBad[x*badsize+baddir]^=7
              else
                HoriBad[x*badsize+baddir]++
                if(HoriBad[x*badsize+baddir]&3)==0
                  HoriBad[x*badsize+baddir]-=4
                  HoriBad[x*badsize+badx]++
                if(HoriBad[x*badsize+baddir]&3)==0 and HoriBad[x*badsize+badx]>HoriBad[x*badsize+badmax]
                  HoriBad[x*badsize+badx]--
                  HoriBad[x*badsize+baddir]^=7
            HoriBad[x*badsize+badrealx]:=(HoriBad[x*badsize+badx]<<3)+((HoriBad[x*badsize+baddir]&3)<<1)

             
        repeat x from 0 to 3
          if (VertBad[x*badsize+badattr]<>0) and level_num<>13
            j:=(x+4+1)<<3
            sprites[j+0]:=level_ptr+768+(VertBad[x*badsize+baddir]<<5)
            if level_num==4 and x==1
              sprites[j+0]:=@eugene_sprite
            sprites[j+1]:=VertBad[x*badsize+badx]
            sprites[j+2]:=VertBad[x*badsize+bady]
            sprites[j+3]:=(VertBad[x*badsize+badattr]&7)+byte[level_ptr][544]&$f8
            sprites[j+6]:=@spritebuffer+((x+5)*36)
         
            if ((gameframe&3)==x)
              VertBad[x*badsize+baddir]++
              if(VertBad[x*badsize+baddir]&3)==0
                VertBad[x*badsize+baddir]-=4
              VertBad[x*badsize+bady]+=VertBad[x*badsize+badspd]
              if VertBad[x*badsize+bady]<VertBad[x*badsize+badmin] or VertBad[x*badsize+bady]>VertBad[x*badsize+badmax]
                VertBad[x*badsize+bady]-=VertBad[x*badsize+badspd]
                VertBad[x*badsize+badspd]:=-VertBad[x*badsize+badspd]
            VertBad[x*badsize+badrealx]:=(VertBad[x*badsize+badx]<<3)+((VertBad[x*badsize+baddir]&3)<<1)

            if level_num==4 and x==1
              if byte[level_ptr][655]&128
                if (gameframe&7)==0
                  VertBad[x*badsize+badattr]:=((VertBad[x*badsize+badattr]+1)&7)|8
                VertBad[x*badsize+badspd]:=1
                            
          if (VertBad[x*badsize+badattr]<>0) and level_num==13
            j:=(x+4+1)<<3
            sprites[j+0]:=level_ptr+768+(VertBad[x*badsize+baddir]<<5)
            sprites[j+1]:=VertBad[x*badsize+badx]
            sprites[j+2]:=VertBad[x*badsize+bady]
            sprites[j+3]:=(VertBad[x*badsize+badattr]&7)+byte[level_ptr][544]&$f8
            sprites[j+6]:=@spritebuffer+((x+5)*36)
            if ((gameframe&3)==x)
              VertBad[x*badsize+bady]+=VertBad[x*badsize+badspd]
              if VertBad[x*badsize+bady]>VertBad[x*badsize+badmax]
                VertBad[x*badsize+bady]:=VertBad[x*badsize+badmax]
                VertBad[x*badsize+baddir]++
                if (VertBad[x*badsize+baddir]&7)==0
                  VertBad[x*badsize+bady]:=0
                  VertBad[x*badsize+baddir]:=0
                  VertBad[x*badsize+badx]+=8
                  VertBad[x*badsize+badx]&=31
            VertBad[x*badsize+badrealx]:=(VertBad[x*badsize+badx]<<3)+((VertBad[x*badsize+baddir]&3)<<1)

          if (VertBad[x*badsize+badattr]<>0) and (level_num==7 or level_num==11)
            j:=(x+4+1)<<3
            sprites[j+0]:=level_ptr+768+(gameframe&32)
            if VertBad[x*badsize+bady]<>0
              sprites[j+0]+=64
              VertBad[x*badsize+bady]++
              if VertBad[x*badsize+bady]>$68
                VertBad[x*badsize+badattr]:=0
    
        Spectrum.SetBorderColor(byte[level_ptr][627]&7)

        i:=((willyy>>3)<<5)+willyx
        j:=(((willyy+15)>>3)<<5)+willyx
        k:=0
        if Attr[i]&$80
          k|=1
        if Attr[i+1]&$80
          k|=2
        if Attr[i+32]&$80
          k|=1
        if Attr[i+33]&$80
          k|=2
        if k==3
          gameloopexittype:=LEVEL_DONE
        x:=byte[level_ptr][589]
        y:=byte[level_ptr][598]
        if byte[level_ptr][i]==x or byte[level_ptr][i]==y or byte[level_ptr][i+1]==x or byte[level_ptr][i+1]==y or byte[level_ptr][i+32]==x or byte[level_ptr][i+32]==y or byte[level_ptr][i+33]==x or byte[level_ptr][i+33]==y or byte[level_ptr][j]==x or byte[level_ptr][j]==y or byte[level_ptr][j+1]==x or byte[level_ptr][j+1]==y
          if demo==0
            gameloopexittype:=LIFE_LOST

        if demo
          if joypadbits & (NES_A+NES_START)
            gameloopexittype:=BACK_TO_MENU
          demo--
          if demo==0
            gameloopexittype:=LEVEL_DONE
            demo:=255

        if (gameframe&31)==4
          airbits:=(airbits<<1)&255
          Screen[$1140+airleft]:=airbits
          Screen[$1160+airleft]:=airbits
          Screen[$1180+airleft]:=airbits
          Screen[$11A0+airleft]:=airbits
          if airbits==0
            airleft--
            airbits:=255
            if airleft==2
              if demo==0
                gameloopexittype:=LIFE_LOST      

        x:=((willyx<<3)+((willyframe>>4)&6))
        y:=Willyy
        if demo==0
          repeat i from 0 to 3
            if HoriBad[(i*badsize)+badattr] <> 0
              if ( ( y + 15 ) > HoriBad[(i*badsize)+bady] ) and ( ( HoriBad[(i*badsize)+bady] + 15 ) > y ) and ( ( x + 8 ) > HoriBad[(i*badsize)+badrealx] ) and ( ( HoriBad[(i*badsize)+badrealx] + 8 ) > x )
                gameloopexittype:=LIFE_LOST
          repeat i from 0 to 3
            if VertBad[(i*badsize)+badattr] <> 0
              if ( ( y + 15 ) > VertBad[(i*badsize)+bady] ) and ( ( VertBad[(i*badsize)+bady] + 15 ) > y ) and ( ( x + 8 ) > VertBad[(i*badsize)+badrealx] ) and ( ( VertBad[(i*badsize)+badrealx] + 10 ) > x )
                gameloopexittype:=LIFE_LOST

        if(gameframe&7)==0
          livesframe+=32
          livesframe&=(3<<5)
    
        ifnot 0'USE_HYDRA_KEYBAORD
          if key.keystate("a")
            gameloopexittype:=LEVEL_DONE
          if key.keystate("d")
            gameloopexittype:=LIFE_LOST
        {else
          if keyhydra.keystate("a")
            gameloopexittype:=LEVEL_DONE
          if keyhydra.keystate("d")
            gameloopexittype:=LIFE_LOST}
        

      stop_ingame_tune

      if gameloopexittype==LEVEL_DONE
        if demo
          fadescreen($3f)
        else
          repeat while airleft=>3
            addscore(@add1)
            airbits:=(airbits<<1)&255
            Screen[$1140+airleft]:=airbits
            Screen[$1160+airleft]:=airbits
            Screen[$1180+airleft]:=airbits
            Screen[$11A0+airleft]:=airbits
            if airbits==0
              airleft--
              airbits:=255
                                 
          
        gameloopexittype:=0
        level_num++
        if level_num>19
          level_num:=0

      if gameloopexittype==LIFE_LOST
        fadescreen(7)
        gameloopexittype:=0
        willylives--
        if willylives==-1
          checkhiscore        
          LongFill(@Attr,0,512/4)
          LongFill(@Screen,0,4096/4)
          LongFill(@Attr,$47474747,512/4)
          booty:=0
          repeat while booty=<$60
            gameframe++
            VSync:=0
            repeat while VSync==0

            JBSpecGfx.GfxPut16x16(@MANDAT+64,15,$60,0)      
            JBSpecGfx.GfxPut16x16(@plinth_sprite,15,$70,0)      
            JBSpecGfx.GfxPut16x16(@boot_sprite,15,booty,0)

            booty++
            x:=((booty&6)<<2)+$47
            x:=x+(x<<8)+(x<<16)+(x<<24)
            longfill(@Attr,x,512/4)

          longfill(@Attr,$47474747,512/4)
          print(@gametext,10,6,%01000111)
          print(@overtext,18,6,%01000111)
    
          x:=0
          repeat while x<(5*60)
            gameframe++
            VSync:=0
            repeat while VSync==0
            x++
            Attr[6<<5+10]:=((x+0)&7)|$40
            Attr[6<<5+11]:=((x+1)&7)|$40
            Attr[6<<5+12]:=((x+2)&7)|$40
            Attr[6<<5+13]:=((x+3)&7)|$40
            Attr[6<<5+18]:=((x+4)&7)|$40
            Attr[6<<5+19]:=((x+5)&7)|$40
            Attr[6<<5+20]:=((x+6)&7)|$40
            Attr[6<<5+21]:=((x+7)&7)|$40
           
          gameloopexittype:=LIFE_LOST      
                                     
PUB GameDrawStuff | i,j,k,x,y
  repeat i from 0 to 8
    if sprites[i<<3+7]<>0
      JBSpecGfx.GfxPut16x16(sprites[i<<3+6],sprites[i<<3+4],sprites[i<<3+5],0)      

  if level_num==18
    repeat y from 0 to 4
      pickupbuf[y]:=Attr[pickups[y]]
    longmove(@Attr,level_ptr,512/4)
    repeat y from 0 to 4
      Attr[pickups[y]]:=pickupbuf[y]
   
  if(gameframe&3)==0
    if (byte[level_ptr][623]==0) 
      y:=byte[conveyorPtr][0]
      i:=((y&$7f)<<1)+((y&$80)>>7)
      y:=byte[conveyorPtr][64]
      j:=((y&$fe)>>1)+((y&$01)<<7)
    else
      y:=byte[conveyorPtr][0]
      i:=((y&$fe)>>1)+((y&$01)<<7)
      y:=byte[conveyorPtr][64]
      j:=((y&$7f)<<1)+((y&$80)>>7)
    bytefill(conveyorPtr,i,conveyorLen)
    bytefill(conveyorPtr+64,j,conveyorLen)
'    repeat y from 0 to conveyorLen-1
'      byte[conveyorPtr][y]:=i
'      byte[conveyorPtr][y+64]:=j
   
  i:=((willyy>>3)<<5)+willyx
  j:=(((willyy+15)>>3)<<5)+willyx
  repeat y from 0 to 4
    if pickups[y]<>0
      if(gameframe&3)==1
        x:=Attr[pickups[y]]-1
        if (x&3)==2
          x+=4
        Attr[pickups[y]]:=x
      x:=pickups[y]
      if (x==i) or (x==(i+1)) or (x==(i+32)) or (x==(i+33)) or (x==j) or (x==(j+1))
        k:=((x&480)<<3)+(x&31)
        Screen[k    ]:=0
        Screen[k+ 32]:=0
        Screen[k+ 64]:=0
        Screen[k+ 96]:=0
        Screen[k+128]:=0
        Screen[k+160]:=0
        Screen[k+192]:=0
        Screen[k+224]:=0
        Attr[x]:=byte[level_ptr][544]
        pickups[y]:=0
        addscore(@add100)

  x:=0
  repeat y from 0 to 4
    if pickups[y]<>0
      x++
  if x==0
    byte[level_ptr][655]|=128

  if level_num==7 or level_num==11
    i:=((willyy>>3)<<5)+willyx
    if (byte[level_ptr][i]==byte[level_ptr][607]) or (byte[level_ptr][i+1]==byte[level_ptr][607])
      if willyx<8
        if byte[level_ptr][11<<5+17]<>byte[level_ptr][544]
          byte[level_ptr][11<<5+17]:=byte[level_ptr][544]      
          byte[level_ptr][12<<5+17]:=byte[level_ptr][544]      
          txtx:=17
          txty:=11
          txtcol:=byte[level_ptr][544]
          print_char(" ")
          txtx:=17
          txty:=12        
          print_char(" ")
          HoriBad[1*badsize+badmax]+=3        
      else
        if byte[level_ptr][2<<5+15]<>byte[level_ptr][544]
          byte[level_ptr][2<<5+15]:=byte[level_ptr][544]      
          byte[level_ptr][2<<5+16]:=byte[level_ptr][544]      
          txtx:=15
          txty:=2
          txtcol:=byte[level_ptr][544]
          print_char(" ")
          print_char(" ")        
          VertBad[0*badsize+bady]:=1
          VertBad[0*badsize+badspd]:=1
          VertBad[0*badsize+badmax]:=$80

  if(gameframe&3)==0 and willyjumping==0
    x:=(((willyy+16)>>3)<<5)+willyx
    if byte[level_ptr][x]==byte[level_ptr][562] 'crumble attr
      k:=((x&480)<<3)+(x&31)
      Screen[k+224]:=Screen[k+192]
      Screen[k+192]:=Screen[k+160]
      Screen[k+160]:=Screen[k+128]
      Screen[k+128]:=Screen[k+ 96]
      Screen[k+ 96]:=Screen[k+ 64]
      Screen[k+ 64]:=Screen[k+ 32]
      Screen[k+ 32]:=Screen[k+  0]
      Screen[k    ]:=0
      j:=Screen[k+32]|Screen[k+64]|Screen[k+96]|Screen[k+128]|Screen[k+160]|Screen[k+192]|Screen[k+224]
      if(j==0)
        byte[level_ptr][x]:=byte[level_ptr][544] 'blank attr
    if byte[level_ptr][x+1]==byte[level_ptr][562] 'crumble attr
      k:=((x&480)<<3)+((x+1)&31)
      Screen[k+224]:=Screen[k+192]
      Screen[k+192]:=Screen[k+160]
      Screen[k+160]:=Screen[k+128]
      Screen[k+128]:=Screen[k+ 96]
      Screen[k+ 96]:=Screen[k+ 64]
      Screen[k+ 64]:=Screen[k+ 32]
      Screen[k+ 32]:=Screen[k+  0]
      Screen[k    ]:=0
      j:=Screen[k+32]|Screen[k+64]|Screen[k+96]|Screen[k+128]|Screen[k+160]|Screen[k+192]|Screen[k+224]
      if(j==0)
        byte[level_ptr][x+1]:=byte[level_ptr][544]
     
  repeat i from 0 to 8
    if sprites[(i<<3)+0]<>0
      sprites[(i<<3)+4]:=sprites[i<<3+1]                          
      sprites[(i<<3)+5]:=sprites[i<<3+2]                          
      JBSpecGfx.GfxGet16x16(sprites[(i<<3)+6],sprites[(i<<3)+1],sprites[(i<<3)+2],0)
      sprites[(i<<3)+7]:=1

  if level_num==18
    x:=23
    y:=32
    repeat while byte[level_ptr][x]==byte[level_ptr][544]
      Attr[x]:=%01110111
      x+=y
      if(x&31)==23 or (x&31)==22
        if (x>>5)==3 or (x>>5)==4 
          i:=HoriBad[0*badsize+badx]
          if (x&31)==i or (x&31)==i+1
            y:=31-y
        if (x>>5)==6 or (x>>5)==7 
          i:=HoriBad[1*badsize+badx]
          if (x&31)==i or (x&31)==i+1
            y:=31-y
        if (x>>5)==9 or (x>>5)==10 
          i:=HoriBad[2*badsize+badx]
          if (x&31)==i or (x&31)==i+1
            y:=31-y
        if (x>>5)==13 or (x>>5)==14 
          i:=HoriBad[3*badsize+badx]
          if (x&31)==i or (x&31)==i+1
            y:=31-y
      if(x&31)==5 or (x&31)==6
        i:=(VertBad[0*badsize+bady]>>3)
        if (x>>5)==i or (x>>5)==i+1
          y:=31-y
      if(x&31)==11 or (x&31)==12
        i:=(VertBad[1*badsize+bady]>>3)
        if (x>>5)==i or (x>>5)==i+1
          y:=31-y
      if(x&31)==16 or (x&31)==17
        i:=(VertBad[2*badsize+bady]>>3)
        if (x>>5)==i or (x>>5)==i+1
          y:=31-y

  if demo==0
    i:=0      
    if sprites[(i<<3)+0]<>0
      JBSpecGfx.GfxOr16x16noattr(sprites[(i<<3)+0],sprites[(i<<3)+1],sprites[(i<<3)+2],sprites[(i<<3)+3])
    i:=(((willyy)>>3)<<5)+willyx
    j:=(((willyy+15)>>3)<<5)+willyx
    if byte[level_ptr][i]==byte[level_ptr][544] 'blank attr
      Attr[i]|=7
    if byte[level_ptr][i+1]==byte[level_ptr][544] 'blank attr
      Attr[i+1]|=7
    if byte[level_ptr][i+32]==byte[level_ptr][544] 'blank attr
      Attr[i+32]|=7
    if byte[level_ptr][i+33]==byte[level_ptr][544] 'blank attr
      Attr[i+33]|=7
    if i<>j
      if byte[level_ptr][j]==byte[level_ptr][544] 'blank attr
        Attr[j]|=7
      if byte[level_ptr][j+1]==byte[level_ptr][544] 'blank attr
        Attr[j+1]|=7
      
  repeat i from 1 to 8
    if sprites[(i<<3)+0]<>0
      JBSpecGfx.GfxOr16x16(sprites[(i<<3)+0],sprites[(i<<3)+1],sprites[(i<<3)+2],sprites[(i<<3)+3])      
    
  JBSpecGfx.GfxDraw16x16(level_ptr+656,byte[level_ptr][688]&31,((word[level_ptr][688>>1]>>5)&15)<<3,byte[level_ptr][655])

  x:=0
  repeat while x<willylives
    JBSpecGfx.GfxDraw16x16(@MANDAT+livesframe, x<<1,168,$47)
    x++      
  JBSpecGfx.GfxPut16x16(@blanksprite,x<<1,168,0)      

  if level_num==19
    JBSpecGfx.GfxDraw16x16(@MANDAT+96,19,2<<3,$2f)
    Attr[3<<5+19]:=$27
    Attr[3<<5+20]:=$27
    JBSpecGfx.GfxDraw16x16(@swordfish_sprite,19,5<<3,$45)
    Attr[6<<5+19]:=$46
    Attr[6<<5+20]:=$47
    Attr[7<<5+19]:=0
    Attr[7<<5+20]:=0
    
PUB getmapblock(x,y) : blocktype | p
  if y&4
   blocktype:=0
    return
  p:=byte[level_ptr][(((y)>>3)<<5)+x]
  repeat blocktype from 0 to 7
    if p==byte[level_ptr][544+(blocktype*9)]
      return
  blocktype:=3                  'force wall type if unknown block ( usually level 20 graphic at the top of screen )

PUB testmapblock(x,y) : q | p
  q:=0
  if byte[level_ptr][(((y)>>3)<<5)+x]==byte[level_ptr][571] ' wall block
    q:=1

PUB showlevel | i,x,y,p,c,y2,p2

    {ifnot USE_SDCARD_INSTEAD_OF_HIEEPROM
      eepromAddress:=$8000+(level_num*1024)
      p:=@level_buf
      repeat i from 0 to (1024/32)-1
        read32(p)
        p+=32
        eepromAddress+=32
    else}
      levelfilename[5]:=((level_num+1)/10)+"0"
      levelfilename[6]:=((level_num+1)//10)+"0"
      kyefat.openFile(@levelfilename,"r")
      kyefat.readData(@level_buf,1024)
      kyefat.closeFile
    
    LongFill(@Attr,$0,512/4)

    level_ptr:=@level_buf '+(level_num*1024)

    y:=16
    repeat x from 0 to 31
      Attr[y<<5 + x] := %01110000
    y:=17
    repeat x from 0 to 9
      Attr[y<<5 + x] := %01010111
    repeat x from 10 to 31
      Attr[y<<5 + x] := %01100111
    y:=19        
    repeat x from 0 to 31
      Attr[y<<5 + x] := %01000110

    if 1'USE_SDCARD_INSTEAD_OF_HIEEPROM
      kyefat.openFile(String("titlescr.bin"),"r")
      repeat y from 0 to 63
        kyefat.readData(@Screen+((y&7)<<8)+((y&$38)<<2)+((y&64)<<5),32)
      kyefat.closeFile
     
    i:=0
    repeat y from 0 to 15
      repeat x from 0 to 31
        p:=byte[level_ptr][i]
         p2:=0
         y2:=0
         Attr[y<<5+x]:=p
         repeat c from 0 to 7
          if p==byte[level_ptr][544+(c*9)]
            p2 := level_ptr+545+(c*9)
            if c==0 '((p>>3)&7)==(p&7)
              p|=%00000111
            Attr[y<<5 + x] := p
            repeat y2 from 0 to 7
              Screen[((y<<3+y2)<<5)+x] := byte[p2][y2]
         {ifnot USE_SDCARD_INSTEAD_OF_HIEEPROM
           if y2==0
             Attr[y<<5 + x] := i2c.ReadByte(i2c#BootPin, i2c#EEPROM, $E000+y<<5+x)
             repeat y2 from 0 to 7
               Screen[((y<<3+y2)<<5)+x] := i2c.ReadByte(i2c#BootPin, i2c#EEPROM, $D000+((y&8)<<8)+((y&7)<<5)+(y2<<8)+x) }
        i++
        
    txtx:=0
    txty:=16
    txtcol:=%01110000
    repeat x from 0 to 31
      p:=byte[level_ptr][i++]
      print_char(p)

    print(@airtext,0,17,%01010111)
    print(@hiscoretext,0,19,%01000110)
    print(@hiscore,11,19,%01000110)
    print(@scoretext,20,19,%01000110)
    print(@score,26,19,%01000110)

    willyx:=word[level_ptr][620>>1]&31
    willyy:=((word[level_ptr][620>>1]>>5)&15)<<3
    willydir:=byte[level_ptr][618]                      'sprite number
    willyframe:=(byte[level_ptr][617]<<5)+byte[level_ptr][618]<<7
    willyjumping:=0
    willyydir:=0

    conveyorX:=(byte[level_ptr][624])&31
    conveyorY:=((((byte[level_ptr][624]>>5)&7)+((byte[level_ptr][625])&8))<<3)
    conveyorPtr:=@Screen+(conveyorY<<5)+(conveyorX)
    conveyorLen:=byte[level_ptr][626]

    levelbits:=byte[level_ptr][627]

    p:=629
    repeat y from 0 to 4
      c:=byte[level_ptr][p]
      pickups[y]:=0
      if (c<>255) and (c<>0)
        x:=(byte[level_ptr][p+2]<<8)+byte[level_ptr][p+1]
        Attr[(x&$1ff)]:=c
        pickups[y]:=x&$1ff
        repeat c from 0 to 7
          Screen[((((x>>2)&120)+c)<<5)+(x&$1f)]:=byte[level_ptr][692+c]
      p+=5

    x:=3
    repeat while x=<(byte[level_ptr][700]&31)
      Screen[$1140+x]:=255
      Screen[$1160+x]:=255
      Screen[$1180+x]:=255
      Screen[$11A0+x]:=255
      x++
    if x<31
      c:=byte[level_ptr][701]
      Screen[$1140+x]:=c
      Screen[$1160+x]:=c
      Screen[$1180+x]:=c
      Screen[$11A0+x]:=c
    x++
    repeat while x<32
      Screen[$1140+x]:=0
      Screen[$1160+x]:=0
      Screen[$1180+x]:=0
      Screen[$11A0+x]:=0
      x++
    airleft:=(byte[level_ptr][700]&31)
    airbits:=byte[level_ptr][701]

    JBSpecGfx.GfxDraw16x16(level_ptr+656,byte[level_ptr][688]&31,((word[level_ptr][688>>1]>>5)&15)<<3,byte[level_ptr][655])

    p2:=1
    repeat x from 0 to 3
      HoriBad[(x*badsize)+badattr]:=0
      if byte[level_ptr][702+(x*7)+0]==0 or byte[level_ptr][702+(x*7)+0]==255
        p2:=0
      if p2==1
        HoriBad[(x*badsize)+badattr]:=byte[level_ptr][702+(x*7)+0]
        HoriBad[(x*badsize)+badx]   :=byte[level_ptr][702+(x*7)+1]&31
        HoriBad[(x*badsize)+bady]   :=(((byte[level_ptr][702+(x*7)+1]>>5)+(byte[level_ptr][702+(x*7)+3]&8))<<3)&127
        HoriBad[(x*badsize)+baddir] :=byte[level_ptr][702+(x*7)+4]
        HoriBad[(x*badsize)+badmin] :=byte[level_ptr][702+(x*7)+5]&31
        HoriBad[(x*badsize)+badmax] :=byte[level_ptr][702+(x*7)+6]&31
        HoriBad[(x*badsize)+badspd] :=1
        if HoriBad[(x*badsize)+baddir]>4
          HoriBad[(x*badsize)+badspd]:=-1

    p2:=1
    repeat x from 0 to 3
      VertBad[(x*badsize)+badattr]:=0
      if byte[level_ptr][733+(x*7)+0]==0 or byte[level_ptr][733+(x*7)+0]==255
        p2:=0
      if p2==1
        VertBad[(x*badsize)+badattr]:=byte[level_ptr][733+(x*7)+0]&127
        VertBad[(x*badsize)+badx]   :=byte[level_ptr][733+(x*7)+3]&31
        VertBad[(x*badsize)+bady]   :=byte[level_ptr][733+(x*7)+2]&127
        VertBad[(x*badsize)+baddir] :=byte[level_ptr][733+(x*7)+1]
        VertBad[(x*badsize)+badmin] :=byte[level_ptr][733+(x*7)+5]
        VertBad[(x*badsize)+badmax] :=byte[level_ptr][733+(x*7)+6]
        VertBad[(x*badsize)+badspd] :=byte[level_ptr][733+(x*7)+4]

    if level_num==4
      VertBad[(1*badsize)+badattr]:=$17
      VertBad[(1*badsize)+badx]   :=15
      VertBad[(1*badsize)+bady]   :=1<<3
      VertBad[(1*badsize)+baddir] :=1
      VertBad[(1*badsize)+badmin] :=$00
      VertBad[(1*badsize)+badmax] :=$58
      VertBad[(1*badsize)+badspd] :=1

    if level_num==7
      HoriBad[(2*badsize)+badattr]:=$05
      HoriBad[(2*badsize)+badx]   :=18
      HoriBad[(2*badsize)+bady]   :=7<<3
      HoriBad[(2*badsize)+baddir] :=1
      HoriBad[(2*badsize)+badmin] :=18
      HoriBad[(2*badsize)+badmax] :=21
      HoriBad[(2*badsize)+badspd] :=1

    if level_num==11
      HoriBad[(2*badsize)+badattr]:=$05
      HoriBad[(2*badsize)+badx]   :=25
      HoriBad[(2*badsize)+bady]   :=6<<3
      HoriBad[(2*badsize)+baddir] :=1
      HoriBad[(2*badsize)+badmin] :=25
      HoriBad[(2*badsize)+badmax] :=28
      HoriBad[(2*badsize)+badspd] :=1

    if level_num==7 or level_num==11
      VertBad[(0*badsize)+badattr]:=$04
      VertBad[(0*badsize)+badx]   :=15
      VertBad[(0*badsize)+bady]   :=0<<3
      VertBad[(0*badsize)+baddir] :=0
      VertBad[(0*badsize)+badmin] :=$00
      VertBad[(0*badsize)+badmax] :=$00
      VertBad[(0*badsize)+badspd] :=0

    if level_num==10
      VertBad[(3*badsize)+badspd]:=4
    if level_num==14
      VertBad[(2*badsize)+badspd]:=4
    if level_num==18
      VertBad[(1*badsize)+badspd]:=2

    if level_num==19
      JBSpecGfx.GfxDraw16x16(@MANDAT+96,19,2<<3,$2f)
      Attr[3<<5+19]:=$27
      Attr[3<<5+20]:=$27
      JBSpecGfx.GfxDraw16x16(@swordfish_sprite,19,5<<3,$45)
      Attr[6<<5+19]:=$46
      Attr[6<<5+20]:=$47
      Attr[7<<5+19]:=0
      Attr[7<<5+20]:=0
      

PUB fadescreen(x) | y
  y:=x
  repeat while x=>0
    gameframe++
    VSync:=0
    repeat while VSync==0
    bytefill(@Attr,x,512)
    x--
    if y==$3f and x>8
      x--    

PUB print(p,x,y,col)
  txtx:=x
  txty:=y
  txtcol:=col
  repeat while byte[p][0]<>0
    print_char(byte[p][0])
    p++    

PUB print_char(p) | y
  repeat y from 0 to 7
    Screen[(((txty<<3)+y)<<5)+txtx]:=font[(p-32)<<3+y]
  Attr[txty<<5+txtx]:=txtcol
  txtx++
  if txtx>31
    txtx:=0
    if txty<24
      txty++

PUB NES_Read_Gamepad : nes_bits   |       i
' //////////////////////////////////////////////////////////////////
' NES Game Pad Read
' //////////////////////////////////////////////////////////////////       
' reads both gamepads in parallel encodes 8-bits for each in format
' right game pad #1 [15..8] : left game pad #0 [7..0]
'
' set I/O ports to proper direction
' P3 = JOY_CLK      (4)
' P4 = JOY_SH/LDn   (5) 
' P5 = JOY_DATAOUT0 (6)
' P6 = JOY_DATAOUT1 (7)
' NES Bit Encoding
'
' RIGHT  = %00000001
' LEFT   = %00000010
' DOWN   = %00000100
' UP     = %00001000
' START  = %00010000
' SELECT = %00100000
' B      = %01000000
' A      = %10000000

' step 1: set I/Os
DIRA [JOY_CLK] := 1 ' output
DIRA [JOY_LCH] := 1 ' output
DIRA [JOY_DATAOUT0] := 0 ' input
DIRA [JOY_DATAOUT1] := 0 ' input

' step 2: set clock and latch to 0
OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0
'Delay(1)

' step 3: set latch to 1
OUTA [JOY_LCH] := 1 ' JOY_SH/LDn = 1
'Delay(1)                            

' step 4: set latch to 0
OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0

' data is now ready to shift out, clear storage
nes_bits := 0

' step 5: read 8 bits, 1st bits are already latched and ready, simply save and clock remaining bits
repeat i from 0 to 7

 nes_bits := (nes_bits << 1)
 nes_bits := nes_bits | INA[JOY_DATAOUT0] | (INA[JOY_DATAOUT1] << 8)

 OUTA [JOY_CLK] := 1 ' JOY_CLK = 1
 'Delay(1)             
 OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
 
 'Delay(1)             

' invert bits to make positive logic
nes_bits := (!nes_bits & $FFFF) 


' Keyboard (mapped onto NES buttons)
  'ifnot USE_HYDRA_KEYBAORD
    if(key.keystate($C2))       'Up Arrow
      nes_bits|=NES_UP
    if(key.keystate($C3))       'Down Arrow
      nes_bits|=NES_DOWN
    if(key.keystate($C0))       'Left Arrow
      nes_bits|=NES_LEFT
    if(key.keystate($C1))       'Right Arrow
      nes_bits|=NES_RIGHT
    if(key.keystate($0D))       'Enter
      nes_bits|=NES_START
    if(key.keystate(" "))       'Space
      nes_bits|=NES_A
  {else
    if(keyhydra.keystate($C2))       'Up Arrow
      nes_bits|=NES_UP
    if(keyhydra.keystate($C3))       'Down Arrow
      nes_bits|=NES_DOWN
    if(keyhydra.keystate($C0))       'Left Arrow
      nes_bits|=NES_LEFT
    if(keyhydra.keystate($C1))       'Right Arrow
      nes_bits|=NES_RIGHT
    if(keyhydra.keystate($0D))       'Enter
      nes_bits|=NES_START
    if(keyhydra.keystate(" "))       'Space
      nes_bits|=NES_A}

  'nes_bits := (nes_bits & $FFFF)

{pub read32(buffer)
   if i2c.ReadPage(i2c#BootPin, i2c#EEPROM, eepromAddress, buffer, 32)
        abort ' an error occurred during the read

pub write32(buffer) | startTime
   if i2c.WritePage(i2c#BootPin, i2c#EEPROM, eepromAddress, buffer, 32)
     abort ' an error occured during the write
   startTime := cnt ' prepare to check for a timeout
   repeat while i2c.WriteWait(i2c#BootPin, i2c#EEPROM, eepromAddress)
     if cnt - startTime > clkfreq / 10
       abort ' waited more than a 1/10 second for the write to finish
}
pub checkhiscore | x
  repeat x from 0 to 5
    if score[x]<hiscore[x]
      return
    if score[x]>hiscore[x]
      bytemove(@hiscore,@score,6)
      print(@hiscore,11,19,%01000110)
      return
        
pub addscore(addstring) | x,c
  c:=0
  repeat x from 0 to 5
    score[5-x]+=(byte[addstring][5-x]-"0")+c
    c:=0
    if score[5-x]>"9"
      score[5-x]-=10
      c:=1 

  print(@score,26,19,%01000110)

pub init_title_tune
  title_tune_pos:=0
  title_tune_note1:=0
  title_tune_note2:=0
  title_tune_delay:=1
  if start_scrolling<>0
    return
  if musicon==0
    start_scrolling:=1
    return
  if muscog<>0
    cogstop(muscog)
  muscog:=cognew(@MusicEntry, @title_tune_note1)

pub stop_title_tune
  if muscog<>0
    cogstop(muscog)
  muscog:=0
  
pub handle_title_tune | x
  if (USE_HYDRA_KEYBAORD==0 and key.key=="m")' or (USE_HYDRA_KEYBAORD==1 and keyhydra.key=="m")
    musicon:=1-musicon
    if musicon==0
      title_tune_delay:=$7fffffff
      title_tune_note1:=$ffffffff         
      title_tune_note2:=$ffffffff
      stop_title_tune
      if start_scrolling==0
        start_scrolling:=1
      return
    else
      if start_scrolling<>0
        return
      init_title_tune
  if musicon==0
    if start_scrolling==0
      start_scrolling:=1
    return
          
  if title_tune_delay>1
    title_tune_delay-=4
  else
    longfill(@Attr+$1c0,$38383838,64/4)
    if title_tune[title_tune_pos]<>255
      title_tune_delay:=title_tune[title_tune_pos++]
      title_tune_note1:=title_tune[title_tune_pos++]
      title_tune_note2:=title_tune[title_tune_pos++]
      x:=0
      repeat while notetable[x]<>1
        if notetable[x]==title_tune_note1
          Attr[$1c0+notetable[x+1]]:=%01010000         
        if notetable[x]==title_tune_note2
          Attr[$1c0+notetable[x+1]]:=%01101000
        if notetable[x]==title_tune_note2+1
          Attr[$1c0+notetable[x+1]]:=%01101000
        x+=2
    else
      title_tune_delay:=$7fffffff
      title_tune_note1:=$ffffffff         
      title_tune_note2:=$ffffffff
      stop_title_tune
      start_scrolling:=1         

pub init_ingame_tune
  if demo<>0
    return
  title_tune_pos:=0
  title_tune_note1:=0
  title_tune_note2:=0
  title_tune_delay:=1
  if musicon==0
    return
  if muscog<>0
    cogstop(muscog)
  muscog:=cognew(@MusicEntry, @title_tune_note1)

pub stop_ingame_tune
  if muscog<>0
    cogstop(muscog)
  muscog:=0
  
pub handle_ingame_tune
  if (USE_HYDRA_KEYBAORD==0 and key.key=="m")' or (USE_HYDRA_KEYBAORD==1 and keyhydra.key=="m")
    musicon:=1-musicon
    if musicon==0
      title_tune_delay:=$7fffffff
      title_tune_note1:=$ffffffff         
      title_tune_note2:=$ffffffff
      stop_title_tune
      return
    else
      init_title_tune
  if musicon==0
    return

  if title_tune_delay>1
    title_tune_delay-=4
  else
    title_tune_delay:=50
    if ingame_tune[title_tune_pos]==0
      title_tune_pos:=0
    title_tune_note1:=ingame_tune[title_tune_pos++]
    title_tune_note2:=title_tune_note1+1

pub wobble_ingame_tune
  title_tune_note1:=254
  title_tune_note1:=254
  
DAT
              org       $000
MusicEntry    mov       musbitmask,#USE_SOUND_BITS_FOR_STEREO
              shl       musbitmask,#USE_SOUND_PINS       
              or        DIRA, musbitmask
              mov       r0, PAR
              mov       tone1ptr,r0
              rdlong    t1,tone1ptr
              add       r0,#4
              mov       tone2ptr,r0
              rdlong    t2,tone2ptr
              mov       bits,musbitmask
:loop
              sub       t1,#1
              and       t1,#255
              cmp       t1,#0 wz
        if_z  xor       bits,musbitmask
        if_z  rdlong    t1,tone1ptr    
              sub       t2,#1
              and       t2,#255
              cmp       t2,#0 wz
        if_z  xor       bits,musbitmask
        if_z  rdlong    t2,tone2ptr

        if_z  cmp       t2,#254 wz
        if_z  jmp       #:stopme  

              mov       r0, #256
:lp           djnz      r0, #:lp

              mov       outa, bits

              jmp      #:loop

:stopme       rdlong    t2,tone2ptr
              cmp       t2,#254 wz
        if_z  jmp       #:stopme
          
              rdlong    t1,tone1ptr
              rdlong    t2,tone2ptr

              jmp      #:loop
                                   
tone1ptr      res       1
tone2ptr      res       1
t1            res       1              
t2            res       1              
r0            res       1
bits          res       1
musbitmask    res       1              
       
DAT

font          file  "SpectrumFont.bin"

title_tune    byte
              byte     80,$80,$81,    80,$66,$67,    80,$56,$57,    50,$56,$57
              byte     50,$ab,$cb,    50,$2b,$33,    50,$2b,$33,    50,$ab,$cb
              byte     50,$33,$40,    50,$33,$40,    50,$ab,$cb,    50,$80,$81
              byte     50,$80,$81,    50,$66,$67,    50,$56,$57,    50,$60,$56
              byte     50,$ab,$c0,    50,$2b,$30,    50,$2b,$30,    50,$ab,$c0
              byte     50,$30,$44,    50,$30,$44,    50,$ab,$c0,    50,$88,$89
              byte     50,$88,$89,    50,$72,$73,    50,$4c,$4d,    50,$4c,$4d
              byte     50,$ab,$c0,    50,$26,$30,    50,$26,$30,    50,$ab,$c0
              byte     50,$30,$44,    50,$30,$44,    50,$ab,$c0,    50,$88,$89
              byte     50,$88,$89,    50,$72,$73,    50,$4c,$4d,    50,$4c,$4d
              byte     50,$ab,$cb,    50,$26,$33,    50,$26,$33,    50,$ab,$cb
              byte     50,$33,$40,    50,$33,$40,    50,$ab,$cb,    50,$80,$81
              byte     50,$80,$81,    50,$66,$67,    50,$56,$57,    50,$40,$41
              byte     50,$80,$ab,    50,$20,$2b,    50,$20,$2b,    50,$80,$ab
              byte     50,$2b,$33,    50,$2b,$33,    50,$80,$ab,    50,$80,$81
              byte     50,$80,$81,    50,$66,$67,    50,$56,$57,    50,$40,$41
              byte     50,$80,$98,    50,$20,$26,    50,$20,$26,    50,$80,$98
              byte     50,$26,$30,    50,$26,$30,    50,$00,$00,    50,$72,$73
              byte     50,$72,$73,    50,$60,$61,    50,$4c,$4d,    50,$4c,$99
              byte     50,$4c,$4d,    50,$4c,$4d,    50,$4c,$99,    50,$5b,$5c
              byte     50,$56,$57,    50,$33,$cd,    50,$33,$34,    50,$33,$34
              byte     50,$33,$cd,    50,$40,$41,    50,$66,$67,   100,$66,$67
              byte     50,$72,$73,   100,$4c,$4d,    50,$56,$57,    50,$80,$cb
              byte     25,$80,$00,    25,$80,$81,    50,$80,$cb,   $ff,$00,$00

'data conversion of File = 'music_ingame.bin'
ingame_tune   byte
              byte    $80,$72,$66,$60,$56,$66,$56,$56
              byte    $51,$60,$51,$51,$56,$66,$56,$56
              byte    $80,$72,$66,$60,$56,$66,$56,$56
              byte    $51,$60,$51,$51,$56,$56,$56,$56
              byte    $80,$72,$66,$60,$56,$66,$56,$56
              byte    $51,$60,$51,$51,$56,$66,$56,$56
              byte    $80,$72,$66,$60,$56,$66,$56,$40
              byte    $56,$66,$80,$66,$56,$56,$56,$56
              byte    $00,0,0,0
        
blanksprite   byte  0,0,0,0,0,0,0,0
              byte  0,0,0,0,0,0,0,0
              byte  0,0,0,0,0,0,0,0
              byte  0,0,0,0,0,0,0,0

notetable     byte  $00,$24
              byte  $ff,$24,$f2,$04,$e6,$25,$d8,$05,$cc,$26,$c0,$27,$b4,$07,$ab,$28,$a2,$08,$99,$29,$90,$09,$88,$2a
              byte  $80,$2b,$79,$0b,$73,$2c,$6c,$0c,$66,$2d,$60,$2e,$5b,$0e,$56,$2f,$51,$0f,$4c,$30,$48,$10,$44,$31    
              byte  $40,$32,$3c,$12,$39,$33,$36,$13,$33,$34,$30,$35,$2d,$15,$2b,$36,$28,$16,$26,$37,$24,$17,$22,$38    
              byte  $20,$39,$1f,$19,$1d,$3a,$1b,$1a,$19,$3b,$18,$3c,$17,$1c,$16,$3d,$14,$1d,$13,$3e,$12,$1e,$11,$3f    
              byte  $10,$2b
              byte    1,1,1,1    

MANDAT        byte  $06,$00,$3E,$00,$7C,$00,$34,$00,$3E,$00,$3C,$00,$18,$00,$3C,$00,$7E,$00,$7E,$00,$F7,$00,$FB,$00,$3C,$00,$76,$00,$6E,$00,$77,$00
              byte  $01,$80,$0F,$80,$1F,$00,$0D,$00,$0F,$80,$0F,$00,$06,$00,$0F,$00,$1B,$80,$1B,$80,$1B,$80,$1D,$80,$0F,$00,$06,$00,$06,$00,$07,$00
              byte  $00,$60,$03,$E0,$07,$C0,$03,$40,$03,$E0,$03,$C0,$01,$80,$03,$C0,$07,$E0,$07,$E0,$0F,$70,$0F,$B0,$03,$C0,$07,$60,$06,$E0,$07,$70
              byte  $00,$18,$00,$F8,$01,$F0,$00,$D0,$00,$F8,$00,$F0,$00,$60,$00,$F0,$01,$F8,$03,$FC,$07,$FE,$06,$F6,$00,$F8,$01,$DA,$03,$0E,$03,$84
        
              byte  $18,$00,$1F,$00,$0F,$80,$0B,$00,$1F,$00,$0F,$00,$06,$00,$0F,$00,$1F,$80,$3F,$C0,$7F,$E0,$6F,$60,$1F,$00,$5B,$80,$70,$C0,$21,$C0
              byte  $06,$00,$07,$C0,$03,$E0,$02,$C0,$07,$C0,$03,$C0,$01,$80,$03,$C0,$07,$E0,$07,$E0,$0E,$F0,$0D,$F0,$03,$C0,$06,$E0,$07,$60,$0E,$E0
              byte  $01,$80,$01,$F0,$00,$F8,$00,$B0,$01,$F0,$00,$F0,$00,$60,$00,$F0,$01,$F8,$01,$D8,$01,$D8,$01,$B8,$00,$F0,$00,$60,$00,$60,$00,$E0
              byte  $00,$60,$00,$7C,$00,$3E,$00,$2C,$00,$7C,$00,$3C,$00,$18,$00,$3C,$00,$7E,$00,$7E,$00,$EF,$00,$DF,$00,$3C,$00,$6E,$00,$76,$00,$EE

'                   B,B,R,M,G,C,Y,W
pickuppals    byte  0,0,0,6,3,4,5,0

airtext       byte  "AIR",0
hiscoretext   byte  "High Score",0,0
hiscore       byte  "000000",0,0
scoretext     byte  "Score",0,0,0
score         byte  "000000",0,0

sderrortext   byte  "SD-Fat Error.",0,0,0
levelfilename byte  "mmlev01.bin",0

add100        byte  "000100",0,0
add5          byte  "000005",0,0
add1          byte  "000001",0,0

gametext      byte  "game",0,0,0,0
overtext      byte  "over",0,0,0,0

titlemsg      byte  ".  .  .  .  .  ."
              byte  "  .  .  .  .  . "
              byte  "Original MANIC M"
              byte  "INER . .  BUG-B"
              byte  "YTE ltd. 1983 . "
              byte  ". By Matthew Smi"
              byte  "th  . . . Prop V"
              byte  "ersion  Jim Bag"
              byte  "ley using a modi"
              byte  "fied JLC_Spectru"
              byte  "m_TV_010.spin  ."
              byte  "  .  . Arrow Key"
              byte  "s to move Willy "
              byte  "Left and Right, "
              byte  "Space to Jump, M"
              byte  " to turn Tune On"
              byte  "/Off . . . G"
              byte  "uide Miner Willy"
              byte  " through 20 leth"
              byte  "al caverns .  . "
              byte  " .  .  .  .  .  "
              byte  ".",0,0,0
              