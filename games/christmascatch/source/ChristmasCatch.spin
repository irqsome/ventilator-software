{{
 Christmas Catch V1.1
┌────────────────────────────────────────────────────────────────────────────┐
│ MERRY CHISTAMS 2009 / CHRISMAS CARD/GAME BY JEFF LEDGER - OLDBITCOLLECTOR  │
│ =========================================================================  │
│                                                                            │
│    * Catch the falling presents with Santa's Sleigh as they fall.          │
│    * Consistant catches increases your rank.                               │
│                                                                            │
│   Using GM_Synth by: Andy Schenk                                           │
│   Using Improved 8x8 Video Driver by: Doug Dingus (potatohead)             │
│                                                                            │
│   I was going to do an animated Christmas card, but I thought why not      │
│   make a simple game instead?  We should do a Christmas game contest       │
│   next year.  Baggers will have to be the judge. :)                        │
│                                                                            │
│   Merry Christmas to all our forum & expo friends!  Jeff & Rebekah Ledger  │
└────────────────────────────────────────────────────────────────────────────┘
}}

  '' joypad serial shifter settings
  JOY_CLK = 16
  JOY_LCH = 17
  JOY_DATAOUT0 = 19
  JOY_DATAOUT1 = 18

  Left            = 10      '<-- Audio Out pins
  Right           = 11      '<-- Audio Out pins                                      

     ' NES bit encodings for NES gamepad 0
  NES0_RIGHT    = %00000000_00000001
  NES0_LEFT     = %00000000_00000010
  NES0_DOWN     = %00000000_00000100
  NES0_UP       = %00000000_00001000
  NES0_START    = %00000000_00010000
  NES0_SELECT   = %00000000_00100000
  NES0_B        = %00000000_01000000
  NES0_A        = %00000000_10000000

  
VAR

  word Nes_Pad
  word freq[4]
  byte tbuf[20]
  long stack[100]
   
  long  time, tq, ts, us
  long  ramptr
  
OBJ
        TEXT           : "Potato_Text_Start_RC3a.spin"                                  'TV Video Cog   
        rr             : "RealRandom" 
        synth          : "pm_synth_20"
        key            : "Keyboard"    
      
PUB main

    TEXT.Start(40)
    rr.start                        ' Start Random
    key.start(8,9)
    ChristmasCatch(0)

Pub   ChristmasCatch(gamesound) | {
} character,                     { Santa's Sleigh
} score,                         { Score
} misses,                        { How many missed gifts
} level,                         { Game Level
} skillmonitor,                  { Increase level when skillmonitor = 20
} gift1,                         { Falling gift 1 character (picked randomly)
} gift1x,                        { Gift 1 postion X  (across screen)
} gift1y,                        { Gift 1 position Y (down screen)                    
} gift1color,                    { Gift 1 color (picked randomly)
} gift1active,                   { Determins of gift 1 is still active 0=no
} gift1junkvar                   { A junk var for recalculating gift1
}
    ' Set inital Vars
      
      character:=10
      score:=0
      misses:=0
      level:=1
      skillmonitor:=0
      gift1active:=0
      
      
      TEXT.CharsPerLine(20)
      TEXT.ColorMode(0)
      TEXT.ClearText($20)
      TEXT.ClearColors((8*15)+2, $07) 
      TEXT.ColorMode(0)
      TEXT.PrintStringAt(0,0, string("x"))


      clearscreen
      Titlescreen
      
      clearscreen
      if gamesound == 0       'Start Background music if Sd was detected
         cognew(music,@stack)
         
      TEXT.SetColors(0,62) 'color red   
      text.printstringat(0,0,string(" abcdefg hijklmnopqr"))
      text.setcolors(0,50) 'color dark red    50
      text.printstringat(0,1,string(" ==================="))

      text.setcolors(0,7)
      text.printstringat(5,8,string("GET READY!"))
      repeat 600000  ' Sure, I could have used waitcnt, but why not? :)
      text.printstringat(5,8,string("          "))
      
      DrawSleigh(character) 
      text.setcolors(0,06)
      text.printstringat(1,22,string("RANK:"))
      'text.printstringat(14,22,string("MISS:"))
      
      repeat
      
         Nes_Pad := NES_Read_Gamepad

         if(Nes_Pad & NES0_RIGHT) and character < 18
            character++
            DrawSleigh(character)
            
         if(Nes_Pad & NES0_LEFT) and character > 1
            character--
            DrawSleigh(character)

         if (Nes_Pad & NES0_SELECT)
             gift1active:=0

         updatescoreboard(score,misses)
         
         if gift1active == 0
            gift1junkvar:=rr.random // 7 'pick a gift
            if gift1junkvar == 0
                gift1:=$00
            if gift1junkvar == 1
                gift1:=$01
            if gift1junkvar == 2
                gift1:=$02
            if gift1junkvar == 3
                gift1:=$03
            if gift1junkvar == 4
                gift1:=$06
            if gift1junkvar == 5
                gift1:=$07
            if gift1junkvar == 6
                gift1:=$08
            if gift1junkvar == 7
                gift1:=$09    

            gift1y:=4       ' Gift start position downscreen  
            gift1active:=1  ' Activate Gift

            '' Pick random X position of gift
            gift1x:=rr.random // 18 'pick an x location for gift to appear
            if gift1x == 0
               gift1x := 1

            '' Set Gift Color   
            gift1junkvar:=rr.random // 18 ' pick a random color for the gift
            if gift1junkvar == 0
               gift1color := 02
            if gift1junkvar == 1
               gift1color := 05
            if gift1junkvar == 2
               gift1color := 07                              
            if gift1junkvar == 3
               gift1color := 10
            if gift1junkvar == 4
               gift1color := 12
            if gift1junkvar == 5
               gift1color := 14
            if gift1junkvar == 6
               gift1color := 26
            if gift1junkvar == 7
               gift1color := 28
            if gift1junkvar == 8
               gift1color := 36
            if gift1junkvar == 9
               gift1color := 42                              
            if gift1junkvar == 10
               gift1color := 50
            if gift1junkvar == 11
               gift1color := 54
            if gift1junkvar == 12
               gift1color := 58
            if gift1junkvar == 13
               gift1color := 60
            if gift1junkvar == 14
               gift1color := 62
            if gift1junkvar == 15
               gift1color := 70
            if gift1junkvar == 16
               gift1color := 76
            if gift1junkvar == 17
               gift1color := 94
            if gift1junkvar == 18
               gift1color := 100
                           
         DrawGift(gift1,gift1color,gift1x,gift1y)

         '' Check to see if gift was caught.. working correctly.. *i think*
         
         gift1y++
         if gift1y == 20
            DrawGift($20,0,gift1x,gift1y)
            gift1active:=0
             DrawSleigh(character)
             '' Check to see if the gift was caught.
             if gift1x == character or gift1x == character+1 or gift1x == character-1 or gift1x == character+2 or gift1x == character -2 'Very forgiving game.
                score++
             
         waitcnt(cnt + 5_000_000) ''increase delay to slow the game a little

Pub UpdateScoreboard (scr,miss)

         '' Update scoreboard
         text.setcolors(0,06) 
         if scr > -1 and scr < 20
            text.printstringat(6,22,string("GRINCH"))
         if scr > 20 and scr < 30
            text.printstringat(6,22,string("FROSTY"))
         if scr > 30 and scr < 40
            text.printstringat(6,22,string("ELF   "))
         if scr > 40 and scr < 50
            text.printstringat(6,22,string("RUDOLF"))
         if scr > 59
            text.printstringat(6,22,string("SANTA "))

         {
         if miss == 0
            text.printstringat(19,22,string("0"))
         if miss == 1
            text.printstringat(19,22,string("1"))
         if miss == 2
            text.printstringat(19,22,string("2"))
         if miss == 3
            text.printstringat(19,22,string("3"))
         if miss == 4
            text.printstringat(19,22,string("4"))
         if miss == 5
            text.printstringat(19,22,string("5"))
         if miss == 6
            text.printstringat(19,22,string("6"))
         if miss == 7
            text.printstringat(19,22,string("7"))
         if miss == 8
            text.printstringat(19,22,string("8"))
         if miss == 9
            text.printstringat(19,22,string("9"))    }       
            
Pub DrawGift (toy,crayon,toyx,toyy)

    text.settextat(toyx,toyy-1,$20)
    text.setcolorcell(toyx,toyy,0,crayon) 
    text.settextat(toyx,toyy,toy) 
    
          
pub DrawSleigh (sleigh)


      text.settextat(sleigh,19,$20)
      text.settextat(sleigh-1,19,$20)
      text.settextat(sleigh+1,19,$20)
      text.settextat(sleigh,20,$20)
      text.settextat(sleigh-1,20,$20)
      text.settextat(sleigh+1,20,$20)
      text.settextat(sleigh+2,20,$20)  
      text.settextat(sleigh+2,19,$20)
      
      text.setcolorcell(sleigh,19,0,62)
      text.setcolorcell(sleigh+1,19,0,62)                             
      text.settextat(sleigh,19,$4) 'top left of sleigh
      text.settextat(sleigh+1,19,$5) 'top right of sleigh

      text.setcolorcell(sleigh,20,0,3)
      text.setcolorcell(sleigh+1,20,0,3)
      text.settextat(sleigh,20,$24) 'bottom left of sleigh
      text.settextat(sleigh+1,20,$25) 'bottom right of sleigh
          
pub Titlescreen

      text.setcolors(0,62) 
      text.printstringat(0,3,string("   CHRISTMAS CATCH"))
      text.setcolors(0,100)   ' 50
      text.printstringat(0,4,string("   ==============="))
      text.setcolors(0,12)
      text.printstringat(0,7,string("   BY: JEFF LEDGER"))
      text.setcolors(0,2)
      text.printstringat(0,12,string("  CATCH THE FALLING"))
      text.printstringat(0,13,string("  GIFTS WITH SANTAS"))
      text.printstringat(0,14,string("  SLEIGH."))
      text.setcolors(0,26)
      text.printstringat(0,20,string("  PRESS START ON NES"))

      repeat
         Nes_Pad := NES_Read_Gamepad
         if(Nes_Pad & NES0_START) 
            return
            
pub clearscreen
    repeat 500
      TEXT.SetColors(0, (0)+1)
      text.printchar($20)
    
PUB NES_Read_Gamepad : nes_bits        |  i
' //////////////////////////////////////////////////////////////////
' NES Game Paddle Read
' //////////////////////////////////////////////////////////////////       
' reads both gamepads in parallel encodes 8-bits for each in format
' right game pad #1 [15..8] : left game pad #0 [7..0]
'
' set I/O ports to proper direction
' P3 = JOY_CLK      (4)
' P4 = JOY_SH/LDn   (5) 
' P5 = JOY_DATAOUT0 (6)
' P6 = JOY_DATAOUT1 (7)
' NES Bit Encoding
'
' RIGHT  = %00000001
' LEFT   = %00000010
' DOWN   = %00000100
' UP     = %00001000
' START  = %00010000
' SELECT = %00100000
' B      = %01000000
' A      = %10000000

' step 1: set I/Os
DIRA [JOY_CLK] := 1 ' output
DIRA [JOY_LCH] := 1 ' output
DIRA [JOY_DATAOUT0] := 0 ' input
DIRA [JOY_DATAOUT1] := 0 ' input

' step 2: set clock and latch to 0
OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0
'Delay(1)

' step 3: set latch to 1
OUTA [JOY_LCH] := 1 ' JOY_SH/LDn = 1
'Delay(1)                            

' step 4: set latch to 0
OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0

' data is now ready to shift out, clear storage
nes_bits := 0

' step 5: read 8 bits, 1st bits are already latched and ready, simply save and clock remaining bits
repeat i from 0 to 7

 nes_bits := (nes_bits << 1)
 nes_bits := nes_bits | INA[JOY_DATAOUT0] | (INA[JOY_DATAOUT1] << 8)

 OUTA [JOY_CLK] := 1 ' JOY_CLK = 1
 'Delay(1)             
 OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
 
 'Delay(1)             

' invert bits to make positive logic
nes_bits := (!nes_bits & $FFFF)

{nes_bits|=NES0_START
if nes_bits&$ff==$ff
    nes_bits:=nes_bits&$ff00
        
if nes_bits&$ff00==$ff00
    nes_bits:=nes_bits&$ff}

' Keyboard (mapped onto NES buttons)
    if(key.keystate($C2))       'Up Arrow
      nes_bits|=NES0_UP
    if(key.keystate($C3))       'Down Arrow
      nes_bits|=NES0_DOWN
    if(key.keystate($C0))       'Left Arrow
      nes_bits|=NES0_LEFT
    if(key.keystate($C1))       'Right Arrow
      nes_bits|=NES0_RIGHT
    if(key.keystate($0D))       'Enter
      nes_bits|=NES0_START
    if(key.keystate(" "))       'Space
      nes_bits|=NES0_B

          
' //////////////////////////////////////////////////////////////////
' End NES Game Paddle Read
' //////////////////////////////////////////////////////////////////

' ////////////////////////////////////////////////////////////////////


    
PUB music

 
  synth.start(Left,Right,2)                             'start synth with 20 voices

 repeat
  waitcnt(clkfreq*2+cnt)
  midiplay(@midifile)                                   'play the file
  synth.allOff


PUB midiplay(pntr)  | lg, dt, b, v, st, d1, d2, ch
    ramptr := pntr
    us := clkfreq / 1_000_000                           'ticks / microsecond
    b := st := d1 := d2 := v := 0
    pread(@v,4)
    if v == "M" + "T"<<8 + "h"<<16 + "d"<<24            'Header ID
      pread(@v,4)                                       'Header Len
      v~
      pread(@v,2)                                       'Format
      if v <> 0
        'disp.str(string("only Format 0 supported"))
      else
        pread(@v,2)                                     'Tracks
        pread(@v,2)                                     'Ticks/Quarter
        tq := v>>8 + (v&255)<<8
        ts := 500_000 / tq                              'default Tempo 120 BPM
        pread(@v,4)                                     '"MTrk"
        pread(@v,4)                                     'Track length
        lg := v>>24 + (v>>16&255)<<8 + (v>>8&255)<<16
        time := cnt
        repeat
          pread(@b,1)                                   'get delta time
          lg--
          dt := b & $7F
          repeat while b>127
            pread(@b,1)
            lg--
            dt := dt<<7 + (b & $7F)
          time := time + dt*us*ts
          repeat until time-cnt < 0                     'wait deltatime
          b~
          pread(@b,1)                                   'MIDI byte
          if b > 127
            st := b                                     'new status byte
          elseif st==$FF
            st~
          ch := st & $0F
          d1~
          d2~
          case st & $F0                                 'decode MIDI Event
            $90: pread(@d1,1)                           'Note 
                 pread(@d2,1)                           'Velocity
                 lg -= 3
                 if d2>0
                   synth.noteOn(d1,ch,d2)               'Note On
                   show(15,ch+1,string(127),-1)
                 else
                   synth.noteOff(d1,ch)                 'Note Off if Vel=0
                   show(15,ch+1,string("."),-1)
            $80: pread(@d1,1)                           'Note Off 
                 pread(@d2,1)                           'Velocity
                 lg -= 3
                 synth.noteOff(d1,ch)
                 show(15,ch+1,string("."),-1)
                 if ch==9
                   show(20,ch+1,string(">"),d1)
            $A0: pread(@d1,1)                           'Poly AfterTouch 
                 pread(@d2,1)                           '(not supported)
                 lg -= 3
            $C0: pread(@d1,1)                           'Prg Change 
                 lg -= 2
                 synth.prgChange(d1,ch)
                 show(0,ch+1,string("Ch:"),ch+1)
                 show(6,ch+1,string("Prg:"),d1)
            $D0: pread(@d1,1)                           'Mono AfterTouch 
                 lg -= 2                                '(not supported)
            $B0: pread(@d1,1)                           'Controller (nr) 
                 pread(@d2,1)                           'value
                 lg -= 3
                 if d1==7
                   synth.volContr(d2,ch)                '7=Vol
            $E0: pread(@d1,1)                           'Pitch Bender 
                 pread(@d2,1)                           '(not supported!)
                 lg -= 3
            $F0: pread(@d1,1)                           'Meta Event
                 pread(@b,1)                            'Len
                 lg -= 2
                 v~
                 repeat while b>0                       'read meta data
                   pread(@d2,1)
                   v := v<<8 + d2
                   b--
                   lg--
                   if d1==5 and v&$80 > 0
                     quit
                 if d1==81                              'Tempo
                   ts := v / tq
        until lg < 1
   
    synth.allOff

PUB show(x,y,strp,val)                                  ''visualize MIDI events
  {
  if y<12
    disp.out(10)
    disp.out(x)
    disp.out(11)
    disp.out(y)
    disp.str(strp)
    if val=>0
      disp.dec(val)
    disp.out(" ")
   }
PUB pread(rptr,size)                                    ''Read RAM data
  repeat size
    byte[rptr++] := byte[ramptr++]
          
DAT
midifile        file    "ccatch1.mid"