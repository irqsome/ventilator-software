'Boulderdash, for Hydra or Demo Board.
'Version 0.96
'Released to the public domain.

' Changes:
'       2009-11-10       - Modified for the Demo Board.
'                        - Added 'readkey' routine to allow playing the game from the keyboard.
'                        - Rockford now can be moved around and the screen scrolls automatically.
'       2009-11-12       - Halved tile horizontal resolution to eliminate redundant pixel pairs
'                          (and BoulderDash uses 8x16 tiles anyway). This also halves tile memory
'                          requirements.
'                        - The scroller now runs in a separate cog.
'       2009-11-13       - Changed pixel clock settings in order to display 20 tiles per line,
'                          as in the classic C64/Atari BoulderDash I.
'                        - Added code to handle boulders and diamonds. Rockford cannot push yet
'                          boulders or pick-up diamonds.
'       2009-11-14       - Reversed pixel order in video driver in order to avoid having to define
'                          mirrored tiles.
'                        - Added more tiles, hopefully all the characters are there.
'                        - The scroller cog now also handles animated characters.
'                        - Rockford can pick-up diamonds and the screen flashes when diamond count
'                          reaches the target.
'                        - Rockford now walks facing in the right direction and gets "bored".
'       2009-11-15       - Added an optional status line.
'                        - Added a character font for the status line.
'                        - Main game loop moved to a separate routine.
'                        - Boulders can be pushed around.
'                        - New routines to cover and uncover the screen.
'                        - Added an infinite loop to the main routine, pressing ESC restarts the
'                          game.
'       2009-11-16       - Inbox now morphs into Rockford.
'                        - Rockford can exit through the Outbox.
'       2009-11-17       - Added hardware detection feature (Hydra, Demo Board, etc.) Video driver
'                          changed accordingly. Not sure if keyboard will work on Hydra.
'                        - Falling boulders and diamonds now can kill Rockford.
'                        - Added PAL timings table to video driver, but PAL mode is not working yet.
'       2009-11-18       - Synchronized scroller to vertical sync, scrolling is totally smooth now.
'                        - PAL mode now works.
'       2009-11-25       - Started adding support for fireflies and butterflies. Had to split the
'                          case statement in the main game loop into sections due to poor performance.
'                        - Added code to decode raw cave data of original Atari/C64 BoulderDash I.
'                          You can now import your favorite caves :)
'       2009-11-26       - Fireflies and butterflies now explode in contact with Rockford.
'                        - Time counts down, but there is no "out of time" condition yet.
'       2009-11-27       - Implemented the "pick without moving" Rockford trick (use left CTRL key).
'                        - Stop cave when time runs out.
'                        - Do not advance to next cave until current one is successfully completed.
'                          Game is now playable! Use LeftCtrl + LeftShift as a "cheat" to move to
'                          next cave without finishing the current one. The Escape key restarts the
'                          current cave. Sorry, no score points yet.
'       2009-11-28       - Status line now handled by a separate cog.
'                        - Flashing "Out of time" message when time runs out.
'                        - Space bar now pauses game.
'                        - Score points implemented.
'                        - No more unlimited lifes, sorry ;)))
'                        - New life every 500 points.
'       2009-11-29       - Using a table of codes to reset the 'scanned' flag avoids a second case
'                          statement and improves performance.
'                        - Implemented amoebas and magic wall. Now all the original caves work!
'                        - Added all the original BoulderDash I caves (except the intermissions)
'                        - Added a Level variable. After successfully finishing the last cave the
'                          game starts again from the first one of the next difficulty level.
'       2009-11-30       - Fixed a bug in amoeba handling that caused cave M to crash.
'       2009-12-02       - Game controller apparently does not like to be read too fast.
'                        - Added intermission caves.
'       2009-12-04       - Aborting the current cave will cost you a life.
'                        - Restart (or exit) automatically the cave if "Out of time" has been
'                          flashing for more than a minute without user input.
'                        - Throtle down Rockford a bit in levels 1 and 2 (is running too fast!)
'       2009-12-05       - Added the title screen (had a hard time scrolling the background behind
'                          those big letters!).
'                        - Starting cave and level can be selected from the title screen.
'                        - Run a demo if the title screen has been inactive for about a minute.
'       2009-12-06       - Fixed boulder/diamond rolling off: they must move to the side first,
'                          not directly diagonally.
'                        - Added a preliminary sound object. Nothing there yet.
'                        - Bonus points at the end of the cave increase with difficulty level.
'       2009-12-07       - First steps towards SID emulation via PWM. Single voice waveform generation
'                          already works (well, sort of). No envelope control yet.                          
'       2009-12-08       - SID emulator: noise waveform, amplitude control, 3 voices. Envelope
'                          control using a separate cog (see the ChangeLog in the SIDemu.spin file).
'       2009-12-09       - Integrated the SID emulator into the game, running out of cogs.
'                        - Got the main tune working.                        
'                        - Added a few other sound/noise effects the game.
'                        - Boulder noise interfers with diamond pick up sound. Which one has the
'                          preference?
'       2009-12-12       - Added the cover/uncover screen sound.
'                        - Added the bonus points sound.
'                        - Added amoeba and magic wall sound.
'
' TODO:
'  - Finish it :)
'  - Add broadcast TV mode with sound carrier to use the TV antenna input (like in old times :))

con

  ' This will allow us to use Coley's Hardware detection routine...
  _clkmode = XTAL1 + PLL16X
  _xinfreq = 5_000_000

  ' What kind of system are we dealing with.
  TYPE_DEMO   = 0
  TYPE_HYDRA  = 1
  TYPE_HYBRID = 2

  ' The supported video modes
  TV_NTSC = 0
  TV_PAL  = 1
  
  TV_MODE = TV_NTSC      ' <--- Select your TV system here -- NTSC or PAL ---<<<

  ' Game controller codes
  GP_RIGHT  = %00000001         '(Right arrow)  Move right
  GP_LEFT   = %00000010         '(Left arrow)   Move left
  GP_DOWN   = %00000100         '(Down arrow)   Move down
  GP_UP     = %00001000         '(Up arrow)     Move up
  GP_START  = %00010000         '(Left Shift)   Start game. Together with GP_SELECT: go to next cave (cheat)
  GP_SELECT = %00100000         '(Left Ctrl)    Pick
  GP_B      = %01000000         '(Space)        Pause
  GP_A      = %10000000         '(Escape)       Abort and restart current cave
        
var

  long nes
  long last_dir
  long cave_addr
  long random_ptr
  long screen_height
  long sx, sy
  long video_params[7]
  long Score
  long NextLife
  byte temp_cave[40*22]
  'byte HWType
  byte Cave
  byte Level
  byte CaveNumber
  byte DiamondCount
  byte DiamondsNeeded
  byte DiamondValue
  byte ExtraValue
  byte CaveTime
  byte Men
  byte Alive
  byte MagicWallStatus
  byte MagicWallTime
  byte AmoebaStatus
  byte AmoebaTime
  byte target
  byte vsync
  byte door_x, door_y
              
obj

  tv: "Boulderdash_Tile_TV"
  kb: "Keyboard"
  sc: "Scroller"
  st: "Status"
  rr: "RealRandom"
  gc: "Game_controller"
  sn: "Sounds"

pub main | success

  ' Detect hardware and set clock frequency
  video_params[0] := %0_11_101_000   ' Set Mode for VCFG
  video_params[1] := 1               ' Set Pingroup used by TV driver.
  video_params[2] := %0111_0000      ' Set Pinmask for VCFG pins.
  video_params[3] := %0000_0111<<12  ' Set Pinmask for output pins.                

  video_params[4] := TV_MODE
  video_params[5] := @screen
  video_params[6] := @vsync

  if video_params[4] == TV_NTSC
    screen_height := 12
  else
    screen_height := 14

  rr.start                               'start the real random number generator
  random_ptr := rr.random_ptr

  bytefill(@screen, $00, constant(40 * 22))  'clear screen

  tv.start(@video_params)                'start the TV driver
  sc.start(@video_params, random_ptr)    'start the scroller
  st.start(@video_params)                'start the status line handler
  kb.start(8, 9)                         'start the keyboard driver           
  sn.start(@video_params, 11, 10, random_ptr) 'start the sound engine

  cave_addr := @screen

  repeat

    repeat
      success := TitleScreen

      palette.byte[1] := $04
      palette.byte[2] := $4B
      palette.byte[3] := $06
      bytefill(@screen, $3C, constant(40 * 22)) 'fill the screen with the scrolling pattern
      status := 0

      Score := 0
      Men := 3

      if success
        quit
      else
        Demo

    repeat

      CaveNumber := byte[@@caves[Cave]]
      st.player_params(1, Men, CaveNumber, Level, Score)
      st.set_mode(st#STATUS_PRE)

      CreateCave(@@caves[Cave], @temp_cave)
      st.cave_params(DiamondsNeeded, DiamondValue, CaveTime)

      last_dir := -1    'last rockford direction
      sc.rockford_go(0) 'rockford initially stands still
      sc.milling_off

      if CaveNumber > 16
        'intermission
        sx := 0
        sy := 0
      else
        'normal cave
        sx := door_x - 10
        sx <#= constant(40 - 20)
        sx #>= 0
        sy := door_y - screen_height / 2
        sy <#= 22 - screen_height
        sy #>= 0
      sc.scroll_to(sx * 8, sy * 16)

      Uncover           'uncover screen
      success := GameLoop
      
      if not success
        if --Men == 0
          st.set_mode(st#STATUS_GAME_OVER)
          waitcnt(clkfreq + cnt)
          Cover
          quit

      if success or CaveNumber > 16
        'go to next cave only if current one was completed
        'but never restart intermissions
        if success and CaveNumber > 16 and Men < 9
          ++Men  'the promised bonus life 
        if caves[++Cave] == 0
          if Level < 4
            ++Level
          Cave := 0

      st.player_params(1, Men, CaveNumber, Level, Score)
      st.set_mode(st#STATUS_PRE)
      Cover             'cover screen with scrolling pattern

pub TitleScreen | x, y, h, t, i, j, lk, ticks, count

  if TV_MODE == TV_NTSC
    h := 24
    t := 2
    ticks := constant(60 * 40 / 2)
  else
    h := 28
    t := 4
    ticks := constant(50 * 40 / 2)

  bytefill(@screen, $00, constant(40 * 28))  'clear screen
  palette.byte[1] := $FC 
  palette.byte[2] := $FB 
  palette.byte[3] := $06
  st.set_mode(st#STATUS_NONE)

  Cave := 0
  Level := 0
  CaveNumber := byte[@@caves[Cave]]

  repeat x from 0 to 19
    byte[@screen + x] := $B1
    byte[@screen + x + (h - 6) * 40] := $B1
  repeat y from 1 to h - 7
    byte[@screen + y * 40] := $B5
    repeat x from 1 to 18
      byte[@screen + x + y * 40] := $B0
    byte[@screen + 19 + y * 40] := $B6

  i := 0
  repeat y from t to t + 6
    repeat x from 1 to 18
      byte[@screen + x + y * 40] := big_boulder.byte[i++]

  i := 0
  repeat y from t + 8 to t + 14
    repeat x from 3 to 15
      byte[@screen + x + y * 40] := big_dash.byte[i++]

  status := 2
  scroll := 0
  'st.text_out(@screen + (h - 4) * 40, @str1, 20)
  repeat i from 0 to 19
    j := str1.byte[i]
    if j == $20 
      byte[@screen + (h - 4) * 40 + i] := $90
    else
      byte[@screen + (h - 4) * 40 + i] := j + $A0
  st.text_out(@screen + (h - 3) * 40, @str2, 20)
  st.text_out(@screen + (h - 2) * 40, @str3, 20)
  st.text_out(@screen + (h - 1) * 40, @str4, 20)

  screen.byte[(h - 2) * 40] := $D1  'player(s)
  screen.byte[(h - 2) * 40 + 10] := $D1  'joystick(s)
  screen.byte[(h - 1) * 40 + 7] := CaveNumber + $E0
  screen.byte[(h - 1) * 40 + 17] := Level + $D1

  lk := 0  'last key
  count := ticks

  sn.music_on
  
  repeat while count > 0

    'wait for vsync, count every other tick
    repeat while vsync == 0
    repeat while vsync <> 0
    repeat while vsync == 0
    repeat while vsync <> 0
    --count

    ' scrolling background
    x := @tiles + constant(16 * $B0)
    y := word[x]
    wordmove(x, x + 2, 7)
    word[x + 15] := y

    repeat i from 0 to 5
      repeat y from 0 to 14 step 2
        x := @tiles + constant(16 * $B0) + y
        t := @tiles + constant(16 * $BD) + y + 16 * i
        j := @tiles + constant(16 * $B3) + y + 16 * i
        word[j] := word[x] | word[t]
    repeat y from 0 to 14 step 2
      x := @tiles + constant(16 * $B0) + y
      t := @tiles + constant(16 * $C3) + y
      j := @tiles + constant(16 * $B9) + y
      word[j] := (word[x] & $00FF) | word[t]
    repeat y from 0 to 14 step 2
      x := @tiles + constant(16 * $B0) + y
      t := @tiles + constant(16 * $C4) + y
      j := @tiles + constant(16 * $BA) + y
      word[j] := (word[x] & $FF00) | word[t]
    repeat y from 0 to 14 step 2
      x := @tiles + constant(16 * $B0) + y
      t := @tiles + constant(16 * $C5) + y
      j := @tiles + constant(16 * $BB) + y
      word[j] := (word[x] & $FF00) | word[t]

    i := read_input
    if i <> lk
      count := ticks  'reset countdown
      if i & GP_START
        sn.music_off
        return 1
      elseif i & GP_LEFT
        if Cave => 5
          Cave -= 5
        CaveNumber := byte[@@caves[Cave]]
        screen.byte[(h - 1) * 40 + 7] := CaveNumber + $E0
      elseif i & GP_RIGHT
        if Cave =< 10
          Cave += 5
        CaveNumber := byte[@@caves[Cave]]
        screen.byte[(h - 1) * 40 + 7] := CaveNumber + $E0
      elseif i & GP_UP
        if Level < 4
          ++Level
        screen.byte[(h - 1) * 40 + 17] := Level + $D1
      elseif i & GP_DOWN
        if Level > 0
          --Level
        screen.byte[(h - 1) * 40 + 17] := Level + $D1
      lk := i

  sn.music_off
  return 0
    
dat

big_boulder
        byte  $B2, $B1, $B7, $B0, $B0, $B0, $B0, $B0, $B0, $B0, $B0, $B0, $B0, $B0, $B0, $B0, $B0, $B0
        byte  $B2, $B0, $B9, $B2, $B1, $BA, $B5, $B9, $B2, $B0, $BA, $B1, $B7, $B2, $B1, $BA, $B1, $B7
        byte  $B2, $B1, $B8, $B2, $BA, $BA, $B5, $B9, $B2, $B0, $BA, $B5, $B9, $B2, $B0, $BA, $B5, $B9
        byte  $B2, $B1, $B7, $B2, $BA, $BA, $B5, $B9, $B2, $B0, $BA, $B5, $B9, $B2, $B5, $BA, $B1, $B8
        byte  $B2, $B0, $B9, $B2, $BA, $BA, $B5, $B9, $B2, $B0, $BA, $B5, $B9, $B2, $B0, $BA, $B1, $B7
        byte  $B2, $B1, $B5, $B2, $B1, $BA, $B1, $B5, $B2, $B1, $BA, $B1, $B5, $B2, $B1, $BA, $B5, $B9
        byte  $B2, $B1, $B8, $B2, $B1, $BA, $B1, $B5, $B2, $B1, $BA, $B1, $B8, $B2, $B1, $BA, $B5, $B9

big_dash
        byte  $BA, $B1, $B3, $B0, $B0, $B0, $B0, $B0, $B0, $B0, $B0, $B0, $B0
        byte  $BA, $B5, $BA, $B0, $B2, $B7, $B2, $B1, $BA, $B5, $BA, $BB, $BC
        byte  $BA, $B5, $BA, $BA, $B5, $B9, $B2, $B0, $BA, $B5, $BA, $B0, $B0
        byte  $BA, $B5, $BA, $BA, $B1, $B5, $B2, $B1, $BA, $B1, $B1, $B0, $B0
        byte  $BA, $B5, $BA, $BA, $B5, $B9, $B0, $B2, $BA, $B5, $BA, $B0, $B0
        byte  $BA, $B1, $B1, $BA, $B5, $B9, $B2, $B1, $BA, $B5, $BA, $B0, $B0
        byte  $BA, $B1, $B4, $BA, $B5, $B9, $B2, $B1, $BA, $B5, $BA, $B0, $B0

str1    byte  " PROPELLER VERSION  "
str2    byte  "PRESS BUTTON TO PLAY"
str3    byte  "  PLAYER    JOYSTICK"
str4    byte  " CAVE:    LEVEL:    "

pub Demo

  Score := 0
  Cave := 0             'demo happens on cave A
  Level := 0
  CaveNumber := byte[@@caves[Cave]]
  st.player_params(1, Men, CaveNumber, Level, Score)
  st.set_mode(st#STATUS_PRE)

  CreateCave(@@caves[Cave], @temp_cave)
  st.cave_params(DiamondsNeeded, DiamondValue, CaveTime)

  last_dir := -1        'last rockford direction
  sc.rockford_go(0)     'rockford initially stands still
  sc.milling_off

  sx := door_x - 10
  sx <#= constant(40 - 20)
  sx #>= 0
  sy := door_y - screen_height / 2
  sy <#= 22 - screen_height
  sy #>= 0
  sc.scroll_to(sx * 8, sy * 16)

  demo_ptr := @demo_data 'set demo data pointer
  kcount := 0

  Uncover                'uncover screen
  GameLoop               'play the demo
  Cover                  'cover screen back

var

  long demo_ptr
  byte kcount, last_key

dat 
       
' The low nibble of each byte is the direction of movement:
'   $x0 = no movement
'   $x1 = Right
'   $x2 = Left
'   $x3 = Down
'   $x4 = Up
'   bit 3 ($x8) set means GP_SELECT is pressed (not used here anyway)
' The high nibble is the number of scan frames to apply the movement.
' $00 means end of demo data.

demo_data
        byte  $F0, $10, $14, $71, $23, $91, $40, $23
        byte  $41, $34, $12, $40, $14, $B1, $13, $21
        byte  $40, $63, $11, $43, $32, $40, $13, $12
        byte  $41, $32, $40, $44, $52, $34, $52, $43
        byte  $32, $50, $34, $A2, $14, $32, $13, $62
        byte  $43, $11, $40, $33, $41, $43, $42, $24
        byte  $21, $34, $A1, $A1, $13, $41, $13, $41
        byte  $23, $50, $51, $44, $51, $60, $13, $00

pub readkey : key

  if demo_ptr
    if kcount > 0
      --kcount
      return last_key
    else
      key := byte[demo_ptr++]
      if key == $00
        demo_ptr := 0
      else
        kcount := key / 16 - 1
        case key & $07
          $0: last_key := 0
          $1: last_key := GP_RIGHT
          $2: last_key := GP_LEFT
          $3: last_key := GP_DOWN
          $4: last_key := GP_UP
        if key & $08
          last_key |= GP_SELECT  
        return last_key

  key := 0
  if kb.keystate($C0)
    key |= GP_LEFT
  if kb.keystate($C1)
    key |= GP_RIGHT
  if kb.keystate($C2)
    key |= GP_UP
  if kb.keystate($C3)
    key |= GP_DOWN
  if kb.keystate($F2)  'left control key
    key |= GP_SELECT
  if kb.keystate($CB)  'escape key
    key |= GP_A
  if kb.keystate($20)  'space key
    key |= GP_B
  if kb.keystate($F0)  'left shift
    key |= GP_START
  if kb.keystate($0D)  'or enter
    key |= GP_START

pub read_input

  return gc.read | readkey

var
  byte RandSeed1, RandSeed2
  
pub GetRandom | temp1, temp2, temp3, cy, r

'' Generate a pseudo-random number, given a seed
'' The output matches the one used in the original BoulderDash I

  temp1 := (RandSeed1 & 1) << 7
  temp2 := (RandSeed2 & 1) << 7
  temp3 := (RandSeed2 >> 1) & $7F

  r := RandSeed2 + temp2
  if r > $FF
    cy := 1
    r &= $FF
  else
    cy := 0

  r += cy + $13
  if r > $FF
    cy := 1
    r &= $FF
  else
    cy := 0

  RandSeed2 := r

  r := RandSeed1 + cy + temp1
  if r > $FF
    cy := 1
    r &= $FF
  else
    cy := 0

  r += cy + temp3
  RandSeed1 := r & $FF

dat

caves   long  @cave_A, @cave_B, @cave_C, @cave_D, @inter_1
        long  @cave_E, @cave_F, @cave_G, @cave_H, @inter_2
        long  @cave_I, @cave_J, @cave_K, @cave_L, @inter_3
        long  @cave_M, @cave_N, @cave_O, @cave_P, @inter_4
        long  0

cave_A  byte  1                                 'cave number
        byte  20                                'magic wall/amoeba time
        byte  10                                'initial diamond value
        byte  15                                'extra diamond value
        byte  10, 11, 12, 13, 14                'randomizer seed per level
        byte  12, 12, 12, 12, 12                'diamonds needed per level
        byte  150, 110, 70, 40, 30              'time per level
        byte  $08, $0B, $09, $D4, $20           'bg color 1, bg color 2, fg color
        byte  $00, $10, $14, $00                'random objects
        byte  $3C, $32, $09, $00                'probability of object
        byte  $42, $01, $09, $1E, $02           'raw cave data
        byte  $42, $09, $10, $1E, $02
        byte  $25, $03, $04
        byte  $04, $26, $12
        byte  $FF

cave_B  byte  2
        byte  20
        byte  20, 50
        byte  $03, $00, $01, $57, $58
        byte  $0A, $0C, $09, $0D, $0A
        byte  $96, $6E, $46, $46, $46
        byte  $0A, $04, $09, $00, $00
        byte  $00, $10, $14, $08
        byte  $3C, $32, $09, $02
        byte  $42, $01, $08, $26, $02, $42, $01, $0F, $26, $02, $42, $08, $03, $14, $04, $42
        byte  $10, $03, $14, $04, $42, $18, $03, $14, $04, $42, $20, $03, $14, $04, $40, $01
        byte  $05, $26, $02, $40, $01, $0B, $26, $02, $40, $01, $12, $26, $02, $40, $14, $03
        byte  $14, $04, $25, $12, $15, $04, $12, $16, $FF

cave_C  byte  3
        byte  $00
        byte  $0F, $00
        byte  $00, $32, $36, $34, $37
        byte  $18, $17, $18, $17, $15
        byte  $96, $64, $5A, $50, $46
        byte  $09, $08, $09, $04, $00
        byte  $02, $10, $14, $00
        byte  $64, $32, $09, $00
        byte  $25, $03, $04, $04, $27, $14, $FF

cave_D  byte  4
        byte  $14
        byte  $05, $14
        byte  $00, $6E, $70, $73, $77
        byte  $24, $24, $24, $24, $24
        byte  $78, $64, $50, $3C, $32
        byte  $04, $08, $09, $00, $00
        byte  $10, $00, $00, $00
        byte  $14, $00, $00, $00
        byte  $25, $01, $03, $04, $26, $16, $81, $08, $0A, $04, $04, $00, $30, $0A, $0B, $81
        byte  $10, $0A, $04, $04, $00, $30, $12, $0B, $81, $18, $0A, $04, $04, $00, $30, $1A
        byte  $0B, $81, $20, $0A, $04, $04, $00, $30, $22, $0B, $FF

cave_E  byte  $05, $14, $32, $5A
        byte  $00, $00, $00, $00, $00
        byte  $04, $05, $06, $07, $08
        byte  $96, $78, $5A, $3C, $1E
        byte  $09, $0A, $09, $00, $00
        byte  $00, $00, $00, $00
        byte  $00, $00, $00, $00
        byte  $25, $01, $03, $04, $27, $16, $80, $08, $0A, $03, $03, $00, $80, $10, $0A, $03
        byte  $03, $00, $80, $18, $0A, $03, $03, $00, $80, $20, $0A, $03, $03, $00, $14, $09
        byte  $0C, $08, $0A, $0A, $14, $11, $0C, $08, $12, $0A, $14, $19, $0C, $08, $1A, $0A
        byte  $14, $21, $0C, $08, $22, $0A, $80, $08, $10, $03, $03, $00, $80, $10, $10, $03
        byte  $03, $00, $80, $18, $10, $03, $03, $00, $80, $20, $10, $03, $03, $00, $14, $09
        byte  $12, $08, $0A, $10, $14, $11, $12, $08, $12, $10, $14, $19, $12, $08, $1A, $10
        byte  $14, $21, $12, $08, $22, $10, $FF

cave_F  byte  $06, $14, $28, $3C
        byte  $00, $14, $15, $16, $17
        byte  $04, $06, $07, $08, $08
        byte  $96, $78, $64, $5A, $50
        byte  $0E, $0A, $09, $00, $00
        byte  $10, $00, $00, $00
        byte  $32, $00, $00, $00
        byte  $82, $01, $03, $0A, $04, $00, $82, $01, $06, $0A, $04, $00, $82, $01, $09, $0A
        byte  $04, $00, $82, $01, $0C, $0A, $04, $00, $41, $0A, $03, $0D, $04, $14, $03, $05
        byte  $08, $04, $05, $14, $03, $08, $08, $04, $08, $14, $03, $0B, $08, $04, $0B, $14
        byte  $03, $0E, $08, $04, $0E, $82, $1D, $03, $0A, $04, $00, $82, $1D, $06, $0A, $04
        byte  $00, $82, $1D, $09, $0A, $04, $00, $82, $1D, $0C, $0A, $04, $00, $41, $1D, $03
        byte  $0D, $04, $14, $24, $05, $08, $23, $05, $14, $24, $08, $08, $23, $08, $14, $24
        byte  $0B, $08, $23, $0B, $14, $24, $0E, $08, $23, $0E, $25, $03, $14, $04, $26, $14
        byte  $FF

cave_G  byte  $07, $4B, $0A, $14
        byte  $02, $07, $08, $0A, $09
        byte  $0F, $14, $19, $19, $19
        byte  $78, $78, $78, $78, $78
        byte  $09, $0A, $0D, $00, $00
        byte  $00, $10, $08, $00
        byte  $64, $28, $02, $00
        byte  $42, $01, $07, $0C, $02, $42, $1C, $05, $0B, $02, $7A, $13, $15, $02, $02, $14
        byte  $04, $06, $14, $04, $0E, $14, $04, $16, $14, $22, $04, $14, $22, $0C, $14, $22
        byte  $16, $25, $14, $03, $04, $27, $07, $FF

cave_H  byte  $08, $14, $0A, $14
        byte  $01, $03, $04, $05, $06
        byte  $0A, $0F, $14, $14, $14
        byte  $78, $6E, $64, $5A, $50
        byte  $02, $0E, $09, $00, $00
        byte  $00, $10, $08, $00
        byte  $5A, $32, $02, $00
        byte  $14, $04, $06, $14, $22, $04, $14, $22, $0C, $04, $00, $05, $25, $14, $03, $42
        byte  $01, $07, $0C, $02, $42, $01, $0F, $0C, $02, $42, $1C, $05, $0B, $02, $42, $1C
        byte  $0D, $0B, $02, $43, $0E, $11, $08, $02, $14, $0C, $10, $00, $0E, $12, $14, $13
        byte  $12, $41, $0E, $0F, $08, $02, $FF

cave_I  byte  $09, $14, $05, $0A
        byte  $64, $89, $8C, $FB, $33
        byte  $4B, $4B, $50, $55, $5A
        byte  $96, $96, $82, $82, $78
        byte  $08, $04, $09, $00, $00
        byte  $10, $14, $00, $00
        byte  $F0, $78, $00, $00
        byte  $82, $05, $0A, $0D, $0D, $00, $01, $0C, $0A, $82, $19, $0A, $0D, $0D, $00, $01
        byte  $1F, $0A, $42, $11, $12, $09, $02, $40, $11, $13, $09, $02, $25, $07, $0C, $04
        byte  $08, $0C, $FF

cave_J  byte  $0A, $14, $19, $3C
        byte  $00, $00, $00, $00, $00
        byte  $0C, $0C, $0C, $0C, $0C
        byte  $96, $82, $78, $6E, $64
        byte  $06, $08, $09, $00, $00
        byte  $00, $00, $00, $00
        byte  $00, $00, $00, $00
        byte  $25, $0D, $03, $04, $27, $16, $54, $05, $04, $11, $03, $54, $15, $04, $11, $05
        byte  $80, $05, $0B, $11, $03, $08, $C2, $01, $04, $15, $11, $00, $0D, $04, $C2, $07
        byte  $06, $0D, $0D, $00, $0D, $06, $C2, $09, $08, $09, $09, $00, $0D, $08, $C2, $0B
        byte  $0A, $05, $05, $00, $0D, $0A, $82, $03, $06, $03, $0F, $08, $00, $04, $06, $54
        byte  $04, $10, $04, $04, $FF

cave_K  byte  $0B, $14, $32, $00
        byte  $00, $04, $66, $97, $64
        byte  $06, $06, $06, $06, $06
        byte  $78, $78, $96, $96, $F0
        byte  $0B, $08, $09, $00, $00
        byte  $00, $10, $08, $00
        byte  $64, $50, $02, $00
        byte  $42, $0A, $03, $09, $04, $42, $14, $03, $09, $04, $42, $1E, $03, $09, $04, $42
        byte  $09, $16, $09, $00, $42, $0C, $0F, $11, $02, $42, $05, $0B, $09, $02, $42, $0F
        byte  $0B, $09, $02, $42, $19, $0B, $09, $02, $42, $1C, $13, $0B, $01, $14, $04, $03
        byte  $14, $0E, $03, $14, $18, $03, $14, $22, $03, $14, $04, $16, $14, $23, $15, $25
        byte  $14, $14, $04, $26, $11, $FF

cave_L  byte  $0C, $14, $14, $00
        byte  $00, $3C, $02, $3B, $66
        byte  $13, $13, $0E, $10, $15
        byte  $B4, $AA, $A0, $A0, $A0
        byte  $0C, $0A, $09, $00, $00
        byte  $00, $10, $14, $00
        byte  $3C, $32, $09, $00
        byte  $42, $0A, $05, $12, $04, $42, $0E, $05, $12, $04, $42, $12, $05, $12, $04, $42
        byte  $16, $05, $12, $04, $42, $02, $06, $0B, $02, $42, $02, $0A, $0B, $02, $42, $02
        byte  $0E, $0F, $02, $42, $02, $12, $0B, $02, $81, $1E, $04, $04, $04, $00, $08, $20
        byte  $05, $81, $1E, $09, $04, $04, $00, $08, $20, $0A, $81, $1E, $0E, $04, $04, $00
        byte  $08, $20, $0F, $25, $03, $14, $04, $27, $16, $FF

cave_M  byte  $0D, $8C, $05, $08
        byte  $00, $01, $02, $03, $04
        byte  $32, $37, $3C, $46, $50
        byte  $A0, $9B, $96, $91, $8C
        byte  $06, $08, $0D, $00, $00
        byte  $10, $00, $00, $00
        byte  $28, $00, $00, $00
        byte  $25, $12, $03, $04, $0A, $03, $3A, $14, $03, $42, $05, $12, $1E, $02, $70, $05
        byte  $13, $1E, $02, $50, $05, $14, $1E, $02, $C1, $05, $15, $1E, $02, $FF

cave_N  byte  $0E, $14, $0A, $14
        byte  $00, $00, $00, $00, $00
        byte  $1E, $23, $28, $2A, $2D
        byte  $96, $91, $8C, $87, $82
        byte  $0C, $08, $09, $00, $00
        byte  $10, $00, $00, $00
        byte  $00, $00, $00, $00
        byte  $81, $0A, $0A, $0D, $0D, $00, $70, $0B, $0B, $0C, $03, $C1, $0C, $0A, $03, $0D
        byte  $C1, $10, $0A, $03, $0D, $C1, $14, $0A, $03, $0D, $50, $16, $08, $0C, $02, $48
        byte  $16, $07, $0C, $02, $C1, $17, $06, $03, $04, $C1, $1B, $06, $03, $04, $C1, $1F
        byte  $06, $03, $04, $25, $03, $03, $04, $27, $14, $FF

cave_O  byte  $0F, $08, $0A, $14
        byte  $01, $1D, $1E, $1F, $20
        byte  $0F, $14, $14, $19, $1E
        byte  $78, $78, $78, $78, $8C
        byte  $08, $0E, $09, $00, $00
        byte  $00, $10, $08, $00
        byte  $64, $50, $02, $00
        byte  $42, $02, $04, $0A, $03, $42, $0F, $0D, $0A, $01, $41, $0C, $0E, $03, $02, $43
        byte  $0C, $0F, $03, $02, $04, $14, $16, $25, $14, $03, $FF

cave_P  byte  $10, $14, $0A, $14
        byte  $01, $78, $81, $7E, $7B
        byte  $0C, $0F, $0F, $0F, $0C
        byte  $96, $96, $96, $96, $96
        byte  $09, $0A, $09, $00, $00
        byte  $10, $00, $00, $00
        byte  $32, $00, $00, $00
        byte  $25, $01, $03, $04, $27, $04, $81, $08, $13, $04, $04, $00, $08, $0A, $14, $C2
        byte  $07, $0A, $06, $08, $43, $07, $0A, $06, $02, $81, $10, $13, $04, $04, $00, $08
        byte  $12, $14, $C2, $0F, $0A, $06, $08, $43, $0F, $0A, $06, $02, $81, $18, $13, $04
        byte  $04, $00, $08, $1A, $14, $81, $20, $13, $04, $04, $00, $08, $22, $14, $FF

inter_1 byte  $11, $14, $1E, $00
        byte  $0A, $0B, $0C, $0D, $0E
        byte  $06, $06, $06, $06, $06
        byte  $0A, $0A, $0A, $0A, $0A
        byte  $0E, $02, $09, $00, $00
        byte  $00, $14, $00, $00
        byte  $FF, $09, $00, $00
        byte  $87, $00, $02, $28, $16, $07, $87, $00, $02, $14, $0C, $00, $32, $0A, $0C, $10
        byte  $0A, $04, $01, $0A, $05, $25, $03, $05, $04, $12, $0C, $FF

inter_2 byte  $12, $14, $0A, $00
        byte  $0A, $0B, $0C, $0D, $0E
        byte  $10, $10, $10, $10, $10
        byte  $0F, $0F, $0F, $0F, $0F
        byte  $06, $0F, $09, $00, $00
        byte  $00, $00, $00, $00
        byte  $00, $00, $00, $00
        byte  $87, $00, $02, $28, $16, $07, $87, $00, $02, $14, $0C, $01, $50, $01, $03, $09
        byte  $03, $48, $02, $03, $08, $03, $54, $01, $05, $08, $03, $50, $01, $06, $07, $03
        byte  $50, $12, $03, $09, $05, $54, $12, $05, $08, $05, $50, $12, $06, $07, $05, $25
        byte  $01, $04, $04, $12, $04, $FF

inter_3 byte  $13, $04, $0A, $00
        byte  $0A, $0B, $0C, $0D, $0E
        byte  $0E, $0E, $0E, $0E, $0E
        byte  $14, $14, $14, $14, $14
        byte  $06, $08, $09, $00, $00
        byte  $00, $00, $00, $00
        byte  $00, $00, $00, $00
        byte  $87, $00, $02, $28, $16, $07, $87, $00, $02, $14, $0C, $00, $54, $01, $0C, $12
        byte  $02, $88, $0F, $09, $04, $04, $08, $25, $08, $03, $04, $12, $07, $FF

inter_4 byte  $14, $03, $1E, $00
        byte  $00, $00, $00, $00, $00
        byte  $06, $06, $06, $06, $06
        byte  $14, $14, $14, $14, $14
        byte  $06, $08, $09, $00, $00
        byte  $00, $00, $00, $00
        byte  $00, $00, $00, $00
        byte  $87, $00, $02, $28, $16, $07, $87, $00, $02, $14, $0C, $01, $D0, $0B, $03, $03
        byte  $02, $80, $0B, $07, $03, $06, $00, $43, $0B, $06, $03, $02, $43, $0B, $0A, $03
        byte  $02, $50, $08, $07, $03, $03, $25, $03, $03, $04, $09, $0A, $FF

dat

' C64 color table approximation. Not the most accurate one, but gives the desired effect. 

ctable  byte  $02, $06, $4B, $BC,  $2B, $9C, $EB, $7D
        byte  $4B, $4A, $4B, $04,  $04, $9D, $0B, $04

pub CreateCave(raw_cave, dest) | i, j, k, cx, cy, ci, co, dir, len

'' Create cave contents from the description array

  CaveNumber := byte[raw_cave]

  RandSeed1 := 0
  RandSeed2 := byte[raw_cave + 4 + Level]

  ' Set colors
  palette.byte[1] := ctable.byte[byte[raw_cave + 20]] 
  palette.byte[2] := ctable.byte[byte[raw_cave + 19]] 
  palette.byte[3] := ctable.byte[byte[raw_cave + 21] - 8] 

  ' Place random objects
  repeat cy from 1 to 21
    repeat cx from 0 to 39
      co := $01  ' dirt
      GetRandom
      repeat ci from 0 to 3
        if RandSeed1 < byte[raw_cave + 28 + ci]
          co := byte[raw_cave + 24 + ci] 
      byte[dest + cx + cy * 40] := co

  ' Steel bounds
  repeat cx from 0 to 39
    byte[dest + cx] := $07
    byte[dest + cx + 21 * 40] := $07
  repeat cy from 0 to 21
    byte[dest + cy * 40] := $07
    byte[dest + 39 + cy * 40] := $07

  'Decode raw data
  i := raw_cave + 32
  repeat
    ci := byte[i++]
    cx := byte[i++]
    cy := byte[i++] - 2
    if ci == $FF
      quit
    case ci & $C0
      $00:
        byte[dest + cx + cy * 40] := ci & $3F
        if (ci & $3F) == $25
          door_x := cx
          door_y := cy
      $40:
        len := byte[i++]
        dir := byte[i++]
        repeat
          byte[dest + cx + cy * 40] := ci & $3F
          case dir
            0: --cy
            1: --cy
               ++cx
            2: ++cx
            3: ++cy
               ++cx
            4: ++cy
            5: ++cy
               --cx
            6: --cx
            7: --cy
               --cx            
        while --len > 0
      $80:
        len := byte[i++] - 1  'width
        dir := byte[i++] - 1  'height
        co := byte[i++]
        repeat j from 0 to len
          byte[dest + cx + j + cy * 40] := ci & $3F
          byte[dest + cx + j + (cy + dir) * 40] := ci & $3F
        repeat j from 1 to dir - 1
          byte[dest + cx + (cy + j) * 40] := ci & $3F
          repeat k from 1 to len - 1
            byte[dest + cx + k + (cy + j) * 40] := co
          byte[dest + cx + len + (cy + j) * 40] := ci & $3F
      $C0:
        len := byte[i++] - 1  'width
        dir := byte[i++] - 1  'height
        repeat j from 0 to len
          byte[dest + cx + j + cy * 40] := ci & $3F
          byte[dest + cx + j + (cy + dir) * 40] := ci & $3F
        repeat j from 0 to dir
          byte[dest + cx + (cy + j) * 40] := ci & $3F
          byte[dest + cx + len + (cy + j) * 40] := ci & $3F

  DiamondsNeeded := byte[raw_cave + 9 + Level]
  CaveTime := byte[raw_cave + 14 + Level]
  MagicWallTime := byte[raw_cave + 1]
  if MagicWallTime > CaveTime
    AmoebaTime := CaveTime
  else
    AmoebaTime := CaveTime - MagicWallTime
  DiamondValue := byte[raw_cave + 2]
  ExtraValue := byte[raw_cave + 3]
  DiamondCount := 0

pub Cover | n, cell

'' Ramdomly cover the screen with a scrolling pattern

  n := 0
  repeat while n <> constant(40 * 22)
    cell := ||long[random_ptr] // constant(40 * 22)
    if byte[cave_addr + cell] <> $3C
      byte[cave_addr + cell] := $3C
      ++n
      sn.cover_sound
      waitcnt(clkfreq / 1000 + cnt)

pub Uncover | n, cell

'' Ramdomly uncover the screen

  n := 0
  repeat while n <> constant(40 * 22)
    cell := ||long[random_ptr] // constant(40 * 22)
    if byte[cave_addr + cell] == $3C
      byte[cave_addr + cell] := temp_cave[cell]
      ++n
      sn.cover_sound
      waitcnt(clkfreq / 1000 + cnt)

dat

reset   byte  $00, $01, $02, $03,  $04, $05, $06, $07
        byte  $08, $09, $0A, $0B,  $08, $09, $0A, $0B
        byte  $10, $10, $12, $12,  $14, $14, $16, $16
        byte  $18, $19, $1A, $1B,  $1C, $1D, $1E, $1F
        byte  $20, $21, $22, $23,  $24, $25, $26, $27
        byte  $28, $29, $2A, $2B,  $2C, $2D, $2E, $2F
        byte  $30, $31, $32, $33,  $30, $31, $32, $33
        byte  $38, $38, $3A, $3B,  $3C, $3D, $3E, $3F

dirs    long  1, 40, -1, -40

pub GameLoop | cx, cy, ca, cn, ob, o1, o2, ready, count, amoebas, enclosed

'' Main game loop. Everything happens here.

  target := 0
  count := 0    'scans before rockford is born
  ready := 0

  NextLife := ((Score / 500) + 1) * 500
  MagicWallStatus := 0  'dormant
  AmoebaStatus := 0     'growing slowly

  ' game loop  
  repeat
    Alive := false
    amoebas := 0
    enclosed := true
    repeat cy from 1 to 21
      repeat cx from 0 to 39
        ca := cave_addr + cx + cy * 40  'address of current object

        'large case statements execute rather slowly to the point
        'that the game becomes unplayable, so we have to split the 
        'case into sections (too bad there is no indirect function
        'call in Spin)

        ob := byte[ca]
        if ob < $10 
          case ob
            $04:  'out door
              if target
                byte[ca] := $05
            $08:  'firefly facing left
              if CheckAround(ca)
                ExplodeCenter(ca, $1B)
              else
                if byte[ca + 40] == $00
                  byte[ca + 40] := $0F
                  byte[ca] := $00
                elseif byte[ca - 1] == $00
                  byte[ca - 1] := $0C
                  byte[ca] := $00
                else
                  byte[ca] := $0D 
            $09:  'firefly facing up
              if CheckAround(ca)
                ExplodeCenter(ca, $1B)
              else
                if byte[ca - 1] == $00
                  byte[ca - 1] := $0C
                  byte[ca] := $00
                elseif byte[ca - 40] == $00
                  byte[ca - 40] := $0D
                  byte[ca] := $00
                else
                  byte[ca] := $0E 
            $0A:  'firefly facing right
              if CheckAround(ca)
                ExplodeCenter(ca, $1B)
              else
                if byte[ca - 40] == $00
                  byte[ca - 40] := $0D
                  byte[ca] := $00
                elseif byte[ca + 1] == $00
                  byte[ca + 1] := $0E
                  byte[ca] := $00
                else
                  byte[ca] := $0F 
            $0B:  'firefly facing down
              if CheckAround(ca)
                ExplodeCenter(ca, $1B)
              else
                if byte[ca + 1] == $00
                  byte[ca + 1] := $0E
                  byte[ca] := $00
                elseif byte[ca + 40] == $00
                  byte[ca + 40] := $0F
                  byte[ca] := $00
                else
                  byte[ca] := $0C

        elseif ob < $20
          case ob 
            $10:  'boulder
              case byte[ca + 40]  'check object below
                $00:
                  'boulder can fall
                  byte[ca] := $00
                  byte[ca + 40] := $13  'falling boulder, scanned
                  sn.boulder_sound
                $02, $10, $14:  'wall, boulder or diamond 
                  if byte[ca - 1] == $00 and byte[ca + constant(40 - 1)] == $00
                    'boulder can roll left
                    byte[ca] := $00
                    byte[ca - 1] := $13  'falling boulder, scanned
                    sn.boulder_sound
                  elseif byte[ca + 1] == $00 and byte[ca + constant(40 + 1)] == $00
                    'boulder can roll right
                    byte[ca] := $00
                    byte[ca + 1] := $13  'falling boulder, scanned
                    sn.boulder_sound
            $12:  'falling boulder
              case byte[ca + 40]  'check object below
                $00:
                  'boulder can continue falling
                  byte[ca] := $00
                  byte[ca + 40] := $13  'falling boulder, scanned
                $03:  'magic wall
                  byte[ca] := $00
                  if MagicWallStatus < 2  'dormant or milling
                    if MagicWallStatus == 0
                      MagicWallStatus := 1
                      cn := st.get_time #> MagicWallTime
                      MagicWallTime := cn - MagicWallTime
                      sc.milling_on
                      sn.magic_wall_sound_on
                    if byte[ca + constant(40 * 2)] == $00
                      byte[ca + constant(40 * 2)] := $17  'falling diamond, scanned
                  sn.diamond_sound
                $02, $10, $14:  'wall, boulder or diamond
                  'we hit something
                  sn.boulder_sound
                  if byte[ca - 1] == $00 and byte[ca + constant(40 - 1)] == $00
                    'boulder can roll left
                    byte[ca] := $00
                    byte[ca - 1] := $13  'falling boulder, scanned
                  elseif byte[ca + 1] == $00 and byte[ca + constant(40 + 1)] == $00
                    'boulder can roll right
                    byte[ca] := $00
                    byte[ca + 1] := $13 'falling boulder, scanned
                  else
                    'boulder came to a stop
                    byte[ca] := $11  'stationary boulder, scanned
                $38:  'rockford?
                  'explode
                  ExplodeBelow(ca, $1B)
                $08, $09, $0A, $0B:  'firefly?
                  ExplodeBelow(ca, $1B)
                $30, $31, $32, $33:  'butterfly?
                  ExplodeBelow(ca, $20)
                other:
                  'boulder came to a stop
                  byte[ca] := $11  'stationary boulder, scanned
                  sn.boulder_sound
            $14:  'diamond
              case byte[ca + 40]  'check object below
                $00:
                  'diamond can fall
                  byte[ca] := $00
                  byte[ca + 40] := $17  'falling diamond, scanned
                  sn.diamond_sound
                $02, $10, $14:  'wall, boulder or diamond
                  if byte[ca - 1] == $00 and byte[ca + constant(40 - 1)] == $00
                    'diamond can roll left
                    byte[ca] := $00
                    byte[ca - 1] := $17  'falling diamond, scanned
                    sn.diamond_sound
                  elseif byte[ca + 1] == $00 and byte[ca + constant(40 + 1)] == $00
                    'diamond can roll right
                    byte[ca] := $00
                    byte[ca + 1] := $17  'falling diamond, scanned
                    sn.diamond_sound
            $16:  'falling diamond
              case byte[ca + 40]  'check object below
                $00:
                  'diamond can continue falling
                  byte[ca] := $00
                  byte[ca + 40] := $17  'falling diamond, scanned
                $03: 'magic wall
                  byte[ca] := $00
                  if MagicWallStatus < 2  'dormant or milling
                    if MagicWallStatus == 0
                      MagicWallStatus := 1
                      cn := st.get_time #> MagicWallTime
                      MagicWallTime := cn - MagicWallTime
                      sc.milling_on
                      sn.magic_wall_sound_on
                    if byte[ca + constant(40 * 2)] == $00
                      byte[ca + constant(40 * 2)] := $13  'falling boulder, scanned
                  sn.boulder_sound
                $02, $10, $14:  'wall, boulder or diamond
                  'we hit something
                  sn.diamond_sound
                  if byte[ca - 1] == $00 and byte[ca + constant(40 - 1)] == $00
                    'diamond can roll left
                    byte[ca] := $00
                    byte[ca - 1] := $17  'falling diamond, scanned
                  elseif byte[ca + 1] == $00 and byte[ca + constant(40 + 1)] == $00
                    'diamond can roll right
                    byte[ca] := $00
                    byte[ca + 1] := $17  'falling diamond, scanned
                  else
                    'diamond came to a stop
                    byte[ca] := $15  'stationary diamond, scanned
                $38:  'rockford?
                  'explode
                  ExplodeBelow(ca, $1B)
                $08, $09, $0A, $0B:  'firefly
                  ExplodeBelow(ca, $1B)
                $30, $31, $32, $33:  'butterfly
                  ExplodeBelow(ca, $20)
                other:
                  'diamond came to a stop
                  byte[ca] := $15  'stationary diamond, scanned
                  sn.diamond_sound
            $1B, $1C, $1D, $1E:  'explosion stages
              byte[ca]++
            $1F:  'explosion to space, final stage
              byte[ca] := $00

        elseif ob < $30
          case ob
            $20, $21, $22, $23:  'explosion stages
              byte[ca]++
            $24:  'explosion to diamonds, final stage
              byte[ca] := $15
            $25:  'inbox
              Alive := true
              if count++ == 20
                byte[ca] := $26
                sn.crack_sound
            $26, $27:
              Alive := true
              byte[ca]++
            $28:
              Alive := true
              byte[ca] := $38  'rockford is born
              sc.rockford_reset
              st.set_time(CaveTime)
              st.set_mode(st#STATUS_GAME)
              ready := 1

        elseif ob < $40
          case ob
            $30:  'butterfly facing down
              if CheckAround(ca)
                ExplodeCenter(ca, $20)
              else
                if byte[ca - 1] == $00
                  byte[ca - 1] := $35
                  byte[ca] := $00
                elseif byte[ca + 40] == $00
                  byte[ca + 40] := $34
                  byte[ca] := $00
                else
                  byte[ca] := $37 
            $31:  'butterfly facing left
              if CheckAround(ca)
                ExplodeCenter(ca, $20)
              else
                if byte[ca - 40] == $00
                  byte[ca - 40] := $36
                  byte[ca] := $00
                elseif byte[ca - 1] == $00
                  byte[ca - 1] := $35
                  byte[ca] := $00
                else
                  byte[ca] := $34 
            $32:  'butterfly facing up
              if CheckAround(ca)
                ExplodeCenter(ca, $20)
              else
                if byte[ca + 1] == $00
                  byte[ca + 1] := $37
                  byte[ca] := $00
                elseif byte[ca - 40] == $00
                  byte[ca - 40] := $36
                  byte[ca] := $00
                else
                  byte[ca] := $35 
            $33:  'butterfly facing right
              if CheckAround(ca)
                ExplodeCenter(ca, $20)
              else
                if byte[ca + 40] == $00
                  byte[ca + 40] := $34
                  byte[ca] := $00
                elseif byte[ca + 1] == $00
                  byte[ca + 1] := $37
                  byte[ca] := $00
                else
                  byte[ca] := $36

            $38:  'rockford
              Alive := true
              nes := read_input
              cn := 0
              if nes
                if nes & GP_RIGHT
                  last_dir := 1
                  sc.rockford_go(last_dir)
                  cn := ca + 1
                elseif nes & GP_LEFT
                  last_dir := -1
                  sc.rockford_go(last_dir)
                  cn := ca - 1
                elseif nes & GP_UP
                  sc.rockford_go(last_dir)
                  cn := ca - 40
                elseif nes & GP_DOWN
                  sc.rockford_go(last_dir)
                  cn := ca + 40

              if cn
                if nes & GP_SELECT
                  o1 := $39
                  o2 := $00
                else
                  o1 := $00
                  o2 := $39
                case byte[cn]
                  $00, $01:  'empty or dirt
                    sn.moving_sound(byte[cn])
                    byte[ca] := o1
                    byte[cn] := o2
                  $05:  'out door
                    sn.magic_wall_sound_off
                    sn.amoeba_sound_off
                    byte[ca] := o1
                    byte[cn] := o2
                    AddBonusPoints
                    waitcnt(clkfreq + cnt) '1 second delay 
                    return true 'means cave succesfully completed
                  $10, $11:    'boulder, stationary
                    if (nes & GP_RIGHT) and (byte[cn + 1] == $00) and ((long[random_ptr] & $03) == 0)
                      'boulder can be pushed
                      byte[cn + 1] := byte[cn]
                      byte[ca] := o1
                      byte[cn] := o2
                      sn.boulder_sound
                    elseif (nes & GP_LEFT) and (byte[cn - 1] == $00) and ((long[random_ptr] & $03) == 0)
                      'boulder can be pushed
                      byte[cn - 1] := byte[cn]
                      byte[ca] := o1
                      byte[cn] := o2
                      sn.boulder_sound
                  $14, $15, $16, $17:  'diamond
                    byte[ca] := o1
                    byte[cn] := o2
                    sn.pick_sound
                    st.set_diamond_count(++DiamondCount)
                    IncrementScore(DiamondValue)
                    if DiamondCount == DiamondsNeeded and not target
                      'flash screen when target is reached
                      sc.flash
                      sn.crack_sound
                      DiamondValue := ExtraValue
                      st.set_diamond_value(DiamondValue)
                      target := 1
        
              else
                sc.rockford_go(0)

              'screen may need scrolling
              if CaveNumber > 16
                'do not scroll intermission caves
                sx := 0
                sy := 0
              else
                if cx - sx > constant(20 - 6)
                  sx += constant(10 - 5)
                  sx <#= constant(40 - 20)
                elseif cx - sx < 5
                  sx -= constant(10 - 5)
                  sx #>= 0
                if cy - sy > screen_height - 4
                  sy += screen_height / 2 - 3
                  sy <#= 22 - screen_height
                elseif cy - sy < 3
                  sy -= screen_height / 2 - 3
                  sy #>= 0
              sc.scroll_to(sx * 8, sy * 16)

            $3A:  'amoeba
              if ready
                sn.amoeba_sound_on
              if AmoebaStatus == 2
                'turn into stone
                byte[ca] := $10  'stationary boulder
              elseif AmoebaStatus == 3
                'turn into diamond
                byte[ca] := $14  'stationary diamond
              else
                ++amoebas
                if enclosed
                  enclosed := CheckEnclosed(ca)
                cn := long[random_ptr]
                if AmoebaStatus == 0
                  cn &= $7F
                else
                  cn &= $0F
                if cn =< 3
                  cn := ca + dirs[cn & $03]
                  ob := byte[cn]
                  if ob == $00 or ob == $01
                    byte[cn] := $3A              

        'reset scanned flag
        cn := ca - constant(40 + 1)
        byte[cn] := reset.byte[byte[cn]]

    if MagicWallStatus == 1
      if st.get_time < MagicWallTime
        MagicWallStatus := 2  'expired
        sc.milling_off
        sn.magic_wall_sound_off

    if AmoebaStatus == 0
      if st.get_time < AmoebaTime
        AmoebaStatus := 1  'growing fast
    if amoebas > 200
      AmoebaStatus := 2  'grew too big, turn into stones
    elseif enclosed
      AmoebaStatus := 3  'fully enclosed, turn into diamonds
    if amoebas == 0
      sn.amoeba_sound_off

    if demo_ptr
      nes := 0
    else
      nes := read_input
    if nes & GP_A
      st.set_time(0)            'stop the time
      sn.magic_wall_sound_off
      sn.amoeba_sound_off
      return false              'not completed, restart the same level

    elseif nes & GP_B
      'pause game
      st.set_mode(st#STATUS_PAUSE)
      repeat while nes & GP_B
        nes := read_input
      repeat while (nes & GP_B) == 0
        nes := read_input
      if ready
        st.set_mode(st#STATUS_GAME)
      else
        st.set_mode(st#STATUS_PRE)
      repeat while nes & GP_B
        nes := read_input
      
    elseif (nes & GP_SELECT) and (nes & GP_START)
      st.set_time(0)
      sn.magic_wall_sound_off
      sn.amoeba_sound_off
      return true               '*cheat* go to the next level

    cn := st.get_time
    if cn < 10
      sn.time_ending_sound(cn)

    if ready and (cn == 0)
      'out of time
      sn.magic_wall_sound_off
      sn.amoeba_sound_off
      st.set_mode(st#STATUS_OUT_OF_TIME)
      count := 60 * 60          'approx 1 minute
      repeat while count > 0
        repeat while vsync == 0
        repeat while vsync <> 0
        --count
        nes := read_input
        if nes & GP_A
          return false          'not completed, restart the same level
        elseif (nes & GP_SELECT) and (nes & GP_START)
          return true           '*cheat* go to the next level
      return false

    if Level < 2
      waitcnt(clkfreq / 50 + cnt)

pub ExplodeBelow(cell, towhat) 
  if byte[cell - 1] <> $07
    byte[cell - 1] := towhat + 1
  if byte[cell] <> $07
    byte[cell] := towhat + 1
  if byte[cell + 1] <> $07
    byte[cell + 1] := towhat
  if byte[cell + constant(40 - 1)] <> $07
    byte[cell + constant(40 - 1)] := towhat
  if byte[cell + 40] <> $07
    byte[cell + 40] := towhat
  if byte[cell + constant(40 + 1)] <> $07
    byte[cell + constant(40 + 1)] := towhat
  if byte[cell + constant(40 * 2 - 1)] <> $07
    byte[cell + constant(40 * 2 - 1)] := towhat
  if byte[cell + constant(40 * 2)] <> $07
    byte[cell + constant(40 * 2)] := towhat
  if byte[cell + constant(40 * 2 + 1)] <> $07
    byte[cell + constant(40 * 2 + 1)] := towhat
  sn.explosion_sound

pub ExplodeCenter(cell, towhat) 
  if byte[cell - constant(40 + 1)] <> $07
    byte[cell - constant(40 + 1)] := towhat + 1
  if byte[cell - 40] <> $07
    byte[cell - 40] := towhat + 1
  if byte[cell - constant(40 - 1)] <> $07
    byte[cell - constant(40 - 1)] := towhat + 1
  if byte[cell - 1] <> $07
    byte[cell - 1] := towhat + 1
  if byte[cell] <> $07
    byte[cell] := towhat + 1
  if byte[cell + 1] <> $07
    byte[cell + 1] := towhat
  if byte[cell + constant(40 - 1)] <> $07
    byte[cell + constant(40 - 1)] := towhat
  if byte[cell + 40] <> $07
    byte[cell + 40] := towhat
  if byte[cell + constant(40 + 1)] <> $07
    byte[cell + constant(40 + 1)] := towhat
  sn.explosion_sound

pub CheckAround(cell) | ob
  ob := byte[cell - 40]
  if ob => $38 and ob =< $3B  'rockford or amoeba, scanned or not
    return true
  ob := byte[cell - 1]
  if ob => $38 and ob =< $3B  'rockford or amoeba, scanned or not
    return true
  ob := byte[cell + 1]
  if ob => $38 and ob =< $3B  'rockford or amoeba, scanned or not
    return true
  ob := byte[cell + 40]
  if ob => $38 and ob =< $3B  'rockford or amoeba, scanned or not
    return true
  return false

pub CheckEnclosed(cell) | ob, i
  repeat i from 0 to 3
    ob := byte[cell + dirs[i]]
    if ob == $00 or ob == $01
      return false
  return true

pub IncrementScore(amount)
  Score += amount
  st.set_score(Score)
  if Score => NextLife and Men < 9
    ++Men
    sc.new_life
    NextLife += 500

pub AddBonusPoints | i, j

  i := j := st.get_time  
  repeat while i > 0
    st.set_time(--i)
    IncrementScore(Level + 1)
    if i < 10
      sn.time_ending_sound(i)
      waitcnt(clkfreq / 32 + cnt)
    else
      sn.bonus_point_sound(j - i)

dat

        long 0 'align

        ' screen array
screen  byte $00[40*28]

' color index  3  1  2  0
palette long $06_6B_04_02

'              -x-  -y-
scroll  long $0000_0000
                                                                                                                                                                                                                                                                                                                                       
status  long 1  'set this to 1 to show the status line at the top

        ' status line
        byte $90[20]

        ' foreground color for status line characters
        byte $06[20]

        ' tiles
tiles   word %%0_0_0_0_0_0_0_0  ' $00 space    
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_1_0_0_1_0_1  ' $01 dirt
        word %%1_1_0_0_1_0_0_1 
        word %%0_1_2_1_0_1_2_0 
        word %%1_1_0_1_1_1_0_1
        word %%1_0_1_1_1_0_1_1
        word %%0_2_1_0_1_2_1_0 
        word %%2_1_1_2_2_0_1_1 
        word %%1_0_1_1_1_1_2_0 
        word %%0_1_1_1_2_1_1_1 
        word %%1_2_0_1_1_1_2_0 
        word %%1_2_1_0_2_1_0_1 
        word %%0_2_1_1_0_1_2_1 
        word %%1_0_1_0_1_0_1_0
        word %%1_1_0_0_1_1_0_1 
        word %%1_0_1_2_0_1_0_0 
        word %%0_1_0_0_1_0_1_0 

        word %%0_3_3_3_0_3_3_3  ' $02 brick wall
        word %%0_2_3_3_0_2_3_3
        word %%0_2_2_2_0_2_2_2
        word %%0_0_0_0_0_0_0_0
        word %%3_3_0_3_3_3_0_3
        word %%3_3_0_2_3_3_0_2
        word %%2_2_0_2_2_2_0_2
        word %%0_0_0_0_0_0_0_0
        word %%0_3_3_3_0_3_3_3
        word %%0_2_3_3_0_2_3_3
        word %%0_2_2_2_0_2_2_2
        word %%0_0_0_0_0_0_0_0
        word %%3_3_0_3_3_3_0_3
        word %%3_3_0_2_3_3_0_2
        word %%2_2_0_2_2_2_0_2
        word %%0_0_0_0_0_0_0_0
         
        word %%0_3_3_3_0_3_3_3  ' $03 magic wall
        word %%0_2_3_3_0_2_2_2
        word %%0_2_2_2_0_2_2_2
        word %%0_0_0_0_0_0_0_0
        word %%3_3_0_3_3_3_0_3
        word %%3_3_0_2_3_3_0_3
        word %%2_2_0_2_2_2_0_2
        word %%0_0_0_0_0_0_0_0
        word %%0_3_3_3_0_3_3_3
        word %%0_2_3_3_0_2_3_3
        word %%0_2_2_2_0_2_2_2
        word %%0_0_0_0_0_0_0_0
        word %%3_3_0_3_3_3_0_3
        word %%3_3_0_2_3_3_0_2
        word %%2_2_0_2_2_2_0_2
        word %%0_0_0_0_0_0_0_0

        word %%2_2_2_2_2_2_2_2  ' $04 out door (invisible)
        word %%2_2_2_2_2_2_2_2
        word %%2_0_0_2_2_0_0_2
        word %%2_2_0_2_2_2_0_2
        word %%2_3_0_2_2_3_0_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2
        word %%2_0_0_2_2_0_0_2
        word %%2_2_0_2_2_2_0_2
        word %%2_3_0_2_2_3_0_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2

        word %%0_0_0_0_0_0_0_0  ' $05 out door (flashing)
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $06          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%2_2_2_2_2_2_2_2  ' $07 steel wall
        word %%2_2_2_2_2_2_2_2
        word %%2_0_0_2_2_0_0_2
        word %%2_2_0_2_2_2_0_2
        word %%2_3_0_2_2_3_0_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2
        word %%2_0_0_2_2_0_0_2
        word %%2_2_0_2_2_2_0_2
        word %%2_3_0_2_2_3_0_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2

        word %%0_0_0_0_0_0_0_0  ' $08          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $09          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $0A          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $0B          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $0C          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $0D          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $0E          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $0F          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_3_3_3_0_0_0  ' $10 boulder stationary
        word %%0_3_2_3_1_3_0_0
        word %%3_1_3_2_3_3_3_0
        word %%2_3_1_2_1_3_3_3
        word %%2_2_2_1_2_3_3_3
        word %%2_1_2_2_2_1_2_3
        word %%2_2_1_1_2_2_3_3
        word %%2_2_2_2_2_1_2_3
        word %%2_0_2_2_2_2_2_1
        word %%2_2_0_2_2_1_2_1
        word %%2_0_2_2_2_2_2_2
        word %%2_2_0_2_2_2_2_2
        word %%2_2_2_0_2_2_2_0
        word %%0_2_2_2_2_2_2_0
        word %%0_0_2_0_1_2_0_0
        word %%0_0_0_2_2_0_0_0

        word %%0_0_3_3_3_0_0_0  ' $11 boulder stationary, scanned this frame
        word %%0_3_2_3_1_3_0_0
        word %%3_1_3_2_3_3_3_0
        word %%2_3_1_2_1_3_3_3
        word %%2_2_2_1_2_3_3_3
        word %%2_1_2_2_2_1_2_3
        word %%2_2_1_1_2_2_3_3
        word %%2_2_2_2_2_1_2_3
        word %%2_0_2_2_2_2_2_1
        word %%2_2_0_2_2_1_2_1
        word %%2_0_2_2_2_2_2_2
        word %%2_2_0_2_2_2_2_2
        word %%2_2_2_0_2_2_2_0
        word %%0_2_2_2_2_2_2_0
        word %%0_0_2_0_1_2_0_0
        word %%0_0_0_2_2_0_0_0

        word %%0_0_3_3_3_0_0_0  ' $12 boulder falling
        word %%0_3_2_3_1_3_0_0
        word %%3_1_3_2_3_3_3_0
        word %%2_3_1_2_1_3_3_3
        word %%2_2_2_1_2_3_3_3
        word %%2_1_2_2_2_1_2_3
        word %%2_2_1_1_2_2_3_3
        word %%2_2_2_2_2_1_2_3
        word %%2_0_2_2_2_2_2_1
        word %%2_2_0_2_2_1_2_1
        word %%2_0_2_2_2_2_2_2
        word %%2_2_0_2_2_2_2_2
        word %%2_2_2_0_2_2_2_0
        word %%0_2_2_2_2_2_2_0
        word %%0_0_2_0_1_2_0_0
        word %%0_0_0_2_2_0_0_0

        word %%0_0_3_3_3_0_0_0  ' $13 boulder falling, scanned this frame
        word %%0_3_2_3_1_3_0_0
        word %%3_1_3_2_3_3_3_0
        word %%2_3_1_2_1_3_3_3
        word %%2_2_2_1_2_3_3_3
        word %%2_1_2_2_2_1_2_3
        word %%2_2_1_1_2_2_3_3
        word %%2_2_2_2_2_1_2_3
        word %%2_0_2_2_2_2_2_1
        word %%2_2_0_2_2_1_2_1
        word %%2_0_2_2_2_2_2_2
        word %%2_2_0_2_2_2_2_2
        word %%2_2_2_0_2_2_2_0
        word %%0_2_2_2_2_2_2_0
        word %%0_0_2_0_1_2_0_0
        word %%0_0_0_2_2_0_0_0

        word %%0_0_0_2_3_0_0_0  ' $14 diamond stationary
        word %%0_0_0_1_2_0_0_0
        word %%0_0_2_3_3_3_0_0
        word %%0_0_1_3_3_2_0_0
        word %%0_2_2_2_2_2_3_0
        word %%0_1_1_1_1_1_2_0
        word %%2_0_0_0_0_0_0_3
        word %%1_0_0_0_0_0_0_2
        word %%2_1_1_1_1_1_1_3
        word %%1_2_2_2_2_2_2_2
        word %%0_2_3_3_3_3_3_0
        word %%0_1_3_3_3_3_2_0
        word %%0_0_2_1_1_3_0_0
        word %%0_0_1_1_1_2_0_0
        word %%0_0_0_1_2_0_0_0
        word %%0_0_0_1_1_0_0_0

        word %%0_0_0_2_3_0_0_0  ' $15 diamond stationary, scanned
        word %%0_0_0_1_2_0_0_0
        word %%0_0_2_3_3_3_0_0
        word %%0_0_1_3_3_2_0_0
        word %%0_2_2_2_2_2_3_0
        word %%0_1_1_1_1_1_2_0
        word %%2_0_0_0_0_0_0_3
        word %%1_0_0_0_0_0_0_2
        word %%2_1_1_1_1_1_1_3
        word %%1_2_2_2_2_2_2_2
        word %%0_2_3_3_3_3_3_0
        word %%0_1_3_3_3_3_2_0
        word %%0_0_2_1_1_3_0_0
        word %%0_0_1_1_1_2_0_0
        word %%0_0_0_1_2_0_0_0
        word %%0_0_0_1_1_0_0_0

        word %%0_0_0_2_3_0_0_0  ' $16 diamond falling
        word %%0_0_0_1_2_0_0_0
        word %%0_0_2_3_3_3_0_0
        word %%0_0_1_3_3_2_0_0
        word %%0_2_2_2_2_2_3_0
        word %%0_1_1_1_1_1_2_0
        word %%2_0_0_0_0_0_0_3
        word %%1_0_0_0_0_0_0_2
        word %%2_1_1_1_1_1_1_3
        word %%1_2_2_2_2_2_2_2
        word %%0_2_3_3_3_3_3_0
        word %%0_1_3_3_3_3_2_0
        word %%0_0_2_1_1_3_0_0
        word %%0_0_1_1_1_2_0_0
        word %%0_0_0_1_2_0_0_0
        word %%0_0_0_1_1_0_0_0

        word %%0_0_0_2_3_0_0_0  ' $17 diamond falling, scanned
        word %%0_0_0_1_2_0_0_0
        word %%0_0_2_3_3_3_0_0
        word %%0_0_1_3_3_2_0_0
        word %%0_2_2_2_2_2_3_0
        word %%0_1_1_1_1_1_2_0
        word %%2_0_0_0_0_0_0_3
        word %%1_0_0_0_0_0_0_2
        word %%2_1_1_1_1_1_1_3
        word %%1_2_2_2_2_2_2_2
        word %%0_2_3_3_3_3_3_0
        word %%0_1_3_3_3_3_2_0
        word %%0_0_2_1_1_3_0_0
        word %%0_0_1_1_1_2_0_0
        word %%0_0_0_1_2_0_0_0
        word %%0_0_0_1_1_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $18          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $19          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $1A          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $1B explode to space frame 1         
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_3_0_0
        word %%0_0_3_0_0_0_0_0
        word %%0_3_0_0_3_0_0_0
        word %%0_0_0_3_0_0_3_0
        word %%0_0_0_0_3_0_0_0
        word %%0_0_3_0_0_0_0_0
        word %%0_3_0_0_0_3_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $1C explode to space frame 2          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_3_0_0
        word %%0_0_0_3_0_1_0_0
        word %%0_3_1_0_0_0_3_0
        word %%0_1_0_0_1_3_1_0
        word %%0_3_0_1_0_0_1_0
        word %%0_0_0_0_1_3_0_0
        word %%0_3_1_0_1_0_0_0
        word %%0_1_0_1_0_1_0_0
        word %%0_0_3_1_1_0_3_0
        word %%0_0_3_0_3_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $1D explode to space frame 3          
        word %%0_0_0_0_0_0_0_0
        word %%0_3_0_0_3_0_3_0
        word %%0_0_0_3_0_2_0_0
        word %%0_3_0_1_0_1_3_0
        word %%0_1_2_0_0_0_1_0
        word %%0_2_0_0_2_1_2_0
        word %%3_1_0_2_0_0_2_0
        word %%0_0_0_0_1_1_0_0
        word %%3_1_2_0_1_0_3_0
        word %%0_2_0_2_0_1_0_0
        word %%0_0_1_2_1_0_1_0
        word %%3_0_1_0_1_0_3_0
        word %%0_0_0_3_3_0_0_0
        word %%0_3_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $1E explode to space frame 4, same as frame 2         
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_3_0_0
        word %%0_0_0_3_0_1_0_0
        word %%0_3_1_0_0_0_3_0
        word %%0_1_0_0_1_3_1_0
        word %%0_3_0_1_0_0_1_0
        word %%0_0_0_0_1_3_0_0
        word %%0_3_1_0_1_0_0_0
        word %%0_1_0_1_0_1_0_0
        word %%0_0_3_1_1_0_3_0
        word %%0_0_3_0_3_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $1F explode to space frame 5, same as frame 1          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_3_0_0
        word %%0_0_3_0_0_0_0_0
        word %%0_3_0_0_3_0_0_0
        word %%0_0_0_3_0_0_3_0
        word %%0_0_0_0_3_0_0_0
        word %%0_0_3_0_0_0_0_0
        word %%0_3_0_0_0_3_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $20 explode to diamond frame 1          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_3_0_0
        word %%0_0_3_0_0_0_0_0
        word %%0_3_0_0_3_0_0_0
        word %%0_0_0_3_0_0_3_0
        word %%0_0_0_0_3_0_0_0
        word %%0_0_3_0_0_0_0_0
        word %%0_3_0_0_0_3_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_3_0_0_0  ' $21 explode to diamond frame 2          
        word %%3_0_3_0_0_0_3_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_3_0_3_0_3_0
        word %%0_3_0_0_0_0_0_0
        word %%0_0_0_0_0_3_0_0
        word %%3_0_0_0_0_0_0_3
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%3_0_0_0_0_0_3_0
        word %%0_0_0_0_0_0_0_0
        word %%0_3_0_0_3_0_0_3
        word %%0_0_0_3_0_0_0_0
        word %%3_0_0_0_0_0_3_0
        word %%0_0_3_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_1_1_0_0_0  ' $22 explode to diamond frame 3          
        word %%0_3_0_3_0_0_0_0
        word %%0_0_1_0_0_1_0_3
        word %%0_0_3_0_2_3_0_0
        word %%0_0_0_2_0_0_1_0
        word %%0_0_0_0_0_0_3_0
        word %%1_0_2_0_0_2_0_1
        word %%3_0_0_0_0_0_0_3
        word %%1_0_0_0_0_0_0_2
        word %%3_0_2_0_0_0_0_3
        word %%0_0_0_0_0_2_2_0
        word %%0_3_0_2_0_0_3_0
        word %%0_0_1_0_1_2_0_0
        word %%0_0_3_0_0_0_0_3
        word %%0_3_0_1_2_0_0_0
        word %%0_0_0_3_3_0_0_0

        word %%0_0_0_1_1_0_0_0  ' $23 explode to diamond frame 4          
        word %%0_0_0_3_3_0_0_0
        word %%0_0_1_0_0_1_0_0
        word %%0_0_3_0_2_3_0_0
        word %%0_1_0_2_0_0_1_0
        word %%0_3_0_0_3_0_3_0
        word %%1_0_2_3_1_2_0_1
        word %%3_1_0_2_2_0_2_3
        word %%1_0_0_3_0_1_1_2
        word %%3_0_2_0_2_0_0_3
        word %%0_1_1_0_0_3_2_0
        word %%0_3_0_2_2_0_3_0
        word %%0_0_1_3_0_2_0_0
        word %%0_0_3_0_1_3_0_0
        word %%0_0_0_1_2_0_0_0
        word %%0_0_0_3_3_0_0_0

        word %%0_0_0_1_1_0_0_0  ' $24 explode to diamond frame 5          
        word %%0_0_0_3_3_0_0_0
        word %%0_0_1_2_1_1_0_0
        word %%0_0_3_2_2_3_0_0
        word %%0_1_1_3_3_2_1_0
        word %%0_3_2_3_2_2_3_0
        word %%1_2_3_1_1_3_3_1
        word %%3_3_2_3_3_2_1_3
        word %%1_2_2_3_2_3_1_2
        word %%3_2_3_1_3_1_1_3
        word %%0_1_2_3_1_3_2_0
        word %%0_3_2_1_2_1_3_0
        word %%0_0_1_3_3_2_0_0
        word %%0_0_3_2_1_3_0_0
        word %%0_0_0_1_2_0_0_0
        word %%0_0_0_3_3_0_0_0

        word %%2_2_2_2_2_2_2_2  ' $25 inbox, flashing         
        word %%2_0_0_0_0_0_0_2
        word %%2_0_0_0_0_0_0_2
        word %%2_0_0_0_0_0_0_2
        word %%2_0_0_0_0_0_0_2
        word %%2_0_0_0_0_0_0_2
        word %%2_0_0_0_0_0_0_2
        word %%2_0_0_0_0_0_0_2
        word %%2_0_0_0_0_0_0_2
        word %%2_0_0_0_0_0_0_2
        word %%2_0_0_0_0_0_0_2
        word %%2_0_0_0_0_0_0_2
        word %%2_0_0_0_0_0_0_2
        word %%2_0_0_0_0_0_0_2
        word %%2_0_0_0_0_0_0_2
        word %%2_2_2_2_2_2_2_2

        word %%0_0_0_0_0_0_0_0  ' $26 inbox morphing into rockford, same as explode to space frame 1
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_3_0_0
        word %%0_0_3_0_0_0_0_0
        word %%0_3_0_0_3_0_0_0
        word %%0_0_0_3_0_0_3_0
        word %%0_0_0_0_3_0_0_0
        word %%0_0_3_0_0_0_0_0
        word %%0_3_0_0_0_3_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $27 inbox morphing into rockford, same as explode to space frame 2          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_3_0_0
        word %%0_0_0_3_0_1_0_0
        word %%0_3_1_0_0_0_3_0
        word %%0_1_0_0_1_3_1_0
        word %%0_3_0_1_0_0_1_0
        word %%0_0_0_0_1_3_0_0
        word %%0_3_1_0_1_0_0_0
        word %%0_1_0_1_0_1_0_0
        word %%0_0_3_1_1_0_3_0
        word %%0_0_3_0_3_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $28 inbox morphing into rockford, same as explode to space frame 3          
        word %%0_0_0_0_0_0_0_0
        word %%0_3_0_0_3_0_3_0
        word %%0_0_0_3_0_2_0_0
        word %%0_3_0_1_0_1_3_0
        word %%0_1_2_0_0_0_1_0
        word %%0_2_0_0_2_1_2_0
        word %%3_1_0_2_0_0_2_0
        word %%0_0_0_0_1_1_0_0
        word %%3_1_2_0_1_0_3_0
        word %%0_2_0_2_0_1_0_0
        word %%0_0_1_2_1_0_1_0
        word %%3_0_1_0_1_0_3_0
        word %%0_0_0_3_3_0_0_0
        word %%0_3_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $29          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $2A          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $2B          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $2C          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $2D          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $2E          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $2F          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $30          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $31          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $32          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $33          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $34          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $35          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $36          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $37          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $38 rockford
        word %%0_0_2_0_0_2_0_0
        word %%0_0_2_2_2_2_0_0
        word %%0_2_0_2_2_0_2_0
        word %%0_2_0_2_2_0_2_0
        word %%0_0_2_2_2_2_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_2_2_2_2_0_0
        word %%0_2_0_3_3_0_2_0
        word %%0_0_3_2_2_3_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_1_3_3_1_0_0
        word %%0_0_1_0_0_1_0_0
        word %%0_0_1_0_0_1_0_0
        word %%0_3_3_0_0_3_3_0

        word %%0_0_0_0_0_0_0_0  ' $39          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $3A          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $3B          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%2_2_2_2_2_2_2_2  ' $3C          
        word %%2_2_2_2_2_2_2_2
        word %%2_0_0_2_2_0_0_2
        word %%2_2_0_2_2_2_0_2
        word %%2_3_0_2_2_3_0_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2
        word %%2_0_0_2_2_0_0_2
        word %%2_2_0_2_2_2_0_2
        word %%2_3_0_2_2_3_0_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_2_2_2_2_2_2

        word %%0_0_0_0_0_0_0_0  ' $3D          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $3E          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

        word %%0_0_0_0_0_0_0_0  ' $3F          
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0

' chargen

        word %%0_0_0_0_0_0_0_0 ' $80 '0'
        word %%0_3_3_3_3_3_0_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%0_3_3_3_3_3_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $81 '1'
        word %%0_0_0_3_3_0_0_0
        word %%0_0_3_3_3_0_0_0
        word %%0_0_3_3_3_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_3_3_3_3_3_3_0
        word %%0_3_3_3_3_3_3_0
         
        word %%0_0_0_0_0_0_0_0 ' $82 '2'
        word %%0_3_3_3_3_3_0_0
        word %%3_3_0_0_3_3_3_0
        word %%0_0_0_3_3_3_0_0
        word %%0_0_3_3_3_0_0_0
        word %%0_3_3_3_0_0_0_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_3_3_3_3_0
         
        word %%0_0_0_0_0_0_0_0 ' $83
        word %%0_3_3_3_3_3_3_0
        word %%0_0_0_0_3_3_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_0_3_3_0_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%0_3_3_3_3_3_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $84
        word %%3_3_0_0_0_0_0_0
        word %%3_3_0_0_0_0_0_0
        word %%3_3_0_3_3_3_0_0
        word %%3_3_3_3_3_3_3_0
        word %%0_0_0_3_3_3_0_0
        word %%0_0_0_3_3_3_0_0
        word %%0_0_0_3_3_3_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $85
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_3_3_3_0_0
        word %%0_0_0_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%0_3_3_3_3_3_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $86
        word %%0_3_3_3_3_3_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_3_3_3_0_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%0_3_3_3_3_3_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $87
        word %%3_3_3_3_3_3_3_0
        word %%3_0_0_0_3_3_3_0
        word %%0_0_0_3_3_3_0_0
        word %%0_0_3_3_3_0_0_0
        word %%0_3_3_3_0_0_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $88
        word %%0_3_3_3_3_3_0_0
        word %%3_3_3_0_0_3_3_0
        word %%0_3_3_3_3_3_0_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%0_3_3_3_3_3_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $89
        word %%0_3_3_3_3_3_0_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%0_3_3_3_3_3_3_0
        word %%0_0_0_3_3_3_0_0
        word %%0_0_3_3_3_0_0_0
        word %%0_3_3_3_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $8A ':'
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $8B ';'
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_3_3_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $8C '/'
        word %%0_0_0_0_0_0_3_0
        word %%0_0_0_0_0_3_3_0
        word %%0_0_0_0_3_3_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_3_3_0_0_0_0
        word %%0_3_3_0_0_0_0_0
        word %%3_3_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $8D
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $8E
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $8F
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $90 space
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $91 'A'
        word %%0_0_3_3_3_0_0_0
        word %%0_3_3_3_3_3_0_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
         
        word %%0_0_0_0_0_0_0_0 ' $92
        word %%3_3_3_3_3_3_0_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_3_3_3_0_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_3_3_3_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $93
        word %%0_3_3_3_3_3_0_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%0_3_3_3_3_3_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $94
        word %%3_3_3_3_3_0_0_0
        word %%3_3_3_0_3_3_0_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_3_3_0_0
        word %%3_3_3_3_3_3_0_0
        word %%3_3_3_3_3_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $95
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_3_3_3_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_3_3_3_3_0
         
        word %%0_0_0_0_0_0_0_0 ' $96
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_3_3_3_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $97
        word %%0_3_3_3_3_3_3_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_0_3_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%0_3_3_3_3_3_3_0
         
        word %%0_0_0_0_0_0_0_0 ' $98
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
         
        word %%0_0_0_0_0_0_0_0 ' $99
        word %%0_3_3_3_3_3_0_0
        word %%0_0_3_3_3_0_0_0
        word %%0_0_3_3_3_0_0_0
        word %%0_0_3_3_3_0_0_0
        word %%0_0_3_3_3_0_0_0
        word %%0_3_3_3_3_3_0_0
        word %%0_3_3_3_3_3_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $9A
        word %%0_0_0_0_0_3_3_0
        word %%0_0_0_0_0_3_3_0
        word %%0_0_0_0_0_3_3_0
        word %%0_0_0_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%0_3_3_3_3_3_3_0
        word %%0_0_3_3_3_3_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $9B
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_3_3_0_0
        word %%3_3_3_3_3_0_0_0
        word %%3_3_3_3_3_0_0_0
        word %%3_3_3_0_3_3_0_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
         
        word %%0_0_0_0_0_0_0_0 ' $9C
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_3_3_3_3_0
         
        word %%0_0_0_0_0_0_0_0 ' $9D
        word %%3_3_0_0_0_3_3_0
        word %%3_3_3_0_3_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_0_3_0_3_3_0
        word %%3_3_0_0_0_3_3_0
        word %%3_3_0_0_0_3_3_0
        word %%3_3_0_0_0_3_3_0
         
        word %%0_0_0_0_0_0_0_0 ' $9E
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_3_0_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_0_3_3_3_0
        word %%3_3_3_0_0_3_3_0
         
        word %%0_0_0_0_0_0_0_0 ' $9F
        word %%0_3_3_3_3_3_0_0
        word %%3_3_0_0_0_3_3_0
        word %%3_3_0_0_0_3_3_0
        word %%3_3_0_0_0_3_3_0
        word %%3_3_0_0_0_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%0_3_3_3_3_3_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $A0
        word %%3_3_3_3_3_3_0_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_3_3_3_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $A1
        word %%0_0_3_3_3_3_0_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_3_3_0_0
        word %%0_3_3_3_3_3_3_0
        word %%0_0_3_3_0_3_3_0
         
        word %%0_0_0_0_0_0_0_0 ' $A2
        word %%3_3_3_3_3_3_0_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_3_3_3_0_0
        word %%3_3_3_3_3_3_0_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
         
        word %%0_0_0_0_0_0_0_0 ' $A3
        word %%0_3_3_3_3_3_0_0
        word %%3_3_3_0_0_0_0_0
        word %%0_3_3_3_3_3_0_0
        word %%0_0_0_0_0_3_3_0
        word %%0_0_0_0_0_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_3_3_3_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $A4
        word %%3_3_3_3_3_3_3_0
        word %%0_0_3_3_3_0_0_0
        word %%0_0_3_3_3_0_0_0
        word %%0_0_3_3_3_0_0_0
        word %%0_0_3_3_3_0_0_0
        word %%0_0_3_3_3_0_0_0
        word %%0_0_3_3_3_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $A5
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_3_3_3_3_0
         
        word %%0_0_0_0_0_0_0_0 ' $A6
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%0_3_3_3_3_3_0_0
        word %%0_0_3_3_3_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $A7
        word %%3_3_0_0_0_3_3_0
        word %%3_3_0_0_0_3_3_0
        word %%3_3_0_3_0_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_0_3_3_3_0
        word %%3_3_0_0_0_3_3_0
         
        word %%0_0_0_0_0_0_0_0 ' $A8
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%0_0_3_3_3_3_0_0
        word %%0_0_3_3_3_3_0_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
        word %%3_3_3_0_0_3_3_0
         
        word %%0_0_0_0_0_0_0_0 ' $A9
        word %%3_3_0_0_0_3_3_0
        word %%3_3_0_0_0_3_3_0
        word %%0_3_3_3_3_3_0_0
        word %%0_0_3_3_3_0_0_0
        word %%0_0_3_3_3_0_0_0
        word %%0_0_3_3_3_0_0_0
        word %%0_0_3_3_3_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $AA
        word %%3_3_3_3_3_3_3_0
        word %%0_0_0_3_3_3_0_0
        word %%0_0_3_3_3_0_0_0
        word %%0_3_3_3_0_0_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_3_3_3_3_0
         
        word %%0_0_0_0_0_0_0_0 ' $AB '('
        word %%0_0_0_0_3_3_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_0_3_3_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $AC diamond
        word %%0_0_0_3_0_0_0_0
        word %%0_0_3_0_3_0_0_0
        word %%0_3_3_3_3_3_0_0
        word %%3_0_0_0_0_0_3_0
        word %%0_3_3_3_3_3_0_0
        word %%0_0_3_0_3_0_0_0
        word %%0_0_0_3_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $AD ')'
        word %%0_0_3_3_0_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_3_3_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $AE
        word %%0_0_3_0_0_3_0_0
        word %%0_0_3_3_3_3_0_0
        word %%0_3_0_3_3_0_3_0
        word %%0_3_0_3_3_0_3_0
        word %%0_0_3_3_3_3_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_3_3_3_3_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $AF ','
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_0_3_3_0_0_0
        word %%0_0_3_3_0_0_0_0
         
        word %%1_3_2_2_1_3_2_2 ' $B0 scrolling bgnd of the title screen
        word %%1_2_2_2_1_2_2_2
        word %%0_1_2_2_0_1_2_2
        word %%2_2_2_2_2_2_2_2
        word %%2_2_1_3_2_2_1_3
        word %%2_2_1_2_2_2_1_2
        word %%2_2_0_1_2_2_0_1
        word %%2_2_2_2_2_2_2_2

        word %%3_3_3_3_3_3_3_3 ' $B1
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3

        word %%1_2_3_3_3_3_3_3 ' $B2
        word %%1_2_3_3_3_3_3_3
        word %%1_2_3_3_3_3_3_3
        word %%1_2_3_3_3_3_3_3
        word %%1_2_3_3_3_3_3_3
        word %%1_2_3_3_3_3_3_3
        word %%1_2_3_3_3_3_3_3
        word %%1_2_3_3_3_3_3_3
        
        word %%3_3_3_3_0_0_0_0 ' $B3
        word %%3_3_3_3_3_0_0_0
        word %%3_3_3_3_3_3_0_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3
        
        word %%3_3_3_3_3_3_3_3 ' $B4
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_3_3_3_0_0
        word %%3_3_3_3_3_0_0_0
        
        word %%3_3_3_3_0_0_0_0 ' $B5
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        
        word %%0_0_0_0_3_3_3_3 ' $B6
        word %%0_0_0_0_3_3_3_3
        word %%0_0_0_0_3_3_3_3
        word %%0_0_0_0_3_3_3_3
        word %%0_0_0_0_3_3_3_3
        word %%0_0_0_0_3_3_3_3
        word %%0_0_0_0_3_3_3_3
        word %%0_0_0_0_3_3_3_3

        word %%0_0_0_0_0_0_0_0 ' $B7
        word %%3_0_0_0_0_0_0_0
        word %%3_3_0_0_0_0_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        
        word %%3_3_3_3_0_0_0_0 ' $B8
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_0_0_0_0_0_0
        word %%3_0_0_0_0_0_0_0
        
        word %%1_2_3_3_0_0_0_0 ' $B9
        word %%1_2_3_3_0_0_0_0
        word %%1_2_3_3_0_0_0_0
        word %%1_2_3_3_0_0_0_0
        word %%1_2_3_3_0_0_0_0
        word %%1_2_3_3_0_0_0_0
        word %%1_2_3_3_0_0_0_0
        word %%1_2_3_3_0_0_0_0
        
        word %%0_0_0_0_1_2_3_3 ' $BA
        word %%0_0_0_0_1_2_3_3
        word %%0_0_0_0_1_2_3_3
        word %%0_0_0_0_1_2_3_3
        word %%0_0_0_0_1_2_3_3
        word %%0_0_0_0_1_2_3_3
        word %%0_0_0_0_1_2_3_3
        word %%0_0_0_0_1_2_3_3

        word %%0_0_0_0_1_2_3_3 ' $BB
        word %%0_0_0_0_1_2_2_2
        word %%0_0_0_0_1_2_2_2
        word %%0_0_0_0_1_2_3_2
        word %%0_0_0_0_1_2_3_2
        word %%0_0_0_0_1_2_3_2
        word %%0_0_0_0_1_2_3_2
        word %%0_0_0_0_1_2_3_3
         
        word %%3_3_3_3_3_3_3_3 ' $BC
        word %%2_3_2_3_3_3_2_3
        word %%2_3_2_2_3_2_2_3
        word %%3_3_2_2_2_2_2_3
        word %%3_3_2_3_2_3_2_3
        word %%3_3_2_3_3_3_2_3
        word %%3_3_2_3_3_3_2_3
        word %%3_3_3_3_3_3_3_3

        word %%3_3_3_3_0_0_0_0 ' $BD
        word %%3_3_3_3_3_0_0_0
        word %%3_3_3_3_3_3_0_0
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3
        
        word %%3_3_3_3_3_3_3_3 ' $BE
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_3
        word %%3_3_3_3_3_3_3_0
        word %%3_3_3_3_3_3_0_0
        word %%3_3_3_3_3_0_0_0
        
        word %%3_3_3_3_0_0_0_0 ' $BF
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        
        word %%0_0_0_0_3_3_3_3 ' $C0
        word %%0_0_0_0_3_3_3_3
        word %%0_0_0_0_3_3_3_3
        word %%0_0_0_0_3_3_3_3
        word %%0_0_0_0_3_3_3_3
        word %%0_0_0_0_3_3_3_3
        word %%0_0_0_0_3_3_3_3
        word %%0_0_0_0_3_3_3_3

        word %%0_0_0_0_0_0_0_0 ' $C1
        word %%3_0_0_0_0_0_0_0
        word %%3_3_0_0_0_0_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        
        word %%3_3_3_3_0_0_0_0 ' $C2
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_3_0_0_0_0
        word %%3_3_3_0_0_0_0_0
        word %%3_3_0_0_0_0_0_0
        word %%3_0_0_0_0_0_0_0
        
        word %%1_2_3_3_0_0_0_0 ' $C3
        word %%1_2_3_3_0_0_0_0
        word %%1_2_3_3_0_0_0_0
        word %%1_2_3_3_0_0_0_0
        word %%1_2_3_3_0_0_0_0
        word %%1_2_3_3_0_0_0_0
        word %%1_2_3_3_0_0_0_0
        word %%1_2_3_3_0_0_0_0
        
        word %%0_0_0_0_1_2_3_3 ' $C4
        word %%0_0_0_0_1_2_3_3
        word %%0_0_0_0_1_2_3_3
        word %%0_0_0_0_1_2_3_3
        word %%0_0_0_0_1_2_3_3
        word %%0_0_0_0_1_2_3_3
        word %%0_0_0_0_1_2_3_3
        word %%0_0_0_0_1_2_3_3

        word %%0_0_0_0_1_2_3_3 ' $C5
        word %%0_0_0_0_1_2_2_2
        word %%0_0_0_0_1_2_2_2
        word %%0_0_0_0_1_2_3_2
        word %%0_0_0_0_1_2_3_2
        word %%0_0_0_0_1_2_3_2
        word %%0_0_0_0_1_2_3_2
        word %%0_0_0_0_1_2_3_3

        word %%0_0_0_0_0_0_0_0 ' $C6 unused... padding for second chargen (see below)
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $C7
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $C8
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $C9
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $CA
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $CB
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $CC
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $CD
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $CE
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $CF
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
' this is basically the same character generator, but using
' a different color (I need this for the title screen). Either
' this, or separate color attributes for each cell of the
' screen. We have still some space here, so...

        word %%0_0_0_0_0_0_0_0 ' $D0 '0'
        word %%0_2_2_2_2_2_0_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%0_2_2_2_2_2_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $D1 '1'
        word %%0_0_0_2_2_0_0_0
        word %%0_0_2_2_2_0_0_0
        word %%0_0_2_2_2_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_2_2_2_2_2_2_0
        word %%0_2_2_2_2_2_2_0
         
        word %%0_0_0_0_0_0_0_0 ' $D2 '2'
        word %%0_2_2_2_2_2_0_0
        word %%2_2_0_0_2_2_2_0
        word %%0_0_0_2_2_2_0_0
        word %%0_0_2_2_2_0_0_0
        word %%0_2_2_2_0_0_0_0
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_2_2_2_2_0
         
        word %%0_0_0_0_0_0_0_0 ' $D3
        word %%0_2_2_2_2_2_2_0
        word %%0_0_0_0_2_2_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_0_2_2_0_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%0_2_2_2_2_2_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $D4
        word %%2_2_0_0_0_0_0_0
        word %%2_2_0_0_0_0_0_0
        word %%2_2_0_2_2_2_0_0
        word %%2_2_2_2_2_2_2_0
        word %%0_0_0_2_2_2_0_0
        word %%0_0_0_2_2_2_0_0
        word %%0_0_0_2_2_2_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $D5
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_2_2_2_0_0
        word %%0_0_0_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%0_2_2_2_2_2_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $D6
        word %%0_2_2_2_2_2_0_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_2_2_2_0_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%0_2_2_2_2_2_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $D7
        word %%2_2_2_2_2_2_2_0
        word %%2_0_0_0_2_2_2_0
        word %%0_0_0_2_2_2_0_0
        word %%0_0_2_2_2_0_0_0
        word %%0_2_2_2_0_0_0_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $D8
        word %%0_2_2_2_2_2_0_0
        word %%2_2_2_0_0_2_2_0
        word %%0_2_2_2_2_2_0_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%0_2_2_2_2_2_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $D9
        word %%0_2_2_2_2_2_0_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%0_2_2_2_2_2_2_0
        word %%0_0_0_2_2_2_0_0
        word %%0_0_2_2_2_0_0_0
        word %%0_2_2_2_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $DA ':'
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $DB ';'
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_2_2_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $DC '/'
        word %%0_0_0_0_0_0_2_0
        word %%0_0_0_0_0_2_2_0
        word %%0_0_0_0_2_2_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_2_2_0_0_0_0
        word %%0_2_2_0_0_0_0_0
        word %%2_2_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $DD
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $DE
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $DF
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $E0 space
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $E1 'A'
        word %%0_0_2_2_2_0_0_0
        word %%0_2_2_2_2_2_0_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
         
        word %%0_0_0_0_0_0_0_0 ' $E2
        word %%2_2_2_2_2_2_0_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_2_2_2_0_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_2_2_2_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $E3
        word %%0_2_2_2_2_2_0_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%0_2_2_2_2_2_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $E4
        word %%2_2_2_2_2_0_0_0
        word %%2_2_2_0_2_2_0_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_2_2_0_0
        word %%2_2_2_2_2_2_0_0
        word %%2_2_2_2_2_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $E5
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_2_2_2_0_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_2_2_2_2_0
         
        word %%0_0_0_0_0_0_0_0 ' $E6
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_2_2_2_0_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $E7
        word %%0_2_2_2_2_2_2_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_0_2_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%0_2_2_2_2_2_2_0
         
        word %%0_0_0_0_0_0_0_0 ' $E8
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
         
        word %%0_0_0_0_0_0_0_0 ' $E9
        word %%0_2_2_2_2_2_0_0
        word %%0_0_2_2_2_0_0_0
        word %%0_0_2_2_2_0_0_0
        word %%0_0_2_2_2_0_0_0
        word %%0_0_2_2_2_0_0_0
        word %%0_2_2_2_2_2_0_0
        word %%0_2_2_2_2_2_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $EA
        word %%0_0_0_0_0_2_2_0
        word %%0_0_0_0_0_2_2_0
        word %%0_0_0_0_0_2_2_0
        word %%0_0_0_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%0_2_2_2_2_2_2_0
        word %%0_0_2_2_2_2_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $EB
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_2_2_0_0
        word %%2_2_2_2_2_0_0_0
        word %%2_2_2_2_2_0_0_0
        word %%2_2_2_0_2_2_0_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
         
        word %%0_0_0_0_0_0_0_0 ' $EC
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_2_2_2_2_0
         
        word %%0_0_0_0_0_0_0_0 ' $ED
        word %%2_2_0_0_0_2_2_0
        word %%2_2_2_0_2_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%2_2_0_2_0_2_2_0
        word %%2_2_0_0_0_2_2_0
        word %%2_2_0_0_0_2_2_0
        word %%2_2_0_0_0_2_2_0
         
        word %%0_0_0_0_0_0_0_0 ' $EE
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_2_0_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_0_2_2_2_0
        word %%2_2_2_0_0_2_2_0
         
        word %%0_0_0_0_0_0_0_0 ' $EF
        word %%0_2_2_2_2_2_0_0
        word %%2_2_0_0_0_2_2_0
        word %%2_2_0_0_0_2_2_0
        word %%2_2_0_0_0_2_2_0
        word %%2_2_0_0_0_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%0_2_2_2_2_2_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $F0
        word %%2_2_2_2_2_2_0_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_2_2_2_0_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_0_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $F1
        word %%0_0_2_2_2_2_0_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_2_2_0_0
        word %%0_2_2_2_2_2_2_0
        word %%0_0_2_2_0_2_2_0
         
        word %%0_0_0_0_0_0_0_0 ' $F2
        word %%2_2_2_2_2_2_0_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_2_2_2_0_0
        word %%2_2_2_2_2_2_0_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
         
        word %%0_0_0_0_0_0_0_0 ' $F3
        word %%0_2_2_2_2_2_0_0
        word %%2_2_2_0_0_0_0_0
        word %%0_2_2_2_2_2_0_0
        word %%0_0_0_0_0_2_2_0
        word %%0_0_0_0_0_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_2_2_2_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $F4
        word %%2_2_2_2_2_2_2_0
        word %%0_0_2_2_2_0_0_0
        word %%0_0_2_2_2_0_0_0
        word %%0_0_2_2_2_0_0_0
        word %%0_0_2_2_2_0_0_0
        word %%0_0_2_2_2_0_0_0
        word %%0_0_2_2_2_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $F5
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_2_2_2_2_0
         
        word %%0_0_0_0_0_0_0_0 ' $F6
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%0_2_2_2_2_2_0_0
        word %%0_0_2_2_2_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $F7
        word %%2_2_0_0_0_2_2_0
        word %%2_2_0_0_0_2_2_0
        word %%2_2_0_2_0_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_0_2_2_2_0
        word %%2_2_0_0_0_2_2_0
         
        word %%0_0_0_0_0_0_0_0 ' $F8
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%0_0_2_2_2_2_0_0
        word %%0_0_2_2_2_2_0_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
        word %%2_2_2_0_0_2_2_0
         
        word %%0_0_0_0_0_0_0_0 ' $F9
        word %%2_2_0_0_0_2_2_0
        word %%2_2_0_0_0_2_2_0
        word %%0_2_2_2_2_2_0_0
        word %%0_0_2_2_2_0_0_0
        word %%0_0_2_2_2_0_0_0
        word %%0_0_2_2_2_0_0_0
        word %%0_0_2_2_2_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $FA
        word %%2_2_2_2_2_2_2_0
        word %%0_0_0_2_2_2_0_0
        word %%0_0_2_2_2_0_0_0
        word %%0_2_2_2_0_0_0_0
        word %%2_2_2_0_0_0_0_0
        word %%2_2_2_2_2_2_2_0
        word %%2_2_2_2_2_2_2_0
         
        word %%0_0_0_0_0_0_0_0 ' $FB '('
        word %%0_0_0_0_2_2_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_0_2_2_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $FC diamond
        word %%0_0_0_2_0_0_0_0
        word %%0_0_2_0_2_0_0_0
        word %%0_2_2_2_2_2_0_0
        word %%2_0_0_0_0_0_2_0
        word %%0_2_2_2_2_2_0_0
        word %%0_0_2_0_2_0_0_0
        word %%0_0_0_2_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $FD ')'
        word %%0_0_2_2_0_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_2_2_0_0_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $FE
        word %%0_0_2_0_0_2_0_0
        word %%0_0_2_2_2_2_0_0
        word %%0_2_0_2_2_0_2_0
        word %%0_2_0_2_2_0_2_0
        word %%0_0_2_2_2_2_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_2_2_2_2_0_0
         
        word %%0_0_0_0_0_0_0_0 ' $FF ','
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_0_0_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_0_2_2_0_0_0
        word %%0_0_2_2_0_0_0_0
        