con

  
  JOY_CLK = 16
  JOY_LCH = 17
  JOY_DATAOUT0 = 19
  JOY_DATAOUT1 = 18

  GP_RIGHT  = %00000001
  GP_LEFT   = %00000010
  GP_DOWN   = %00000100
  GP_UP     = %00001000
  GP_START  = %00010000
  GP_SELECT = %00100000
  GP_B      = %01000000
  GP_A      = %10000000

pub read

  'return nes_bits
  return process

pri process : nes_bits | i, bits

  ' set I/O ports to proper direction
  ' P3 = JOY_CLK    (4)
  ' P4 = JOY_SH/LDn (5) 
  ' P5 = JOY_DATAOUT0 (6)
  ' P6 = JOY_DATAOUT1 (7)
  ' NES Bit Encoding

  ' step 1: set I/Os
  ' step 1: set I/Os
DIRA [JOY_CLK] := 1 ' output
DIRA [JOY_LCH] := 1 ' output
DIRA [JOY_DATAOUT0] := 0 ' input
DIRA [JOY_DATAOUT1] := 0 ' input1

' step 2: set clock and latch to 0
OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0
'Delay(1)

' step 3: set latch to 1
OUTA [JOY_LCH] := 1 ' JOY_SH/LDn = 1
'Delay(1)

' step 4: set latch to 0
OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0

' step 5: read first bit of each game pad

' data is now ready to shift out
' first bit is ready 
nes_bits := 0

' left controller
nes_bits := INA[JOY_DATAOUT0] | (INA[JOY_DATAOUT1] << 8)

' step 7: read next 7 bits
repeat i from 0 to 6
 OUTA [JOY_CLK] := 1 ' JOY_CLK = 1
 'Delay(1)             
 OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
 nes_bits := (nes_bits << 1)
 nes_bits := nes_bits | INA[JOY_DATAOUT0] | (INA[JOY_DATAOUT1] << 8)

 'Delay(1)             
' invert bits to make positive logic
nes_bits := (!nes_bits & $FFFF)
        