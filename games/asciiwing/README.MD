ASCII Wing
==========

## Video
[![](http://img.youtube.com/vi/5pfVgepYIvE/0.jpg)](http://www.youtube.com/watch?v=5pfVgepYIvE "Video")

## Info
- Type: Game
- Genre: Vertical Shooter
- Author(s): Rick Cassidy
- First release: 2008
- Improved version: Yes
- Players: 1
- Special requirements: None
- Video formats: NTSC
- Inputs: Gamepad,Keyboard
- License: ???

## Description
The evil **{v}** are invading earth! You have been chosen to pilot the last **/^\\** spaceship. You are textkinds last hope.

Or something along those lines, I don't know.


## Controls
|Gamepad|Keyboard|Action|
|------|---------|------|
|D-Pad|Arrow Keys|Move|
|A|Right CTRL|Shoot|
|Start|Enter|Pause|
|Select|Tab|Pause, but different?|

## TODOs
- ?
