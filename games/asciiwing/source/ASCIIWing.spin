{
ASCII Wing

'+--------------------------------------+
'|         __   ___   __  __  __        |
'|        (  ) / __) / _)(  )(  )       |
'|        /__\ \__ \( (_  )(  )(        |
'|       (_)(_)(___/ \__)(__)(__)       |
'|         _    _  __  _  _  __         |
'|        ( \/\/ )(  )( \( )/ _)        |
'|         \    /  )(  )  (( (/\        |
'|          \/\/  (__)(_)\_)\__/        |
'|                                      |
'|             CassLan 2008             |
'|                                      |
'+--------------------------------------+

For the Record: Started 6/25/08
                Rick Cassidy

Enjoy ;)
}                                 
CON

  _clkmode = xtal1 + pll16x
  _xinfreq = 5_000_000
  ' NES Joystick PINS....
  CLK   = 16
  LATCH = 17
  DATA  = 19
  ' Single Channel Audio Pin
  SOUNDPIN = 10
  
OBJ

  Text  : "tv_text_casslan" 
  Nes   : "NES"
  Freq  : "Synth"
  rr    : "realrandom"
  hdmf  : "epm_hdmf_driver_010.spin"

VAR

  LONG Frequency , score
  Word VideoPage[520]
  WORD CustomChar, Charx, Chary, CHarLoc, BullLoc, StarChar
  Byte type, keyval, BullX[40],BullY[40], BullS[40], MaxBull, BulletType,h, i, j, k
  Long Ammo[5]
  Byte EnemyS[100], EnemyX[100], EnemyY[100], EnemyH[100], MaxEnemy
  Byte StarY[40], Lives, Shield, LastEnemyHit, Level, LUF, l
PUB start
  
  hdmf.start(0)                  'start sound driver
  rr.start                      'start realrandom
  Text.start(12)                 'start text display
  Text.setcolors(@palette)
  Type := NES.Init(LATCH, DATA, CLK)
  OpeningScreen

PUB OpeningScreen
  Text.out($00)
  hdmf.stop_song
  Text.str(string("+--------------------------------------+"))
  Text.str(string("|         __   ___   __  __  __        |"))
  Text.str(string("|        (  ) / __) / _)(  )(  )       |"))
  Text.str(string("|        /__\ \__ \( (_  )(  )(        |"))
  Text.str(string("|       (_)(_)(___/ \__)(__)(__)       |"))
  Text.str(string("|         _    _  __  _  _  __         |"))
  Text.str(string("|        ( \/\/ )(  )( \( )/ _)        |"))
  Text.str(string("|         \    /  )(  )  (( (/\        |"))
  Text.str(string("|          \/\/  (__)(_)\_)\__/        |"))
  Text.str(string("|                                      |"))
  Text.str(string("|             CassLan 2008             |"))
  Text.str(string("+--------------------------------------+"))
  hdmf.play_song(@opening_data, 0 {no flags})
  repeat
    repeat k from 0 to 4
      DoDelay
      text.out($0A)
      text.out(9) 'set the x pos
      text.out($0B)
      text.out(12) 'set the y pos
      text.out($0C)
      text.out(k)
      text.str(string("Press START to Continue"))
    if NES.BtnPressed(NES#START)
      repeat until !NES.BtnPressed(NES#START)
      Text.out($00)
      Frequency := 0
      Lives := 0
      Level := 0
      Score := 0
      Shield := 3
      MaxBull := 10  '''really means 1
      'MaxEnemy := 10    ''Now Derived from LevelData DAT
      StartLevel   
    
PUB StartLevel

  CHarx:=20
  CHary:=10
  hdmf.stop_song
  InitEnemies
  InitStars


  Repeat j from 0 to 10

    DoDelay
    DrawBackGround
    DrawHUD
    UpdateScreen
    repeat k from 0 to 4
      DoDelay
      text.out($0C)
      text.out(k)
      text.out($0A)
      text.out(7) 'set the x pos
      text.out($0B)
      text.out(5) 'set the y pos
      text.str(string("┌─────────────────────────┐"))
      text.out($0A)
      text.out(7) 'set the x pos
      text.out($0B)
      text.out(6) 'set the y pos
      text.str(string("│ Get READY for Level - "))
      text.hex(Level,1)
      text.str(string(" │"))
      text.out($0A)
      text.out(7) 'set the x pos
      text.out($0B)
      text.out(7) 'set the y pos
      text.str(string("└─────────────────────────┘"))
    Text.setcolors(@palette)
    'DrawEnemies
    'DrawCharacter
    'DrawBullets
    'CheckCollisions
    'GetInput
    'MoveEnemies
      
''''''''''''''''''''''''''''''''''
''''Main Game Loop''''''''''''''''
''''''''''''''''''''''''''''''''''
  Repeat
    DoDelay
    DrawBackGround
    DrawEnemies
    DrawCharacter
    DrawBullets
    DrawHUD
    UpdateScreen
    Text.setcolors(@palette)
    CheckCollisions
    IsLevelOver
    
    GetInput
    MoveEnemies
    
'''''''''''''''''''''''''''''''''
''''End Main Game Loop'''''''''''
'''''''''''''''''''''''''''''''''             
PUB DrawHUD
  '''''SCORE''''''''''
  ''''''''''''''''''''
  text.out($0A)
  text.out(0) 'set the x pos
  text.out($0B)
  text.out(12) 'set the y pos
  text.out($0C)
  text.out(1)
  text.hex(score,8)

  '''''LIVES'''''''''''
  '''''''''''''''''''''
  text.str(string("  x"))
  text.hex(Lives,2)

  ''''WEAPON INFO''''''''
  '''''''''''''''''''''''
  text.str(string("   -"))
  if BulletType == 0
    text.str(string("∞"))
  BullLoc := (12*40) + 20
  wordmove (text.screenAddress+(24*40+30),@Bullet[BulletType],1)               

  '''''ENEMY INFO (Last ENEMY HIT)
  ''''''''''''''''''''''''''''''''
  text.str(string("  E-"))
  text.hex(EnemyH[LastEnemyHit],3)

  '''''SHEILD INFO
  ''''''''''''''''''''''''''''''''
  'text.str(string(" S-"))
  'text.hex(Shield,2)
  
  
  
  wordmove (@VideoPage[12*40],text.screenAddress+(24*40),40)

  
PUB DoDelay
  waitcnt ((2500000) + cnt)

PUB SideMenu

  repeat until !NES.BtnPressed(NES#SELECT)
  repeat until NES.BtnPressed(NES#SELECT)
  '
  '    ┌
  '    │
  '    └
  '
  '


  repeat until !NES.BtnPressed(NES#SELECT)

  
PUB CheckCollisions
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ''''''First One Checks Character Bullets Against Enemies
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''
  repeat j from 0 to MaxEnemy
    if EnemyS[j] <> 0
      repeat i from 0 to MaxBull
        if BullS[i] <> 0
          if BullY[i] == EnemyY[j]
            if (BullX[i] > EnemyX[j]-2) AND (BullX[i] < EnemyX[j]+2)
              EnemyH[j] --
              BullS[i] := 0
              text.out($0A)
              text.out(BullX[i]) 'set the x pos
              text.out($0B)
              text.out(BullY[i]) 'set the y pos
              text.out($0C)
              text.out(5)
              text.str(string("."))
              repeat Frequency from 10000 to 100 step 1000
                Freq.Synth("A",SOUNDPIN, Frequency)
              Frequency :=0
              Freq.Synth("A",SOUNDPIN, Frequency)
              if EnemyH[j] == 0
              
              ''''fantastic explosion and death sequence comming soon
                Sound_EnemyDeath
                EnemyS[j] := 0
                score := score + 5
              else
                LastEnemyHit :=j
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  '''''''This One Checks Character Body Against Enemie SHips (Kamakazie)
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  repeat j from 0 to MaxEnemy
    if EnemyS[j] == 1       ''''''Low Enemy Type Kamikazie
      if EnemyY[j] == CharY
        if EnemyX[j] > (CharX - 3) AND ENemyX[j] < (CHarX + 3)
          repeat Frequency from 10000 to 100 step 100
            Freq.Synth("A",SOUNDPIN, Frequency)
          Frequency :=0
          Freq.Synth("A",SOUNDPIN, Frequency)
          EnemyS[j] := 0
          score := score + 5
          ShipHit
          '''''''''' Enemy Dies and You are hit!!!!!!!!!!! 

          
PUB IsLevelOver
  k := 0
  repeat j from 0 to MaxEnemy
    if EnemyS[j] == 1
      k := 1
  if k == 0
    LevelUp

PUB LevelUp
  '''''''''''
  '''''''''''
  '''We should Play a "Level Complete" Sound Here
  hdmf.play_song(@levelup_data, 0 {no flags})
  LUF := 0
  Repeat
    If LUF < 2
      DoDelay
    DoDelay
    DrawBackGround
    DrawCharacter
    DrawHUD
    UpdateScreen
    Text.setcolors(@palette)
    if LUF == 0
      if CharY < 11
        CharY ++
      else
        LUF := 1
        
    if LUF == 1
      if CharX < 20
        CharX ++
      if CharX > 20
        CharX --
      if CharX == 20
        LUF := 2
     
    if LUF == 2
      StarChar := @Star[1]
      if CharY > 0
        CharY --
      else
        LUF := 3
        l := 0

    if LUF == 3
      if l == 50
        LUF := 0
        Level ++
        StarChar := @Star
        StartLevel
      else
        l ++
        
PUB ShipHit
  '''''''''''''''''''''''''''''''''''''''''''''
  ''Basicly we will take some sheild off the ship and see if its dead
  '''''''''''''''''''''''''''''''''''''''''''''
  Shield --
  if Shield == 0    '''We are Dead Now
    ''''''''''''''We should have some sound and explosion graphic here
    Lives --
    Shield := 2
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  '''''Next Life or Game Over ??'''''''''''''''''''''''''''''''''''''''
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  if Lives == 255
    GameOver

PUB GameOver
  Text.out($00)
  text.out($0A)
  text.out(15) 'set the x pos
  text.out($0B)
  text.out(2) 'set the y pos
  text.out($0C)
  text.out(3)
  text.str(string("GAME  OVER"))
  text.out($0A)
  text.out(11) 'set the x pos
  text.out($0B)
  text.out(5) 'set the y pos
  text.out($0C)
  text.out(3)
  text.str(string("Score:    "))
  text.hex(score,8)
  hdmf.play_song(@gameover_data, 0 {no flags})
  'repeat Frequency from 5000 to 100
    'Freq.Synth("A",SOUNDPIN, Frequency)
  'repeat i from 0 to 8
    'repeat Frequency from 5000 to 10000 step 100
      'Freq.Synth("A",SOUNDPIN, Frequency)
  'Frequency :=0
  'Freq.Synth("A",SOUNDPIN, Frequency)
    
  text.out($0A)
  text.out(9) 'set the x pos
  text.out($0B)
  text.out(12) 'set the y pos
  text.out($0C)
  text.out(3)
  text.str(string("Press START to Continue"))

    
  repeat
    repeat k from 0 to 4
      DoDelay
      text.out($0A)
      text.out(9) 'set the x pos
      text.out($0B)
      text.out(12) 'set the y pos
      text.out($0C)
      text.out(k)
      text.str(string("Press START to Continue"))
    if NES.BtnPressed(NES#START)
      repeat until !NES.BtnPressed(NES#START)
      OpeningScreen

PUB DrawBullets
  repeat i from 0 to MaxBull
    If BullS[i]==1
      If BullY[i]> 0
        BullY[i] --
        BullLoc := (BullY[i]*40) + BullX[i]
        wordmove (@VideoPage[BullLoc],@Bullet,1)
      Else
        BullS[i] :=0

PUB DrawBackGround
  wordfill (@VideoPage,%0000001000100000,520)
  repeat i from 0 to 39
    StarY[i]++
    if StarY[i]>11
      Stary[i] := 0
    CharLoc := (StarY[i]*40) + i
      wordmove (@VideoPage[CharLoc],StarChar,1)
PUB DrawEnemies
  repeat i from 0 to MaxEnemy
    If EnemyS[i] <> 0
      CharLoc := (EnemyY[i]*40) + (EnemyX[i]-1)
      wordmove (@VideoPage[CharLoc],@Enemy,3)

PUB Sound_EnemyDeath

  repeat Frequency from 800 to 300 step 100
    Freq.Synth("A",SOUNDPIN, Frequency)
  repeat Frequency from 800 to 300 step 50
    Freq.Synth("A",SOUNDPIN, Frequency)
  repeat Frequency from 800 to 300 step 20
    Freq.Synth("A",SOUNDPIN, Frequency)
  repeat Frequency from 800 to 300 step 10
    Freq.Synth("A",SOUNDPIN, Frequency)
  Frequency :=0
  Freq.Synth("A",SOUNDPIN, Frequency)

PUB MoveEnemies
  repeat i from 0 to MaxEnemy
    If EnemyS[i] == 1      ''' Low Level Enemy Type Movement
      j := rr.random >> 27
        if j < 3
          EnemyX[i] --
          if EnemyX[i] < 2
            EnemyX[i] := 2

        if j > 28
          EnemyX[i] ++
          if EnemyX[i] > 38
            EnemyX[i] := 38
            
      j := rr.random >> 27
        if j >30
          EnemyY[i] ++
          if EnemyY[i] > 11
            EnemyS[i] := 0

PUB Sound_Pause

  repeat Frequency from 300 to 800 step 10
    Freq.Synth("A",SOUNDPIN, Frequency)
  repeat Frequency from 300 to 800 step 20
    Freq.Synth("A",SOUNDPIN, Frequency)
  repeat Frequency from 300 to 800 step 50
    Freq.Synth("A",SOUNDPIN, Frequency)
  repeat Frequency from 300 to 800 step 100
    Freq.Synth("A",SOUNDPIN, Frequency)
  Frequency :=0
  Freq.Synth("A",SOUNDPIN, Frequency)

PUB Pause

  repeat until !NES.BtnPressed(NES#START)

  Sound_Pause

  repeat Frequency from 300 to 800 step 10
    Freq.Synth("A",SOUNDPIN, Frequency)
  repeat Frequency from 300 to 800 step 20
    Freq.Synth("A",SOUNDPIN, Frequency)
  repeat Frequency from 300 to 800 step 50
    Freq.Synth("A",SOUNDPIN, Frequency)
  repeat Frequency from 300 to 800 step 100
    Freq.Synth("A",SOUNDPIN, Frequency)
  Frequency :=0
  Freq.Synth("A",SOUNDPIN, Frequency)

  'PERFECT WEAPON SOUND
  'repeat Frequency from 1000 to 500 step 10
  '  Freq.Synth("A",SOUNDPIN, Frequency)
  'Frequency :=0
  'Freq.Synth("A",SOUNDPIN, Frequency)

  repeat until NES.BtnPressed(NES#START)
    repeat k from 0 to 4
      DoDelay
      text.out($0C)
      text.out(k)
      text.out($0A)
      text.out(7) 'set the x pos
      text.out($0B)
      text.out(5) 'set the y pos
      text.str(string("┌─────────────────────────┐"))
      text.out($0A)
      text.out(7) 'set the x pos
      text.out($0B)
      text.out(6) 'set the y pos
      text.str(string("│ Press START to Continue │"))
      text.out($0A)
      text.out(7) 'set the x pos
      text.out($0B)
      text.out(7) 'set the y pos
      text.str(string("└─────────────────────────┘"))
  repeat until !NES.BtnPressed(NES#START)

  Sound_Pause
  

PUB DrawCharacter
  '''''''Check SHield Value and Adjust Pallette Value'''''''''''''''''
  '''''''Also Check if LUF or Invins and Adjust Pallette Value''''''''
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  if Shield < 2
    if palette[0]==$BA
      bytefill (@palette,$03,1)
    else
      bytefill (@palette,$BA,1)

  if Shield => 2
      bytefill (@palette,$07,1)

  if LUF == 2
    bytefill (@palette,$1E,1)

  if LUF == 3
    bytefill (@palette,$11,1) 
      
  CharLoc := (Chary*40) + (CHarx-1)
  wordmove (@VideoPage[CharLoc],@Character,3)

PUB GetInput

  'keyval := kb.key

  if NES.BtnPressed(NES#LEFT) or NES.BtnPressed(NES#LEFT & NES#A) or NES.BtnPressed(NES#LEFT & NES#DOWN) or NES.BtnPressed(NES#LEFT & NES#UP) ' left arrow key
    if Charx > 1
      Charx --

  if NES.BtnPressed(NES#RIGHT)or NES.BtnPressed(NES#RIGHT & NES#A) or NES.BtnPressed(NES#RIGHT & NES#DOWN) or NES.BtnPressed(NES#RIGHT & NES#UP)' left arrow key
    if Charx <38
      Charx ++

  if NES.BtnPressed(NES#DOWN) or NES.BtnPressed(NES#DOWN & NES#A) or NES.BtnPressed(NES#DOWN & NES#LEFT) or NES.BtnPressed(NES#DOWN & NES#RIGHT) ' left arrow key
    if Chary <11
      Chary ++

  if NES.BtnPressed(NES#UP) or NES.BtnPressed(NES#UP & NES#A) or NES.BtnPressed(NES#UP & NES#LEFT) or NES.BtnPressed(NES#UP & NES#RIGHT) ' left arrow key
    if Chary >0
      Chary --

  if NES.BtnPressed(NES#A) or  NES.BtnPressed(NES#A & NES#LEFT) or  NES.BtnPressed(NES#A & NES#RIGHT) or NES.BtnPressed(NES#A & NES#DOWN) or NES.BtnPressed(NES#A & NES#UP) or NES.BtnPressed(NES#A & NES#UP & NES#LEFT) or NES.BtnPressed(NES#A & NES#UP & NES#RIGHT) or NES.BtnPressed(NES#A & NES#DOWN & NES#LEFT) or NES.BtnPressed(NES#A & NES#DOWN & NES#RIGHT)' left arrow keyA Button
    repeat i from 0 to MaxBull
      if BullS[i] == 0
        BullS[i] := 1
        BullX[i] := Charx
        BullY[i] := Chary
        repeat Frequency from 1000 to 100 step 100
          Freq.Synth("A",SOUNDPIN, Frequency)
        Frequency :=0
        Freq.Synth("A",SOUNDPIN, Frequency)
        Quit
        
  if NES.BtnPressed(NES#START)
    Pause

  if NES.BtnPressed(NES#SELECT)
    SideMenu
PUB UpdateScreen
  WordMOVE (text.screenAddress,@VideoPage, 520)

  'text.out($0A)
  'text.out(1) 'set the x pos
  'text.out($0B)
  'text.out(1) 'set the y pos
  'text.dec(charx)

PUB InitStars

repeat i from 0 to 39
  StarY[i] := (rr.random >> 28)        '' Background STar
StarChar := @Star[LevelData[(Level*2)+1]]



PUB InitEnemies
  MaxEnemy := LevelData[(Level*2)]
repeat i from 0 to MaxEnemy
  EnemyS[i] := 1        '' Low Level Enemy
  EnemyX[i] := (rr.random >> 27)+1
  EnemyY[i] := (rr.random >> 29)
  EnemyH[i] := 10


DAT

LevelData     Byte 1, 0         'Level 0 - 2 Enemies..RegStars
              Byte 3, 0         'Level 1 - 4 Enemies..RegStars
              Byte 6, 0         'Level 2 - 7 Enemies..RegStars
              Byte 11, 0         'Level 3 - 7 Enemies..RegStars
              Byte 18, 0         'Level 4 - 7 Enemies..RegStars
              Byte 25, 0         'Level 5 - 7 Enemies..RegStars
              Byte 35, 0         'Level 6 - 7 Enemies..RegStars

Star          Word %0000101000101110         ' .  Reg Period Star
              Word %0000101001111100         ' |  Fast Stars

BackChar      Word %0000001000100000         ' BlankSpace

Character     Word %0000011000101110         '  /     /^\
              Word %0000001001011110         '  ^      
              Word %0000001001011100         '  \

Enemy         Word %0001111001111010         '  {     {v}
              Word %0001101001110110         '  v
              Word %0001111001111100         '  }

Bullet        Word %0001001000101110         ' .  Yellow Bullet
              Word %0000001000010000         ' Δ
              Word %0000001010111100         ' 
              Word %0000001000000110         ' A


palette byte $07, $12    '0 - Char Ship Color - Bright White on Black
        byte $03, $12    '1 - Star Color - Dim White on Black
        byte $08, $12    '2 - Bullet Color - Green on LightBlue
        byte $07, $12    '3 - Enemy Ship Color 1 - Bright White on Black
        byte $07, $12    '4 - Enemy Ship Color 2 - Flashing from Red
        byte $AA, $12    '5 - Bullet Hit Color - Red on Black
        byte $06, $02    '6 -
        byte $07, $03    '7 -

gameover_data
        byte    $02, $EF, $AD, $79, $35          'Instrument 0
        byte    $FF                              'End of Instrument list
        byte    $00, $03, $3F, $0F, $0C
        byte    $00, $23, $3F, $0F, $0C
        byte    $12, $02, $6E, $0F, $0C
        byte    $00, $22, $6E, $0F, $0C
        byte    $11, $02, $4B, $0F, $0C
        byte    $00, $22, $4B, $0F, $0C
        byte    $12, $03, $A4, $0F, $0C
        byte    $00, $23, $A4, $0F, $0C
        byte    $12, $03, $70, $0F, $0C
        byte    $00, $23, $70, $0F, $0C
        byte    $11, $02, $2A, $0F, $0C
        byte    $00, $22, $2A, $0F, $0C
        byte    $12, $02, $0B, $0F, $0C
        byte    $00, $22, $0B, $0F, $0C
        byte    $12, $03, $3F, $0F, $0C
        byte    $00, $23, $3F, $0F, $0C
        byte    $11, $02, $BA, $0F, $0C
        byte    $00, $22, $BA, $0F, $0C
        byte    $12, $02, $0B, $0F, $0C
        byte    $00, $22, $0B, $0F, $0C
        byte    $11, $01, $EE, $0F, $0C
        byte    $00, $21, $EE, $0F, $0C
        byte    $12, $03, $10, $0F, $0C
        byte    $00, $23, $10, $0F, $0C
        byte    $12, $02, $E4, $0F, $0C
        byte    $00, $22, $E4, $0F, $0C
        byte    $11, $01, $D2, $0F, $0C
        byte    $00, $21, $D2, $0F, $0C
        byte    $12, $01, $B8, $0F, $0C
        byte    $00, $21, $B8, $0F, $0C
        byte    $12, $02, $BA, $0F, $0C
        byte    $00, $22, $BA, $0F, $0C
        byte    $11, $02, $4B, $0F, $0C
        byte    $00, $22, $4B, $0F, $0C
        byte    $12, $01, $B8, $0F, $0C
        byte    $00, $21, $B8, $0F, $0C
        byte    $12, $01, $9F, $0F, $0C
        byte    $00, $21, $9F, $0F, $0C
        byte    $11, $02, $93, $0F, $0C
        byte    $00, $22, $93, $0F, $0C
        byte    $12, $02, $6E, $0F, $0C
        byte    $00, $22, $6E, $0F, $0C
        byte    $12, $01, $88, $0F, $0C
        byte    $00, $21, $88, $0F, $0C
        byte    $11, $01, $72, $0F, $0C
        byte    $00, $21, $72, $0F, $0C
        byte    $12, $02, $4B, $0F, $0C
        byte    $00, $22, $4B, $0F, $0C
        byte    $12, $01, $EE, $0F, $0C
        byte    $00, $21, $EE, $0F, $0C
        byte    $11, $01, $72, $0F, $0C
        byte    $00, $21, $72, $0F, $0C
        byte    $12, $01, $5D, $0F, $0C
        byte    $00, $21, $5D, $0F, $0C
        byte    $11, $02, $2A, $0F, $0C
        byte    $00, $22, $2A, $0F, $0C
        byte    $12, $02, $0B, $0F, $0C
        byte    $00, $22, $0B, $0F, $0C
        byte    $12, $01, $4A, $0F, $0C
        byte    $00, $21, $4A, $0F, $0C
        byte    $11, $01, $37, $0F, $0C
        byte    $00, $21, $37, $0F, $0C
        byte    $12, $01, $EE, $0F, $18
        byte    $00, $21, $EE, $0F, $18
        byte    $23, $01, $26, $0F, $2F
        byte    $00, $21, $26, $0F, $2F
        byte    $ff

victory_data
        byte    $02, $EF, $AD, $79, $35          'Instrument 0
        byte    $FF                              'End of Instrument list
        byte    $00, $00, $DC, $0C, $26
        byte    $00, $20, $57, $0C, $26
        byte    $00, $41, $06, $0C, $26
        byte    $26, $00, $E9, $0C, $26
        byte    $00, $21, $5D, $0C, $26
        byte    $00, $40, $6E, $0C, $26
        byte    $25, $61, $B8, $0C, $26
        byte    $00, $80, $83, $0C, $26
        byte    $00, $A1, $06, $0C, $26
        byte    $25, $01, $5D, $0C, $26
        byte    $00, $20, $6E, $0C, $26
        byte    $00, $42, $0B, $0C, $26
        byte    $26, $00, $4E, $0C, $26
        byte    $00, $21, $D2, $0C, $26
        byte    $00, $41, $88, $0C, $26
        byte    $26, $01, $B8, $0C, $26
        byte    $00, $20, $62, $0C, $26
        byte    $00, $41, $5D, $0C, $26
        byte    $25, $60, $75, $0C, $26
        byte    $00, $81, $D2, $0C, $26
        byte    $00, $A1, $88, $0C, $26
        byte    $25, $00, $4E, $0C, $26
        byte    $00, $21, $88, $0C, $26
        byte    $00, $41, $88, $0C, $26
        byte    $26, $01, $B8, $0C, $81, $2C
        byte    $00, $21, $72, $0C, $4B
        byte    $00, $40, $49, $0C, $26
        byte    $26, $40, $5C, $0C, $26
        byte    $25, $20, $6E, $0C, $26
        byte    $00, $61, $72, $0C, $26
        byte    $25, $40, $93, $0C, $26
        byte    $00, $81, $4A, $0C, $26
        byte    $26, $20, $49, $0C, $80, $96
        byte    $00, $41, $72, $0C, $80, $96
        byte    $ff



levelup_data
        byte    $02, $EF, $AD, $79, $35          'Instrument 0
        byte    $FF                              'End of Instrument list
        byte    $00, $01, $15, $0C, $07
        byte    $00, $20, $A5, $0C, $07
        byte    $07, $01, $4A, $0C, $07
        byte    $00, $20, $DC, $0C, $07
        byte    $07, $01, $B8, $0C, $07
        byte    $00, $21, $15, $0C, $07
        byte    $08, $02, $2A, $0C, $07
        byte    $00, $21, $26, $0C, $07
        byte    $07, $01, $4A, $0C, $07
        byte    $00, $22, $4B, $0C, $07
        byte    $07, $02, $93, $0C, $07
        byte    $00, $21, $B8, $0C, $07
        byte    $07, $03, $70, $0C, $07
        byte    $00, $22, $2A, $0C, $07
        byte    $07, $04, $55, $0C, $07
        byte    $00, $22, $4B, $0C, $07
        byte    $07, $02, $93, $0C, $07
        byte    $00, $21, $B8, $0C, $07
        byte    $07, $00, $C4, $0C, $20
        byte    $00, $20, $6E, $0C, $1B
        byte    $00, $41, $4A, $0C, $0B
        byte    $00, $60, $DC, $0C, $20
        byte    $00, $80, $8B, $0C, $20
        byte    $00, $A0, $75, $0C, $20
        byte    $00, $41, $B8, $0C, $0B
        byte    $20, $00, $68, $0C, $20
        byte    $00, $20, $6E, $0C, $1B
        byte    $00, $41, $4A, $0C, $0B
        byte    $00, $61, $B8, $0C, $0B
        byte    $0B, $42, $93, $0C, $2B
        byte    $00, $62, $2A, $0C, $2B
        byte    $15, $00, $6E, $0C, $10
        byte    $0B, $20, $68, $0C, $05
        byte    $0A, $00, $8B, $0C, $1C
        byte    $00, $20, $C4, $0C, $1C
        byte    $00, $80, $DC, $0C, $1C
        byte    $00, $A1, $EE, $0C, $1C
        byte    $00, $42, $4B, $0C, $1C
        byte    $00, $60, $75, $0C, $1C
        byte    $00, $00, $62, $0C, $15
        byte    $10, $00, $68, $0C, $05
        byte    $0D, $00, $8B, $0C, $1C
        byte    $00, $20, $75, $0C, $1C
        byte    $00, $40, $DC, $0C, $1C
        byte    $00, $60, $C4, $0C, $1C
        byte    $00, $80, $7B, $0C, $17
        byte    $00, $A2, $2A, $0C, $1C
        byte    $00, $81, $B8, $0C, $1C
        byte    $13, $00, $68, $0C, $05
        byte    $09, $01, $EE, $0C, $1C
        byte    $00, $20, $C4, $0C, $1C
        byte    $00, $40, $DC, $0C, $1C
        byte    $00, $60, $75, $0C, $1C
        byte    $00, $80, $93, $0C, $15
        byte    $00, $A0, $8B, $0C, $1C
        byte    $00, $81, $88, $0C, $1C
        byte    $12, $00, $68, $0C, $05
        byte    $0A, $00, $C4, $0C, $80, $80
        byte    $00, $20, $DC, $0C, $80, $80
        byte    $00, $40, $75, $0C, $80, $80
        byte    $00, $60, $8B, $0C, $80, $80
        byte    $00, $82, $2A, $0C, $80, $80
        byte    $00, $A1, $4A, $0C, $80, $80
        byte    $00, $00, $6E, $0C, $80, $80
        byte    $ff

opening_data
        byte    $02, $EF, $AD, $79, $35          'Instrument 0
        byte    $FF                              'End of Instrument list
        byte    $00, $03, $70, $0F, $0C
        byte    $0B, $23, $3F, $0F, $0C
        byte    $0C, $03, $10, $0F, $0C
        byte    $0B, $22, $E4, $0F, $0C
        byte    $0C, $03, $70, $0F, $0C
        byte    $00, $22, $BA, $0F, $0C
        byte    $0B, $43, $3F, $0F, $0C
        byte    $00, $62, $93, $0F, $0C
        byte    $0C, $02, $6E, $0F, $0C
        byte    $00, $23, $10, $0F, $0C
        byte    $0B, $42, $E4, $0F, $0C
        byte    $00, $62, $4B, $0F, $0C
        byte    $0C, $02, $BA, $0F, $0C
        byte    $00, $22, $2A, $0F, $0C
        byte    $0B, $42, $0B, $0F, $0C
        byte    $00, $62, $93, $0F, $0C
        byte    $0C, $01, $EE, $0F, $0C
        byte    $00, $22, $6E, $0F, $0C
        byte    $0C, $02, $4B, $0F, $0C
        byte    $00, $21, $D2, $0F, $0C
        byte    $0B, $41, $B8, $0F, $0C
        byte    $00, $63, $DC, $0F, $0C
        byte    $0C, $04, $17, $0F, $0C
        byte    $00, $21, $9F, $0F, $0C
        byte    $0B, $44, $55, $0F, $0C
        byte    $00, $61, $88, $0F, $0C
        byte    $0C, $01, $72, $0F, $0C
        byte    $00, $24, $97, $0F, $0C
        byte    $0B, $40, $57, $0F, $06
        byte    $00, $61, $37, $0F, $80, $B9
        byte    $00, $82, $6E, $0F, $80, $B9
        byte    $0C, $00, $57, $0F, $06
        byte    $0B, $00, $83, $0F, $06
        byte    $0C, $00, $83, $0F, $06
        byte    $0B, $00, $AF, $0F, $06
        byte    $0C, $00, $AF, $0F, $06
        byte    $0B, $00, $83, $0F, $06
        byte    $0C, $00, $83, $0F, $06
        byte    $0C, $00, $57, $0F, $06
        byte    $0B, $00, $57, $0F, $06
        byte    $0C, $00, $83, $0F, $06
        byte    $0B, $00, $83, $0F, $06
        byte    $0C, $00, $AF, $0F, $06
        byte    $0B, $00, $AF, $0F, $06
        byte    $0C, $00, $83, $0F, $06
        byte    $0B, $00, $83, $0F, $06
        byte    $0C, $00, $E9, $0F, $5C
        byte    $00, $21, $D2, $0F, $5C
        byte    $00, $40, $57, $0F, $06
        byte    $0B, $40, $57, $0F, $06
        byte    $0C, $40, $83, $0F, $06
        byte    $0B, $40, $83, $0F, $06
        byte    $0C, $40, $AF, $0F, $06
        byte    $0C, $40, $AF, $0F, $06
        byte    $0B, $40, $83, $0F, $06
        byte    $0C, $40, $83, $0F, $06
        byte    $0B, $01, $06, $0F, $2E
        byte    $00, $20, $57, $0F, $06
        byte    $00, $42, $0B, $0F, $2E
        byte    $0C, $20, $57, $0F, $06
        byte    $0B, $20, $83, $0F, $06
        byte    $0C, $20, $83, $0F, $06
        byte    $0B, $00, $AF, $0F, $06
        byte    $00, $21, $D2, $0F, $0F
        byte    $00, $40, $E9, $0F, $0F
        byte    $0C, $00, $AF, $0F, $06
        byte    $04, $21, $B8, $0F, $0F
        byte    $00, $40, $DC, $0F, $0F
        byte    $07, $00, $83, $0F, $06
        byte    $08, $01, $5D, $0F, $0F
        byte    $00, $20, $AF, $0F, $0F
        byte    $04, $40, $83, $0F, $06
        byte    $0B, $02, $BA, $0F, $80, $B9
        byte    $00, $20, $75, $0F, $06
        byte    $00, $41, $5D, $0F, $81, $71
        byte    $0C, $20, $75, $0F, $06
        byte    $0C, $20, $AF, $0F, $06
        byte    $0B, $20, $AF, $0F, $06
        byte    $0C, $20, $E9, $0F, $06
        byte    $0B, $20, $E9, $0F, $06
        byte    $0C, $20, $AF, $0F, $06
        byte    $0B, $20, $AF, $0F, $06
        byte    $0C, $20, $75, $0F, $06
        byte    $0B, $20, $75, $0F, $06
        byte    $0C, $20, $AF, $0F, $06
        byte    $0B, $20, $AF, $0F, $06
        byte    $0C, $20, $E9, $0F, $06
        byte    $0B, $20, $E9, $0F, $06
        byte    $0C, $20, $AF, $0F, $06
        byte    $0C, $20, $AF, $0F, $06
        byte    $0B, $00, $75, $0F, $06
        byte    $00, $22, $BA, $0F, $80, $B9
        byte    $0C, $00, $75, $0F, $06
        byte    $0B, $00, $AF, $0F, $06
        byte    $0C, $00, $AF, $0F, $06
        byte    $0B, $00, $E9, $0F, $06
        byte    $0C, $00, $E9, $0F, $06
        byte    $0B, $00, $AF, $0F, $06
        byte    $0C, $00, $AF, $0F, $06
        byte    $0B, $00, $E9, $0F, $06
        byte    $0C, $00, $E9, $0F, $06
        byte    $0B, $00, $AF, $0F, $06
        byte    $0C, $00, $AF, $0F, $06
        byte    $0C, $00, $E9, $0F, $06
        byte    $0B, $00, $E9, $0F, $06
        byte    $0C, $00, $AF, $0F, $06
        byte    $0B, $00, $AF, $0F, $06
        byte    $0C, $00, $57, $0F, $06
        byte    $00, $21, $37, $0F, $80, $B9
        byte    $00, $42, $6E, $0F, $80, $B9
        byte    $0B, $00, $57, $0F, $06
        byte    $0C, $00, $83, $0F, $06
        byte    $0B, $00, $83, $0F, $06
        byte    $0C, $00, $AF, $0F, $06
        byte    $0B, $00, $AF, $0F, $06
        byte    $0C, $00, $83, $0F, $06
        byte    $0B, $00, $83, $0F, $06
        byte    $0C, $00, $57, $0F, $06
        byte    $0C, $00, $57, $0F, $06
        byte    $0B, $00, $83, $0F, $06
        byte    $0C, $00, $83, $0F, $06
        byte    $0B, $00, $AF, $0F, $06
        byte    $0C, $00, $AF, $0F, $06
        byte    $0B, $00, $83, $0F, $06
        byte    $0C, $00, $83, $0F, $06
        byte    $0B, $00, $E9, $0F, $5C
        byte    $00, $60, $57, $0F, $06
        byte    $00, $81, $D2, $0F, $5C
        byte    $0C, $20, $57, $0F, $06
        byte    $0B, $20, $83, $0F, $06
        byte    $0C, $20, $83, $0F, $06
        byte    $0B, $20, $AF, $0F, $06
        byte    $0C, $20, $AF, $0F, $06
        byte    $0C, $20, $83, $0F, $06
        byte    $0B, $20, $83, $0F, $06
        byte    $0C, $02, $0B, $0F, $2E
        byte    $00, $21, $06, $0F, $2E
        byte    $00, $40, $57, $0F, $06
        byte    $0B, $40, $57, $0F, $06
        byte    $0C, $40, $83, $0F, $06
        byte    $0B, $40, $83, $0F, $06
        byte    $0C, $01, $D2, $0F, $0F
        byte    $00, $20, $E9, $0F, $0F
        byte    $00, $40, $AF, $0F, $06
        byte    $0B, $40, $AF, $0F, $06
        byte    $04, $00, $DC, $0F, $0F
        byte    $00, $21, $B8, $0F, $0F
        byte    $08, $40, $83, $0F, $06
        byte    $08, $01, $5D, $0F, $0F
        byte    $00, $20, $AF, $0F, $0F
        byte    $03, $40, $83, $0F, $06
        byte    $0C, $01, $5D, $0F, $81, $71
        byte    $00, $22, $BA, $0F, $80, $B9
        byte    $00, $40, $75, $0F, $06
        byte    $0B, $40, $75, $0F, $06
        byte    $0C, $40, $AF, $0F, $06
        byte    $0C, $40, $AF, $0F, $06
        byte    $0B, $40, $E9, $0F, $06
        byte    $0C, $40, $E9, $0F, $06
        byte    $0B, $40, $AF, $0F, $06
        byte    $0C, $40, $AF, $0F, $06
        byte    $0B, $40, $75, $0F, $06
        byte    $0C, $40, $75, $0F, $06
        byte    $0B, $40, $AF, $0F, $06
        byte    $0C, $40, $AF, $0F, $06
        byte    $0B, $40, $E9, $0F, $06
        byte    $0C, $40, $E9, $0F, $06
        byte    $0B, $40, $AF, $0F, $06
        byte    $0C, $40, $AF, $0F, $06
        byte    $0C, $20, $75, $0F, $06
        byte    $00, $42, $BA, $0F, $80, $B9
        byte    $0B, $20, $75, $0F, $06
        byte    $0C, $20, $AF, $0F, $06
        byte    $0B, $20, $AF, $0F, $06
        byte    $0C, $20, $E9, $0F, $06
        byte    $0B, $20, $E9, $0F, $06
        byte    $0C, $20, $AF, $0F, $06
        byte    $0B, $20, $AF, $0F, $06
        byte    $0C, $20, $E9, $0F, $06
        byte    $0B, $20, $E9, $0F, $06
        byte    $0C, $20, $AF, $0F, $06
        byte    $0B, $20, $AF, $0F, $06
        byte    $0C, $20, $E9, $0F, $06
        byte    $0C, $20, $E9, $0F, $06
        byte    $0B, $20, $AF, $0F, $06
        byte    $0C, $20, $AF, $0F, $06
        byte    $0B, $00, $49, $0F, $06
        byte    $0C, $00, $49, $0F, $06
        byte    $0B, $00, $93, $0F, $06
        byte    $00, $22, $4B, $0F, $0C
        byte    $00, $40, $6E, $0F, $06
        byte    $0C, $00, $93, $0F, $06
        byte    $00, $20, $6E, $0F, $06
        byte    $00, $42, $4B, $0F, $0C
        byte    $0B, $00, $93, $0F, $06
        byte    $00, $22, $BA, $0F, $0C
        byte    $00, $60, $AF, $0F, $06
        byte    $0C, $00, $93, $0F, $06
        byte    $00, $22, $BA, $0F, $0C
        byte    $00, $40, $AF, $0F, $06
        byte    $0B, $03, $10, $0F, $0C
        byte    $00, $40, $6E, $0F, $06
        byte    $00, $60, $C4, $0F, $06
        byte    $0C, $00, $6E, $0F, $06
        byte    $00, $23, $10, $0F, $0C
        byte    $00, $40, $C4, $0F, $06
        byte    $0B, $00, $DC, $0F, $06
        byte    $00, $43, $70, $0F, $0C
        byte    $00, $60, $93, $0F, $06
        byte    $0C, $00, $DC, $0F, $06
        byte    $00, $23, $70, $0F, $0C
        byte    $00, $40, $93, $0F, $06
        byte    $0C, $03, $10, $0F, $0C
        byte    $00, $20, $C4, $0F, $06
        byte    $00, $40, $DC, $0F, $06
        byte    $0B, $20, $C4, $0F, $06
        byte    $00, $40, $DC, $0F, $06
        byte    $00, $63, $10, $0F, $0C
        byte    $0C, $02, $BA, $0F, $0C
        byte    $00, $20, $AF, $0F, $06
        byte    $00, $41, $26, $0F, $06
        byte    $0B, $22, $BA, $0F, $0C
        byte    $00, $41, $26, $0F, $06
        byte    $00, $60, $AF, $0F, $06
        byte    $0C, $00, $93, $0F, $06
        byte    $00, $20, $DC, $0F, $06
        byte    $00, $42, $4B, $0F, $0C
        byte    $0B, $00, $93, $0F, $06
        byte    $00, $22, $4B, $0F, $0C
        byte    $00, $60, $DC, $0F, $06
        byte    $0C, $03, $10, $0F, $0C
        byte    $00, $20, $49, $0F, $06
        byte    $00, $40, $C4, $0F, $06
        byte    $0B, $20, $49, $0F, $06
        byte    $00, $40, $C4, $0F, $06
        byte    $00, $63, $10, $0F, $0C
        byte    $0C, $02, $4B, $0F, $0C
        byte    $00, $20, $93, $0F, $06
        byte    $00, $40, $6E, $0F, $06
        byte    $0B, $20, $93, $0F, $06
        byte    $00, $40, $6E, $0F, $06
        byte    $00, $62, $4B, $0F, $0C
        byte    $0C, $00, $AF, $0F, $06
        byte    $00, $20, $93, $0F, $06
        byte    $00, $42, $BA, $0F, $0C
        byte    $0B, $02, $BA, $0F, $0C
        byte    $00, $20, $AF, $0F, $06
        byte    $00, $60, $93, $0F, $06
        byte    $0C, $00, $6E, $0F, $06
        byte    $00, $22, $4B, $0F, $0C
        byte    $00, $40, $93, $0F, $06
        byte    $0C, $00, $93, $0F, $06
        byte    $00, $22, $4B, $0F, $0C
        byte    $00, $40, $6E, $0F, $06
        byte    $0B, $00, $6E, $0F, $06
        byte    $00, $40, $93, $0F, $06
        byte    $00, $61, $B8, $0F, $0C
        byte    $0C, $00, $93, $0F, $06
        byte    $00, $21, $B8, $0F, $0C
        byte    $00, $40, $6E, $0F, $06
        byte    $0B, $00, $DC, $0F, $06
        byte    $00, $40, $93, $0F, $06
        byte    $00, $62, $4B, $0F, $0C
        byte    $0C, $02, $4B, $0F, $0C
        byte    $00, $20, $DC, $0F, $06
        byte    $00, $40, $93, $0F, $06
        byte    $0B, $20, $49, $0F, $06
        byte    $00, $41, $26, $0F, $06
        byte    $00, $61, $26, $0F, $0C
        byte    $0C, $01, $26, $0F, $06
        byte    $00, $21, $26, $0F, $0C
        byte    $00, $40, $49, $0F, $06
        byte    $0B, $01, $B8, $0F, $0C
        byte    $00, $40, $6E, $0F, $06
        byte    $00, $60, $DC, $0F, $06
        byte    $0C, $01, $B8, $0F, $0C
        byte    $00, $20, $6E, $0F, $06
        byte    $00, $40, $DC, $0F, $06
        byte    $0B, $20, $52, $0F, $06
        byte    $0C, $00, $52, $0F, $06
        byte    $0B, $00, $A5, $0F, $06
        byte    $00, $22, $93, $0F, $0C
        byte    $00, $40, $75, $0F, $06
        byte    $0C, $02, $93, $0F, $0C
        byte    $00, $20, $A5, $0F, $06
        byte    $00, $40, $75, $0F, $06
        byte    $0C, $03, $10, $0F, $0C
        byte    $00, $20, $A5, $0F, $06
        byte    $00, $40, $C4, $0F, $06
        byte    $0B, $23, $10, $0F, $0C
        byte    $00, $40, $A5, $0F, $06
        byte    $00, $60, $C4, $0F, $06
        byte    $0C, $03, $70, $0F, $0C
        byte    $00, $20, $DC, $0F, $06
        byte    $00, $40, $75, $0F, $06
        byte    $0B, $23, $70, $0F, $0C
        byte    $00, $40, $DC, $0F, $06
        byte    $00, $60, $75, $0F, $06
        byte    $0C, $00, $A5, $0F, $06
        byte    $00, $23, $A4, $0F, $0C
        byte    $00, $40, $E9, $0F, $06
        byte    $0B, $00, $A5, $0F, $06
        byte    $00, $40, $E9, $0F, $06
        byte    $00, $63, $A4, $0F, $0C
        byte    $0C, $00, $DC, $0F, $06
        byte    $00, $20, $E9, $0F, $06
        byte    $00, $43, $70, $0F, $0C
        byte    $0B, $00, $DC, $0F, $06
        byte    $00, $23, $70, $0F, $0C
        byte    $00, $60, $E9, $0F, $06
        byte    $0C, $01, $4A, $0F, $06
        byte    $00, $20, $C4, $0F, $06
        byte    $00, $43, $10, $0F, $0C
        byte    $0B, $00, $C4, $0F, $06
        byte    $00, $21, $4A, $0F, $06
        byte    $00, $63, $10, $0F, $0C
        byte    $0C, $00, $A5, $0F, $06
        byte    $00, $22, $93, $0F, $0C
        byte    $00, $40, $E9, $0F, $06
        byte    $0B, $02, $93, $0F, $0C
        byte    $00, $40, $E9, $0F, $06
        byte    $00, $60, $A5, $0F, $06
        byte    $0C, $00, $DC, $0F, $06
        byte    $00, $20, $52, $0F, $06
        byte    $00, $43, $70, $0F, $0C
        byte    $0C, $03, $70, $0F, $0C
        byte    $00, $20, $52, $0F, $06
        byte    $00, $40, $DC, $0F, $06
        byte    $0B, $20, $A5, $0F, $06
        byte    $00, $42, $93, $0F, $0C
        byte    $00, $60, $75, $0F, $06
        byte    $0C, $00, $75, $0F, $06
        byte    $00, $22, $93, $0F, $0C
        byte    $00, $40, $A5, $0F, $06
        byte    $0B, $00, $A5, $0F, $06
        byte    $00, $40, $C4, $0F, $06
        byte    $00, $63, $10, $0F, $0C
        byte    $0C, $00, $C4, $0F, $06
        byte    $00, $20, $A5, $0F, $06
        byte    $00, $43, $10, $0F, $0C
        byte    $0B, $00, $75, $0F, $06
        byte    $00, $20, $A5, $0F, $06
        byte    $00, $62, $93, $0F, $0C
        byte    $0C, $00, $A5, $0F, $06
        byte    $00, $22, $93, $0F, $0C
        byte    $00, $40, $75, $0F, $06
        byte    $0B, $01, $D2, $0F, $0C
        byte    $00, $40, $A5, $0F, $06
        byte    $00, $60, $75, $0F, $06
        byte    $0C, $00, $75, $0F, $06
        byte    $00, $20, $A5, $0F, $06
        byte    $00, $41, $D2, $0F, $0C
        byte    $0B, $00, $A5, $0F, $06
        byte    $00, $20, $E9, $0F, $06
        byte    $00, $62, $93, $0F, $0C
        byte    $0C, $00, $E9, $0F, $06
        byte    $00, $20, $A5, $0F, $06
        byte    $00, $42, $93, $0F, $0C
        byte    $0B, $01, $4A, $0F, $0C
        byte    $00, $20, $52, $0F, $06
        byte    $00, $61, $4A, $0F, $06
        byte    $0C, $00, $52, $0F, $06
        byte    $00, $21, $4A, $0F, $0C
        byte    $00, $41, $4A, $0F, $06
        byte    $0C, $00, $75, $0F, $06
        byte    $00, $20, $E9, $0F, $06
        byte    $00, $41, $D2, $0F, $0C
        byte    $0B, $00, $75, $0F, $06
        byte    $00, $20, $E9, $0F, $06
        byte    $00, $61, $D2, $0F, $0C
        byte    $0C, $00, $49, $0F, $06
        byte    $0B, $00, $49, $0F, $06
        byte    $0C, $01, $9F, $0F, $0C
        byte    $00, $20, $68, $0F, $06
        byte    $00, $40, $6E, $0F, $06
        byte    $0B, $20, $6E, $0F, $06
        byte    $00, $40, $6E, $0F, $06
        byte    $00, $61, $B8, $0F, $0C
        byte    $0C, $00, $93, $0F, $06
        byte    $00, $20, $AF, $0F, $06
        byte    $00, $42, $BA, $0F, $0C
        byte    $0B, $00, $93, $0F, $06
        byte    $00, $22, $93, $0F, $0C
        byte    $00, $60, $A5, $0F, $06
        byte    $0C, $00, $8B, $0F, $06
        byte    $00, $20, $6E, $0F, $06
        byte    $00, $42, $2A, $0F, $0C
        byte    $0B, $00, $93, $0F, $06
        byte    $00, $22, $4B, $0F, $0C
        byte    $00, $60, $6E, $0F, $06
        byte    $0C, $02, $93, $0F, $0C
        byte    $00, $20, $A5, $0F, $06
        byte    $00, $40, $93, $0F, $06
        byte    $0B, $20, $AF, $0F, $06
        byte    $00, $40, $93, $0F, $06
        byte    $00, $62, $BA, $0F, $0C
        byte    $0C, $00, $C4, $0F, $06
        byte    $00, $23, $10, $0F, $0C
        byte    $00, $40, $DC, $0F, $06
        byte    $0C, $00, $DC, $0F, $06
        byte    $00, $23, $3F, $0F, $0C
        byte    $00, $40, $D0, $0F, $06
        byte    $0B, $01, $26, $0F, $06
        byte    $00, $43, $70, $0F, $0C
        byte    $00, $60, $DC, $0F, $06
        byte    $0C, $00, $AF, $0F, $06
        byte    $00, $22, $BA, $0F, $0C
        byte    $00, $41, $26, $0F, $06
        byte    $0B, $02, $4B, $0F, $0C
        byte    $00, $40, $DC, $0F, $06
        byte    $00, $60, $93, $0F, $06
        byte    $0C, $02, $4B, $0F, $0C
        byte    $00, $20, $93, $0F, $06
        byte    $00, $40, $DC, $0F, $06
        byte    $0B, $20, $62, $0F, $06
        byte    $0C, $00, $62, $0F, $06
        byte    $0B, $00, $68, $0F, $06
        byte    $00, $20, $93, $0F, $06
        byte    $00, $41, $9F, $0F, $0C
        byte    $0C, $00, $6E, $0F, $06
        byte    $00, $21, $B8, $0F, $0C
        byte    $00, $40, $93, $0F, $06
        byte    $0B, $00, $C4, $0F, $06
        byte    $00, $42, $BA, $0F, $0C
        byte    $00, $60, $AF, $0F, $06
        byte    $0C, $00, $A5, $0F, $06
        byte    $00, $22, $93, $0F, $0C
        byte    $00, $40, $C4, $0F, $06
        byte    $0B, $00, $8B, $0F, $06
        byte    $00, $40, $93, $0F, $06
        byte    $00, $62, $2A, $0F, $0C
        byte    $0C, $00, $93, $0F, $06
        byte    $00, $20, $93, $0F, $06
        byte    $00, $42, $4B, $0F, $0C
        byte    $0C, $00, $62, $0F, $06
        byte    $00, $20, $A5, $0F, $06
        byte    $00, $42, $93, $0F, $0C
        byte    $0B, $00, $AF, $0F, $06
        byte    $00, $22, $BA, $0F, $0C
        byte    $00, $60, $62, $0F, $06
        byte    $0C, $00, $C4, $0F, $06
        byte    $00, $23, $10, $0F, $0C
        byte    $00, $40, $93, $0F, $06
        byte    $0B, $03, $3F, $0F, $0C
        byte    $00, $40, $D0, $0F, $06
        byte    $00, $60, $93, $0F, $06
        byte    $0C, $03, $70, $0F, $0C
        byte    $00, $20, $DC, $0F, $06
        byte    $00, $40, $C4, $0F, $06
        byte    $0B, $22, $BA, $0F, $0C
        byte    $00, $40, $AF, $0F, $06
        byte    $00, $60, $C4, $0F, $06
        byte    $0C, $02, $4B, $0F, $0C
        byte    $00, $20, $93, $0F, $06
        byte    $00, $40, $93, $0F, $06
        byte    $0B, $20, $93, $0F, $06
        byte    $00, $40, $93, $0F, $06
        byte    $00, $62, $4B, $0F, $0C
        byte    $0C, $03, $70, $0F, $0C
        byte    $0B, $23, $3F, $0F, $0C
        byte    $0C, $03, $10, $0F, $0C
        byte    $0B, $22, $E4, $0F, $0C
        byte    $0C, $02, $BA, $0F, $0C
        byte    $00, $21, $B8, $0F, $0C
        byte    $0C, $02, $93, $0F, $0C
        byte    $00, $21, $9F, $0F, $0C
        byte    $0B, $42, $6E, $0F, $0C
        byte    $00, $61, $88, $0F, $0C
        byte    $0C, $02, $4B, $0F, $0C
        byte    $00, $21, $72, $0F, $0C
        byte    $0B, $42, $2A, $0F, $0C
        byte    $00, $61, $5D, $0F, $0C
        byte    $0C, $02, $0B, $0F, $0C
        byte    $00, $21, $4A, $0F, $0C
        byte    $0B, $41, $37, $0F, $0C
        byte    $00, $61, $EE, $0F, $0C
        byte    $0C, $01, $26, $0F, $0C
        byte    $00, $21, $D2, $0F, $0C
        byte    $0B, $41, $EE, $0F, $0C
        byte    $00, $61, $B8, $0F, $0C
        byte    $0C, $01, $9F, $0F, $0C
        byte    $00, $22, $0B, $0F, $0C
        byte    $0B, $41, $88, $0F, $0C
        byte    $00, $62, $2A, $0F, $0C
        byte    $0C, $02, $4B, $0F, $0C
        byte    $00, $21, $72, $0F, $0C
        byte    $0B, $42, $6E, $0F, $1F
        byte    $00, $61, $5D, $0F, $1F
        byte    $1F, $02, $6E, $0D, $1F
        byte    $00, $21, $5D, $0D, $1F
        byte    $1F, $01, $5D, $0A, $1F
        byte    $00, $22, $6E, $0A, $1F
        byte    $1F, $02, $6E, $08, $1F
        byte    $00, $21, $5D, $08, $1F
        byte    $1F, $02, $6E, $05, $1F
        byte    $00, $21, $5D, $05, $1F
        byte    $1E, $42, $6E, $03, $1F
        byte    $00, $61, $5D, $03, $1F
        byte    $ff
'+--------------------------------------+
'|         __   ___   __  __  __        |
'|        (  ) / __) / _)(  )(  )       |
'|        /__\ \__ \( (_  )(  )(        |
'|       (_)(_)(___/ \__)(__)(__)       |
'|         _    _  __  _  _  __         |
'|        ( \/\/ )(  )( \( )/ _)        |
'|         \    /  )(  )  (( (/\        |
'|          \/\/  (__)(_)\_)\__/        |
'|                                      |
'|             CassLan 2008             |
'|                                      |
'+--------------------------------------+