'''  WITH APOLOGIES TO LODERUNNER
'''  By Jeff Ledger / AKA Oldbitcollector
'''
'''  ##     ###### ###### ####   ####   ##  ## ##  ## ##  ## ###### ####     V.0011
'''  ##     ##  ## ##  ## ##  ## ##  ## ##  ## ### ## ### ## ##     ##  ##
'''  ##     ##  ## ###### ##  ## ## ### ##  ## ## ### ## ### ####   ## ###   The I hate
'''  ##     ##  ## ##  ## ##  ## ## ##  ##  ## ## ### ## ### ##     ## ##    timers edition.
'''  ###### ###### ##  ## ####   ##   # ###### ##  ## ##  ## ###### ##   #
'''
''' If you are reading this message, be warned that this game is *unfinished*
''  and glitches are abundant! :)  -Oldbitcollector

'' GAME PREP BEFORE PLAYING: Copy any "level#.txt" files to SD before loading!!!

'' GAME CONTROLS / Requires NES Controller on P3,P4,P5 (Hydra Compatible Setup)
'' A & B Buttons Drill right and left.
'' SELECT Bypass to the next level.
'' START Restart if trapped or dead.

'' ** LEVEL CREATION INFORMATION **
'' Currently supports up to 5 levels in "level#.txt" format. (5 included)
'' All files must be the exact same size as level1. (40x20)
'' All files must have the line of $$$$$ as bottom line.
'' ! = Ladders
'' * = Gems (must be twelve)
'' - = Ropes
'' ^ = escape ladder (must be one)
'' % = solid brick (can't be drilled)
'' # = drillable brick


  '' I/O SETTINGS

   _clkmode  = xtal1 + pll16x
   _xinfreq  = 5_000_000
  
  '' Adjust for video and sound.
  
   video        = 12    '' Hydra = 24
   sound        = 10    '' Hydra = 7
   
  '' Adjust for location of the SD card.
  
   spiDO     = 21
   spiClk    = 24
   spiDI     = 20
   spiCS     = 25

  '' Joypad serial shifter settings.

   JOY_CLK = 16
   JOY_LCH = 17
   JOY_DATAOUT0 = 19
   JOY_DATAOUT1 = 18 

DAT
'' ********************* NO CHANGES REQUIRED BEYOND THIS POINT ************************

CON
  BLACK             = $02 
  GREY              = $03
  LWHITE            = $06
  BWHITE            = $07
  LMAGENTA          = $A3 
  WHITE             = $AF
  RED               = $A2
  LRED              = $1A
  LPURPLE           = $1B
  PURPLE            = $03 
  LGREEN            = $1C
  SKYBLUE           = $1D
  YELLOW            = $1E
  BYELLOW           = $A6
  LGREY             = $1F
  DGREY             = $08
  GREEN             = $A4
  ORANGE            = $06
  BLUE              = $A1 
  CYAN              = $1C
  MAGENTA           = $FC
  LYELLOW           = $1E




  ' NES bit encodings for NES gamepad 0
  NES0_RIGHT    = %00000000_00000001
  NES0_LEFT     = %00000000_00000010
  NES0_DOWN     = %00000000_00000100
  NES0_UP       = %00000000_00001000
  NES0_START    = %00000000_00010000
  NES0_SELECT   = %00000000_00100000
  NES0_B        = %00000000_01000000
  NES0_A        = %00000000_10000000
     
OBJ
     snd   : "NS_sound_drv_052_22khz_16bit_stereo"          ' Nick Sabalausky's Sound Driver
     text  : "aigeneric_driver_ACS"          ' AIGeneric Video Driver
     fat   : "SD-MMC_FATEngine_Slim" ' Modified SD routines
      
VAR
     byte command[32]
     byte f0[32]
     byte tbuf[20] ' SD Vars 
     long tp
     byte strbuff[5]

     word  Nes_Pad

     byte bricky[100]
     byte brickx[100]
     byte bricktimer[100]
     byte timer[100]
          
PUB mainProgram  | size, r,sta,bytes,counter,font,hero_x,hero_y,x,pad,board,oldboard,checkfloor,face,checkdrill,standing,right,left,below,above,gems,finishsound,count,openbricks,men,score_total,current_level,junk,total_levels,baddie1_movecounter,baddie1_standing,baddie1_below,baddie1_above,baddie1_right,baddie1_left,baddie1_y,baddie1_x,baddie1_oldboard,baddie1_board,baddie1_gem,baddie1_freeze, {
} character_chr,  character_step, game_delay_counter, bcharacter_chr, bcharacter_step

  total_levels:=9 '' Change to reflect how many levels are on the SD

  ''DIRA[25]~~
  text.start(video)
  snd.start(sound)
   
  text.str(string("Attempting to mount SD card... ")) 
  text.UpdateScreen  
  fat.FATEngineStart(spiDO,spiClk,spiDI,spiCS,-1,-1,-1,-1,-1)           
  fat.mountPartition(0)   
  text.str(string("OK!",13))
  text.UpdateScreen
  
  text.str(string("Changing directory... ")) 
  text.UpdateScreen
  fat.changeDirectory(string("ldrunner"))
  text.str(string("OK!",13))
  text.UpdateScreen
     
  graphics
  titlescreen
  text.UpdateScreen
    repeat until Nes_Pad & NES0_UP
      Nes_Pad := NES_Read_Gamepad
  text.color(white)
  current_level:=1
  text.out(0)
  drawlevel(current_level)
  text.UpdateScreen
  men:=10 '' Increase/Decrease Player Count Here
  score_total:=0
  score(score_total,men,current_level)
  hero_x:=30    '' Hero Location Right & Left
  hero_y:=17    '' Hero Location Up & Down
  text.pokechar(hero_y,hero_x,16,43)

  baddie1_gem:=0
  baddie1_x:=20
  baddie1_y:=0
  text.pokechar(baddie1_y,baddie1_x,16,43)  '' Position Baddie1

  face:=0    'facing 0=left, 1=right
  pad:=1
  gems:=12 '' All boards must have 12 gems to open the exit
  finishsound:=0
  timer:=0
  openbricks:=0
  baddie1_movecounter:=0
  oldboard:=1568
  baddie1_oldboard:=1568
  baddie1_freeze:=0

 '' IN GAME

repeat 
  text.UpdateScreen
  Nes_Pad := NES_Read_Gamepad

  '' Keep track of all tiles around hero and baddies before moving them

  standing:=text.getchar(hero_y,hero_x)  '' Check tile under hero
  below:=text.getchar(hero_y+1,hero_x)   '' Check tile below hero
  right:=text.getchar(hero_y,hero_x+1)   '' Check tile right of hero
  left:=text.getchar(hero_y,hero_x-1)    '' Check tile left of hero
  above:=text.getchar(hero_y-1,hero_x)   '' Check tile above hero

  baddie1_standing:=text.getchar(baddie1_y,baddie1_x) '' Check tile under baddie1
  baddie1_below:=text.getchar(baddie1_y+1,baddie1_x)  '' Check tile below baddie1
  baddie1_right:=text.getchar(baddie1_y,baddie1_x+1)  '' Check tile right of baddie1
  baddie1_left:=text.getchar(baddie1_y,baddie1_x-1)   '' Check tile left of baddie1
  baddie1_above:=text.getchar(baddie1_y-1,baddie1_x)  '' Check tile above baddie1


 if (below== 23587) or (standing==1568+13) or (below==1569) or (below==23589)'' unfreeze pad if done falling
   pad:=1
   
 'use start for debugging
 if (Nes_Pad & NES0_SELECT) '' Bypass this level to the next
    {text.dec(baddie1_standing)
    text.out(32)}

      text.out(0)
      graphics
      current_level++
      drawlevel(current_level)
      score(score_total,men,current_level)
      text.UpdateScreen
      hero_x:=30    '' Hero Location Right & Left
      hero_y:=17    '' Hero Location Up & Down
      text.pokechar(hero_y,hero_x,16,43)
      face:=0    'facing 0=left, 1=right
      pad:=1
      gems:=12 '' All boards must have 12 gems to open the exit
      finishsound:=0
      timer:=0
      openbricks:=0
      baddie1_x:=20
      baddie1_y:=0


 if (Nes_Pad & NES0_START) ''Start over unless men are used up.
     if men < 1
       gameoverscreen
       titlescreen
       repeat until (Nes_Pad & NES0_UP)
             Nes_Pad := NES_Read_Gamepad
       text.color(white)
       current_level:=1
       men:=11
       score_total:=0

     text.out(0)
     graphics
     drawlevel(current_level)
     text.UpdateScreen
     men--
     score(score_total,men,current_level)
     hero_x:=30    '' Hero Location Right & Left
     hero_y:=17    '' Hero Location Up & Down
     text.pokechar(hero_y,hero_x,16,43)
     face:=0    'facing 0=left, 1=right
     pad:=1
     gems:=12 '' All boards must have 12 gems to open the exit
     finishsound:=0
     timer:=0
     openbricks:=0
     baddie1_x:=20
     baddie1_y:=0
     baddie1_gem:=0
     baddie1_freeze:=0
     text.pokechar(baddie1_y,baddie1_x,16,43)  '' Position Baddie1
     board:=0
     oldboard:=0

     repeat until junk == 50
        bricky[junk]:=0
        brickx[junk]:=0
        junk++

 '' Brick Timer & Closer

 count:=0
 repeat until count == 50
   timer[count]++
   count++

 count:=0
  repeat until count == 50
    if timer[openbricks] > 250 ''Determines how long bricks will stay open
         text.putchar(bricky[0],brickx[0],23587)
         openbricks--
         junk:=0
         repeat until junk == 50
           bricky[junk]:=bricky[junk+1]
           brickx[junk]:=brickx[junk+1]
           junk++

    count++

 if game_delay_counter == 20  ''  Increase to slow gameplay down
      game_delay_counter:=0
      character_step++
      if (character_step > 1)
         character_step := 0

    'Did we drill a hole to the left?
    if (Nes_Pad & NES0_A and standing <> 15691) '' can't drill under a ladder 15691
       checkdrill:=text.getchar(hero_y+1,hero_x+1)
         if checkdrill==23587
            text.putchar(hero_y+1,hero_x+1,1570)''was 1568 (blank)
            snd.playsoundFM(1,snd#SHAPE_SQUARE,500,1000,255,snd#NO_ENVELOPE)
            openbricks++
            bricky[openbricks]:=hero_y+1
            brickx[openbricks]:=hero_x+1 ''Found that brick bug! was hero_x-1
            timer[openbricks]:=0
         
    'Did we drill a hole to the right?
    if (Nes_Pad & NES0_B and standing <> 15691) ''can't drill under a ladder 15691
       checkdrill:=text.getchar(hero_y+1,hero_x-1)
         if checkdrill==23587
            text.putchar(hero_y+1,hero_x-1,1570) ''was 1568 (blank)
            snd.playsoundFM(1,snd#SHAPE_SQUARE,500,1000,255,snd#NO_ENVELOPE)
            openbricks++
            bricky[openbricks]:=hero_y+1
            brickx[openbricks]:=hero_x-1
            timer[openbricks]:=0
         
     'Did we go right?
    if (Nes_Pad & NES0_RIGHT)
         if (hero_x < 39 and pad==1 and right<>23587)
            board:=text.getchar(hero_y,hero_x+1)
            text.putchar(hero_y,hero_x,oldboard)
            oldboard:=board
            if oldboard == 36394 ''grab a gem
              oldboard:=1568 ''replace with a space
              snd.playsoundFM(1,snd#SHAPE_SQUARE,1500,1000,255,snd#NO_ENVELOPE)
              gems-- ''deduct gem from total
            hero_x := hero_x + 1

            if (character_step == 0)
              character_chr := 40
            else
              character_chr := 46
            MoveCharacter(hero_x,hero_y,character_chr)


         face:=1 'note facing right

           
      'Did we go left
    if (Nes_Pad & NES0_LEFT)
         if (hero_x > 0 and pad==1 and left<>23587)
            board:=text.getchar(hero_y,hero_x-1)
            text.putchar(hero_y,hero_x,oldboard)
            oldboard:=board
            if oldboard == 36394 ''grab a gem
               oldboard:=1568 ''replace with a space
               snd.playsoundFM(1,snd#SHAPE_SQUARE,1500,1000,255,snd#NO_ENVELOPE)
               gems-- ''deduct gem from total
            hero_x := hero_x - 1

            if (character_step == 0)
              character_chr := 41
            else
              character_chr := 44
            MoveCharacter(hero_x,hero_y,character_chr)

            face:=0 'note facing left 'default drilling
                 
      'Did we go up
    if (Nes_Pad & NES0_UP and above<>23587)
         if (hero_y > 1 and oldboard==1569 or oldboard==1569 and pad==1)
            board:=text.getchar(hero_y-1,hero_x)
            text.putchar(hero_y,hero_x,oldboard)
            oldboard:=board
            hero_y := hero_y - 1
            text.pokechar(hero_y,hero_x,16,43)
         
      'Did we go down
    if (Nes_Pad & NES0_DOWN) '' standard check
         if (hero_y < 18 and checkfloor == 1569)
            board:=text.getchar(hero_y+1,hero_x)
            text.putchar(hero_y,hero_x,oldboard)
            oldboard:=board
            hero_y := hero_y + 1
            text.pokechar(hero_y,hero_x,16,43)
 
    if (Nes_Pad & NES0_DOWN) ''FALL FROM A ROPE
         if (hero_y < 18 and checkfloor==1568 and oldboard<>13)
           board:=text.getchar(hero_y+1,hero_x)
           text.putchar(hero_y,hero_x,oldboard)
           oldboard:=board
           hero_y := hero_y + 1
           text.pokechar(hero_y,hero_x,16,43)

    checkfloor:=text.getchar(hero_y+1,hero_x)
 
    if checkfloor == 1568+13 '' Char 43 looks like a rope
           board:=text.getchar(hero_y+1,hero_x)
           MoveHeroDown(hero_x,hero_y)
           text.putchar(hero_y,hero_x,oldboard)
           oldboard:=board
           hero_y := hero_y + 1
           pad:=1
        
    if checkfloor == 36394 '' Fell on a gem
           pad:=1
           MoveHeroDown(hero_x,hero_y)
           text.putchar(hero_y+1,hero_x-1,1568)
           snd.playsoundFM(1,snd#SHAPE_SQUARE,500,1000,255,snd#NO_ENVELOPE)
           hero_y:=hero_y+1
           gems-- ''deduct gem from total
         
    if checkfloor == 1568 and oldboard<>1568+13 and below<>1569 OR checkfloor == 1570 and oldboard<>1568+13 and below<>1569  ''Freefall through the air
           board:=text.getchar(hero_y+1,hero_x)
           MoveHeroDown(hero_x,hero_y)
           text.putchar(hero_y,hero_x,oldboard)
           oldboard:=board
           hero_y := hero_y + 1
           snd.playsoundFM(1,snd#SHAPE_TRIANGLE,4000+hero_y*20,4000,255,snd#NO_ENVELOPE)
           pad:=0

    '' Check gem count for an exit to appear
    if gems == 0 and finishsound==0
         text.redefine(30,130,130,254,130,130,130,254,130)  '' Finish ladder ^
         snd.playsoundFM(1,snd#SHAPE_SINE,3000,2000,255,snd#NO_ENVELOPE)
         finishsound:=1

    if gems==0 and above== 1566 '' Winner!
         finishsound:=2000
         repeat until finishsound==0
            snd.playsoundFM(1,snd#SHAPE_TRIANGLE,finishsound,2000,255,snd#NO_ENVELOPE)
            finishsound--

         if current_level == total_levels
            gameoverscreen
            titlescreen
            repeat until (Nes_Pad & NES0_UP)
                  Nes_Pad := NES_Read_Gamepad
            text.color(white)
            current_level:=0
            men:=9
            score_total:=0

         text.out(0)
         graphics
         current_level++
         drawlevel(current_level)
         text.UpdateScreen
         men++
         score(score_total,men,current_level)
         hero_x:=30    '' Hero Location Right & Left
         hero_y:=17    '' Hero Location Up & Down
         text.pokechar(hero_y,hero_x,16,43)
         face:=0    'facing 0=left, 1=right
         pad:=1
         gems:=12 '' All boards must have 12 gems to open the exit
         finishsound:=0
         timer:=0
         openbricks:=0
         baddie1_x:=20
         baddie1_y:=0
         baddie1_gem:=0
         text.pokechar(baddie1_y,baddie1_x,16,43)  '' Position Baddie1
         board:=0
         oldboard:=1568
         baddie1_oldboard:=1568
         baddie1_freeze:=0

 ''MOVE BADDIES
 if baddie1_movecounter == 40  ''  Increase to slow baddie down
      baddie1_movecounter:=0
      bcharacter_step++
      if (bcharacter_step > 1)
         bcharacter_step := 0


     '' TRAP BADDIE1 INTO A HOLE
     if (baddie1_below == 1570)
         baddie1_freeze:=1
         baddie1_board:=text.getchar(baddie1_y+1,baddie1_x)
         'MoveBaddieDown(baddie1_x,baddie1_y)
         text.putchar(baddie1_y,baddie1_x,baddie1_oldboard)
         baddie1_oldboard:=baddie1_board
         baddie1_y := baddie1_y + 1
         text.pokechar(baddie1_y,baddie1_x,16,43)
         if baddie1_gem == 1
             text.pokechar(baddie1_y-1,baddie1_x,$1E,36394) '' Drop Gem if owned
             baddie1_gem:= 0


     if (baddie1_standing == 1570)
         baddie1_freeze:=1

     ''MAKE BADDIE1 FALL IF NOTHING OR ROPE IS BELOW HIM
     if (baddie1_y < 18 and baddie1_below == 1568 OR baddie1_y < 18 and baddie1_below == 1581 and baddie1_standing <> 1576) ''Check for air below baddie1
         if baddie1_freeze==0
           baddie1_board:=text.getchar(baddie1_y+1,baddie1_x)
           'MoveBaddieDown(baddie1_x,baddie1_y)
           text.putchar(baddie1_y,baddie1_x,baddie1_oldboard)
           baddie1_oldboard:=baddie1_board
           baddie1_y := baddie1_y + 1
           text.pokechar(baddie1_y,baddie1_x,16,43)

     '' MAKE BADDIE1 GO DOWN IF A LADDER AND HERO ARE BELOW HIM
     if (baddie1_y < 18 and hero_y > baddie1_y and baddie1_below == 1569) ''Check for ladder bellow baddie1
         if baddie1_freeze==0
           baddie1_board:=text.getchar(baddie1_y+1,baddie1_x)
           'MoveBaddieDown(baddie1_x,baddie1_y)
           text.putchar(baddie1_y,baddie1_x,baddie1_oldboard)
           baddie1_oldboard:=baddie1_board
           baddie1_y := baddie1_y + 1
           text.pokechar(baddie1_y,baddie1_x,16,43)

     '' MAKE BADDIE1 GO UP IF A LADDER AND HERO ARE ABOVE HIM
     if (baddie1_y > 1 and hero_y < baddie1_y and baddie1_standing == 1569 or baddie1_y > 1 and hero_y < baddie1_y and baddie1_above  == 1569)
         baddie1_board:=text.getchar(baddie1_y-1,baddie1_x)
         'MoveBaddieUp(baddie1_x,baddie1_y)
         text.putchar(baddie1_y,baddie1_x,baddie1_oldboard)
         baddie1_oldboard:=baddie1_board
         baddie1_y := baddie1_y - 1
         text.pokechar(baddie1_y,baddie1_x,16,43)

     '' BADDIE1 FINDS A GEM TO THE LEFT AND STEALS IT IF HE DOESN'T HAVE 1
     if (baddie1_left == 36394 and baddie1_gem == 0)
         baddie1_gem:=1
         baddie1_board:=1568
         'MoveBaddieLeft(baddie1_x,baddie1_y)
         text.putchar(baddie1_y,baddie1_x,1568)
         baddie1_oldboard:=baddie1_board
         baddie1_x := baddie1_x - 1
         if (bcharacter_step == 0)
           bcharacter_chr := 41
         else
           bcharacter_chr := 44
         MoveCharacter(baddie1_x,baddie1_y,bcharacter_chr)




     '' MAKE BADDIE1 GO LEFT IF HERO IS LEFT OF HIM AND BOARD IS OPEN OR LADDER
     if (baddie1_x > 0 and hero_x < baddie1_x and baddie1_left <> 23587 and baddie1_below <> 1568 and baddie1_standing <> 1570) OR (baddie1_x > 0 and hero_x < baddie1_x and baddie1_left <> 23587 and baddie1_below <> 1568 and baddie1_standing <> 1570)
         if baddie1_freeze==0
           baddie1_board:=text.getchar(baddie1_y,baddie1_x-1)
           'MoveBaddieLeft(baddie1_x,baddie1_y)
           text.putchar(baddie1_y,baddie1_x,baddie1_oldboard)
           baddie1_oldboard:=baddie1_board
           baddie1_x := baddie1_x - 1
           if (bcharacter_step == 0)
             bcharacter_chr := 41
           else
             bcharacter_chr := 44
           MoveCharacter(baddie1_x,baddie1_y,bcharacter_chr)



     '' BADDIE1 FINDS A GEM TO THE RIGHT AND STEALS IT IF HE DOESN'T HAVE 1
     if (baddie1_right == 36394 and baddie1_gem == 0)
        baddie1_gem:=1
        baddie1_board:=1568
        'MoveBaddieRight(baddie1_x,baddie1_y)
        text.putchar(baddie1_y,baddie1_x,1568)
        baddie1_oldboard:=baddie1_board
        baddie1_x := baddie1_x + 1
        if (bcharacter_step == 0)
           bcharacter_chr := 40
        else
           bcharacter_chr := 46
        MoveCharacter(baddie1_x,baddie1_y,bcharacter_chr)



     '' MAKE BADDIE1 GO RIGHT IF HERO IS LEFT OF HIM AND BOARD IS OPEN OR LADDER '''1568
     if (baddie1_x < 39 and hero_x > baddie1_x and baddie1_right <> 23587 and baddie1_below <> 1568 and baddie1_standing <> 1570) OR (baddie1_x < 39 and hero_x > baddie1_x and baddie1_right <> 23587 and baddie1_below <> 1568 and baddie1_standing <> 1570)
         if baddie1_freeze==0
           baddie1_board:=text.getchar(baddie1_y,baddie1_x+1)
           text.putchar(baddie1_y,baddie1_x,baddie1_oldboard)
           baddie1_oldboard:=baddie1_board
           baddie1_x := baddie1_x + 1
           if (bcharacter_step == 0)
              bcharacter_chr := 40
           else
              bcharacter_chr := 46
           MoveCharacter(baddie1_x,baddie1_y,bcharacter_chr)




    '' ALLOW BADDIE TO CLIMB OUT OF THE HOLE.. (MAYBE)
   { if freeze==1 and baddie1_counter==10
       freeze:=0
       baddie1_counter:=0    }


     '' BADDIE1 KILLED BY BRICK / DROP GEM IF CARRIED.
     if (baddie1_standing == 23587)
      {  if baddie1_gem == 1
             text.pokechar(baddie1_y-1,baddie1_x,$1E,36394) '' Drop Gem if owned
             baddie1_gem:= 0   }

        baddie1_oldboard:=baddie1_board
        text.putchar(baddie1_y,baddie1_x,23587)
        baddie1_x:=20
        baddie1_y:=0
        text.pokechar(baddie1_y,baddie1_x,16,43)  '' Position Baddie1
        baddie1_oldboard:=1568
        baddie1_freeze:=0

     ''KILL HERO IF BADDIE RUNS INTO HIM
     if (baddie1_y == hero_y and baddie1_x == hero_x)
        text.out(0)
        graphics
        drawlevel(current_level)
        men--
        score(score_total,men,current_level)
        hero_x:=30    '' Hero Location Right & Left
        hero_y:=17    '' Hero Location Up & Down
        text.pokechar(hero_y,hero_x,16,43)
        face:=0    'facing 0=left, 1=right
        pad:=1
        gems:=12 '' All boards must have 12 gems to open the exit
        finishsound:=0
        timer:=0
        openbricks:=0
        baddie1_x:=20
        baddie1_y:=0
        baddie1_gem:=0
        text.pokechar(baddie1_y,baddie1_x,16,43)  '' Position Baddie1
        board:=0
        oldboard:=1568
        baddie1_oldboard:=1568
        baddie1_freeze:=0

        repeat until junk == 50
          bricky[junk]:=0
          brickx[junk]:=0
          junk++


 baddie1_movecounter++
 game_delay_counter++


PUB MoveCharacter (hero_x,hero_y,character_chr) | counter

    text.pokechar(hero_y,hero_x,white,character_chr)



PUB MoveHeroDown(hero_x,hero_y) | counter

    text.pokechar(hero_y,hero_x,16,43)
    counter:=0
    repeat until counter == 1500
      counter++
    text.pokechar(hero_y,hero_x,16,32)
    hero_y++ 
    text.pokechar(hero_y,hero_x,16,43)
    counter:=0
    repeat until counter == 1500
      counter++

PUB score(scored,mend,leveld)
 '' Placeholder for actual scoreboard
 text.color(white)
 text.out(13)
 text.str(string("       :LOADRUNNER: BY JEFF LEDGER",13))
 text.str(string("             MEN:"))
 text.dec(mend)
 text.str(string(" / LEVEL:"))
 text.dec(leveld)
 text.out(13)
 text.color(white)

PUB gameoverscreen | junk3
       text.out(0)
       repeat until junk3 == 200
           text.UpdateScreen
           text.color(junk3)
           text.str(string("            G A M E  O V E R",13))
           junk3++
           waitcnt(3_000_000 + cnt) ''Wait a moment

PUB graphics

  text.redefine(35,253,253,253,0,191,191,191,0)      '' bricks
  text.redefine(34,0,0,0,0,0,0,0,129)                '' dug bricks
  text.redefine(42,0,0,62,0,62,0,62,0)               '' gems
  text.redefine(33,130,130,254,130,130,130,254,130)  '' ladders
  text.redefine(45,0,255,0,0,0,0,0,0)                '' ropes
  text.redefine(43,60,189,153,126,60,54,110,110)     '' player falling/climbing
  text.redefine(40,120,120,31,52,110,110,231,3)      '' player running left 1
  text.redefine(46,120,120,31,112,108,111,96,96)     '' player running left 2
  text.redefine(41,30,30,248,28,118,118,231,224)     '' player running right 1
  text.redefine(44,31,31,64,28,102,246,10,10)        '' player running right 2
  text.redefine(36,255,255,255,255,0,0,0,0)          '' foundation $$$$
  text.redefine(37,255,255,255,255,255,255,255,0)    '' solid bricks %%%% can't drill
  text.redefine(30,0,0,0,0,0,0,0,0)                  '' Hide Finish ladder


PUB titlescreen
  text.out(0)
  text.color(red)
  text.str(string("########################################",13))
  text.str(string("##                                    ##",13,13,13,13))
  text.color(white)
  text.str(string("     L  O  A  D  R  U  N  N  E  R",13,13,13))
  text.str(string("            By: JEFF LEDGER",13,13,13,13,13))
  text.color(lgreen)
  text.str(string("    controls:",13,13))
  text.str(string("    [UP]      start game",13))
  text.str(string("    [START]   restart level",13))
  text.str(string("    [SELECT]  bypass current level",13))
  text.str(string("    [A] & [B] dig right & left",13,13,13))
  text.color(red)
  text.str(string("##                    "))
  text.color(white)
  text.str(string("."))
  text.color(yellow)
  text.str(string("  *"))
  text.color(red)
  text.str(string("            ##",13))
  text.str(string("########################################",13))
  text.color(white)
  text.UpdateScreen

PUB drawlevel(load_level) | counter, r, lname, font
 '' Draw the first level

  lname := string("levelN.txt")
  byte[lname][5] := "0"+ load_level//10

  r:=\fat.openFile(lname,"r")
  if fat.partitionError
    text.cls
    text.str(string("Error opening "))
    text.str(lname)
    text.str(string(" : "))
    text.str(r)
    text.updateScreen
    repeat

  repeat
     r~
     ifnot fat.readData(@r,1) ' end of data?
     'text.out(13)
      quit
     if r == 10 or r == 13  ''Dump the linefeed character(s)
       r:=0
     if r == "#" or r=="%"
       text.color(red)    ''Paint bricks # and %
     if r == "*"
       text.color(yellow) '' Paint gems *
     if r == "$"
       text.color(blue)   '' Paint floor $$$$$
           
     if r > 0
        text.out(r)
        text.color(white)  '' Paint everything else white
           'text.dec(r)
           'text.out(32)
 '' Draw the scoreboard
 

PUB NES_Read_Gamepad : nes_bits
' //////////////////////////////////////////////////////////////////
' NES Game Paddle Read
' //////////////////////////////////////////////////////////////////
' reads both gamepads in parallel encodes 8-bits for each in format
' right game pad #1 [15..8] : left game pad #0 [7..0]
'
' set I/O ports to proper direction
' P3 = JOY_CLK      (4)
' P4 = JOY_SH/LDn   (5)
' P5 = JOY_DATAOUT0 (6)
' P6 = JOY_DATAOUT1 (7)
' NES Bit Encoding
'
' RIGHT  = %00000001
' LEFT   = %00000010
' DOWN   = %00000100
' UP     = %00001000
' START  = %00010000
' SELECT = %00100000
' B      = %01000000
' A      = %10000000

' step 1: set I/Os
DIRA [JOY_CLK] := 1 ' output
DIRA [JOY_LCH] := 1 ' output
DIRA [JOY_DATAOUT0] := 0 ' input
DIRA [JOY_DATAOUT1] := 0 ' input

' step 2: set clock and latch to 0
OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0
'Delay(1)

' step 3: set latch to 1
OUTA [JOY_LCH] := 1 ' JOY_SH/LDn = 1
'Delay(1)

' step 4: set latch to 0
OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0

' data is now ready to shift out, clear storage
'nes_bits := 0

' step 5: read 8 bits, 1st bits are already latched and ready, simply save and clock remaining bits
repeat 8

 nes_bits <<= 1
 nes_bits |= INA[JOY_DATAOUT0] | (INA[JOY_DATAOUT1] << 8)

 OUTA [JOY_CLK] := 1 ' JOY_CLK = 1
 'Delay(1)
 OUTA [JOY_CLK] := 0 ' JOY_CLK = 0

 'Delay(1)
' invert bits to make positive logic
nes_bits := (!nes_bits & $FFFF)