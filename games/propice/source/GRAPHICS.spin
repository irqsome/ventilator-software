''*******************************
''* GRAPHICS.spin               *               
''* Author: Werner L. Schneider *
''*******************************
''
'' Based on Code from Marco Maccaferri 
''
''+------------------------------------------------------------------------------------------------------------------------------+
''|                                 Propeller Game Engine Demo (C) 2013 Marco Maccaferri                                         |
''+------------------------------------------------------------------------------------------------------------------------------+
''|                                   TERMS OF USE: Parallax Object Exchange License                                             |
''+------------------------------------------------------------------------------------------------------------------------------+
''|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    |
''|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
''|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
''|is furnished to do so, subject to the following conditions:                                                                   |
''|                                                                                                                              |
''|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
''|                                                                                                                              |
''|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
''|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
''|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
''|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
''+------------------------------------------------------------------------------------------------------------------------------+
''
''
PUB get_palette_def

    return @palette_def


PUB get_tile_def

    return @tiles_def


PUB get_sprite_def

    return @sprites_def


DAT

'' Palette definitions
palette_def

        'Wall' 
       
        long    $0A_5B_BA_02
        long    $04_2B_EB_8B
        long    $1D_6E_BD_AD
        long    $07_3E_DD_8E
        
        long    $0A_5B_BA_02
        long    $04_2B_EB_8B 
        long    $1D_6E_6E_04
        long    $07_3E_DD_8E

        long    $0A_5B_BA_02
        long    $04_2B_EB_8B 
        long    $1D_6E_8E_AD
        long    $07_3E_DD_8E

        long    $0A_5B_BA_02
        long    $04_2B_EB_8B
        long    $1D_6E_DD_AD
        long    $07_3E_DD_8E

        long    $0A_5B_BA_02
        long    $04_2B_EB_8B
        long    $1D_6E_3E_AD
        long    $07_3E_DD_8E

        'Char Colors'
        ''                          0
        long    $02_02_02_02
        long    $02_02_02_02
        long    $02_02_02_02
        long    $02_02_02_02

        ''                          1
        long    $02_02_BA_02
        long    $02_02_02_02
        long    $02_02_02_02
        long    $02_02_02_02

        ''                          2
        long    $02_02_5B_02
        long    $02_02_02_02
        long    $02_02_02_02
        long    $02_02_02_02

        ''                          3
        long    $02_02_0A_02
        long    $02_02_02_02
        long    $02_02_02_02
        long    $02_02_02_02

        ''                          4
        long    $02_02_8B_02
        long    $02_02_02_02
        long    $02_02_02_02
        long    $02_02_02_02

        ''                          5
        long    $02_02_EB_02
        long    $02_02_02_02
        long    $02_02_02_02
        long    $02_02_02_02

        ''                          6     
        long    $02_02_2B_02
        long    $02_02_02_02
        long    $02_02_02_02
        long    $02_02_02_02

        ''                          7     
        long    $02_02_04_02    
        long    $02_02_02_02
        long    $02_02_02_02
        long    $02_02_02_02

        ''                          8     
        long    $02_02_AD_02
        long    $02_02_02_02
        long    $02_02_02_02
        long    $02_02_02_02

        ''                          9
        long    $02_02_BD_02
        long    $02_02_02_02
        long    $02_02_02_02
        long    $02_02_02_02

        ''                          A
        long    $02_02_6E_02
        long    $02_02_02_02
        long    $02_02_02_02
        long    $02_02_02_02

        ''                          B
        long    $02_02_1D_02
        long    $02_02_02_02
        long    $02_02_02_02
        long    $02_02_02_02

        ''                          C
        long    $02_02_8E_02        
        long    $02_02_02_02
        long    $02_02_02_02
        long    $02_02_02_02

        ''                          D
        long    $02_02_DD_02
        long    $02_02_02_02
        long    $02_02_02_02
        long    $02_02_02_02

        ''                          E
        long    $02_02_3E_02
        long    $02_02_02_02
        long    $02_02_02_02
        long    $02_02_02_02

        ''                          F
        long    $02_02_07_02
        long    $02_02_02_02
        long    $02_02_02_02
        long    $02_02_02_02 


DAT


'' Tiles definitions
tiles_def

        '' name='Font Space'
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=WALL
        long    $11110000
        long    $11110000
        long    $11110000
        long    $11110000
        long    $00001111
        long    $00001111
        long    $00001111
        long    $00001111

        '' name=BLOCKUL
        long    $00000000
        long    $000FFFFF
        long    $00FFFFFF
        long    $0FFFFFFF
        long    $0FFFFFFF
        long    $0FFFFFFF
        long    $0FFFFFFF
        long    $0FFFFFFF

        '' name=BLOCKUR
        long    $00000000
        long    $FFFFF000
        long    $FFFFFF00
        long    $FFFFFFF0
        long    $FFFFFFF0
        long    $FFFFFFF0
        long    $FFFFFFF0
        long    $FFFFFFF0

        '' name=BLOCKLL
        long    $0FFFFFFF
        long    $0FFFFFFF
        long    $0FFFFFFF
        long    $0FFFFFFF
        long    $0FFFFFFF
        long    $00FFFFFF
        long    $000FFFFF
        long    $00000000

        '' name=BLOCKLR
        long    $FFFFFFF0
        long    $FFFFFFF0
        long    $FFFFFFF0
        long    $FFFFFFF0
        long    $FFFFFFF0
        long    $FFFFFF00
        long    $FFFFF000
        long    $00000000

        '' name=ICEUL
        long    $77700007
        long    $70000007
        long    $70700707
        long    $00070077
        long    $00007007
        long    $00700707
        long    $00070077
        long    $77777777

        '' name=ICEUR
        long    $70000777
        long    $70000007
        long    $70700707
        long    $77007000
        long    $70070000
        long    $70700700
        long    $77007000
        long    $77777777

        '' name=ICELL
        long    $77777777
        long    $00070077
        long    $00700707
        long    $00007007
        long    $00070077
        long    $70700707
        long    $70000007
        long    $77700007

        '' name=ICELR
        long    $77777777
        long    $77007000
        long    $70700700
        long    $70070000
        long    $77007000
        long    $70700707
        long    $70000007
        long    $70000777


        '' name='0'
        long    $00000000
        long    $00111100
        long    $01000110
        long    $01001010
        long    $01010010
        long    $01100010
        long    $00111100
        long    $00000000

        '' name='1'
        long    $00000000
        long    $00011000
        long    $00101000
        long    $00001000
        long    $00001000
        long    $00001000
        long    $00111110
        long    $00000000

        '' name='2'
        long    $00000000
        long    $00111100
        long    $01000010
        long    $00000010
        long    $00111100
        long    $01000000
        long    $01111110
        long    $00000000

        '' name='3'
        long    $00000000
        long    $00111100
        long    $01000010
        long    $00001100
        long    $00000010
        long    $01000010
        long    $00111100
        long    $00000000

        '' name='4'
        long    $00000000
        long    $00001000
        long    $00011000
        long    $00101000
        long    $01001000
        long    $01111110
        long    $00001000
        long    $00000000

        '' name='5'
        long    $00000000
        long    $01111110
        long    $01000000
        long    $01111100
        long    $00000010
        long    $01000010
        long    $00111100
        long    $00000000

        '' name='6'
        long    $00000000
        long    $00111100
        long    $01000000
        long    $01111100
        long    $01000010
        long    $01000010
        long    $00111100
        long    $00000000

        '' name='7'
        long    $00000000
        long    $01111110
        long    $00000010
        long    $00000100
        long    $00001000
        long    $00010000
        long    $00010000
        long    $00000000

        '' name='8'
        long    $00000000
        long    $00111100
        long    $01000010
        long    $00111100
        long    $01000010
        long    $01000010
        long    $00111100
        long    $00000000

        '' name='9'
        long    $00000000
        long    $00111100
        long    $01000010
        long    $01000010
        long    $00111110
        long    $00000010
        long    $00111100
        long    $00000000


        '' name=''
        long    $00000000
        long    $00111100
        long    $01000010
        long    $01000010
        long    $01111110
        long    $01000010
        long    $01000010
        long    $00000000

        '' name=''
        long    $00000000
        long    $01111100
        long    $01000010
        long    $01111100
        long    $01000010
        long    $01000010
        long    $01111100
        long    $00000000

        '' name=''
        long    $00000000
        long    $00111100
        long    $01000010
        long    $01000000
        long    $01000000
        long    $01000010
        long    $00111100
        long    $00000000

        '' name=''
        long    $00000000
        long    $01111000
        long    $01000100
        long    $01000010
        long    $01000010
        long    $01000100
        long    $01111000
        long    $00000000

        '' name=''
        long    $00000000
        long    $01111110
        long    $01000000
        long    $01111100
        long    $01000000
        long    $01000000
        long    $01111110
        long    $00000000

        '' name=''
        long    $00000000
        long    $01111110
        long    $01000000
        long    $01111100
        long    $01000000
        long    $01000000
        long    $01000000
        long    $00000000

        '' name=''
        long    $00000000
        long    $00111100
        long    $01000010
        long    $01000000
        long    $01001110
        long    $01000010
        long    $00111100
        long    $00000000

        '' name=''
        long    $00000000
        long    $01000010
        long    $01000010
        long    $01111110
        long    $01000010
        long    $01000010
        long    $01000010
        long    $00000000

        '' name=''
        long    $00000000
        long    $00111110
        long    $00001000
        long    $00001000
        long    $00001000
        long    $00001000
        long    $00111110
        long    $00000000

        '' name=''
        long    $00000000
        long    $00000010
        long    $00000010
        long    $00000010
        long    $01000010
        long    $01000010
        long    $00111100
        long    $00000000

        '' name=''
        long    $00000000
        long    $01000100
        long    $01001000
        long    $01110000
        long    $01001000
        long    $01000100
        long    $01000010
        long    $00000000

        '' name=''
        long    $00000000
        long    $01000000
        long    $01000000
        long    $01000000
        long    $01000000
        long    $01000000
        long    $01111110
        long    $00000000

        '' name=''
        long    $00000000
        long    $01000010
        long    $01100110
        long    $01011010
        long    $01000010
        long    $01000010
        long    $01000010
        long    $00000000

        '' name=''
        long    $00000000
        long    $01000010
        long    $01100010
        long    $01010010
        long    $01001010
        long    $01000110
        long    $01000010
        long    $00000000

        '' name=''
        long    $00000000
        long    $00111100
        long    $01000010
        long    $01000010
        long    $01000010
        long    $01000010
        long    $00111100
        long    $00000000

        '' name=''
        long    $00000000
        long    $01111100
        long    $01000010
        long    $01000010
        long    $01111100
        long    $01000000
        long    $01000000
        long    $00000000

        '' name=''
        long    $00000000
        long    $00111100
        long    $01000010
        long    $01000010
        long    $01010010
        long    $01001010
        long    $00111100
        long    $00000000

        '' name=''
        long    $00000000
        long    $01111100
        long    $01000010
        long    $01000010
        long    $01111100
        long    $01000100
        long    $01000010
        long    $00000000

        '' name=''
        long    $00000000
        long    $00111100
        long    $01000000
        long    $00111100
        long    $00000010
        long    $01000010
        long    $00111100
        long    $00000000

        '' name=''
        long    $00000000
        long    $11111110
        long    $00010000
        long    $00010000
        long    $00010000
        long    $00010000
        long    $00010000
        long    $00000000

        '' name=''
        long    $00000000
        long    $01000010
        long    $01000010
        long    $01000010
        long    $01000010
        long    $01000010
        long    $00111100
        long    $00000000

        '' name=''
        long    $00000000
        long    $01000010
        long    $01000010
        long    $01000010
        long    $01000010
        long    $00100100
        long    $00011000
        long    $00000000

        '' name=''
        long    $00000000
        long    $01000010
        long    $01000010
        long    $01000010
        long    $01000010
        long    $01011010
        long    $00100100
        long    $00000000

        '' name=''
        long    $00000000
        long    $01000010
        long    $00100100
        long    $00011000
        long    $00011000
        long    $00100100
        long    $01000010
        long    $00000000

        '' name=''
        long    $00000000
        long    $10000010
        long    $01000100
        long    $00101000
        long    $00010000
        long    $00010000
        long    $00010000
        long    $00000000

        '' name=''
        long    $00000000
        long    $01111110
        long    $00000100
        long    $00001000
        long    $00010000
        long    $00100000
        long    $01111110
        long    $00000000







        '' name=COLON
        long    $00000000
        long    $00000000
        long    $00011000
        long    $00011000
        long    $00000000
        long    $00011000
        long    $00011000
        long    $00000000

        '' name=SLASH
        long    $00000110
        long    $00001100
        long    $00011000
        long    $00110000
        long    $01100000
        long    $11000000
        long    $10000000
        long    $00000000

        '' name=LEFT
        long    $00000000
        long    $00000000
        long    $00010000
        long    $00110000
        long    $01111110
        long    $00110000
        long    $00010000
        long    $00000000

        '' name=RIGHT
        long    $00000000
        long    $00000000
        long    $00001000
        long    $00001100
        long    $01111110
        long    $00001100
        long    $00001000
        long    $00000000

        '' name=UP
        long    $00000000
        long    $00001000
        long    $00011100
        long    $00111110
        long    $00001000
        long    $00001000
        long    $00001000
        long    $00000000

        '' name=DN
        long    $00000000
        long    $00010000
        long    $00010000
        long    $00010000
        long    $01111100
        long    $00111000
        long    $00010000
        long    $00000000

        '' name=DOT
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00011000
        long    $00011000
        long    $00000000

        '' name=EXC
        long    $00011000
        long    $00111100
        long    $00111100
        long    $00011000
        long    $00011000
        long    $00000000
        long    $00011000
        long    $00000000

        '' name=QUOT
        long    $00000000
        long    $00100100
        long    $00100100
        long    $00100100
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=BALL
        long    $00000000
        long    $00111100
        long    $01111110
        long    $01111110
        long    $01111110
        long    $01111110
        long    $00111100
        long    $00000000

        '' name=BLOCK2
        long    $00000000
        long    $00111110
        long    $01111111
        long    $01111111
        long    $01111111
        long    $01111111
        long    $01111111
        long    $00111110

        '' name=CURSOR
        long    $11111111
        long    $11111111
        long    $11111111
        long    $11111111
        long    $11111111
        long    $11111111
        long    $11111111
        long    $11111111

        '' name=EAT_0UL
        long    $00000000
        long    $000F00FF
        long    $00FFFF00
        long    $0FF000FF
        long    $000FF00F
        long    $0000FF0F
        long    $0FF00F0F
        long    $0F000F00

        '' name=EAT_0UR
        long    $00000000
        long    $00FFF000
        long    $F000FF00
        long    $FFFFF000
        long    $F000FFF0
        long    $FFFF00F0
        long    $F0000FF0
        long    $FFFFF000

        '' name=EAT_0LL
        long    $0FF000FF
        long    $00FFFFFF
        long    $000F000F
        long    $00FFFF00
        long    $0FF00FFF
        long    $00F0000F
        long    $000FFFFF
        long    $00000000

        '' name=EAT_0LR
        long    $FFFF00F0
        long    $000FFF00
        long    $FF000F00
        long    $00FFF000
        long    $FFF00FF0
        long    $F0000F00
        long    $FFFFF000
        long    $00000000

        '' name=EAT_1UL
        long    $00000000
        long    $000F000F
        long    $0000FF00
        long    $00F000F0
        long    $000F000F
        long    $00000F00
        long    $00F0000F
        long    $0F000F00

        '' name=EAT_1UR
        long    $00000000
        long    $00F00000
        long    $F0000F00
        long    $00F00000
        long    $0000F000
        long    $000F00F0
        long    $F0000FF0
        long    $F000F000

        '' name=EAT_1LL
        long    $0FF000FF
        long    $00F0000F
        long    $000F000F
        long    $000F0000
        long    $0F00000F
        long    $00F00000
        long    $000000FF
        long    $00000000

        '' name=EAT_1LR
        long    $000000F0
        long    $000F0000
        long    $F0000F00
        long    $00F00000
        long    $00000FF0
        long    $F0000000
        long    $000FF000
        long    $00000000

        '' name=EAT_2UL
        long    $00000000
        long    $000F0000
        long    $00000000
        long    $00F00000
        long    $00000000
        long    $00000F00
        long    $00F00000
        long    $00000000

        '' name=EAT_2UR
        long    $00000000
        long    $00F00000
        long    $00000F00
        long    $00000000
        long    $00000000
        long    $000F00F0
        long    $000000F0
        long    $00000000

        '' name=EAT_2LL
        long    $00F000F0
        long    $00000000
        long    $00000000
        long    $000F0000
        long    $0F00000F
        long    $00000000
        long    $000000F0
        long    $00000000

        '' name=EAT_2LR
        long    $000000F0
        long    $00000000
        long    $00000000
        long    $00F00000
        long    $00000000
        long    $00000000
        long    $000FF000
        long    $00000000



        '' name=HAT_1
        long    $00000000
        long    $00000000
        long    $09999999
        long    $99999999
        long    $99999999
        long    $99999999
        long    $99999999
        long    $09999999

        '' name=HAT_2
        long    $00000000
        long    $00000000
        long    $99999900
        long    $99999999
        long    $99999999
        long    $99999999
        long    $99999999
        long    $99999999

        '' name=HAT_3
        long    $00000000
        long    $00000000
        long    $00000000
        long    $90000000
        long    $99999999
        long    $99999999
        long    $99999999
        long    $99999999

        '' name=HAT_4
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $99999999
        long    $99999999
        long    $99999000
        long    $00000000

        '' name=HAT_5
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00099900
        long    $99999999
        long    $90099900
        long    $00009000
        long    $00009000

        '' name=HAT_6
        long    $00000000
        long    $00000000
        long    $00099999
        long    $99999999
        long    $99999999
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_7
        long    $00000999
        long    $99999999
        long    $99999999
        long    $99999999
        long    $99999999
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_8
        long    $99999999
        long    $99999999
        long    $99999999
        long    $99999999
        long    $99999999
        long    $09999999
        long    $00009999
        long    $00000000

        '' name=HAT_9
        long    $99999990
        long    $99999999
        long    $99999999
        long    $99999999
        long    $99999999
        long    $99999999
        long    $99999990
        long    $00000000

        '' name=HAT_10
        long    $00099999
        long    $00000099
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_11
        long    $99999999
        long    $99990000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_12
        long    $99000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_13
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $000000BB

        '' name=HAT_14
        long    $00022200
        long    $00022200
        long    $00022200
        long    $00222200
        long    $00222220
        long    $00222220
        long    $00022200
        long    $CCC888CC

        '' name=HAT_15
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $CBB00000

        '' name=HAT_16
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_17
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_18
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_19
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_20
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_21
        long    $00000000
        long    $00000000
        long    $000000BB
        long    $00000BBB
        long    $000BBBBB
        long    $00BBBBBC
        long    $0BBBBBCC
        long    $BBBBBCCC

        '' name=HAT_22
        long    $000BBBCC
        long    $BBBCCCCC
        long    $BBCCCCCC
        long    $BCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCC8
        long    $CCCCCCC8

        '' name=HAT_23
        long    $CC88888C
        long    $C8888888
        long    $C8888888
        long    $88888888
        long    $88888888
        long    $88888888
        long    $88888888
        long    $88888888

        '' name=HAT_24
        long    $CCCBBB00
        long    $CCCCCBBB
        long    $CCCCCCCB
        long    $CCCCCCCC
        long    $8CCCCCCC
        long    $8CCCCCCC
        long    $8CCCCCCC
        long    $88CCCCCC

        '' name=HAT_25
        long    $00000000
        long    $00000000
        long    $BBB00000
        long    $BBBB0000
        long    $CBBBBB00
        long    $CCBBBBB0
        long    $CCCBBBBB
        long    $CCCCBBBB

        '' name=HAT_26
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $B0000000

        '' name=HAT_27
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_28
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_29
        long    $0000000B
        long    $000000BB
        long    $00000BBB
        long    $0000BBBB
        long    $000BBBBB
        long    $000BBBBB
        long    $00BBBBBB
        long    $0BBBBBBB

        '' name=HAT_30
        long    $BBBBCCCC
        long    $BBBBCCCC
        long    $BBBCCCCC
        long    $BBCCCCCC
        long    $BBCCCCCC
        long    $BBCCCCCC
        long    $BCCCCCCC
        long    $BCCCCCCC

        '' name=HAT_31
        long    $CCCCCCC8
        long    $CCCCCC88
        long    $CCCCCC88
        long    $CCCCCC88
        long    $CCCCCC88
        long    $CCCCCC88
        long    $CCCCCC88
        long    $CCCCC888

        '' name=HAT_32
        long    $88888888
        long    $88888888
        long    $88888888
        long    $88888888
        long    $88888888
        long    $88888888
        long    $88888888
        long    $88888888

        '' name=HAT_33
        long    $88CCCCCC
        long    $888CCCCC
        long    $888CCCCC
        long    $888CCCCC
        long    $888CCCCC
        long    $888CCCCC
        long    $888CCCCC
        long    $8888CCCC

        '' name=HAT_34
        long    $CCCCCBBB
        long    $CCCCCBBB
        long    $CCCCCCBB
        long    $CCCCCCBB
        long    $CCCCCCCB
        long    $CCCCCCCB
        long    $CCCCCCCC
        long    $CCCCCCCC

        '' name=HAT_35
        long    $BB000000
        long    $BBB00000
        long    $BBBB0000
        long    $BBBBB000
        long    $BBBBBB00
        long    $BBBBBB00
        long    $BBBBBBB0
        long    $BBBBBBBB

        '' name=HAT_36
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_37
        long    $00000000
        long    $00000000
        long    $00000000
        long    $0000000B
        long    $0000000B
        long    $0000000B
        long    $000000BB
        long    $000000BB

        '' name=HAT_38
        long    $0BBBBBBB
        long    $0BBBBBBB
        long    $BBBBBBBC
        long    $BBBBBBBC
        long    $BBBBBBBC
        long    $BBBBBBBC
        long    $BBBBBBCC
        long    $BBBBBBCC

        '' name=HAT_39
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC

        '' name=HAT_40
        long    $CCCCC888
        long    $CCCCC888
        long    $CCCCC888
        long    $CCCCC888
        long    $CCCCC888
        long    $CCCC8888
        long    $CCCC8888
        long    $CCCC8888

        '' name=HAT_41
        long    $88888888
        long    $88888888
        long    $88888888
        long    $88888888
        long    $88888888
        long    $88888888
        long    $88888888
        long    $88888888

        '' name=HAT_42
        long    $8888CCCC
        long    $8888CCCC
        long    $8888CCCC
        long    $8888CCCC
        long    $8888CCCC
        long    $88888CCC
        long    $88888CCC
        long    $88888CCC

        '' name=HAT_43
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC

        '' name=HAT_44
        long    $CBBBBBBB
        long    $CBBBBBBB
        long    $CCBBBBBB
        long    $CCBBBBBB
        long    $CCBBBBBB
        long    $CCBBBBBB
        long    $CCCBBBBB
        long    $CCCBBBBB

        '' name=HAT_45
        long    $00000000
        long    $00000000
        long    $B0000000
        long    $BB000000
        long    $BB000000
        long    $BB000000
        long    $BBB00000
        long    $BBB00000

        '' name=HAT_46
        long    $000000BB
        long    $00000BBB
        long    $00000BBB
        long    $00000BBB
        long    $00000BBB
        long    $00000BBB
        long    $00000BBB
        long    $0000BBBB

        '' name=HAT_47
        long    $BBBBBBCC
        long    $BBBBBBCC
        long    $BBBBBBCC
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBCCC

        '' name=HAT_48
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBCCC
        long    $CCCCCCCC

        '' name=HAT_49
        long    $CCCC8888
        long    $CCCC8888
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $CCCCCCCC
        long    $CCCCCCCC

        '' name=HAT_50
        long    $88888888
        long    $88888888
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $CCCCCCCC
        long    $CCCCCCCC

        '' name=HAT_51
        long    $88888CCC
        long    $88888CCC
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $CCCCCCCC
        long    $CCCCCCCC

        '' name=HAT_52
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $BBCCCCCC
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $CCCCBBBB
        long    $CCCCCCCC

        '' name=HAT_53
        long    $CCCBBBBB
        long    $CCCBBBBB
        long    $CCCBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $CCCCBBBB

        '' name=HAT_54
        long    $BBB00000
        long    $BBBB0000
        long    $BBBB0000
        long    $BBBB0000
        long    $BBBB0000
        long    $BBBB0000
        long    $BBBB0000
        long    $BBBBB000

        '' name=HAT_55
        long    $0000BBBB
        long    $0000BBBB
        long    $0000BBBB
        long    $00000BBB
        long    $000000BB
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_56
        long    $BCCCCCCC
        long    $CCCCCCCC
        long    $BBBBBCCC
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $00BBBBBB
        long    $00000000
        long    $00000000

        '' name=HAT_57
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $BBBBBBCC
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $0BBBBBBB
        long    $00000000

        '' name=HAT_58
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $0BBBBBBB

        '' name=HAT_59
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB

        '' name=HAT_60
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBB0

        '' name=HAT_61
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $00000000

        '' name=HAT_62
        long    $CCCCCCCC
        long    $CCCCCCCC
        long    $CCCCBBBB
        long    $BBBBBBBB
        long    $BBBBBBBB
        long    $BBBBBBB0
        long    $00000000
        long    $00000000

        '' name=HAT_63
        long    $CBBBB000
        long    $CBBBB000
        long    $BBBBB000
        long    $BBBB0000
        long    $BBB00000
        long    $00000000
        long    $00000000
        long    $00000000


DAT


'' Sprites definitions
sprites_def

        '' name=EXC_L
        long    $80000000, $00000000
        long    $08008080, $80808080
        long    $00808080, $80808080
        long    $00808080, $80808080
        long    $00800880, $00008800
        long    $00808888, $88888880
        long    $00808888, $88888880
        long    $00888880, $00888880
        long    $00888880, $00888880
        long    $00808888, $88888880
        long    $00808888, $88888880
        long    $00800880, $00008800
        long    $00808080, $80808080
        long    $00808080, $80808080
        long    $08008080, $80808080
        long    $80000000, $00000000

        '' name=EXC_R
        long    $00000000, $00000008
        long    $08080808, $08080080
        long    $08080808, $08080800
        long    $08080808, $08080800
        long    $00880000, $08800800
        long    $08888888, $88880800
        long    $08888888, $88880800
        long    $08888800, $08888800
        long    $08888800, $08888800
        long    $08888888, $88880800
        long    $08888888, $88880800
        long    $00880000, $08800800
        long    $08080808, $08080800
        long    $08080808, $08080800
        long    $08080808, $08080080
        long    $00000000, $00000008

        '' name=EXC_U
        long    $80000000, $00000008
        long    $08000000, $00000080
        long    $00888888, $88888800
        long    $00000008, $80000000
        long    $08880888, $88808880
        long    $00008888, $88880000
        long    $08888888, $88888880
        long    $00000880, $08800000
        long    $08880880, $08808880
        long    $00000880, $08800000
        long    $08880888, $88808880
        long    $00000888, $88800000
        long    $08888888, $88888880
        long    $00008888, $88880000
        long    $08880888, $88808880
        long    $00000000, $00000000

        '' name=EXC_D
        long    $00000000, $00000000
        long    $08880888, $88808880
        long    $00008888, $88880000
        long    $08888888, $88888880
        long    $00000888, $88800000
        long    $08880888, $88808880
        long    $00000880, $08800000
        long    $08880880, $08808880
        long    $00000880, $08800000
        long    $08888888, $88888880
        long    $00008888, $88880000
        long    $08880888, $88808880
        long    $00000008, $80000000
        long    $00888888, $88888800
        long    $08000000, $00000080
        long    $80000000, $00000008

        '' name=BLOCK
        long    $00000000, $00000000
        long    $000FFFFF, $FFFFF000
        long    $00FFFFFF, $FFFFFF00
        long    $0FFFFFFF, $FFFFFFF0
        long    $0FFFFFFF, $FFFFFFF0
        long    $0FFFFFFF, $FFFFFFF0
        long    $0FFFFFFF, $FFFFFFF0
        long    $0FFFFFFF, $FFFFFFF0
        long    $0FFFFFFF, $FFFFFFF0
        long    $0FFFFFFF, $FFFFFFF0
        long    $0FFFFFFF, $FFFFFFF0
        long    $0FFFFFFF, $FFFFFFF0
        long    $0FFFFFFF, $FFFFFFF0
        long    $00FFFFFF, $FFFFFF00
        long    $000FFFFF, $FFFFF000
        long    $00000000, $00000000

        '' name=ICE
        long    $77700007, $70000777
        long    $70000007, $70000007
        long    $70700707, $70700707
        long    $00070077, $77007000
        long    $00007007, $70070000
        long    $00700707, $70700700
        long    $00070077, $77007000
        long    $77777777, $77777777
        long    $77777777, $77777777
        long    $00070077, $77007000
        long    $00700707, $70700700
        long    $00007007, $70070000
        long    $00070077, $77007000
        long    $70700707, $70700707
        long    $70000007, $70000007
        long    $77700007, $70000777

        '' name=ENEMY_0
        long    $00000000, $00000000
        long    $00000000, $00000000
        long    $00000000, $00000000
        long    $00000000, $00000000
        long    $00000000, $00000000
        long    $00000099, $99000000
        long    $00000999, $99900000
        long    $00000999, $99900000
        long    $00000999, $99900000
        long    $00000999, $99900000
        long    $00000099, $99000000
        long    $00000000, $00000000
        long    $00000000, $00000000
        long    $00000000, $00000000
        long    $00000000, $00000000
        long    $00000000, $00000000

        '' name=ENEMY_1
        long    $00000000, $00000000
        long    $00000000, $00000000
        long    $00000000, $00000000
        long    $00000099, $99000000
        long    $00009999, $99990000
        long    $00009999, $99990000
        long    $00099999, $99999000
        long    $00099999, $99999000
        long    $00099999, $99999000
        long    $00099999, $99999000
        long    $00009999, $99990000
        long    $00009999, $99990000
        long    $00000099, $99000000
        long    $00000000, $00000000
        long    $00000000, $00000000
        long    $00000000, $00000000

        '' name=ENEMY_2
        long    $00000999, $99900000
        long    $00099999, $99999000
        long    $00999999, $99999900
        long    $09999999, $99999990
        long    $09999999, $99999990
        long    $99999999, $99999999
        long    $99999999, $99999999
        long    $99999999, $99999999
        long    $99999999, $99999999
        long    $99999999, $99999999
        long    $99999999, $99999999
        long    $09999999, $99999990
        long    $09999999, $99999990
        long    $00999999, $99999900
        long    $00099999, $99999000
        long    $00000999, $99900000

        '' name=ENEMY_N
        long    $00000999, $99900000
        long    $00099999, $99999000
        long    $00999999, $99999900
        long    $09900099, $99000990
        long    $09000009, $90000090
        long    $99000009, $90000099
        long    $99900099, $99000999
        long    $99999999, $99999999
        long    $99999999, $99999999
        long    $99999999, $99999999
        long    $99990000, $00009999
        long    $09900000, $00000990
        long    $09000999, $99900090
        long    $00999999, $99999900
        long    $00099999, $99999000
        long    $00000999, $99900000

        '' name=ENEMY_L
        long    $00000999, $99900000
        long    $00099999, $99999000
        long    $00999999, $99999900
        long    $09900099, $99999990
        long    $09000009, $99999990
        long    $99000009, $99999999
        long    $99900099, $99999999
        long    $99999999, $99999999
        long    $99999999, $99999999
        long    $99999999, $99999999
        long    $00000999, $99999999
        long    $00000099, $99999990
        long    $09999009, $99999990
        long    $00999999, $99999900
        long    $00099999, $99999000
        long    $00000999, $99900000

        '' name=ENEMY_R
        long    $00000999, $99900000
        long    $00099999, $99999000
        long    $00999999, $99999900
        long    $09999999, $99000990
        long    $09999999, $90000090
        long    $99999999, $90000099
        long    $99999999, $99000999
        long    $99999999, $99999999
        long    $99999999, $99999999
        long    $99999999, $99999999
        long    $99999999, $99900000
        long    $09999999, $99000000
        long    $09999999, $90099990
        long    $00999999, $99999900
        long    $00099999, $99999000
        long    $00000999, $99900000


CON

'' Tiles
FONT_SPACE = 0
WALL       = 1 
BLOCKUL    = 2
BLOCKUR    = 3
BLOCKLL    = 4
BLOCKLR    = 5
ICEUL      = 6
ICEUR      = 7
ICELL      = 8
ICELR      = 9
FONT_0     = 10
FONT_A     = 20
FONT_COLON = 46
FONT_SLASH = 47
FONT_LEFT  = 48
FONT_RIGHT = 49
FONT_UP    = 50
FONT_DN    = 51
FONT_DOT   = 52
FONT_EXC   = 53
FONT_QUOT  = 54
BALL       = 55
BLOCK2     = 56
CURSOR     = 57
EAT_0UL    = 58 
EAT_0UR    = 59 
EAT_0LL    = 60 
EAT_0LR    = 61 
EAT_1UL    = 62
EAT_1UR    = 63 
EAT_1LL    = 64 
EAT_1LR    = 65
EAT_2UL    = 66 
EAT_2UR    = 67 
EAT_2LL    = 68 
EAT_2LR    = 69 
HAT        = 70


'' Sprites
EXC_L     = 0
EXC_R     = 1
EXC_U     = 2
EXC_D     = 3
BLOCK     = 4
ICE       = 5
ENEMY_0   = 6
ENEMY_1   = 7
ENEMY_2   = 8
ENEMY_N   = 9
ENEMY_L   = 10
ENEMY_R   = 11