'--------------------------------------------------------------------------------------------------------------
'
' File:    jkiss32.spin
'
' Author:  Michael Rychlik
'
' License: MIT (See end of file for details)
'
' Revision History:
'
' 1.0      31-11-2011  Initial version.
'
'--------------------------------------------------------------------------------------------------------------
'
' Implementation of a 32-bit KISS PRNG which uses no multiply instructions, no unsigned
' arithmetic and has a long period.
'
'--------------------------------------------------------------------------------------------------------------
'
' In looking for a good Pseudo Random Number Generator (PRNG) with a long period, far longer than Spin's
' built in PRNG, we find that many good candidates rely on unsigned arithmetic, which is not available in
' Spin, or make use of multiplication which is slow on the Propeller. George Marsaglia proposed using an
' add-with-carry generator rather than the superior multiply-with-carry to get around these problems.
' This PRNG is based on Marsaglia's ideas as implemented by David Jones of the UCL Bioinformatics Group.
' it is one of the simplest and fastest PRNGs which passes all of the Dieharder tests and the BigCrunch tests
' in TestU01. It has a quite long period of ≈2^121 = 2.6 x 10^36.
'
' The following is David Jones implementation in C which can be found in in his paper
' "Good Practice in (Pseudo) Random Number Generation for ' Bioinformatics Applications" of May 7th 2010.
' Which was last seen here: http://www.cs.ucl.ac.uk/staff/d.jones/GoodPracticeRNG.pdf
'
' /* Implementation of a 32-bit KISS generator which uses no multiply instructions */
' static unsigned int x=123456789,y=234567891,z=345678912,w=456789123,c=0;
' unsigned int JKISS32()
' {
'     int t;
'     y ^= (y<<5); y ^= (y>>7); y ^= (y<<22);
'     t = z+w+c; z = w; c = t < 0; w = t&2147483647;
'     x += 1411392427;
'     return x + y + w;
' }
'
'--------------------------------------------------------------------------------------------------------------


VAR
    long x
    long y
    long z
    long w
    long c

' Start with default seed values, these values will be used if zeros are given in the seed later.
' Also good for checking results against the original JKISS32 in C.
PUB start
    x := 123456789
    y := 234567891
    z := 345678912
    w := 456789123
    c := 0

' Seed the generator
' Caller should avoid use of zero or small numbers and try to use large "complex" numbers.
PUB seed (xx, yy, zz, ww, cc)
    if xx <> 0         ' Probably better not to have x as zero
        x := xx
    if YY <> 0         ' y must not be zero
        y := yy
    if zz <> 0         ' Probably better not to have z as zero
        z := zz
    if ww <> 0         ' Probably better not to have w as zero
        w := ww
    c := cc & 1         'Carry is 1 or zero.

' Get the next pseudo random number in the sequence
PUB random | t
    y ^= (y <<  5)
    y ^= (y >>  7)
    y ^= (y << 22)
    t := z + w + c
    z := w
    c := (t < 0) & 1
    w := t & 2147483647
    x += 1411392427
    return (x + y + w)

'--------------------------------------------------------------------------------------------------------------
'                         TERMS OF USE: MIT License
'--------------------------------------------------------------------------------------------------------------
' Permission is hereby granted, free of charge, to any person obtaining a copy of
' this software and associated documentation files (the "Software"), to deal in
' the Software without restriction, including without limitation the rights to
' use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
' the Software, and to permit persons to whom the Software is furnished to do so,
' subject to the following conditions:
'
' The above copyright notice and this permission notice shall be included in all
' copies or substantial portions of the Software.
'
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
' IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
' FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
' COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
' IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
' CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'--------------------------------------------------------------------------------------------------------------



