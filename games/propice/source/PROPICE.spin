''*************************************
''* PROPICE_1.1_NO_EEPROM.spin 1/2020 *               
''* Author: Werner L. Schneider       *
''*************************************
''
'' Based on Code from Propeller Game Engine Demo (C) 2013 Marco Maccaferri 
'' Based on Code from Marko Lukat
'' Based on PS/2 Keyboard Driver v1.0.1 by Chip Gracey Copyright (c) 2004 Parallax, Inc.
'' Based on AYcog - AY3891X / YM2149F emulator V0.8 (C) 2012 Johannes Ahlebrand                                 
''
''+------------------------------------------------------------------------------------------------------------------------------+
''|                                   TERMS OF USE: Parallax Object Exchange License                                             |
''+------------------------------------------------------------------------------------------------------------------------------+
''|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    |
''|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
''|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
''|is furnished to do so, subject to the following conditions:                                                                   |
''|                                                                                                                              |
''|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
''|                                                                                                                              |
''|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
''|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
''|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
''|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
''+------------------------------------------------------------------------------------------------------------------------------+
''
''
CON

    _XINFREQ = 5_000_000
    _CLKMODE = XTAL1 + PLL16X


    right      = 10                     ' P10 Audio Right
    left       = 11                     ' P11 Audio Left

    kbd        = 8                     ' P26 Keyboard Data                                                                    Q
    kbc        = 9                     ' P27 Keyboard Clock

    '' joypad serial shifter settings
    JOY_CLK = 16
    JOY_LCH = 17
    JOY_DATAOUT0 = 19
    JOY_DATAOUT1 = 18
    

OBJ

    ay1:     "AYCOG"                    ' AY3891X Emulator     need 1 Cog 

    data:    "GRAPHICS"                 ' Tiles + Sprites      Main Cog

    prng:    "JKISS32"                  ' Pseudo Random        Main Cog 

    kb:      "KEYBOARD"                 ' Keyboard Driver      need 1 Cog

    play:    "PLAY"                     ' Sound Driver         need 1 Cog

    rr:      "REALRANDOM"               ' Random Generator     need 1 Cog at Startup, then release Cog

    render:  "RENDERER_320x240"         ' Renderer             need 3 Cog's

    driver:   "TV_DRIVER"                ' VGA Driver           need 1 Cog 


VAR

    long pad_state,pad_prev,pad_press
    
    long frame_buffer[80]
    long link

    long ay1_regs           

    long ptr
    long score
    long bonus
    long count
    long mcount
    long freq

    byte mapbuff[247]

    byte tmp[16]

    byte exc_act
    byte exc_x
    byte exc_y
    word exc_xa
    word exc_ya
    byte exc_r
    byte exc_c
    byte exc_done
    byte exc_d
    byte exc_move
    
    byte shot_x
    byte shot_y
    word shot_xa
    word shot_ya
    byte shot_d
    byte shot_act
    byte shot_move
    byte shot_type
  
    byte level
    byte music 
    byte gwait
    byte blocks
    byte crystals
    byte lifes
    byte enemys_max

    byte enemys[15]
    byte enemy_x[5] 
    byte enemy_y[5] 
    word enemy_xa[5] 
    word enemy_ya[5] 
    byte enemy_d[5] 
    byte enemy_act[5] 
    byte enemy_eat[5] 
    byte enemy_eatwait[5] 
    byte enemy_eatpos[5] 
    word enemy_eatptr[5] 
    byte enemy_color[5] 
    byte enemy_life[5] 
    byte enemy_wait[5]
    byte enemy_stat[5]
    byte enemy_move[5]
    byte enemy_stop[5]

    word testx[2]
    word testy[2]


PUB start | i, y, state, finish, mel
    init

    state := 0

    gwait := 12

    music := 1

    repeat

        flip
        nes_read_gamepad
    
        if gwait <> 0
            waitcnt((clkfreq / 1000) * gwait + cnt) 

        case state

            0:

                intro
                state := 1

            1:

                initWall

                enemys_max := 2

                crystals := 3

                score := 0

                bonus := 1000
                
                lifes := 5

                level := 1
                
                state := 2

            2:

                createMap

                repeat
                    y := (prng.random & $7) + (prng.random & $3) + (prng.random & $1) + (prng.random & $1)        ' Random 0 - 12 

                    if mapbuff[y * 19] == 0

                        exc_x := 0    
                        exc_y := y
                        exc_d := 1
                        exc_act := 1
                        exc_move := 0

                        exc_xa := (exc_x * 16) + 8
                        exc_ya := (exc_y * 16) + 16

                        printEXC(0)

                        initEnemys

                        quit

                play.wait(3478)

                if music == 1
                    play.melody(@@melody[0])

                updateEnemys

                state := 3

            3:

                initEnemy

                growEnemys
                
                moveEXC

                initShot

                moveShot

                enemysEat

                moveEnemys
                
                printStatus
                
                checkMusic

                if blocks == 0
                    exc_act := 0

                if exc_act == 0

                    play.noise(@noise2)

                    exc_d := 2                  ' Up
                    if exc_xa < 160
                        exc_r := 1              ' Right
                    else
                        exc_r := 0              ' Left
                    exc_c := 10            
                    exc_done := 0    

                    lifes--

                    count := 0
                    mcount := 0

                    state := 4


                finish := 0
                repeat i from 0 to enemys_max-1
                    finish += enemy_life[i]
                if finish == 0
                    state := 7

                if music == 1
                    if play.stat == 0
                        mel := prng.random & $1 + prng.random & $1
                        play.melody(@@melody[mel])


            4:

                initEnemy

                growEnemys
                
                deadEXC

                moveShot

                enemysEat

                moveEnemys
                
                printStatus

                checkMusic

                if music == 1

                    if count == 0 and play.stat == 0
                        play.melody(@@melody[mel])                    
                        count := 1

                    if count == 1 and play.stat == 0
                        play.wait(2400)
                        play.melody(@@melody[mel])                    
                        count := 2

                else

                    mcount++
                    if mcount == 600
                        play.wait(2400)

                if (count == 2 and play.stat == 0 and testEat == 0) or (mcount > 900 and testEat == 0) 
                    
                    allOff

                    clrEnemys

                    if lifes > 0
                        printFrame(0, 1, 40, 28, data#WALL, $2)

                    flip
                    waitcnt((clkfreq / 1000) * 1000 + cnt)  ' wait 1000 ms
                    flip

                    state := 5
                
            5:

                if lifes == 0
                    state := 6
                else
                    state := 2

            6:

                if testEat == 0

                    play.wait(3478)

                    endGame

                    state := 0
            
            7:

                sprite_hide(5)                        ' Sprite 5 = EXC
                sprite_hide(6)                        ' Sprite 6 = Shot
                shot_act := 0

                flip

                printFrame(12, 12, 16, 5, data#WALL, $2)

                strxy(14, 14, $F, @strBonus)

                dec(25, 14, $C, bonus)

                flip
                flip

                waitcnt(clkfreq * 1 + cnt)                

                repeat bonus / 1000
                    incrScore(1000)
                    bonus -= 1000
                    repeat i from 20 to 24
                        printxy(i, 14, 7, $20)
                    dec(25, 14, $C, bonus)
                    flip
                    play.tone(@tone3)
                    waitcnt(clkfreq * 1 + cnt)                

                bonus := 1000

                enemys_max++
                if enemys_max == 6
                    enemys_max := 2
                    level++
                    crystals++
                    if level == 4
                        level := 3
                        if gwait <> 0
                            gwait -= 4

                printFrame(0, 1, 40, 28, data#WALL, $2)

                flip
                waitcnt((clkfreq / 1000) * 1000 + cnt)  ' wait 1000 ms
                flip

                state := 2


PRI init

    rr.start                                                            ' Start Real Random, uses a COG
    repeat while rr.random == 0                                         ' Wait for Real Random to warm up

    prng.start                                                          ' Start the Pseudo Random Number generator
                                                                        ' (Uses default seed values)
    prng.seed (rr.random, rr.random, rr.random, rr.random, rr.random)   ' Seed the PRNG with real random numbers
    rr.stop                                                             ' Free Real Random COG

    kb.start(kbd, kbc)                                                  ' Start Keyboard Driver

    waitcnt((clkfreq / 1000) * 1000 + cnt)  ' wait 1000 ms

    link{0} := @frame_buffer{0} << 16 | @frame_buffer{0}
    driver.start(@link{0})

    render.start(@frame_buffer, @link, @map1, data.get_tile_def, data.get_palette_def, data.get_sprite_def, @sprite_state)

    ay1_Regs := ay1.start(right, left)                                  ' Start the emulated AY chip in one cog
    ay1.resetRegisters                                                  ' Reset all AY registers

    play.start(ay1_Regs)

DAT
''********************************************************************
''* Functions for Enemys                                             *
''********************************************************************
''
PRI updateEnemys | i, j, x

    repeat i from 0 to 14
        enemys[i] := 0

    j := 0
    repeat i from 0 to enemys_max-1
        if enemy_color[i] == 0
            repeat enemy_life[i]
                enemys[j++] := 9

    repeat i from 0 to enemys_max-1
        if enemy_color[i] == 1
            repeat enemy_life[i]
                enemys[j++] := 10

    repeat i from 0 to enemys_max-1
        if enemy_color[i] == 2
            repeat enemy_life[i]
                enemys[j++] := 12

    repeat i from 0 to enemys_max-1
        if enemy_color[i] == 3
            repeat enemy_life[i]
                enemys[j++] := 13

    repeat i from 0 to enemys_max-1
        if enemy_color[i] == 4
            repeat enemy_life[i]
                enemys[j++] := 14

    clrEnemys

    x := 0
    repeat i from 0 to 14
        if enemys[i] == 0
            quit
        else
            x++

    ptr := get_tilemap_address((40-x) / 2, 0)
    repeat i from 0 to 14
        if enemys[i] == 0
            quit
        else
            print(enemys[i], $2A)
    print($0, $20)


PRI initEnemys | i, x, y, err

    repeat i from 0 to enemys_max-1
        if enemy_act[i] == 0 

            enemy_act[i] := 1
            enemy_life[i] := level 
            enemy_wait[i] := ((prng.random & $F) + 1) * 16
            enemy_stat[i] := 0
            enemy_stop[i] := 0
            enemy_d[i] := (prng.random & $3)         ' Random 0 - 3
            enemy_color[i] := (prng.random & $3) + (prng.random & $1)                                    ' Random 0 - 4

            repeat

                x := (prng.random & $7) + (prng.random & $7) + (prng.random & $3) + 1                    ' Random 1 - 18 
                y := (prng.random & $7) + (prng.random & $3) + (prng.random & $1) + (prng.random & $1)   ' Random 0 - 12 

                if mapbuff[x + (y * 19)] == 0
                
                    enemy_x[i] := x
                    enemy_y[i] := y

                    enemy_xa[i] := (enemy_x[i] * 16) + 8
                    enemy_ya[i] := (enemy_y[i] * 16) + 16

                    testx[0] := enemy_x[i] 
                    testy[0] := enemy_y[i]

                    err := probeEnemy(i)

                    if err == 0
                        quit


PRI initEnemy | i, x, y, err

    repeat i from 0 to enemys_max-1
        if enemy_act[i] == 0 and enemy_life [i] > 0

            enemy_wait[i]--
            if enemy_wait[i] == 0

                enemy_act[i] := 1
                enemy_wait[i] := ((prng.random & $7) + 1) * 16
                enemy_stat[i] := 0
                enemy_stop[i] := 0

                enemy_d[i] := (prng.random & $3)         ' Random 0 - 3

                repeat

                    x := (prng.random & $7) + (prng.random & $7) + (prng.random & $3) + 1                    ' Random 1 - 18 
                    y := (prng.random & $7) + (prng.random & $3) + (prng.random & $1) + (prng.random & $1)   ' Random 0 - 12 

                    if mapbuff[x + (y * 19)] == 0
                
                        enemy_x[i] := x
                        enemy_y[i] := y

                        enemy_xa[i] := (enemy_x[i] * 16) + 8
                        enemy_ya[i] := (enemy_y[i] * 16) + 16

                        testx[0] := enemy_x[i] 
                        testy[0] := enemy_y[i]

                        err := probeEnemy(i)

                        if err == 0
                            quit


PRI probeEnemy(i) | j, contact

    contact := 0
    repeat j from 0 to enemys_max-1
        if j <> i
            if enemy_act[j] == 1
                if testx[0] == enemy_x[j] and testy[0] == enemy_y[j]
                    contact := 1

    return contact


PRI growEnemys | i

    repeat i from 0 to enemys_max-1
        if enemy_act[i] == 1 and enemy_stat[i] < 4

            enemy_wait[i]--
            if enemy_wait[i] == 0
                sprite_show(i, data#ENEMY_0 + enemy_stat[i], enemy_color[i], (enemy_x[i] * 16) + 8, (enemy_y[i] * 16) + 16)
                enemy_wait[i] := 32
                enemy_stat[i]++


PRI enemysEat | i

    repeat i from 0 to enemys_max-1

        if enemy_eat[i] > 0

            enemy_eatwait[i]--
            if enemy_eatwait[i] == 0

                eatBlock(enemy_eatptr[i], enemy_eat[i])
                
                if enemy_eat[i] == 4
                    play.noise(@noise1)

                enemy_eatwait[i] := 16
                enemy_eat[i]--
                if enemy_eat[i] == 0
                    blocks--
                    enemy_stop[i] := 0 


PRI eatBlock(p, t)

    if t == 4
        word[p] := data#EAT_0UL         ' Tile 55, Palette 0
        word[p+2] := data#EAT_0UR       ' Tile 56, Palette 0
        word[p+80] := data#EAT_0LL      ' Tile 57, Palette 0
        word[p+82] := data#EAT_0LR      ' Tile 58, Palette 0
    elseif t == 3
        word[p] := data#EAT_1UL         ' Tile 59, Palette 0
        word[p+2] := data#EAT_1UR       ' Tile 60, Palette 0
        word[p+80] := data#EAT_1LL      ' Tile 61, Palette 0
        word[p+82] := data#EAT_1LR      ' Tile 62, Palette 0
    elseif t == 2
        word[p] := data#EAT_2UL         ' Tile 63, Palette 0
        word[p+2] := data#EAT_2UR       ' Tile 64, Palette 0
        word[p+80] := data#EAT_2LL      ' Tile 65, Palette 0
        word[p+82] := data#EAT_2LR      ' Tile 66, Palette 0
    elseif t == 1
        word[p] := data#FONT_SPACE      ' Tile 0, Palette 0
        word[p+2] := data#FONT_SPACE      ' Tile 0, Palette 0
        word[p+80] := data#FONT_SPACE      ' Tile 0, Palette 0
        word[p+82] := data#FONT_SPACE      ' Tile 0, Palette 0


PRI moveEnemys | i, pos, err, contact

    repeat i from 0 to enemys_max-1

        if enemy_move[i] == 0
        
            pos := enemy_x[i] + (enemy_y[i] * 19)

            if enemy_act[i] == 1 and enemy_stat[i] == 4 and enemy_stop[i] == 0

                if enemy_d[i] == 0                  ' Left

                    if enemy_x[i] > 0 and mapbuff[pos-1] == 0
                        if prng.random & $FF < 64
                            enemy_d[i] := changeDir(i)
                        else
                            err := testColl(i, 0, pos)
                            if err == 0
                                enemy_move[i] := 4
                                enemy_xa[i] := (enemy_x[i] * 16) + 8
                                enemy_ya[i] := (enemy_y[i] * 16) + 16
                                enemy_x[i]--

                            else
                                enemy_d[i] := 1

                    elseif enemy_x[i] > 0 and mapbuff[pos-1] == 1
                        if prng.random & $1 == 1
                            enemy_d[i] := changeDir(i)
                        else
                            enemy_xa[i] := (enemy_x[i] * 16) + 8
                            enemy_ya[i] := (enemy_y[i] * 16) + 16
                            printEnemy(i)
                            mapbuff[pos-1] := 0
                            enemy_eat[i] := 4
                            enemy_eatpos[i] := pos-1
                            enemy_eatptr[i] := calcPtr(pos-1)
                            enemy_eatwait[i] := 16
                            enemy_stop[i] := 1

                    elseif enemy_x[i] > 0 and mapbuff[pos-1] == 2
                        enemy_d[i] := changeDir(i)

                    elseif enemy_x[i] == 0
                        enemy_d[i] := changeDir(i)

                elseif enemy_d[i] == 1              ' Right

                    if enemy_x[i] < 18 and mapbuff[pos+1] == 0
                        if prng.random & $FF < 64
                            enemy_d[i] := changeDir(i)
                        else
                            err := testColl(i, 1, pos)
                            if err == 0
                                enemy_move[i] := 4
                                enemy_xa[i] := (enemy_x[i] * 16) + 8
                                enemy_ya[i] := (enemy_y[i] * 16) + 16
                                enemy_x[i]++

                            else
                                enemy_d[i] := 0

                    elseif enemy_x[i] < 18 and mapbuff[pos+1] == 1
                        if prng.random & $1 == 1
                            enemy_d[i] := changeDir(i)
                        else
                            enemy_xa[i] := (enemy_x[i] * 16) + 8
                            enemy_ya[i] := (enemy_y[i] * 16) + 16
                            printEnemy(i)
                            mapbuff[pos+1] := 0
                            enemy_eat[i] := 4
                            enemy_eatpos[i] := pos+1
                            enemy_eatptr[i] := calcPtr(pos+1)
                            enemy_eatwait[i] := 16
                            enemy_stop[i] := 1

                    elseif enemy_x[i] < 18 and mapbuff[pos+1] == 2
                        enemy_d[i] := changeDir(i)

                    elseif enemy_x[i] == 18
                         enemy_d[i] := changeDir(i)

                elseif enemy_d[i] == 2              ' Up

                    if enemy_y[i] > 0 and mapbuff[pos-19] == 0
                        if prng.random & $FF < 64
                            enemy_d[i] := changeDir(i)
                        else
                            err := testColl(i, 2, pos)
                            if err == 0
                                enemy_move[i] := 4
                                enemy_xa[i] := (enemy_x[i] * 16) + 8
                                enemy_ya[i] := (enemy_y[i] * 16) + 16
                                enemy_y[i]--

                            else
                                enemy_d[i] := 3

                    elseif enemy_y[i] > 0 and mapbuff[pos-19] == 1
                        if prng.random & $1 == 1
                            enemy_d[i] := changeDir(i)
                        else
                            enemy_xa[i] := (enemy_x[i] * 16) + 8
                            enemy_ya[i] := (enemy_y[i] * 16) + 16
                            printEnemy(i)
                            mapbuff[pos-19] := 0
                            enemy_eat[i] := 4
                            enemy_eatpos[i] := pos-19
                            enemy_eatptr[i] := calcPtr(pos-19)
                            enemy_eatwait[i] := 16
                            enemy_stop[i] := 1

                    elseif enemy_y[i] > 0 and mapbuff[pos-19] == 2
                        enemy_d[i] := changeDir(i)

                    elseif enemy_y[i] == 0
                        enemy_d[i] := changeDir(i)

                elseif enemy_d[i] == 3              ' Down

                    if enemy_y[i] < 12 and mapbuff[pos+19] == 0
                        if prng.random & $FF < 64
                            enemy_d[i] := changeDir(i)
                        else
                            err := testColl(i, 3, pos)
                            if err == 0
                                enemy_move[i] := 4
                                enemy_xa[i] := (enemy_x[i] * 16) + 8
                                enemy_ya[i] := (enemy_y[i] * 16) + 16
                                enemy_y[i]++

                            else
                                enemy_d[i] := 2

                    elseif enemy_y[i] < 12 and mapbuff[pos+19] == 1
                        if prng.random & $1 == 1
                            enemy_d[i] := changeDir(i)
                        else
                            enemy_xa[i] := (enemy_x[i] * 16) + 8
                            enemy_ya[i] := (enemy_y[i] * 16) + 16
                            printEnemy(i)
                            mapbuff[pos+19] := 0
                            enemy_eat[i] := 4
                            enemy_eatpos[i] := pos+19
                            enemy_eatptr[i] := calcPtr(pos+19)
                            enemy_eatwait[i] := 16
                            enemy_stop[i] := 1

                    elseif enemy_y[i] < 12 and mapbuff[pos+19] == 2
                        enemy_d[i] := changeDir(i)

                    elseif enemy_y[i] == 12
                        enemy_d[i] := changeDir(i)

        else

            if enemy_d[i] == 0                  ' Left
                enemy_xa[i] -= 4
            
            elseif enemy_d[i] == 1              ' Right
                enemy_xa[i] += 4

            elseif enemy_d[i] == 2              ' Up
                enemy_ya[i] -= 4

            elseif enemy_d[i] == 3              ' Down
                enemy_ya[i] += 4

            if enemy_act[i] == 1
                printEnemy(i)
            enemy_move[i]--

            if exc_act == 1

                if enemy_d[i] == 0
                    testx[0] := enemy_xa[i] 
                    testx[1] := enemy_xa[i] 
                    testy[0] := enemy_ya[i]
                    testy[1] := enemy_ya[i] + 15

                elseif enemy_d[i] == 1
                    testx[0] := enemy_xa[i] + 15 
                    testx[1] := enemy_xa[i] + 15
                    testy[0] := enemy_ya[i]
                    testy[1] := enemy_ya[i] + 15

                elseif enemy_d[i] == 2
                    testx[0] := enemy_xa[i] 
                    testx[1] := enemy_xa[i] + 15 
                    testy[0] := enemy_ya[i]
                    testy[1] := enemy_ya[i]

                elseif enemy_d[i] == 3
                    testx[0] := enemy_xa[i] 
                    testx[1] := enemy_xa[i] + 15 
                    testy[0] := enemy_ya[i] + 15
                    testy[1] := enemy_ya[i] + 15

                contact := testEXC
                if contact == 1
                    exc_act := 0


PRI testEXC | k, contact

    contact := 0
    repeat k from 0 to 1
        if testx[k] => exc_xa and testx[k] =< exc_xa + 15 and testy[k] => exc_ya and testy[k] =< exc_ya + 15                                                                                 
            contact := 1
    
    return contact


PRI testColl(i, d, mypos) | j, k, x, y, pos, contact

    contact := 0
    repeat j from 0 to enemys_max-1
        if j <> i
            if enemy_act[j] == 1 or enemy_eat[j] > 0

                if d == 0                       ' Left

                    pos := enemy_x[j] + (enemy_y[j] * 19)
                    if mypos-1 == pos or (enemy_eat[j] > 0 and enemy_eatpos[j] == mypos-1) 
                        contact := 1

                elseif d == 1                   ' Right

                    pos := enemy_x[j] + (enemy_y[j] * 19)
                    if mypos+1 == pos or (enemy_eat[j] > 0 and enemy_eatpos[j] == mypos+1)
                        contact := 1

                elseif d == 2                   ' Up

                    pos := enemy_x[j] + (enemy_y[j] * 19)
                    if mypos-19 == pos or (enemy_eat[j] > 0 and enemy_eatpos[j] == mypos-19)
                        contact := 1

                elseif d == 3                   ' Down

                    pos := enemy_x[j] + (enemy_y[j] * 19)
                    if mypos+19 == pos or (enemy_eat[j] > 0 and enemy_eatpos[j] == mypos+19)
                        contact := 1
    
    return contact


PRI calcPtr(pos) | x, y, cptr

    x := pos // 19 
    y := pos / 19 

    cptr := get_tilemap_address(0, 0)
    cptr += ((x * 4) + 2) + ((y * 160) + 160)

    return cptr


PRI changeDir(i) | seed

    seed := (prng.random & $FF)         ' Random 0 - 255

    if enemy_d[i] == 0                  ' Left
        if seed < 192
            return 0
        elseif seed < 208
            return 1
        elseif seed < 224
            return 2
        else
            return 3

    elseif enemy_d[i] == 1              ' Right
        if seed < 192
            return 1
        elseif seed < 208
            return 0
        elseif seed < 224
            return 2
        else
            return 3

    elseif enemy_d[i] == 2              ' Up
        if seed < 192
            return 2
        elseif seed < 208
            return 0
        elseif seed < 224
            return 1
        else
            return 3

    elseif enemy_d[i] == 3              ' Down
        if seed < 192
            return 3
        elseif seed < 208
            return 0
        elseif seed < 224
            return 1
        else
            return 2


PRI printEnemy(i)

    if enemy_d[i] == 0                              ' Left
        sprite_show(i, data#ENEMY_L, enemy_color[i], enemy_xa[i], enemy_ya[i])

    elseif enemy_d[i] == 1                          ' Right
        sprite_show(i, data#ENEMY_R, enemy_color[i], enemy_xa[i], enemy_ya[i])

    elseif enemy_d[i] == 2 or enemy_d[i] == 3       ' Up / Down
        sprite_show(i, data#ENEMY_N, enemy_color[i], enemy_xa[i], enemy_ya[i])


PRI clrEnemys

    ptr := get_tilemap_address(10, 0)
    repeat 20
        print($0, $20) 


DAT
''********************************************************************
''* Functions for Shot                                               *
''********************************************************************
''
PRI initShot | pos

    if shot_act == 0
    
        pos := exc_x + (exc_y * 19)

        if exc_d == 0                  ' Left
            if exc_x > 1 and mapbuff[pos-1] <> 0 and mapbuff[pos-2] == 0
                if kb.keystate($20) or (pad_state & NES0_A)
                    shot_type := mapbuff[pos-1]
                    mapbuff[pos-1] := 0
                    shot_x := exc_x-1
                    shot_y := exc_y
                    shot_xa := (shot_x * 16) + 8
                    shot_ya := (shot_y * 16) + 16
                    shot_d := 0
                    clrBlock(calcPtr(pos-1))
                    printShot
                    shot_move := 4
                    shot_act := 1
                    play.tone(@tone1)
                    
        elseif exc_d == 1              ' Right
            if exc_x < 17 and mapbuff[pos+1] <> 0 and mapbuff[pos+2] == 0
                if kb.keystate($20) or (pad_state & NES0_A)  
                    shot_type := mapbuff[pos+1]
                    mapbuff[pos+1] := 0
                    shot_x := exc_x+1
                    shot_y := exc_y
                    shot_xa := (shot_x * 16) + 8
                    shot_ya := (shot_y * 16) + 16
                    shot_d := 1
                    clrBlock(calcPtr(pos+1))
                    printShot
                    shot_move := 4
                    shot_act := 1
                    play.tone(@tone1)

        elseif exc_d == 2              ' Up
            if exc_y > 1 and mapbuff[pos-19] <> 0 and mapbuff[pos-38] == 0
                if kb.keystate($20) or (pad_state & NES0_A)  
                    shot_type := mapbuff[pos-19]
                    mapbuff[pos-19] := 0
                    shot_x := exc_x
                    shot_y := exc_y-1
                    shot_xa := (shot_x * 16) + 8
                    shot_ya := (shot_y * 16) + 16
                    shot_d := 2
                    clrBlock(calcPtr(pos-19))
                    printShot
                    shot_move := 4
                    shot_act := 1
                    play.tone(@tone1)

        elseif exc_d == 3              ' Down
            if exc_y < 11 and mapbuff[pos+19] <> 0 and mapbuff[pos+38] == 0
                if kb.keystate($20) or (pad_state & NES0_A)  
                    shot_type := mapbuff[pos+19]
                    mapbuff[pos+19] := 0
                    shot_x := exc_x
                    shot_y := exc_y+1
                    shot_xa := (shot_x * 16) + 8
                    shot_ya := (shot_y * 16) + 16
                    shot_d := 3
                    clrBlock(calcPtr(pos+19))
                    printShot
                    shot_move := 4
                    shot_act := 1
                    play.tone(@tone1)


PRI moveShot | i, pos, contact

    if shot_act == 1

        if shot_move == 0

            pos := shot_x + (shot_y * 19)

            if shot_d == 0
                if shot_x > 0 and mapbuff[pos-1] == 0
                    shot_move := 4
                    return

            elseif shot_d == 1
                if shot_x < 18 and mapbuff[pos+1] == 0
                    shot_move := 4
                    return

            elseif shot_d == 2
                if shot_y > 0 and mapbuff[pos-19] == 0
                    shot_move := 4
                    return

            elseif shot_d == 3
                if shot_y < 12 and mapbuff[pos+19] == 0
                    shot_move := 4
                    return

            sprite_hide(6)                                ' Sprite 6 = Shot
            setBlock(calcPtr(pos), shot_type)
            mapbuff[pos] := shot_type
            shot_act := 0

        else

            if shot_d == 0                  ' Left
                shot_xa -= 4
            
            elseif shot_d == 1              ' Right
                shot_xa += 4

            elseif shot_d == 2              ' Up
                shot_ya -= 4

            elseif shot_d == 3              ' Down
                shot_ya += 4

            printShot
            shot_move--

            repeat i from 0 to enemys_max-1

                if shot_d == 0
                    testx[0] := shot_xa 
                    testx[1] := shot_xa 
                    testy[0] := shot_ya
                    testy[1] := shot_ya + 15

                elseif shot_d == 1
                    testx[0] := shot_xa + 15 
                    testx[1] := shot_xa + 15
                    testy[0] := shot_ya
                    testy[1] := shot_ya + 15

                elseif shot_d == 2
                    testx[0] := shot_xa 
                    testx[1] := shot_xa + 15 
                    testy[0] := shot_ya
                    testy[1] := shot_ya

                elseif shot_d == 3
                    testx[0] := shot_xa 
                    testx[1] := shot_xa + 15 
                    testy[0] := shot_ya + 15
                    testy[1] := shot_ya + 15

                contact := testEnemy(i)

                if contact == 1
                    sprite_hide(i)
                    enemy_wait[i] := 32
                    enemy_act[i] := 0
                    if shot_type == 1
                        enemy_life[i]--
                        incrScore(50)
                    elseif shot_type == 2
                        enemy_life[i] := 0
                        incrScore(50)
                        bonus += 1000
                    updateEnemys
                    play.tone(@tone2)
                    quit

            if shot_move == 0

                if shot_d == 0
                    shot_x--
                elseif shot_d == 1
                    shot_x++
                elseif shot_d == 2
                    shot_y--
                elseif shot_d == 3
                    shot_y++


PRI testEnemy(i) | k, contact

    contact := 0
    if enemy_act[i] == 1
        repeat k from 0 to 1
            if testx[k] => enemy_xa[i] and testx[k] =< enemy_xa[i] + 15 and testy[k] => enemy_ya[i] and testy[k] =< enemy_ya[i] + 15                                                                                 
                contact := 1
    
    return contact
            

PRI printShot

    if shot_type == 1
        sprite_show(6, data#BLOCK, 0, shot_xa, shot_ya)          ' Sprite 6 = Shot
    elseif shot_type == 2
        sprite_show(6, data#ICE, 0, shot_xa, shot_ya)            ' Sprite 6 = Shot


DAT
''********************************************************************
''* Functions for Excavator                                          *
''********************************************************************
''
PRI deadEXC

    if exc_done == 0    

        if exc_d == 2
            exc_ya -= 2

        elseif exc_d == 3
            exc_ya += 2
            
        if exc_r == 0
            exc_xa -= 2
        else
            exc_xa += 2
        exc_c--
        if exc_c == 0
            exc_c := 10            
            exc_r ^= 1

        printEXC(1)

        if exc_d == 3
            freq += $10
            ay1.setFreq(1, freq)

        if exc_xa < 8
            exc_r := 1
            exc_c := 20            
        elseif exc_xa > 312
            exc_r := 0
            exc_c := 20            

        if exc_ya < 2
            exc_d := 3
            exc_c := 20            
            freq := $80
            ay1.setVolume(1, $F)
            ay1.setFreq(1, freq)
            
        elseif exc_ya > 232
            ay1.setVolume(1, $0)
            sprite_hide(5)                             ' Sprite 5 = EXC
            exc_done := 1    


PRI moveEXC | i, contact

    if shot_act == 1
        return

    if exc_move == 0

        if kb.keystate($C0) or (pad_state & NES0_LEFT)                
            exc_d := 0
            printEXC(0)
            if exc_x > 0 and testFree
                exc_x--
                exc_move := 4 

        elseif kb.keystate($C1) or (pad_state & NES0_RIGHT)
            exc_d := 1
            printEXC(0)
            if exc_x < 18 and testFree
                exc_x++
                exc_move := 4 

        elseif kb.keystate($C2) or (pad_state & NES0_UP)   
            exc_d := 2
            printEXC(0)
            if exc_y > 0 and testFree
                exc_y--
                exc_move := 4 

        elseif kb.keystate($C3) or (pad_state & NES0_DOWN) 
            exc_d := 3
            printEXC(0)
            if exc_y < 12 and testFree
                exc_y++
                exc_move := 4 

    else
        if exc_d == 0                  ' Left
            exc_xa -= 4
            
        elseif exc_d == 1              ' Right
            exc_xa += 4

        elseif exc_d == 2              ' Up
            exc_ya -= 4

        elseif exc_d == 3              ' Down
            exc_ya += 4

        printEXC(0)
        exc_move--

        repeat i from 0 to enemys_max-1

            if exc_d == 0
                testx[0] := exc_xa 
                testx[1] := exc_xa 
                testy[0] := exc_ya
                testy[1] := exc_ya + 15

            elseif exc_d == 1
                testx[0] := exc_xa + 15 
                testx[1] := exc_xa + 15
                testy[0] := exc_ya
                testy[1] := exc_ya + 15

            elseif exc_d == 2
                testx[0] := exc_xa 
                testx[1] := exc_xa + 15 
                testy[0] := exc_ya
                testy[1] := exc_ya

            elseif exc_d == 3
                testx[0] := exc_xa 
                testx[1] := exc_xa + 15 
                testy[0] := exc_ya + 15
                testy[1] := exc_ya + 15

            contact := testEnemy(i)

            if contact == 1
                exc_act := 0


PRI testFree | pos

    pos := exc_x + (exc_y * 19)

    if exc_d == 0                  ' Left
        if mapbuff[pos-1] <> 0
            return false
        else
            return true     

    elseif exc_d == 1              ' Right
        if mapbuff[pos+1] <> 0
            return false
        else
            return true     

    elseif exc_d == 2              ' Up
        if mapbuff[pos-19] <> 0
            return false
        else
            return true     

    elseif exc_d == 3              ' Down
        if mapbuff[pos+19] <> 0
            return false
        else
            return true     


PRI printEXC(c)

    sprite_show(5, data#EXC_L + exc_d, c, exc_xa, exc_ya)         ' Sprite 5 = EXC


DAT
''********************************************************************
''* Functions for Map's                                              *
''********************************************************************
''
PRI createMap | d, i, j, x, y, pos, bptr

    bytefill(@mapbuff, $00, 247)

    calc(0, 0, 18, 12, 0)
    calc(1, 1, 17, 11, 1)
    calc(2, 2, 16, 10, 0)
    calc(3, 3, 15, 9, 1)
    calc(4, 4, 14, 8, 0)
    calc(5, 5, 13, 7, 1)

    repeat i from 6 to 12
        mapbuff[i + (6 * 19)] := getSeed(0)

    d := 0
    repeat
        x := prng.random & $7 + prng.random & $7 + prng.random & $1 + prng.random & $1 + 1 
        y := prng.random & $7 + prng.random & $3 + 1 
        pos := x + (y * 19)
        if mapbuff[pos] == 1
            mapbuff[pos] := 2
            d++
        if d == crystals
            quit

    bptr := get_tilemap_address(1, 2)
    d := 0
    blocks := 0
    repeat i from 0 to 12
        repeat j from 0 to 18
            if mapbuff[d] == 0
                clrBlock(bptr)
                bptr += 4
                d++
            elseif mapbuff[d] == 1
                setBlock(bptr, 1)
                bptr += 4
                d++
                blocks++
            elseif mapbuff[d] == 2
                setBlock(bptr, 2)
                bptr += 4
                d++

        flip
        waitcnt((clkfreq / 1000) * 100 + cnt)  ' wait 100 ms
        flip

        bptr += 84

    allOff


PRI clrBlock(p) | pos

    word[p] := data#FONT_SPACE          ' Tile 0, Palette 0
    word[p+2] := data#FONT_SPACE        ' Tile 0, Palette 0
    word[p+80] := data#FONT_SPACE       ' Tile 0, Palette 0
    word[p+82] := data#FONT_SPACE       ' Tile 0, Palette 0


PRI setBlock(p, t)

    if t == 1
        word[p] := data#BLOCKUL         ' Tile 2, Palette 0
        word[p+2] := data#BLOCKUR       ' Tile 3, Palette 0
        word[p+80] := data#BLOCKLL      ' Tile 4, Palette 0
        word[p+82] := data#BLOCKLR      ' Tile 5, Palette 0
    elseif t == 2
        word[p] := data#ICEUL           ' Tile 6, Palette 0
        word[p+2] := data#ICEUR         ' Tile 7, Palette 0
        word[p+80] := data#ICELL        ' Tile 8, Palette 0
        word[p+82] := data#ICELR        ' Tile 9, Palette 0


PRI calc(x1, y1, x2, y2, s) | i, x, y

    repeat x from x1 to x2
        i := x + (y1 * 19)
        mapbuff[i] := getSeed(s)

    repeat y from y1+1 to y2-1
        i := (y * 19) + x2
        mapbuff[i] := getSeed(s)

    repeat x from x1 to x2
        i := x + (y2 * 19)
        mapbuff[i] := getSeed(s)

    repeat y from y1+1 to y2-1
        i := (y * 19) + x1
        mapbuff[i] := getSeed(s)



PRI getSeed(s) | seed

    if s == 1
        seed := prng.random & $FF                   ' Random 0 - 255
        if seed > 96
            return 1
        else
            return 0    

    else
        seed := prng.random & $FF                   ' Random 0 - 255
        if seed < 16
            return 1
        else
            return 0    


DAT
''********************************************************************
''* Functions for Intro                                              *
''********************************************************************
''
PRI intro | i, x, y, offs, da, bit, col, counter, toggle, key, rdy, mel

    col := 0

    printFrame(0, 0, 40, 29, data#WALL, $2)

    strxy(0, 29, 7, @strVer)

    printFrame(12, 17, 16, 7, data#WALL, $2)


    strxy(1 , 27, $F, @strVer)
    strxy(14, 19, $F, @strIntro1)
    strxy(14, 21, $F, @strIntro2)

    ptr := get_tilemap_address(5, 2)
    i := data#HAT 
    repeat y from 0 to 6
        repeat x from 0 to 8
            word[ptr] := i++
            ptr += 2
        ptr += 62          '66

    repeat i from 0 to 6
        ptr := get_tilemap_address(18, 2+i)
        offs := @propice + (i*4)
        da := long[offs]
        repeat 17
            da <-= 1
            bit := da & 1
            if bit <> 0
                word[ptr] := (5 + $F) << 8 | data#BLOCK2     
                ptr += 2
            else
                ptr += 2

    repeat i from 1 to 6
        printxy(9, 9+i, coltab[i-1], $30+i)
        print(coltab[i-1], $2E)
        dec(30, 9+i, coltab[i-1], 0)

    strxy(12, 10, $F, @strNoScore)

    flip

    {ifnot (kb.present)                                    ' Keyboard present???
        clrTmp($20)
        tmp[12] :=  0

        counter := toggle := 0                             ' No
        repeat
            counter++
            if counter == 80
                counter := 0
                toggle ^= 1
                if toggle == 0
                    strxy(14, 26, $9, @strNoKeyb)
                    play.error(@tone4)
                else
                    strxy(14, 26, $9, @tmp)
            flip}

    strxy(11, 25, coltab[col], @strPress)
    flip
    
    kb.clearkeys

    mel := -1
    rdy := 0
    repeat

        repeat 100   

            key := kb.key
            nes_read_gamepad
            if key==$DC or pad_press & NES0_SELECT
              driver.toggleformat
            if key == $53 or key == $73 or (pad_state & NES0_START) ' "S" or "s"
                rdy := 1
                quit

            waitcnt((clkfreq / 1000) * 10 + cnt)  ' wait 10 ms

            'checkMusic ' can't do this here because we need SELECT to toggle NTSC/PAL

        col++
        if col == 9
            col := 0
        strxy(11, 25, coltab[col], @strPress)
        flip

        if mel == -1
            mel := 0 
            if music == 1 
                play.melody(@@melody[mel])
         
        if play.stat == 0
            mel++
            if mel == 3
                mel := 0
            if music == 1 
                play.melody(@@melody[mel])

        if rdy == 1
            quit

    play.melody_stop


DAT
''********************************************************************
''* Miscellaneous Functions                                          *
''********************************************************************
''
PRI flip

    repeat
    until link{0} == constant(render#vres - 16)          ' last line has been fetched


PRI testEat | i

    repeat i from 0 to enemys_max-1
        if enemy_eat[i] > 0
            return 1 

    return 0


PRI checkMusic

    if kb.keystate($D0) or (pad_press & NES0_SELECT) ' F1
        if music == 1
            play.melody_stop
            music := 0
            repeat
                ifnot kb.keystate($D0)
                    quit
        else
            play.melody_stop
            music := 1
            play.melody(@@melody[0])
            repeat
               ifnot kb.keystate($D0)
                   quit

PRI allOff | i

    repeat i from 0 to 4
        sprite_hide(i)
        enemy_act[i] := 0
        enemy_eat[i] := 0
        enemy_stat[i] := 0
        enemy_eat[i] := 0
        enemy_move[i] := 0
    sprite_hide(5)                         ' Sprite 5 = EXC
    sprite_hide(6)                         ' Sprite 6 = Shot
    flip


PRI endGame

    allOff

    printFrame(15, 12, 10, 6, data#WALL, $2)
    strxy(18, 14, $F, @strGood)
    strxy(17, 15, $F, @strResult)

    flip

    play.melody(@melody4)

    repeat
        if play.stat == 0
            quit



PRI printStatus

    ptr := get_tilemap_address(0, 0)

    repeat lifes
        print($8, $2A)
    print($0, $20)

'    dec2(6, 0, $F, blocks)


PRI initWall | i, j

    ptr := get_tilemap_address(0, 0)
    wordfill(ptr, $00, 40)
    printFrame(0, 1, 40, 28, data#WALL, $2)

    strxy(0, 29, 7, @strVer)


PRI printFrame(x, y, w, h, t, c)

    ptr := get_tilemap_address(x, y)
    repeat w
        printTile(t, c)

    ptr += (40 - w) * 2
    repeat h-2
        printTile(t, c)
        repeat w-2
            printTile(data#FONT_SPACE, 0)
        printTile(t, c)
        ptr += (40 - w) * 2
    repeat w
        printTile(t, c)


PRI printTile(tile, color)

    word[ptr] := (5 + color) << 8 | tile 
    ptr += 2


PRI incrScore(s)

    score += s
    dec(39, 0, $F, score)


PRI clrTmp(char)

    bytefill(@tmp, char, 16)


PRI print(color, char)

    case char
        $30..$39: char := char - $30 + data#FONT_0
        $41..$5A: char := char - $41 + data#FONT_A
        $61..$7A: char := char - $61 + data#FONT_A
        $3A: char := data#FONT_COLON 
        $2F: char := data#FONT_SLASH 
        $3C: char := data#FONT_LEFT 
        $3E: char := data#FONT_RIGHT 
        $28: char := data#FONT_UP 
        $29: char := data#FONT_DN 
        $2E: char := data#FONT_DOT
        $21: char := data#FONT_EXC
        $22: char := data#FONT_QUOT
        $2A: char := data#BALL 
        $7F: char := data#CURSOR 
        other: char := data#FONT_SPACE

    word[ptr] := (5 + color) << 8 | char
    ptr += 2


PRI printxy(x, y, color, char)

    ptr := get_tilemap_address(x, y)
    print(color, char)
    

PRI str(color, stringptr)

    repeat strsize(stringptr)
        print(color, byte[stringptr++])


PRI strxy(x, y, color, stringptr)

    ptr := get_tilemap_address(x, y)
    str(color, stringptr)


PRI bin(x, y, val, digits) | v, c, optr
  
    optr := get_tilemap_address(x, y)

    val <<= 32 - digits
    repeat digits
        v := (val <-= 1) & 1 + "0"
        c := v - $30 + data#FONT_0
        word[optr] := 256 + c
        optr += 2


PRI hex(x, y, val, digits) | v, c, optr

    optr := get_tilemap_address(x, y)

    val <<= (8 - digits) << 2
    repeat digits


        v := lookupz((val <-= 4) & $f : "0".."9", "A".."F") 

        if v > $39
            c := v - $41 + data#FONT_A
            word[optr] := c
            optr += 2
        else
            c := v - $30 + data#FONT_0
            word[optr] := c
            optr += 2


PRI dec(x, y, color, val) | i, p, c, optr 

    clrTmp($00)

    optr := get_tilemap_address(x, y)

    ptr := 0
    prn(val)

    p := ptr-1
    repeat ptr
        c := tmp[p] - $30 + data#FONT_0
        word[optr] := (5 + color) << 8 | c
        optr -= 2
        p--
    if val < 10
        word[optr] := data#FONT_SPACE


PRI dec2(x, y, color, val) | i, p, c, optr 

    clrTmp($00)

    optr := get_tilemap_address(x, y)

    ptr := 0
    prn(val)

    p := 0
    repeat ptr
        c := tmp[p] - $30 + data#FONT_0
        word[optr] := (5 + color) << 8 | c
        optr += 2
        p++
    word[optr] := data#FONT_SPACE


PRI prn(val) | dig

    dig := 48 + (val // 10)
    val := val/10
    if val > 0
        prn(val)
    tmp[ptr++] := dig


PRI get_tilemap_address(sx, sy)

    return @map1 + ((sy * 40 + sx) * 2)
    

PRI sprite_show(si, st, sp, sx, sy) | t

    t := (st << SPRITE_TILE_SHIFT) | (sp << SPRITE_PALETTE_SHIFT) | (sy << SPRITE_Y_SHIFT) | sx
    long[@sprite_state + (si * 4)] := t


PRI sprite_hide(si)

    long[@sprite_state + (si * 4)] := $FFFFFFFF


CON

    SPRITE_TILE_SHIFT    = 24
    SPRITE_PALETTE_SHIFT = 18
    SPRITE_Y_SHIFT       = 9


DAT

map1           word 0 [render#MAP_SIZE_WORD]   ' 40 x 30 Words               2400 Bytes

sprite_state   long $FFFFFFFF [7]              ' 7 Sprites                     28 Bytes


DAT
'                    I       C       E     
propice        long %11111_0_01110_0_11111_000000000000000
               long %00100_0_10001_0_10000_000000000000000
               long %00100_0_10000_0_10000_000000000000000
               long %00100_0_10000_0_11110_000000000000000
               long %00100_0_10000_0_10000_000000000000000
               long %00100_0_10001_0_10000_000000000000000
               long %11111_0_01110_0_11111_000000000000000


strVer         byte "PROPICE V1.1 NE FOR V.OS     WS 1/2020", 0

strNoKeyb      byte "NO KEYBOARD!", 0

strBonus       byte "BONUS:", 0

strPress       byte "PRESS ", $22, "S", $22, " TO START", 0

strIntro1      byte " <>() : MOVE", 0
strIntro2      byte "SPACE : PUSH", 0

strGood        byte "GOOD", 0
strResult      byte "RESULT", 0

strNoScore     byte "NO SCORES", 0

coltab         byte $F, $A, $B, $C, $D, $9, $8, $E, $7


DAT


tone1          word $0300
               word $0280
               word $090F
               word $8400
               word $0300
               word $02A0
               word $090F
               word $8400
               word $0300
               word $02C0
               word $090F
               word $8300
               word $0300
               word $02D0
               word $090F
               word $8300
               word $0300
               word $02E0
               word $090F
               word $8300
               word $0900
               word $FFFF   


DAT


tone2          word $0306
               word $02C0
               word $090F
               word $8600
               word $0306
               word $02D0
               word $090F
               word $8600
               word $0306
               word $02E0
               word $090F
               word $8600
               word $0306
               word $02F0
               word $090F
               word $8600
               word $0900
               word $FFFF   


DAT


tone3          word $0300
               word $02C0
               word $090F
               word $8500
               word $0900
               word $FFFF   


DAT


tone4          word $0738
               word $080F
               word $090F
               word $0A0F
               word $0104
               word $00FF
               word $0304
               word $02F0
               word $0504
               word $04E8
               word $C000
               word $0800
               word $0900
               word $0A00
               word $FFFF   


DAT


noise1         word $061E
               word $0A0F
               word $8600
               word $0A00
               word $8300
               word $0A0F
               word $8600
               word $0A00
               word $8300
               word $0A0F
               word $8600
               word $0A00
               word $FFFF


DAT


noise2         word $061E
               word $0A0F
               word $8B00
               word $0A0E
               word $8400
               word $0A0D
               word $8300
               word $0A0C
               word $8200
               word $0A0B
               word $8200
               word $0A0A
               word $8200
               word $0A09
               word $8200
               word $0A08
               word $8200
               word $0A07
               word $8200
               word $0A06
               word $8200
               word $0A05
               word $8200
               word $0A04
               word $8200
               word $0A03
               word $8200
               word $0A02
               word $8200
               word $0A01
               word $8200
               word $0A00
               word $FFFF


DAT


melody         word @melody1, @melody2, @melody3


DAT
               

melody1        word $071C
               word $0800
               word $0C12               
               word $0100
               word $00E2
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00FE
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00E2
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $001D
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $007C
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $001D
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $9CE0
               word $0100
               word $00E2
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00FE
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00E2
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $001D
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $007C
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $001D
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $9CE0
               word $0100
               word $00E2
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00C9
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00BE
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00C9
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00BE
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00E2
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00C9
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00E2
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00C9
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00FE
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00E2
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00FE
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $002E
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00FE
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00E2
               word $0810
               word $0D00
               word $B000
               word $FFFF


DAT


melody2        word $071C
               word $0800
               word $0C12               
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $00FC
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8E70
               word $0102
               word $003A
               word $0810
               word $0D00
               word $8E70
               word $0102
               word $00F9
               word $0810
               word $0D00
               word $8E70
               word $0102
               word $003A
               word $0810
               word $0D00
               word $8E70
               word $0103
               word $008A
               word $0810
               word $0D00
               word $9CE0
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $00FC
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8E70
               word $0102
               word $003A
               word $0810
               word $0D00
               word $8E70
               word $0102
               word $00F9
               word $0810
               word $0D00
               word $8E70
               word $0102
               word $003A
               word $0810
               word $0D00
               word $8E70
               word $0103
               word $008A
               word $0810
               word $0D00
               word $9CE0
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $0093
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $007C
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $0093
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $007C
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $0093
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $0093
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $00FC
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $00FC
               word $0810
               word $0D00
               word $8E70
               word $0102
               word $005C
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $00FC
               word $0810
               word $0D00
               word $8E70
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $B000
               word $FFFF


DAT


melody3        word $071C
               word $0800
               word $0C12               
               word $0100
               word $0071
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $007F
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $0071
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $008E
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00BE
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $008E
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00E2
               word $0810
               word $0D00
               word $9CE0
               word $0100
               word $0071
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $007F
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $0071
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $008E
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00BE
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $008E
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $00E2
               word $0810
               word $0D00
               word $9CE0
               word $0100
               word $0071
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $0064
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $005F
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $0064
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $005F
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $0071
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $0064
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $0071
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $0064
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $007F
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $0071
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $007F
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $0097
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $007F
               word $0810
               word $0D00
               word $8E70
               word $0100
               word $0071
               word $0810
               word $0D00
               word $B000
               word $FFFF


DAT


melody4        word $071C
               word $0800
               word $0C12               
               word $0C1E
               word $0D00
               word $0102
               word $00F9
               word $0810
               word $0D00
               word $99F3
               word $0102
               word $003A
               word $0810
               word $0D00
               word $99F3
               word $0102
               word $003A
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00FC
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $99F3
               word $0102
               word $003A
               word $0810
               word $0D00
               word $99F2
               word $0101
               word $007C
               word $0810
               word $0D00
               word $B37D
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $A6B8
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00AB
               word $0810
               word $0D00
               word $99F3
               word $0101
               word $007C
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00AB
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00AB
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $007C
               word $0810
               word $0D00
               word $99F3
               word $0101
               word $00FC
               word $0810
               word $0D00
               word $8D2E
               word $0102
               word $003A
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00FC
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00FC
               word $0810
               word $0D00
               word $99F3
               word $0102
               word $00F9
               word $0810
               word $0D00
               word $99F3
               word $0102
               word $003A
               word $0810
               word $0D00
               word $99F3
               word $0102
               word $003A
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00FC
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $99F3
               word $0102
               word $003A
               word $0810
               word $0D00
               word $99F2
               word $0101
               word $007C
               word $0810
               word $0D00
               word $B37D
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $A6B8
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00AB
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $007C
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00AB
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00FC
               word $0810
               word $0D00
               word $A6B8
               word $0102
               word $003A
               word $0810
               word $0D00
               word $8D2E
               word $0102
               word $003A
               word $0810
               word $0D00
               word $B000
               word $FFFF

CON
  NES0_RIGHT    = %00000000_00000001
  NES0_LEFT     = %00000000_00000010
  NES0_DOWN     = %00000000_00000100
  NES0_UP       = %00000000_00001000
  NES0_START    = %00000000_00010000
  NES0_SELECT   = %00000000_00100000
  NES0_B        = %00000000_01000000
  NES0_A        = %00000000_10000000

PUB NES_Read_Gamepad                   |  i,nes_bits
' //////////////////////////////////////////////////////////////////
' NES Game Pad Read
' //////////////////////////////////////////////////////////////////       
' reads both gamepads in parallel encodes 8-bits for each in format
' right game pad #1 [15..8] : left game pad #0 [7..0]
'
' set I/O ports to proper direction
' P3 = JOY_CLK      (4)
' P4 = JOY_SH/LDn   (5) 
' P5 = JOY_DATAOUT0 (6)
' P6 = JOY_DATAOUT1 (7)
' NES Bit Encoding
'
' RIGHT  = %00000001
' LEFT   = %00000010
' DOWN   = %00000100
' UP     = %00001000
' START  = %00010000
' SELECT = %00100000
' B      = %01000000
' A      = %10000000

' step 1: set I/Os
DIRA [JOY_CLK] := 1 ' output
DIRA [JOY_LCH] := 1 ' output
DIRA [JOY_DATAOUT0] := 0 ' input
DIRA [JOY_DATAOUT1] := 0 ' input

' step 2: set clock and latch to 0
OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0
'Delay(1)

' step 3: set latch to 1
OUTA [JOY_LCH] := 1 ' JOY_SH/LDn = 1
'Delay(1)                            

' step 4: set latch to 0
OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0

' data is now ready to shift out, clear storage
nes_bits := 0

' step 5: read 8 bits, 1st bits are already latched and ready, simply save and clock remaining bits
repeat i from 0 to 7

 nes_bits := (nes_bits << 1)
 nes_bits := nes_bits | INA[JOY_DATAOUT0] | (INA[JOY_DATAOUT1] << 8)

 OUTA [JOY_CLK] := 1 ' JOY_CLK = 1
 'Delay(1)             
 OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
 
 'Delay(1)             

' invert bits to make positive logic
nes_bits := (!nes_bits & $FFFF)

pad_prev := pad_state
pad_state := nes_bits
pad_press := (pad_state^pad_prev)&pad_state
          
' //////////////////////////////////////////////////////////////////
' End NES Game Pad Read
' //////////////////////////////////////////////////////////////////
  

      