

for kb in 0..2
	for nes in 0..2
		next unless kb+nes >= 2
		for hyper in 0..1
			filename = "./sdbins/HM#{"X" if hyper>0}_K#{kb}N#{nes}"
			`bstc #{"-D PINIT1" if nes>=1} #{"-D PINIT2" if nes>=2} #{"-D PINIT3" if kb>=1} #{"-D PINIT4" if kb>=2} #{"-D PINITHYPER" if hyper>0} -Ox -e -o #{filename}   ./source/CP_HYDRAMAN_005c.spin`
			`mv #{filename}.eeprom #{filename}.BIN`
		end
	end
end
