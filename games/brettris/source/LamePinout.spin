' --------------------------------------------------------------------------------
' LamePinout.spin
' Version: 0.6.2-rc2
' Copyright (c) 2015-2016 LameStation LLC
' See end of file for terms of use.
' --------------------------------------------------------------------------------
' The LameStation pinout is defined in a separate file to
' make cross-compatibility between board versions easier.
'
' Applicable boards:
' - Ventilator Computer
'
CON
' LCD pins
' -------------------------
    {DI          = 0
    E           = 1

    D0          = 2
    D1          = 3
    D2          = 4
    D3          = 5
    D4          = 6
    D5          = 7
    D6          = 8
    D7          = 9

    CSA         = 10
    CSB         = 11}

' Joystick pins
' -------------------------
    {JOY_UP      = 12
    JOY_DOWN    = 13
    JOY_LEFT    = 14
    JOY_RIGHT   = 15}
  JOY_CLK = 16
  JOY_LCH = 17
  JOY_DATAOUT0 = 19
  JOY_DATAOUT1 = 18

' Expansion Port
' -------------------------
    {EX0         = 16
    EX1         = 17
    EX2         = 18
    EX3         = 19
    EX4         = 20
    EX5         = 21
    EX6         = 22
    EX7         = 23}

' Software-controlled LED
' -------------------------
    'LED         = 24

' Buttons
' -------------------------
    'BUTTON_A    = 25
    'BUTTON_B    = 26

' TV (TV.spin - style)
' -------------------------
    TV          = %001_0101

' Audio output
' -------------------------
    AUDIO       = 10
    AUDIO2      = 11

' EEPROM
' -------------------------
    SDA         = 28
    SCL         = 29

' Serial port
' -------------------------
    TX          = 30
    RX          = 31

PUB null


DAT
' --------------------------------------------------------------------------------------------------------
' TERMS OF USE: MIT License
'
' Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
' associated documentation files (the "Software"), to deal in the Software without restriction, including
' without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
' copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
' following conditions:
'
' The above copyright notice and this permission notice shall be included in all copies or substantial
' portions of the Software.
'
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
' IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
' WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
' SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
' --------------------------------------------------------------------------------------------------------
DAT