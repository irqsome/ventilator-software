mkdir -p sdbins
cd source
cat BMP_Title.bin BMP_Options.bin Gothic.bin FF1_Battle.bin Eagle.bin "A Gentle Wind.bin" "North Wall.bin" > ../sdbins/SPACEWAR.RAM
rm ../sdbins/SPACEWAR.BIN
bstc -Ocgru -e -o ../sdbins/SPACEWAR EPM_SpaceWar_025.spin
mv ../sdbins/SPACEWAR.eeprom ../sdbins/SPACEWAR.BIN
cd ..