'///////////////////////////////////////////////////////////////////////
' 
'' Hydra Dense Music Format (HDMF) Driver 
'' AUTHOR: Eric Moyer
'' LAST MODIFIED: 05.26.07
'' VERSION 1.0
''
'' For the latest version search the parallax forums at:
''    http://forums.parallax.com 
''                                                 
''
'' //////////////////////////////////////////////////////////////////////
''
''  Revision History
''
''  Rev  Date      Description
''  ---  --------  ---------------------------------------------------
''  001  05-16-07  Initial Creation
''  002  05-17-07  Add support for song assets in high EEPROM (>32K)
''  003  05-17-07  Merge EEPROM driver into this file.
''                 Load notes one at a time from EEPROM on the fly.
''  004  05-19-07  Add instrumnet list support.
''  010  05-27-07  Release
''  011  06-20-07  Add PlaySoundFM pass-thru call
''
'' //////////////////////////////////////////////////////////////////////  
''
''  Open issues:
''     Handle starting the eeprom/snd drivers and cleaning them up
''        more gracefully.
''
''  Documentation:
''  ==============
''  Please refer to the 'HDMF User's Manual' for extensive documentation on this
''  driver, the creation of HDMF assets (i.e. music data), and operation of the
''  'HDMF Converter' Windows application which can be used to covert MIDI data into
''  HDMF assets.      
''
''  NOTE: If you are not playing back HDMF assets from EEPROM you can save
''        a cog by commenting out the EEPROM driver load & calls.
''
'' //////////////////////////////////////////////////////////////////////  

'///////////////////////////////////////////////////////////////////////
' CONSTANTS SECTION ////////////////////////////////////////////////////
'///////////////////////////////////////////////////////////////////////

CON

  DEBUG__ENABLE_SINGLE_NOTE_DBG = FALSE

  '--------------------
  ' Lock Semaphores
  '--------------------
  SOUND_DRIVER_SEMAPHORE    = 0
  EEPROM_DRIVER_SEMAPHORE   = 1
  

  HDMF_TICK                 = 80_000_000 / 100         ' HDMF uses a 10 mesc tick (100 ticks per second).
                                                       ' HDMF_TICK is expressed in system clock ticks
                                                       '     so at 80MHz is 80,000,000 / 100
                                                       
  DURATION_TICK             = snd#SAMPLE_RATE / 100    ' HDMF duration is also 100 ticks per second
                                                       ' DURATION_TICK is expressed in sound driver sample
                                                       '     rate ticks so is SAMPLE_RATE / 100
  
  'Debug states
  DEBUG_STATE_PLAY_NOTE     = 0
  DEBUG_STATE_NOTE_DONE     = 1

  EEPROM_ASSET              = $80000000
  SONG_BUFFER_SIZE          = 32
  SONG_HALF_BUFFER_SIZE     = (SONG_BUFFER_SIZE / 2)
  SONG_BUFFER_INDEX_MASK    = (SONG_BUFFER_SIZE - 1)

  MAX_NOTE_SIZE_BYTES       = 6
  MAX_INSTRUMENTS           = 8
  
  '----------------------------
  'HDMF playback flags
  '---------------------------- 

  'Internal flags
  HDMF__IDLE                = %00000010_00000000_00000000_00000000
  HDMF__NO_SONG_PLAYING     = %00000100_00000000_00000000_00000000
  HDMF__EEPROM_PLAYBACK     = %00001000_00000000_00000000_00000000
  HDMF__HALT_PLAYBACK       = %00010000_00000000_00000000_00000000
  HDMF__LAST_NOTE_5_BYTES   = %00100000_00000000_00000000_00000000 

  'External  flags
  HDMF__ENABLE_PLAYBACK     = %00000001_00000000_00000000_00000000
  HDMF__USER_FLAGS_MASK     = %00000000_00000000_00000000_11111111
  HDMF__LOOP                = %00000000_00000000_00000000_00000001
  

  '----------------------------
  'Sound driver pass-thru constants
  '----------------------------
  
  'These constants are defined within the sound driver and are passed-through here so that
  'objects making use of the .PlaySoundFM pass through call have access to them
  SHAPE_NOISE    = snd#SHAPE_NOISE
  SHAPE_SQUARE   = snd#SHAPE_SQUARE
  SHAPE_SINE     = snd#SHAPE_SINE    
  SAMPLE_RATE    = snd#SAMPLE_RATE
  SHAPE_SAWTOOTH = snd#SHAPE_SAWTOOTH
    
'///////////////////////////////////////////////////////////////////////
' VARIABLES SECTION ////////////////////////////////////////////////////
'///////////////////////////////////////////////////////////////////////

VAR

long  debug_data_ptr
long  cogon
long  cog
long  my_stack[30]

byte  note_data[MAX_NOTE_SIZE_BYTES]
byte  song_buffer[SONG_BUFFER_SIZE]
byte  instrument_wave[MAX_INSTRUMENTS]
long  instrument_envelope[MAX_INSTRUMENTS]

long  song_ptr
long  song_buf_index
long  song_start_ptr
long  internal_flags
long  external_flags
  
'///////////////////////////////////////////////////////////////////////
' OBJECT DECLARATION SECTION ///////////////////////////////////////////
'///////////////////////////////////////////////////////////////////////
OBJ

  snd    : "NS_sound_drv_052_22khz_16bit_stereo.spin"   'Sound driver
  'eeprom : "NS_eeprom_drv_011.spin"            'EEPROM Driver
  ram    : "23lc1024.spin"
  
'///////////////////////////////////////////////////////////////////////
' EXPORT PUBLICS  //////////////////////////////////////////////////////
'///////////////////////////////////////////////////////////////////////

PUB start(arg_debug_data_ptr)
'' Starts the HDMF Driver.
''
''   The driver will occupy 3 cogs:
''      1 for the driver itself
''      1 for the eeprom driver 
''      1 for the sound driver
''
''   Comment out the EEPROM driver code to save 1 cog if you are using HDMF to play back
''   song assets stored in EEPROM.
''
''   arg_debug_data_ptr:   Set this to zero.
''                         (When non-zero it used to enable development debugging)
''
  stop
  
  debug_data_ptr := arg_debug_data_ptr
  external_flags := 0
  internal_flags := 0

  'Start sound driver
  snd.start(10)

  'Start eeprom driver
  'eeprom.start(28,29,0)
  
  ram.start(21,24,20,26)
  
  return cogon := (cog := cognew(HDMF_main, @my_stack)) > 0


'///////////////////////////////////////////////////////////////////////
PUB stop
'' Stops the driver. Frees all used cogs.
''
  if cogon~
    cogstop(cog)
    snd.stop
    'eeprom.stop
    
'/////////////////////////////////////////////////////////////////////// 
PUB play_song(song_address, user_flags) | temp_flags
'' Starts song playback.
''
''   song_address:    The starting address of the song data.
''                    The address can be either a RAM address or a EEPROM address.
''                    If passing a EEPROM addres, OR the addres with EEPROM_ASSET
''
''   user_flags:      Playbak flags.  Multiple flags can be OR'd together.
''                    Supported flags:
''                       HDMF__LOOP   Loop the song forever. ( Use stop_song() to stop it )
''                    

  'stop playing the current song
  external_flags &= !(HDMF__ENABLE_PLAYBACK)
   
  'wait for the player to become idle
  repeat while not (internal_flags & HDMF__IDLE)

  if song_address & EEPROM_ASSET
    song_address -= $8000
   
  'point to the new song
  song_start_ptr := song_address & !HDMF__EEPROM_PLAYBACK

  'set flag if address is in eeprom
  if song_address & EEPROM_ASSET
    internal_flags |= HDMF__EEPROM_PLAYBACK
  else
    internal_flags &= !HDMF__EEPROM_PLAYBACK 

  'start the new song (and set user any flag options)
  external_flags := (external_flags & !HDMF__USER_FLAGS_MASK) | (user_flags & HDMF__USER_FLAGS_MASK) | HDMF__ENABLE_PLAYBACK
   
    
'///////////////////////////////////////////////////////////////////////
PUB stop_song
'' Stops a song in progress.
''
  'stop playing the current song
  external_flags &= !(HDMF__ENABLE_PLAYBACK)   
   
  'wait for the player to become idle
  repeat while not (internal_flags & HDMF__IDLE)
   
    
'///////////////////////////////////////////////////////////////////////
PUB song_is_playing
'' Returns 'true' if a song is currently playing.
'' Returns 'false' otherwise.
''

  'check whether a song is currently playing and return the status
  if (internal_flags & HDMF__NO_SONG_PLAYING)
    return(false)
  else
    return(true)

'///////////////////////////////////////////////////////////////////////
' PRIVATE FUNCTIONS  ///////////////////////////////////////////////////
'///////////////////////////////////////////////////////////////////////
PRI HDMF_main | i, HDMF_freq, HDMF_duration, HDMF_channel, HDMF_volume, HDMF_time, HDMF_instrument, current_note_time, y, note_delta_time, starting_buf_index
''  Main HDMF playback engine
''
  
  repeat while (LOCKSET(SOUND_DRIVER_SEMAPHORE)==1)  
  snd.PlaySoundFM(0, snd#SHAPE_SINE, 600, CONSTANT( snd#SAMPLE_RATE * 2 ), 255, $3579_ADEF )  'Play for two seconds
  LOCKCLR(SOUND_DRIVER_SEMAPHORE) 

  internal_flags := HDMF__NO_SONG_PLAYING
  
  repeat

    'Hang here while not playing anything
    repeat while not (external_flags & HDMF__ENABLE_PLAYBACK) 
      internal_flags |=  HDMF__IDLE
      
    'Clear a bunch of internal flags
    internal_flags &= !(HDMF__IDLE | HDMF__HALT_PLAYBACK | HDMF__LAST_NOTE_5_BYTES | HDMF__NO_SONG_PLAYING)

    'point to start of song
    song_ptr := song_start_ptr

    'load instruments
    i := 0
    if internal_flags & HDMF__EEPROM_PLAYBACK
       'Data is in EEPROM
       
       'Pre-read the first byte (into y)
       repeat while (LOCKSET(EEPROM_DRIVER_SEMAPHORE)==1)
       'ram.preparecog 
       ram.Read(@y, song_ptr, 1)
       'repeat while not eeprom.IsDone

       repeat while (i =< MAX_INSTRUMENTS - 1) and (y <> $ff)
          'Get instrument
          instrument_wave[i] := y
          ++song_ptr
          'Get envelope
          ram.Read(@instrument_envelope + (i*4), song_ptr, 4)   
          song_ptr += 4
          'repeat while not eeprom.IsDone
          'Pre-read the next byte (into y)
          ram.Read(@y, song_ptr, 1)
          'repeat while not eeprom.IsDone
          'Inrement intrument index
          ++i
       LOCKCLR(EEPROM_DRIVER_SEMAPHORE)                                
        
    else
      'Data is in RAM
      repeat while (i =< MAX_INSTRUMENTS - 1) and (byte[song_ptr] <> $ff)
        'Get instrument
        instrument_wave[i] := byte[song_ptr]
        ++song_ptr
        'Get envelope
        instrument_envelope[i] := byte[song_ptr] | byte[song_ptr+1] << 8 | byte[song_ptr+2] << 16 | byte[song_ptr+3] << 24
        song_ptr += 4
        'Increment instrument index
        ++i

    'skip over 'end of instruments' indicator byte (0xff)
    ++song_ptr  
  
    'Play the song
    current_note_time := cnt  
    repeat while not(internal_flags & HDMF__HALT_PLAYBACK)

      if internal_flags & HDMF__EEPROM_PLAYBACK
        'Fetch song data from EEPROM
        repeat while (LOCKSET(EEPROM_DRIVER_SEMAPHORE)==1)  
        if internal_flags & HDMF__LAST_NOTE_5_BYTES
          'One byte of note data already in buffer.  Read 5 more.
           ram.Read(@note_data + 1, song_ptr, 5)
           song_ptr += 5
        else
          'Read 6 new bytes of note data
          ram.Read(@note_data, song_ptr, 6)
          song_ptr += 6  
        'repeat until eeprom.IsDone
        LOCKCLR(EEPROM_DRIVER_SEMAPHORE)        
      else
        'Fetch song data from RAM

        if internal_flags & HDMF__LAST_NOTE_5_BYTES
          'One byte of note data already in buffer.  Read 5 more.
           repeat i from 1 to 5
            note_data[i] := byte[song_ptr]
            song_ptr++
        else
          'Read 6 new bytes of note data
          repeat i from 0 to 5
            note_data[i] := byte[song_ptr]  
            song_ptr++

      note_delta_time := note_data[0] * HDMF_TICK  

      'Repeat song until end reached, or until the playback flag is cleared.
      '   Note: Care is taken to not stop on a note with a delta time of zero when
      '         the playback flag is cleared.  This ensures that if a song is stopped on
      '         a chord then all notes of that chord will play before the song stopps.
      if (note_data[0] <> $ff) and  ((external_flags & HDMF__ENABLE_PLAYBACK) or (note_delta_time == 0)) 
        
        HDMF_time := current_note_time + note_delta_time  
        HDMF_channel := (note_data[1] & $e0) >> 5
        HDMF_freq := (((note_data[1] & $1f) << 8) |  note_data[2] ) << 1
        HDMF_volume :=  note_data[3] << 3
        HDMF_instrument :=  (note_data[3] & $e0) >> 5

        if note_data[4] & $80
          HDMF_duration := ((note_data[4] & $7f) << 8 | note_data[5]) * DURATION_TICK
          internal_flags &= !HDMF__LAST_NOTE_5_BYTES  
        else
          HDMF_duration := note_data[4] * DURATION_TICK  
          'Note was only 5 bytes long, so save the 6th byte for the next note
          note_data[0] := note_data[5]  
          internal_flags |= HDMF__LAST_NOTE_5_BYTES  
       
        'wait for time to play next note
        if not DEBUG__ENABLE_SINGLE_NOTE_DBG
          if (current_note_time > $40000000) and (HDMF_time < $40000000)
            repeat while(cnt > $40000000) 'wait for counter wrap
          repeat while(cnt < HDMF_time)

        repeat while (LOCKSET(SOUND_DRIVER_SEMAPHORE)==1)  
        snd.PlaySoundFM(HDMF_channel, {snd#SHAPE_SINE} instrument_wave[HDMF_instrument], HDMF_freq, HDMF_duration, HDMF_volume, instrument_envelope[HDMF_instrument] )    
        LOCKCLR(SOUND_DRIVER_SEMAPHORE)
        
        current_note_time :=  HDMF_time

        if debug_data_ptr
          'Debug enabled.  Dump debug data.
          long[debug_data_ptr+1*4] := note_data[0] << 24 | note_data[1] << 16 | note_data[2] << 8 | note_data[3]
          long[debug_data_ptr+2*4] := note_data[4] << 24 | note_data[5] << 16
          long[debug_data_ptr+3*4] := HDMF_time
          long[debug_data_ptr+4*4] := HDMF_channel
          long[debug_data_ptr+5*4] := HDMF_freq
          long[debug_data_ptr+6*4] := HDMF_volume
          long[debug_data_ptr+7*4] := HDMF_duration

          'Indicate that note has been played
          long[debug_data_ptr] := DEBUG_STATE_NOTE_DONE
          
          'Wait until calling application releases us to proceed
          repeat while long[debug_data_ptr] <> DEBUG_STATE_PLAY_NOTE

          'Set ouselves up to halt again
          long[debug_data_ptr] := 0    
               
      else
        'Done or instructed to stop.  Halt playback.
        internal_flags |= HDMF__HALT_PLAYBACK
        'Finish the last read if not done
        'repeat until eeprom.IsDone  

    'If not looping
    if not (external_flags & HDMF__LOOP)  
      internal_flags |=  HDMF__NO_SONG_PLAYING
      'Hang here until another stop or play is called 
      repeat while(external_flags & HDMF__ENABLE_PLAYBACK)

PUB PlaySoundFM(arg_channel, arg_shape, arg_freq, arg_duration, arg_volume, arg_amp_env)
'' This is a pass-thru.  The parameters are passed unchanged to the snd driver.
''
  repeat while (LOCKSET(SOUND_DRIVER_SEMAPHORE)==1)
  
  snd.PlaySoundFM(arg_channel, arg_shape, arg_freq, arg_duration, arg_volume, arg_amp_env)

  LOCKCLR(SOUND_DRIVER_SEMAPHORE)

PUB EEPROM_Read(arg_buffer_address, arg_eeprom_address, arg_byte_count)
'' This is a pass-thru.  The parameters are passed unchanged to the eeprom driver.
''
  repeat while (LOCKSET(EEPROM_DRIVER_SEMAPHORE)==1)
  ram.Read(arg_buffer_address, arg_eeprom_address-$8000, arg_byte_count) 

PUB EEPROM_IsDone | done
'' This is a pass-thru.  The query is issued to the eeprom driver.
''     
  done := true'eeprom.IsDone
  LOCKCLR(EEPROM_DRIVER_SEMAPHORE)
  
  return done