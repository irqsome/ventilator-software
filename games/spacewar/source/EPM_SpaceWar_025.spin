' //////////////////////////////////////////////////////////////////////
' TITLE: EPM_SpaceWar_025.SPIN - Free flight space battle
'
' DESCRIPTION:
'          Based on the orinial PDP-1 SpaceWar! by Steve "Slug" Russell, Martin "Shag" Graetz and Wayne Wiitanen,
'          with modifications based on other implementations, and whatever struck my fancy.
'          SpaceWar! was hacked relentlessly by the development community in its day.  Feel free to 
'          continue the tradition and hack away!  Send me your cool mods and I'll work them into
'          the main release.
'
' AUTHOR:  Eric Moyer
'          Monumental thanks to Andre' LaMothe, for creating "SPACE_DEMO_001", on which
'          this game is based.
'
' UPDATES: Search the Parallax forums at http://forums.parallax.com for the latest version.
'          As of this time, releases are being posted to the thread:
'             http://forums.parallax.com/forums/default.aspx?f=33&m=187691   
'
' CONTROLS:
'          Two NES gamepads, or a gamepad and the keyboard
'          Press START or <Spacebar> at the GAME OVER screen to return to the options menu.
'
'          Player 0    Player 1       Function 
'          --------    ------------   ------------------------------------------------------------------------
'          LEFT        LEFT  or 'A'   Rotate Left
'          RIGHT       RIGHT or 'D'   Rotate Right
'          UP          UP    or 'W'   Shields       
'          DOWN        DOWN  or 'S'   <unused>      (Eventually hyperspace, or other special options trigger)
'          A           A     or 'Y'   Fire
'          B           B     or 'G'   Thrust
'
'          The options screen is controlled by player 0's gamepad.  Use UP/DOWN to choose, and A to toggle an option.
'
' HACKING: If you want to hack SpaceWar!, start by turning on DEBUG__SHOW_PACING_OVERRUNS. If your processing
'          overhead overruns the allotted frame rate a white square will appear in the upper right hand corner
'          for each overrun frame.
'
'          If you want to monitor a variable, turn on DEBUG__SHOW_WATCH_VARIABLES and set watchval_fixed or 
'          watchval_int (for fixed point and integer values) in the main execution loop somewhere.  Monitoring
'          will slow down the frame rate a bit.
'
'          If you set DEBUG__ENABLE_TEST_MONKEY the game will play itself randomly. Usefull
'          for getting a long-term "burn in" run to verify code stability.
'                                                                   
'
' //////////////////////////////////////////////////////////////////////
'
'  Revision History
'
'  Rev  Date      Description
'  ---  --------  ---------------------------------------------------
'  011  04-13-07  First playable release.
'  013  04-25-07  Release for first upload.
'  014  05-01-07  Migrate to 2 color video driver to open up 12K of code space.
'                 Add keyboard control for player 1.
'                 Fix pacing so that frame rate is consistent regardless of the processing overhead
'                   within a given frame.
'                 Add shield animation.
'                 Add explode, disappear, delay sequencing to player death.
'                 Use temporary debug graphic ('x') for exploding ship.
'                 Add ship/ship collision detection.
'                 Add shield functionality (against shot/ship and ship/ship).
'                 Show shield energy, and deplete as used.
'                 Shrink shots to single pixels.
'  015  05-05-07  Add Nick Sabalausky's sound driver (not using any sounds yet though).
'                 Add fuel tracking and display fuel gauge.
'                 Cleanup/optimize player stats display code.
'                 Implement explosion particle animation.
'                 Modify pacing so that waitcnt() call is bypassed whenever game frames run
'                    over their allotted time.
'                 Add options screen, and change gameflow so next match can be started
'                    without resetting the console.
'                 Implement option flags for all currently supported options.
'                 Move origination point of shots to nose of ship (was center).
'                 Use fixed point math for shield and fuel tracking to avoid a costly divide when
'                    rendering the gauges.
'                 Swapped shield/fuel so shields are the dashed line.
'                 Offset thrust drawing to rear of ships.
'                *Temporarily disable shot/shield bounce code (not ready yet).
'                 Fix pacing calculation to correct pacing hang
'                 Fix bug where shield or fuel could run negative
'  016  05-06-07  Code some of BFG for testing (not ready for release yet).
'                 Add debug mode.
'                 Change option menu up/down behavior to work on keydown event, and to autorepeat.
'                 Change default options, and make non-inertial shots the default setting.
'                 Increase the velocity of non-inertial shots.
'                 Change black hole animation
'  017  05-07-07  Revise pacing algorithm - found a flaw in the old one.
'                 Light debug LED during pacing delay.
'                 Add test monkey.
'                *DEBUG__SHOW_PACING_OVERRUNS is temporarily non-functional.  Will fix when I port
'                   the new pacing algorithm to assembly where it belongs.
'  018  05-15-07  Fix bug where keyboard control did not work unless controller 1 was plugged in
'                   (which embarassingly defeated the whole point of adding keyboard control!)
'  019  05-15-07  Add upper EEPROM memory asset managment.
'                 Add Nick's EEPROM driver.
'                 Load starup screen from EEPROM
'  022  06-20-07  Disable EEPROM stuff for now.
'                 Fix game over hang (ignoring start button) when only one connector is inserted.
'                 Add sound effects.
'  023  06-20-07  Fix indexing bug on player shots.                                                               
'                                        
'  Open Issues:  
'     High Priority:
'       Refine shield behavior (ship/ship and ship/shot bounce)
'
'     Medium Priority:
'       Modify explosion particle distribution to a circular pattern with highest density nearest the origin.
' 
'     Lower Priority:
'       Add remaining options: 
'          Hyperspace.
'          BFG.
'          partial damage model.
'       Properly handle dual death (wait for both players to sequence through explode, disappear, delay) 
'          before starting the next match
'
'       Possible other options:
'         Edge bounce (closed space instead of wrap around)
'         Handicap (vary starting number of ships for each player)
'
'
' //////////////////////////////////////////////////////////////////////
'
' SCCuM Preprocessor Definitions
' (You can Get SCCuM here: http://forums.parallax.com/forums/default.aspx?f=33&m=194963)
'
'#DEFINE LCD_DEBUGGER_ENABLE    = FALSE
'
' Don't bother turning either of these options on, the EEPROM graphics and music assets have not been released
'#DEFINE EEPROM_GRAPHICS_ENABLE = TRUE   
'#DEFINE MUSIC_ENABLE           = TRUE
'
'
'///////////////////////////////////////////////////////////////////////
' CONSTANTS SECTION ////////////////////////////////////////////////////
'///////////////////////////////////////////////////////////////////////

CON

  _clkmode = xtal1 + pll8x            ' enable external clock and pll times 8
  _xinfreq = 5_000_000 + 0000        ' set frequency to 10 MHZ plus some error for xtals that are not exact add 1000-5000 usually works
  _stack = ($1800 + $1800 + 64) >> 2  ' accomodate display memory and stack

  'DEBUG OPTIONS:
  DEBUG__SHOW_WATCH_VARIABLES   = FALSE
  DEBUG__SHOW_PACING_OVERRUNS   = FALSE
  DEBUG__ENABLE_DEBUG_MODE      = FALSE
  DEBUG__ENABLE_TEST_MONKEY     = FALSE
  DEBUG__SHOW_PACING_LED        = FALSE

  'EEPROM ASSET MAP
  '' Porting note: some of these wre wrong, resulting in no gameover music
  '' Also, many of them are unused and could be removed to reduce load time
  '' Oddly enough, "Lost Love" isn't part of the actual image and does not exist in the source folder
  '' Even stranger, there is an "Eagle.bin" file that is part of the image and appears to be entirely unused
  EEPROM_SCREEN_TITLE           = $08000
  EEPROM_SCREEN_OPTIONS         = $09800 
  EEPROM_SONG_GOTHIC            = $0B000
  EEPROM_SONG_FF1_BATTLE        = $0BE52
  EEPROM_SONG_NORTH_WALL        = $0FBE6'$10108
  EEPROM_SONG_GENTLE_WIND       = $0F129'$117FB
  'EEPROM_SONG_LOST_LOVE         = $122B8
  
  ' graphics driver and screen constants
  PARAMCOUNT        = 14        
  OFFSCREEN_BUFFER  = $5000           ' offscreen buffer
  ONSCREEN_BUFFER   = $6800           ' onscreen buffer

  ' size of graphics tile map
  X_TILES           = 16
  Y_TILES           = 12
  
  SCREEN_WIDTH      = 256
  SCREEN_HEIGHT     = 192 

  ' Status display constants 
  PLAYER_1_STATS_X_POS       = -SCREEN_WIDTH/2  + 10
  PLAYER_1_STATS_Y_POS       =  SCREEN_HEIGHT/2 - 12

  PLAYER_0_STATS_X_POS       =  SCREEN_WIDTH/2  - 10/2*12 - 8
  PLAYER_0_STATS_Y_POS       =  SCREEN_HEIGHT/2 - 12

  STATUS_GAUGE_LENGTH        = 50

  ' game states
  GAME_STATE_INIT      = 0
  GAME_STATE_MENU      = 1
  GAME_STATE_START     = 2
  GAME_STATE_RUN       = 3

  ' angular constants to make object declarations easier
  ANG_0    = $0000
  ANG_360  = $2000
  ANG_240  = ($2000*2/3)
  ANG_180  = ($2000/2)
  ANG_270  = ($2000*3/4)
  ANG_120  = ($2000/3)
  ANG_90   = ($2000/4)
  ANG_60   = ($2000/6)
  ANG_45   = ($2000/8)
  ANG_30   = ($2000/12)
  ANG_22_5 = ($2000/16)
  ANG_15   = ($2000/24)
  ANG_10   = ($2000/36)
  ANG_5    = ($2000/72)
  ANG_1    = ($2000/360)

  ' constants for math functions
  SIN      = 0
  COS      = 1

  ' player states
  SHIP_STATE_ALIVE     = $70
  SHIP_STATE_EXPLODING = $6F
  SHIP_STATE_DIE_DELAY = $20
  SHIP_STATE_DEAD      = $00

  ' control interface
  THRUST_BUTTON_ID = 1
  FIRE_BUTTON_ID   = 0

  ' physical modeling constants
  SPACE_FRICTION                = 8   ' from 0 to 15, 0 is infinite friction, 15 is 0.000001 approximately
  MAX_SHIP_VEL                  = 15   ' maximum velocity of player's ship
  MAX_PROJECTILE_VEL            = 20

  ' SNES bit encodings (porting note: original uses NES only,
  ' but the title screen shows a SNES-style controller with a button map
  ' that differs from what a SNES pad read like a NES pad gives.
  ' Then OCD happened.)
  NES_R      = %0000000000010000
  NES_L      = %0000000000100000
  NES_X      = %0000000001000000
  NES_A      = %0000000010000000
  NES_RIGHT  = %0000000100000000
  NES_LEFT   = %0000001000000000
  NES_DOWN   = %0000010000000000
  NES_UP     = %0000100000000000
  NES_START  = %0001000000000000
  NES_SELECT = %0010000000000000
  NES_Y      = %0100000000000000
  NES_B      = %1000000000000000

  ' Number of players
  NUM_PLAYERS  = 2
  PLAYER_0     = 0
  PLAYER_1     = 1

  ' Player shots
  NUM_SHOTS = 8                              ' Number of shots on screen at any time    
  PLAYER_1_SHOT_START = (NUM_SHOTS / 2)      ' Index of first player 2 shot
  SHOT_TTL_INIT = 130                        ' Shot time to live
  COLLISION_RANGE_FP      = $0005_0000       ' Collision bounding box range, in fixed point
  SHIP_GUN_PLACEMENT_DISTANCE = 7

  ' Objects
  NUM_OBJECTS  = NUM_PLAYERS + NUM_SHOTS
  OBJECT_PLAYER_0        = 0
  OBJECT_PLAYER_1        = 1
  OBJECT_PLAYER_0_SHOT_0 = 2
  OBJECT_PLAYER_1_SHOT_0 = 6 

  ' Black holes
  BLACK_HOLE_GRAVITY    = 64
  BLACK_HOLE_X          = 0
  BLACK_HOLE_Y          = 0
  
  ' Game options
  OPTION_BLACK_HOLE      = %00000000_00000000_00000000_00000001
  OPTION_SHOT_GRAVITY    = %00000000_00000000_00000000_00000010
  OPTION_INERTIAL_SHOTS  = %00000000_00000000_00000000_00000100
  OPTION_SHIELDS         = %00000000_00000000_00000000_00001000
  OPTION_FUEL            = %00000000_00000000_00000000_00010000
  OPTION_BFG             = %00000000_00000000_00000000_00100000
  OPTION_HYPERSPACE      = %00000000_00000000_00000000_01000000
  OPTION_DAMAGE          = %00000000_00000000_00000000_10000000

  OPTION_MONKEY_IS_ALIVE = %10000000_00000000_00000000_00000000   ' Enables input banger test
  ' Future options: Barriers?

  'Ship Flags
  SHIP_FLAG_SHIELDS_UP  = %00000001
  SHIP_FLAG_THRUSTING   = %00000010

  'Menu Flags
  MENU_FLAG__A_HELD                 = %00000001
  MENU_FLAG__UP_DOWN_HELD           = %00000010
  MENU_FLAG__REPEAT_GO_TIME_MET     = %00000100
  MENU_FLAG__DO_REPEAT_CLICK        = %00001000

  'BFG Flags
  BFG_FLAG__OUT_OF_PLAY       = %00000001
  BFG_FLAG__PICKUP            = %00000010
  BFG_FLAG__HELD_BY_PLAYER_0  = %00000100
  BFG_FLAG__HELD_BY_PLAYER_1  = %00001000
  BFG_FLAG__DEPLOYED          = %00010000

  'BFG
  BFG_SPOKE_SIZE              = 50
  BFG_HUB_SIZE                = 11

  'Shield settings
  SHIELD_CONSUMPTION_RATE_FXP = (STATUS_GAUGE_LENGTH << 16) / 100    ' Shield consumption rate, fixed point
  SHIELD_RADIUS               = 70

  'Fuel settings
  FUEL_CONSUMPTION_RATE_FXP   = (STATUS_GAUGE_LENGTH << 16) / 600    ' Fuel consumption rate, fixed point
  P0_THRUSTER_DISTANCE        = 3
  P1_THRUSTER_DISTANCE        = 6 

  'Explosions
  NUM_EXPLOSION_PARTICLES = 15      ' Number of particles in an explosion
  EXPLOSION_RADIUS        = 17      ' Explosion radius, in pixels

  'Default game options
  DEFAULT_OPTIONS       = OPTION_BLACK_HOLE | OPTION_SHOT_GRAVITY | OPTION_SHIELDS | OPTION_FUEL  {| OPTION_BFG}

  'Vector drawing commands
  VECTOR_MOVE           = $4000
  VECTOR_DRAW           = $8000
  VECTOR_END            = $0000

  'Game pacing tick
  PACING_TICK           = 1666_666 '1666_666   ' Target game frame rate
  PACING_MARGIN         =    3_000

  'Black hole
  NUM_BLACK_HOLE_PARTICLES = 10 
   
'///////////////////////////////////////////////////////////////////////
' VARIABLES SECTION ////////////////////////////////////////////////////
'///////////////////////////////////////////////////////////////////////

VAR

  long  tv_status     '0/1/2 = off/visible/invisible           read-only
  long  tv_enable     '0/? = off/on                            write-only
  long  tv_pins       '%ppmmm = pins                           write-only
  long  tv_mode       '%ccinp = chroma,interlace,ntsc/pal,swap write-only
  long  tv_screen     'pointer to screen (words)               write-only
  long  tv_colors     'pointer to colors (longs)               write-only               
  long  tv_hc         'horizontal cells                        write-only
  long  tv_vc         'vertical cells                          write-only
  long  tv_hx         'horizontal cell expansion               write-only
  long  tv_vx         'vertical cell expansion                 write-only
  long  tv_ho         'horizontal offset                       write-only
  long  tv_vo         'vertical offset                         write-only
  long  tv_broadcast  'broadcast frequency (Hz)                write-only
  long  tv_auralcog   'aural fm cog                            write-only

  word  screen[x_tiles * y_tiles] ' storage for screen tile map
  long  colors[64]                ' color look up table

  ' common object properties
  long object_x[NUM_OBJECTS]
  long object_y[NUM_OBJECTS]
  long object_dx[NUM_OBJECTS]
  long object_dy[NUM_OBJECTS]
  
  ' player's ship
  byte ship_state[NUM_PLAYERS]
  byte ship_flags[NUM_PLAYERS]
  long ship_shield_power_fxp[NUM_PLAYERS]
  long ship_fuel_fxp[NUM_PLAYERS] 
  byte lives[NUM_PLAYERS]
  long ship_angle[NUM_PLAYERS]
  long thrust_dx[NUM_PLAYERS], thrust_dy[NUM_PLAYERS] 
  long friction_dx[NUM_PLAYERS], friction_dy[NUM_PLAYERS]
  long bforce_dx[NUM_PLAYERS], bforce_dy[NUM_PLAYERS]
  long player_nes_buttons[NUM_PLAYERS]
  long score[NUM_PLAYERS]
  long shield_angle

  ' bfg
  long bfg_x
  long bfg_y
  byte bfg_flags

  ' explosion particles
  long explosion_dx_fxp[NUM_EXPLOSION_PARTICLES]          ' particle delta x (fixed point)
  long explosion_dy_fxp[NUM_EXPLOSION_PARTICLES]          ' particle delta y (fixed point)

  ' player's shots
  word shot_time_to_live[NUM_SHOTS]

  long random_var                                         ' global random variable
  long curr_count                                         ' saves counter  

  byte fire_debounce                                      ' button debounce

  ' nes gamepad vars
  long nes_buttons
  long button_temp

  ' black hole
  long black_hole_particle_mask
  long black_hole_particle_angle[NUM_BLACK_HOLE_PARTICLES]
  long black_hole_particle_radius[NUM_BLACK_HOLE_PARTICLES] 

  ' game mode options
  long game_options

  ' pacing
  long pacing_cnt
  long pacing_target
  long pacing_wrapped_target
  
  ' sound effects process stack
  long sfx_stack[40]

  '#IF LCD_DEBUGGER_ENABLE
  { '<added by SCCuM preprocessor utility>
  byte debugger_key_held
  } '<added by SCCuM preprocessor utility>
  '#ENDIF
  
'///////////////////////////////////////////////////////////////////////
' OBJECT DECLARATION SECTION ///////////////////////////////////////////
'///////////////////////////////////////////////////////////////////////
OBJ

  tv     : "EPM_tv_drv_011.spin"                ' Paralax tv driver, modified for 2 color 
  gr     : "EPM_graphics_drv_spacewar_012.spin" ' Paralax graphics driver, modified for 2 color 
  key    : "keyboard.spin"              ' Paralax keyboard driver
  hdmf  : "EPM_HDMF_driver_011.spin"             'HDMF song player

  '#IF LCD_DEBUGGER_ENABLE  
  { '<added by SCCuM preprocessor utility>
  debug  : "EPM_LCD_debug_monitor_001.spin"     ' LCD Debugger
  } '<added by SCCuM preprocessor utility>
  '#ENDIF  

'///////////////////////////////////////////////////////////////////////
' EXPORT PUBLICS  //////////////////////////////////////////////////////
'///////////////////////////////////////////////////////////////////////

PUB start | i , dx, dy

  '//// GLOBAL INITIALIZTION
  
  'default game options
  game_options := DEFAULT_OPTIONS
                                                                         
  if DEBUG__ENABLE_TEST_MONKEY
    game_options |= OPTION_MONKEY_IS_ALIVE
  
  'start tv
  longmove(@tv_status, @tvparams, paramcount)
  tv_screen := @screen
  tv_colors := @colors
  tv.start(@tv_status)

  ' Start keyboard driver
  key.start(8,9)

  'start sound driver
  'snd.start(7)

  'start sound effects manager
  cognew(SFX_manager, @sfx_stack)

  'start music (& sound effects) driver
  hdmf.start(0)
  
  'start debugger
  '#IF LCD_DEBUGGER_ENABLE  
  { '<added by SCCuM preprocessor utility>
  debug.start
  debug.add_watch(@sfx_flags,debug#FORMAT__TYPE_HEX | debug#FORMAT__SIZE_8,string("sfx_flags"))  
  debug.add_watch(@curr_count,debug#FORMAT__TYPE_UINT,string("curr_count"))  
  debug.add_watch(@curr_count,debug#FORMAT__TYPE_INT,string("curr_count")) 
  debug.add_watch(@curr_count,debug#FORMAT__TYPE_HEX | debug#FORMAT__SIZE_32,string("curr_count"))
  debug.add_watch(@shot_time_to_live,debug#FORMAT__TYPE_HEX | debug#FORMAT__SIZE_16,string("time_live[0]"))
  debug.add_watch(@lives[0],debug#FORMAT__TYPE_HEX | debug#FORMAT__SIZE_8,string("lives[0]"))
  debug.add_watch(@object_dx[0],debug#FORMAT__TYPE_FIX,string("object_dx[0]"))
  debug.add_watch(@object_dy[0],debug#FORMAT__TYPE_FIX,string("object_dy[0]"))
  debug.add_watch(@object_dx[1],debug#FORMAT__TYPE_FIX,string("object_dx[1]"))
  debug.add_watch(@object_dy[1],debug#FORMAT__TYPE_FIX,string("object_dy[1]"))
  } '<added by SCCuM preprocessor utility>
  '#ENDIF

  'init debug LED
  DIRA [0] := 1 ' output 
  
  'init colors
  repeat i from 0 to 64
    colors[i] := $00001010 * (i+4) & $F + $FB060C02

  'init tile screen
  repeat dx from 0 to tv_hc - 1
    repeat dy from 0 to tv_vc - 1
      screen[dy * tv_hc + dx] := onscreen_buffer >> 6 + dy + dx * tv_vc + ((dy & $3F) << 10)

  'start and setup graphics
  gr.start
  gr.setup(16, 12, 128, 96, OFFSCREEN_BUFFER)

  '---------------------------------------------------------------
  'Show title screen
  
  '#IF EEPROM_GRAPHICS_ENABLE
  waitcnt(cnt + 5000)
  gr.clear
  hdmf.EEPROM_Read(OFFSCREEN_BUFFER, EEPROM_SCREEN_TITLE, CONSTANT((SCREEN_WIDTH * SCREEN_HEIGHT)>>3))  
  repeat until hdmf.EEPROM_IsDone
  gr.textmode(1,1,6,1)
  gr.colorwidth(2,0)
  gr.text(Constant(-SCREEN_WIDTH/2 + 1), constant(-SCREEN_HEIGHT/2 + 5), @version_string)
  gr.copy(onscreen_buffer)

  'Wait for start button (or spacebar) to be pressed
  WaitForStartButton
  '#ENDIF

  '---------------------------------------------------------------      
  
  
  '//// MAIN EXECUTION MANAGER
  repeat
    'PlaySong
    Configure_Options 
    main_game

PUB main_game | i, j, base, base2, dx, dy, x, y, x2, y2, length, f, v, a, r, r_squared, player_index, shot_index, watchval_int, watchval_fixed, died, thrust_snd_flag, shield_snd_flag                    
  
  ' initialize random variable
  random_var := cnt*171732
  random_var := ?random_var
  lives[PLAYER_0] := 5
  lives[PLAYER_1] := 5
  '#IF LCD_DEBUGGER_ENABLE
  { '<added by SCCuM preprocessor utility>
  debugger_key_held := false
  } '<added by SCCuM preprocessor utility>
  '#ENDIF

  'Initialize debug watch variables
  watchval_fixed := 0
  watchval_int   := 0

  '#IF MUSIC_ENABLE
  hdmf.play_song(EEPROM_SONG_FF1_BATTLE | hdmf#EEPROM_ASSET, hdmf#HDMF__LOOP)
  '#ENDIF
  
  repeat while (( lives[PLAYER_0] <> 0) & ( lives[PLAYER_1] <> 0))
  
    '//// Stop any ship action sounds in progress
    SFX_stop_ship_action_sounds(0)
    SFX_stop_ship_action_sounds(1)   
     
    '//// Initialize object info
    repeat i from 0 to NUM_OBJECTS - 1
      object_x[i] := 0        
      object_y[i] := 0        
      object_dx[i] := 0        
      object_dy[i] := 0
     
    '//// Initialize ship info
    object_x[OBJECT_PLAYER_0] := ((SCREEN_WIDTH / 2) - 15) << 16
    object_y[OBJECT_PLAYER_0] := ((SCREEN_HEIGHT / 2) - 15) << 16
    object_x[OBJECT_PLAYER_1] := -(((SCREEN_WIDTH / 2) - 15) << 16)
    object_y[OBJECT_PLAYER_1] := -(((SCREEN_HEIGHT / 2) - 15) << 16)
    ship_angle[OBJECT_PLAYER_0] := ANG_270
    ship_angle[OBJECT_PLAYER_1] := ANG_90
    shield_angle := ANG_0 
     
    repeat player_index from 0 to NUM_PLAYERS - 1
      ship_state[player_index]            := SHIP_STATE_ALIVE
      ship_flags[player_index]            := 0
      score[player_index]                 := 0
      ship_shield_power_fxp[player_index] := STATUS_GAUGE_LENGTH << 16    ' Fixed point
      ship_fuel_fxp[player_index]         := STATUS_GAUGE_LENGTH << 16    ' Fixed point
     
    '//// Initialize shot info
    repeat shot_index from 0 to NUM_SHOTS -1
      shot_time_to_live[shot_index] := 0

    '//// Initialize BFG
    bfg_flags := BFG_FLAG__OUT_OF_PLAY

    '//// Initialize black hole
    black_hole_particle_mask := 0

    '//// Initialize explosion particles  
    repeat i from 0 to NUM_EXPLOSION_PARTICLES - 1
      'Create random fixed point dx's and dy's between -1.0 and 1.0
      'Note: The distrubution of these random points is currently a square.
      '      Would be better to pick points in polar space and make the distribution
      '      a circle, or even to force the density distribution so that it was
      '      more dense near the middle. 
      explosion_dx_fxp[i] := Rand_Range(-$0000ffff,$0000ffff)
      explosion_dy_fxp[i] := Rand_Range(-$0000ffff,$0000ffff) 
     
    ' BEGIN GAME LOOP ////////////////////////////////////////////////////
    died := 0

    repeat while (died == 0)
      curr_count := cnt ' Save current counter value
      gr.clear        ' Clear bitmap 

      shield_angle += CONSTANT($2000 / 31)
      
      '//// Read NES controller buttons
      if (game_options & OPTION_MONKEY_IS_ALIVE)
        'Simulate random input for testing
        if Rand_Range(1,20) =< 1
          nes_buttons := Rand_Range(0,$ffffffff)
        else
          nes_buttons := 0  
      else
        nes_buttons := NES_Read_Gamepad
      player_nes_buttons[0] := nes_buttons & $ffff
      player_nes_buttons[1] := (nes_buttons >> 16)& $ffff

      '//// Process keyboard keys
      if(key.keystate(119)) 'w'
        player_nes_buttons[PLAYER_1]|=CONSTANT(NES_UP)
      elseif(key.keystate(115)) 's'
        player_nes_buttons[PLAYER_1]|=CONSTANT(NES_DOWN)
      if(key.keystate(97)) 'a'
        player_nes_buttons[PLAYER_1]|=CONSTANT(NES_LEFT)
      elseif(key.keystate(100)) 'd'
        player_nes_buttons[PLAYER_1]|=CONSTANT(NES_RIGHT)
      if(key.keystate(121)) 'y'
        player_nes_buttons[PLAYER_1]|=CONSTANT(NES_A)
      elseif(key.keystate(103)) 'g'
        player_nes_buttons[PLAYER_1]|=CONSTANT(NES_B)

      '//// LCD Debugger control keys
      '#IF LCD_DEBUGGER_ENABLE
      { '<added by SCCuM preprocessor utility>
      'Process debugger keys
      if(key.keystate($c6)) 'Page UP'
        if not debugger_key_held
          debug.page_up
          debugger_key_held := TRUE
      elseif (key.keystate($c7)) 'Page DOWN'
        if not debugger_key_held   
          debug.page_down
          debugger_key_held := TRUE
      else
        debugger_key_held := FALSE
      } '<added by SCCuM preprocessor utility>
      '#ENDIF LCD_DEBUGGER_ENABLE  

      '//// Debugging mode features
      if DEBUG__ENABLE_DEBUG_MODE
        ' Display debug mode indication
        gr.text(-20, 83, @debug_string)

        if(key.keystate(49)) '1'
          bfg_flags := BFG_FLAG__PICKUP
          bfg_x     := 115
          bfg_y     := 60
          
      '//// Process both player ships
      repeat player_index from 0 to NUM_PLAYERS - 1

        'Determine sound flags for current plater
        if player_index == 0
           thrust_snd_flag := SFX_SND_SHIP0_THRUST
           shield_snd_flag := SFX_SND_SHIP0_SHIELD
        else
           thrust_snd_flag := SFX_SND_SHIP1_THRUST
           shield_snd_flag := SFX_SND_SHIP1_SHIELD

        if ship_state[player_index] == SHIP_STATE_ALIVE 
           
          '//// Process ROTATE
          button_temp := player_nes_buttons[player_index] ' This is a workaround for a spin compiler bug.
                                                          ' The compiler was not properly handling references
                                                          ' to player_nes_buttons[player_index] in the math below.    
          ' Process rotate left / right
          if((button_temp & NES_RIGHT) <> 0)
            ship_angle[player_index] := ship_angle[player_index] - 3 << 5 
          if((button_temp & NES_LEFT) <> 0)
            ship_angle[player_index] := ship_angle[player_index] + 3 << 5  
           
          ' Bounds test ship angle
          if (ship_angle[player_index] > ANG_360)
            ship_angle[player_index] -= ANG_360
          elseif (ship_angle[player_index] < 0)
            ship_angle[player_index] +=ANG_360

          '//// Process SHIELDS
          if((button_temp & NES_UP) <> 0) and ship_shield_power_fxp[player_index] and (game_options & OPTION_SHIELDS) 
            ship_flags[player_index] |= SHIP_FLAG_SHIELDS_UP

            ' decrement shield power remaining
            if (ship_shield_power_fxp[player_index] > SHIELD_CONSUMPTION_RATE_FXP)
              ship_shield_power_fxp[player_index] -= SHIELD_CONSUMPTION_RATE_FXP
            else
              ship_shield_power_fxp[player_index] := 0

            'Play shield sound  
            sfx_flags |= shield_snd_flag     
            
          else
            ship_flags[player_index] &= !(SHIP_FLAG_SHIELDS_UP)

            'Stop shield sound
            sfx_flags &= !shield_snd_flag
         
          '//// Process FIRE
          if ((button_temp & NES_A) <> 0) & ((fire_debounce & (player_index + 1)) == 0)
            if (player_index == 0)
              i := 0
              j := PLAYER_1_SHOT_START - 1
            else
              i := PLAYER_1_SHOT_START
              j := NUM_SHOTS - 1
            repeat shot_index from i to j
              if (shot_time_to_live[shot_index] == 0)
                shot_time_to_live[shot_index] := SHOT_TTL_INIT
                object_x[shot_index + OBJECT_PLAYER_0_SHOT_0] := object_x[player_index] + SinCos(COS, ship_angle[player_index]) * SHIP_GUN_PLACEMENT_DISTANCE
                object_y[shot_index + OBJECT_PLAYER_0_SHOT_0] := object_y[player_index] + SinCos(SIN, ship_angle[player_index]) * SHIP_GUN_PLACEMENT_DISTANCE 
                if (game_options & OPTION_INERTIAL_SHOTS)
                  ' Shot velocity is ship velocity + muzzle velocity
                  object_dx[shot_index + OBJECT_PLAYER_0_SHOT_0] := SinCos(COS, ship_angle[player_index]) + object_dx[player_index] 
                  object_dy[shot_index + OBJECT_PLAYER_0_SHOT_0] := SinCos(SIN, ship_angle[player_index]) + object_dy[player_index]
                else
                  ' Shot velocity is fixed regardless of ship velocity
                  object_dx[shot_index + OBJECT_PLAYER_0_SHOT_0] := SinCos(COS, ship_angle[player_index]) << 1
                  object_dy[shot_index + OBJECT_PLAYER_0_SHOT_0] := SinCos(SIN, ship_angle[player_index]) << 1

                fire_debounce |= (player_index + 1) ' Set the debounce flag

                'Play shot sound
                if (player_index == 0)   
                  sfx_flags |= SFX_SND_SHIP0_SHOT_START
                else
                  sfx_flags |= SFX_SND_SHIP1_SHOT_START 
                
                quit
          elseif ((button_temp & NES_A) == 0)
            fire_debounce &= !(player_index + 1) ' Clear the debounce flag
               
           
          '//// Process THRUST
          ' all calculations for ship thrust and position model are performed in fixed point math          
          if (player_nes_buttons[player_index] & NES_B) and (ship_fuel_fxp[player_index] or not(game_options & OPTION_FUEL))
            ' compute thrust vector, scale down cos/sin a bit to slow ship's acceleration
            thrust_dx[player_index] := SinCos(COS, ship_angle[player_index]) ~> 5
            thrust_dy[player_index] := SinCos(SIN, ship_angle[player_index]) ~> 5
           
            ' apply thrust to ships current velocity
            object_dx[player_index] += thrust_dx[player_index]
            object_dy[player_index] += thrust_dy[player_index]

            ' decrement fuel remaining
            if ( ship_fuel_fxp[player_index] > FUEL_CONSUMPTION_RATE_FXP)
              ship_fuel_fxp[player_index] -= FUEL_CONSUMPTION_RATE_FXP
            else
              ship_fuel_fxp[player_index] := 0

            ' set thrusting flag
            ship_flags[player_index] |= SHIP_FLAG_THRUSTING

            ' play thrust sound
            sfx_flags |= thrust_snd_flag
            
          else
            ' clear thrusting flag
            ship_flags[player_index] &= !(SHIP_FLAG_THRUSTING)

            'stop thrust sound
            sfx_flags &= !thrust_snd_flag
            
            
 
           
           
          '//// Process Friction
          ' apply friction model, always in opposite direction of velocity
          ' frictional force is proportional to velocity, use power of 2 math to save time
          object_dx[player_index] -= object_dx[player_index] ~> SPACE_FRICTION
          object_dy[player_index] -= object_dy[player_index] ~> SPACE_FRICTION
        else
         
          '//// PROCESS DEGRADING SHIP STATE (EXPLODING -> DELAY -> DEAD) 
          if ship_state[player_index] > 0
            if (--ship_state[player_index] == 0)
              ' ship has gone thorough all the decaying states of death, now set 'died' flag so game round will end.
              died := 1
 
      repeat i from 0 to NUM_OBJECTS - 1
     
        '//// Process Black Hole
        '     NOTE: processed if enabled, and object is not a dead ship
        if ((game_options & OPTION_BLACK_HOLE) <> 0) and not (  (i =< PLAYER_1 ) and (ship_state[i] <> SHIP_STATE_ALIVE) ) and not ((i => OBJECT_PLAYER_0_SHOT_0) and not (game_options & OPTION_SHOT_GRAVITY))
          ' now compute the acceleration toward the black hole, model based on F = (G*M1*M2)/r^2
          ' in other words, the force is equal to the product of the two masses times some constant G divided
          ' by the distance between the masses squared. Thus, we more or less need to accelerate the ship
          ' toward the black hole(s) proportional to some lumped constant divided by the distance to the black
          ' hole squared...
      
          ' compute each force direction vector d(dx ,dy) toward the black hole, so we can compute its length
          dx := BLACK_HOLE_X - (object_x[i] ~> 16)
          dy := BLACK_HOLE_Y - (object_y[i] ~> 16)
          r_squared := (dx*dx + dy*dy) ' no need to compute length r, when we are going to use r^2 in a moment 
         
          ' now compute the actual force itself, which is proportional to accel which in this sim will be used to change velocity each frame
          f := (BLACK_HOLE_GRAVITY << 16 ) / r_squared
          if (f > $0001_0000 ) ' Clip force at 1.0
            f := $0001_0000
         
          ' f can be thought of as acceleration since its proportional to mass which is virtual and can be assumed to be 1
          ' thus we can use it to create a velocity vector toward the black hole now in the direction of of the vector d(dx, dy)
          dx := dx << 16 ' convert to fixed point
          dy := dy << 16
         
          ' compute length of d which is just r, careful to compute fixed point values properly
          r := (^^(r_squared << 16)) >> 8 ' square root operation turns 16.16 into 24.8. Convert r to integer
         
          ' normalize the vector and scale by force magnityde
          dx := (f~>8) * ((dx / r) ~> 8)
          dy := (f~>8) * ((dy / r) ~> 8)
         
          ' update velocity with acceleration due to black hole
          object_dx[i] += dx
          object_dy[i] += dy

          'if object is a plyer ship
          if (i < OBJECT_PLAYER_0_SHOT_0)
            'Check for Collision with black hole
            if ship_state[i] == SHIP_STATE_ALIVE
              if (object_x[i] > (BLACK_HOLE_X - COLLISION_RANGE_FP)) & (object_x[i] < (BLACK_HOLE_X + COLLISION_RANGE_FP))
                if (object_y[i] > (BLACK_HOLE_Y - COLLISION_RANGE_FP)) & (object_y[i] < (BLACK_HOLE_Y + COLLISION_RANGE_FP))
                  --lives[i]
                  ship_state[i] := SHIP_STATE_EXPLODING
                  sfx_flags |= SFX_SND_EXPLODE
                  'Colliding with the black hole tends to fling your exploding bits super fast across
                  '  space, which looks silly, so chop the velocity down
                  object_dx[i] ~>= 2
                  object_dy[i] ~>= 2
                  SFX_stop_ship_action_sounds(i)   
         
        ' CLAMP OBJECT VELOCITIES ///////////////////////////////////////
        ' clamp maximum velocity, otherwise ship will get going light speed due to black holes when the distance approaches 0!
        dx := (object_dx[i] ~> 16)
        dy := (object_dy[i] ~> 16)           
         
        ' test if object velocity greater than threshold
        v := ^^(dx*dx + dy*dy)
         
        ' perform comparison (to squared max, to make math easier)
        if (i < OBJECT_PLAYER_0_SHOT_0)
          ' Object is a ship
          if (v > MAX_SHIP_VEL)
            ' scale velocity vector back approx 1/8th
            object_dx[i] := MAX_SHIP_VEL*(object_dx[i] / v)                           
            object_dy[i] := MAX_SHIP_VEL*(object_dy[i] / v)
        else
          ' Object is a projectile
          if (v > MAX_PROJECTILE_VEL)
            ' scale velocity vector back approx 1/8th
            object_dx[i] := MAX_PROJECTILE_VEL*(object_dx[i] / v)                     
            object_dy[i] := MAX_PROJECTILE_VEL*(object_dy[i] / v)
         
         
        ' UPDATE OBJECT POSITIONS ///////////////////////////////////////
        ' Note: Time is currently wasted updating "inactive" shots, in order to
        ' conserve the code space necessary to test for those cases.1
        object_x[i] += object_dx[i]                    
        object_y[i] += object_dy[i]
         
        ' screen bounds test for objects
        if (object_x[i] > (SCREEN_WIDTH/2)<< 16)
          object_x[i] -= SCREEN_WIDTH<<16
        elseif (object_x[i] < (-SCREEN_WIDTH/2)<<16)
          object_x[i] += SCREEN_WIDTH<<16
         
        if (object_y[i] > (SCREEN_HEIGHT/2)<<16)
          object_y[i] -= SCREEN_HEIGHT<<16
        elseif (object_y[i] < (-SCREEN_HEIGHT/2)<<16)
          object_y[i] += SCREEN_HEIGHT<<16
         
      ' PLAYER SHOTS //////////////////////////////////////////////////
      repeat shot_index from 0 to NUM_SHOTS -1
        if (shot_time_to_live[shot_index] <> 0)
          --shot_time_to_live[shot_index]
          if (shot_index < PLAYER_1_SHOT_START)
            player_index := PLAYER_1
          else
            player_index := PLAYER_0

          '//// SHOT TO SHIELD COLLISIONS
          {
          ' If the target ship's shields are up
          if ship_flags[player_index] & SHIP_FLAG_SHIELDS_UP
            dx := (object_x[shot_index+OBJECT_PLAYER_0_SHOT_0] - object_x[player_index]) ~> 16   ' dx, as integer
            dy := (object_y[shot_index+OBJECT_PLAYER_0_SHOT_0] - object_y[player_index]) ~> 16   ' dy, as integer
            r_squared := (dx*dx + dy*dy)
            ' If the shot is within the shield radius
            if r_squared < CONSTANT(SHIELD_RADIUS * SHIELD_RADIUS)
              if true
                'SIMPLE REVERSE
                object_x[shot_index+OBJECT_PLAYER_0_SHOT_0] := -object_x[shot_index+OBJECT_PLAYER_0_SHOT_0]
                object_y[shot_index+OBJECT_PLAYER_0_SHOT_0] := -object_y[shot_index+OBJECT_PLAYER_0_SHOT_0]
              else
                'COMPLEX REVERSE
          }
          
          '//// SHOT TO SHIP COLLISIONS
          if (ship_state[player_index] == SHIP_STATE_ALIVE) and not (ship_flags[player_index] & SHIP_FLAG_SHIELDS_UP)  
            if (object_x[shot_index+OBJECT_PLAYER_0_SHOT_0] > (object_x[player_index] - COLLISION_RANGE_FP)) & (object_x[shot_index+OBJECT_PLAYER_0_SHOT_0] < (object_x[player_index] + COLLISION_RANGE_FP))
              if (object_y[shot_index+OBJECT_PLAYER_0_SHOT_0] > (object_y[player_index] - COLLISION_RANGE_FP)) & (object_y[shot_index+OBJECT_PLAYER_0_SHOT_0] < (object_y[player_index] + COLLISION_RANGE_FP))
                  --lives[player_index]
                  ship_state[player_index] := SHIP_STATE_EXPLODING
                  sfx_flags |= SFX_SND_EXPLODE   
                  SFX_stop_ship_action_sounds(player_index)

      ' PLAYER COLLISION /////////////////////////////////////////////////////
      if (ship_state[PLAYER_0] == SHIP_STATE_ALIVE) and (ship_state[PLAYER_1] == SHIP_STATE_ALIVE)
        'NOTE: Ship/Ship collision detection uses 2x the standard Shot/Ship collision detection range
        if (object_x[PLAYER_0] > (object_x[PLAYER_1] - CONSTANT(COLLISION_RANGE_FP<<1))) & (object_x[PLAYER_0] < (object_x[PLAYER_1] + CONSTANT(COLLISION_RANGE_FP<<1)))
          if (object_y[PLAYER_0] > (object_y[PLAYER_1] - CONSTANT(COLLISION_RANGE_FP<<1))) & (object_y[PLAYER_0] < (object_y[PLAYER_1] + CONSTANT(COLLISION_RANGE_FP<<1)))
            if not (ship_flags[PLAYER_0] & SHIP_FLAG_SHIELDS_UP)
              ' Kill Player 0
              --lives[PLAYER_0]
              ship_state[PLAYER_0] := SHIP_STATE_EXPLODING
              sfx_flags |= SFX_SND_EXPLODE   
              SFX_stop_ship_action_sounds(0)    
            if not (ship_flags[PLAYER_1] & SHIP_FLAG_SHIELDS_UP)
              ' Kill Player 1
              --lives[PLAYER_1]
              ship_state[PLAYER_1] := SHIP_STATE_EXPLODING
              sfx_flags |= SFX_SND_EXPLODE   
              SFX_stop_ship_action_sounds(1)    

      '==================================================================================================
      ' RENDERING SECTION 
      '==================================================================================================
      
      '//// INITIALE TEXT COLOR AND SIZE
      gr.textmode(1,1,5,3)
      gr.colorwidth(2,0)
      
      '//// DRAW DEBUGGING WATCH VARIABLES
      if DEBUG__SHOW_WATCH_VARIABLES
        'Draw fixed point watch variable
        Fixed_To_String(@fixed_pt_string_0, watchval_fixed )
        gr.text(PLAYER_1_STATS_X_POS, PLAYER_1_STATS_Y_POS - 20, @fixed_pt_string_0)
         
        'Draw integer watch variable
        Signed_Int_To_String(@int_string_0, watchval_int )
        gr.text(PLAYER_0_STATS_X_POS, PLAYER_0_STATS_Y_POS - 22, @int_string_0)
      
      '//// DRAW PLAYER SHIPS AND STATS
      repeat player_index from 0 to NUM_PLAYERS - 1

        '////---------------------
        '//// DRAW SHIPS
        '////---------------------
        ' extract whole parts of fixed point position to save time in parameters
        x := object_x[player_index] ~> 16
        y := object_y[player_index] ~> 16

        gr.colorwidth(2,0)
        if (ship_state[player_index] =< SHIP_STATE_EXPLODING) and (ship_state[player_index] > SHIP_STATE_DIE_DELAY)
          'Ship is in the SHIP_STATE_EXPLODING state, so draw explosion

          'Set j to a fixed point value between 0.0 and 1.0 reflecting the progress of the
          '   player explosion state.  Starts at 0.0 at beginning of explosion, and
          '   ends at 1.0 at the end of the explosion.
          j := $00010000 - (((ship_state[player_index] - SHIP_STATE_DIE_DELAY) << 16) / CONSTANT(SHIP_STATE_EXPLODING - SHIP_STATE_DIE_DELAY))

          'Scale j by 90 degrees and convert back to integer.  Now j will range from 0 to 90 degrees
          '   over the course of the explosion.
          j := (j * ANG_90) >> 16

          'Compute a fixed point value which ranges from 0.0 to the explosion radius over the course 
          '   of the explosion in a sinusoidal expansion (rapidly at first, then slowly at the end).
          '   This value will be used to scale the displayed distance of the explosion particles
          '   from the ship's center.
          j := SinCos(SIN, j) * EXPLOSION_RADIUS 

          'Loop over all the explosion particles 
          repeat i from 0 to NUM_EXPLOSION_PARTICLES - 1
            'Draw an explosion particle 
            gr.plot((explosion_dx_fxp[i] * j~>16)~>16 + x, (explosion_dy_fxp[i] * j~>16)~>16 + y)

        else
          if (ship_state[player_index] == SHIP_STATE_ALIVE)
            'Ship is alive, so draw it
            if (player_index == PLAYER_0)
              gr.vec(x, y, $0020, ship_angle[player_index], @player_ship0) 
            else
              gr.vec(x, y, $0020, ship_angle[player_index], @player_ship1)
                                                                                         
            'If shields are up, then show them
            if (ship_flags[player_index] & SHIP_FLAG_SHIELDS_UP)  
              gr.vec(x, y, $0020, shield_angle, @vectors_shield)

            'Draw thruster
            if ((ship_flags[player_index] & SHIP_FLAG_THRUSTING ) and (?random_var & $01))
              gr.colorwidth(1,0)
              'Offset the flare from ship center 
              if player_index
                'Player 1 
                dx := x - (SinCos(COS, ship_angle[PLAYER_1]) * P1_THRUSTER_DISTANCE) ~> 16
                dy := y - (SinCos(SIN, ship_angle[PLAYER_1]) * P1_THRUSTER_DISTANCE) ~> 16     
              else
                'Player 0
                dx := x - (SinCos(COS, ship_angle[PLAYER_0]) * P0_THRUSTER_DISTANCE) ~> 16
                dy := y - (SinCos(SIN, ship_angle[PLAYER_0]) * P0_THRUSTER_DISTANCE) ~> 16
              gr.plot(dx, dy)
              gr.line(dx - (thrust_dx[player_index] ~> 9), dy - (thrust_dy[player_index] ~> 9) )

        '////---------------------
        '//// DRAW PLAYER STATS 
        '////---------------------
        if player_index == 0
          x := PLAYER_0_STATS_X_POS
          y := PLAYER_0_STATS_Y_POS
        else
          x := PLAYER_1_STATS_X_POS
          y := PLAYER_1_STATS_Y_POS

        '//// DRAW FUEL REMAINING        
        if ship_fuel_fxp[player_index] and (game_options & OPTION_FUEL)
          gr.plot(x                                  , y + 7) 
          gr.line(x + ship_fuel_fxp[player_index]>>16, y + 7)
          gr.plot(x                                  , y + 8) 
          gr.line(x + ship_fuel_fxp[player_index]>>16, y + 8)          

        '//// DRAWSHIELD POWER REMAINING
        if ship_shield_power_fxp[player_index] and (game_options & OPTION_SHIELDS)
          j := ship_shield_power_fxp[player_index] >> 16
          repeat i from x to x + j step 2                            
            gr.plot(i, y + 10)
            gr.plot(i, y + 11)  

      '//// DRAW SHOTS
      'gr.colorwidth(2,2)
      repeat shot_index from 0 to NUM_SHOTS - 1
        if (shot_time_to_live[shot_index] <> 0)
          ' Draw 2x2 shot
          x := object_x[shot_index+OBJECT_PLAYER_0_SHOT_0] ~> 16
          y := object_y[shot_index+OBJECT_PLAYER_0_SHOT_0] ~> 16
          gr.plot(x  ,y  )
          gr.plot(x+1,y+1)
          gr.plot(x+1,y  )
          gr.plot(x  ,y+1)
          
      '//// DRAW SHIPS REMAINING
      gr.colorwidth(3,0)
      if (lives[PLAYER_0] <> 0)
        repeat i from 1 to lives[PLAYER_0]
          gr.vec(PLAYER_0_STATS_X_POS + (i - 1) << 3 + 4, PLAYER_0_STATS_Y_POS, $0012, $2000 >> 2, @player_ship0)
      if (lives[PLAYER_1] <> 0)
        repeat i from 1 to lives[PLAYER_1]
          gr.vec(PLAYER_1_STATS_X_POS + (i - 1) << 3 + 4, PLAYER_1_STATS_Y_POS, $0012, $2000 >> 2, @player_ship1)

      '//// DRAW BFG
      if bfg_flags & BFG_FLAG__PICKUP
          gr.vec(bfg_x, bfg_y, $0020, shield_angle, @vectors_bfg)

      '//// PROCESS AND RENDER BLACK HOLE
      if ((game_options & OPTION_BLACK_HOLE) <> 0)
        {
        '//// Old style black hole animation
        gr.colorwidth(1,0)
        length := 8
        repeat 5
          gr.plot(BLACK_HOLE_X,BLACK_HOLE_Y)
          gr.line(BLACK_HOLE_X + Rand_Range(-length,length), BLACK_HOLE_Y + Rand_Range(-length,length) )
        }
        gr.plot(0,0)
        j := 1
        repeat i from 0 to NUM_BLACK_HOLE_PARTICLES - 1
          if not(black_hole_particle_mask & j)
             'Particle does not exist.  Randomly choose whether to spawin it
             if Rand_Range(1,20) < 2
               'Spawn particle
               black_hole_particle_angle[i] := Rand_Range(ANG_0,ANG_360)
               black_hole_particle_radius[i] := (8 << 16)
               black_hole_particle_mask |= j
          else
             'Particle exists so render and update it
             gr.plot((SinCos(COS, black_hole_particle_angle[i]) * (black_hole_particle_radius[i] >> 16))~>16,  (SinCos(SIN, black_hole_particle_angle[i]) * (black_hole_particle_radius[i]>>16))~>16)
             'gr.plot((SinCos(COS, black_hole_particle_angle[i]) * 6)~>16,  (SinCos(SIN, black_hole_particle_angle[i]) * 6)~>16) 
             black_hole_particle_radius[i] -= $8000
             black_hole_particle_angle[i] += CONSTANT(ANG_1 * 3) * (9 - (black_hole_particle_radius[i]>>16))
             if(black_hole_particle_angle[i] > ANG_360)
               black_hole_particle_angle[i] -= ANG_360
               
             'If particle has reached the center
             if  black_hole_particle_radius[i] < 0
                'Extinguish particle
                black_hole_particle_mask &= !j
          j <<= 1


      
      '==================================================================================================
      ' PACING
      '==================================================================================================
      {
      'OLD Pacing algorithm
      if (curr_count>>1 < $7fffffff - CONSTANT(PACING_TICK>>1) )
        'No counter wrap
        if (curr_count>>1) + CONSTANT(PACING_TICK>>1 - PACING_MARGIN>>1) > (cnt>>1)
          'Wait for pacing
          waitcnt(curr_count + PACING_TICK)
        else
          if DEBUG__SHOW_PACING_OVERRUNS
            'Missed target frame rate.  Indicate with a large square dot in the corner, and continue on
            gr.plot(CONSTANT(SCREEN_WIDTH/2-1), CONSTANT(SCREEN_HEIGHT/2-1))
            gr.plot(CONSTANT(SCREEN_WIDTH/2-2), CONSTANT(SCREEN_HEIGHT/2-1))
            gr.plot(CONSTANT(SCREEN_WIDTH/2-1), CONSTANT(SCREEN_HEIGHT/2-2))
            gr.plot(CONSTANT(SCREEN_WIDTH/2-2), CONSTANT(SCREEN_HEIGHT/2-2))
      else
        if DEBUG__SHOW_PACING_OVERRUNS
          'Counter wrap
          gr.plot(-CONSTANT(SCREEN_WIDTH/2-1), CONSTANT(SCREEN_HEIGHT/2-1))
          gr.plot(-CONSTANT(SCREEN_WIDTH/2-2), CONSTANT(SCREEN_HEIGHT/2-1))
          gr.plot(-CONSTANT(SCREEN_WIDTH/2-1), CONSTANT(SCREEN_HEIGHT/2-2))
          gr.plot(-CONSTANT(SCREEN_WIDTH/2-2), CONSTANT(SCREEN_HEIGHT/2-2))
      }

      'NEW Pacing algorithm (DEBUG__SHOW_PACING_OVERRUNS not currently supported)
      if DEBUG__SHOW_PACING_LED
        OUTA [0] := 1 'Set debug LED  
      
      pacing_target := (curr_count + PACING_TICK) >> 2
      pacing_cnt := cnt >> 2
      if(pacing_target < CONSTANT($7fffffff >> 2))
        'No wrap
        j := pacing_target + CONSTANT($7fffffff >> 2)
        repeat while(pacing_cnt < pacing_target) or (pacing_cnt > j)
          pacing_cnt := cnt >> 2
      else
        'Wrap
        j := pacing_target - CONSTANT($7fffffff >> 2)
        repeat while(pacing_cnt < pacing_target) and (pacing_cnt > j)
          pacing_cnt := cnt >> 2

      'Clear debug LED
      OUTA [0] := 0 'Clear debug LED
      
      'copy bitmap to display
      gr.copy(onscreen_buffer)    
     
    ' END MAIN GAME LOOP REPEAT BLOCK //////////////////////////////////

  '----------------------------
  ' Game Over
  '----------------------------
  
  '#IF MUSIC_ENABLE
  hdmf.stop_song  'stop playing music
  '#ENDIF
  
  SFX_stop_ship_action_sounds(0)
  SFX_stop_ship_action_sounds(1)
  
  gr.textmode(4,1,5,3)
  gr.colorwidth(2,0)
  gr.text(-90, 30, @game_over_string) 
  gr.copy(onscreen_buffer)
  waitcnt(70_000_000 + cnt)
  
  '#IF MUSIC_ENABLE
  'hdmf.play_song(EEPROM_SONG_LOST_LOVE | hdmf#EEPROM_ASSET, hdmf#HDMF__LOOP)
  'hdmf.play_song(EEPROM_SONG_GENTLE_WIND | hdmf#EEPROM_ASSET, hdmf#HDMF__LOOP)
  hdmf.play_song(EEPROM_SONG_NORTH_WALL | hdmf#EEPROM_ASSET, hdmf#HDMF__LOOP) 
  '#ENDIF
  
  gr.textmode(1,1,5,3) 
  gr.text(-34, -35, @game_over_restart)
  gr.copy(onscreen_buffer)
  
  '//// WAIT FOR RESTART
  WaitForStartButton

  '#IF MUSIC_ENABLE
  hdmf.stop_song  'stop playing music
  '#ENDIF
    
' ////////////////////////////////////////////////////////////////////

PUB WaitForStartButton
  nes_buttons := 0
  'hang here until 'START' or '<SPACEBAR>' is pressed    
  repeat while not (nes_buttons & CONSTANT( NES_START | (NES_START << 8) )) and not (key.keystate(32)) and not((game_options & OPTION_MONKEY_IS_ALIVE) and (Rand_Range(1,100) == 1)) 
    '//// Read NES controller buttons
    nes_buttons := NES_Read_Gamepad

  'hang here until 'START' and '<SPACEBAR>' are released   
  repeat while (nes_buttons & CONSTANT( NES_START | (NES_START << 8) )) or (key.keystate(32)) and not((game_options & OPTION_MONKEY_IS_ALIVE) and (Rand_Range(1,100) == 1)) 
    '//// Read NES controller buttons
    nes_buttons := NES_Read_Gamepad


PUB Configure_Options | i, y, selected_option, string_ptr, num_options, menu_flags, key_repeat_ticks, current_option_mask, done

  selected_option := 0
  menu_flags      := 0
  done            := false

  '//// Clear screen
  gr.clear        ' Clear bitmap

  '#IF EEPROM_GRAPHICS_ENABLE
  ' Get option screen
  hdmf.EEPROM_Read(OFFSCREEN_BUFFER, EEPROM_SCREEN_OPTIONS, CONSTANT((SCREEN_WIDTH * SCREEN_HEIGHT)>>3))  
  repeat until hdmf.EEPROM_IsDone
  '#ENDIF

  'Start music
  '#IF MUSIC_ENABLE  
  hdmf.play_song(EEPROM_SONG_GOTHIC | hdmf#EEPROM_ASSET, hdmf#HDMF__LOOP)
  '#ENDIF

        
  repeat while not done

    ClearBox(SCREEN_WIDTH-70,32,SCREEN_WIDTH-32,97)
    
    '//// GET USER INPUT
    nes_buttons := NES_Read_Gamepad
    nes_buttons := ((nes_buttons & $ff00) >> 8) | nes_buttons  'Accept input from either gamepad  
      
    '//// Draw screen borders
    {
    gr.plot(-constant(SCREEN_WIDTH/2 -1),-constant(SCREEN_HEIGHT/2 -1))
    gr.line(constant(SCREEN_WIDTH/2 -1),-constant(SCREEN_HEIGHT/2 -1))
    gr.line(constant(SCREEN_WIDTH/2 -1),constant(SCREEN_HEIGHT/2 -1))
    gr.line(-constant(SCREEN_WIDTH/2 -1),constant(SCREEN_HEIGHT/2 -1))
    gr.line(-constant(SCREEN_WIDTH/2 -1),-constant(SCREEN_HEIGHT/2 -1)) 
    }

    '//// Draw titles
    gr.textmode(1,1,6,1)
    gr.colorwidth(2,0)
    '#IF !EEPROM_GRAPHICS_ENABLE
    { '<added by SCCuM preprocessor utility>
    gr.text(-20, 86, @title1_string)
    gr.text(-3, 73, @version_string)
    } '<added by SCCuM preprocessor utility>
    '#ENDIF
    gr.text(-110, -51, @title3_string)
    gr.text(-123, -64, @title4_string) 
    gr.text(-58, -87, @title_start_string) 
    

    '//// Draw options states
    string_ptr := @option_strings
    y := 55
    num_options := 0
    current_option_mask := $00000001
    repeat while (byte[string_ptr] <> $ff)

      '//// Draw option name
      gr.text(-60, y, string_ptr)
      repeat while byte[string_ptr] <> $0
          ++string_ptr
      ++string_ptr

       '//// Draw option setting
      if (game_options & current_option_mask)
        gr.text(60, y, @on_string)
      else
        gr.text(60, y, @off_string)

      '//// Toggle option, if seleted
      '     Note: 'num_options' at this point is really the current option.
      if (nes_buttons & NES_A) and (selected_option == num_options) and not(menu_flags & MENU_FLAG__A_HELD)
         game_options ^=  current_option_mask  'Toggle the current option
       
      '//// Advance to next line
      y -= 13
      ++num_options
      current_option_mask <<= 1
    
    '//// Point to current option
    y :=55 -(13 * selected_option)
    gr.vec(90, y, $0020, ANG_180, @player_ship0)

    '//// PROCESS MENU SELECTION, ON KEY DOWN EVENTS WITH KEY REPEATS
    if menu_flags & MENU_FLAG__UP_DOWN_HELD 
      ++key_repeat_ticks
      if not(nes_buttons & CONSTANT(NES_DOWN | NES_UP))
        menu_flags &= CONSTANT(!(MENU_FLAG__UP_DOWN_HELD | MENU_FLAG__REPEAT_GO_TIME_MET | MENU_FLAG__DO_REPEAT_CLICK))
      if not(menu_flags & MENU_FLAG__REPEAT_GO_TIME_MET) and (key_repeat_ticks => 8)
        menu_flags |= CONSTANT(MENU_FLAG__REPEAT_GO_TIME_MET | MENU_FLAG__DO_REPEAT_CLICK)
      if (menu_flags & MENU_FLAG__REPEAT_GO_TIME_MET) and (key_repeat_ticks => 2)
        menu_flags |= MENU_FLAG__DO_REPEAT_CLICK  
        
    if (not(menu_flags & MENU_FLAG__UP_DOWN_HELD)) or (menu_flags & MENU_FLAG__DO_REPEAT_CLICK)
      if nes_buttons & NES_UP  
        menu_flags |= MENU_FLAG__UP_DOWN_HELD
        if selected_option == 0
          selected_option := num_options - 1
        else
          --selected_option
      if nes_buttons & NES_DOWN  
        menu_flags |= MENU_FLAG__UP_DOWN_HELD  
        if selected_option == num_options - 1
          selected_option := 0
        else
          ++selected_option
      key_repeat_ticks := 0
      menu_flags &= !MENU_FLAG__DO_REPEAT_CLICK

    '//// PROCESS "START"
    if (nes_buttons & NES_START) or ((game_options & OPTION_MONKEY_IS_ALIVE) and (Rand_Range(1,100) == 1)) 
      done := true    

    '//// PROCESS BUTTON HELD FLAG
    if (nes_buttons & NES_A) 
       menu_flags |= MENU_FLAG__A_HELD
    else 
       menu_flags &= !MENU_FLAG__A_HELD

    '//// COPY BITMAP TO DISPLAY
    gr.copy(onscreen_buffer)

  'Stop playing music   
  '#IF MUSIC_ENABLE
  hdmf.stop_song
  '#ENDIF

PUB ClearBox (x1,y1,x2,y2) | x,y,ptr,mask,i
'' This is a temprary fill routine to fill a box area of the screen with black
'' Only here because my hacked 2 color graphics driver does not currently support
'' a black "draw" color.

  {
  repeat x from x1 to x2
     repeat y from y1 to y2
        ptr := OFFSCREEN_BUFFER + y * 4 + (((x & $ffe0) >> 5) * 4 * SCREEN_HEIGHT)
        mask :=  1 << (x & $1f)
        long[ptr] |= mask
  }
  
  'The proper version ran too slow.  This is just a hack to fill the required block quickly.
  ptr := OFFSCREEN_BUFFER + y1 * 4 + (((x1 & $ffe0) >> 5) * 4 * SCREEN_HEIGHT)
  repeat i from ptr to ptr + (y2-y1) << 2
    long[i] := 0
    long[i + constant(4 * SCREEN_HEIGHT)] := 0 
     
  

PUB Int_To_String(str, i) | t
   
  ' does an sprintf(str, "%05d", i); job
  str+=4
  repeat t from 0 to 4
    BYTE[str] := 48+(i // 10)
    i/=10
    str--

PUB Signed_Int_To_String(str, i) | t
   
  'Print the sign
  if (i<0)
    i := -i
    BYTE[str] := 45  ' ASCII '-'
  else
    BYTE[str] := 43  ' ASCII '+' 
    
  str+=5
  repeat t from 0 to 4
    BYTE[str] := 48+(i // 10)
    i/=10
    str--
    
PUB Fixed_To_String(str, fixed) | t , i
  ' does an sprintf(str, "%+05.04f", i); job

  'Print the sign
  if (fixed<0)
    fixed := -fixed
    BYTE[str] := 45  ' ASCII '-' 
  else
    BYTE[str] := 43  ' ASCII '+'     
    
  'Print the fractional portion
  str+=10
  i := ((fixed & $0000_FFFF) * 10000) ~>16 'Get the fractional part, and convert to 5 decimal digits of accuracy.
  repeat t from 0 to 3
    BYTE[str] := 48+(i // 10)
    i/=10
    str--
   
  'Print the decimal point
  BYTE[str] := 46  '46 = ASCII Decimal point
  str-- 
   
  'Print the decimal portion
  i:=fixed~>16
  repeat t from 0 to 4
    BYTE[str] := 48+(i // 10)
    i/=10
    str--

Pub Rand_Range(rstart, rend) : r_delta
  ' returns a random number from [rstart to rend] inclusive
  r_delta := rend - rstart + 1
  result := rstart + ((?random_var & $7FFFFFFF) // r_delta)
   
  return result

' ////////////////////////////////////////////////////////////////////

PUB SinCos(op, angle): xy               
  if (op==COS)
    angle+=$800   

  if angle & $1000 
    if angle & $800
      -angle
    angle |= $7000
    xy := -word[angle << 1]
  else
    if angle & $800
      -angle
    angle |= $7000
    xy := word[angle << 1]

' //////////////////////////////////////////////////////////////////////


PUB NES_Read_Gamepad : nes_bits   |       i

DIRA [17] := 1 ' output
DIRA [16] := 1 ' output
DIRA [19] := 0 ' input
DIRA [18] := 0 ' input

OUTA [16] := 0 ' JOY_CLK = 0
OUTA [17] := 0 ' JOY_SH/LDn = 0
OUTA [17] := 1 ' JOY_SH/LDn = 1
OUTA [17] := 0 ' JOY_SH/LDn = 0
nes_bits := 0
nes_bits := INA[19] | (INA[18] << 16)

repeat i from 0 to 14
  OUTA [16] := 1 ' JOY_CLK = 1
  OUTA [16] := 0 ' JOY_CLK = 0
  nes_bits := (nes_bits << 1)
  nes_bits := nes_bits | INA[19] | (INA[18] << 16)

nes_bits := !nes_bits
   
  ' //////////////////////////////////////////////////////////////////
  ' End NES Game Paddle Read
  ' //////////////////////////////////////////////////////////////////

'///////////////////////////////////////////////////////////////////////
' DATA SECTION /////////////////////////////////////////////////////////
'///////////////////////////////////////////////////////////////////////

VAR
   long sfx_flags

   word sfx_cnt_ship0_shot
   word sfx_cnt_ship1_shot
   byte sfx_cnt_shield
   byte shield_snd_ascending 

CON

   SFX_SND_SHIP0_SHOT_START = %00000000_00000001
   SFX_SND_SHIP0_SHOT       = %00000000_00000010  
   SFX_SND_SHIP1_SHOT_START = %00000000_00000100
   SFX_SND_SHIP1_SHOT       = %00000000_00001000
   SFX_SND_SHIP0_THRUST     = %00000000_00010000
   SFX_SND_SHIP1_THRUST     = %00000000_00100000  
   SFX_SND_SHIP0_SHIELD     = %00000000_01000000
   SFX_SND_SHIP1_SHIELD     = %00000000_10000000
   SFX_SND_EXPLODE          = %00000001_00000000
   SFX_SND_SHIELD_SUSTATIN  = %00000010_00000000

   SFX_SHIELD_CNT_CYCLE     = 255
   
   
PUB SFX_manager

  sfx_flags := 0
  sfx_cnt_shield := 0
  shield_snd_ascending := true
  
  repeat

    '-------------------
    ' SHOT SOUNDS
    '-------------------
    if (sfx_flags & SFX_SND_SHIP0_SHOT_START) <> 0
       'Start ship 0 shot sound  
       sfx_cnt_ship0_shot := 0
       sfx_flags &= !SFX_SND_SHIP0_SHOT_START
       sfx_flags |= SFX_SND_SHIP0_SHOT

    if (sfx_flags & SFX_SND_SHIP0_SHOT) <> 0
      'Play ship 0 shot sound    
      hdmf.PlaySoundFM(5, hdmf#SHAPE_SAWTOOTH, $0400 - (sfx_cnt_ship0_shot * 2) , CONSTANT( hdmf#SAMPLE_RATE / 60 ), 128, $2468_ACEF)
      if (++sfx_cnt_ship0_shot == 511)
        sfx_flags &= !SFX_SND_SHIP0_SHOT

    if (sfx_flags & SFX_SND_SHIP1_SHOT_START) <> 0
      'Start ship 1 shot sound    
      sfx_cnt_ship1_shot := 0
      sfx_flags &= !SFX_SND_SHIP1_SHOT_START
      sfx_flags |= SFX_SND_SHIP1_SHOT

    if (sfx_flags & SFX_SND_SHIP1_SHOT) <> 0
      'Stop ship 1 shot sound   
      hdmf.PlaySoundFM(4, hdmf#SHAPE_SAWTOOTH, $0520 - (sfx_cnt_ship1_shot * 2) , CONSTANT( hdmf#SAMPLE_RATE / 60 ), 128, $2468_ACEF)
      if (++sfx_cnt_ship1_shot == 511)
        sfx_flags &= !SFX_SND_SHIP1_SHOT

    '-------------------
    ' THRUST SOUNDS
    '-------------------
    if (sfx_flags & (SFX_SND_SHIP0_THRUST | SFX_SND_SHIP1_THRUST)) <> 0
      'Play thrust sound   
      hdmf.PlaySoundFM(3, hdmf#SHAPE_NOISE, $080 + Rand_Range(0,$030) , CONSTANT( hdmf#SAMPLE_RATE / 2 ), 110, $2468_ACEF)

    '-------------------
    ' SHIELD SOUNDS
    '-------------------
    if (sfx_flags & (SFX_SND_SHIP0_SHIELD | SFX_SND_SHIP1_SHIELD | SFX_SND_SHIELD_SUSTATIN)) <> 0
      'play shield sound
      hdmf.PlaySoundFM(2, hdmf#SHAPE_SINE, $255 + sfx_cnt_shield * 2, CONSTANT( hdmf#SAMPLE_RATE / 8 ), 80, $2468_ACEF)

      'set sustain, so that shield sound always ends and bottom of pitch warble cycle
      sfx_flags |= SFX_SND_SHIELD_SUSTATIN
      
      'Model shield sound warble as a triangle shaped pitch change
      if shield_snd_ascending
        ++sfx_cnt_shield
        if(sfx_cnt_shield => SFX_SHIELD_CNT_CYCLE)
          shield_snd_ascending := false
      else
        -- sfx_cnt_shield
        if(sfx_cnt_shield == 0)
          shield_snd_ascending := true
          'End sustain.  Shield sound will stop if shields are currently inactive. (This forces
          'the shield sound to always end at the bottom of the pitch warble cycle).
          sfx_flags &= !SFX_SND_SHIELD_SUSTATIN 
    else
      sfx_cnt_shield := 0
      shield_snd_ascending := true       

    '-------------------
    ' EXPLOSION SOUNDS
    '-------------------      
    if (sfx_flags & SFX_SND_EXPLODE) <> 0
      'Play explosion sound   
      hdmf.PlaySoundFM(3, hdmf#SHAPE_NOISE, $100 , CONSTANT( hdmf#SAMPLE_RATE / 1 ), 110, $2468_ACEF)
      sfx_flags &= !SFX_SND_EXPLODE


      


PUB SFX_stop_ship_action_sounds (player)

  if player == 0
    sfx_flags &= !(SFX_SND_SHIP0_THRUST | SFX_SND_SHIP0_SHIELD)
  else
    sfx_flags &= !(SFX_SND_SHIP1_THRUST | SFX_SND_SHIP1_SHIELD)  


DAT


' BLACK HOLES ///////////////////////////////////////////////////////////
{
' black hole initialization
black_x                  long 20,  80, -90 ' x position of each black hole
black_y                  long 50, -60, -10 ' y position of each black hole
black_gravity            long  8,  4,  2   ' single value that is "lumped" parameter that relates to the final "pull"
                                           ' of the black hole relative to the ship's constant mass
}

' STRING STORAGE //////////////////////////////////////////////////////
{
score_string            byte    "Score",0               'text
hiscore_string          byte    "High",0                'text
ships_string            byte    "Ships",0               'text
}

' TV PARAMETERS FOR DRIVER /////////////////////////////////////////////

tvparams                long    0               'status
                        long    1               'enable
                        long    %001_0101       'pins
                        long    %0000           'mode
                        long    0               'screen
                        long    0               'colors
                        long    x_tiles         'hc
                        long    y_tiles         'vc
                        long    10              'hx timing stretch
                        long    1               'vx
                        long    0               'ho
                        long    0               'vo
                        long    55_250_000      'broadcast
                        long    0               'auralcog

' POLYGON OBJECTS ///////////////////////////////////////////////////////
  
player_ship0            word    VECTOR_MOVE + ANG_0             
                        word    50

                        word    VECTOR_DRAW + ANG_120+ANG_10
                        word    50

                        word    VECTOR_DRAW + ANG_180
                        word    10

                        word    VECTOR_DRAW + ANG_240-ANG_10  
                        word    50

                        word    VECTOR_DRAW + ANG_0
                        word    50

                        word    VECTOR_END
                        

player_ship1            word    VECTOR_MOVE + ANG_0             
                        word    50

                        word    VECTOR_DRAW + ANG_90
                        word    10

                        word    VECTOR_DRAW + ANG_90 + (ANG_1 * 30)
                        word    40

                        word    VECTOR_DRAW + ANG_180
                        word    50

                        word    VECTOR_DRAW + ANG_270 - (ANG_1 * 30)
                        word    40

                        word    VECTOR_DRAW + ANG_270
                        word    10

                        word    VECTOR_DRAW + ANG_0
                        word    50

                        word    VECTOR_END


vectors_shield          word    VECTOR_MOVE + ANG_0                         ' Octagonal shield          
                        word    70

                        word    VECTOR_DRAW + ANG_45
                        word    70
                        
                        word    VECTOR_MOVE + ANG_90
                        word    70

                        word    VECTOR_DRAW + ANG_90+ANG_45 
                        word    70

                        word    VECTOR_MOVE + ANG_180  
                        word    70

                        word    VECTOR_DRAW + ANG_180+ANG_45   
                        word    70

                        word    VECTOR_MOVE + ANG_270  
                        word    70

                        word    VECTOR_DRAW + ANG_270+ANG_45   
                        word    70

                        word    VECTOR_MOVE + ANG_0             
                        word    70

                        word    0

vectors_bfg             word    VECTOR_MOVE + ANG_0 
                        word    BFG_SPOKE_SIZE

                        word    VECTOR_DRAW + (ANG_1 * 60)
                        word    BFG_HUB_SIZE

                        word    VECTOR_DRAW + (ANG_1 * 120)
                        word    BFG_SPOKE_SIZE

                        word    VECTOR_DRAW + (ANG_1 * 180)
                        word    BFG_HUB_SIZE

                        word    VECTOR_DRAW + (ANG_1 * 240)
                        word    BFG_SPOKE_SIZE

                        word    VECTOR_DRAW + (ANG_1 * 300)
                        word    BFG_HUB_SIZE

                        word    VECTOR_DRAW + ANG_0
                        word    BFG_SPOKE_SIZE

                        word    0                    
                        

fixed_pt_string_0        byte    "-00000.0000",0    'text
int_string_0             byte    "-00000",0         'text
game_over_string         byte    "GAME OVER",0      'text
game_over_restart        byte    "PRESS <START>",0  'text
debug_string             byte    "DEBUG",0          'text

title1_string            byte    "SPACEWAR!",0                                   'text
version_string            byte    "2.5",0                                       'text
title3_string            byte    "WRITTEN BY ERIC MOYER. FOR THE LATEST",0       'text
title4_string            byte    "VERSION SEARCH HTTP://FORUMS.PARALLAX.COM",0   'text
title_start_string       byte    "PRESS <START> TO BEGIN",0                      'text 

options_string           byte    "OPTIONS",0        'text

option_strings           byte    "BLACK HOLE",0     'text
                         byte    "SHOT GRAVITY",0   'text
                         byte    "INERTIAL SHOTS",0 'text
                         byte    "SHIELDS",0        'text
                         byte    "FUEL",0           'text
                         byte    $ff                'end of option string list  
                         byte    "BFG",0            'text
                         
                         
                         {
                         byte    "HYPERSPACE",0     'text
                         byte    "PARTIAL DAMAGE",0 'text
                         byte    $ff                'end of option string list
                         }
                        
on_string                byte    "ON",0              'text
off_string               byte    "OFF",0             'text

'#IF MUSIC_ENABLE 
_song_mario
        byte    $02, $EF, $AD, $79, $35          'Instrument 0
        byte    $FF                              'End of Instrument list
        byte    $00, $00, $49, $0D, $0F
        byte    $00, $20, $93, $0D, $0F
        byte    $00, $42, $93, $0D, $0F
        byte    $10, $61, $72, $0D, $0F
        byte    $00, $80, $93, $0D, $0F
        byte    $00, $A0, $49, $0D, $0F
        byte    $00, $02, $93, $0D, $0F
        byte    $1F, $20, $49, $0D, $0F
        byte    $00, $41, $72, $0D, $0F
        byte    $00, $02, $93, $0D, $0F
        byte    $00, $60, $93, $0D, $0F
        byte    $1F, $81, $72, $0D, $0F
        byte    $00, $A0, $93, $0D, $0F
        byte    $00, $00, $49, $0D, $0F
        byte    $00, $22, $0B, $0D, $0F
        byte    $10, $41, $72, $0D, $0F
        byte    $00, $60, $49, $0D, $0F
        byte    $00, $00, $93, $0D, $0F
        byte    $00, $22, $93, $0D, $0F
        byte    $1F, $83, $10, $0D, $10
        byte    $00, $A1, $EE, $0D, $10
        byte    $00, $01, $88, $0D, $10
        byte    $00, $20, $C4, $0D, $10
        byte    $00, $40, $62, $0D, $10
        byte    $3F, $61, $88, $0D, $10
        byte    $00, $00, $C4, $0D, $10
        byte    $00, $20, $62, $0D, $10
        byte    $3E, $41, $4A, $0D, $0F
        byte    $00, $80, $C4, $0D, $0F
        byte    $00, $A0, $83, $0D, $0F
        byte    $00, $02, $0B, $0D, $0F
        byte    $2F, $21, $06, $0D, $0F
        byte    $00, $60, $A5, $0D, $0F
        byte    $00, $00, $83, $0D, $0F
        byte    $00, $41, $88, $0D, $0F
        byte    $2F, $80, $83, $0D, $0F
        byte    $00, $A1, $4A, $0D, $0F
        byte    $00, $00, $C4, $0D, $0F
        byte    $2F, $21, $06, $0D, $0F
        byte    $00, $41, $B8, $0D, $0F
        byte    $00, $60, $AF, $0D, $0F
        byte    $1F, $01, $EE, $0D, $0F
        byte    $00, $81, $26, $0D, $0F
        byte    $00, $A0, $C4, $0D, $0F
        byte    $1F, $21, $D2, $0D, $0F
        byte    $00, $41, $15, $0D, $0F
        byte    $00, $60, $B9, $0D, $0F
        byte    $10, $01, $B8, $0D, $0F
        byte    $00, $81, $06, $0D, $0F
        byte    $00, $A0, $AF, $0D, $0F
        byte    $1F, $21, $06, $0D, $14
        byte    $00, $41, $88, $0D, $14
        byte    $00, $60, $A5, $0D, $14
        byte    $15, $01, $88, $0D, $14
        byte    $00, $81, $06, $0D, $14
        byte    $00, $A0, $C4, $0D, $14
        byte    $00, $22, $93, $0D, $14
        byte    $15, $41, $EE, $0D, $14
        byte    $00, $61, $06, $0D, $14
        byte    $00, $03, $10, $0D, $14
        byte    $00, $21, $4A, $0D, $14
        byte    $14, $81, $5D, $0D, $0F
        byte    $00, $A0, $DC, $0D, $0F
        byte    $00, $03, $70, $0D, $0F
        byte    $00, $22, $0B, $0D, $0F
        byte    $20, $41, $B8, $0D, $0F
        byte    $00, $61, $26, $0D, $0F
        byte    $00, $02, $BA, $0D, $0F
        byte    $0F, $21, $EE, $0D, $1F
        byte    $00, $83, $10, $0D, $1F
        byte    $00, $A1, $4A, $0D, $1F
        byte    $20, $01, $B8, $0D, $0F
        byte    $00, $42, $93, $0D, $0F
        byte    $00, $60, $DC, $0D, $0F
        byte    $00, $21, $06, $0D, $0F
        byte    $1F, $82, $0B, $0D, $0F
        byte    $00, $A1, $4A, $0D, $0F
        byte    $00, $00, $DC, $0D, $0F
        byte    $10, $22, $4B, $0D, $0F
        byte    $00, $41, $5D, $0D, $0F
        byte    $00, $60, $F7, $0D, $0F
        byte    $0F, $01, $26, $0D, $0F
        byte    $00, $81, $EE, $0D, $0F
        byte    $00, $A0, $C4, $0D, $0F
        byte    $2F, $22, $0B, $0D, $0F
        byte    $00, $41, $4A, $0D, $0F
        byte    $00, $60, $C4, $0D, $0F
        byte    $00, $00, $83, $0D, $0F
        byte    $2F, $81, $06, $0D, $0F
        byte    $00, $A1, $88, $0D, $0F
        byte    $00, $00, $A5, $0D, $0F
        byte    $00, $20, $83, $0D, $0F
        byte    $2F, $41, $4A, $0D, $0F
        byte    $00, $60, $C4, $0D, $0F
        byte    $00, $00, $83, $0D, $0F
        byte    $2F, $21, $B8, $0D, $0F
        byte    $00, $81, $06, $0D, $0F
        byte    $00, $A0, $AF, $0D, $0F
        byte    $1F, $01, $26, $0D, $0F
        byte    $ff
'#ENDIF