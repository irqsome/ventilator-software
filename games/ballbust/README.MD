Ball Buster
===========

## Video
[![](http://img.youtube.com/vi/EkLacbXkd_k/0.jpg)](http://www.youtube.com/watch?v=EkLacbXkd_k "Video")

## Info
- Type: Game
- Genre: ?
- Author(s): JT Cook
- First release: 2006
- Improved version: No
- Players: 1
- Special requirements: None
- Video formats: NTSC
- Inputs: Gamepad, Keyboard, Mouse (not yet)

## Description
A simple _Breakout_ clone. Not a lot to write about this one. Graphics are quite nice. (Altough your TV may not like them, COP is strange like that)

## Controls
|Gamepad|Keyboard|Mouse|Action|
|-------|--------|-----|------|
|Left/Right|Left/Right|X Movement|Move paddle|
|B|Space|Left click|Serve|

## TODOs
- ~~Implement real SNES mouse driver~~ (done!)
- Fix COP_drv's TV compatibility problems
