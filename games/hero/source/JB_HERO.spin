''****************************************
''*  JB_Hero for Single Propeller boards *
''*  (C) 2009 Jim Bagley.                *
''****************************************

CON

{ Hybrid Settings
_clkmode = xtal1 + pll16x
_xinfreq = 6_000_000
_stack = (100) >> 2   'accomodate display memory and stack
PINS=%011_0000
COGS = 3
KEYSPIN  = 12 
'' gamepad pin configuration (adjust for your setup)
  JOY_CLK = 3
  JOY_LCH = 4
  JOY_DATAOUT0 = 5
  JOY_DATAOUT1 = 6

  ' NES bit encodings for NES gamepad 0
  NES0_RIGHT    = %00000000_00001000
  NES0_LEFT     = %00000000_00000100
  NES0_DOWN     = %00000000_00000010
  NES0_UP       = %00000000_00000001
  NES0_START    = %00000000_00100000
  NES0_SELECT   = %00000000_00100000
  NES0_B        = %00000000_10000000
  NES0_A        = %00000000_10000000    

'-----------------------------------   
   AUDIO_L = 0
   AUDIO_R = 7
'-----------------------------------   
'}

'{ Proto/Demoboard Settings
_clkmode = xtal1 + pll16x
_xinfreq = 5_000_000
_stack = (100) >> 2   'accomodate display memory and stack
PINS=%001_0101
COGS = 4
KEY_DATA   = 8
KEY_CLOCK  = 9
'' gamepad pin configuration (adjust for your setup)
  JOY_CLK = 16                                                       
  JOY_LCH = 17
  JOY_DATAOUT0 = 19
  JOY_DATAOUT1 = 18

  ' NES bit encodings for NES gamepad 0
  NES0_RIGHT    = %00000000_00000001
  NES0_LEFT     = %00000000_00000010
  NES0_DOWN     = %00000000_00000100
  NES0_UP       = %00000000_00001000
  NES0_START    = %00000000_00010000
  NES0_SELECT   = %00000000_00100000
  NES0_B        = %00000000_01000000
  NES0_A        = %00000000_10000000    
'-----------------------------------   
   AUDIO_L = 11
   AUDIO_R = 10
'-----------------------------------   
'}

{ Hydra Settings
_clkmode = xtal1 + pll8x
_xinfreq = 10_000_000
_stack = (100) >> 2   'accomodate display memory and stack
PINS=%011_0000
COGS = 4
KEYSPIN  = 13
'' gamepad pin configuration (adjust for your setup)
  JOY_CLK = 3
  JOY_LCH = 4
  JOY_DATAOUT0 = 5
  JOY_DATAOUT1 = 6

  ' NES bit encodings for NES gamepad 0
  NES0_RIGHT    = %00000000_00000001
  NES0_LEFT     = %00000000_00000010
  NES0_DOWN     = %00000000_00000100
  NES0_UP       = %00000000_00001000
  NES0_START    = %00000000_00010000
  NES0_SELECT   = %00000000_00100000
  NES0_B        = %00000000_01000000
  NES0_A        = %00000000_10000000    
'-----------------------------------   
   AUDIO_L = 7
   AUDIO_R = 0
'-----------------------------------   
'}

PAL=%0001
NTSC=%0000
INTERLACED=%0010

mode = NTSC
modepins = PINS
myhx = 17+(mode&1)*4

SPRX = 0
SPRY = 1
SPRP = 2
SPRC = 3

MAX_SPRITES=16

WATERLINE=123*8

MAX_BADDIES=4

BADX = 0
BADY = 1
BADP = 2
BADC = 3
BADTYP = 4
BADACT = 5
BADCNT = 6
BADHGT = 7

BADLEN = 8

MANL = $c0
MANR = $c2
SPID = $c4
ABAT = $c5
LGTL = $c6
LGTR = $c7
STAR = $c8
SNAK = $ca

OBJ
  key   : "Keyboard"
  tv    : "JB_tv_030"
  rend  : "JB_drv_030"
  sound : "SIDcog_Lite"

VAR
  long  frame
  
  long  opad,dpad

  byte  herox,heroy,herot,herop,herog
  byte  bombx,bomby,bombt
  byte  oherox,oheroy,levelnumber,roomnumber
  byte  powerbar,powerto,powertime,NumBadSprites
  byte  bullx,bully,bullt,bulld
  byte  bonux,bonuy,bonut,bonuc
  byte  numbombs,numlives,score[6]

  byte  scrrendbuf[COGS*256]
  
  byte  baddy_data[MAX_BADDIES*BADLEN]
  byte  numbaddies,stopair,pad1,pad2
  byte  badsprs[MAX_BADDIES]
    
PUB Main | i

'-----------------------------------
'init sound drivers
  sound.start(AUDIO_R, AUDIO_L)
  repeat i from 0 to 24
    sound.setRegister(i,0)
  sound.setVolume(15)
  sound.setPWM(2,2048) 
'-----------------------------------
'init tv and render drivers
  scanbufptr:=@scrrendbuf
  rend.start(COGS,@tvparams)
  tv.start(@tvparams)
'-----------------------------------
'init keyboard driver
  key.start(KEY_DATA,KEY_CLOCK)
'-----------------------------------
'main loop
  repeat i from 0 to 5
    score[i]:=" "
  score[5]:="0"
  reset_level(0)
  repeat
    do_menus
    play_game

PUB addscore(addptr) | i,c
  c:=0
  repeat i from 5 to 0
    if (byte[addptr][i]>"0") or (c==1)
      if score[i]==" "
        score[i]:="0"
      score[i]+=(byte[addptr][i]-"0")+c
      c:=0
      if score[i]>"9"
        score[i]-=10
        c:=1
    scrmap[CONSTANT((21-16)*20+7)+i]:=score[i]
        
PUB do_menus | done
  stopair:=1
  done:=0
  repeat
    Draw_All_Stuff

    if (frame&127)==0
      show_level
    if (frame&127)==64
      show_score
    if dpad&NES0_SELECT
      tvparams[3] ^= PAL
    
  until dpad&NES0_A  
     
PUB play_game | i,ptr,j
  frame:=0
  numlives:=3
  numbombs:=6
  show_bombs
  show_lives
  repeat i from 0 to 5
    score[i]:=" "
  score[5]:="0"
  reset_level(0)
  bring_hero_on
  repeat
    Draw_All_Stuff

    if key.keystate("1")
      getroom(0,0,0)
    if key.keystate("2")
      getroom(0,1,0)
    if key.keystate("3")
      getroom(1,0,0)     
    if key.keystate("4")
      getroom(1,1,0)
    if key.keystate("5")
      getroom(1,2,0)
    if key.keystate("6")
      getroom(1,3,0)
    if key.keystate("7")
      getroom(2,0,0)
    if key.keystate("8")
      getroom(2,1,0)
    if key.keystate("9")
      getroom(2,2,0)     
    if key.keystate("0")
      getroom(2,3,0)
    if key.keystate("-")
      getroom(levelnumber-1,0,0)
    if key.keystate("=")
      getroom(levelnumber+1,0,0)

    oherox:=herox
    oheroy:=heroy
    if opad & NES0_LEFT
      herox--
      herot&=$fe
    if opad & NES0_RIGHT
      herox++
      herot|=1
    if check_collision(herox,heroy+1,23)
      herox:=oherox
      opad&=-1-(NES0_LEFT+NES0_RIGHT)         
    if opad & NES0_UP
      if herog==CONSTANT(255-10)
        heroy-=2
      else
        herog--
    else
      if herog==10
        heroy+=2
      else
        herog++
    if check_collision(herox,heroy+1,23)
      heroy:=oheroy
    if check_collision(herox,heroy+24,1)
      if herog<128
        herog:=0
      herot:=(herot&1)+10
      if opad & (NES0_LEFT+NES0_RIGHT)
        if herot&1
          herot:=herot&1+((((herox>>1)^3)&3)<<1)
        else
          herot:=herot&1+(((herox>>1)&3)<<1)
' Running
'-----------------------------------   
        ifnot frame&3   
          sound.play(1,10000-((frame&12)<<7),sound#NOISE,0,1,0,2)
        ifnot (frame-1)&3
          sound.noteOff(1)
'----------------------------------- 
      else
         sound.noteOff(1) 
        if dpad & NES0_DOWN and numbombs
          if (bombt==0) and ((byte[@baddy_data][3]<$c0) or (byte[@baddy_data][3]>$c3))
            bombx:=herox-2+(herot&1)
            bomby:=heroy+8
            bombt:=64
            numbombs--
            show_bombs
            
    else
' Flying   
'----------------------------------- 
      ifnot frame&3
        sound.play(1,2100-((frame&8)<<6),sound#NOISE,0,3,5,6)   
      ifnot (frame-1)&7
        sound.noteOff(1)  

'----------------------------------- 
      herot:=herot&1

    if bullt
      bullt--
      DoBullToBaddyCollision
      bullx+=bulld

' Fire weapon
'-----------------------------------
      sound.play(2,5000+(3700>>((frame)&3)),sound#SQUARE,0,0,5,0)   
'-----------------------------------   
    else          
      if opad & NES0_A
        sound.play(2,5000+(3700>>((frame)&3)),sound#SQUARE,0,0,5,0) 
        bullx:=herox+4
        bully:=heroy+1
        bullt:=4
        bulld:=4
        if not(herot&1)
          bullx-=10
          bulld:=-4
      else
'----------------------------------- 
        sound.noteOff(2)
'-----------------------------------
              
    if (byte[@baddy_data][3]=>$c0) and (byte[@baddy_data][3]=<$c3)
      if collision(herox,heroy,8,24,byte[@baddy_data][0],byte[@baddy_data][1]+4,8,12)
        byte[@baddy_data][3]|=1
        byte[@spritesbuf][4*4+3]|=1
        byte[@spritesbuf][5*4+3]|=1
        powerto:=0
'-----------------------------------         
        soundOff
'----------------------------------- 
        repeat while powerbar>0
          Draw_All_Stuff
'----------------------------------- 
          if(powerbar&4)
            sound.play(2,5500,sound#SQUARE,0,2,0,2)
          else
            sound.noteOff(2)
          addscore(@add40)
        repeat while numbombs
          sound.noteOff(0)   
          sound.play(0,2000,sound#NOISE,0,9,0,3)
          addscore(@add50)
          numbombs--
          show_bombs
          repeat 20
            Draw_All_Stuff
'----------------------------------- 
        reset_level(levelnumber+1)
        Bring_Hero_On

    ptr:=@spritesbuf
    if NumBadSprites
      repeat i from 1 to NumBadSprites
        if (byte[ptr][3]=>SPID) and (byte[ptr][3]<>LGTR) and (byte[ptr][3]<>LGTL)
          if collision(herox,heroy,6,22,byte[ptr][0],byte[ptr][1],6,6)
'-----------------------------------  
            soundOff
            sound.play(2, 0, sound#Square, 0, 12, 0, 5)
'-----------------------------------  

            repeat 100
'-----------------------------------  
              sound.setFreq(2,7500+((frame&2)<<12))
'-----------------------------------
              VSync
              Nes_Read_Gamepad
              LONG[@SpriteSet[$8c<<5]]:=LONG[@SpriteSet[$6c<<5+(((frame>>1)&3)<<2)]]
              RippleWater
              Do_Copyright_Anim
              Draw_PowerBar
'-----------------------------------  
            soundOff
'-----------------------------------  
            byte[ptr][1]:=0
            remove_baddy_from_sprite(i)
            numlives--
            Show_Lives
            if numlives
              Bring_Hero_On
            else
              return
        else
          if (byte[ptr][3]=>SPID)
' turn the lights off now          
        ptr+=4
                  
    if heroy>$72
      getroom(levelnumber,roomnumber+1,0)
      heroy:=2
    if heroy<2
      if roomnumber
        getroom(levelnumber,roomnumber-1,0)
        heroy:=$72
      else
        heroy:=2
    if herox<$08
      getroom(levelnumber,roomnumber-~levExitRightDir[levelnumber],0)
      herox:=$98
    if herox>$98
      getroom(levelnumber,roomnumber+~levExitRightDir[levelnumber],0)
      herox:=$08

PUB soundOff
  sound.noteOff(0)
  sound.noteOff(1)
  sound.noteOff(2)
  
PUB Remove_Baddy_From_Sprite(sprnum) | i
  if numbaddies
    repeat i from 0 to numbaddies-1
      if badsprs[i]=>sprnum
        baddy_data[i*8+1]:=0
        baddy_data[i*8+3]:=0
        return
   
PUB Bring_Hero_On | t
  stopair:=1
  show_level
  t:=heroy
  heroy:=2
  herot:=(herot&1)+10
  repeat while heroy<t 
    Draw_All_Stuff 
    ifnot frame&3
      sound.play(1,2100-((frame&8)<<6),sound#NOISE,0,3,5,6)   
    ifnot (frame-1)&7
      sound.noteOff(1)
    heroy++
  show_lives
  if numbombs==0
    numbombs:=6
    show_bombs
  show_score
  stopair:=0

PUB show_level | i
  scrmap[CONSTANT((21-16)*20+7+0)]:=$70
  scrmap[CONSTANT((21-16)*20+7+1)]:=$71
  scrmap[CONSTANT((21-16)*20+7+2)]:=$72
  scrmap[CONSTANT((21-16)*20+7+3)]:=$73
  i:=levelnumber+1
  if i<10
    scrmap[CONSTANT((21-16)*20+7+4)]:=$74
  else
    scrmap[CONSTANT((21-16)*20+7+4)]:=$76 + i/10
  scrmap[CONSTANT((21-16)*20+7+5)]:=$76 + i//10
  
PUB show_score | i
  repeat i from 0 to 5
    scrmap[CONSTANT((21-16)*20+7)+i]:=score[i]

PUB show_lives | i
  repeat i from 0 to 5
    if i<numlives
      scrmap[CONSTANT((17-16)*20+12)-i]:=$19
      scrmap[CONSTANT((18-16)*20+12)-i]:=$1a
    else
      scrmap[CONSTANT((17-16)*20+12)-i]:=" "
      scrmap[CONSTANT((18-16)*20+12)-i]:=" "

PUB show_bombs | i
  repeat i from 0 to 5
    if i<numbombs
      scrmap[CONSTANT((19-16)*20+12)-i]:=$1b
      scrmap[CONSTANT((20-16)*20+12)-i]:=$1c
    else      
      scrmap[CONSTANT((19-16)*20+12)-i]:=" "
      scrmap[CONSTANT((20-16)*20+12)-i]:=" "
      
PUB Draw_All_Stuff | i, u
    VSync
    longmove(@sprites,@spritesbuf,MAX_SPRITES)
    ResetSprites
    AddSprite(herox-((herot&1)*3),heroy    ,0,$8c)
    AddSprite(herox              ,heroy    ,0,$60+herot)
    AddSprite(herox              ,heroy+$08,0,$80+herot)
    AddSprite(herox              ,heroy+$10,0,$a0+herot)
    Draw_Baddies

    NumBadSprites:=(sprite_ptr-@spritesbuf)/4
    
    if bullt
      AddSprite(bullx ,bully ,0,$6d)
    if bonut
      AddSprite(bonux ,bonuy ,0,bonuc)
      bonut--

    if bombt
'Explosion
'----------------------------------- 
      if bombt==16
        sound.noteOff(0)   
        sound.play(0,2000,sound#NOISE,0,9,0,9)
'----------------------------------- 
      if bombt<17
        AddSprite(bombx,bomby  ,0,bombt>>2&1+$ce)
        AddSprite(bombx,bomby+8,0,bombt>>2&1+$ee)
        if(bombt&1)
          repeat i from 0 to 122
            if byte[@displist+i<<3+5]<8
              byte[@displist+i<<3+5]^=5
      else
'Fuse
'----------------------------------- 
        sound.play(0,((frame&3)<<12)+(frame<<11),sound#NOISE,8,8,4,0)
 '----------------------------------- 
        AddSprite(bombx,bomby  ,0,bombt>>2&1+$cc)
        AddSprite(bombx,bomby+8,0,bombt>>2&1+$ec)
      bombt--
      if bombt==0
'-----------------------------------
        sound.noteOff(0)
'-----------------------------------
        if (check_collision(bombx+8,bomby,1)<>0) and (check_collision(bombx+12,bomby,1)<>0) and (check_collision(bombx+20,bomby,1)==0)
          getroom(levelnumber,roomnumber,1)
          addscore(@add75)          
          addbonus(bombx,bomby,$6f)
        if (check_collision(bombx-4,bomby,1)<>0) and (check_collision(bombx-8,bomby,1)<>0) and (check_collision(bombx-16,bomby,1)==0)
          getroom(levelnumber,roomnumber,1)          
          addscore(@add75)          
          addbonus(bombx,bomby,$6f)

    Nes_Read_Gamepad

    LONG[@SpriteSet[$8c<<5]]:=LONG[@SpriteSet[$6c<<5+(((frame>>1)&3)<<2)]]
    RippleWater
    Do_Copyright_Anim
    Draw_PowerBar


PUB reset_level(level)
  herox:=$30
  heroy:=$40
  bullt:=0
  levelnumber:=level
  roomnumber:=0
  bombt:=0
  bonut:=0
  powerbar:=0
  powerto:=80
  powertime:=0
  getroom(level,0,0)
  herog:=10
  numbombs:=6
  show_bombs
  show_lives

PUB Draw_PowerBar | i,j,s
  if powerbar<powerto
    powerbar++
'----------------------------------- 
    sound.play(0,(powerbar<<5)+500,sound#NOISE,4,4,7,8)
    if powerbar>73
      sound.noteOff(0)
'----------------------------------- 
  elseif powerbar>powerto
    powerbar--

  s:=@scrmap[constant((16-16)*20+6)]
    
  j:=powerbar
  repeat i from 0 to 9
    if j>7
      word[s]:=12
    elseif j<0   
      word[s]:=4
    else  
      word[s]:=4+j
    s+=2
    j-=8

  if stopair==0
    if powertime
      powertime--
    else
      powertime:=60
      if powerto
        powerto--
    
PUB getroom(level,room,wall) | i,d,s
  if level=>MAXLEVELS
    level:=MAXLEVELS-1
  if level<0
    level:=0
  levelnumber:=level
  roomnumber:=room
  d:=@displist
  s:=@levels+((lev_scr_offs[level]+room)*6*8)+(wall*8)
  repeat i from 0 to 2
    long[d]:=long[s]&(-1-long[@waves][(i)*2+0])
    d+=4
    byte[d]:=long[s+4]&(-1-long[@waves][(i)*2+1])
    d+=4
  repeat i from 3 to 42
    long[d]:=long[s]
    d+=4
    long[d]:=long[s+4]
    d+=4
  repeat i from 43 to 82
    long[d]:=long[s+16]
    d+=4
    long[d]:=long[s+20]
    d+=4
  repeat i from 83 to 122
    long[d]:=long[s+32]
    d+=4
    long[d]:=long[s+36]
    d+=4

  if wall==0
    setup_baddies(level,room)

PUB ResetSprites
  sprite_count:=0
  longfill(@spritesbuf,0,MAX_SPRITES)
  sprite_ptr:=@spritesbuf

PUB AddSprite(x,y,p,c)
  if sprite_count=>(MAX_SPRITES-1)
    return
  byte[sprite_ptr++]:=x
  byte[sprite_ptr++]:=y
  byte[sprite_ptr++]:=p
  byte[sprite_ptr++]:=c
  sprite_count++  

PUB RippleWater
    long[@displist+WATERLINE    ]:=long[@waves+waveframe   ] & long[@displist+WATERLINE-8]
    byte[@displist+WATERLINE+$04]:=byte[@waves+waveframe+4 ] & long[@displist+WATERLINE-4]
    long[@displist+WATERLINE+$08]:=long[@waves+waveframe+8 ] & long[@displist+WATERLINE-8]
    byte[@displist+WATERLINE+$0c]:=byte[@waves+waveframe+12] & long[@displist+WATERLINE-4]
    long[@displist+WATERLINE+$10]:=long[@waves+waveframe+16] & long[@displist+WATERLINE-8]
    byte[@displist+WATERLINE+$14]:=byte[@waves+waveframe+20] & long[@displist+WATERLINE-4]

    byte[@displist+WATERLINE+5]:=2
    byte[@displist+WATERLINE+5+8]:=2
    byte[@displist+WATERLINE+5+16]:=2
        
    byte[@displist+WATERLINE+$18]:=$c0
    byte[@displist+WATERLINE+$20]:=$c0

    if roomnumber=>levwaterlevel[levelnumber]
      if (frame&7)==0
        waveframe+=24 '3*8
        if waveframe==144
          waveframe~

pub prhex(x,y,v)
  y-=16
  scrmap[y*20+x+0]:=hextab[(v>>28)&15]
  scrmap[y*20+x+1]:=hextab[(v>>24)&15]
  scrmap[y*20+x+2]:=hextab[(v>>20)&15]
  scrmap[y*20+x+3]:=hextab[(v>>16)&15]
  scrmap[y*20+x+4]:=hextab[(v>>12)&15]
  scrmap[y*20+x+5]:=hextab[(v>> 8)&15]
  scrmap[y*20+x+6]:=hextab[(v>> 4)&15]
  scrmap[y*20+x+7]:=hextab[(v>> 0)&15]

pub check_collision(x,y,h) | i,w1,w2,w3,w4,w5,w6,s,s1,s2
  y-=8 ' for top of screen coord 8=top line
  'w1 to w6 is already cleared
  backpix(x,@w1) 'get pixel mask for left edge
  backpix(x+3,@w3) 'get pixel mask for middle 
  backpix(x+5,@w5) 'get pixel mask for right edge
  if(y<0) ' is he off top of screen
    h:=h+y ' alter height to check, as he's slightly off top
    if(h<0) ' if he's all off top of screen
      return 0 'return
    y:=0 'set y to 0 to start at top of screen for collision.
  if y<4
    y:=4 'hack for top of screen so that it copies the walls from the top layer of the game layout
  if(y+h>191)
    h-=((y+h)-191)
    if h<0
      return 0
  s:=@displist+(y<<3)
  s1:=0
  s2:=0
  repeat i from 0 to h
    if(byte[s+5]<8) '==$02) changed to <8 for when flashing from explosions :D
      s1|=long[s]
      s2|=long[s+4]
    else
      s1|=-1-long[s]
      s2|=-1-long[s+4]
    s+=8
  return (s1&w1)|(s2&w2)|(s1&w3)|(s2&w4)|(s1&w5)|(s2&w6)

pub backpix(x,pw)
  x>>=2
  long[pw][0]:=0 'they should already be 0 from spin init of W1 to W4         
  long[pw][1]:=0         
  if(x=>0 and x<40)
    byte[pw][x>>3]:=$80>>(x&7)         
        
PUB Do_Copyright_Anim | i,s,d      
  repeat i from 0 to 7
    s:=@SpriteSet+get_copyright_scan((frame>>1),i)
    d:=@SpriteSet+($f0<<5)+i<<2
    long[d+$00]:=long[s+$00]
    long[d+$20]:=long[s+$20]
    long[d+$40]:=long[s+$40]
    long[d+$60]:=long[s+$60]
    long[d+$80]:=long[s+$80]
    long[d+$a0]:=long[s+$a0]
    
PUB get_copyright_scan(fr,of) | t
  fr&=255
  if(fr<128-8)
    t:=0
  elseif(fr<128)
    t:=fr-120
  elseif(fr<256-8)
    t:=8
  else
    t:=8+(fr-248)
  t:=(t+of)&15
  if(t<8)
    return ($0d<<5)+(t<<2)
  return ($12<<5)+(t<<2)
           
pub VSync
'  bordercolour:=$02
  repeat until VSyncVal<>2
  repeat until VSyncVal==2
'  bordercolour:=$2d
  frame ++
              
PUB NES_Read_Gamepad : nes_bits        |  i
' //////////////////////////////////////////////////////////////////
' NES Game Paddle Read
' //////////////////////////////////////////////////////////////////       
' reads both gamepads in parallel encodes 8-bits for each in format
' right game pad #1 [15..8] : left game pad #0 [7..0]
'
' set I/O ports to proper direction
' P3 = JOY_CLK      (4)
' P4 = JOY_SH/LDn   (5) 
' P5 = JOY_DATAOUT0 (6)
' P6 = JOY_DATAOUT1 (7)
' NES Bit Encoding
'
' RIGHT  = %00000001
' LEFT   = %00000010
' DOWN   = %00000100
' UP     = %00001000
' START  = %00010000
' SELECT = %00100000
' B      = %01000000
' A      = %10000000

' step 1: set I/Os
  DIRA [JOY_CLK] := 1 ' output
  DIRA [JOY_LCH] := 1 ' output
  DIRA [JOY_DATAOUT0] := 0 ' input
  DIRA [JOY_DATAOUT1] := 0 ' input

' step 2: set clock and latch to 0
  OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
  OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0
'Delay(1)

' step 3: set latch to 1
  OUTA [JOY_LCH] := 1 ' JOY_SH/LDn = 1
'Delay(1)                            

' step 4: set latch to 0
  OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0

' data is now ready to shift out, clear storage
  nes_bits := 0

' step 5: read 8 bits, 1st bits are already latched and ready, simply save and clock remaining bits
  repeat i from 0 to 7
   nes_bits := (nes_bits << 1)
   nes_bits := nes_bits | INA[JOY_DATAOUT0] | (INA[JOY_DATAOUT1] << 8)
   OUTA [JOY_CLK] := 1 ' JOY_CLK = 1
   OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
 ' invert bits to make positive logic
  nes_bits := (!nes_bits & $FFFF)

  if nes_bits&$ff00==$ff00
    nes_bits&=$00ff
  if nes_bits&$00ff==$00ff
    nes_bits&=$ff00
  
' Keyboard (mapped onto NES buttons)
  if(key.keystate($C2))       'Up Arrow
    nes_bits|=NES0_UP
  if(key.keystate($C3))       'Down Arrow
    nes_bits|=NES0_DOWN
  if(key.keystate($C0))       'Left Arrow
    nes_bits|=NES0_LEFT
  if(key.keystate($C1))       'Right Arrow
    nes_bits|=NES0_RIGHT
  if(key.keystate($0D))       'Enter
    nes_bits|=NES0_START
  if(key.keystate(" "))       'Space
    nes_bits|=NES0_A

  dpad := nes_bits&(-1-opad)
  opad := nes_bits

' //////////////////////////////////////////////////////////////////
' End NES Game Paddle Read
' //////////////////////////////////////////////////////////////////       

PUB addbonus(x,y,b)
  bonux:=x
  bonuy:=y
  bonut:=10
  bonuc:=b
        
PUB DoBullToBaddyCollision | i,ptr
  ptr:=@baddy_data
  if numbaddies
    repeat i from 1 to numbaddies
      if byte[ptr][3]==SPID 'spider web
        if collision(bullx,bully+3,8,1,byte[ptr][0],byte[ptr][1]+7+(frame>>3&1),7,8)
          byte[ptr][3]:=0
          bullt:=0
          addbonus(byte[ptr][0],byte[ptr][1]+8,$6e)
          addscore(@add50)          

      if byte[ptr][3]==ABAT 'bat
        if collision(bullx,bully+3,8,1,byte[ptr][0],byte[ptr][1]+batoffs[frame>>3&15],7,8)
          byte[ptr][3]:=0
          bullt:=0
          addbonus(byte[ptr][0],byte[ptr][1]+8,$6e)
          addscore(@add50)          

      if (byte[ptr][3]==LGTL) OR (byte[ptr][3]==LGTR) 'LGTL&LGTR
        if collision(bullx,bully+3,8,1,byte[ptr][0],byte[ptr][1],8,8)
          byte[ptr][2]:=1
          bullt:=0
          repeat i from 0 to CONSTANT(WATERLINE>>3+2)
            byte[@displist][i*8+6]:=$82

      if byte[ptr][3]==STAR 'STAR
        if collision(bullx,bully+3,8,1,byte[ptr][0],byte[ptr][1],8,10)
          byte[ptr][3]:=0
          bullt:=0
          addbonus(byte[ptr][0],byte[ptr][1]+8,$6e)
          addscore(@add50)          
     
      ptr+=8

PUB collision(x1,y1,w1,h1,x2,y2,w2,h2)
  if x1+w1=<x2
    return 0
  if x2+w2=<x1
    return 0
  if y1+h1=<y2
    return 0
  if y2+h2=<y1
    return 0
  return 1
      
PUB draw_baddies | i,ptr
  ptr:=@baddy_data
  if numbaddies
    repeat i from 1 to numbaddies
      if(byte[ptr][3]=>$c0) and (byte[ptr][3]=<$c3) 'rescuee left and right
        AddSprite(byte[ptr][0],byte[ptr][1]  ,byte[ptr][2],byte[ptr][3]    )
        AddSprite(byte[ptr][0],byte[ptr][1]+8,byte[ptr][2],byte[ptr][3]+$20)
     
      if byte[ptr][3]==SPID 'spider web
        AddSprite(byte[ptr][0],byte[ptr][1],byte[ptr][2],byte[ptr][3])
        AddSprite(byte[ptr][0],byte[ptr][1]+7+(frame>>3&1),byte[ptr][2],byte[ptr][3]+$20)

      if byte[ptr][3]==ABAT 'bat
        AddSprite(byte[ptr][0],byte[ptr][1]+batoffs[frame>>3&15],byte[ptr][2],byte[ptr][3]+(((frame>>3)&1)<<5))

      if (byte[ptr][3]==LGTL) OR (byte[ptr][3]==LGTR) 'LGTL&LGTR
        AddSprite(byte[ptr][0],byte[ptr][1],byte[ptr][2],byte[ptr][3])

      if byte[ptr][3]==STAR 'STAR
        AddSprite(byte[ptr][0],byte[ptr][1]  ,byte[ptr][2],byte[ptr][3]+(frame>>3&1))
        AddSprite(byte[ptr][0],byte[ptr][1]+8,byte[ptr][2],byte[ptr][3]+(frame>>3&1)+$20)
     
      ptr+=8
      
      badsprs[i-1]:=(sprite_ptr-@spritesbuf)/4

PUB setup_baddies(level,room) | i,ptr
  ptr:=word[level_baddies[level]][room]
  bytefill(@baddy_data,0,MAX_BADDIES*BADLEN)
  numbaddies:=byte[ptr++]
  if numbaddies
    bytemove(@baddy_data,ptr,numbaddies*BADLEN)    

DAT

'BADX = 0
'BADY = 1
'BADP = 2
'BADC = 3
'BADTYP = 4
'BADACT = 5
'BADCNT = 6
'BADHGT = 7

' DO NOT ALTER ANYTHING FROM HERE ESPECIALLY LINE ORDER!
nextline      long      0                        
bordercolour  long      $02
tvparams      long      0               'status
              long      1               'enable
              long      modepins        'pins ' PROTO/DEMO BOARD = %001_0101 ' HYDRA = %011_0000
              long      mode            'mode
              long      10              'hc
              long      12              'vc
              long      myhx            'hx
              long      1               'vx
              long      0               'ho
              long      0               'vo
              long      50_000_000      '_xinfreq<<4  'broadcast
              long      0               'auralcog
scanbufptr    long      0
              long      @bordercolour+$10  'pointer to border colour
              long      @nextline+$10
              long      0
VSyncVal      long      0
dispptr       long      @displist+$10        
sprset        long      @SpriteSet+$10
chrmap        long      @scrmap+$10-(16*20*2)        
sprpal        long      @SpritePal+$10
cogcount      long      COGS
draw_spr_cnt  long      MAX_SPRITES
sprites       byte      0[MAX_SPRITES*4] 'this is the sprite list that gets draw
' DO NOT ALTER ANYTHING TO HERE ESPECIALLY LINE ORDER!

sprite_count  long      0
sprite_ptr    long      @spritesbuf+$10
'Sprite list is 4 bytes per sprite, bytes are Xpos,Ypos,Palette,Charnum  Xpos 0 = showing at 0 onscreen, Ypos 0 = off top of screen, 8= showing at top line of display, that way when they are 0'd out, they're off screen :D Palette is colour index offset*16, 0=first 16 colours, 1=second 16 colours, Charnum=sprite character index
spritesbuf    byte      0[MAX_SPRITES*4] 'this is the sprite list buffer, so it can be updated without tears if drawing screen at the same time.

waveframe     long      0
waves         byte      %00100111,%11100111,%11100111,%11100111,%11100111,$02,$cc,$02
              byte      %00000011,%11000011,%11000011,%11000011,%11000011,$02,$cd,$02
              byte      %00000001,%10000001,%10000001,%10000001,%10000001,$02,$ce,$02

              byte      %00011011,%11011011,%11011011,%11011011,%11011011,$02,$cc,$02
              byte      %00000001,%10000001,%10000001,%10000001,%10000001,$02,$cd,$02
              byte      %00000000,%00000000,%00000000,%00000000,%00000000,$02,$ce,$02

              byte      %00111101,%10111101,%10111101,%10111101,%10111101,$02,$cc,$02
              byte      %00011000,%00011000,%00011000,%00011000,%00011000,$02,$cd,$02
              byte      %00000000,%00000000,%00000000,%00000000,%00000000,$02,$ce,$02

              byte      %00111110,%01111110,%01111110,%01111110,%01111110,$02,$cc,$02
              byte      %00111100,%00111100,%00111100,%00111100,%00111100,$02,$cd,$02
              byte      %00011000,%00011000,%00011000,%00011000,%00011000,$02,$ce,$02

              byte      %00111111,%11111111,%11111111,%11111111,%11111111,$02,$cc,$02
              byte      %00111110,%01111110,%01111110,%01111110,%01111110,$02,$cd,$02
              byte      %00100100,%00100100,%00100100,%00100100,%00100100,$02,$ce,$02

              byte      %00111111,%11111111,%11111111,%11111111,%11111111,$02,$cc,$02
              byte      %00100111,%11100111,%11100111,%11100111,%11100111,$02,$cd,$02
              byte      %00000010,%01000010,%01000010,%01000010,%01000010,$02,$ce,$02
              
'Displist is made up of 192 * (8 bytes per scanline), each scanline is 5bytes ( 40 bits for each WIDE background pixel ), the next byte is background colour ( 0 in the 40bits ), the next byte is foreground colour ( 1 in the 40 bits ), the last byte is going to be border colour for that scanline.
'if background colour = 0 then it doesn't use the display list pixels, it uses scrmap as an 8x8 character map screen, 20x24 chars in total. each char is indexed from sprset's 16 colour characters
displist      file      "scr1.vcs"

levels        file      "herolvls.vcs"

scrmap        word
{
              word      $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 '  0
              word      $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 '  8
              word      $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 ' 16
              word      $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 ' 24
              word      $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 ' 32
              word      $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 ' 40
              word      $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 ' 48
              word      $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 ' 56
              word      $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 ' 64
              word      $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 ' 72
              word      $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 ' 80
              word      $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 ' 88
              word      $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 ' 96
              word      $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 '104
              word      $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 '112
              word      $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 '120
}
              word      $00,$20,$20,$01,$02,$03,$04,$04,$04,$04,$04,$04,$04,$04,$04,$04,$20,$20,$20,$20 '128
              word      $00,$20,$20,$20,$20,$20,$20,$20,$20,$20,$19,$19,$19,$20,$20,$20,$20,$20,$20,$20 '136
              word      $00,$20,$20,$20,$20,$20,$20,$20,$20,$20,$1a,$1a,$1a,$20,$20,$20,$20,$20,$20,$20 '144
              word      $00,$20,$20,$20,$20,$20,$20,$1b,$1b,$1b,$1b,$1b,$1b,$20,$20,$20,$20,$20,$20,$20 '152
              word      $00,$20,$20,$20,$20,$20,$20,$1c,$1c,$1c,$1c,$1c,$1c,$20,$20,$20,$20,$20,$20,$20 '160
              word      $00,$20,$20,$20,$20,$20,$20,"1","0","1","3","2","4",$20,$20,$20,$20,$20,$20,$20 '168
              word      $00,$00,$00,$00,$00,$00,$00,$f0,$f1,$f2,$f3,$f4,$f5,$00,$00,$00,$00,$00,$00,$00 '176
              word      $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00 '184

'SpriteSet and SpritePal are generated by the Bmp8ToLite.exe app and an 8bit bmp file called sprset.bmp, using
'Bmp8ToLite sprset -charmap16
'This generates two files, sprset.chr and sprset.pal
SpriteSet     file      "sprset.chr"
SpritePal     file      "sprset.pal"

hextab        byte      "0123456789ABCDEF"

add10         byte      "000010"
add40         byte      "000040"
add50         byte      "000050"
add75         byte      "000075"

batoffs       byte      0,1,2,3,4,5,6,7,8,7,6,5,4,3,2,1

CON

MAXLEVELS = 9

DAT

lev_scr_offs  word      0
              word      0+2
              word      0+2+4
              word      0+2+4+6
              word      0+2+4+6+8
              word      0+2+4+6+8+8
              word      0+2+4+6+8+8+10
              word      0+2+4+6+8+8+10+12
              word      0+2+4+6+8+8+10+12+14
              word      0+2+4+6+8+8+10+12+14+16

level_baddies word      @lev1bads+$10
              word      @lev2bads+$10
              word      @lev3bads+$10
              word      @lev4bads+$10
              word      @lev5bads+$10
              word      @lev6bads+$10
              word      @lev7bads+$10
              word      @lev8bads+$10
              word      @lev9bads+$10

lev1bads      word      @lev1scr1bad+$10,@lev1scr2bad+$10
lev2bads      word      @lev2scr1bad+$10,@lev2scr2bad+$10,@lev2scr3bad+$10,@lev2scr4bad+$10
lev3bads      word      @lev3scr1bad+$10,@lev3scr2bad+$10,@lev3scr3bad+$10,@lev3scr4bad+$10,@lev3scr5bad+$10,@lev3scr6bad+$10
lev4bads      word      @lev4scr1bad+$10,@lev4scr2bad+$10,@lev4scr3bad+$10,@lev4scr4bad+$10,@lev4scr5bad+$10,@lev4scr6bad+$10,@lev4scr7bad+$10,@lev4scr8bad+$10
lev5bads      word      @lev5scr1bad+$10,@lev5scr2bad+$10,@lev5scr3bad+$10,@lev5scr4bad+$10,@lev5scr5bad+$10,@lev5scr6bad+$10,@lev5scr7bad+$10,@lev5scr8bad+$10
lev6bads      word      @lev6scr1bad+$10,@lev6scr2bad+$10,@lev6scr3bad+$10,@lev6scr4bad+$10,@lev6scr5bad+$10,@lev6scr6bad+$10,@lev6scr7bad+$10,@lev6scr8bad+$10,@lev6scr9bad+$10,@lev6scr10bad+$10
lev7bads      word      @lev7scr1bad+$10,@lev7scr2bad+$10,@lev7scr3bad+$10,@lev7scr4bad+$10,@lev7scr5bad+$10,@lev7scr6bad+$10,@lev7scr7bad+$10,@lev7scr8bad+$10,@lev7scr9bad+$10,@lev7scr10bad+$10,@lev7scr11bad+$10,@lev7scr12bad+$10
lev8bads      word      @lev8scr1bad+$10,@lev8scr2bad+$10,@lev8scr3bad+$10,@lev8scr4bad+$10,@lev8scr5bad+$10,@lev8scr6bad+$10,@lev8scr7bad+$10,@lev8scr8bad+$10,@lev8scr9bad+$10,@lev8scr10bad+$10,@lev8scr11bad+$10,@lev8scr12bad+$10,@lev8scr13bad+$10,@lev8scr14bad+$10
lev9bads      word      @lev9scr1bad+$10,@lev9scr2bad+$10,@lev9scr3bad+$10,@lev9scr4bad+$10,@lev9scr5bad+$10,@lev9scr6bad+$10,@lev9scr7bad+$10,@lev9scr8bad+$10,@lev9scr9bad+$10,@lev9scr10bad+$10,@lev9scr11bad+$10,@lev9scr12bad+$10,@lev9scr13bad+$10,@lev9scr14bad+$10,@lev9scr15bad+$10,@lev9scr16bad+$10

levwaterlevel byte       1, 3, 5, 7, 7, 9,10,10,10,10,10,10, 9,10,10,10,10,10,10,10

levExitRightDir byte    0,0,0,0,-1,0,1,-1,1,-1,1,-1,-1,1,-1,1,-1,1,-1,1

lev1scr1bad   byte      0
lev1scr2bad   byte      2
              byte      $18,$4a,$00,MANL,  0,0,0,0
              byte      $30,$38,$00,SPID,  0,0,0,0
            
lev2scr1bad   byte      0
lev2scr2bad   byte      1
              byte      $3c,$3a,$00,SPID,  0,0,0,0
lev2scr3bad   byte      2
              byte      $38,$38,$00,ABAT,  0,0,0,0
              byte      $50,$60,$00,SPID,  0,0,0,0
lev2scr4bad   byte      2
              byte      $80,$4a,$00,MANR,  0,0,0,0
              byte      $6c,$3c,$00,ABAT,  0,0,0,0

lev3scr1bad   byte      0
lev3scr2bad   byte      3
              byte      $50,$1a,$00,LGTR,  0,0,0,0
              byte      $2c,$3a,$00,SPID,  0,0,0,0
              byte      $50,$5a,$00,STAR,  0,0,0,0
lev3scr3bad   byte      1
              byte      $30,$38,$00,SPID,  0,0,0,0
lev3scr4bad   byte      3
              byte      $68,$1a,$00,LGTR,  0,0,0,0
              byte      $68,$3a,$00,ABAT,  0,0,0,0
              byte      $28,$5a,$00,SPID,  0,0,0,0
lev3scr5bad   byte      1
              byte      $48,$3a,$00,STAR,  0,0,0,0
lev3scr6bad   byte      2
              byte      $18,$4a,$00,MANL,  0,0,0,0
              byte      $38,$3c,$00,STAR,  0,0,0,0

lev4scr1bad   byte      0
lev4scr2bad   byte      0
lev4scr3bad   byte      0
lev4scr4bad   byte      0
lev4scr5bad   byte      0
lev4scr6bad   byte      0
lev4scr7bad   byte      0
lev4scr8bad   byte      2
              byte      $18,$4a,$00,MANL,  0,0,0,0
              byte      $38,$3c,$00,STAR,  0,0,0,0

lev5scr1bad   byte      0
lev5scr2bad   byte      0
lev5scr3bad   byte      0
lev5scr4bad   byte      0
lev5scr5bad   byte      0
lev5scr6bad   byte      0
lev5scr7bad   byte      0
lev5scr8bad   byte      2
              byte      $18,$4a,$00,MANL,  0,0,0,0
              byte      $38,$3c,$00,STAR,  0,0,0,0

lev6scr1bad   byte      0
lev6scr2bad   byte      0
lev6scr3bad   byte      0
lev6scr4bad   byte      0
lev6scr5bad   byte      0
lev6scr6bad   byte      0
lev6scr7bad   byte      0
lev6scr8bad   byte      0
lev6scr9bad   byte      0
lev6scr10bad  byte      2
              byte      $18,$4a,$00,MANL,  0,0,0,0
              byte      $38,$3c,$00,STAR,  0,0,0,0

lev7scr1bad   byte      0
lev7scr2bad   byte      0
lev7scr3bad   byte      0
lev7scr4bad   byte      0
lev7scr5bad   byte      0
lev7scr6bad   byte      0
lev7scr7bad   byte      0
lev7scr8bad   byte      0
lev7scr9bad   byte      0
lev7scr10bad  byte      0
lev7scr11bad  byte      0
lev7scr12bad  byte      2
              byte      $18,$4a,$00,MANL,  0,0,0,0
              byte      $38,$3c,$00,STAR,  0,0,0,0

lev8scr1bad   byte      0
lev8scr2bad   byte      0
lev8scr3bad   byte      0
lev8scr4bad   byte      0
lev8scr5bad   byte      0
lev8scr6bad   byte      0
lev8scr7bad   byte      0
lev8scr8bad   byte      0
lev8scr9bad   byte      0
lev8scr10bad  byte      0
lev8scr11bad  byte      0
lev8scr12bad  byte      0
lev8scr13bad  byte      0
lev8scr14bad  byte      2
              byte      $18,$4a,$00,MANL,  0,0,0,0
              byte      $38,$3c,$00,STAR,  0,0,0,0

lev9scr1bad   byte      0
lev9scr2bad   byte      0
lev9scr3bad   byte      0
lev9scr4bad   byte      0
lev9scr5bad   byte      0
lev9scr6bad   byte      0
lev9scr7bad   byte      0
lev9scr8bad   byte      0
lev9scr9bad   byte      0
lev9scr10bad  byte      0
lev9scr11bad  byte      0
lev9scr12bad  byte      0
lev9scr13bad  byte      0
lev9scr14bad  byte      0
lev9scr15bad  byte      0
lev9scr16bad  byte      2
              byte      $18,$4a,$00,MANL,  0,0,0,0
              byte      $38,$3c,$00,STAR,  0,0,0,0
              