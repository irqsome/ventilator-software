'┌─┬─┬───┬┬───┬─┬───┐
'│ │ │ │ │└┐ ┌┘─┤ | │
'│ │ │ │ │ │ │ ─┤   ┤
'└──┬┴┬┴┬─┬┴─┼─┬┴─┼─┘
'   │ │ │ │ ─┤ │ ┌┘
'   │ │ │ │ ─┤ └┐│
'   └─────┴──┴──┴┘ 


' DESCRIPTION: Ein Puzzlespiel, selbsterklärend
'
' VERSION: 0.1.0 with Sound
' AUTHOR: Michael Zinn
' LAST MODIFIED:
' COMMENTS:
'
' CONTROLS: gamepad


'------------------------------

' Notizen
'
' 0 : frei
' 1 : Wand
' 2 : Stein
' 3 : Leiter
' 4 : Sand
' 5 : Monster
' 6 : Flugmonster
' 7 : Player
'
'
' Animationsregeln
' 
' 
'
' TODO
' - Grafik      (naja)                                  DONE
' - Animationen (mittel)--------------------------------DONE
'   - Debugging von "Halten"-Animation                     DONE
' - Gravitation (schwierig)-----------------------------DONE
' - Scrolling                                           DONE
' - 2 Spieler Funktionalität                            DONE
' - Levels                                              DONE
'   - designen                                            DONE
'   - konvertieren                                        DONE
'   - auslesen (knifflig)                                 DONE
' - Titelbild   (einfach, aber muehsam)                 DONE
'   - Malen                                               DONE
'   - konvertieren                                        DONE
'   - darstellen                                          DONE
' - Menü          (bäh)  -------------------------------DONE
'   - Aufbau                                              DONE
'   - Grafik                                              DONE
'   - Funktionalitaet                                     DONE
' - Passwortsystem (arbeit)-----------------------------DONE
'   - Passworte generiert                                 DONE
'   - anzeigen                                            DONE
'   - eingeben                                            DONE
' - Musik ----------------------------------------------DONE
'   - Konvertieren                                        DONE
'   - Einbinden                                           DONE
' - Endsequenz' ----------------------------------------DONE
'   - Grafik                                              DONE
'   - Tilemap                                             DONE
'   - Tanzen                                              DONE
'   - kreisende Geister (schwierig)                       DONE
'     - Optimale Ellipse finden                             DONE
'     - Sinustabellen auslesen                              DONE
'     - Paletten passend ändern                             DONE



CON

  _clkmode = xtal1 + pll16x       ' enable external clock range 5-10MHz and pll times 8
  _xinfreq = 5_000_000 + 0000   ' set frequency to 10 MHZ plus some error due to XTAL (1000-5000 usually works)
  _stack   = 128                 ' accomodate display memory and stack

  'Leveldata
  _=$00_55 'frei
  W=$01_56 'wall
  R=$02_57 'rock
  L=$00_58 'ladder
  S=$03_59 'AENDERN ?
  M=$04_5A 'Monster
  N=$04_5B 'Monster, haltend
  F=$04_5C 'Monster, fliegend


  
  Wgreen = $07_5f'56
  Wbrown = $08_60
  Wgrey  = $09_61

  Mblue  = $33_5a
  Mpink  = $39_5a
  Mgreen = $3f_5a

   
  JRO=$0C_6a 'Player Right Orange
  JLO=$0C_6c 'Player Left Orange
  JROH=$0C_6e       'right orange hold
  JLOH=$0C_70
  JCO=$0E_5e    'climb
  
  JRT=$0D_6a
  JLT=$0D_6c
  JRTH=$0D_6e
  JLTH=$0D_70
  JCT=$0F_5e

  
 { 
  JRO=$07_14 'Player Right Orange
  JLO=$07_16 'Player Left Orange
  JROH=$07_18       'right orange hold
  JLOH=$07_1a
  JCO=$07_13    'climb
  
  JRT=$08_14
  JLT=$08_16
  JRTH=$08_18
  JLTH=$08_1a
  JCT=$08_13
  }

  BANG = $06_5d    'gekillter Gegner 
  FALLERS=$05_55   'Hintergrund fuer Steine/Monster 
  ROCKSLIDE=$02_55 'Hintergrund für Stein-Sprite
  J=$05_02 'Player

  DIRRIGHT = 0
  DIRLEFT = 1

  TILESIZE = 64
  SPRITESIZE = 128     'groesse einer Sprite-Grafik im Speicher (Bild + Maske)



  'Numbers      (Bestehen aus zwei Tiles)
  up0 = $0D_35     
  down0 = $0D_3F

  TILE_digitStartUp = $35    'oberer Teil der Ziffer 0
  TILE_digitStartDown = $3f  'unterer Teil der Ziffer 0
  TILE_black = $29_55
  TILE_effect = $2a_55

  
  PAL_digitStart = $1a
  PAL_whiteFont = $10
  PAL_black= $29
  PAL_effect= $30
  PAL_menuSelected = $25
  PAL_menuUnselected = $24
  
  ' button ids/bit masks
  ' NES bit encodings general for state bits
  NES_RIGHT  = %00000001
  NES_LEFT   = %00000010
  NES_DOWN   = %00000100
  NES_UP     = %00001000
  NES_START  = %00010000
  NES_SELECT = %00100000
  NES_B      = %01000000
  NES_A      = %10000000

  ' NES bit encodings for NES gamepad 0
  NES0_RIGHT  = %00000000_00000001
  NES0_LEFT   = %00000000_00000010
  NES0_DOWN   = %00000000_00000100
  NES0_UP     = %00000000_00001000
  NES0_START  = %00000000_00010000
  NES0_SELECT = %00000000_00100000
  NES0_B      = %00000000_01000000
  NES0_A      = %00000000_10000000

  ' NES bit encodings for NES gamepad 1
  NES1_RIGHT  = %00000001_00000000
  NES1_LEFT   = %00000010_00000000
  NES1_DOWN   = %00000100_00000000
  NES1_UP     = %00001000_00000000
  NES1_START  = %00010000_00000000
  NES1_SELECT = %00100000_00000000
  NES1_B      = %01000000_00000000
  NES1_A      = %10000000_00000000

  ' color constant's to make setting colors for parallax graphics setup easier
  COL_Black       = %0000_0010
  COL_DarkGrey    = %0000_0011
  COL_Grey        = %0000_0100
  COL_LightGrey   = %0000_0101
  COL_BrightGrey  = %0000_0110
  COL_White       = %0000_0111 

  ' colors are in reverse order from parallax drivers, or in order 0-360 phase lag from 0 = Blue, on NTSC color wheel
  ' so code $0 = 0 degrees, $F = 360 degrees, more intuitive mapping, and is 1:1 with actual hardware
  COL_PowerBlue   = %1111_1_100 
  COL_Blue        = %1110_1_100
  COL_SkyBlue     = %1101_1_100
  COL_AquaMarine  = %1100_1_100
  COL_LightGreen  = %1011_1_100
  COL_Green       = %1010_1_100
  COL_GreenYellow = %1001_1_100
  COL_Yellow      = %1000_1_100
  COL_Gold        = %0111_1_100
  COL_Orange      = %0110_1_100
  COL_Red         = %0101_1_100
  COL_VioletRed   = %0100_1_100
  COL_Pink        = %0011_1_100
  COL_Magenta     = %0010_1_100
  COL_Violet      = %0001_1_100
  COL_Purple      = %0000_1_100

' Zustaende des Spiels
  STATE_mainMenu = 0
  STATE_enterPassword = 1 
  STATE_playLevelIntro = 2 
  STATE_loadLevelData = 3 
  STATE_playLevel = 4 
  STATE_pauseMenu = 5 
  STATE_victory = 6

  'Menueeintraege des Hauptmenues
  MAINMENU_start = 0
  MAINMENU_passwort = 1

  'Menueeintraege des Pause-Menues
  PAUSEMENU_weiter = 0
  PAUSEMENU_neustart = 1
  PAUSEMENU_ende = 2

  'Musik
  Musik_Kraftwerk = $00000
  Musik_Beethoven = $078BD
  Musik_Rammstein =$09394
  Musik_OdeFreude =$12A24 'auch von Beethoven
  
VAR
byte FSM_state


' fuer funktionen
byte enableInput      'Damit nur ein Knopf gleichzeitig ausgewertet wird
byte aniCounter       'von 0 bis 15 animationscounter beim laufen/fallen
long aniIncrement     'Für nette Beschleunigungseffekte beim Fallen

byte currentLevel     'von 1 bis 100
byte monstersLeft     'Wenn auf 0 -> naechster Level

byte scrollPos        'fuers scrolling, von 0 bis 3 




long anistack[40]   ' Stack für den COG der die Tiles animiert

' begin parameter list ////////////////////////////////////////////////////////
' tile engine data structure pointers (can be changed in real-time by app!)
long tile_map_base_ptr_parm       ' base address of the tile map
long tile_bitmaps_base_ptr_parm   ' base address of the tile bitmaps
long tile_palettes_base_ptr_parm  ' base address of the palettes

long tile_map_sprite_cntrl_parm   ' pointer to the value that holds various "control" values for the tile map/sprite engine
                                  ' currently, encodes width of map, and number of sprites to process up to 8 in following format
                                  ' $xx_xx_ss_ww, xx is don't care/unused
                                  ' ss = number of sprites to process 1..8
                                  ' ww = the number of "screens" or multiples of 16 that the tile map is
                                  ' eg. 0 would be 16 wide (standard), 1 would be 32 tiles, 2 would be 64 tiles, etc.
                                  ' this allows multiscreen width playfields and thus large horizontal/vertical scrolling games
                                  ' note that the final size is always a power of 2 multiple of 16

long tile_sprite_tbl_base_ptr_parm ' base address of sprite table


' real-time engine status variables, these are updated in real time by the
' tile engine itself, so they can be monitored outside in SPIN/ASM by game
long tile_status_bits_parm      ' vsync, hsync, etc.

' format of tile_status_bits_parm, only the Vsync status bit is updated
'
' byte 3 (unused)|byte 2 (line)|   byte 1 (tile postion)    |                     byte 0 (sync and region)      |
'|x x x x x x x x| line 8-bits | row 4 bits | column 4-bits |x x x x | region 2-bits | hsync 1-bit | vsync 1-bit|
'   b31..b24         b23..b16      b15..b12     b11..b8                    b3..b2          b1            b0
' Region 0=Top Overscan, 1=Active Video, 2=Bottom Overscan, 3=Vsync
' NOTE: In this version of the tile engine only VSYNC and REGION are valid 

' end parameter list ///////////////////////////////////////////////////////////

'byte sbuffer[80]                                        ' string buffer for printing
'long x, y, tx, ty, index, dir, tile_map_index, test_tile' demo working vars
'long ghost_x, ghost_y                                   ' position of ghost
'long old_ghost_x, old_ghost_y                           ' holds last position of ghost                        
'byte bx, by                                             ' byte valued x,y

byte PlayerX, PlayerY                                            ' position in grid (0-10 +128, 0-7 +128)
byte PlayerXtemp,PlayerYtemp                                'Fuer Zwischenrechnungen
byte PlayerDir '0 = rechts, 1 = links
byte Player1X, Player1Y, Player1Dir   'fuer den nicht aktiven spieler 
byte Player2X, Player2Y, Player2Dir   'fuer den nicht aktiven spieler

byte Players          'Wieviele Spieler es gibt (1 oder 2)
byte currentPlayer  '0: Orange  1: Turkis

' Tile-Animations-COG ------------------------
byte count       ' zählt die 16 LONGS die man umschreiben muss, um ein Tile zu ändern   

' Musik, die gerade gespielt wird (siehe CON MUSIK)
byte currentMusic



OBJ

game_pad :      "gamepad.spin"     'Standardtreiber
gfx:            "HEL_GFX_ENGINE_040.spin"  'vielleicht die neuere benutzen?
'glow  : "glow_led_001.spin"                'fliegt später wieder raus
hdmf : "EPM_HDMF_driver_010.spin" 'HDMF song driver  (musik)


PUB Start  | i 'temporär                      'main game loop
' Jetzt, gehts, lo-os!

  COGNEW(animate_tiles,@anistack[0])

  ' star the game pad driver
  game_pad.start
         
  scrollPos := 0

  ' Level initialisieren
  currentLevel := 1
  currentPlayer := 0     '0: Orange  1:Turkis

  ' 1- 39:  normal      (gruen)
  '40- 69:  Wueste      (gelb/rot)
  '70-100:  Industriell (silber metallic)
  

' points ptrs to actual memory storage for tile engine
  tile_map_base_ptr_parm        := @mainMenu 'gameField
  tile_bitmaps_base_ptr_parm    := @Tiles'$9c20 '@Tiles '1'@tile_bitmaps
  tile_palettes_base_ptr_parm   := @palette_map
  tile_map_sprite_cntrl_parm    := $00_00_08_00 ' set for 8 sprites and width 16 tiles (1 screens wide), 0 = 16 tiles, 1 = 32 tiles, 2 = 64 tiles, 3 = 128 tiles, etc.
  tile_sprite_tbl_base_ptr_parm := @sprite_tbl[0] 
  tile_status_bits_parm         := 0

  ' launch a COG with ASM video driver
  gfx.start(@tile_map_base_ptr_parm)



  ' Musik anschalten
  hdmf.start(0 {debug off})
  hdmf.play_song(Musik_Kraftwerk | hdmf#EEPROM_ASSET, 1) 'loop
  currentMusic := 1

          
  ' FSM des Games
  FSM_state := STATE_mainMenu ' Startzustand
  repeat
    CASE FSM_state
    
      STATE_mainMenu:     'Startzustand
        manageMainMenu
        
      STATE_enterPassword:
        manageEnterPassword
      
      STATE_playLevelIntro:
        playLevelIntro(currentLevel)
        REPEAT UNTIL (NOT game_pad.button(NES0_START)) AND (NOT game_pad.button(NES0_A))
        repeat 1000
        REPEAT UNTIL game_pad.button(NES0_START) OR game_pad.button(NES0_A)

        FSM_state := STATE_loadLevelData
                      
      STATE_loadLevelData:
        loadLevelData(currentLevel)
        repeat 1000
        REPEAT UNTIL (NOT game_pad.button(NES0_START)) AND (NOT game_pad.button(NES0_A))
        repeat 1000
        
        FSM_state := STATE_playLevel
               
      STATE_playLevel:
        playLevel
      
      STATE_pauseMenu:
        managePauseMenu
       
      STATE_victory:
        displayVictory    


      
PUB manageMainMenu | menuSelection

  'playEffectZoom
  hideAllSprites

  tile_map_base_ptr_parm := @mainMenu
  'Passwort ausblenden
  drawRect( _ , 3,8,4,2)     


  menuSelection := MAINMENU_start

  palette_map.LONG[$13] := palette_map.LONG[  PAL_menuSelected] 'Start      
  palette_map.LONG[$14] := palette_map.LONG[PAL_menuUnselected] 'Passwort


  repeat
    if(game_pad.button(NES0_UP) OR game_pad.button(NES0_DOWN) OR game_pad.button(NES0_SELECT))
      menuSelection := (menuSelection + 1) // 2

      palette_map.LONG[$13] := palette_map.LONG[PAL_menuUnselected] 'Start      
      palette_map.LONG[$14] := palette_map.LONG[PAL_menuUnselected] 'Passwort
      palette_map.LONG[$13+menuSelection] := palette_map.LONG[PAL_menuSelected] 'aktiv

      REPEAT UNTIL (NOT game_pad.button(NES0_DOWN)) AND (NOT game_pad.button(NES0_SELECT)) AND (NOT game_pad.button(NES0_UP))   'loslassen

    if(game_pad.button(NES0_A) OR game_pad.button(NES0_Start))
      CASE menuSelection
        MAINMENU_start:
          currentLevel := 1
          currentPlayer := 0
          REPEAT UNTIL (NOT game_pad.button(NES0_A)) AND (NOT game_pad.button(NES0_START))
          FSM_state := STATE_playLevelIntro
          return

        MAINMENU_passwort:
          REPEAT UNTIL (NOT game_pad.button(NES0_A)) AND (NOT game_pad.button(NES0_START))
          FSM_state := STATE_enterPassword
          return
  
'  playEffectClose
'  FSM_state := STATE_playLevelIntro

PUB manageEnterPassword | enteredPassword, digitFocus, matchingLevel , counter
  enteredPassword := passwords[currentLevel-1]
  digitFocus := 3  '3 = linke ziffer, 0 = rechte ziffer

  drawColoredDigit( enteredPassword>>12    , 3,8)
  drawColoredDigit((enteredPassword>> 8)&$F, 4,8)
  drawColoredDigit((enteredPassword>> 4)&$F, 5,8)
  drawColoredDigit( enteredPassword     &$F, 6,8)

  ' Steuerung 
  'Ziffern mit links/B und rechts/A auswaehlen
  'Zurueck mit Select oder B ganz links 
  'Los mit Start oder A ganz rechts
  'Ziffer aendern mit hoch (mehr) oder runter (weniger)

  counter := 0
  repeat
    ' aktive Ziffer blinken lassen

  
    if(game_pad.button(NES0_UP))
      'Ziffer um 1 erhoehen
      enteredPassword := (enteredPassword & ($FFFF - ($F<<(digitFocus<<2)))) +(((((enteredPassword>>(digitFocus<<2))&$F) + 1) // 10 )<<(digitFocus<<2))
      REPEAT UNTIL NOT game_pad.button(NES0_UP)   'loslassen 
      repeat 1000
      
    if(game_pad.button(NES0_DOWN))
      'Ziffer um 1 verringern
      enteredPassword := (enteredPassword & ($FFFF - ($F<<(digitFocus<<2)))) +(((((enteredPassword>>(digitFocus<<2))&$F) + 9) // 10 )<<(digitFocus<<2))
      REPEAT UNTIL NOT game_pad.button(NES0_DOWN)
      repeat 1000
      
    if(game_pad.button(NES0_A) OR game_pad.button(NES0_RIGHT))
      if(digitFocus>0)
        digitFocus--         
      REPEAT UNTIL NOT game_pad.button(NES0_RIGHT) AND NOT game_pad.button(NES0_A)
      repeat 1000
    if(game_pad.button(NES0_B) OR game_pad.button(NES0_LEFT))
      if(digitFocus<3)
        digitFocus++     
      REPEAT UNTIL NOT game_pad.button(NES0_LEFT) AND NOT game_pad.button(NES0_B)
      repeat 1000

    if(game_pad.button(NES0_START))
      matchingLevel := checkPassword(enteredPassword)

      if(matchingLevel)
        currentLevel := matchingLevel
        FSM_state := STATE_playLevelIntro
        return

      FSM_state := STATE_mainMenu
      drawColoredDigit( 0, 3,8)
      drawColoredDigit( 0, 4,8)
      drawColoredDigit( 0, 5,8)
      drawColoredDigit( 0, 6,8)
      REPEAT UNTIL NOT game_pad.button(NES0_START                )
      repeat 1000
  
      return

    if(game_pad.button(NES0_SELECT))
      FSM_state := STATE_mainMenu
      return

    if(digitFocus <> 3)
      drawColoredDigit( enteredPassword>>12    , 3,8)
    if(digitFocus <> 2)
      drawColoredDigit((enteredPassword>> 8)&$F, 4,8)
    if(digitFocus <> 1)
      drawColoredDigit((enteredPassword>> 4)&$F, 5,8)
    if(digitFocus <> 0)
      drawColoredDigit( enteredPassword     &$F, 6,8)

    if(counter > 200)
      drawRect(_,6-digitFocus,8,1,2)
    else
      drawColoredDigit((enteredPassword>>(digitFocus<<2))&$F,6-digitFocus,8)
    
    counter++
    if(counter > 300)
      counter := 0
    
   


PUB playLevel

  updatePlayer
  tile_map_base_ptr_parm := @gameField + (scrollPos<<1)

  
' Loop zum Umsetzen der Bewegungen
repeat
   
  if(monstersLeft < 1)
    'killFlag := currentLevel + 1
    currentLevel++ 'switchToLevel(currentLevel + 1) 'loadLevel(currentLevel + 1)
    if(currentLevel > 100)
      FSM_state := STATE_victory
    else
      FSM_state := STATE_playLevelIntro
    playEffectClose
    return
 
  enableInput := true
 
  if (game_pad.button(NES0_RIGHT) AND enableInput)   ' Right
    enableInput := false
    PlayerDir := 0
    updatePlayer
    if(PlayerX < 10)' 138)
      CASE getField(PlayerX+1,PlayerY)'gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +18] 
        _:
          if gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +34] == _
            jump'Right
          else
            walk'Right
        'PlayerX+=1
          'repeat 50000
        L:
          if gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +34] <> W AND gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +34] <> S
            jump'Right 'walkRight
          else
            walk'Right
          'PlayerX+=1
          'repeat 50000
        S:
          walk'Right
          gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +17] := _

          'PlayerX+=1
          'repeat 50000
        R:
          if(gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +19] == _)
            kick'Right'gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +19] := R
'            gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +18] := _
'            repeat 25000
        M,N,F:
          jump

  if (game_pad.button(NES0_LEFT) AND enableInput) 'LEFT
    enableInput := false                                             'nach Links
    PlayerDir := 1
    updatePlayer
    if(PlayerX > 0)'128)                                                            'nur wenn nicht ganz links
      CASE gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +16]                   'Was ist links von Player?
        _:                                                                      '  frei?
          if gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +32] == _
            jump'Left
          else
            walk'Left
           'PlayerX-=1                                                             '    Dann los
          'repeat 50000
        L:                                                                      '  Leiter?
          if gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +32] <> W
            jump'Left'
          else
            walk'Left
          'PlayerX-=1                                                             '    auch okay
          'repeat 50000
        S:                                                                      '  Sand?
          walk'Left
                         '    passt scho
          'PlayerX-=1
          'repeat 50000
        R:                                                                      '  Stein?
          if(gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +15] == _)           '    Ist dahinter platz?
            kick'Left
        M,N,F:
          jump
            
            
      
  if (game_pad.button(NES0_DOWN) AND enableInput) 'DOWN
    enableInput := false
 '   tile_bitmaps_base_ptr_parm    += 1    
    if(PlayerY < 7)'135)
      CASE gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +33] 
        L:
          climbDown 'PlayerY+=1
        _:
          fallPlayer
          'repeat 75000

  if (game_pad.button(NES0_UP) AND enableInput)      'UP
    enableInput := false
  '  tile_bitmaps_base_ptr_parm    -= 1    

'    if(PlayerY > 128)
      CASE gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +1]                    'Was ist über Player?
        _,L:                                                                    'Ist es ne Leiter?
          if(gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +17] == L)           '  Steht er auf ner Leiter?
            climbUp 'PlayerY-=1
           ' repeat 75000


  if (game_pad.button(NES0_A) AND enableInput )'AND (scrollPos < 3))  'A, nach rechts scrollen
    scrollRight
    repeat 65000

  if (game_pad.button(NES0_B) AND enableInput )'AND (scrollPos > 0))  'B, nach links scrollen
    scrollLeft
    repeat 65000

  if (game_pad.button(NES0_START) AND enableInput)

    FSM_state := STATE_pauseMenu
    return
  
' 

  ' Spielfigur wechseln
  if (game_pad.button(NES0_SELECT) AND enableInput AND (Players == 2))
    enableInput := false
    togglePlayer
    repeat 1000
    REPEAT WHILE game_pad.button(NES0_SELECT)
    repeat 1000
 '    REPEAT WHILE NOT game_pad.button(NES0_START)
 
 '    repeat 65000

       

    
          
  ' verschub
  sprite_tbl[0] := (((PlayerY&15)+2) << 28) + (((PlayerX&15)+2 - scrollPos) << 20) + (0 << 8) + ($01)            
      
PUB managePauseMenu | menuSelection

  playEffectZoom
  hideAllSprites

  tile_map_base_ptr_parm := @pauseMenu
  drawPassword(3,10)

  menuSelection := PAUSEMENU_weiter

  palette_map.LONG[$16] := palette_map.LONG[ PAL_menuSelected ]   'Weiter      
  palette_map.LONG[$17] := palette_map.LONG[PAL_menuUnselected] 'Neustart
  palette_map.LONG[$18] := palette_map.LONG[PAL_menuUnselected] 'Ende

  repeat
    if(game_pad.button(NES0_DOWN) OR game_pad.button(NES0_SELECT))
      menuSelection := (menuSelection + 1) // 3

      palette_map.LONG[$16] := palette_map.LONG[PAL_menuUnselected] 'Weiter      
      palette_map.LONG[$17] := palette_map.LONG[PAL_menuUnselected] 'Neustart
      palette_map.LONG[$18] := palette_map.LONG[PAL_menuUnselected] 'Ende
      palette_map.LONG[$16+menuSelection] := palette_map.LONG[PAL_menuSelected] 'Ende

      repeat 1000
      REPEAT UNTIL (NOT game_pad.button(NES0_DOWN)) AND (NOT game_pad.button(NES0_SELECT))   'loslassen
      repeat 1000
      
    if(game_pad.button(NES0_UP))
      menuSelection := (menuSelection + 2) // 3

      palette_map.LONG[$16] := palette_map.LONG[PAL_menuUnselected] 'Weiter      
      palette_map.LONG[$17] := palette_map.LONG[PAL_menuUnselected] 'Neustart
      palette_map.LONG[$18] := palette_map.LONG[PAL_menuUnselected] 'Ende
      palette_map.LONG[$16+menuSelection] := palette_map.LONG[PAL_menuSelected] 'Ende

      repeat 10000
      REPEAT UNTIL NOT game_pad.button(NES0_UP)   'loslassen
      repeat 10000
      REPEAT UNTIL NOT game_pad.button(NES0_UP)   'loslassen
      repeat 10000
      REPEAT UNTIL NOT game_pad.button(NES0_UP)   'loslassen
      repeat 10000

    if(game_pad.button(NES0_A) OR game_pad.button(NES0_Start))
      CASE menuSelection
        PAUSEMENU_weiter:
          REPEAT UNTIL (NOT game_pad.button(NES0_A)) AND (NOT game_pad.button(NES0_START))
          repeat 1000
          FSM_state := STATE_playLevel
          return

        PAUSEMENU_neustart:
          REPEAT UNTIL (NOT game_pad.button(NES0_A)) AND (NOT game_pad.button(NES0_START))
          repeat 1000
          FSM_state := STATE_playLevelIntro
          return

        PAUSEMENU_ende:
          REPEAT UNTIL (NOT game_pad.button(NES0_A)) AND (NOT game_pad.button(NES0_START))
          repeat 1000
          FSM_state := STATE_mainMenu
          return 
  
    if(game_pad.button(NES0_B))
      REPEAT UNTIL (NOT game_pad.button(NES0_A))
      repeat 1000
      FSM_state := STATE_playLevel
      return


  
  'repeat 1000000

  'FSM_state := STATE_playLevel
  
PUB displayVictory  | angle , ghostphase
': Geister fliegen lassen
'Musik spielen
'Spielfiguren tanzen lassen

  hideAllSprites
  tile_map_base_ptr_parm := @victoryScreen

  hdmf.play_song(Musik_OdeFreude | hdmf#EEPROM_ASSET , 1 {loop})

  'Mitte der Ellipse: X=88 Y=64
  
  ' Geister        'Y(Zeile)     X(Spalte)
'  sprite_tbl[0] := (64 << 24) + (88 << 16)  + ($01)
'  sprite_tbl[1] := @AniGhost1  

  ' Spieler         
'  sprite_tbl[6] := (128 << 24) + (64 << 16)  + ($01)
'  sprite_tbl[7] := @PlayerRightKick  
'  sprite_tbl[8] := (128 << 24) + (112 << 16)  + ($01)
'  sprite_tbl[9] := @PlayerRightKick  

  
  angle := 1


  'WARNING: Magic code ahead!
  
  repeat
    angle+=2
    angle &= %1_1111_1111_1111
    ghostphase := (angle>>2) & %1000_0000 
    
     
                                                                           ' /1158
    sprite_tbl[0] := ((64+(Sin(angle+2048)/1449)) << 24) + ((88+(Sin(angle)>>10)) << 16)  + ($01)
    sprite_tbl[1] := @AniGhost1 + ghostphase  

    ' 120 grad = 21845
    ' 240 grad = 43691
    sprite_tbl[2] := ((64+(Sin(angle+23893)/1449)) << 24) + ((88+(Sin(angle+21845)>>10)) << 16)  + ($01)
    sprite_tbl[3] := @AniGhost1 + ghostphase  
  
    sprite_tbl[4] := ((64+(Sin(angle+45739)/1449)) << 24) + ((88+(Sin(angle+43691)>>10)) << 16)  + ($01)
    sprite_tbl[5] := @AniGhost2 - ghostphase  

    palette_map.LONG[$2B] := palette_map.LONG[$33+6*(((angle-1536)&$1FFF ) / 2731)]   'unten
    palette_map.LONG[$2C] := palette_map.LONG[$33+6*(((angle-3584)&$1FFF ) / 2731)]   'rechts
    palette_map.LONG[$2D] := palette_map.LONG[$33+6*(((angle+2560)&$1FFF ) / 2731)]   'oben
    palette_map.LONG[$2E] := palette_map.LONG[$33+6*(((angle+ 512)&$1FFF ) / 2731)]   'links
    
    ' Tanzen
    CASE (angle >> 12)
      0:
        CASE (angle >> 9) // 4
          0:
            sprite_tbl[12] := (128 << 24) + (59 << 16)  + ($01)
            sprite_tbl[13] := @PlayerRightKick                     
            sprite_tbl[14] := (128 << 24) + (107 << 16)  + ($01)   
            sprite_tbl[15] := @PlayerRightKick                     
          1:                                                     
            sprite_tbl[12] := (128 << 24) + (64 << 16)  + ($01)    
            sprite_tbl[13] := @PlayerRight                         
            sprite_tbl[14] := (128 << 24) + (112 << 16)  + ($01)   
            sprite_tbl[15] := @PlayerRight                         
          2:                                                       
            sprite_tbl[12] := (128 << 24) + (69 << 16)  + ($01)    
            sprite_tbl[13] := @PlayerLeftKick                      
            sprite_tbl[14] := (128 << 24) + (117 << 16)  + ($01)   
            sprite_tbl[15] := @PlayerLeftKick
          3:                                                     
            sprite_tbl[12] := (128 << 24) + (64 << 16)  + ($01)    
            sprite_tbl[13] := @PlayerLeft                         
            sprite_tbl[14] := (128 << 24) + (112 << 16)  + ($01)   
            sprite_tbl[15] := @PlayerLeft                         

      1:                      
        CASE (angle >> 9) // 4
          0:
         '   sprite_tbl[12] := (128 << 24) + (59 << 16)  + ($01)
            sprite_tbl[13] := @PlayerRightFall                     
         '   sprite_tbl[14] := (128 << 24) + (107 << 16)  + ($01)   
            sprite_tbl[15] := @PlayerRightFall                     
          1:                                                     
          '  sprite_tbl[12] := (128 << 24) + (64 << 16)  + ($01)    
            sprite_tbl[13] := @PlayerRight                        
          '  sprite_tbl[14] := (128 << 24) + (112 << 16)  + ($01)   
            sprite_tbl[15] := @PlayerRight                         
                                                                   
          2:                                                       
          '  sprite_tbl[12] := (128 << 24) + (69 << 16)  + ($01)    
            sprite_tbl[13] := @PlayerLeftFall                      
          '  sprite_tbl[14] := (128 << 24) + (117 << 16)  + ($01)   
            sprite_tbl[15] := @PlayerLeftFall
          3:
         '   sprite_tbl[12] := (128 << 24) + (64 << 16)  + ($01)    
            sprite_tbl[13] := @PlayerLeft                         
         '   sprite_tbl[14] := (128 << 24) + (112 << 16)  + ($01)   
            sprite_tbl[15] := @PlayerLeft                         
 
        

     

PUB Sin(x) : winkel | quadrant
  '4098 Sinus-Eintraege
  '360 = 16388

' y = sin(x)
winkel := x & %1_1111_1111_1111  'auf 0 bis 359 setzen
quadrant := winkel>>11           'Quadrant ausrechnen 
winkel        &= %111_1111_1111  'auf 0 bis 89 setzen

CASE quadrant
  0:
    return  WORD[$E000 | (      winkel <<1)] 
  1:
    return  WORD[$E000 | ((2049-winkel)<<1)]
  2:
    return -WORD[$E000 | (      winkel <<1)]
  3:
    return -WORD[$E000 | ((2049-winkel)<<1)]
    
PUB scrollRight
  if( scrollPos < 3)
    scrollTo(scrollPos + 1)
           
PUB scrollLeft
  if( scrollPos > 0 )
    scrollTo(scrollPos -1)
           
PUB scrollReset
  scrollTo(1)

PUB scrollFollow
  'adapt scrolling
  if(scrollPos > playerX)
    scrollTo(playerX)
  if(scrollPos < playerX-7)
    scrollTo(playerX-7)

  
PUB scrollTo(position)

  'Level-Nr verschieben
  scrollPos := position
  tile_map_base_ptr_parm        := @gameField + (scrollPos<<1)

  'Erst schwarz machen
  drawRect( _ , 0,10,10,2)

  sprite_tbl[0] := (((PlayerY&15)+2) << 28) + (((PlayerX&15)+2 - scrollPos) << 20) + (0 << 8) + ($01)            
  
  repeat 4000
  drawLevelNumber(4,10)

PUB drawRect(palTile,x,y,width,height) | ix,iy
  ' Zeichnet ein Rechteck aus tiles
  REPEAT ix FROM x TO x+width -1
    REPEAT iy FROM y TO y+height -1
      WORD[tile_map_base_ptr_parm][ix+(iy<<4)] := palTile

PUB drawPassword(x,y) | passwd 'of currentLevel
    passwd := passwords[currentLevel-1]

    drawColoredDigit((passwd>>12)& $F,x,y)  
    drawColoredDigit((passwd>> 8)& $F,x+1,y)  
    drawColoredDigit((passwd>> 4)& $F,x+2,y)  
    drawColoredDigit( passwd     & $F,x+3,y)  

PUB drawColoredDigit(digit,x,y)
  drawDigit(digit,PAL_digitStart+digit,x,y)

PUB drawLevelNumber(x,y)
  if(currentLevel == 100)
    drawWhiteDigit(1,x-1,y)                        

  if(currentLevel => 10 )
    drawWhiteDigit((currentLevel /  10)//10,x,y)                        

  drawWhiteDigit(currentLevel // 10,x+1,y)                        

PUB drawWhiteDigit(digit,x,y)
  drawDigit(digit,PAL_whiteFont,x,y)

PUB drawDigit(digit,pal,x,y)
  WORD[tile_map_base_ptr_parm][x+(y<<4)] := (pal<<8) + TILE_digitStartUp + digit 
  WORD[tile_map_base_ptr_parm][x+((y+1)<<4)] := (pal<<8) + TILE_digitStartDown+digit


PUB hideAllSprites
  sprite_tbl[0] := 0
  sprite_tbl[1] := 0
  sprite_tbl[2] := 0
  sprite_tbl[3] := 0
  sprite_tbl[4] := 0
  sprite_tbl[5] := 0
  sprite_tbl[6] := 0
  sprite_tbl[7] := 0
  sprite_tbl[8] := 0
  sprite_tbl[9] := 0
  sprite_tbl[10]:= 0
  sprite_tbl[11] :=0
  sprite_tbl[12]:= 0
  sprite_tbl[13] :=0
  sprite_tbl[14]:= 0
  sprite_tbl[15] :=0



PUB checkPassword(password) | i

  REPEAT i FROM 0 TO 99
    if(password == passwords.WORD[i] )
      return i+1                        'passwort gefunden

  return 0                              'passwort ungueltig

  
PUB playEffectClose | i
  ' Ein cooler Effekt.
  ' Aehnlich einem sich schliessenden Kamera-Loch

  ' Aktuelles Bild kopieren
  REPEAT i FROM 0 TO 119
    effectScreen.WORD[i//10 + ((i/10)<<4)] :=     WORD[tile_map_base_ptr_parm][i//10 + ((i/10)<<4)]

  ' auf die Kopie zeigen 
  tile_map_base_ptr_parm := @effectScreen

  'Schwaerzen
  drawRect( TILE_effect ,0,0,10,1) ' erst die Leiste entfernen
  drawRect( TILE_effect ,0,11,10,1) ' erst die Leiste entfernen

  REPEAT i FROM 0 TO 5
    ' schwarzen Rahmen malen
    drawRect( TILE_effect ,i  , i+1,10-i, 1)  'oben
    drawRect( TILE_effect ,i  , i+1,   1,10-i)  'links
    drawRect( TILE_effect ,9-i, i+1,   1,10-i)  'rechts
    drawRect( TILE_effect ,i  ,10-i,10-i, 1)  'unten
    repeat 30000            'Verzoegerung

PUB playEffectZoom | i
  ' Ein cooler Effekt.

  ' Aktuelles Bild kopieren
  REPEAT i FROM 0 TO 119
    effectScreen.WORD[i//10 + ((i/10)<<4)] :=     WORD[tile_map_base_ptr_parm][i//10 + ((i/10)<<4)]

  ' auf die Kopie zeigen 
  tile_map_base_ptr_parm := @effectScreen


  REPEAT i FROM 0 TO 5
    ' schwarzen Rahmen malen
    drawRect( TILE_effect ,4-i  , 5-i,2+i<<1, 2+i<<1)  'oben
    repeat 30000            'Verzoegerung

  drawRect( TILE_effect ,0,0,10,1)  
  drawRect( TILE_effect ,0,11,10,1) 
  

PUB playLevelIntro(lev)    |  paintSecond , temp
  ' Spielt das Intro ab (Levelnummer und Passwort anzeigen)


  tile_map_base_ptr_parm := @levelIntro

  'drawRect( _ ,0,0,10,12)

      ' 1- 39:  normal      (gruen)
      '40- 69:  Wueste      (gelb/rot)
      '70-100:  Industriell (silber metallic)
      '                    Wgreen=$07_5f'56
      '  Wbrown=$08_60
      '   Wgrey=$09_61

  'Rahmen malen
  'Linken Rand
  CASE lev
    1..40:
      drawRect(Wgreen,0,0,5,1)
      drawRect(Wgreen,0,1,1,9)
      drawRect(Wgreen,0,11,5,1)
    41..70:
      drawRect(Wbrown,0,0,5,1)
      drawRect(Wbrown,0,1,1,9)
      drawRect(Wbrown,0,11,5,1)
    71..100:
      drawRect(Wgrey ,0,0,5,1)
      drawRect(Wgrey ,0,1,1,9)
      drawRect(Wgrey ,0,11,5,1)

       
  'Rechten Rand
  CASE lev
    1..39:
      drawRect(Wgreen,5,0,5,1)
      drawRect(Wgreen,9,1,1,9)
      drawRect(Wgreen,5,11,5,1)
    40..69:
      drawRect(Wbrown,5,0,5,1)
      drawRect(Wbrown,9,1,1,9)
      drawRect(Wbrown,5,11,5,1)
    70..100:
      drawRect(Wgrey ,5,0,5,1)
      drawRect(Wgrey ,9,1,1,9)
      drawRect(Wgrey ,5,11,5,1)


  'Levelnummer malen
  drawRect( _ ,3,4,3,2)       'alte Nummer schwarz uebermalen
  drawLevelNumber(4,4)        'neue Nummer malen

  'Passwort malen
  drawPassword(3,8)           'Passwort nach unten malen

  'Unteren Weg malen (Oranger Spieler)
  drawRect($0A_55,0,10,10,1)


  sprite_tbl[0] := 0
  sprite_tbl[1] := 0
  sprite_tbl[2] := 0'((12) << 28) + (( 16) << 16) ' + ($01)
  sprite_tbl[3] := 0'@PlayerRightWalk1
  sprite_tbl[4] := 0'((12) << 28) + (( 16) << 16) ' + ($01)
  sprite_tbl[5] := 0'@PlayerRightWalk1
  sprite_tbl[6] := 0'((12) << 28) + (( 16) << 16) ' + ($01)
  sprite_tbl[7] := 0'@PlayerRightWalk1
  sprite_tbl[8] := 0'((12) << 28) + (( 16) << 16) ' + ($01)
  sprite_tbl[9] := 0'@PlayerRightWalk1
  sprite_tbl[10]:= 0'((12) << 28) + (( 16) << 16) ' + ($01)
  sprite_tbl[11] :=0' @PlayerRightWalk1
  sprite_tbl[12]:= 0'((12) << 28) + (( 16) << 16) ' + ($01)
  sprite_tbl[13] :=0' @PlayerRightWalk1
  sprite_tbl[14]:= 0'((12) << 28) + (( 32) << 16)'  + ($01)
  sprite_tbl[15] :=0' @PlayerRightWalk1


'  Spieler laueft durchs Bild
  paintSecond := ((currentLevel => 30) AND currentLevel =<39) OR ((currentLevel => 60) AND currentLevel =<69) OR (currentLevel => 90) 

  aniCounter := 0
  repeat UNTIL aniCounter > 160'6
    'Orange geht vorraus
    sprite_tbl[2] := ((11) << 28) + ((  aniCounter+27) << 16)  + ($01)
    'sprite_tbl[3] := @PlayerRightWalk1 + (((aniCounter>>2)//4)<<7) 

    temp :=  (aniCounter>>2)//4 
    if(temp == 3)
      temp := 1
                
    sprite_tbl[3] := @PlayerRightWalk1 + (temp<<7)' + PlayerDir*386 '3 frames  '*(SPRITESIZE<<2)

    repeat 3750

    if(paintSecond)
      levelIntro[10<<4 + aniCounter>>4] :=     $0B_55 'B                                   

      'Turkis folgt    
      sprite_tbl[0] := ((11) << 28) + ((aniCounter+7) << 16)  + ($01)
      temp :=  (((aniCounter+2)>>2)+1)//4 
      if(temp == 3)
        temp := 1
    
      sprite_tbl[1] := @PlayerRightWalk1 + (temp<<7) '+ PlayerDir*386 '3 frames  '*(SPRITESIZE<<2)

    aniCounter += 1
    repeat 3750


  sprite_tbl[0] := 0 
  sprite_tbl[1] := 0 
  sprite_tbl[2] := 0 
  sprite_tbl[3] := 0 

PUB loadLevelData(newlev) | levelPointer, i, gi, themeIndex', addme  ',fieldPointer
  'Laed den Level aus dem Speicher.

  currentLevel := newlev
  tile_map_base_ptr_parm := @gameField


  'newlev von 1 bis 100
  
  'Level Stil:
  ' 1- 39:  normal      (gruen)
  '40- 69:  Wueste      (gelb/rot)
  '70-100:  Industriell (silber metallic)

  CASE newlev
     1.. 39:
      themeIndex := 0

      if(currentMusic <> 1)
        hdmf.play_song(Musik_Kraftwerk | hdmf#EEPROM_ASSET , 1 {loop})
        currentMusic := 1

    40.. 69:
      themeIndex := 1
      if(currentMusic <> 2)
        hdmf.play_song(Musik_Beethoven | hdmf#EEPROM_ASSET , 1 {loop})
        currentMusic := 2


    70..100:
      themeIndex := 2
      if(currentMusic<>3)
        hdmf.play_song(Musik_Rammstein | hdmf#EEPROM_ASSET , 1 {loop})
        currentMusic := 3


  'Das Thema anpassen
  'Die Wände bekommen eine andere Grafik
  i:= 0
  REPEAT UNTIL i == 16
    TileWall.LONG[i] := Tiles.LONG[(($5f+themeIndex)<<4) + i]
    i++

  'Die Paletten anpassen
  i:= 0
  REPEAT UNTIL i == 6  '8
    palette_map.LONG[i] := palette_normal.LONG[(themeIndex*6) + i]
    i++

  'Rahmen malen
  '  Oben und unten, sowie schwarzer hintergrund
  i:= 0
  REPEAT WHILE i < 13
    gameField[i] := W
    gameField[i+(9<<4)] := W
    gameField[i+(10<<4)] := _
    gameField[i+(11<<4)] := _
    i++

  scrollTo(1)
     
  '  Links und rechts
  i:=0
  REPEAT WHILE i < 8
    gameField[(i<<4)+16] := W  'magische zahlen
    gameField[(i<<4)+28] := W   '   ''
    i++
  
  i := 0      'Pointer im Level
  gi := 0     'Pointer im Grafikspeicher
  levelPointer := @levels + ((newlev - 1)*33)   'pointer wo geladen wird

  repeat while i < 33                      ' immer 3 bytes / 8 tiles auf einmal setzen
   '----
   '   ______________________
   '  -2--2  1--1--1  0--0--0
   '  -5  4--4--4  3--3--3  2-
   '   7--7--7  6--6--6  5--5-
             ' 0
    gameField[gi+17+5*(gi/11)] := triplet.WORD[  BYTE[levelPointer][ i ]                                          & 7 ]
    gi++     ' 1
    gameField[gi+17+5*(gi/11)] := triplet.WORD[ (BYTE[levelPointer][ i ] >> 3)                                    & 7 ]
    gi++     ' 2
    gameField[gi+17+5*(gi/11)] := triplet.WORD[((BYTE[levelPointer][ i ] >> 6) | (BYTE[levelPointer][i+1] << 2 )) & 7 ] 
    gi++     ' 3
    gameField[gi+17+5*(gi/11)] := triplet.WORD[ (BYTE[levelPointer][i+1] >> 1)                                    & 7 ]
    gi++     ' 4                                                                             
    gameField[gi+17+5*(gi/11)] := triplet.WORD[ (BYTE[levelPointer][i+1] >> 4)                                    & 7 ]
    gi++     ' 5
    gameField[gi+17+5*(gi/11)] := triplet.WORD[((BYTE[levelPointer][i+1] >> 7) | (BYTE[levelPointer][i+2] << 1 )) & 7 ]
    gi++     ' 6
    gameField[gi+17+5*(gi/11)] := triplet.WORD[ (BYTE[levelPointer][i+2] >> 2)                                    & 7 ]
    gi++     ' 7
    gameField[gi+17+5*(gi/11)] := triplet.WORD[ (BYTE[levelPointer][i+2] >> 5)                                        ]
    gi++


    
   '----
    i+= 3


  ' Monster zählen
  monstersLeft := 0


  ' Monster grafisch anpassen
  i := 0
  repeat while i < 88                      ' immer 3 bytes / 8 tiles auf einmal setzen

    if gameField[i+17+5*(i/11)] == M
      CASE gameField[i+1+5*(i/11)]
        M,N,R,J:
          gameField[i+17+5*(i/11)] := N        
      monstersLeft++

    if gameField[i+17+5*(i/11)] == F
      monstersLeft++
    i++

  ' Player auslesen : zweite Figur auslesen
  i := 0
  Players := 0
  repeat while i < 88
    if(gameField[i+17+5*(i/11)] == J)
      if Players > 0
        Player2X := PlayerX
        Player2Y := PlayerY
        gameField[Player2X+17+16*Player2Y] := JRT
      PlayerX := i // 11 ' +128
      PlayerY := i / 11  '+128
      gameField[i+17+5*(i/11)] := _
      Players++
    i++


    
  'Auf Player1 setzen
  currentPlayer := 0
  palette_map.LONG[0] := palette_map.LONG[$A]     

  Player2Dir := 0
  
  if PlayerX & 15 > 5
    PlayerDir := 1
  else
    PlayerDir := 0  
  updatePlayer


  

PUB togglePlayer
    'TODO Dieser Code ist eine einzige Katastrophe. Viel zu viel doppelt. :-(


    ' spieler in tile umwandeln
    ' palette ändern
    '  palette_map.LONG[0] := palette_map.LONG[7]     
    ' sprite neu positionieren
    ' tile zurückverwandeln
   ' CASE currentPlayer
     ' 0: 'orange

  ' Aktuellen Spieler in ein Tile umwandeln                                   
  if (getField(PlayerX,PlayerY) == L)  'wenn es eine leiter ist...            
    setField(PlayerX,PlayerY,JCO+currentPlayer<<8)    '...klettern            
  else                                                                        
    CASE getField(PlayerX,PlayerY-1)                                          
      _,L,W,F,S,JCT:                                                          
        if PlayerDir                                                          
          setField(PlayerX,PlayerY,JLO+currentPlayer<<8)                      
        else                                                                  
          setField(PlayerX,PlayerY,JRO+currentPlayer<<8)                      
      OTHER:                         ' dinge, die fallen können               
        if PlayerDir                                                          
          setField(PlayerX,PlayerY,JLOH+currentPlayer<<8)                     
        else                                                                  
          setField(PlayerX,PlayerY,JROH+currentPlayer<<8)                     

         
  CASE currentPlayer
    0:

         
      'Sprite-Daten sichern         
      player1X := playerX
      player1Y := playerY
      player1Dir:=playerDir

      'Spielerfarbe ändern
      palette_map.LONG[0] := palette_map.LONG[$B]
         
      'anderen Spieler in Sprite umwandeln (Dazu Tile in anderes Tile konvertieren)
      CASE getField(player2X,player2Y)
        JCT:
          setField(player2X,player2Y,L)  'klettern -> Leiter
        OTHER:
          setField(player2X,player2Y,_)  'sonstiges-> freier Raum

                'Spieler-Position swappen
      playerX := player2X
      playerY := player2Y
      playerDir := player2Dir
      updatePlayer
      currentPlayer := 1
    
               
    1: 'turkis
      'Sprite-Daten sichern         
      player2X := playerX
      player2Y := playerY
      player2Dir:=playerDir

      'Spielerfarbe ändern
      palette_map.LONG[0] := palette_map.LONG[$A]

      'anderen Spieler in Sprite umwandeln (Dazu Tile in anderes Tile konvertieren)
      CASE getField(player1X,player1Y)
        JCO:
          setField(player1X,player1Y,L)  'klettern -> Leiter
        OTHER:
          setField(player1X,player1Y,_)  'sonstiges-> freier Raum

      'Spieler-Position swappen
      playerX := player1X
      playerY := player1Y
      playerDir := player1Dir
      updatePlayer
      currentPlayer := 0             
    





  

'///////////////////  Bewegungskommandos


'-------------------------





    
' Animationen und bewegungen
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@



     
PRI walk | temp    'TODO nochmal richtig schreiben (wie jump)
    aniCounter := 0
    repeat UNTIL aniCounter > 15
      'TODO hier noch irgendwie ne Animation einbauen
      temp :=  (aniCounter>>2)//4 
      if(temp == 3)
        temp := 1
    
      sprite_tbl[0] := (((PlayerY&15)+2) << 28) + (((((PlayerX&15)+2 -scrollPos)<<4) + (1-(PlayerDir<<1))*aniCounter) << 16) + (0 << 8) + ($01)
      sprite_tbl[1] := @PlayerRightWalk1 + (temp<<7) + PlayerDir*386 '3 frames  '*(SPRITESIZE<<2)
                
      aniCounter += 1
      repeat 7500
      if(aniCounter == 8)
        'Sand wegbuddeln
        if(getField(PlayerX+1-PlayerDir<<1,PlayerY) == S)
          setField(PlayerX+1-PlayerDir<<1,PlayerY,_) 
        'Monstergrafik aendern
        CASE getField(PlayerX,PlayerY+1)
          N:
           setField(PlayerX,PlayerY+1,M)
          JROH:
           setField(PlayerX,PlayerY+1,JRO)
          JLOH:
            setField(PlayerX,PlayerY+1,JLO)
          JRTH:
            setField(PlayerX,PlayerY+1,JRT)
          JLTH:
            setField(PlayerX,PlayerY+1,JLT)
        temp := PlayerX+1-PlayerDir<<1
        CASE getField(temp,PlayerY+1) 
          M:
           setField(temp,PlayerY+1,N)
          JRO:
           setField(temp,PlayerY+1,JROH)
          JLO:
            setField(temp,PlayerY+1,JLOH)
          JRT:
            setField(temp,PlayerY+1,JRTH)
          JLT:
            setField(temp,PlayerY+1,JLTH)

        temp := 1-(PlayerDir<<1)
        PlayerX+= temp
        scrollFollow
        PlayerX-= temp


    PlayerX+=1-(PlayerDir<<1)
    
    'savePlayerPos
    PlayerXtemp := PlayerX
    PlayerYtemp := PlayerY
    if getField(PlayerX,PlayerY) <> L 
      fallPlayer
    updatePlayer                'falls er gerade unter was schwerem steht      
    fallStack(PlayerXtemp-1+(PlayerDir<<1),PlayerYtemp-1)
    fallStack(PlayerXtemp,PlayerYtemp-1)
    updatePlayer                'falls ihm was auf den kopf gefallen ist






PRI jump  | killFlag
    killFlag := 0
    aniCounter := 0
    repeat UNTIL aniCounter > 7
      sprite_tbl[0] := (((((PlayerY&15)+2)<<4) - jumpNums[aniCounter])<< 24) + (((((PlayerX&15)+2 -scrollPos)<<4) +(1-(PlayerDir<<1))*aniCounter) << 16) + (0 << 8) + ($01)
      sprite_tbl[1] := @PlayerRightJump + PlayerDir*SPRITESIZE' + (((aniCounter>>2)//4)<<7)
      
      aniCounter += 1
      repeat 5000

    'Mitte des Sprungs. Zunächst die Monstergrafik anpassen
    CASE getField(PlayerX,PlayerY+1)
      N:
        setField(PlayerX,PlayerY+1,M)
      JROH:
        setField(PlayerX,PlayerY+1,JRO)
      JLOH:
        setField(PlayerX,PlayerY+1,JLO)
      JRTH:
        setField(PlayerX,PlayerY+1,JRT)
      JLTH:
        setField(PlayerX,PlayerY+1,JLT)

    'Jetzt die Position updaten
    PlayerX+= 1 - (PlayerDir<<1)

    scrollFollow
                                                        
    
    ' Sand wegbuddeln
    CASE getField(PlayerX,PlayerY)
      S:
        setField(PlayerX,PlayerY,_)


      
    repeat UNTIL aniCounter > 15
      'TODO hier noch irgendwie ne Animation einbauen
    
      sprite_tbl[0] := (((((PlayerY&15)+2)<<4) - jumpNums[aniCounter])<< 24) + (((((PlayerX&15)+2 -scrollPos)<<4) -(1-(PlayerDir<<1))*(16-aniCounter)) << 16) + (0 << 8) + ($01)
      sprite_tbl[1] := @PlayerRightJump + PlayerDir*SPRITESIZE
      aniCounter += 1
      repeat 5000

    CASE getField(PlayerX,PlayerY)
      M,N,F:
        setField(PlayerX,PlayerY,BANG)
        monstersLeft := monstersLeft -1 '--   'ein monster weniger
        killFlag := 1
        sprite_tbl[0] &= $FF_FF_FF_FE    'Player unsichtbar machen

    
'    PlayerX+=1-(PlayerDir<<1)
    if killFlag                 'BANG?
      repeat 100000             'Bang zeigen
      sprite_tbl[0] |= 1        'Spieler wieder sichtbar machen
      setField(PlayerX,PlayerY,_)
      

    if(monstersLeft < 1)
      return
      
    'savePlayerPos
    PlayerXtemp := PlayerX
    PlayerYtemp := PlayerY
    if getField(PlayerX,PlayerY) <> L
      fallPlayer
    fallStack(PlayerXtemp-1+(PlayerDir<<1),PlayerYtemp-1)
    fallStack(PlayerXtemp,PlayerYtemp-1)

    updatePlayer

 
PRI kick                   ' einen Stein kicken
  sprite_tbl[1] := @PlayerRightKick + PlayerDir * SPRITESIZE
  
  'Stein-Sprite positionieren
  sprite_tbl[2] := (((((PlayerY&15)+2)<<4) ) << 24) + (((((PlayerX&15)+3-(PlayerDir<<1)-scrollPos)<<4)) << 16) + (0 << 8) + ($01)  
  sprite_tbl[3] := @Rock
  
  'tiles vorbereiten
  gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +18 - 3*PlayerDir] := ROCKSLIDE             '      kick!
  gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +19 - 3*PlayerDir] := ROCKSLIDE


  ' Den Stein animiert verschieben
  aniCounter := 1                                       ' ein Feld fallen
  repeat UNTIL aniCounter > 10
    sprite_tbl[2] := (((((PlayerY&15)+2)<<4) ) << 24) + (((((PlayerX&15)+3-(PlayerDir<<1)-scrollPos)<<4)+(1-(PlayerDir<<1))*aniCounter) << 16) + (0 << 8) + ($01)

    repeat aniCounter<<10 'slideNums[aniCounter]

    aniCounter++

  updatePlayer
  ' Monstergrafik aendern
  if(gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) + 34-(PlayerDir<<1)] == N)
    gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) + 34-(PlayerDir<<1)] := M
  if(gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) + 35-(PlayerDir<<2)] == M)
    gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) + 35-(PlayerDir<<2)] := N


  repeat UNTIL aniCounter > 16
    sprite_tbl[2] := (((((PlayerY&15)+2)<<4) ) << 24) + (((((PlayerX&15)+3-(PlayerDir<<1)-scrollPos)<<4)+(1-(PlayerDir<<1))*aniCounter) << 16) + (0 << 8) + ($01)

    repeat aniCounter<<10 'slideNums[aniCounter]

    aniCounter++

 ' Wieder in ein Tile umwandeln
  gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +17 +2 - (PlayerDir<<2)] := R             '      kick!
  gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +17 +1 - (PlayerDir<<1)] := _
  ' Sprite wieder ausblenden
  sprite_tbl[2] := 0   


  ' Kram fallen lassen
  fallRock((PlayerX&15)+2-(PlayerDir<<2),(PlayerY&15))
  fallStack((PlayerX&15)+1-(PlayerDir<<1),(PlayerY&15)-1)

      


      
PRI climbDown

  aniCounter := 0
    repeat UNTIL aniCounter > 3'15
      'TODO hier noch irgendwie ne Animation einbauen
    
      sprite_tbl[0] := (((((PlayerY&15)+2) << 2)+aniCounter)<<26) + ((((PlayerX&15)+2 -scrollPos)<<4)  << 16) + (0 << 8) + ($01)
      sprite_tbl[1] := @PlayerClimb1 + ((aniCounter//2)<<7)
      
      aniCounter += 1
      repeat 20000

  PlayerY++
  fallStack(PlayerX,PlayerY-2)
'  fallPlayer  
  updatePlayer



PRI climbUp
  aniCounter := 0
    repeat UNTIL aniCounter > 3'15
      'TODO hier noch irgendwie ne Animation einbauen
    
      sprite_tbl[0] := (((((PlayerY&15)+2) << 2)-aniCounter)<<26) + ((((PlayerX&15)+2 -scrollPos)<<4)  << 16) + (0 << 8) + ($01)
      sprite_tbl[1] := @PlayerClimb1 + ((aniCounter//2)<<7)
      
      aniCounter += 1
      repeat 20000

  PlayerY--  
  updatePlayer


PRI updatePlayer | PlayerField, below   'Nur Grafik
  ' Auf Leiter: Klettern
  ' Sonst:
  '   Wenn ueber ihm Stein/Monster -> Halten
  '   Sonst: stehen
  PlayerField := getField(playerX,playerY) 'gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +17]
  below := gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +33]  
  if PlayerField == L  'PlayerPos
    if below <> W AND below <> S
      sprite_tbl[1] := @PlayerClimb1
    else
      sprite_tbl[1] := @PlayerRight + ( PlayerDir << 7)
  else
    CASE gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +1]
      M,N,R,JRO,JLO,JRT,JLT:
        sprite_tbl[1] := @PlayerRightHold + ( PlayerDir << 7)
      OTHER:
        sprite_tbl[1] := @PlayerRight + ( PlayerDir << 7)

  sprite_tbl[0] := (((PlayerY&15)+2) << 28) + (((PlayerX&15)+2 - scrollPos) << 20) + ($01)  

  if PlayerField<>L
    CASE below
      M:
        gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +33] := N
      JRO:
        gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +33] := JROH
      JLO:
        gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +33] := JLOH
      JRT:
        gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +33] := JRTH
      JLT:
        gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +33] := JLTH


  'adapt scrolling
  if(scrollPos > playerX)
    scrollTo(playerX)
  if(scrollPos < playerX-7)
    scrollTo(playerX-7)

  
   
PRI fallPlayer
  sprite_tbl[1] := @PlayerRightFall + (PlayerDir<<7)

  aniIncrement := 0   ' sollte eigentlich Logarithmisch/Wurzel sein. Vielleicht später aus dem ROM lesen?
  ' Fallen, so lange nichts im Weg ist
  repeat WHILE gameField.WORD[(PlayerX&15) + ((PlayerY&15)<<4) +33] == _                ' unter Player
    aniCounter := 0                                       ' ein Feld fallen
    repeat UNTIL aniCounter > 15
      sprite_tbl[0] := (((((PlayerY&15)+2)<<4) +aniCounter) << 24) + (((((PlayerX&15)+2-scrollPos)<<4)) << 16) + (0 << 8) + ($01)

      repeat fallNums[aniIncrement]

      aniIncrement++
      aniCounter++
    PlayerY++

  if PlayerDir
    sprite_tbl[1] := @PlayerLeft
  else
    sprite_tbl[1] := @PlayerRight

PRI fallRock(rX,rY) | topX,topY,bottomX, bottomY
                  
  'Stein-Sprite positionieren
  sprite_tbl[2] := (((rY+1)<<4) << 24) + (( (rX-scrollPos+1) <<4) << 16) + (0 << 8) + ($01)  
  sprite_tbl[3] := @Rock

  'tiles vorbereiten
  topX := rX
  topY := rY
  bottomX := rX
  bottomY := rY
  gameField.WORD[bottomX+(bottomY<<4)+17] := ROCKSLIDE

  'Den Fall-Bereich auf die korrekte Palette setzen
  repeat while (getField(bottomX,bottomY+1) == _) AND (PlayerX <> bottomX OR PlayerY <> bottomY+1)
    bottomY++
    gameField.WORD[bottomX+(bottomY<<4)+17] := ROCKSLIDE    
    
  'Den Stein im Paletten-Schacht fallen lassen
  aniIncrement := 0  
  ' Fallen, so lange nichts im Weg ist
  repeat WHILE gameField.WORD[rX + (rY<<4)+33] == ROCKSLIDE   ' unter der aktuellen Steinposition
    aniCounter := 0                                       ' ein Feld fallen
    repeat UNTIL aniCounter > 15
      sprite_tbl[2] := ((((rY+2)<<4) +aniCounter) << 24) + ((((rX+2-scrollPos)<<4)) << 16) + (0 << 8) + ($01)

      repeat fallNums[aniIncrement]

      aniIncrement++
      aniCounter++
    rY++


  ' Falls der Stein auf ein Monster fällt muss dieses den Stein halten
  if(gameField.WORD[bottomX + (bottomY<<4) + 33] == M)
    gameField.WORD[bottomX + (bottomY<<4) + 33] := N

 
  ' Wieder in ein Tile umwandeln
  gameField.WORD[bottomX + (bottomY<<4) + 17] := R             '      kick!
  if getField(bottomX,bottomY+1) == M
    setField(bottomX,bottomY+1,N)
  'Palette wieder normalisieren
  bottomY--
  repeat while gameField.WORD[bottomX + (bottomY<<4) + 17] == ROCKSLIDE
    gameField.WORD[bottomX + (bottomY<<4) + 17] := _
    bottomY--    

  ' Sprite wieder ausblenden
  sprite_tbl[2] := 0   
  


PRI fallStack(sX,sY) | bottomOfStack, belowStack, tempField , topX, topY, bottomX, bottomY  , spriteCounter,i  ' ,rX, rY
  'Koordinaten sind in Level-Modus (0..10, 0..7)

  'Diese Funktion lässt einen Objekt-Stapel fallen. Als Parameter wird
  'die Position des untersten Objektes des Stapels übergeben.
  'Der Code ist etwas kompliziert, das es so etwas wie "Objekte"
  'garnicht gibt. Stattdessen wird der Grafik-Speicher, der auch
  'gleichzeitig der Level ist, analysiert. Hierbei werden Tiles
  'in Sprites umgewandelt, bewegt, und schliesslich in andere
  'Tiles zurueckkonvertiert.

  'tempField sei das unterste Objekt des Stapels
  'tempField := getField(sX,sY)

  'Es geht um den Boden des Stapels, sowie was sich darunter befindet
  bottomOfStack := getField(sX,sY)
  'kann es überhaupt fallen?
  CASE bottomOfStack
    R,M,N,JRO,JROH,JLO,JLOH,JRT,JRTH,JLT,JLTH:  'Liste der Objekte, die überhaupt fallen können
      'okay, kann fallen
    OTHER:
      return
  
  belowStack := getField(sX,sY+1)
  CASE belowStack
    _:
      'alles okay, der Stapel darf fallen
    OTHER:
      'kein Platz, also Schluss
      return
  'Sonderfall: Spielfigur steht im Weg
  if(PlayerX==sX AND PlayerY==sY+1)
    return

  'unter dem Stapel ist Platz. Also prüfen, ob überhaupt etwas fallen kann...  
                                                        '  if ( (tempField <> R) AND (tempField <> M) AND (tempField <> N) ) OR (getField(sX,sY+1) <> _) OR (PlayerX==sX AND PlayerY==sY+1)
                                                        '    return
  
  'Sonderfall: Nur ein Stein fällt
'  tempField := getField(sX,sY-1)'gameField.WORD[(sX&15)+((sY&15)<<4)+1] 'das darüberliegende Feld
  if  (getField(sX,sY) == R)
    CASE getField(sX,sY-1)'tempField                                      ' AND ((tempField <> R) AND (tempField <> M) AND (tempField <> N))
      _,W,L,S,F:         'Wenn es nicht fallen kann... TODO: Spieler, der an Leiter hängt (zweifarbig)
        fallRock(sX,sY)  '...kann man zur schöneren Stein-Funktion wechseln...
        return           '...und ist schon fertig

  'Ab hier wird es kompliziert.
  'Zunächst muss der Stapel als zusammenhängender
  'Block erkannt, und in Sprites konvertiert werden       
  
  'palette aender
  'stapel fallen lassen
  'palette wieder normal machen

  'Stein-Sprite positionieren
  'sprite_tbl[2] := (((rY+1)<<4) << 24) + (( (rX-scrollPos+1) <<4) << 16) + (0 << 8) + ($01)  

  'tiles vorbereiten
  topX := sX
  topY := sY
  bottomX := sX
  bottomY := sY
'  gameField.WORD[bottomX+(bottomY<<4)+17] := ROCKSLIDE

  'Den Fall-Bereich auf die korrekte Palette setzen
  'Objekte können nur in freien Bereich fallen und auch nur dann, wenn Player nicht im Weg steht
  repeat while (getField(bottomX,bottomY+1) == _) AND (PlayerX <> bottomX OR PlayerY <> bottomY+1)
    bottomY++
    gameField.WORD[bottomX+(bottomY<<4)+17] := FALLERS 'Spezialpalette    
  'Der Weg nach unten ist nun frei.
  'Jetzt muss nach oben hin gescant werden...


  'Die Tiles in Sprites konvertieren und gleichzeitig die Palette richtig setzen
  spriteCounter := 0
  topY++
  repeat
    topY--
    spriteCounter++
    sprite_tbl[spriteCounter<<1] := ((((topY+2)<<4) ) << 24) + (((topX+2-scrollPos)<<4)<<16) + (0 << 8) + ($01)
    CASE getField(topX,topY)
      M,N:
        sprite_tbl[1+(spriteCounter<<1)] := @MonsterFall
      R:
        sprite_tbl[1+(spriteCounter<<1)] := @RockFall
      JRO,JROH,JRT,JRTH:
        sprite_tbl[1+(spriteCounter<<1)] := @PlayerRightFall
      JLO,JLOH,JLT,JLTH:
        sprite_tbl[1+(spriteCounter<<1)] := @PlayerLeftFall
    setField(topX,topY,FALLERS)

    CASE getField(topX,topY-1)
      R,N,M,JRO,JROH,JLO,JLOH,JRT,JRTH,JLT,JLTH:  'alles was fallen kann
        NEXT
      OTHER:
        QUIT
       
  
  'Den ganzen Kram fallen lassen
  ' Fallen, so lange nichts im Weg ist
  repeat aniIncrement FROM 0 TO ((bottomY - sY)<<4)     'so weit fällt es (in pixel)
    repeat i From 1 TO spriteCounter                    'so viele sprites müssen bewegt werden
      sprite_tbl[i<<1] += (1 << 24) '+ (((topX+2-scrollPos)<<4)<<16) + (0 << 8) + ($01)
    repeat fallNums[aniIncrement]

  'jetzt wieder in Tiles umwandeln

  repeat i from 1 to spriteCounter
    case sprite_tbl[1+(i<<1)]
      @MonsterFall:
        setField(bottomX,bottomY+1-i,N)
      @RockFall:
        setField(bottomX,bottomY+1-i,R)
      @PlayerRightFall:
        CASE currentPlayer
          0:
            setField(bottomX,bottomY+1-i,JRTH) 'TODO: korrekte Farbe, basierend auf aktiver Figur
            player2Y:=bottomY+1-i
          1:
            setField(bottomX,bottomY+1-i,JROH) 'TODO: korrekte Farbe, basierend auf aktiver Figur
            player1Y:=bottomY+1-i
        
      @PlayerLeftFall:
        CASE currentPlayer
          0:
            setField(bottomX,bottomY+1-i,JLTH) 'TODO: korrekte Farbe, basierend auf aktiver Figur
            player2Y:=bottomY+1-i
          1:
            setField(bottomX,bottomY+1-i,JLOH) 'TODO: korrekte Farbe, basierend auf aktiver Figur
            player1Y:=bottomY+1-i
      
    sprite_tbl[i<<1] := 0
                    
  ' Optimierung um Speicher zu sparen
  i := bottomY+1-spriteCounter 

  ' Oberstes hält nichts
  CASE getField(bottomX,i)
    N:
      setField(bottomX,i,M)
    JROH:
      setField(bottomX,i,JRO)
    JLOH:
      setField(bottomX,i,JLO)
    JRTH:
      setField(bottomX,i,JRT)
    JLTH:
      setField(bottomX,i,JLT)

  i := bottomY+1
  ' Unter dem Stapel muss jetzt was halten
  CASE getField(bottomX,i)
    M:
      setField(bottomX,i,N)
    JRO:
      setField(bottomX,i,JROH)
    JLO:
      setField(bottomX,i,JLOH)
    JRT:
      setField(bottomX,i,JRTH)
    JLT:
      setField(bottomX,i,JLTH)

  
  'die darüberliegenden Felder wieder in _ umwandeln
  repeat i from topY to bottomY - spriteCounter
    setField(topX,i,_)




              
PUB getField(fX,fY)
'  return gameField.WORD[(fX&15)+((fY&15)<<4)+17]
  return gameField.WORD[fX+(fY<<4)+17]

PUB setField(fX,fY,tile)
  gameField.WORD[fX+(fY<<4)+17] := tile 

  
PUB animate_tiles | flip              'läuft in eigenem COG und animiert die Tiles der HEL engine unelegant
                                'eigentlich sollte man eine weiter Indirektion einbauen, allerdings
                                'ist es "einfacher" die Tile-Grafik zu überschreiben, anstatt
                                'die HEL engine umzuschreiben. Zumindest für mich :)

  flip := 0 
  repeat                                                        'Tiles werden in Endlosschleife animiert

    count := 0                                                  '1 Tile = 16 LONGs
    repeat UNTIL count > 15
      TileMonster.LONG[count] := AniMonster1.LONG[count+flip]     'Monster (normal)
      TileMonsterHold.LONG[count] := AniMonsterHold1.LONG[count+flip]         'Monster (etwas haltend)
      TileGhost.LONG[count] := AniGhost1.LONG[count+flip<<1]     'Geist (normal)

      count++

    repeat 100000
    flip ^= 16                                                               

DAT 'Spielfeld
'{
gameField     word $0B_00,$0B_01,$0B_02,$0B_03,$0B_04,$0B_05,$0B_00,$0B_00,$0B_00,$0B_00,_,_,_,_,_,_
              word $0B_00,$0B_06,$0B_07,$0B_08,$0B_09,$0B_0a,$0B_00,$0B_00,$0B_00,$0B_00,_,_,_,_,_,_
              word $0B_00,$0B_0b,$0B_0c,$0B_0d,$0B_0e,$0B_0f,$0B_00,$0B_00,$0B_00,$0B_00,_,_,_,_,_,_
              word $0B_00,$0B_00,$0B_10,$0B_11,$0B_12,$0B_13,$0B_00,$0B_00,$0B_00,$0B_00,_,_,_,_,_,_
              word $0B_00,$0B_00,$0B_14,$0B_15,$0B_16,$0B_17,$0B_18,$0B_00,$0B_00,$0B_00,_,_,_,_,_,_
              word $0B_00,$0B_00,$0B_19,$0B_1a,$0B_1b,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,_,_,_,_,_,_
              word $0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,_,_,_,_,_,_
              word $0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,_,_,_,_,_,_
              word $0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,_,_,_,_,_,_
              word $0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,_,_,_,_,_,_
              word $0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,_,_,_,_,_,_
              word $0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,$0B_00,_,_,_,_,_,_
'}

effectScreen  word R     ,R     ,R     ,R     ,R     ,R     ,R     ,R     ,R     ,R     ,_,_,_,_,_,_
              word R     ,_     ,_     ,_     ,_     ,_     ,R     ,R     ,R     ,R     ,_,_,_,_,_,_
              word R     ,_     ,R     ,_     ,R     ,_     ,R     ,R     ,R     ,R     ,_,_,_,_,_,_
              word R     ,_     ,R     ,_     ,R     ,_     ,R     ,R     ,R     ,R     ,_,_,_,_,_,_
              word R     ,_     ,R     ,_     ,R     ,_     ,R     ,R     ,R     ,R     ,_,_,_,_,_,_
              word R     ,R     ,R     ,R     ,R     ,R     ,R     ,R     ,R     ,R     ,_,_,_,_,_,_
              word R     ,R     ,R     ,R     ,R     ,_     ,_     ,_     ,R     ,R     ,_,_,_,_,_,_
              word R     ,R     ,R     ,R     ,R     ,R     ,R     ,_     ,R     ,R     ,_,_,_,_,_,_
              word R     ,R     ,R     ,R     ,R     ,R     ,_     ,R     ,R     ,R     ,_,_,_,_,_,_
              word R     ,R     ,R     ,R     ,R     ,_     ,R     ,R     ,R     ,R     ,_,_,_,_,_,_
              word R     ,R     ,R     ,R     ,R     ,_     ,_     ,_     ,R     ,R     ,_,_,_,_,_,_
              word R     ,R     ,R     ,R     ,R     ,R     ,R     ,R     ,R     ,R     ,_,_,_,_,_,_


' Titelbild
'{
mainMenu      word _     ,$0D_00,$0D_01,$0D_02,$0D_03,$0D_04,$0D_05,_     ,_     ,_     ,  _,R,_,_,_,R
              word _     ,_     ,$0D_06,$0D_07,$0D_08,$0D_09,$0D_0a,_     ,_     ,_     ,  _,R,R,_,R,R
              word _     ,_     ,$0D_0B,$0D_0c,$0D_0d,$0D_0e,$0D_0f,_     ,_     ,_     ,  _,R,_,R,_,R
              word _     ,_     ,_     ,$0D_10,$0D_11,$0D_12,$0D_13,_     ,_     ,_     ,  _,R,_,R,_,R
              word _     ,_     ,_     ,$0D_14,$0D_15,$0D_16,_     ,_     ,_     ,_     ,  _,R,_,R,_,R
              word _     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,  _,_,_,_,_,_
              word _     ,_     ,_     ,$13_55,$13_24,$13_25,$13_55,_     ,_     ,_     ,  _,_,_,_,_,_
              word $0A_6A,_     ,_     ,$14_26,$14_27,$14_28,$14_29,_     ,_     ,$04_63,  _,R,R,R,R,R
              word W     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,R     ,  _,_,_,_,R,_
              word W     ,W     ,_     ,_     ,_     ,_     ,_     ,_     ,R     ,R     ,  _,_,_,R,_,_
              word W     ,R     ,R     ,W     ,W     ,W     ,R     ,W     ,W     ,W     ,  _,_,R,_,_,_
              word R     ,W     ,$11_2A,$11_2B,$11_2C,$12_2A,$12_2B,$12_2C,W     ,R     ,  _,R,R,R,R,R
'}
                       
' Level geladen
'{
levelIntro    word Wgreen,Wgreen,Wgreen,Wgreen,Wgreen,Wbrown,Wbrown,Wbrown,Wbrown,Wbrown,  _,_,_,_,_,_
              word Wgreen,_     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,Wbrown,  _,_,_,_,_,_
              word Wgreen,_     ,_     ,$0D_2D,$0D_2E,$0D_2F,$0D_30,_     ,_     ,Wbrown,  _,_,_,_,_,_
              word Wgreen,_     ,_     ,$0D_31,$0D_32,$0D_33,$0D_34,_     ,_     ,Wbrown,  _,_,_,_,_,_
              word Wgreen,_     ,_     ,_     ,$0D_39,$0D_35,_     ,_     ,_     ,Wbrown,  _,_,_,_,_,_
              word Wgreen,_     ,_     ,_     ,$0D_43,$0D_3F,_     ,_     ,_     ,Wbrown,  _,_,_,_,_,_
              word Wgreen,_     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,Wbrown,  _,_,_,_,_,_
              word Wgreen,_     ,_     ,$0D_26,$0D_27,$0D_28,$0D_29,_     ,_     ,Wbrown,  _,_,_,_,_,_
              word Wgreen,_     ,_     ,$0D_39,$0D_3A,$0D_3B,$0D_3C,_     ,_     ,Wbrown,  _,_,_,_,_,_
              word Wgreen,_     ,_     ,$0D_43,$0D_44,$0D_45,$0D_46,_     ,_     ,Wbrown,  _,_,_,_,_,_
              word $0A_55,$0A_55,$0A_55,$0A_55,$0A_55,$0A_55,$0A_55,$0A_55,$0A_55,$0A_55,  _,_,_,_,_,_
              word Wgreen,Wgreen,Wgreen,Wgreen,Wgreen,Wbrown,Wbrown,Wbrown,Wbrown,Wbrown,  _,_,_,_,_,_
'}


'Pause-Menue
'{
pauseMenu     word _     ,_     ,_     ,$15_17,_     ,_     ,_     ,_     ,_     ,_     ,  _,_,_,_,_,_
              word _     ,_     ,_     ,$15_18,$15_19,$15_1A,_     ,_     ,_     ,_     ,  _,_,_,_,_,_
              word _     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,  _,_,_,_,_,_
              word _     ,_     ,_     ,$16_1B,$16_1C,$16_1D,$16_55,_     ,_     ,_     ,  _,_,_,_,_,_
              word _     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,  _,_,_,_,_,_
              word _     ,_     ,_     ,$17_1E,$17_1F,$17_20,$17_21,_     ,_     ,_     ,  _,_,_,_,_,_
              word _     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,  _,_,_,_,_,_
              word _     ,_     ,_     ,$18_22,$18_23,$18_55,$18_55,_     ,_     ,_     ,  _,_,_,_,_,_
              word _     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,_     ,  _,_,_,_,_,_
              word _     ,_     ,_     ,$19_26,$19_27,$19_28,$19_29,_     ,_     ,_     ,  _,_,_,_,_,_
              word _     ,_     ,_     ,$0D_3A,$0D_3B,$0D_3C,$0D_3D,_     ,_     ,_     ,  _,_,_,_,_,_
              word _     ,_     ,_     ,$0D_44,$0D_45,$0D_46,$0D_47,_     ,_     ,_     ,  _,_,_,_,_,_

        '}



'Sieg
victoryScreen word $2D_55,$2D_55,$2D_55,$2D_55,$2D_55,$2D_55,$2D_55,$2C_55,$2C_55,$2C_55,  _,_,_,_,_,_
              word $2D_55,$2D_55,$2D_55,$2D_55,$2D_55,$2D_55,$2D_55,$2C_55,$2C_55,$2C_55,  _,_,_,_,_,_
              word $2E_55,$2E_55,$2E_55,$10_49,$10_4A,$10_4B,$10_4C,$2C_55,$2C_55,$2C_55,  _,_,_,_,_,_
              word $2E_55,$2E_55,$2E_55,$10_4D,$10_4E,$10_4F,$10_50,$2C_55,$2C_55,$2C_55,  _,_,_,_,_,_
              word $2E_55,$2E_55,$2E_55,$10_51,$10_52,$10_53,$10_54,$2C_55,$2C_55,$2C_55,  _,_,_,_,_,_
              word $2E_55,$2E_55,$2E_55,$2B_55,$2B_55,$2B_55,$2B_55,$2B_55,$2B_55,$2B_55,  _,_,_,_,_,_
              word $2E_55,$2E_55,$2E_55,$2B_55,$2B_55,$2B_55,$2B_55,$2B_55,$2B_55,$2B_55,  _,_,_,_,_,_
              word _     ,_     ,_     ,$0A_55,_     ,_     ,$0B_55,_     ,_     ,_     ,  _,_,_,_,_,_
              word _     ,_     ,_     ,$31_57,_     ,_     ,$3d_57,_     ,_     ,_     ,  _,_,_,_,_,_
              word _     ,Mpink ,_     ,$1c_57,_     ,Mblue ,$37_57,_     ,Mgreen,_     ,  _,_,_,_,_,_
              word $06_57,$10_57,$1d_57,$25_57,$22_57,$1b_57,$21_57,$31_57,$1e_57,$06_57,  _,_,_,_,_,_
              word $1a_57,$25_57,$24_57,$10_57,$1f_57,$06_57,$23_57,$1c_57,$10_57,$20_57,  _,_,_,_,_,_


         
'Testbild Schrift
{
gameField     word $0D_17,$0D_1B,$0D_1C,$0D_1D,$0D_00,$0D_01,$0D_02,$0D_03,$0D_04,$0D_05,_,_,_,_,_,_
              word $0D_18,$0D_19,$0D_1A,_     ,_     ,$0D_06,$0D_07,$0D_08,$0D_09,$0D_0a,_,_,_,_,_,_
              word _     ,_     ,_     ,_     ,_     ,$0D_0B,$0D_0c,$0D_0d,$0D_0e,$0D_0f,  _,_,_,_,_,_
              word _     ,_     ,_     ,_     ,_     ,_     ,$0D_10,$0D_11,$0D_12,$0D_13,  _,_,_,_,_,_
              word _     ,_     ,_     ,_     ,_     ,_     ,$0D_14,$0D_15,$0D_16,_, _,_,_,_,_,_
              word $0D_1E,$0D_1F,$0D_20,$0D_21,_     ,_     ,_     ,_     ,_     ,_     ,  _,_,_,_,_,_
              word $0D_26,$0D_27,$0D_28,$0D_29,_     ,_     ,_     ,_     ,_     ,_     ,  _,_,_,_,_,_
              word $0E_2A,$0E_2B,$0E_2C,$0F_2A,$0F_2B,$0F_2C,$0D_49,$0D_4A,$0D_4B,$0D_4C,  _,_,_,_,_,_
              word $0D_2D,$0D_2E,$0D_2F,$0D_30,$0D_24,$0D_25,$0D_4D,$0D_4E,$0D_4F,$0D_50,  _,_,_,_,_,_
              word $0D_31,$0D_32,$0D_33,$0D_34,$0D_22,$0D_23,$0D_51,$0D_52,$0D_53,$0D_54,  _,_,_,_,_,_

              word $0D_35,$0D_36,$0D_37,$0D_38,$0D_39,$0D_3A,$0D_3B,$0D_3C,$0D_3D,$0D_3E,_,_,_,_,_,_
              word $0D_3F,$0D_40,$0D_41,$0D_42,$0D_43,$0D_44,$0D_45,$0D_46,$0D_47,$0D_48,_,_,_,_,_,_

        '}

{
gameField     word W,W,W,W,W,W,W,W,W,W,W,W,W,_,_,_ 'dach
              word W,S,S,S,S,S,S,S,S,S,S,S,W,_,_,_
              word W,R,R,S,S,S,_,S,S,S,S,S,W,_,_,_
              word W,R,S,S,S,S,S,S,S,S,S,S,W,_,_,_
              word W,R,R,W,W,R,R,W,W,W,R,R,W,_,_,_
              word W,R,S,W,S,R,S,W,S,W,R,S,W,_,_,_ 'level
              word W,R,R,W,S,R,S,W,W,W,R,S,W,_,_,_
              word W,S,S,S,S,S,S,S,S,S,S,S,W,_,_,_
              word W,M,N,F,L,S,S,S,S,S,S,S,W,_,_,_
              word W,W,W,W,W,W,W,W,W,W,W,W,W,_,_,_ 'boden
              word $1C_00,$1C_01,$1C_02,$1C_03,$1C_04,$1C_05,$01_06,$01_07,$01_08,$01_09,$01_0A,$01_0B,$01_0C,_,_,_
              word _,W,R,L,S,M,N,F,_,_,_,_,L,_,_,_
'}

{
gameField     word W,W,S,S,S,R,R,W,_,W,_,_,_,_,_,_'dach
              word _,W,S,_,S,R,_,W,W,_,_,_,_,_,_,_
              word _,W,S,S,S,R,_,W,W,_,_,_,_,_,_,_
              word W,W,S,_,S,R,R,W,_,W,_,_,_,_,_,_
              word _,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_
              word S,S,W,W,_,W,W,W,S,S,_,_,_,_,_,_
              word S,_,W,R,R,W,_,W,S,_,_,_,_,_,_,_
              word S,S,W,R,R,W,_,W,S,S,_,_,_,_,_,_
              word _,S,W,R,R,W,_,W,S,_,_,_,_,_,_,_
              word S,S,W,R,R,W,_,W,S,S,_,_,_,_,_,_
              word _,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_
              word _,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_


 }


' triplets to word table
triplet  word _ '$05_00  'nothing                                   000
         word W   '$00_01  'wall                                      001
         word R'$01_02  'rock                                      010
         word L'$05_03  'ladder                                    011
         word S'$02_04  'sandAENDERN ?                             100
         word M'$04_05  'monster                                   101
         word F'N'$04_08  'flugmonster                               110
         word J'$05_00  'player starting position (don't use!)     111


fallNums word 16384,6786,5207,4390,3867,3496,3215,2992,2811,2658,2528,2416,2317,2229,2151,2081,2016,1958,1904,1855,1809,1766,1727,1689,1655,1622,1591,1562,1534,1508,1483,1459,1437,1415,1394,1374,1355,1337,1320,1303,1287,1271,1256,1242,1228,1214,1201,1188,1176,1164,1152,1141,1130,1119,1109,1099,1089,1080,1071,1062,1053,1044,1036,1028,1020,1012,1004,997,989,982,975,968,962,955,949,942,936,930,924,918,913,907,901,896,891,885,880,875,870,865,861,856,851,847,842,838,833,829,825,821,817,813,809,805,801,797,793,790,786,782,779,775

'Dank an Centerius     '____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ 
'slideNums word 1000,2000,3000,4000,5000,6000,7000,8000,9000,10000,11000,12000,13000,14000,15000,16000
'             _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
jumpNums byte 0,0,1,1,1,2,2,2,2,2,2,1,1,1,0,0

              
' Palette fuer Röhren Fernseher      
{palette_map
              long $3E_6C_04_02  '0 Spieler/Leiter/frei  (Haut-Haare-Hose-Hintergrundschwarz) $3E_6C_04_02
              long $BD_BC_BB_BA '_AA '$BD_BC_BB_BA  '1 wand $AC_02_8E_AC' $AE_AD_AC_AB '  
              long $07_6C_6B_02  '2 stein           (weiss-hellrot-rot-schwarz)
              long $7E_8D_6D_7C  '3 sand                                                    $7C_8B_6B_7A'
              long $07_EC_EB_02  '4 Monster         (Augenweiss-Blau-Dunkelblau-Schwarz
              long $07_FC_6B_02  '5 Stein/Monster fallen (weiss-blau-rot-schwarz)
              long $7E_6D_6C_02  '6 BANG
              long $3E_3C_04_02  '7 Spieler/Leiter/frei  (Haut-Haare-Hose-Hintergrundschwarz) $3E_6C_04_02
' }    
' Palette fuer LCD Fernseher      
DAT 'Paletten und Sprite Tables

' Die Farben sehen auf einem Röhrenbildschirm ganz anders aus als
' auf einem TFT. Deswegen sind sie hier in doppelter Ausführung angegeben.
' Die erste ist für TFTs, die zweite für Röhren


'TFT-Farben:
         
              'Diese Farben ändern sich (es gibt 3 verschiedene Themes im Spiel)
palette_map   long $07_5B_04_02  '0 =VAR= frei         '$3E_6C_04_02  '0 aktuellerSpieler/Leiter/frei  (Haut-Haare-Hose-Hintergrundschwarz) $3E_6C_04_02  VARIABEL
              long $AD_AC_9B_9A  '1       wand         $AD_AC_AB_AA '_AA '$BD_BC_BB_BA  '1 wand $AC_02_8E_AC' $AE_AD_AC_AB '  
              long $07_5C_5B_02  '2       stein           (weiss-hellrot-rot-schwarz)                'long $07_6C_6B_02 '2 stein           (weiss-hellrot-rot-schwarz)
              long $7E_8D_6D_7C  '3       sand                                                    $7C_8B_6B_7A'
              long $07_fC_fB_02  '4       Monster         (Augenweiss-Blau-Dunkelblau-Schwarz
              long $07_FC_5B_02  '5       Stein/Monster fallen (weiss-blau-rot-schwarz)
              long $7E_6D_6C_02  '6       BANG

              'Diese Farben sind konstant (Spielfiguren, Schriften, etc.)'
              long $AD_AC_9B_9A  '7  grüne Wand
              long $7d_6c_8d_03  '8  braune Wand
              long $06_05_04_03  '9  graue Wand


              long $3E_6C_04_02  'A =KON= Spieler Orange  
              long $3E_CD_04_02  'B =KON= Spieler Turkis
              long $06_03_03_02'$3D_6B_03_02'$06_03_03_02  'C Spieler Orange  KONSTANT dunkler  grau
              long $06_04_03_02'$3D_CC_03_02'$06_04_03_02  'D Spieler Turkis  KONSTANT dunkler
              long $06_03_04_02'$3D_6B_04_02'$06_03_04_02  'E Spieler Orange  KONSTANT dunkler Leiter
              long $06_04_04_02'$3D_CC_04_02'$06_04_04_02  'F Spieler Turkis  KONSTANT dunkler Leiter
              
 '
              'Standardschriften
              long $07_05_04_02  '10 weiss auf schwarz
              long $07_07_02_02  '11 doppelfont 1
              long $02_07_07_02 '12 doppelfont 2


              ' Menuefarben                            
              '  Hauptmenue
              long $fe_fc_fa_f9  '13 Start (variabel)
              long $04_03_02_02  '14 Passwort (variabel)

              ' Pause-Menue
              long $07_05_04_02  '15 Pause 
              long $fe_fc_fa_f9  '16 Weiter (variabel)
              long $04_03_02_02 '17 Neustart (variabel)
              long $04_03_02_02 '18 Ende (variabel)
              long $07_05_04_02  '19 Passwort (variabel)

              ' Farben fuer Ziffern (Im Passwort)
              long $7C_7B_7A_02  '1a 0
              long $5C_5A_59_02  '1b 1
              long $1C_1B_1A_02  '1c 2
              long $aD_aB_aA_02  '1d 3
              long $FC_FB_FA_02  '1e 4
              long $6C_6B_6A_02  '1f 5
              long $3D_3C_3B_02  '20 6
              long $EE_EC_EA_02  '21 7
              long $8E_8C_8A_02  '22 8
              long $CD_CB_CA_02  '23 9
     
              ' Schriften
              long $04_03_02_02  '24 grau auf schwarz  (nicht ausgewaehlter Menueeintrag)
              long $fe_fd_fb_fa  '25 hellblau auf schwarz (ausgewaehlter Menueeintrag)
              long $07_05_04_02  '26 ?
              long $07_05_04_02  '27 ?
              long $07_05_04_02  '28 ?

              ' Sonderfarben
              long $03_03_03_03  '29 PAL_black
              long $02_02_02_02'$f9_f9_f9_f9  '2a PAL_effect

              long $06_05_04_03  '2b (variabel) Farben fuer die 
              long $07_06_05_04  '2c (variabel) kreisenden Geister
              long $07_07_06_05  '2d (variabel) am Ende des Spiels
              long $04_03_02_06  '2e (variabel) Farben sind 2F, 2F+6, 2F+2*6


              
              ' =================================
              
              'Ab hier sind die Themen-Farben. Diese werden nicht direkt
              'angesprochen, sondern auf die verwendete Palette kopiert

              'Normale Farben (Grüne Wände, rote Steine, blaue Monster, gelber Sand)
palette_normal  long $3E_6C_04_02 '$3E_6C_04_02  '0 aktuellerSpieler/Leiter/frei  (Haut-Haare-Hose-Hintergrundschwarz) $3E_6C_04_02  VARIABEL
              long $AD_AC_9B_9A '$AD_AC_AB_AA '_AA '$BD_BC_BB_BA  '1 wand $AC_02_8E_AC' $AE_AD_AC_AB '  
              'long $07_6C_6B_02  '2 stein           (weiss-hellrot-rot-schwarz)
              long $07_5C_5B_02  '2 stein           (weiss-hellrot-rot-schwarz)
              long $7E_8D_6D_7C  '3 sand                                                    $7C_8B_6B_7A'
              long $07_fC_fB_02  '4 Monster         (Augenweiss-Blau-Dunkelblau-Schwarz
              long $07_FC_5B_02  '5 Stein/Monster fallen (weiss-blau-rot-schwarz)
'              long $7E_6D_6C_02  '6 BANG
'                                     'long $3E_3C_04_02  '7 Spieler/Leiter/frei  (Haut-Haare-Hose-Hintergrundschwarz) $3E_6C_04_02 ????
'              long $3E_6C_04_02  '7 Spieler Orange  KONSTANT
'              long $3E_CD_04_02  '8 Spieler Turkis  KONSTANT
'              long $3E_6C_04_02  '7 Spieler Orange  KONSTANT dunkler
'              long $3E_CD_04_02  '8 Spieler Turkis  KONSTANT dunkler

              'Mediteranes Ambiente
palette_desert   long $3E_6C_04_02 '$3E_6C_04_02  '0 aktuellerSpieler/Leiter/frei  (Haut-Haare-Hose-Hintergrundschwarz) $3E_6C_04_02  VARIABEL
              long $7d_6c_8d_03'$07_06_03_02 '$AD_AC_AB_AA '_AA '$BD_BC_BB_BA  '1 wand $AC_02_8E_AC' $AE_AD_AC_AB '  
               'long $07_6C_6B_02  '2 stein           (weiss-hellrot-rot-schwarz)
              long $8d_8c_8b_02  '2 stein           (weiss-hellrot-rot-schwarz)
              long $5a_4b_6b_7b  '3 sand                                                    $7C_8B_6B_7A'
              long $2c_2c_2b_02  '4 Monster         (Augenweiss-Blau-Dunkelblau-Schwarz
              long $05_2c_8b_02  '5 Stein/Monster fallen (weiss-blau-rot-schwarz)
'              long $7E_6D_6C_02  '6 BANG
'                                     'long $3E_3C_04_02  '7 Spieler/Leiter/frei  (Haut-Haare-Hose-Hintergrundschwarz) $3E_6C_04_02 ????
'              long $3e_6c_04_02'$3E_6C_04_02  '7 Spieler Orange  KONSTANT
'              long $3E_CD_04_02  '8 Spieler Turkis  KONSTANT
 '             long $3E_6C_04_02  '7 Spieler Orange  KONSTANT dunkler
'              long $3E_CD_04_02  '8 Spieler Turkis  KONSTANT dunkler
 
                              '  }

              'Industiewelt
palette_metall long $3E_6C_04_02 '$3E_6C_04_02  '0 aktuellerSpieler/Leiter/frei  (Haut-Haare-Hose-Hintergrundschwarz) $3E_6C_04_02  VARIABEL
              long $06_05_04_03 '$AD_AC_AB_AA '_AA '$BD_BC_BB_BA  '1 wand $AC_02_8E_AC' $AE_AD_AC_AB '  
              'long $07_6C_6B_02  '2 stein           (weiss-hellrot-rot-schwarz)
              long $07_dC_dB_02  '2 stein           (weiss-hellrot-rot-schwarz)
              long $6b_5c_7c_6d'$6b_5c_7c_6d  '3 sand                                                    $7C_8B_6B_7A'
              long $02_9c_9b_02             ' long $02_4d_4e_02  '4 Monster         (Augenweiss-Blau-Dunkelblau-Schwarz
              long $02_9c_db_02  '5 Stein/Monster fallen (weiss-blau-rot-schwarz)
'              long $7E_6D_6C_02  '6 BANG
'                                     'long $3E_3C_04_02  '7 Spieler/Leiter/frei  (Haut-Haare-Hose-Hintergrundschwarz) $3E_6C_04_02 ????
'              long $3E_6C_04_02  '7 Spieler Orange  KONSTANT
'              long $3E_CD_04_02  '8 Spieler Turkis  KONSTANT
 '             long $3E_6C_04_02  '7 Spieler Orange  KONSTANT dunkler
'              long $3E_CD_04_02  '8 Spieler Turkis  KONSTANT dunkler
'} 

' Normale palette (Röhrenbildschirm)
{
palette_map   long $02_5B_07_5B '$3E_6C_04_02  '0 aktuellerSpieler/Leiter/frei  (Haut-Haare-Hose-Hintergrundschwarz) $3E_6C_04_02  VARIABEL
              long $AD_AC_9B_9A '$AD_AC_AB_AA '_AA '$BD_BC_BB_BA  '1 wand $AC_02_8E_AC' $AE_AD_AC_AB '  
              'long $07_6C_6B_02  '2 stein           (weiss-hellrot-rot-schwarz)
              long $07_5C_5B_02  '2 stein           (weiss-hellrot-rot-schwarz)
              long $7E_8D_6D_7C  '3 sand                                                    $7C_8B_6B_7A'
              long $07_fC_fB_02  '4 Monster         (Augenweiss-Blau-Dunkelblau-Schwarz
              long $07_FC_5B_02  '5 Stein/Monster fallen (weiss-blau-rot-schwarz)
              long $7E_6D_6C_02  '6 BANG
'                                     'long $3E_3C_04_02  '7 Spieler/Leiter/frei  (Haut-Haare-Hose-Hintergrundschwarz) $3E_6C_04_02 ????
              long $3E_6C_04_02  '7 Spieler Orange  KONSTANT
              long $3E_CD_04_02  '8 Spieler Turkis  KONSTANT
 '             long $3E_6C_04_02  '7 Spieler Orange  KONSTANT dunkler
'              long $3E_CD_04_02  '8 Spieler Turkis  KONSTANT dunkler
                            
'}


            
              ' sprite 0 header  ' Player Stone
sprite_tbl    long $00_00_00_00  ' state/control word: y,x,z,state, enabled, x=$50, y=$60
              long $00_00_00_00  ' bitmap ptr

              ' sprite 1 header  ' kickstein
              long $00_00_00_00  ' state/control word: y,x,z,state
              long $00_00_00_00  ' bitmap ptr

              ' sprite 2 header  ' Zeug, das fällt (Stein/Monster)
              long $00_00_00_00  ' state/control word: y,x,z,state
              long $00_00_00_00  ' bitmap ptr

              ' sprite 3 header
              long $00_00_00_00  ' state/control word: y,x,z,state
              long $00_00_00_00  ' bitmap ptr

              ' sprite 4 header
              long $00_00_00_00  ' state/control word: y,x,z,state
              long $00_00_00_00  ' bitmap ptr

              ' sprite 5 header
              long $00_00_00_00  ' state/control word: y,x,z,state
              long $00_00_00_00  ' bitmap ptr

              ' sprite 6 header
              long $00_00_00_00  ' state/control word: y,x,z,state
              long $00_00_00_00  ' bitmap ptr

              ' sprite 7 header
              long $00_00_00_00  ' state/control word: y,x,z,state
              long $00_00_00_00  ' bitmap ptr



DAT 'Tiles   =============================================
Tiles
                         

'Fonts                                                                                         0
  JackStoneFon_tx0_ty0_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%1_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_2_2_1_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_3_3_2_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%2_2_1_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%1_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(3, 0), size=(36, 36), palette index=(3)    1
JackStoneFon_tx3_ty0_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%1_1_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_3_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%2_2_0_0_0_0_0_0_1_2_1_1_0_0_0_0
              LONG %%1_1_0_0_0_0_0_0_2_3_3_3_2_2_2_1
              LONG %%0_0_0_0_0_0_0_0_1_2_3_3_3_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_1_2_3_3_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_1_3_3_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_1_3_3_3_3_3

' Extracted from file "JackStoneFonts.bmp" at tile=(1, 0), size=(36, 36), palette index=(1)     2
JackStoneFon_tx1_ty0_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_2_1_1_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_3_3_3_2_2_2
              LONG %%0_0_0_0_0_0_0_0_0_1_2_3_3_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_1_3_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_1_3_3_3_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_1_3_3_2_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_3_3_2_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_3_3_1_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_3_3_3_1_0

' Extracted from file "JackStoneFonts.bmp" at tile=(2, 0), size=(36, 36), palette index=(2)      3
JackStoneFon_tx2_ty0_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%2_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_1_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_2_1_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_3_2_1_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_3_3_2_0_0_0_0_0_2_2_2_1_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(4, 0), size=(36, 36), palette index=(4)     4
JackStoneFon_tx4_ty0_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_1_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_2_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_2_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_2_3
              LONG %%1_1_1_0_0_0_0_0_0_0_0_0_0_0_3_3
              LONG %%3_3_3_2_1_0_0_0_0_0_0_0_0_0_3_3
              LONG %%3_3_3_3_2_1_0_0_0_0_0_0_0_0_3_3
              LONG %%3_1_0_2_3_2_0_0_0_0_1_2_1_1_3_3
              LONG %%3_0_0_0_3_3_2_0_0_0_2_3_3_3_3_3
              LONG %%2_0_0_0_2_3_3_0_0_0_1_2_2_3_3_3
              LONG %%2_0_0_0_1_3_3_1_0_0_0_0_1_1_3_3

' Extracted from file "JackStoneFonts.bmp" at tile=(5, 0), size=(36, 36), palette index=(5)   5
JackStoneFon_tx5_ty0_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_2_2_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_2_3_3_2_0_2_2_0_0_0_0_0_0_0_0
              LONG %%0_2_3_3_3_1_2_3_2_0_0_0_0_0_0_0
              LONG %%0_2_3_3_3_3_2_3_3_3_1_0_0_0_0_0
              LONG %%0_0_3_3_1_2_3_3_3_3_3_1_0_0_0_2
              LONG %%0_0_1_1_0_1_3_3_3_2_1_1_0_0_2_3
              LONG %%0_0_0_0_0_0_3_3_3_2_0_0_0_0_3_3
              LONG %%0_0_0_0_0_0_2_3_3_2_0_0_0_1_3_3
              LONG %%0_0_0_0_0_0_2_3_3_2_0_0_0_2_3_3
              LONG %%0_0_0_0_0_0_1_3_3_2_0_0_0_2_3_3
                                                                                              '6
JackStoneFon_tx3_ty3_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_2_3_3_3_3_2
              LONG %%0_0_0_0_0_0_0_0_0_0_2_3_3_3_3_1
              LONG %%0_0_0_0_0_0_0_0_0_1_3_3_3_3_3_1
              LONG %%0_0_0_0_0_0_0_0_0_1_3_3_3_3_2_0
              LONG %%0_0_0_0_0_0_0_0_0_1_3_3_3_3_2_0
              LONG %%0_0_0_0_0_0_0_0_0_2_3_3_3_3_1_0
              LONG %%0_0_0_0_0_0_0_0_1_3_3_3_3_3_1_0
              LONG %%0_0_0_0_0_0_0_0_1_3_3_3_3_2_0_0
              LONG %%0_0_0_0_0_0_0_0_1_3_3_3_3_2_0_0
              LONG %%0_0_0_0_0_0_0_0_2_3_3_3_3_1_0_0
              LONG %%0_0_0_0_0_0_0_0_2_3_3_3_3_1_0_0
              LONG %%0_0_0_0_0_0_0_1_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_1_3_3_3_3_2_0_0_0
              LONG %%0_0_0_0_0_0_0_2_3_3_3_3_1_0_0_0
              LONG %%0_0_0_0_0_0_0_2_3_3_3_3_1_0_0_0
              LONG %%0_0_0_0_0_0_1_3_3_3_3_3_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(1, 3), size=(36, 36), palette index=(31)     7
JackStoneFon_tx1_ty3_bitmap    LONG
              LONG %%1_0_0_0_0_0_0_0_0_0_0_3_3_2_0_0
              LONG %%3_3_1_0_0_0_0_0_0_0_1_3_3_2_0_0
              LONG %%3_3_3_2_1_0_0_0_0_0_1_3_3_2_0_0
              LONG %%3_3_3_3_2_1_0_0_0_0_2_3_3_1_0_0
              LONG %%3_3_3_3_3_2_0_0_0_0_2_3_3_1_0_0
              LONG %%3_3_3_2_1_1_0_0_0_0_3_3_3_1_0_0
              LONG %%3_3_3_1_0_0_0_0_0_0_3_3_2_0_0_0
              LONG %%3_3_3_0_0_0_0_0_0_1_3_3_2_0_0_0
              LONG %%3_3_2_0_0_0_0_0_0_1_3_3_2_0_0_0
              LONG %%3_3_2_0_0_0_0_0_0_2_3_3_1_0_0_0
              LONG %%3_3_2_0_0_0_0_0_0_2_3_3_1_0_0_0
              LONG %%3_3_1_0_0_0_0_0_0_3_3_2_0_0_0_0
              LONG %%3_3_1_0_0_0_0_0_0_3_3_2_0_0_0_0
              LONG %%3_3_1_0_0_0_0_0_1_3_3_2_0_0_0_0
              LONG %%3_3_0_0_0_0_0_0_1_3_3_2_0_0_0_0
              LONG %%3_2_0_0_0_0_0_0_1_3_3_1_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(2, 3), size=(36, 36), palette index=(32)     8
JackStoneFon_tx2_ty3_bitmap    LONG
              LONG %%3_2_2_1_0_0_0_0_3_3_3_3_3_1_0_0
              LONG %%3_1_0_0_0_0_0_2_3_3_3_3_3_3_1_1
              LONG %%3_0_0_0_0_0_0_3_3_3_3_2_2_3_2_1
              LONG %%3_0_0_0_0_0_0_3_3_3_2_0_1_2_3_2
              LONG %%3_0_0_0_0_0_1_3_3_3_1_0_0_0_2_3
              LONG %%3_0_0_0_0_0_1_3_3_3_1_0_0_0_0_3
              LONG %%3_0_0_0_0_0_2_3_3_3_0_0_0_0_0_2
              LONG %%3_0_0_0_0_0_2_3_3_3_0_0_0_0_0_3
              LONG %%3_0_0_0_0_0_2_3_3_3_0_0_0_0_1_3
              LONG %%3_0_0_0_0_0_3_3_3_3_0_0_0_0_1_3
              LONG %%2_0_0_0_0_0_3_3_3_2_0_0_0_0_1_3
              LONG %%2_0_0_0_0_0_3_3_3_2_0_0_0_0_1_3
              LONG %%2_0_0_0_0_0_3_3_3_2_0_0_0_0_2_3
              LONG %%2_0_0_0_0_0_3_3_3_1_0_0_0_0_2_3
              LONG %%1_0_0_0_0_0_3_3_3_1_0_0_0_0_2_3
              LONG %%1_0_0_0_0_1_3_3_3_1_0_0_0_0_3_3

' Extracted from file "JackStoneFonts.bmp" at tile=(4, 3), size=(36, 36), palette index=(34)    9
JackStoneFon_tx4_ty3_bitmap    LONG
              LONG %%2_0_0_0_1_3_3_2_0_0_0_0_0_0_3_3
              LONG %%3_1_1_1_0_3_3_3_0_0_0_0_0_0_3_3
              LONG %%3_3_3_2_2_3_3_3_1_0_0_0_0_0_3_3
              LONG %%1_2_2_3_3_3_3_3_1_0_0_0_0_1_3_3
              LONG %%0_0_1_1_1_3_3_3_1_0_0_0_0_1_3_3
              LONG %%0_0_0_0_0_3_3_3_2_0_0_0_0_1_3_3
              LONG %%0_0_0_0_0_3_3_3_2_0_0_0_0_2_3_3
              LONG %%0_0_0_0_0_3_3_3_2_0_0_0_0_2_3_3
              LONG %%0_0_0_0_1_3_3_3_1_0_0_0_0_2_3_3
              LONG %%0_0_0_0_2_3_3_3_1_0_0_0_0_2_3_3
              LONG %%0_0_0_1_3_3_3_3_0_0_0_0_0_3_3_3
              LONG %%2_0_1_2_3_3_3_2_0_0_0_0_0_3_3_3
              LONG %%3_3_3_3_3_3_3_1_0_0_0_0_0_3_3_3
              LONG %%2_3_3_3_3_3_2_0_0_0_2_0_0_3_3_3
              LONG %%0_1_2_2_2_1_0_0_0_0_2_3_1_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_1_3_3_3_3_3

' Extracted from file "JackStoneFonts.bmp" at tile=(5, 3), size=(36, 36), palette index=(35)   A
JackStoneFon_tx5_ty3_bitmap    LONG
              LONG %%0_0_0_0_0_0_1_3_3_2_0_0_0_2_3_3
              LONG %%0_0_0_0_0_0_1_3_3_2_0_0_0_2_3_3
              LONG %%0_0_0_0_0_0_1_3_3_2_0_0_0_1_2_3
              LONG %%0_0_0_0_0_0_1_3_3_2_0_0_0_0_0_1
              LONG %%0_0_0_0_0_0_1_3_3_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_3_3_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_3_3_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_3_3_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_3_3_2_0_0_0_0_1_0
              LONG %%0_0_0_0_0_2_3_3_3_3_0_0_0_1_3_0
              LONG %%0_0_0_0_0_1_3_3_3_3_2_0_0_0_3_2
              LONG %%0_0_0_0_0_0_0_0_1_2_3_1_0_0_1_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
                                                                                             'B
JackStoneFon_tx3_ty1_bitmap    LONG
              LONG %%0_0_0_0_0_0_1_3_3_3_3_2_0_0_0_0
              LONG %%0_0_0_0_0_0_1_3_3_3_3_1_0_0_0_0
              LONG %%0_0_0_0_0_0_2_3_3_3_3_1_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_3_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_3_3_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_1_3_3_3_3_2_0_0_0_0_0_0
              LONG %%0_0_0_1_3_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_1_3_3_3_3_3_1_0_0_0_0_0_0_0
              LONG %%2_2_3_3_3_3_3_1_0_0_0_0_0_0_0_0
              LONG %%3_3_3_3_3_2_1_0_0_0_0_0_0_0_0_0
              LONG %%2_2_2_2_1_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(1, 1), size=(36, 36), palette index=(11)     C
JackStoneFon_tx1_ty1_bitmap    LONG
              LONG %%3_2_0_0_0_0_0_0_1_3_3_1_0_0_0_0
              LONG %%3_2_0_0_0_0_0_0_1_3_3_0_0_0_0_0
              LONG %%3_2_0_0_0_0_0_0_1_3_3_1_0_0_0_0
              LONG %%3_1_0_0_0_0_0_0_1_3_3_1_0_0_0_0
              LONG %%3_1_0_0_0_0_0_0_0_3_3_1_0_0_0_0
              LONG %%3_3_1_0_0_0_0_0_0_3_3_2_0_0_0_0
              LONG %%2_3_2_0_0_0_0_0_0_2_3_3_0_0_0_0
              LONG %%0_0_1_0_0_0_0_0_0_0_3_3_2_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_1_3_3_2_1_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_3_3_2_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_2_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_1_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(2, 1), size=(36, 36), palette index=(12)     D
JackStoneFon_tx2_ty1_bitmap    LONG
              LONG %%0_0_0_0_0_1_3_3_3_0_0_0_0_1_3_3
              LONG %%0_0_0_1_1_3_3_3_3_0_0_0_0_1_3_3
              LONG %%0_0_0_2_3_3_3_3_3_1_0_0_0_1_3_3
              LONG %%0_0_0_0_1_2_3_3_3_3_1_0_1_2_3_3
              LONG %%0_0_0_0_0_0_0_0_1_2_2_1_3_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_2_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(4, 1), size=(36, 36), palette index=(14)    E
JackStoneFon_tx4_ty1_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_1_3_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_1_2_2_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_1_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_2_1_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_2_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_3_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_3_2_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_1_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(5, 1), size=(36, 36), palette index=(15)      F
JackStoneFon_tx5_ty1_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_1_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_3_1_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_3_3_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_3_3_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_3_3_0_0
                                                              
JackStoneFon_tx1_ty2_bitmap    LONG                                                            '10
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%1_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_3_2_1_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_3_3_3_3_2_0_0_0_0_0_0_0_0_0_0
              LONG %%3_3_3_3_2_2_0_0_1_1_0_0_0_0_0_0
              LONG %%3_3_3_1_0_0_0_0_1_3_3_2_1_0_0_0
              LONG %%3_3_2_0_0_0_0_0_0_1_3_3_3_3_2_1
              LONG %%3_3_0_0_0_0_0_0_0_0_3_3_3_3_3_3
              LONG %%3_3_0_0_0_0_0_0_0_2_3_3_3_3_1_1
              LONG %%3_3_0_0_0_0_0_0_0_3_3_3_3_2_0_0
              LONG %%3_2_0_0_0_0_0_0_2_3_3_3_3_0_0_0
              LONG %%3_3_0_0_0_0_0_1_3_3_3_3_1_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(2, 2), size=(36, 36), palette index=(22)    11
JackStoneFon_tx2_ty2_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_1_1_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_1_3_3_2_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_3_3_3_3_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_2_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_1_0_0_0_0_0_0_3_2
              LONG %%0_0_0_0_0_1_3_1_0_0_0_0_0_0_1_3
              LONG %%0_0_0_0_0_0_3_1_0_0_0_0_0_0_0_2
              LONG %%1_0_0_0_0_0_3_2_0_0_0_0_0_0_0_2
              LONG %%3_0_0_0_0_0_3_2_0_0_0_0_0_0_1_3
              LONG %%3_2_0_0_0_0_3_3_0_0_0_0_0_0_2_3
              LONG %%3_3_0_0_0_0_2_3_0_0_0_0_0_0_3_3
              LONG %%3_3_1_0_0_0_2_3_0_0_0_0_0_2_3_3
              LONG %%3_3_2_0_0_0_2_3_1_0_0_0_0_3_3_3
              LONG %%3_3_2_0_0_0_1_3_1_0_0_0_2_3_3_3
              LONG %%3_3_2_0_0_0_0_3_2_0_0_0_3_3_3_3

' Extracted from file "JackStoneFonts.bmp" at tile=(4, 2), size=(36, 36), palette index=(24)     12
JackStoneFon_tx4_ty2_bitmap    LONG
              LONG %%0_0_0_3_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_2_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_2_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_1_3_3_2_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_1_3_3_2_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_1_3_3_2_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_1_3_3_2_0_0_0_0_1_1_2_1_0_0
              LONG %%0_0_1_3_3_2_0_0_0_1_3_3_3_3_3_1
              LONG %%0_0_1_3_3_1_0_0_0_2_3_3_2_0_2_3
              LONG %%0_0_1_3_3_1_0_0_0_3_3_3_0_0_0_3
              LONG %%0_0_1_3_3_1_0_0_1_3_3_3_0_0_0_2
              LONG %%0_0_1_3_3_1_0_0_1_3_3_3_1_0_0_1
              LONG %%0_0_2_3_3_1_0_0_0_1_2_3_3_2_1_1
              LONG %%0_0_2_3_3_0_0_0_0_0_0_1_1_2_3_3
              LONG %%0_0_2_3_3_0_0_0_0_0_0_0_0_0_1_2
              LONG %%0_2_3_3_3_0_0_0_0_0_0_0_0_0_0_1

' Extracted from file "JackStoneFonts.bmp" at tile=(5, 2), size=(36, 36), palette index=(25)      13
JackStoneFon_tx5_ty2_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_2_2_1_3_3_1_0
              LONG %%0_0_0_0_0_0_0_0_0_1_2_3_3_3_3_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_1_3_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_3_3_3_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_3_3_2_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_3_3_2_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_3_3_2_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_3_3_2_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_3_3_2_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_3_3_2_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_3_3_2_0
              LONG %%0_0_0_0_0_0_0_0_0_1_1_0_3_3_2_0
              LONG %%0_0_0_0_0_0_0_0_0_0_3_1_3_3_2_0
              LONG %%0_0_0_0_0_0_0_0_0_0_1_3_3_3_1_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_1_3_3_1_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
                                                                                                   '14
JackStoneFon_tx1_ty4_bitmap    LONG
              LONG %%2_3_0_0_0_0_0_2_3_3_3_3_0_0_0_0
              LONG %%3_3_0_0_0_0_2_3_3_3_3_1_0_0_0_0
              LONG %%2_3_0_0_0_1_3_3_3_3_2_0_0_0_0_0
              LONG %%2_3_1_0_0_2_3_3_3_3_0_0_0_0_0_0
              LONG %%2_3_1_0_1_3_3_3_3_1_0_0_0_0_0_0
              LONG %%1_3_1_0_3_3_3_3_2_0_0_0_0_0_0_0
              LONG %%1_3_2_1_3_3_3_3_0_0_0_0_0_0_0_0
              LONG %%1_3_2_3_3_3_3_2_0_0_0_0_0_0_0_0
              LONG %%0_3_3_3_3_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_3_3_3_3_3_1_0_0_0_0_0_0_0_0_0
              LONG %%0_3_3_3_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_3_3_3_3_1_0_0_0_0_0_0_0_0_0_0
              LONG %%0_2_3_3_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_2_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
                                                                                                  '15

JackStoneF789on_tx1_ty4_bitmap    LONG
              LONG %%3_3_2_0_0_0_0_3_2_0_0_2_3_3_3_2
              LONG %%3_3_2_0_0_0_0_3_2_0_0_3_3_3_3_1
              LONG %%3_3_1_0_0_0_0_3_3_0_2_3_3_3_2_0
              LONG %%3_2_0_0_0_0_0_2_3_1_3_3_3_3_0_0
              LONG %%3_0_0_0_0_0_0_2_3_2_3_3_3_2_0_0
              LONG %%1_0_0_0_0_0_0_1_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_1_3_3_3_3_2_0_0_0
              LONG %%0_0_0_0_0_0_0_1_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_3_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_2_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

                                                                                                   '16
JackStoneFon_tx4_ty4_bitmap    LONG
              LONG %%0_2_3_3_3_0_0_0_1_1_0_0_0_0_0_2
              LONG %%0_0_0_1_3_3_1_0_1_3_0_0_0_0_0_3
              LONG %%0_0_0_0_0_0_1_0_0_2_3_0_0_0_2_3
              LONG %%0_0_0_0_0_0_0_0_0_0_3_3_2_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_3_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_1_2_2_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0



' ====================================================================
' "Pause"              
' ====================================================================

                                                           
JackStoneFon_tx0_ty5_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0                                         '17
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_2_3_3_3_3_3_2_1
              LONG %%0_0_0_0_0_0_1_3_3_2_1_3_3_3_1_0
              LONG %%0_0_0_0_0_0_3_3_2_0_0_2_3_3_0_0
              LONG %%0_0_0_0_0_1_3_3_1_0_0_2_3_3_0_0
              LONG %%0_0_0_0_0_2_3_3_0_0_0_2_3_3_0_0
              LONG %%0_0_0_0_0_2_3_3_0_0_0_2_3_3_0_0
                                                                  
JackStoneFon_tx0_ty6_bitmap    LONG
              LONG %%1_0_0_0_0_2_3_3_0_0_0_2_3_3_0_0
              LONG %%3_3_2_0_0_1_3_3_0_0_0_2_3_3_0_0
              LONG %%1_0_3_2_0_0_3_3_1_0_0_2_3_3_0_0
              LONG %%0_0_2_3_0_0_2_3_2_0_0_2_3_3_0_0
              LONG %%0_0_3_3_0_0_0_3_3_2_1_2_3_3_0_0
              LONG %%0_0_1_2_0_0_0_0_2_3_3_3_3_3_0_0
              LONG %%2_1_0_0_0_0_0_0_0_0_0_2_3_3_0_0
              LONG %%3_3_2_0_0_0_0_0_0_0_0_2_3_3_0_0                                        '18
              LONG %%0_2_3_2_0_0_0_0_0_0_0_2_3_3_0_0
              LONG %%0_0_3_3_0_0_0_0_0_0_0_2_3_3_0_0
              LONG %%0_0_2_3_2_0_0_0_0_0_0_2_3_3_0_0
              LONG %%0_0_2_3_2_0_0_0_0_0_0_2_3_3_0_0
              LONG %%0_0_2_3_2_0_0_0_0_0_0_2_3_3_0_0
              LONG %%2_0_3_3_1_0_0_0_0_0_0_3_3_3_0_0
              LONG %%2_3_3_2_0_0_0_0_0_0_3_3_3_3_3_1
              LONG %%0_1_1_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(3, 6), size=(36, 36), palette index=(63)
JackStoneFon_tx3_ty6_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_1_3_3_3_0_1_3_3_3_0_0_0_0_3
              LONG %%0_0_1_3_3_1_0_1_3_3_1_0_0_0_2_3
              LONG %%0_0_1_3_3_0_0_1_3_3_0_0_0_0_3_3
              LONG %%1_0_1_3_3_0_0_1_3_3_0_0_0_1_3_3
              LONG %%0_0_1_3_3_0_0_1_3_3_0_0_0_1_3_3
              LONG %%0_0_1_3_3_0_0_1_3_3_0_0_0_2_3_3
              LONG %%0_0_1_3_3_0_0_1_3_3_0_0_0_2_3_3                                         '19
              LONG %%0_0_1_3_3_0_0_1_3_3_0_0_0_2_3_3
              LONG %%0_0_1_3_3_0_0_1_3_3_0_0_0_2_3_3
              LONG %%1_0_1_3_3_0_0_1_3_3_0_0_0_2_3_3
              LONG %%1_0_1_3_3_0_0_1_3_3_0_0_1_2_3_3
              LONG %%1_0_2_3_3_0_0_2_3_3_0_0_3_2_3_3
              LONG %%0_2_3_3_3_3_1_3_3_2_0_0_3_3_3_3
              LONG %%0_0_2_3_3_2_3_3_3_0_0_0_1_3_3_1
              LONG %%0_0_0_0_1_0_1_1_0_0_0_0_0_1_1_0

' Extracted from file "JackStoneFonts.bmp" at tile=(1, 6), size=(36, 36), palette index=(61)
JackStoneFon_tx1_ty6_bitmap    LONG
              LONG %%0_0_0_0_1_0_0_0_0_0_0_0_0_1_0_0
              LONG %%0_0_2_3_3_3_0_0_0_0_1_3_3_3_3_0
              LONG %%0_1_3_3_0_3_3_0_0_0_2_3_1_0_3_2
              LONG %%0_3_3_1_0_1_3_2_0_0_2_2_0_0_3_3
              LONG %%0_3_3_0_0_0_3_3_0_0_2_1_0_0_3_3
              LONG %%0_3_3_0_0_0_3_3_1_0_0_0_0_0_3_3
              LONG %%0_3_3_3_3_3_3_3_2_0_0_0_1_3_3_3
              LONG %%0_2_2_2_2_2_3_3_2_0_0_1_3_3_3_1                                         '1a
              LONG %%0_0_0_0_0_0_3_3_2_0_1_3_3_3_2_0
              LONG %%0_0_0_0_0_0_3_3_2_0_2_3_3_1_0_0
              LONG %%0_0_0_0_0_0_3_3_2_0_3_3_1_0_0_1
              LONG %%0_1_0_0_0_1_3_3_1_0_3_3_0_0_0_3
              LONG %%0_3_0_0_0_2_3_3_0_0_3_3_0_0_0_3
              LONG %%0_2_3_2_2_3_3_1_0_0_2_3_1_0_1_3
              LONG %%0_0_2_3_3_3_2_0_0_0_0_2_3_3_3_3
              LONG %%0_0_0_1_1_1_0_0_0_0_0_0_1_1_1_0

' ============================================================
' "Weiter"
' ============================================================
              

' Extracted from file "JackStoneFonts.bmp" at tile=(2, 6), size=(36, 36), palette index=(62)
JackStoneFon_tx2_ty6_bitmap    LONG
              LONG %%0_0_3_3_2_0_0_3_3_3_3_0_3_3_3_3
              LONG %%0_0_0_3_0_0_0_0_3_3_0_0_0_3_3_0
              LONG %%0_0_0_3_0_0_0_0_3_2_0_0_0_3_2_0
              LONG %%0_0_0_2_1_0_0_1_3_2_0_0_1_3_1_0
              LONG %%0_0_0_2_2_0_0_2_3_2_0_0_2_3_1_0
              LONG %%2_0_0_1_3_0_0_3_3_2_0_0_3_3_0_0
              LONG %%3_2_0_0_3_0_0_3_3_3_0_0_3_3_0_0
              LONG %%2_3_0_0_3_0_0_3_2_2_0_1_3_2_0_0                                         ' 1b
              LONG %%2_3_0_0_2_1_1_3_1_2_0_2_3_1_0_0
              LONG %%2_3_0_0_1_2_2_3_1_1_1_2_3_0_0_0
              LONG %%1_3_1_0_0_2_2_3_0_1_2_3_3_0_0_0
              LONG %%2_3_0_0_0_3_3_3_0_0_3_3_3_0_0_0
              LONG %%3_3_0_0_0_3_3_2_0_0_3_3_2_0_0_0
              LONG %%3_2_0_0_0_2_3_1_0_0_2_3_1_0_0_0
              LONG %%3_0_0_0_0_1_3_0_0_0_1_3_0_0_0_0
              LONG %%0_0_0_0_0_0_1_0_0_0_0_1_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(4, 6), size=(36, 36), palette index=(64)
JackStoneFon_tx4_ty6_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_0_0_0_3_2_0_0_0_0_0
              LONG %%0_0_0_0_1_3_0_0_0_3_2_0_0_0_0_0
              LONG %%0_0_0_0_1_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_3_1_0_0_0_0_0_0_0_0_1
              LONG %%2_0_0_3_3_3_3_1_0_3_2_1_0_1_3_3
              LONG %%3_1_0_0_1_3_1_0_0_3_3_1_0_3_2_0
              LONG %%3_3_0_0_1_3_1_0_0_3_2_0_1_3_1_0
              LONG %%2_3_0_0_1_3_1_0_0_3_2_0_1_3_2_1                                      '1c
              LONG %%2_3_0_0_1_3_1_0_0_3_2_0_0_2_2_2
              LONG %%1_3_0_0_1_3_1_0_0_3_2_0_0_0_0_0
              LONG %%2_3_0_0_1_3_1_0_0_3_2_0_0_0_0_0
              LONG %%3_3_0_0_1_3_1_0_0_3_2_0_0_0_0_0
              LONG %%3_2_0_2_1_3_1_0_0_3_2_0_0_3_0_1
              LONG %%3_0_0_0_3_3_1_0_3_3_3_1_0_1_3_3
              LONG %%0_0_0_0_0_1_0_0_0_0_0_0_0_0_0_1

' Extracted from file "JackStoneFonts.bmp" at tile=(5, 6), size=(36, 36), palette index=(65)
JackStoneFon_tx5_ty6_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_3_1_3_2_0_0_1_3_3
              LONG %%0_0_0_0_0_0_2_3_3_3_3_0_0_3_1_0
              LONG %%0_0_0_0_0_0_0_0_0_3_2_0_1_3_1_0                                       '1d
              LONG %%0_0_0_0_0_0_0_0_0_3_2_0_1_3_2_1
              LONG %%0_0_0_0_0_0_0_0_0_3_2_0_1_2_2_2
              LONG %%0_0_0_0_0_0_0_0_0_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_2_0_1_3_0_1
              LONG %%0_0_0_0_0_0_0_0_3_3_3_1_0_2_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_1

' ============================================================
' "Neustart"
' ============================================================

JackStoneFon_tx6_ty6_bitmap    LONG
              LONG %%0_0_0_0_0_0_2_3_3_0_0_0_0_2_3_1
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_1_3_2_0
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_3_3_1_0
              LONG %%0_0_0_0_0_0_0_3_0_0_0_1_3_3_1_0
              LONG %%0_0_1_0_0_0_0_3_0_0_0_3_3_3_1_0
              LONG %%1_3_3_3_0_0_0_3_0_0_1_3_3_3_1_0
              LONG %%3_2_0_3_2_0_0_3_0_0_3_3_1_3_1_0                                          '1e
              LONG %%3_2_0_2_3_0_0_3_0_1_3_3_0_3_1_0
              LONG %%3_2_1_2_3_0_0_3_0_3_3_1_0_3_1_0
              LONG %%2_2_2_2_3_1_0_3_2_3_3_0_0_3_1_0
              LONG %%0_0_0_1_3_1_0_3_3_3_1_0_0_3_1_0
              LONG %%0_0_0_1_3_1_0_3_3_3_0_0_0_3_1_0
              LONG %%0_0_0_2_3_0_0_3_3_1_0_0_0_3_1_0
              LONG %%3_1_1_3_3_0_0_3_3_0_0_0_0_3_1_0
              LONG %%1_3_3_3_1_0_0_3_1_0_0_0_2_3_3_1
              LONG %%0_0_1_0_0_0_0_1_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(7, 6), size=(36, 36), palette index=(67)
JackStoneFon_tx7_ty6_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%2_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%2_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%2_0_0_0_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_2_0_3_2_3_2_0_2_3_3_0_2_3_2_0
              LONG %%3_0_0_2_0_2_3_0_2_3_0_0_2_3_0_0
              LONG %%3_0_0_2_0_2_3_0_2_3_0_0_2_3_0_0
              LONG %%3_0_0_0_0_3_3_0_2_3_0_0_2_3_0_0                                          '1f
              LONG %%3_0_0_2_3_3_2_0_2_3_0_0_2_3_0_0
              LONG %%3_0_0_3_3_2_0_0_2_3_0_0_2_3_0_0
              LONG %%3_0_2_3_2_0_2_0_2_3_0_0_2_3_0_0
              LONG %%3_0_2_3_0_0_2_0_2_3_0_0_2_3_0_0
              LONG %%3_0_2_3_0_0_3_0_3_3_2_0_3_3_0_0
              LONG %%2_0_0_2_3_2_3_0_2_3_2_3_3_2_0_0
              LONG %%0_0_0_0_2_2_0_0_0_2_0_2_2_0_0_0

JackStoneFon_tx8_ty6_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_2
              LONG %%0_2_0_2_0_0_0_0_0_2_0_0_0_0_0_2
              LONG %%2_3_2_3_3_2_0_0_2_3_3_2_0_2_3_3
              LONG %%0_3_2_3_3_2_0_2_3_2_0_3_2_0_0_3
              LONG %%0_0_0_3_3_0_0_2_3_0_0_3_2_0_0_2                                           '20
              LONG %%0_0_0_3_3_0_0_2_3_2_0_2_0_0_0_2
              LONG %%0_0_0_3_3_0_0_2_3_3_3_0_0_0_0_2
              LONG %%0_0_0_3_3_0_0_2_3_0_2_3_0_0_0_2
              LONG %%0_0_0_3_3_0_0_2_3_0_0_3_2_0_0_2
              LONG %%0_0_0_3_3_0_2_2_3_0_0_3_2_0_0_2
              LONG %%0_0_0_3_3_0_2_2_3_2_0_3_2_0_2_3
              LONG %%0_0_2_3_3_2_2_3_3_2_3_3_0_0_2_3
              LONG %%0_0_0_0_0_0_0_2_0_0_2_0_0_0_0_2

JackStoneFon_tx9_ty6_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_2_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_3_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_3_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_3_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_2_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_2_3_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_3_2                                             '21
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_3_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_3_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_3_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_3_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_3_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_2_2_3_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_3_3_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_2_0

' ============================================================
' "Ende"
' ============================================================

JackStoneFon_tx0_ty7_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_3_3_3_3_3_3_1
              LONG %%0_0_0_0_0_0_0_0_1_3_1_1_2_3_2_0
              LONG %%0_0_0_0_0_0_0_0_2_2_0_0_1_3_1_0
              LONG %%0_0_0_0_0_0_0_0_2_1_0_0_1_3_1_0
              LONG %%0_0_1_0_0_0_0_0_0_0_0_0_1_3_1_0
              LONG %%0_1_3_3_1_3_2_0_0_0_2_0_1_3_1_0
              LONG %%0_3_3_0_3_3_2_0_0_0_3_1_2_3_1_0
              LONG %%0_3_2_0_0_3_1_0_0_0_3_2_2_3_1_0                                        '22
              LONG %%0_3_2_0_0_3_1_0_0_0_2_0_1_3_1_0
              LONG %%0_3_2_0_0_3_1_0_0_0_1_0_1_3_1_0
              LONG %%0_3_2_0_0_3_1_0_0_0_0_0_1_3_1_0
              LONG %%0_3_2_0_0_3_1_0_2_0_0_0_1_3_1_0
              LONG %%0_3_2_0_0_3_1_0_3_0_0_0_1_3_1_0
              LONG %%0_3_3_0_0_3_1_0_3_2_0_0_1_3_2_0
              LONG %%2_3_3_1_3_3_3_0_2_3_3_3_3_3_3_1

' Extracted from file "JackStoneFonts.bmp" at tile=(3, 7), size=(36, 36), palette index=(73)
JackStoneFon_tx3_ty7_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_2_2_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_3_0_0_0_0_0
              LONG %%0_0_0_0_1_0_0_0_0_2_3_1_0_0_0_0
              LONG %%0_0_0_3_3_3_1_0_0_2_3_3_3_2_0_0
              LONG %%0_0_1_3_0_1_3_0_0_2_3_1_0_3_1_0                                          '23
              LONG %%0_0_3_3_0_0_3_1_0_2_3_0_0_3_3_0
              LONG %%0_0_3_3_1_1_3_2_0_2_3_0_0_2_3_0
              LONG %%0_0_2_2_2_2_3_3_0_2_3_0_0_1_3_0
              LONG %%0_0_0_0_0_0_3_3_0_2_3_0_0_1_3_0
              LONG %%0_0_0_0_0_0_3_3_0_2_3_0_0_2_3_0
              LONG %%0_0_1_0_0_0_3_2_0_2_3_0_0_3_3_0
              LONG %%0_0_2_1_0_3_3_0_1_3_3_1_0_3_2_0
              LONG %%0_0_0_3_3_3_2_0_1_3_3_3_3_3_0_0

' ============================================================
' "Start"
' ============================================================

JackStoneFon_tx1_ty7_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_3_3_3_2_0
              LONG %%0_0_0_0_0_0_1_0_0_0_1_3_0_0_2_2
              LONG %%0_0_0_0_0_0_3_1_0_0_1_1_0_0_1_3
              LONG %%0_0_0_0_0_0_3_1_0_0_2_0_0_0_1_3
              LONG %%0_0_0_0_0_0_3_2_0_0_0_0_0_0_3_3
              LONG %%2_2_0_0_3_3_3_3_3_0_0_0_0_2_3_3
              LONG %%0_3_2_0_0_1_3_3_0_0_0_1_3_3_3_1
              LONG %%0_3_2_0_0_0_3_3_0_0_1_3_3_3_2_0
              LONG %%0_1_0_0_0_0_3_3_0_0_2_3_3_1_0_0                                          '24
              LONG %%3_1_0_0_0_0_3_3_0_0_3_3_0_0_0_0
              LONG %%2_3_1_0_0_0_3_3_0_0_3_2_0_0_0_0
              LONG %%0_3_3_0_0_0_3_3_0_0_3_2_0_0_0_2
              LONG %%0_2_3_0_0_0_3_3_0_0_3_2_0_0_0_3
              LONG %%0_3_3_0_2_1_3_3_0_0_2_3_0_0_1_3
              LONG %%3_3_1_0_0_3_3_2_0_0_0_3_3_2_3_3
              LONG %%1_0_0_0_0_0_1_0_0_0_0_0_1_1_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(2, 7), size=(36, 36), palette index=(72)
JackStoneFon_tx2_ty7_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_1_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_2_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_2_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_2_3_0_0_1_0_0_0_0_0_0_0_0_1
              LONG %%1_3_3_3_3_2_3_3_2_3_2_0_0_0_3_3                                         '25
              LONG %%0_0_2_3_1_1_3_2_3_3_2_0_0_2_3_1
              LONG %%0_0_2_3_0_0_0_0_2_3_1_0_0_3_3_0
              LONG %%0_0_2_3_0_0_0_0_2_3_1_0_0_3_3_1
              LONG %%0_0_2_3_0_0_0_0_2_3_1_0_0_3_3_3
              LONG %%0_0_2_3_0_0_0_0_2_3_1_0_0_3_3_0
              LONG %%0_0_2_3_0_0_0_0_2_3_1_0_0_3_3_0
              LONG %%0_0_2_3_0_0_0_0_2_3_1_0_1_3_3_0
              LONG %%0_2_2_3_0_0_0_0_2_3_1_0_3_3_3_1
              LONG %%0_2_3_3_0_0_0_1_3_3_3_1_2_3_3_2
              LONG %%0_0_1_0_0_0_0_0_0_0_0_0_0_1_0_0

' ============================================================
' "Passwort"
' ============================================================

JackStoneFon_tx4_ty7_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_2_3_3_3_3_3_0_0
              LONG %%0_0_0_0_0_0_0_3_3_1_0_3_3_1_0_0
              LONG %%0_0_0_0_0_0_1_3_2_0_0_3_3_1_0_0
              LONG %%0_0_0_0_0_0_2_3_2_0_0_3_3_1_0_0
              LONG %%0_0_1_0_0_0_2_3_2_0_0_3_3_1_0_0
              LONG %%2_3_2_3_0_0_1_3_2_0_0_3_3_1_0_0
              LONG %%3_2_0_2_2_0_0_3_3_0_0_3_3_1_0_0
              LONG %%3_1_0_2_3_0_0_1_3_2_1_3_3_1_0_0                                         '26
              LONG %%3_1_0_0_1_0_0_0_0_2_2_3_3_1_0_0
              LONG %%3_3_3_1_0_0_0_0_0_0_0_3_3_1_0_0
              LONG %%3_1_1_3_2_0_0_0_0_0_0_3_3_1_0_0
              LONG %%3_1_0_2_3_0_0_0_0_0_0_3_3_1_0_0
              LONG %%3_1_0_2_3_0_0_0_0_0_0_3_3_1_0_0
              LONG %%3_2_0_2_3_0_0_0_0_0_0_3_3_1_0_0
              LONG %%3_2_3_3_2_0_0_0_0_0_2_3_3_3_0_0
              LONG %%0_0_1_1_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(5, 7), size=(36, 36), palette index=(75)
JackStoneFon_tx5_ty7_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_0_0_0_0_0_1_0_0_0_0_0
              LONG %%3_1_1_3_2_3_1_0_1_3_3_3_2_0_0_0
              LONG %%1_0_2_2_0_2_3_0_1_2_0_1_3_1_0_1
              LONG %%0_0_2_0_0_2_3_0_1_1_0_1_3_1_0_2                                          '27
              LONG %%0_0_0_0_1_3_3_0_0_0_0_3_3_1_0_2
              LONG %%0_0_0_2_3_3_2_0_0_1_3_3_3_0_0_2
              LONG %%0_0_2_3_3_2_0_0_1_3_3_2_0_0_0_2
              LONG %%0_0_3_3_1_0_1_0_2_3_2_0_0_0_0_2
              LONG %%0_0_3_3_0_0_2_0_2_3_0_0_1_1_1_2
              LONG %%0_0_3_3_0_0_3_0_2_3_0_0_2_1_3_2
              LONG %%0_0_1_3_2_2_3_0_0_3_3_2_3_1_2_3
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_1

' Extracted from file "JackStoneFonts.bmp" at tile=(6, 7), size=(36, 36), palette index=(76)
JackStoneFon_tx6_ty7_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%1_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_3_2_0_0_2_3_3_1_0_3_3_3_1_3_3
              LONG %%0_0_3_2_0_0_2_2_0_0_1_3_2_0_2_3
              LONG %%0_0_3_3_0_0_1_3_0_0_2_3_0_0_3_3                                          '28
              LONG %%0_0_2_3_1_0_0_3_0_0_3_3_0_0_3_2
              LONG %%0_0_2_3_1_0_0_2_1_0_3_3_0_1_3_2
              LONG %%0_0_2_3_1_0_0_1_2_1_3_2_1_2_3_1
              LONG %%0_0_2_3_1_0_0_0_2_3_3_1_3_2_3_0
              LONG %%0_0_3_3_0_0_0_0_2_3_3_0_3_3_3_0
              LONG %%0_1_3_2_0_0_0_0_2_3_2_0_2_3_2_0
              LONG %%2_3_3_0_0_0_0_0_1_3_1_0_0_3_0_0
              LONG %%1_1_0_0_0_0_0_0_0_1_0_0_0_0_0_0

JackStoneFon_tx7_ty7_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_1_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_2_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_2_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_2_3_0_0_1_0_1_0_0_0_0_0_0
              LONG %%0_2_3_3_3_3_2_3_2_3_3_2_0_0_0_3
              LONG %%0_0_0_3_3_0_2_3_2_3_3_2_0_0_3_3                                          '29
              LONG %%0_0_0_2_3_0_0_0_0_3_3_0_0_1_3_2
              LONG %%0_0_0_2_3_0_0_0_0_3_3_0_0_2_3_1
              LONG %%0_0_0_2_3_0_0_0_0_3_3_0_0_2_3_1
              LONG %%0_0_0_2_3_0_0_0_0_3_3_0_0_2_3_1
              LONG %%0_0_0_2_3_0_0_0_0_3_3_0_0_2_3_1
              LONG %%0_0_0_2_3_0_0_0_0_3_3_0_0_1_3_1
              LONG %%0_1_1_3_3_0_0_0_0_3_3_0_0_0_3_2
              LONG %%0_0_3_3_2_0_0_0_2_3_3_3_0_0_0_3
              LONG %%0_0_0_1_0_0_0_0_0_0_0_0_0_0_0_0

' ============================================================
' "2007 Michael Zinn" (6 in 3 tiles, Trick)
' ============================================================

                                                                 
JackStoneFon_tx0_ty8_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_3_3_3_0_0_1_1
              LONG %%3_0_3_3_3_3_3_0_3_3_3_3_3_0_1_1
              LONG %%3_0_3_0_0_0_3_0_3_0_0_0_3_0_1_0                                           '2a
              LONG %%3_0_3_0_0_0_3_0_3_0_0_0_0_0_1_0
              LONG %%3_1_2_3_0_0_3_1_2_0_0_0_1_0_1_0
              LONG %%2_1_2_2_3_0_2_1_2_3_3_2_1_1_1_0
              LONG %%2_0_3_1_3_3_2_0_0_3_3_2_3_1_1_0
              LONG %%2_1_2_1_0_3_2_1_1_1_0_1_3_0_1_0
              LONG %%3_0_3_1_0_0_2_0_0_1_0_1_3_0_1_0
              LONG %%2_0_3_1_0_0_2_0_3_1_0_1_3_0_1_0
              LONG %%2_1_2_2_3_2_2_1_2_2_3_2_3_0_1_0
              LONG %%0_1_1_3_3_3_1_0_2_3_3_2_3_0_1_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(3, 8), size=(36, 36), palette index=(83)
JackStoneFon_tx3_ty8_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_2_1_1_1_1_2_3_3_3_3_0_0_3_2_3
              LONG %%3_2_1_1_1_1_2_3_3_3_3_0_3_3_2_3
              LONG %%3_1_1_0_0_0_2_0_0_0_3_0_3_0_1_0
              LONG %%3_0_1_1_0_0_3_3_0_0_0_0_3_0_1_0
              LONG %%3_0_1_1_0_0_0_3_0_0_0_0_3_3_1_0                                         '2b
              LONG %%3_0_0_1_1_0_0_3_3_0_0_0_3_3_2_0
              LONG %%3_0_0_1_1_0_0_0_3_0_0_0_3_0_2_3
              LONG %%3_0_0_0_1_1_0_0_3_3_0_0_3_0_1_3
              LONG %%3_0_0_0_1_1_0_0_0_3_0_0_3_0_1_0
              LONG %%3_1_0_0_0_1_1_0_0_3_3_0_3_0_1_0
              LONG %%3_2_1_1_1_1_1_0_0_0_3_0_3_2_2_3
              LONG %%3_2_1_1_1_1_1_0_0_0_3_0_0_2_2_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(1, 8), size=(36, 36), palette index=(81)
JackStoneFon_tx1_ty8_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_0_0_3_3_0_0_0_1_0
              LONG %%0_0_0_0_0_0_3_0_0_0_3_3_0_0_1_3
              LONG %%0_0_0_0_0_0_0_0_0_0_3_3_0_0_0_3
              LONG %%0_3_3_0_0_0_0_0_0_0_3_3_0_0_0_3
              LONG %%3_3_3_2_1_0_2_3_1_1_3_2_3_0_2_2
              LONG %%3_0_0_2_1_1_2_3_1_1_2_1_3_0_2_1                                        '2c
              LONG %%0_0_0_2_0_1_2_0_1_0_2_1_3_0_2_0
              LONG %%0_0_0_2_0_0_2_0_1_0_3_1_3_3_2_0
              LONG %%3_0_0_2_0_0_2_0_1_0_3_1_0_3_1_0
              LONG %%3_3_3_2_0_3_2_3_1_3_3_1_0_2_1_1
              LONG %%0_3_3_1_0_3_2_3_1_3_3_1_0_2_1_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' ============================================================
' "Level"
' ============================================================

                          
JackStoneFon_tx0_ty9_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_2_3_2_2_2_2_3_1_0_0_0_0_0
              LONG %%0_0_0_1_2_3_3_3_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0                                         '2d
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(3, 9), size=(36, 36), palette index=(93)
JackStoneFon_tx3_ty9_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0                                        '2e
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%2_1_0_0_1_3_3_2_0_0_0_0_0_0_0_0
              LONG %%3_2_0_2_3_3_2_3_3_0_0_0_0_0_0_0
              LONG %%1_0_0_3_3_0_0_0_3_3_0_0_0_0_0_0
              LONG %%0_0_1_3_1_0_0_0_1_3_1_0_0_0_0_0
              LONG %%0_0_2_3_1_0_0_0_0_3_3_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(1, 9), size=(36, 36), palette index=(91)
JackStoneFon_tx1_ty9_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%1_0_0_0_0_2_2_2_2_1_0_0_1_2_2_2
              LONG %%3_1_0_0_0_3_3_3_3_1_0_0_1_3_3_3
              LONG %%2_3_0_0_0_0_2_3_0_0_0_0_0_0_3_3
              LONG %%0_3_2_0_0_0_0_3_0_0_0_0_0_1_3_3
              LONG %%0_2_3_0_0_0_0_3_1_0_0_0_0_2_3_2

' Extracted from file "JackStoneFonts.bmp" at tile=(2, 9), size=(36, 36), palette index=(92)
JackStoneFon_tx2_ty9_bitmap    LONG
              LONG %%0_0_0_0_0_3_2_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_2_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_1_3_3_3
              LONG %%0_0_0_0_0_3_3_0_0_0_0_1_3_3_2_2
              LONG %%0_0_0_0_0_3_3_0_0_0_0_2_3_1_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_3_3_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_1_3_3_0_0_0
                                                                   
JackStoneFon_tx0_ty30_bitmap    LONG
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_3_0_0_0_0_0_0_0_0
              LONG %%3_2_2_2_2_3_3_3_1_1_0_0_0_0_0_0
              LONG %%3_3_3_3_3_3_3_3_3_3_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

JackStoneFon_tx3_ty30_bitmap    LONG
              LONG %%0_0_3_3_1_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_3_3_1_0_0_0_0_2_3_0_0_0_0_0
              LONG %%0_0_3_3_3_3_3_3_3_3_3_1_0_0_0_0
              LONG %%0_0_2_3_3_3_3_3_3_3_3_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_3_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_3_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_3_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_1_0_2_1_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_1_0_2_1_0
              LONG %%0_0_0_0_0_0_0_0_1_3_3_0_0_1_3_0
              LONG %%0_0_2_0_0_0_0_0_3_3_3_0_0_0_3_0
              LONG %%0_0_2_2_0_0_0_2_3_3_2_0_0_0_3_3
              LONG %%0_0_0_3_3_2_3_3_3_3_0_0_0_0_3_3
              LONG %%0_0_0_1_3_3_3_3_3_0_0_0_0_0_3_3
              LONG %%0_0_0_0_0_1_2_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

JackStoneFon_tx2_ty30_bitmap    LONG
              LONG %%0_1_3_1_0_0_0_2_2_0_0_0_0_2_3_1
              LONG %%0_0_3_2_0_0_0_1_3_0_0_0_0_3_3_0
              LONG %%3_3_3_3_0_0_0_0_3_0_0_0_1_3_3_0
              LONG %%3_3_3_3_0_0_0_0_3_0_0_0_2_3_2_0
              LONG %%0_0_3_3_0_0_0_0_2_2_0_0_3_3_1_0
              LONG %%0_0_3_3_0_0_0_0_1_3_0_0_3_3_0_0
              LONG %%0_0_3_3_0_0_0_0_0_3_0_0_3_3_0_0
              LONG %%0_1_3_3_0_0_0_0_0_3_1_1_3_2_0_0
              LONG %%0_2_3_2_0_0_0_0_0_2_2_2_3_1_0_0
              LONG %%0_3_3_2_0_0_0_0_0_0_3_3_3_0_0_0
              LONG %%2_3_3_1_0_0_0_0_0_0_3_3_3_0_0_0
              LONG %%3_3_3_0_0_0_0_0_0_0_2_3_2_0_0_0
              LONG %%3_3_1_0_0_0_0_0_0_0_2_3_1_0_0_0
              LONG %%3_1_0_0_0_0_0_0_0_0_1_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

JackStoneFon_tx1_ty30_bitmap    LONG
              LONG %%0_0_0_0_0_3_3_0_0_0_2_3_2_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_2_3_2_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_2_3_3_3_3_3
              LONG %%0_0_0_0_0_3_3_0_0_0_2_3_3_3_3_3
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_1_3_0_0_0_1
              LONG %%0_0_0_0_1_3_3_2_1_0_0_2_3_2_2_3
              LONG %%0_0_0_3_3_3_3_3_3_0_0_0_2_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_1_2_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' ============================================================
' "0123456789" (erst die oberen Hälften, dann die unteren)
' ============================================================
                                              
JackStoneFon_tx0_ty33_bitmap    LONG
              LONG %%0_0_0_0_0_1_3_3_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_3_3_3_3_2_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_2_3_1_0_0_0_3_3_0_0_0_0_0                                     '35
              LONG %%0_0_0_3_3_0_0_0_0_2_3_0_0_0_0_0
              LONG %%0_0_0_3_3_0_0_0_0_2_3_1_0_0_0_0
              LONG %%0_0_2_3_2_0_0_0_0_1_3_3_0_0_0_0
              LONG %%0_0_3_3_1_0_0_0_0_0_3_3_0_0_0_0
              LONG %%0_0_3_3_1_0_0_0_0_0_3_3_1_0_0_0
              LONG %%0_0_3_3_0_0_0_0_0_0_3_3_1_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_3_3_2_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_3_3_2_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_3_3_2_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_2_3_2_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_2_3_2_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_2_3_2_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(3, 33), size=(36, 36), palette index=(333)
JackStoneFon_tx3_ty33_bitmap    LONG
              LONG %%0_0_0_0_0_1_2_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_2_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_3_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_1_2_3_1_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(1, 33), size=(36, 36), palette index=(331)
JackStoneFon_tx1_ty33_bitmap    LONG
              LONG %%0_0_0_0_0_2_3_3_2_0_0_0_0_0_0_0
              LONG %%0_0_0_1_3_3_3_2_3_3_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_2_3_0_0_0_0_0
              LONG %%0_0_1_3_3_0_0_0_0_1_3_1_0_0_0_0
              LONG %%0_0_3_3_3_0_0_0_0_0_3_2_0_0_0_0
              LONG %%0_0_3_3_2_0_0_0_0_0_3_3_0_0_0_0
              LONG %%0_0_3_3_2_0_0_0_0_1_3_3_0_0_0_0
              LONG %%0_0_3_3_1_0_0_0_0_1_3_3_0_0_0_0
              LONG %%0_0_3_3_2_0_0_0_0_0_2_1_0_0_0_0
              LONG %%0_0_3_3_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_2_3_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_1_3_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_2_3_2_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_1_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_3_2_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(2, 33), size=(36, 36), palette index=(332)
JackStoneFon_tx2_ty33_bitmap    LONG
              LONG %%0_0_0_0_2_3_3_2_1_0_0_0_0_0_0_0
              LONG %%0_0_0_2_3_3_2_3_3_2_0_0_0_0_0_0
              LONG %%0_0_1_3_3_0_0_0_1_3_1_0_0_0_0_0
              LONG %%0_0_2_3_2_0_0_0_0_2_3_0_0_0_0_0
              LONG %%0_0_3_3_1_0_0_0_0_1_3_1_0_0_0_0
              LONG %%0_0_3_3_0_0_0_0_0_1_3_2_0_0_0_0
              LONG %%0_0_3_3_0_0_0_0_0_1_3_2_0_0_0_0
              LONG %%0_0_1_3_2_0_0_0_0_0_2_1_0_0_0_0
              LONG %%0_0_0_3_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_2_3_1_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_2_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_1_3_3_3_3_1_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_1_0_0_0_0_0_0_0
              LONG %%0_0_3_3_3_2_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_3_3_2_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(4, 33), size=(36, 36), palette index=(334)
JackStoneFon_tx4_ty33_bitmap    LONG
              LONG %%0_0_0_0_0_2_1_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_1_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_2_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_3_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_0_2_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_1_3_1_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_3_2_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_2_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_1_3_1_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_2_3_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(5, 33), size=(36, 36), palette index=(335)
JackStoneFon_tx5_ty33_bitmap    LONG
              LONG %%0_0_3_2_2_2_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_3_3_3_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_3_3_3_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_1_2_2_2_2_2_1_3_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_3_0_0_0_0_0
              LONG %%0_0_0_0_0_1_2_1_1_1_3_0_0_0_0_0
              LONG %%0_0_0_0_2_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_2_3_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_1_2_0_0_0_0_0
              LONG %%0_0_2_3_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_3_3_2_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(6, 33), size=(36, 36), palette index=(336)
JackStoneFon_tx6_ty33_bitmap    LONG
              LONG %%0_0_2_2_1_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_2_2_3_3_1_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_2_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_3_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_3_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_3_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_3_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_3_3_0_0_0_0
              LONG %%0_0_0_0_1_3_3_3_1_0_3_3_0_0_0_0
              LONG %%0_0_0_2_3_3_3_2_3_1_3_3_0_0_0_0
              LONG %%0_0_0_3_3_1_0_0_0_2_3_3_1_0_0_0
              LONG %%0_0_3_3_2_0_0_0_0_0_3_3_1_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(7, 33), size=(36, 36), palette index=(337)
JackStoneFon_tx7_ty33_bitmap    LONG
              LONG %%0_2_1_1_1_1_1_1_1_1_3_1_0_0_0_0
              LONG %%0_3_3_3_3_3_3_3_3_3_3_1_0_0_0_0
              LONG %%0_2_3_3_3_3_3_3_3_3_3_1_0_0_0_0
              LONG %%0_1_3_2_2_2_2_2_2_3_3_1_0_0_0_0
              LONG %%0_0_3_1_0_0_0_0_0_0_3_2_0_0_0_0
              LONG %%0_0_3_2_0_0_0_0_0_0_1_2_0_0_0_0
              LONG %%0_0_2_3_0_0_0_0_0_0_0_2_0_0_0_0
              LONG %%0_0_1_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_1_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_2_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_1_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_1_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_2_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_3_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(8, 33), size=(36, 36), palette index=(338)
JackStoneFon_tx8_ty33_bitmap    LONG
              LONG %%0_0_0_0_1_2_3_3_2_0_0_0_0_0_0_0
              LONG %%0_0_0_1_3_3_1_1_3_3_0_0_0_0_0_0
              LONG %%0_0_0_3_3_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_1_3_2_0_0_0_0_2_3_1_0_0_0_0
              LONG %%0_0_2_3_1_0_0_0_0_1_3_2_0_0_0_0
              LONG %%0_0_2_3_0_0_0_0_0_1_3_3_0_0_0_0
              LONG %%0_0_2_3_0_0_0_0_0_1_3_3_0_0_0_0
              LONG %%0_0_2_3_1_0_0_0_0_1_3_3_0_0_0_0
              LONG %%0_0_0_3_2_0_0_0_0_2_3_2_0_0_0_0
              LONG %%0_0_0_3_3_0_0_0_1_3_3_1_0_0_0_0
              LONG %%0_0_0_1_3_3_0_0_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_1_3_2_3_3_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_3_3_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_3_3_3_3_1_0_0_0_0_0_0
              LONG %%0_0_0_2_3_3_2_0_2_3_0_0_0_0_0_0

JackStoneFon_tx9_ty33_bitmap    LONG
              LONG %%0_0_0_0_0_2_3_3_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_2_3_3_0_0_0_0_0_0
              LONG %%0_0_0_2_3_2_0_0_1_3_3_0_0_0_0_0
              LONG %%0_0_0_3_3_0_0_0_0_2_3_1_0_0_0_0
              LONG %%0_0_2_3_2_0_0_0_0_1_3_2_0_0_0_0
              LONG %%0_0_3_3_0_0_0_0_0_0_3_3_0_0_0_0
              LONG %%0_0_3_3_0_0_0_0_0_0_3_3_0_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_3_3_0_0_0_0
              LONG %%0_1_3_2_0_0_0_0_0_0_3_3_1_0_0_0
              LONG %%0_2_3_2_0_0_0_0_0_0_3_3_1_0_0_0
              LONG %%0_2_3_2_0_0_0_0_0_0_3_3_1_0_0_0
              LONG %%0_2_3_2_0_0_0_0_0_0_3_3_0_0_0_0
              LONG %%0_2_3_2_0_0_0_0_0_1_3_3_0_0_0_0
              LONG %%0_2_3_2_0_0_0_0_0_2_3_2_0_0_0_0
              LONG %%0_2_3_3_0_0_0_0_0_3_3_1_0_0_0_0
              LONG %%0_1_3_3_3_1_0_1_3_3_3_0_0_0_0_0
                     
JackStoneFon_tx0_ty32_bitmap    LONG
              LONG %%0_1_3_3_0_0_0_0_0_0_2_3_2_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_2_3_2_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_3_3_2_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_3_3_2_0_0_0
              LONG %%0_0_3_3_1_0_0_0_0_0_3_3_1_0_0_0
              LONG %%0_0_3_3_1_0_0_0_0_0_3_3_0_0_0_0
              LONG %%0_0_2_3_2_0_0_0_0_0_3_3_0_0_0_0
              LONG %%0_0_1_3_2_0_0_0_0_1_3_2_0_0_0_0
              LONG %%0_0_0_3_3_0_0_0_0_2_3_1_0_0_0_0
              LONG %%0_0_0_3_3_0_0_0_0_2_3_0_0_0_0_0
              LONG %%0_0_0_1_3_1_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_2_3_1_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(3, 31), size=(36, 36), palette index=(313)
JackStoneFon_tx3_ty31_bitmap    LONG
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_1_2_3_3_3_2_1_1_0_0_0_0_0
              LONG %%0_0_1_3_3_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(1, 31), size=(36, 36), palette index=(311)
JackStoneFon_tx1_ty31_bitmap    LONG
              LONG %%0_0_0_0_0_2_3_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_3_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_2_3_0_0_0_0_0_0
              LONG %%0_0_1_0_0_0_0_0_0_3_2_0_0_0_0_0
              LONG %%0_0_3_0_0_0_0_0_0_2_3_0_0_0_0_0
              LONG %%0_0_3_1_0_0_0_0_0_1_3_1_0_0_0_0
              LONG %%0_0_3_2_0_0_0_0_0_0_3_2_0_0_0_0
              LONG %%0_0_2_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_2_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_1_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

JackStoneFon_tx1_ty32_bitmap    LONG
              LONG %%0_1_3_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_2_3_2_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_2_3_2_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_3_3_2_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_2_3_2_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_2_3_2_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_1_3_2_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_3_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_2_3_2_0_0_0_0_0_3_2_0_0_0_0
              LONG %%0_0_0_3_3_0_0_0_0_1_3_3_0_0_0_0
              LONG %%0_0_0_1_3_3_1_0_1_3_3_2_0_0_0_0
              LONG %%0_0_0_0_1_3_3_3_3_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(4, 31), size=(36, 36), palette index=(314)
JackStoneFon_tx4_ty31_bitmap    LONG
              LONG %%0_0_0_0_3_3_0_0_0_1_3_2_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_0_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_0_1_3_2_0_0_0
              LONG %%0_2_3_3_3_3_3_3_3_3_3_3_2_0_0_0
              LONG %%0_2_3_3_3_3_3_3_3_3_3_3_2_0_0_0
              LONG %%0_0_1_1_3_3_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_1_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_1_3_3_3_2_1_0_0_0_0_0_0_0_0
              LONG %%0_1_3_3_3_3_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(5, 31), size=(36, 36), palette index=(315)
JackStoneFon_tx5_ty31_bitmap    LONG
              LONG %%0_0_3_3_1_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_3_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_3_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_3_3_1_0_0_0_0_0_2_0_0_0_0_0
              LONG %%0_0_1_3_3_0_0_0_0_1_3_2_0_0_0_0
              LONG %%0_0_0_3_3_1_0_0_0_3_3_2_0_0_0_0
              LONG %%0_0_0_1_3_3_1_0_2_3_3_1_0_0_0_0
              LONG %%0_0_0_0_1_3_3_3_3_3_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(6, 31), size=(36, 36), palette index=(316)
JackStoneFon_tx6_ty31_bitmap    LONG
              LONG %%0_0_3_3_1_0_0_0_0_0_3_3_1_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_3_3_1_0_0_0
              LONG %%0_1_3_2_0_0_0_0_0_0_3_3_1_0_0_0
              LONG %%0_2_3_2_0_0_0_0_0_0_3_3_1_0_0_0
              LONG %%0_2_3_2_0_0_0_0_0_0_3_3_1_0_0_0
              LONG %%0_2_3_2_0_0_0_0_0_0_3_3_0_0_0_0
              LONG %%0_1_3_2_0_0_0_0_0_0_3_3_0_0_0_0
              LONG %%0_1_3_2_0_0_0_0_0_1_3_3_0_0_0_0
              LONG %%0_0_3_3_0_0_0_0_0_2_3_2_0_0_0_0
              LONG %%0_0_3_3_0_0_0_0_0_3_3_1_0_0_0_0
              LONG %%0_0_1_3_2_0_0_0_1_3_3_0_0_0_0_0
              LONG %%0_0_0_3_3_1_0_1_3_3_1_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_2_2_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(7, 31), size=(36, 36), palette index=(317)
JackStoneFon_tx7_ty31_bitmap    LONG
              LONG %%0_0_0_0_0_3_2_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_2_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_3_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_3_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_3_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_3_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(8, 31), size=(36, 36), palette index=(318)
JackStoneFon_tx8_ty31_bitmap    LONG
              LONG %%0_0_0_3_3_3_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_2_3_3_0_0_0_0_2_3_1_0_0_0_0
              LONG %%0_0_3_3_2_0_0_0_0_0_3_3_0_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_3_3_0_0_0_0
              LONG %%0_1_3_3_0_0_0_0_0_0_2_3_1_0_0_0
              LONG %%0_1_3_2_0_0_0_0_0_0_1_3_2_0_0_0
              LONG %%0_1_3_2_0_0_0_0_0_0_1_3_2_0_0_0
              LONG %%0_0_3_2_0_0_0_0_0_0_1_3_1_0_0_0
              LONG %%0_0_3_3_0_0_0_0_0_0_2_3_1_0_0_0
              LONG %%0_0_2_3_0_0_0_0_0_0_3_3_0_0_0_0
              LONG %%0_0_0_3_2_0_0_0_0_1_3_2_0_0_0_0
              LONG %%0_0_0_2_3_1_0_0_1_3_3_0_0_0_0_0
              LONG %%0_0_0_0_2_3_3_3_3_3_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_2_2_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(9, 31), size=(36, 36), palette index=(319)
JackStoneFon_tx9_ty31_bitmap    LONG
              LONG %%0_1_3_2_1_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_3_3_0_0_2_2_1_0_0_0_0_0_0_0
              LONG %%0_0_3_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_3_3_1_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_2_3_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_2_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_1_3_2_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_3_1_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_3_3_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_3_3_3_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_2_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' ============================================================
' "Sieg!"
' ============================================================

JtoneFon_tx0_ty32_bitmap    LONG
              LONG %%0_0_0_1_2_3_3_2_0_0_0_0_0_0_0_0
              LONG %%0_1_3_3_3_3_3_3_3_1_0_0_0_0_0_0
              LONG %%0_2_3_3_2_0_0_1_3_3_0_0_0_0_0_0
              LONG %%0_2_3_2_0_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_2_3_1_0_0_0_0_0_2_3_1_0_0_0_0
              LONG %%0_3_3_0_0_0_0_0_0_1_3_2_0_0_0_0
              LONG %%0_3_2_0_0_0_0_0_0_0_3_3_0_0_0_0
              LONG %%0_3_0_0_0_0_0_0_0_0_3_3_0_0_0_0
              LONG %%0_3_0_0_0_0_0_0_0_0_3_3_1_0_0_0
              LONG %%0_2_0_0_0_0_0_0_0_1_3_3_1_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_3_3_1_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_2_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_3_3_3_2_0_0_0_0
              LONG %%0_0_0_0_0_0_2_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_1_3_3_3_3_3_2_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(3, 32), size=(36, 36), palette index=(323)
JackStoneFon_tx3_ty32_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_2_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_3_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_0_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_0_3_3_1_0_0_0_0
              LONG %%3_3_0_0_0_0_0_0_0_3_3_3_3_1_0_0
              LONG %%2_3_2_0_0_0_0_0_0_3_3_3_3_1_0_0
              LONG %%0_3_3_0_0_0_0_0_0_3_3_2_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(1, 32), size=(36, 36), palette index=(321)
JStoneFon_tx1_ty32_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%1_1_0_0_0_0_0_0_0_0_0_0_0_1_1_1
              LONG %%3_3_3_1_0_0_0_0_0_0_0_0_3_3_3_3
              LONG %%0_0_2_3_0_0_0_0_0_0_0_3_3_3_1_2
              LONG %%0_0_0_3_3_0_0_0_0_0_2_3_3_0_0_0
              LONG %%0_0_0_2_3_1_0_0_0_0_3_3_1_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(2, 32), size=(36, 36), palette index=(322)
JackStoneFon_tx2_ty32_bitmap    LONG
              LONG %%0_0_0_1_3_1_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_1_3_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_1_3_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_1_3_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_1_3_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_0_1_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_3_3_3_1_0_0_1
              LONG %%0_0_0_3_3_2_0_0_0_3_3_3_3_0_3_3
              LONG %%0_0_0_2_3_2_0_0_0_3_3_0_2_3_3_2
              LONG %%0_0_0_2_3_1_0_0_0_1_1_0_2_3_3_0
              LONG %%0_0_0_1_3_1_0_0_0_0_0_0_3_3_1_0
                         
JackStoneFon_tx0_ty34_bitmap    LONG
              LONG %%0_0_0_2_3_3_3_3_3_2_0_0_0_0_0_0
              LONG %%0_0_2_3_3_3_3_3_2_0_0_0_0_0_0_0
              LONG %%0_2_3_3_3_3_3_2_0_0_0_0_0_0_0_0
              LONG %%0_3_3_3_3_3_1_0_0_0_0_0_0_0_0_0
              LONG %%2_3_3_3_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_3_3_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_3_3_1_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_3_3_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_3_2_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_3_1_0_0_0_0_0_0_0_0_0_1_0_0_0
              LONG %%3_3_1_0_0_0_0_0_0_0_0_2_2_0_0_0
              LONG %%3_3_1_0_0_0_0_0_0_0_0_2_2_0_0_0
              LONG %%3_3_1_0_0_0_0_0_0_0_0_3_2_0_0_0
              LONG %%2_3_2_0_0_0_0_0_0_0_0_3_2_0_0_0
              LONG %%1_3_3_0_0_0_0_0_0_0_1_3_1_0_0_0
              LONG %%0_3_3_1_0_0_0_0_0_0_3_3_1_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(3, 34), size=(36, 36), palette index=(343)
JackStoneFon_tx3_ty34_bitmap    LONG
              LONG %%0_1_3_2_0_0_0_0_0_3_3_1_0_0_0_0
              LONG %%0_0_3_3_0_0_0_0_0_3_3_1_0_0_0_0
              LONG %%0_0_3_3_0_0_0_0_0_3_3_1_0_0_0_0
              LONG %%0_0_3_3_1_0_0_0_0_3_3_1_0_0_0_0
              LONG %%2_2_3_3_2_0_0_0_0_3_3_1_0_0_0_0
              LONG %%3_3_3_3_3_0_0_0_0_3_3_1_0_0_0_0
              LONG %%1_1_3_3_3_0_0_0_0_3_3_1_0_0_0_0
              LONG %%0_0_2_3_3_0_0_0_0_3_3_1_0_0_0_0
              LONG %%0_0_2_3_3_0_0_0_0_3_3_1_0_0_0_0
              LONG %%0_0_3_3_3_0_0_0_0_3_3_1_0_0_0_0
              LONG %%0_0_3_3_3_0_0_0_0_3_3_1_0_0_0_0
              LONG %%0_0_3_3_3_0_0_0_0_3_3_1_0_0_0_0
              LONG %%0_0_3_3_2_0_0_0_0_3_3_1_0_0_0_0
              LONG %%0_2_3_3_1_0_0_0_0_3_3_1_0_0_0_0
              LONG %%0_3_3_3_0_0_0_0_0_3_3_1_0_0_0_0
              LONG %%2_3_3_2_0_0_0_0_0_3_3_1_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(1, 34), size=(36, 36), palette index=(341)
JackStoneFon_tx1_ty34_bitmap    LONG
              LONG %%0_0_0_1_3_2_0_0_0_0_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_1_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_2_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_2_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_2_3_3_2_2_2_2
              LONG %%0_0_0_1_3_3_0_0_0_2_3_3_3_3_3_3
              LONG %%0_0_0_2_3_2_0_0_0_0_1_1_1_1_1_1
              LONG %%0_0_0_3_3_2_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_1_3_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_3_3_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_3_3_3_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%2_2_1_0_3_1_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_2_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_2_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_2_0_0_0_2_0_0_0_0_0_0
              LONG %%0_0_0_0_3_2_0_0_0_2_3_0_0_0_0_0

' Extracted from file "JackStoneFonts.bmp" at tile=(2, 34), size=(36, 36), palette index=(342)
JackStoneFon_tx2_ty34_bitmap    LONG
              LONG %%0_0_0_1_3_1_0_0_0_0_0_0_3_3_0_0
              LONG %%0_0_0_1_3_1_0_0_0_0_0_1_3_3_0_0
              LONG %%0_0_0_0_3_0_0_0_0_0_0_2_3_3_0_0
              LONG %%0_0_0_0_3_0_0_0_0_0_0_2_3_3_0_0
              LONG %%0_0_0_0_3_0_0_0_0_0_0_2_3_3_0_0
              LONG %%0_0_0_0_3_0_0_0_0_0_0_2_3_3_0_0
              LONG %%0_0_0_0_3_0_0_0_0_0_0_1_3_3_0_0
              LONG %%0_0_0_0_3_0_0_0_0_0_0_0_3_3_0_0
              LONG %%0_0_0_0_2_0_0_0_0_0_0_0_2_3_1_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_3_3_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_1_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_2_3_1_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_0_0_0_0_0_0_0
                                                      
JackStoneFon_tx0_ty35_bitmap    LONG
              LONG %%0_1_3_3_0_0_0_0_0_2_3_3_1_0_0_0
              LONG %%0_0_3_3_3_2_1_1_3_3_3_3_0_0_0_0
              LONG %%0_0_0_2_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_1_2_2_2_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

JackStoneFon_tx3_ty35_bitmap    LONG
              LONG %%3_3_3_1_0_0_0_0_1_3_3_2_0_0_0_0
              LONG %%3_3_2_0_0_0_0_2_3_3_3_3_2_1_0_0
              LONG %%3_2_0_0_0_0_1_3_3_3_3_3_3_2_0_0
              LONG %%1_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

JackStoneFon_tx2_ty35_bitmap    LONG
              LONG %%2_2_1_3_3_2_0_0_0_0_3_3_1_0_0_2
              LONG %%3_3_3_3_3_1_0_0_0_0_1_3_3_3_3_3
              LONG %%3_3_3_3_3_0_0_0_0_0_0_1_3_3_3_3
              LONG %%2_2_2_2_3_0_0_0_0_0_0_0_0_2_2_2
              LONG %%0_0_0_0_2_2_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_2_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_2_3_3_1_0_0_0_0_0_0_0_0_0
              LONG %%2_2_3_3_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%3_3_3_3_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%1_1_1_1_0_0_0_0_0_0_0_0_0_0_0_0

JackStoneFon_tx1_ty35_bitmap    LONG
              LONG %%0_0_0_3_3_3_0_0_0_0_0_0_1_2_2_2
              LONG %%0_0_0_3_3_3_0_0_0_0_0_2_3_3_3_3
              LONG %%0_0_0_3_3_3_0_0_0_0_1_3_3_3_3_3
              LONG %%0_0_0_0_2_0_0_0_0_0_3_3_3_3_2_2
              LONG %%0_0_0_0_0_0_0_0_0_2_3_3_2_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_3_0_0_0_0_0                                '54 hex
              LONG %%0_0_0_0_0_0_0_0_0_1_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_3_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_1_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_3_1_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_2_3_3_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_1_2_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
                                                            

' END OF FONTS ==================================================================================
' END OF FONTS ==================================================================================
' END OF FONTS ==================================================================================
' END OF FONTS ==================================================================================
' END OF FONTS ==================================================================================
' END OF FONTS ==================================================================================
' END OF FONTS ==================================================================================
' END OF FONTS ==================================================================================
' END OF FONTS ==================================================================================
' END OF FONTS ==================================================================================
' END OF FONTS ==================================================================================
' END OF FONTS ==================================================================================
' END OF FONTS ==================================================================================
' END OF FONTS ==================================================================================
' END OF FONTS ==================================================================================
' END OF FONTS ==================================================================================
' END OF FONTS ==================================================================================


TileSpace 
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0 '55
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
               
TileWall
{
LONG %%0000322223111300   'bunte Steine
LONG %%3000322223111300
LONG %%3000322223111300
LONG %%3001113223111300

LONG %%3003113323111300
LONG %%3303110003331333
LONG %%2223113000002222
LONG %%3223113000003222

LONG %%3223313000003222
LONG %%3222223333303222
LONG %%3333222211113333
LONG %%1111322231110000

LONG %%3111322231113000
LONG %%3111322231113000
LONG %%3111322231113000
LONG %%3331333233313330
'}
{
LONG %%0031113222230000
LONG %%0031113222230003
LONG %%0031113222330003
LONG %%0031113223111003

LONG %%0031113233113003
LONG %%3331333000113033
LONG %%2222000003113222
LONG %%2223000003113223

LONG %%2223000003133223
LONG %%2223033333222223
LONG %%3333111122223333
LONG %%0000111322231111

LONG %%0003111322231113
LONG %%0003111322231113
LONG %%0003111322231113
LONG %%0333133323331333
}

 {
 LONG %%0333133323331333
LONG %%0003111322231113
LONG %%0003111322231113
LONG %%0003111322231113
LONG %%0000111322231111
LONG %%3333111122223333
LONG %%2223033333222223
LONG %%2223000003133223
LONG %%2223000003113223
LONG %%2222000003113222
LONG %%3331333000113033
LONG %%0031113233113003
LONG %%0031113223111003
LONG %%0031113222330003
LONG %%0031113222230003
LONG %%0031113222230000


 
LONG %%0333133323331333
LONG %%0003111322231113
LONG %%0003111322231113
LONG %%0003111322231113
LONG %%0000111322231111
LONG %%3333111122223333
LONG %%2223033333222222
LONG %%2223000003133222
LONG %%2223000003113222
LONG %%2222000003113222
LONG %%3331333000113033
LONG %%0031113233113000
LONG %%0031113223111000
LONG %%0031113222330000
LONG %%0031113222230000
LONG %%0031113222230000
}

{
LONG %%0003111322231333
LONG %%0003111322231113
LONG %%0003111322231113
LONG %%0003111322231113
LONG %%0000111322231111
LONG %%3333111122223333
LONG %%2223033333222222
LONG %%2223000003133222
LONG %%2223000003113222
LONG %%2222000003113222
LONG %%3331333000113033
LONG %%0031113233113000
LONG %%0031113223111000
LONG %%0031113222330000
LONG %%0031113222230000
LONG %%0031113222230000

}

{
LONG %%2022020202220202                'piano
LONG %%2023030303230303
LONG %%2023030303230303 
LONG %%2023030303230303
LONG %%2023030303230303
LONG %%2023030303230303
LONG %%2023030303230303
LONG %%2023030303230303
LONG %%2123131313231313
LONG %%2323232323232323
LONG %%2323232323232323
LONG %%2323232323232323
LONG %%2323232323232323
LONG %%2323232323232323
LONG %%2222222222222222
LONG %%1111111111111111
'}
{
LONG %%1122211122221112         'glaenzendes Metall
LONG %%0311133211113331
LONG %%0321123311112331
LONG %%0331113321111331
LONG %%0332112331211231
LONG %%0333111332311131
LONG %%0233211233321121
LONG %%0132311133331112
LONG %%0121321123332112
LONG %%0111231113333112
LONG %%0211132112333212
LONG %%0311123111333312
LONG %%0321113211233321
LONG %%0111111111111112
LONG %%0000000000000001
LONG %%0000000000000000
'}
 {

LONG %%1122211122221112
LONG %%0311133311113331
LONG %%0331113311111331
LONG %%0331113331111331
LONG %%0333111331311131
LONG %%0333111333311131
LONG %%0133311133331112
LONG %%0131311133331112
LONG %%0111331113333112
LONG %%0111131113333112
LONG %%0311133111333312
LONG %%0311113111333312
LONG %%0331113311133331
LONG %%0111111111111112
LONG %%0000000000000001
LONG %%0000000000000000
       ' }
        

              LONG %%1_3_3_3_3_3_1_2_3_3_3_1_2_3_3_3 '56   'Mauerwerk
              LONG %%0_2_3_3_3_2_0_3_3_3_2_0_3_3_3_2
              LONG %%0_1_2_3_2_1_0_2_3_3_2_0_2_3_3_2
              LONG %%0_1_1_2_1_1_0_1_3_2_1_0_1_3_2_1
              LONG %%0_1_1_0_1_1_0_1_0_1_1_0_1_0_1_1
              LONG %%0_1_0_0_0_1_0_1_0_0_1_0_1_0_0_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%1_2_3_3_3_1_2_3_3_3_1_3_3_3_3_3
              LONG %%0_3_3_3_2_0_3_3_3_2_0_2_3_3_3_2
              LONG %%0_2_3_3_2_0_2_3_3_2_0_1_2_3_2_1
              LONG %%0_1_3_2_1_0_1_3_2_1_0_1_1_2_1_1
              LONG %%0_1_0_1_1_0_1_0_1_1_0_1_1_0_1_1
              LONG %%0_1_0_0_1_0_1_0_0_1_0_1_0_0_0_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
' }


                   {
              LONG %%3_3_3_3_3_3_3_3_3_3_3_3_3_3_3_3     '56
              LONG %%2_2_2_2_2_2_2_2_2_2_2_2_2_2_2_2
              LONG %%1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_0_0_3_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_3_0_0_3_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_3_0_0_3_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_3_0_0_3_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_3_0_0_3_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_3_0_0_3_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
                        }
         
TileRock
              LONG %%0_0_1_1_1_1_1_1_1_1_1_1_1_0_0_0  '57
              LONG %%0_1_2_2_2_2_2_2_2_2_2_2_2_3_0_0
              LONG %%1_1_1_2_2_2_2_2_2_2_2_2_3_3_3_0
              LONG %%0_1_1_1_2_2_2_2_2_2_2_3_3_3_2_1
              LONG %%0_0_1_1_1_1_1_1_1_1_1_3_3_2_2_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_2_2_2_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_2_2_2_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_2_2_2_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_2_2_2_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_2_2_2_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_2_2_2_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_2_2_2_1
              LONG %%0_0_0_0_1_1_1_1_1_1_1_1_1_2_2_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_1_1_1_2_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_1_1_1_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_1_0_0

TileLadder
              LONG %%0_0_0_1_3_3_3_3_3_3_3_3_3_1_0_0    '58
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_1_1_0_0
              LONG %%0_0_0_1_0_0_0_0_0_0_0_0_0_1_0_0
              LONG %%0_0_0_1_0_0_0_0_0_0_0_0_0_1_0_0
              LONG %%0_0_0_1_3_3_3_3_3_3_3_3_3_1_0_0
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_1_1_0_0
              LONG %%0_0_0_1_0_0_0_0_0_0_0_0_0_1_0_0
              LONG %%0_0_0_1_0_0_0_0_0_0_0_0_0_1_0_0
              LONG %%0_0_0_1_3_3_3_3_3_3_3_3_3_1_0_0
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_1_1_0_0
              LONG %%0_0_0_1_0_0_0_0_0_0_0_0_0_1_0_0
              LONG %%0_0_0_1_0_0_0_0_0_0_0_0_0_1_0_0
              LONG %%0_0_0_1_3_3_3_3_3_3_3_3_3_1_0_0
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_1_1_0_0
              LONG %%0_0_0_1_0_0_0_0_0_0_0_0_0_1_0_0
              LONG %%0_0_0_1_0_0_0_0_0_0_0_0_0_1_0_0

TileSand
              LONG %%2_1_3_1_3_1_0_3_3_1_3_3_3_3_3_3       '59
              LONG %%0_1_3_0_1_3_3_3_3_0_3_3_1_1_3_3
              LONG %%1_0_3_3_3_1_3_2_3_0_1_3_0_3_3_3
              LONG %%0_1_0_1_1_2_0_1_3_1_3_1_3_3_1_1
              LONG %%0_0_1_2_2_1_3_1_0_1_2_0_1_0_3_3
              LONG %%0_0_0_0_3_0_2_1_3_2_1_3_3_1_2_1
              LONG %%2_1_1_2_1_2_2_2_1_0_1_2_0_3_1_3
              LONG %%1_0_2_1_2_3_0_1_1_1_3_2_2_3_3_2
              LONG %%0_0_1_3_1_1_1_3_2_2_2_3_1_1_3_3
              LONG %%1_0_1_1_2_1_2_1_1_2_1_3_0_2_3_1
              LONG %%2_0_3_3_0_0_1_0_2_2_2_1_2_3_3_3
              LONG %%0_1_0_1_1_0_1_2_3_1_1_1_0_2_1_3
              LONG %%0_1_2_0_0_1_2_1_0_0_3_2_0_3_3_3
              LONG %%0_2_0_1_2_0_2_0_2_1_0_0_0_1_0_3
              LONG %%0_0_0_0_0_0_0_0_2_0_0_2_0_2_1_3
              LONG %%0_0_0_0_1_0_0_0_1_0_0_0_0_0_0_1

TileMonster
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0       '5a
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_2_2_0_0_0_0_0_2_2_2_0_0_0
              LONG %%0_0_0_0_2_0_0_0_0_0_2_2_0_0_0_0
              LONG %%0_0_0_0_2_2_0_0_0_2_2_2_0_0_0_0
              LONG %%0_0_0_0_2_2_0_0_0_2_2_2_0_0_0_0
              LONG %%0_0_0_0_2_2_2_0_2_2_2_2_0_0_0_0
              LONG %%0_0_0_0_2_0_2_0_2_2_2_2_0_0_0_0
              LONG %%0_0_0_0_2_0_2_2_2_0_2_2_0_0_0_0
              LONG %%0_0_0_0_2_0_0_2_2_0_2_2_0_0_0_0
              LONG %%0_0_0_0_2_0_0_2_0_0_2_2_0_0_0_0
              LONG %%0_0_0_2_2_0_0_2_0_0_2_2_2_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

TileMonsterHold
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0      '5b
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_2_0_2_2_0_2_2_0_0_0_0_0_2_2_2
              LONG %%0_2_0_2_2_0_0_2_0_0_0_0_0_2_2_0
              LONG %%0_2_0_2_2_0_0_2_2_0_0_0_2_2_2_0
              LONG %%0_2_0_2_2_0_0_2_2_0_0_0_2_2_2_0
              LONG %%0_2_2_2_2_0_0_2_2_2_0_2_2_2_2_0
              LONG %%0_2_2_2_2_0_0_2_0_2_0_2_2_2_2_0
              LONG %%0_2_0_2_2_0_0_2_0_2_2_2_0_2_2_0
              LONG %%0_2_0_2_2_0_0_2_0_0_2_2_0_2_2_0
              LONG %%0_2_0_2_2_0_0_2_0_0_2_0_0_2_2_0
              LONG %%0_2_0_2_2_0_2_2_0_0_2_0_0_2_2_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

TileGhost
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0       '5c
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_2_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_0_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_2_2_2_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

TileBang
              LONG %%0_0_0_0_0_0_0_0_0_0_2_0_0_0_0_0        '5d
              LONG %%0_0_0_0_0_2_0_0_0_0_1_0_0_0_0_0
              LONG %%0_0_0_0_0_2_1_0_0_1_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_2_2_3_3_2_1_2_0_0
              LONG %%0_0_0_0_1_3_3_3_3_3_3_3_3_1_0_0
              LONG %%0_0_2_1_3_3_2_0_0_2_3_3_1_0_0_0
              LONG %%0_0_0_2_3_3_0_0_0_0_3_2_0_0_0_0
              LONG %%0_0_0_0_2_3_2_0_0_2_3_1_0_0_0_0
              LONG %%0_0_0_0_1_3_3_0_0_3_3_1_0_0_0_0
              LONG %%0_0_0_0_1_3_3_3_3_3_3_2_0_0_0_0
              LONG %%0_0_0_0_2_3_3_0_0_3_3_3_1_0_0_0
              LONG %%0_0_0_1_3_3_3_0_0_3_3_1_1_2_0_0
              LONG %%0_0_2_1_2_3_3_3_3_3_2_0_0_0_0_0
              LONG %%0_0_0_0_1_2_3_1_1_3_3_1_0_0_0_0
              LONG %%0_0_0_0_0_2_1_0_0_1_3_1_0_0_0_0
              LONG %%0_0_0_0_0_1_0_0_0_0_1_2_0_0_0_0

PlayerLadder
              LONG %%0_0_0_1_3_3_2_3_2_2_3_3_3_1_0_0     '5e
              LONG %%0_0_0_1_1_1_2_2_2_2_1_1_1_1_0_0
              LONG %%0_0_0_1_0_0_2_2_2_2_2_0_0_1_0_0
              LONG %%0_0_0_1_0_0_0_2_2_2_2_3_0_1_0_0
              LONG %%0_0_0_1_3_3_3_2_2_2_2_3_3_1_0_0
              LONG %%0_0_0_1_1_1_1_3_2_2_1_1_1_1_0_0
              LONG %%0_0_0_1_0_0_0_3_2_3_1_1_0_1_0_0
              LONG %%0_0_0_1_0_3_0_3_3_1_1_0_0_1_0_0
              LONG %%0_0_0_1_3_1_1_1_1_1_3_3_3_1_0_0
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_1_1_0_0
              LONG %%0_0_0_1_0_0_2_2_2_2_0_0_0_1_0_0
              LONG %%0_0_0_1_0_1_1_0_2_2_2_0_0_1_0_0
              LONG %%0_0_0_1_3_3_3_3_3_2_2_3_3_1_0_0
              LONG %%0_0_0_1_1_1_1_1_1_2_2_1_1_1_0_0
              LONG %%0_0_0_1_0_0_0_0_0_1_1_0_0_1_0_0
              LONG %%0_0_0_1_0_0_0_0_0_1_1_1_0_1_0_0
              

                         {
WallBricks
              LONG %%1_3_3_3_3_3_1_2_3_3_3_1_2_3_3_3 '16   'Mauerwerk
              LONG %%0_2_3_3_3_2_0_3_3_3_2_0_3_3_3_2
              LONG %%0_1_2_3_2_1_0_2_3_3_2_0_2_3_3_2
              LONG %%0_1_1_2_1_1_0_1_3_2_1_0_1_3_2_1
              LONG %%0_1_1_0_1_1_0_1_0_1_1_0_1_0_1_1
              LONG %%0_1_0_0_0_1_0_1_0_0_1_0_1_0_0_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%1_2_3_3_3_1_2_3_3_3_1_3_3_3_3_3
              LONG %%0_3_3_3_2_0_3_3_3_2_0_2_3_3_3_2
              LONG %%0_2_3_3_2_0_2_3_3_2_0_1_2_3_2_1
              LONG %%0_1_3_2_1_0_1_3_2_1_0_1_1_2_1_1
              LONG %%0_1_0_1_1_0_1_0_1_1_0_1_1_0_1_1
              LONG %%0_1_0_0_1_0_1_0_0_1_0_1_0_0_0_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

                       }

WallGreen
              LONG %%1_3_3_3_3_3_1_2_3_3_3_1_2_3_3_3 '5f   'Mauerwerk
              LONG %%0_2_3_3_3_2_0_3_3_3_2_0_3_3_3_2
              LONG %%0_1_2_3_2_1_0_2_3_3_2_0_2_3_3_2
              LONG %%0_1_1_2_1_1_0_1_3_2_1_0_1_3_2_1
              LONG %%0_1_1_0_1_1_0_1_0_1_1_0_1_0_1_1
              LONG %%0_1_0_0_0_1_0_1_0_0_1_0_1_0_0_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%1_2_3_3_3_1_2_3_3_3_1_3_3_3_3_3
              LONG %%0_3_3_3_2_0_3_3_3_2_0_2_3_3_3_2
              LONG %%0_2_3_3_2_0_2_3_3_2_0_1_2_3_2_1
              LONG %%0_1_3_2_1_0_1_3_2_1_0_1_1_2_1_1
              LONG %%0_1_0_1_1_0_1_0_1_1_0_1_1_0_1_1
              LONG %%0_1_0_0_1_0_1_0_0_1_0_1_0_0_0_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

        
WallTiny
LONG %%3333022220111033  '60 'bunte Steine
LONG %%0333022220111033
LONG %%0333022220111033
LONG %%0331110220111033

LONG %%0330110020111033
LONG %%0030113330001000
LONG %%2220110333332222
LONG %%0220110333330222

LONG %%0220010333330222
LONG %%0222220000030222
LONG %%0000222211110000
LONG %%1111022201113333

LONG %%0111022201110333
LONG %%0111022201110333
LONG %%0111022201110333
LONG %%0001000200010003

WallMetall
LONG %%1122211122221112   '61      'glaenzendes Metall
LONG %%0311133211113331
LONG %%0321123311112331
LONG %%0331113321111331
LONG %%0332112331211231
LONG %%0333111332311131
LONG %%0233211233321121
LONG %%0132311133331112
LONG %%0121321123332112
LONG %%0111231113333112
LONG %%0211132112333212
LONG %%0311123111333312
LONG %%0321113211233321
LONG %%0111111111111112
LONG %%0000000000000001
LONG %%0000000000000000


AniMonster1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0      '62
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_2_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_2_2_2_2_1_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_3_2_3_1_0_0_0_0_0
              LONG %%0_0_0_1_0_2_3_0_3_0_2_0_1_0_0_0
              LONG %%0_0_0_2_0_2_3_0_3_0_2_0_2_0_0_0
              LONG %%0_0_0_2_1_2_3_3_2_3_2_1_2_0_0_0
              LONG %%0_0_0_1_2_2_2_2_2_2_2_2_1_0_0_0
              LONG %%0_0_0_0_1_2_0_0_3_0_2_1_0_0_0_0
              LONG %%0_0_0_0_0_2_3_0_0_1_2_0_0_0_0_0
              LONG %%0_0_0_0_0_1_2_2_2_2_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_2_2_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_2_2_1_1_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_1_0_0_1_2_0_0_0_0_0
              LONG %%0_0_0_0_1_2_1_0_0_1_2_1_0_0_0_0

AniMonster2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0    ' 63
              LONG %%0_0_0_0_0_0_1_1_2_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_2_2_2_2_1_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_3_2_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_0_3_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_0_3_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_3_2_3_2_0_0_0_0_0
              LONG %%0_0_0_0_1_2_2_2_2_2_2_1_0_0_0_0
              LONG %%0_0_0_1_2_2_0_0_3_0_2_2_1_0_0_0
              LONG %%0_0_0_2_1_2_3_0_0_1_2_1_2_0_0_0
              LONG %%0_0_0_2_0_1_2_2_2_2_1_0_2_0_0_0
              LONG %%0_0_0_1_0_0_2_2_2_2_0_0_1_0_0_0
              LONG %%0_0_0_0_0_0_1_2_2_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_2_1_1_2_1_0_0_0_0_0
              LONG %%0_0_0_0_0_2_2_0_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_1_2_1_0_0_1_2_1_0_0_0_0

AniMonsterHold1
              LONG %%0_0_0_1_0_0_0_0_0_0_0_0_1_0_0_0      '64
              LONG %%0_0_0_2_0_0_0_0_0_0_0_0_2_0_0_0
              LONG %%0_0_0_2_0_0_1_1_2_1_0_0_2_0_0_0
              LONG %%0_0_0_2_0_1_2_2_2_2_1_0_2_0_0_0
              LONG %%0_0_0_2_1_1_3_0_2_0_2_1_2_0_0_0
              LONG %%0_0_0_1_2_2_3_0_3_0_2_2_1_0_0_0
              LONG %%0_0_0_0_2_2_3_3_3_3_2_2_0_0_0_0
              LONG %%0_0_0_0_1_2_3_2_2_3_2_1_0_0_0_0
              LONG %%0_0_0_0_0_2_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_1_0_3_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_0_0_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_1_2_2_2_2_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_2_2_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_2_2_1_1_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_1_0_0_1_2_0_0_0_0_0
              LONG %%0_0_0_0_1_2_1_0_0_1_2_1_0_0_0_0

AniMonsterHold2
              LONG %%0_0_0_1_0_0_0_0_0_0_0_0_1_0_0_0       '65
              LONG %%0_0_0_2_0_0_1_1_2_1_0_0_2_0_0_0
              LONG %%0_0_0_2_0_1_2_2_2_2_1_0_2_0_0_0
              LONG %%0_0_0_2_0_1_3_0_2_0_2_0_2_0_0_0
              LONG %%0_0_0_2_1_2_3_0_3_0_2_1_2_0_0_0
              LONG %%0_0_0_1_2_2_3_3_3_3_2_2_1_0_0_0
              LONG %%0_0_0_0_2_2_3_2_2_3_2_2_0_0_0_0
              LONG %%0_0_0_0_1_2_2_2_2_2_2_1_0_0_0_0
              LONG %%0_0_0_0_0_2_1_0_3_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_0_0_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_1_2_2_2_2_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_2_2_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_2_1_1_2_1_0_0_0_0_0
              LONG %%0_0_0_0_0_2_2_0_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_1_2_1_0_0_1_2_1_0_0_0_0

                                            
              
DAT 'Sprites
Sprites     


'ACHTUNG! AB HIER UEBERLAPPEN SICH TILES UND SPRITES EIN BISSCHEN.
'BITTE DARAN DENKEN, NICHT VERSEHENLICH EINE MASKE ALS TILE ZU BENUTZEN!



'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@



AniGhost1
              LONG %%0_0_0_0_0_0_1_2_2_1_0_0_0_0_0_0       '66
              LONG %%0_0_0_0_0_1_2_2_2_2_1_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_3_2_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_0_3_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_0_3_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_3_2_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_1_2_2_1_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_0_0_0_0_2_0_0_0_0_0
              LONG %%0_0_0_0_2_2_2_1_1_2_2_2_0_0_0_0
              LONG %%0_0_0_0_2_1_2_2_2_2_2_1_2_0_0_0
              LONG %%0_0_0_0_1_0_2_1_2_1_2_0_1_0_0_0
              LONG %%0_0_0_0_0_0_2_0_1_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_0_0_0_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0       '(67)
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_3_3_0_3_0_0_0
              LONG %%0_0_0_0_0_0_3_0_3_0_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_0_0_0_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

AniGhost2
              LONG %%0_0_0_0_0_0_1_2_2_1_0_0_0_0_0_0       '68
              LONG %%0_0_0_0_0_1_2_2_2_2_1_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_3_2_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_0_3_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_0_3_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_3_3_2_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_1_2_2_1_2_0_0_0_0_0
              LONG %%0_0_0_0_0_2_0_0_0_0_2_0_0_0_0_0
              LONG %%0_0_0_0_1_2_2_1_1_2_2_2_0_0_0_0
              LONG %%0_0_0_0_2_1_2_2_2_2_2_2_1_0_0_0
              LONG %%0_0_0_0_2_0_2_1_2_1_2_1_2_0_0_0
              LONG %%0_0_0_0_1_0_1_0_2_0_1_0_1_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0      '(69)
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_3_0_3_0_3_0_3_0_3_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
                                                          '6a
PlayerRight    
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_0_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_3_3_3_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_3_3_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_3_1_1_3_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_3_1_1_3_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_0_1_1_1_0_0_0_0

' Extracted from file "PlayerStoneSprites.bmp" at tile=(0, 0), size=(16, 16), palette index=(0)
PlayerRightMask                                            '(6b)
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_3_3_3_0_0_0_0

' Extracted from file "PlayerStoneSprites.bmp" at tile=(0, 1), size=(16, 16), palette index=(1)
PlayerLeft                                                  '6c
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_3_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_3_3_3_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_3_3_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_3_1_1_3_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_3_1_1_3_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_0_1_1_1_0_0_0_0

' Extracted from file "PlayerStoneSprites.bmp" at tile=(0, 1), size=(16, 16), palette index=(1)
PlayerLeftMask                                             '(6d)
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_0_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_3_3_3_0_0_0_0

' Extracted from file "PlayerStoneSprites.bmp" at tile=(0, 2), size=(16, 16), palette index=(2)
PlayerRightHold    LONG                                     '6e
              LONG %%0_0_0_0_0_3_2_0_0_0_0_3_0_0_0_0
              LONG %%0_0_0_0_0_3_2_2_2_2_0_3_0_0_0_0
              LONG %%0_0_0_0_1_1_2_2_2_2_0_1_1_0_0_0
              LONG %%0_0_0_0_1_1_0_1_1_3_2_1_1_0_0_0
              LONG %%0_0_0_0_1_1_0_1_1_0_2_1_1_0_0_0
              LONG %%0_0_0_0_1_1_0_3_3_3_2_1_1_0_0_0
              LONG %%0_0_0_0_0_1_1_3_3_3_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_3_3_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_0_1_1_1_0_0_0_0

' Extracted from file "PlayerStoneSprites.bmp" at tile=(0, 2), size=(16, 16), palette index=(2)
PlayerRightHoldMask                                           '(6f)
              LONG %%0_0_0_0_0_3_3_0_0_0_0_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_0_3_3_0_0_0
              LONG %%0_0_0_0_3_3_0_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_3_3_0_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_3_3_0_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_3_3_3_0_0_0_0
                                                             '70
PlayerLeftHold  LONG %%0_0_0_0_0_3_0_0_0_0_2_3_0_0_0_0
              LONG %%0_0_0_0_0_3_0_2_2_2_2_3_0_0_0_0
              LONG %%0_0_0_0_1_1_0_2_2_2_2_1_1_0_0_0
              LONG %%0_0_0_0_1_1_2_3_1_1_0_1_1_0_0_0
              LONG %%0_0_0_0_1_1_2_0_1_1_0_1_1_0_0_0
              LONG %%0_0_0_0_1_1_2_3_3_3_0_1_1_0_0_0
              LONG %%0_0_0_0_0_1_1_3_3_3_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_3_3_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_0_1_1_1_0_0_0_0

PlayerLeftHoldMask
              LONG %%0_0_0_0_0_3_0_0_0_0_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_0_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_0_3_3_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_0_3_3_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_0_3_3_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_3_3_3_0_0_0_0


PlayerRightKick   
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0'_0
              LONG %%0_0_2_0_2_2_0_0_0_0_0_0_0_0_0'_0
              LONG %%0_0_2_2_2_2_2_0_0_0_0_0_0_0_0'_0
              LONG %%0_0_0_3_3_3_2_0_0_0_0_0_0_0_0'_0
              LONG %%0_0_0_1_1_0_2_0_0_0_0_0_0_0_0'_0
              LONG %%0_3_0_1_1_3_2_0_0_0_0_0_0_0_0'_0
              LONG %%0_3_1_3_3_3_0_0_0_0_0_0_0_0_0'_0
              LONG %%1_1_1_3_3_1_1_0_0_0_0_0_0_0_0'_0
              LONG %%1_0_1_1_1_1_1_1_0_0_0_0_0_0_0'_0
              LONG %%1_2_0_1_1_1_1_1_0_0_0_0_0_0_0'_0
              LONG %%1_2_2_2_1_0_1_1_0_0_0_0_0_0_0'_0
              LONG %%1_2_2_2_0_0_3_1_0_0_0_0_0_0_0'_0
              LONG %%0_0_0_2_2_0_3_0_0_0_0_0_0_0_0'_0
              LONG %%0_0_0_2_2_0_0_0_0_0_0_0_0_0_0'_0
              LONG %%0_0_0_1_1_0_0_0_0_0_0_0_0_0_0'_0
              LONG %%0_0_0_1_1_1_0_0_0_0_0_0_0_0_0'_0

PlayerRightKickMask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0'_0
              LONG %%0_0_3_0_3_3_0_0_0_0_0_0_0_0_0'_0
              LONG %%0_0_3_3_3_3_3_0_0_0_0_0_0_0_0'_0
              LONG %%0_0_0_3_3_3_3_0_0_0_0_0_0_0_0'_0
              LONG %%0_0_0_3_3_3_3_0_0_0_0_0_0_0_0'_0
              LONG %%0_3_0_3_3_3_3_0_0_0_0_0_0_0_0'_0
              LONG %%0_3_3_3_3_3_0_0_0_0_0_0_0_0_0'_0
              LONG %%3_3_3_3_3_3_3_0_0_0_0_0_0_0_0'_0
              LONG %%3_0_3_3_3_3_3_3_0_0_0_0_0_0_0'_0
              LONG %%3_3_0_3_3_3_3_3_0_0_0_0_0_0_0'_0
              LONG %%3_3_3_3_3_0_3_3_0_0_0_0_0_0_0'_0
              LONG %%3_3_3_3_0_0_3_3_0_0_0_0_0_0_0'_0
              LONG %%0_0_0_3_3_0_3_0_0_0_0_0_0_0_0'_0
              LONG %%0_0_0_3_3_0_0_0_0_0_0_0_0_0_0'_0
              LONG %%0_0_0_3_3_0_0_0_0_0_0_0_0_0_0'_0
              LONG %%0_0_0_3_3_3_0_0_0_0_0_0_0_0_0'_0

PlayerLeftKick
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_2_2_0_2_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_2_2_2_2_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_3_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_0_1_1_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_3_1_1_0_3_0
              LONG %%0_0_0_0_0_0_0_0_0_0_3_3_3_1_3_0
              LONG %%0_0_0_0_0_0_0_0_0_1_1_3_3_1_1_1
              LONG %%0_0_0_0_0_0_0_0_1_1_1_1_1_1_0_1
              LONG %%0_0_0_0_0_0_0_0_1_1_1_1_1_0_2_1
              LONG %%0_0_0_0_0_0_0_0_1_1_0_1_2_2_2_1
              LONG %%0_0_0_0_0_0_0_0_1_3_0_0_2_2_2_1
              LONG %%0_0_0_0_0_0_0_0_0_3_0_2_2_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_2_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_1_1_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_1_1_1_0_0_0


PlayerLeftKickMask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_3_3_0_3_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_3_3_3_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_3_3_0_3_0
              LONG %%0_0_0_0_0_0_0_0_0_0_3_3_3_3_3_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_3_3_3_3_3
              LONG %%0_0_0_0_0_0_0_0_3_3_3_3_3_3_0_3
              LONG %%0_0_0_0_0_0_0_0_3_3_3_3_3_0_3_3
              LONG %%0_0_0_0_0_0_0_0_3_3_0_3_3_3_3_3
              LONG %%0_0_0_0_0_0_0_0_3_3_0_0_3_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_3_0_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_3_3_3_0_0_0

PlayerRightWalk1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_0_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_1_1_0_0_0_0
              LONG %%0_0_0_0_3_0_1_3_3_1_1_1_1_0_0_0
              LONG %%0_0_0_0_3_1_1_1_1_1_1_1_1_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_0_1_1_0_0_0
              LONG %%0_0_0_0_0_1_0_1_1_1_3_1_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_0_2_2_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_1_1_0_0_0_0_0


PlayerRightWalk1Mask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_0_3_3_0_0_0
              LONG %%0_0_0_0_0_3_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_0_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_0_0_0_0_0

' Extracted from file "PlayerStoneSprites.bmp" at tile=(0, 7), size=(16, 16), palette index=(7)
PlayerRightWalk2
              LONG %%0_0_0_0_0_0_2_0_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_3_3_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_1_3_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_3_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_0_0_0_0_0_0

' Extracted from file "PlayerStoneSprites.bmp" at tile=(0, 7), size=(16, 16), palette index=(7)
PlayerRightWalk2Mask
              LONG %%0_0_0_0_0_0_3_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0

' Extracted from file "PlayerStoneSprites.bmp" at tile=(0, 8), size=(16, 16), palette index=(8)
PlayerRightWalk3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_0_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_3_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_0_2_2_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_1_1_0_0_0_0_0

' Extracted from file "PlayerStoneSprites.bmp" at tile=(0, 8), size=(16, 16), palette index=(8)
PlayerRightWalk3Mask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_0_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_0_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_0_0_0_0_0
             {
PlayerRightWalk4
              LONG %%0_0_0_0_0_0_2_0_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_3_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_3_3_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_1_3_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_3_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_0_0_0_0_0_0


PlayerRightWalk4Mask
              LONG %%0_0_0_0_0_0_3_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0

             }
PlayerLeftWalk1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_3_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_1_3_3_1_0_3_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_1_1_3_0_0_0
              LONG %%0_0_0_0_1_1_0_1_1_1_1_1_1_0_0_0
              LONG %%0_0_0_0_0_1_3_1_1_1_0_1_0_0_0_0
              LONG %%0_0_0_0_0_0_3_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_1_2_2_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_1_1_0_0_0_0_0

PlayerLeftWalk1Mask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_0_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_0_3_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_3_3_0_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_0_0_0_0_0

PlayerLeftWalk2
              LONG %%0_0_0_0_0_0_0_2_2_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_3_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_3_3_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_3_1_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_3_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_0_0_0_0_0_0

PlayerLeftWalk2Mask
              LONG %%0_0_0_0_0_0_0_3_3_0_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0

PlayerLeftWalk3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_2_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_0_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_3_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_1_2_2_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_1_1_0_0_0_0_0

PlayerLeft3WalkMask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_0_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_0_0_0_0_0
         {
PlayerLeftWalk4
              LONG %%0_0_0_0_0_0_0_2_2_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_3_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_3_3_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_3_1_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_3_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_0_0_0_0_0_0

PlayerLeftWalk4Mask
              LONG %%0_0_0_0_0_0_0_3_3_0_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
          }

              
              
PlayerClimb1
{
              LONG %%0_0_0_1_3_3_2_3_2_2_3_3_3_1_0_0     '15
              LONG %%0_0_0_1_1_1_2_2_2_2_1_1_1_1_0_0
              LONG %%0_0_0_1_0_0_2_2_2_2_2_0_0_1_0_0
              LONG %%0_0_0_1_0_0_0_2_2_2_2_3_0_1_0_0
              LONG %%0_0_0_1_3_3_3_2_2_2_2_3_3_1_0_0
              LONG %%0_0_0_1_1_1_1_3_2_2_1_1_1_1_0_0
              LONG %%0_0_0_1_0_0_0_3_2_3_1_1_0_1_0_0
              LONG %%0_0_0_1_0_3_0_3_3_1_1_0_0_1_0_0
              LONG %%0_0_0_1_3_1_1_1_1_1_3_3_3_1_0_0
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_1_1_0_0
              LONG %%0_0_0_1_0_0_2_2_2_2_0_0_0_1_0_0
              LONG %%0_0_0_1_0_1_1_0_2_2_2_0_0_1_0_0
              LONG %%0_0_0_1_3_3_3_3_3_2_2_3_3_1_0_0
              LONG %%0_0_0_1_1_1_1_1_1_2_2_1_1_1_0_0
              LONG %%0_0_0_1_0_0_0_0_0_1_1_0_0_1_0_0
              LONG %%0_0_0_1_0_0_0_0_0_1_1_1_0_1_0_0
 }'             {
              LONG %%0_0_0_0_0_0_2_0_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_2_2_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_2_3_1_1_0_0_0_0
              LONG %%0_0_0_0_0_3_0_3_3_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_1_1_0_0_0_0
'}
PlayerClimb1Mask
              LONG %%0_0_0_0_0_0_3_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_3_0_0_0_0

            {
PlayerClimb2
              LONG %%0_0_0_0_0_0_0_2_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_2_3_1_3_0_0_0_0
              LONG %%0_0_0_0_0_3_0_3_3_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

PlayerClimb2Mask
              LONG %%0_0_0_0_0_0_0_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              }   {
PlayerClimb3
              LONG %%0_0_0_0_0_0_0_2_0_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_0_3_2_3_0_3_0_0_0_0
              LONG %%0_0_0_0_0_1_1_3_3_3_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

PlayerClimb3Mask
              LONG %%0_0_0_0_0_0_0_3_0_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_0_3_3_3_0_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
    }   {
PlayerClimb4
              LONG %%0_0_0_0_0_0_0_0_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_1_3_2_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_3_3_0_3_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

PlayerClimb4Mask
              LONG %%0_0_0_0_0_0_0_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              }
PlayerClimb5
              LONG %%0_0_0_0_0_0_0_2_2_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_3_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_2_2_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_3_2_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_3_3_0_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_0_0_0_0_0_0_0_0

PlayerClimb5Mask
              LONG %%0_0_0_0_0_0_0_3_3_0_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_0_0_0_0_0_0_0
             {
PlayerClimb6
              LONG %%0_0_0_0_0_0_0_0_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_1_3_2_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_3_3_0_3_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

PlayerClimb6Mask
              LONG %%0_0_0_0_0_0_0_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
      
               }  {
PlayerClimb7
              LONG %%0_0_0_0_0_0_0_2_0_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_0_3_2_3_0_3_0_0_0_0
              LONG %%0_0_0_0_0_1_1_3_3_3_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

PlayerClimb7Mask
              LONG %%0_0_0_0_0_0_0_3_0_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_0_3_3_3_0_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
        }   {
PlayerClimb8
              LONG %%0_0_0_0_0_0_0_2_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_2_3_1_3_0_0_0_0
              LONG %%0_0_0_0_0_3_0_3_3_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

PlayerClimb8Mask
              LONG %%0_0_0_0_0_0_0_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

               }
PlayerRightJump
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_0_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_0_2_2_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_3_2_2_1_0_0_0_0
              LONG %%0_0_0_0_3_0_0_3_3_2_1_1_0_0_0_0
              LONG %%0_0_0_0_3_1_0_3_3_1_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_2_1_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_0_2_2_1_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_1_0_1_1_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_0_0_1_0_0_0

PlayerRightJumpMask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_0_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_0_3_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_0_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_3_0_0_0

PlayerLeftJump
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_2_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_2_2_0_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_2_2_3_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_2_3_3_0_0_3_0_0_0
              LONG %%0_0_0_0_0_1_1_1_3_3_0_1_3_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_1_1_1_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_1_2_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_1_2_2_0_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_0_0_1_1_1_0_0_0_0_0_0

PlayerLeftJumpMask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_0_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_3_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_3_3_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_0_3_3_3_0_0_0_0_0_0

PlayerClimbRightKick
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_2_0_2_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_2_2_2_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_2_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_0_2_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_2_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_2_0_0_3_0_0_0_0
              LONG %%1_0_0_0_1_1_1_3_3_0_1_1_0_0_0_0
              LONG %%1_0_0_0_1_1_1_1_1_1_1_1_0_0_0_0
              LONG %%1_1_2_0_0_0_1_1_1_1_1_0_0_0_0_0
              LONG %%1_1_2_2_2_2_2_2_2_2_0_0_0_0_0_0
              LONG %%1_1_2_2_2_2_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_0_0_0_0_0_0

PlayerClimbRightKickMask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_0_0_3_0_0_0_0
              LONG %%3_0_0_0_3_3_3_3_3_0_3_3_0_0_0_0
              LONG %%3_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%3_3_3_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%3_3_3_3_3_3_3_3_3_3_0_0_0_0_0_0
              LONG %%3_3_3_3_3_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0

PlayerClimbLeftKick
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_2_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_2_2_2_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_2_2_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_2_2_0_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_2_2_3_1_0_0_0_0
              LONG %%0_0_0_0_0_3_0_0_2_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_1_1_0_3_3_1_1_1_0_0_1
              LONG %%0_0_0_0_0_1_1_1_1_1_1_1_1_0_0_1
              LONG %%0_0_0_0_0_0_1_1_1_1_1_0_0_2_1_1
              LONG %%0_0_0_0_0_0_0_2_2_2_2_2_2_2_1_1
              LONG %%0_0_0_0_0_0_0_2_2_2_2_2_2_2_1_1
              LONG %%0_0_0_0_0_0_0_2_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_0_0_0_0_0_0

PlayerClimbLeftKickMask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_0_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_0_0_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_3_3_0_3_3_3_3_3_0_0_3
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_3_0_0_3
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_3_3_3
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_3_3_3_3
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_3_3_3_3
              LONG %%0_0_0_0_0_0_0_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0

PlayerRightFall
              LONG %%0_0_0_0_0_0_2_0_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_3_2_0_3_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_0_2_0_3_0_0_0
              LONG %%0_0_0_0_3_0_0_1_1_3_2_0_1_0_0_0
              LONG %%0_0_0_0_3_0_0_1_1_3_0_1_1_0_0_0
              LONG %%0_0_0_0_1_1_1_3_3_3_1_1_1_0_0_0
              LONG %%0_0_0_0_1_1_1_3_3_1_1_1_1_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_2_2_2_2_2_0_0_0_0_0_0
              LONG %%0_0_0_0_1_2_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_1_1_2_0_0_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_1_1_0_0_0_0

PlayerRightFallMask
              LONG %%0_0_0_0_0_0_3_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_3_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_0_3_0_0_0
              LONG %%0_0_0_0_3_0_0_3_3_3_3_0_3_0_0_0
              LONG %%0_0_0_0_3_0_0_3_3_3_0_3_3_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_3_3_0_0_0_0

PlayerLeftFall
              LONG %%0_0_0_0_0_0_0_2_2_0_2_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_3_0_2_3_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_3_0_2_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_1_0_2_3_1_1_0_0_3_0_0_0
              LONG %%0_0_0_0_1_1_0_3_1_1_0_0_3_0_0_0
              LONG %%0_0_0_0_1_1_1_3_3_3_1_1_1_0_0_0
              LONG %%0_0_0_0_1_1_1_1_3_3_1_1_1_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_2_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_2_2_2_1_0_0_0
              LONG %%0_0_0_0_0_0_2_2_0_0_2_1_1_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_0_0_0_0_0_0_0

PlayerLeftFallMask
              LONG %%0_0_0_0_0_0_0_3_3_0_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_3_0_0_3_0_0_0
              LONG %%0_0_0_0_3_3_0_3_3_3_0_0_3_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_3_3_3_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_0_0_0_0_0

MonsterFall
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_2_2_2_3_2_0_0_0_0_0_0
              LONG %%0_0_0_0_2_2_3_3_0_2_0_2_0_0_0_0
              LONG %%0_0_0_0_2_3_0_3_0_2_0_2_0_0_0_0
              LONG %%0_0_0_0_2_3_0_3_3_2_2_2_0_0_0_0
              LONG %%0_0_0_0_2_3_3_2_2_2_2_2_0_0_0_0
              LONG %%0_0_0_0_2_2_2_2_3_2_2_0_0_0_0_0
              LONG %%0_0_0_0_2_2_2_0_0_2_0_0_0_0_0_0
              LONG %%0_0_0_2_2_2_0_3_0_2_0_0_0_0_0_0
              LONG %%0_0_0_2_2_2_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_2_0_0_2_2_2_2_2_0_0_0_0_0
              LONG %%0_0_0_2_0_0_0_2_2_0_2_2_0_0_0_0
              LONG %%0_0_0_0_0_0_2_2_0_0_2_2_2_0_0_0
              LONG %%0_0_0_0_0_0_2_2_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_2_2_2_0_0_0_0_0_0_0_0

MonsterFallMask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_0_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_0_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_3_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_3_0_0_0_3_3_0_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_3_3_3_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_0_0_0_0_0_0_0

RockFall
              LONG %%0_0_1_1_1_1_1_1_1_1_1_1_1_0_0_0
              LONG %%0_1_1_1_1_1_1_1_1_1_1_1_1_1_0_0
              LONG %%1_1_1_1_1_1_1_1_1_1_1_1_1_3_3_0
              LONG %%0_1_1_1_1_1_1_1_1_1_1_1_3_3_3_1
              LONG %%0_0_1_1_1_1_1_1_1_1_1_3_3_3_1_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_3_1_1_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_1_1_1_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_1_1_1_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_1_1_1_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_1_1_1_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_1_1_1_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_1_1_1_1
              LONG %%0_0_0_0_1_1_1_1_1_1_1_1_1_1_1_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_1_1_1_1_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_1_1_1_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_1_0_0

RockFallMask
                        long      %%0033333333333000 ' tile 2     rock
                        long      %%0333333333333300
                        long      %%3333333333333330
                        long      %%3333333333333333
                        long      %%3333333333333333
                        long      %%3333333333333333
                        long      %%3333333333333333                
                        long      %%3333333333333333
                        long      %%3333333333333333
                        long      %%3333333333333333
                        long      %%3333333333333333
                        long      %%3333333333333333
                        long      %%3333333333333333
                        long      %%3333333333333333
                        long      %%3333333333333330
                        long      %%0333333333333300


Rock
              LONG %%0_0_1_1_1_1_1_1_1_1_1_1_1_0_0_0
              LONG %%0_1_2_2_2_2_2_2_2_2_2_2_2_3_0_0
              LONG %%1_1_1_2_2_2_2_2_2_2_2_2_3_3_3_0
              LONG %%0_1_1_1_2_2_2_2_2_2_2_3_3_3_2_1
              LONG %%0_0_1_1_1_1_1_1_1_1_1_3_3_2_2_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_2_2_2_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_2_2_2_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_2_2_2_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_2_2_2_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_2_2_2_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_2_2_2_1
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_2_2_2_1
              LONG %%0_0_0_0_1_1_1_1_1_1_1_1_1_2_2_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_1_1_1_2_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_1_1_1_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_1_0_0

RockMask           ' braucht zu viel speicher
      {        long      %%0033333333333000 
              long      %%0333333333333300
              long      %%3333333333333330
              long      %%3333333333333333
              long      %%3333333333333333
              long      %%3333333333333333
              long      %%3333333333333333                
              long      %%3333333333333333
              long      %%3333333333333333
              long      %%3333333333333333
              long      %%3333333333333333
              long      %%3333333333333333
              long      %%3333333333333333
              long      %%3333333333333333
              long      %%3333333333333330
'              long      %%0333333333333300
                                              }

DAT 'Passworte                                                                                  

' Jedes Passwort ist 2 byte gross 
passwords word
        word 0000 '35143' Level  1 Gruen
        word 34456      ' Level  2
        word 20804      ' Level  3
        word 10116      ' Level  4
        word 22280      ' Level  5
        word 4665       ' Level  6
        word 29828      ' Level  7
        word 25432      ' Level  8
        word 10085      ' Level  9
        word 12392      ' Level 10
        word 17552      ' Level 11
        word 22150      ' Level 12
        word 30340      ' Level 13
        word 39031      ' Level 14
        word 38802      ' Level 15
        word 38409      ' Level 16
        word 9029       ' Level 17
        word 850        ' Level 18
        word 25730      ' Level 19
        word 13648      ' Level 20
        word 28753      ' Level 21
        word 8193       ' Level 22
        word 5430       ' Level 23
        word 32841      ' Level 24
        word 9505       ' Level 25
        word 10264      ' Level 26
        word 6486       ' Level 27
        word 12584      ' Level 28
        word 18245      ' Level 29
        word 1284       ' Level 30 Zwei Spieler
        word 390        ' Level 31
        word 8980       ' Level 32
        word 6418       ' Level 33
        word 4407       ' Level 34
        word 24868      ' Level 35
        word 2087       ' Level 36
        word 6553       ' Level 37
        word 29473      ' Level 38
        word 18743      ' Level 39
        word 14705      ' Level 40 Wueste
        word 33140      ' Level 41
        word 13667      ' Level 42
        word 34370      ' Level 43
        word 516        ' Level 44
        word 14408      ' Level 45
        word 24658      ' Level 46
        word 26401      ' Level 47
        word 25688      ' Level 48
        word 21808      ' Level 49
        word 2326       ' Level 50
        word 14729      ' Level 51
        word 37761      ' Level 52
        word 2359       ' Level 53
        word 21510      ' Level 54
        word 9510       ' Level 55
        word 37527      ' Level 56
        word 21401      ' Level 57
        word 1297       ' Level 58
        word 1378       ' Level 59
        word 149        ' Level 60 Zwei Spieler
        word 18512      ' Level 61
        word 18773      ' Level 62
        word 4450       ' Level 63
        word 14592      ' Level 64
        word 9779       ' Level 65
        word 38150      ' Level 66
        word 517        ' Level 67
        word 30617      ' Level 68
        word 37142      ' Level 69
        word 33648      ' Level 70 Metall
        word 18192      ' Level 71
        word 25672      ' Level 72
        word 18453      ' Level 73
        word 1556       ' Level 74
        word 30066      ' Level 75
        word 5174       ' Level 76
        word 2310       ' Level 77
        word 567        ' Level 78
        word 1396       ' Level 79
        word 38807      ' Level 80
        word 22144      ' Level 81
        word 12883      ' Level 82
        word 22388      ' Level 83
        word 1168       ' Level 84
        word 33399      ' Level 85
        word 4889       ' Level 86
        word 21271      ' Level 87
        word 8214       ' Level 88
        word 38214      ' Level 89
        word 17296      ' Level 90 Zwei Spieler
        word 17811      ' Level 91
        word 22841      ' Level 92
        word 16499      ' Level 93
        word 26676      ' Level 94
        word 34384      ' Level 95
        word 6246       ' Level 96
        word 20772      ' Level 97
        word 1672       ' Level 98
        word 4693       ' Level 99
        word 14708      ' Level100

       
DAT 'Level-Data         
' Groesse: 11x8 tiles (33 byte / level)
' 1 level == 33 bytes == 88 triplets (little endian)

levels        byte $49,$92,$24,$49,$92,$24,$49,$92,$24,$49,$92,$24,$49,$92,$24,$49,$12,$00,$00,$00,$24,$38,$0A,$01,$4D,$92,$24,$41,$92,$24,$49,$92   '1
              byte $24,$49,$92,$24,$49,$92,$24,$49,$92,$24,$00,$00,$20,$49,$96,$40,$40,$92,$2C,$81,$D0,$24,$59,$92,$20,$49,$32,$E0,$41,$92,$24,$49
              byte $92,$24,$49,$92,$24,$49,$02,$00,$00,$8A,$00,$C9,$80,$64,$81,$80,$01,$C0,$B2,$00,$03,$80,$65,$00,$30,$00,$CB,$00,$01,$C5,$97,$24
              byte $01,$82,$24,$49,$92,$24,$49,$92,$24,$49,$92,$24,$00,$00,$21,$49,$0A,$00,$44,$92,$04,$59,$92,$24,$49,$B2,$24,$49,$72,$60,$49,$92
              byte $24,$49,$92,$24,$49,$92,$24,$49,$02,$00,$D8,$B6,$05,$55,$01,$00,$0B,$48,$02,$00,$96,$24,$09,$00,$2C,$00,$00,$A0,$5D,$50,$00,$07   '5
              byte $B9,$24,$09,$92,$24,$49,$92,$24,$49,$92,$24,$49,$92,$24,$00,$80,$23,$49,$90,$24,$49,$92,$00,$08,$D4,$24,$29,$90,$49,$49,$92,$02
              byte $AB,$92,$24,$49,$92,$24,$49,$92,$24,$49,$92,$24,$49,$92,$04,$C0,$05,$20,$49,$91,$65,$5B,$92,$20,$03,$80,$24,$49,$06,$00,$49,$92
              byte $0C,$42,$93,$24,$09,$92,$24,$49,$92,$24,$49,$92,$24,$49,$92,$24,$10,$70,$20,$49,$16,$24,$59,$92,$0C,$68,$B3,$24,$19,$90,$64,$49
              byte $92,$00,$C2,$92,$24,$49,$92,$24,$49,$92,$24,$49,$92,$24,$49,$92,$24,$80,$04,$20,$49,$00,$12,$40,$92,$14,$49,$B2,$24,$09,$00,$60
              byte $49,$12,$E0,$E8,$92,$24,$49,$92,$24,$49,$92,$24,$49,$92,$24,$49,$92,$24,$04,$04,$20,$49,$88,$6D,$5B,$92,$10,$49,$B2,$24,$21,$00  '10
              byte $60,$49,$92,$04,$C5,$92,$24,$09,$F6,$25,$49,$92,$24,$49,$92,$24,$49,$92,$24,$01,$70,$24,$49,$82,$24,$4B,$92,$04,$00,$96,$24,$09
              byte $64,$2C,$49,$92,$90,$58,$92,$24,$49,$92,$24,$00,$04,$00,$00,$96,$28,$48,$92,$0D,$10,$02,$00,$1B,$4E,$00,$00,$96,$24,$08,$00,$0C
              byte $00,$02,$00,$58,$01,$00,$00,$B4,$24,$48,$82,$24,$01,$00,$00,$C0,$02,$00,$00,$80,$05,$00,$00,$40,$0B,$00,$00,$00,$17,$00,$00,$0A
              byte $2D,$00,$00,$04,$5C,$0A,$1C,$02,$B0,$04,$49,$C2,$61,$49,$92,$24,$49,$92,$24,$49,$92,$24,$49,$92,$24,$01,$00,$00,$40,$02,$00,$00
              byte $84,$64,$69,$DB,$AA,$C9,$4E,$92,$24,$93,$24,$49,$92,$24,$00,$00,$01,$00,$B6,$6D,$DC,$B6,$0D,$10,$00,$01,$1B,$92,$20,$09,$36,$04
              byte $44,$00,$6C,$08,$02,$02,$58,$12,$04,$01,$B0,$02,$08,$82,$63,$00,$00,$00,$00,$00,$24,$49,$06,$00,$08,$C5,$0C,$00,$10,$96,$19,$90
              byte $20,$14,$33,$A0,$42,$58,$66,$00,$09,$52,$E4,$F0,$24,$49,$92,$24,$24,$03,$00,$15,$48,$06,$00,$A6,$91,$0C,$90,$80,$4B,$12,$00,$40
              byte $06,$00,$10,$00,$0C,$00,$12,$60,$09,$0A,$00,$C0,$00,$04,$00,$F0,$01,$49,$92,$24,$49,$02,$00,$00,$B0,$04,$02,$00,$61,$C9,$36,$72
              byte $DB,$12,$00,$00,$80,$25,$80,$01,$03,$4B,$00,$02,$C4,$97,$24,$49,$92,$24,$C0,$01,$24,$49,$20,$92,$4B,$92,$A0,$24,$97,$24,$01,$41
              byte $0E,$93,$08,$08,$18,$26,$02,$0C,$49,$8C,$C0,$42,$92,$58,$0C,$92,$24,$31,$00,$69,$92,$14,$4D,$A2,$24,$C9,$84,$8C,$49,$92,$70,$18  '20
              byte $93,$24,$08,$36,$02,$00,$86,$6D,$CF,$B6,$2D,$6A,$92,$69,$D8,$A8,$24,$23,$09,$60,$00,$00,$40,$40,$0D,$61,$DB,$06,$02,$05,$00,$60
              byte $80,$22,$40,$D0,$00,$96,$83,$A0,$21,$92,$D6,$2A,$43,$94,$44,$51,$86,$28,$89,$A2,$EC,$00,$04,$00,$00,$10,$65,$4A,$92,$01,$12,$78
              byte $00,$43,$49,$90,$00,$06,$06,$03,$00,$AC,$00,$00,$00,$58,$90,$24,$49,$12,$82,$24,$49,$92,$04,$00,$08,$00,$88,$24,$DB,$B6,$11,$61
              byte $00,$00,$23,$92,$40,$00,$46,$00,$20,$09,$8C,$00,$40,$13,$18,$49,$82,$24,$30,$00,$04,$0E,$60,$00,$00,$00,$49,$00,$40,$10,$92,$8C
              byte $E4,$C0,$25,$19,$80,$11,$4B,$32,$41,$00,$06,$6C,$04,$39,$24,$DD,$40,$60,$44,$92,$1D,$0B,$BB,$90,$49,$92,$24,$49,$0A,$01,$40,$92
              byte $24,$5A,$86,$24,$21,$09,$0C,$49,$02,$74,$19,$92,$84,$14,$30,$24,$49,$D1,$65,$10,$80,$A1,$C0,$CE,$B6,$00,$20,$00,$08,$97,$4C,$48
              byte $10,$0E,$99,$30,$23,$1C,$32,$21,$41,$32,$64,$82,$03,$6C,$48,$42,$00,$D9,$00,$03,$49,$B2,$01,$00,$0E,$60,$01,$84,$00,$40,$02,$08
              byte $01,$D0,$64,$14,$92,$25,$C9,$44,$01,$4B,$92,$09,$02,$96,$24,$23,$08,$2C,$49,$06,$04,$40,$92,$EC,$08,$80,$24,$C1,$06,$01,$48,$82
              byte $09,$02,$9E,$04,$13,$9A,$2C,$09,$26,$4C,$5A,$92,$2C,$28,$B9,$24,$19,$B0,$25,$49,$12,$20,$0B,$C9,$24,$E8,$16,$92,$43,$51,$14,$C5
              byte $06,$41,$10,$84,$0D,$45,$51,$14,$1B,$04,$41,$10,$36,$14,$45,$51,$6C,$10,$04,$41,$D8,$5E,$14,$45,$B1,$91,$24,$49,$72,$49,$92,$24  '30
              byte $49,$92,$24,$49,$92,$24,$49,$92,$24,$49,$00,$00,$40,$92,$00,$00,$80,$24,$F9,$21,$A0,$49,$92,$24,$48,$92,$24,$49,$90,$24,$49,$92
              byte $24,$49,$92,$24,$49,$92,$04,$00,$00,$00,$09,$00,$A0,$00,$12,$60,$4B,$02,$24,$00,$06,$00,$4D,$06,$5C,$07,$92,$24,$49,$92,$24,$01
              byte $00,$00,$40,$02,$00,$00,$80,$E4,$10,$00,$00,$49,$12,$00,$00,$92,$03,$00,$80,$26,$49,$82,$01,$49,$92,$24,$49,$92,$24,$49,$92,$24
              byte $49,$92,$24,$49,$92,$24,$49,$92,$24,$01,$00,$20,$49,$02,$00,$40,$92,$C4,$72,$B1,$24,$09,$4E,$7D,$49,$92,$24,$49,$92,$24,$49,$92
              byte $24,$DB,$B6,$6D,$DB,$06,$00,$02,$80,$2D,$C9,$C8,$25,$DB,$0F,$00,$00,$96,$04,$00,$00,$0C,$00,$00,$00,$58,$01,$00,$00,$B0,$24,$C0
              byte $30,$24,$3F,$00,$00,$40,$49,$86,$60,$48,$02,$00,$06,$00,$28,$00,$10,$00,$4A,$32,$04,$43,$12,$00,$40,$00,$40,$01,$60,$00,$50,$92
              byte $01,$11,$92,$49,$92,$24,$49,$92,$44,$49,$92,$24,$89,$92,$24,$49,$12,$25,$49,$92,$24,$4D,$92,$24,$49,$84,$03,$49,$52,$34,$C9,$92
              byte $24,$29,$8E,$25,$24,$C3,$32,$24,$49,$86,$62,$48,$92,$0C,$CB,$90,$24,$19,$8A,$21,$49,$32,$2C,$43,$32,$49,$28,$26,$C9,$F0,$B1,$DC
              byte $B1,$91,$A3,$38,$72,$00,$50,$14,$00,$80,$25,$48,$06,$EC,$80,$20,$E0,$4B,$92,$20,$49,$06,$00,$E0,$B6,$AD,$00,$00,$00,$58,$06,$00
              byte $88,$32,$00,$80,$30,$6E,$50,$01,$00,$15,$A6,$01,$00,$A6,$4D,$00,$00,$40,$1B,$01,$00,$00,$B7,$00,$00,$80,$6C,$00,$00,$00,$18,$80  '40
              byte $1F,$00,$B0,$6C,$DB,$B6,$2D,$00,$84,$90,$24,$01,$08,$49,$92,$60,$13,$00,$34,$C1,$44,$25,$49,$82,$09,$C2,$B6,$0D,$23,$08,$96,$18
              byte $06,$04,$55,$B1,$0F,$08,$D6,$61,$49,$F2,$6D,$DB,$42,$26,$00,$D4,$85,$64,$00,$95,$0B,$93,$40,$E5,$12,$A6,$40,$B9,$24,$4C,$02,$6E
              byte $61,$C8,$14,$5D,$C2,$24,$19,$96,$90,$9B,$2E,$00,$00,$06,$4D,$00,$00,$0C,$9A,$00,$00,$18,$A2,$60,$00,$30,$40,$10,$06,$60,$48,$10
              byte $24,$C0,$00,$21,$49,$E0,$21,$41,$92,$00,$14,$00,$00,$00,$78,$24,$49,$92,$81,$15,$00,$00,$23,$58,$01,$00,$06,$82,$15,$00,$8C,$20
              byte $58,$01,$18,$08,$82,$05,$30,$82,$20,$08,$60,$91,$06,$00,$40,$D2,$4E,$06,$80,$24,$59,$25,$00,$49,$32,$B1,$03,$92,$64,$95,$06,$24
              byte $C9,$54,$0E,$48,$92,$55,$1B,$9A,$24,$DB,$76,$2C,$09,$C0,$6D,$49,$92,$82,$E8,$92,$24,$02,$AB,$25,$49,$08,$A6,$4B,$92,$00,$84,$96
              byte $24,$D9,$86,$2E,$49,$62,$2C,$5D,$92,$04,$00,$3A,$3C,$10,$90,$01,$05,$46,$42,$C3,$22,$0C,$05,$87,$41,$18,$48,$0C,$20,$37,$00,$1F
              byte $20,$8C,$24,$39,$2C,$18,$00,$00,$18,$B8,$04,$49,$10,$92,$2B,$00,$00,$02,$26,$00,$C2,$A6,$AE,$00,$04,$8C,$9A,$00,$08,$18,$3A,$92
              byte $50,$31,$70,$04,$D8,$80,$C4,$00,$AC,$11,$89,$1D,$58,$23,$12,$01,$04,$00,$40,$32,$68,$DB,$B6,$64,$0C,$00,$60,$C9,$00,$1B,$C0,$92
              byte $C1,$80,$81,$25,$03,$00,$18,$4B,$66,$00,$07,$96,$0C,$DC,$B6,$2D,$00,$20,$00,$00,$00,$40,$00,$00,$00,$56,$92,$24,$03,$70,$00,$00  '50
              byte $B6,$44,$03,$00,$6C,$89,$90,$D8,$DA,$B4,$8E,$E4,$98,$74,$24,$93,$24,$80,$00,$00,$02,$B6,$6D,$D8,$B6,$0D,$10,$00,$01,$1B,$58,$82
              byte $25,$36,$30,$04,$43,$6C,$20,$09,$92,$D8,$00,$0C,$03,$B0,$01,$D8,$07,$60,$00,$84,$43,$00,$B6,$6D,$DC,$B6,$0D,$10,$08,$01,$1B,$68
              byte $82,$26,$36,$84,$04,$19,$6C,$20,$83,$90,$D8,$00,$0C,$03,$B0,$01,$18,$06,$60,$64,$00,$01,$00,$C8,$00,$04,$80,$92,$C1,$1A,$05,$4B
              byte $82,$31,$8D,$40,$12,$03,$C5,$A2,$24,$76,$92,$02,$01,$20,$09,$39,$76,$4C,$49,$72,$00,$70,$41,$00,$00,$60,$91,$00,$40,$58,$50,$01
              byte $80,$16,$A0,$42,$31,$02,$40,$85,$60,$04,$80,$B6,$C2,$80,$01,$6D,$85,$24,$03,$DA,$16,$49,$B6,$6D,$48,$92,$4C,$D2,$90,$24,$99,$AC '''
              byte $21,$49,$B2,$AA,$43,$92,$64,$6D,$87,$24,$C9,$DA,$0E,$49,$92,$64,$19,$92,$24,$E9,$B2,$27,$80,$00,$00,$00,$70,$41,$00,$00,$8C,$C4
              byte $00,$00,$18,$40,$92,$DB,$40,$92,$05,$16,$15,$00,$88,$04,$52,$01,$20,$C0,$AA,$20,$1C,$96,$B1,$49,$92,$44,$4B,$92,$24,$49,$97,$24
              byte $49,$12,$2D,$49,$00,$00,$5D,$92,$C0,$B6,$BB,$24,$01,$00,$68,$49,$82,$24,$C9,$92,$04,$C0,$81,$25,$04,$00,$00,$CA,$04,$00,$05,$A4
              byte $11,$38,$0A,$28,$03,$C9,$2A,$90,$86,$24,$69,$22,$6E,$00,$50,$40,$D8,$48,$92,$24,$B9,$01,$00,$00,$60,$80,$20,$00,$00,$00,$62,$DB
              byte $B6,$01,$D0,$04,$00,$03,$C0,$0E,$30,$86,$00,$9A,$00,$0C,$0E,$88,$01,$18,$00,$00,$00,$BE,$64,$58,$B6,$25,$00,$00,$01,$10,$96,$24  '60
              byte $09,$46,$0C,$00,$B5,$03,$18,$00,$29,$19,$30,$60,$11,$34,$60,$C0,$20,$68,$00,$A0,$81,$D0,$50,$54,$03,$A0,$61,$DB,$B6,$0D,$DB,$06
              byte $41,$18,$06,$2D,$41,$42,$32,$19,$80,$62,$18,$3A,$04,$21,$49,$66,$C8,$83,$61,$C8,$80,$03,$18,$D0,$91,$01,$00,$20,$01,$0E,$E0,$40
              byte $B2,$6D,$DB,$B6,$04,$00,$00,$60,$09,$00,$00,$C0,$12,$40,$00,$80,$25,$80,$6D,$03,$4B,$20,$00,$00,$96,$80,$4B,$02,$20,$1B,$9E,$90
              byte $24,$07,$0D,$09,$49,$0E,$9A,$90,$90,$1C,$A2,$60,$49,$32,$40,$10,$00,$60,$48,$10,$04,$C8,$00,$41,$18,$D0,$3D,$41,$10,$20,$08,$00
              byte $08,$02,$10,$60,$09,$92,$21,$C0,$00,$00,$4B,$80,$01,$28,$76,$00,$03,$10,$2C,$01,$86,$00,$18,$02,$6C,$00,$36,$04,$10,$3E,$40,$00
              byte $00,$49,$D2,$00,$00,$6D,$DB,$15,$00,$DA,$B6,$0B,$08,$92,$24,$D7,$05,$24,$49,$2E,$09,$48,$92,$1C,$19,$90,$24,$39,$32,$00,$80,$7F
              byte $80,$20,$00,$00,$00,$82,$DB,$B6,$01,$10,$06,$14,$03,$42,$4E,$08,$86,$06,$98,$00,$0C,$0B,$00,$02,$18,$20,$E0,$00,$3E,$8E,$24,$39
              byte $72,$04,$00,$08,$00,$88,$24,$DB,$B6,$11,$51,$00,$00,$23,$92,$00,$00,$46,$00,$20,$09,$8C,$24,$91,$06,$18,$4B,$82,$24,$30,$0E,$C4
              byte $0F,$60,$00,$80,$6D,$DB,$00,$00,$00,$8E,$01,$00,$84,$0F,$40,$B0,$04,$49,$B2,$06,$40,$00,$00,$93,$04,$00,$00,$40,$12,$00,$D0,$24
              byte $49,$00,$20,$00,$00,$01,$29,$96,$24,$04,$C2,$2D,$99,$04,$90,$1B,$C9,$52,$3F,$36,$92,$24,$17,$64,$DB,$B6,$2D,$48,$22,$40,$49,$90  '70
              byte $92,$00,$D9,$20,$87,$00,$00,$00,$C8,$00,$00,$42,$26,$01,$00,$24,$01,$80,$03,$00,$00,$94,$1C,$59,$00,$08,$00,$20,$00,$94,$85,$61
              byte $84,$12,$00,$00,$B0,$28,$34,$00,$00,$16,$68,$00,$00,$2C,$D0,$00,$00,$18,$A1,$01,$10,$90,$41,$03,$28,$20,$80,$76,$80,$43,$D0,$92
              byte $45,$86,$B0,$04,$06,$0C,$02,$90,$08,$10,$89,$0B,$20,$D0,$2D,$E4,$48,$82,$44,$85,$91,$20,$40,$92,$03,$06,$22,$11,$16,$04,$40,$42
              byte $92,$04,$8C,$04,$00,$30,$00,$00,$10,$32,$12,$00,$34,$49,$24,$00,$E0,$B6,$70,$03,$0A,$61,$05,$B6,$89,$02,$02,$6C,$27,$05,$14,$D8
              byte $08,$12,$08,$B0,$6D,$DB,$16,$60,$64,$29,$A4,$10,$3A,$52,$48,$94,$09,$00,$17,$48,$23,$17,$0D,$60,$86,$05,$5A,$2B,$8C,$28,$B9,$96
              byte $23,$91,$68,$49,$50,$9E,$E4,$C2,$6C,$19,$00,$00,$03,$32,$92,$24,$21,$64,$02,$0E,$20,$C9,$88,$24,$00,$90,$09,$6D,$80,$35,$23,$A2
              byte $00,$58,$06,$90,$04,$80,$10,$23,$49,$92,$21,$27,$DB,$5A,$42,$AE,$24,$BB,$84,$9C,$6C,$6B,$C9,$B9,$92,$EC,$12,$72,$B2,$AD,$25,$E4
              byte $4A,$B2,$4B,$C8,$C9,$B6,$96,$90,$2B,$C9,$2E,$2C,$09,$01,$00,$48,$72,$DB,$B6,$11,$29,$00,$00,$23,$48,$10,$00,$46,$00,$17,$00,$8C
              byte $24,$46,$00,$18,$00,$00,$00,$B0,$24,$49,$92,$60,$04,$00,$11,$00,$48,$09,$E4,$00,$70,$14,$C1,$E1,$E0,$C0,$22,$49,$C2,$01,$02,$D1
              byte $80,$2B,$04,$2A,$00,$27,$08,$0C,$00,$6E,$DB,$92,$24,$00,$28,$40,$00,$80,$6D,$D9,$B6,$0D,$C0,$02,$00,$4B,$80,$05,$02,$86,$02,$0B  '80
              byte $12,$2C,$01,$16,$24,$18,$00,$6C,$48,$30,$00,$00,$00,$7C,$00,$00,$90,$51,$96,$88,$23,$D3,$2C,$68,$26,$46,$D9,$20,$8C,$8C,$32,$80
              byte $24,$40,$12,$5B,$48,$92,$D2,$70,$41,$00,$C8,$20,$41,$90,$75,$DF,$B6,$6D,$DB,$48,$92,$24,$C9,$91,$AD,$A8,$8A,$23,$CB,$B2,$2C,$47 '82
              byte $4A,$A2,$24,$8E,$2C,$CB,$B2,$1C,$29,$95,$AD,$38,$92,$24,$49,$72,$01,$B6,$6D,$DB,$02,$0C,$00,$80,$05,$18,$DA,$16,$0B,$30,$48,$12
              byte $16,$72,$24,$49,$2E,$E4,$48,$92,$5C,$A4,$01,$00,$B0,$B4,$C3,$01,$60,$00,$00,$00,$17,$39,$66,$5B,$54,$6E,$D8,$B4,$51,$E3,$00,$92
              byte $C0,$C8,$31,$49,$C2,$11,$03,$00,$00,$03,$06,$C0,$00,$06,$0C,$00,$00,$0C,$24,$45,$92,$0C,$09,$08,$00,$10,$72,$93,$92,$2C,$E1,$44
              byte $09,$00,$C0,$09,$14,$9A,$82,$23,$48,$30,$09,$07,$1C,$40,$48,$AE,$1D,$C9,$90,$24,$03,$00,$D0,$49,$06,$00,$AA,$91,$0C,$80,$6A,$23
              byte $99,$A2,$DA,$42,$32,$83,$B6,$84,$64,$06,$B2,$21,$C5,$0C,$60,$C2,$92,$1C,$40,$92,$3B,$00,$01,$E8,$46,$48,$95,$C0,$0D,$A9,$AA,$06
              byte $1B,$AA,$B6,$2A,$36,$44,$6D,$15,$6C,$53,$DB,$56,$DB,$D6,$B6,$6D,$B7,$6D,$DB,$B6,$6D,$D9,$36,$52,$43,$32,$D8,$A0,$86,$64,$68,$41
              byte $0D,$C9,$40,$8E,$1A,$92,$01,$00,$35,$24,$03,$00,$6A,$48,$06,$00,$E4,$90,$0C,$00,$C8,$3D,$43,$01,$88,$14,$97,$04,$28,$C7,$0E,$00
              byte $20,$6E,$19,$80,$A3,$EC,$38,$94,$9C,$A8,$71,$18,$B0,$52,$E3,$30,$92,$A2,$46,$00,$00,$CA,$8E,$A0,$24,$49,$E2,$C7,$AA,$6D,$CB,$8D
              byte $AD,$DA,$96,$1B,$DB,$AA,$2D,$37,$B6,$AD,$5A,$6E,$6C,$DB,$AA,$DC,$D8,$B6,$AD,$B8,$B1,$6D,$DB,$72,$24,$49,$92,$29,$92,$24,$49,$92
              byte $01,$80,$00,$04,$C3,$52,$02,$08,$86,$65,$12,$10,$6C,$24,$4B,$E1,$D8,$00,$90,$5C,$80,$6D,$DB,$B6,$00,$AC,$00,$11,$00,$42,$A9,$C4
              byte $00,$64,$14,$C1,$E1,$CF,$C0,$82,$24,$93,$01,$02,$D9,$30,$2B,$04,$36,$64,$26,$08,$0C,$C9,$6C,$DB,$42,$92,$01,$00,$54,$7B,$02,$00
              byte $D5,$F6,$A4,$40,$B5,$6D,$49,$50,$6D,$DB,$92,$40,$5B,$92,$24,$01,$D9,$90,$4C,$02,$24,$21,$99,$04,$20,$49,$32,$BB,$00,$41,$00,$50
              byte $6E,$DB,$B6,$21,$48,$92,$30,$03,$05,$01,$61,$96,$88,$02,$C2,$6C,$20,$05,$00,$D8,$08,$12,$0F,$B2,$6D,$DB,$16,$64,$18,$49,$DA,$1A
              byte $30,$D9,$24,$39,$60,$24,$63,$6B,$C0,$64,$93,$E4,$80,$91,$A4,$AD,$01,$93,$1D,$92,$03,$46,$92,$B6,$86,$4F,$36,$49,$EE,$00,$70,$49
              byte $00,$00,$7C,$51,$01,$00,$58,$50,$01,$00,$16,$A0,$42,$81,$05,$40,$8B,$00,$01,$00,$B5,$02,$00,$00,$AD,$84,$24,$00,$DA,$16,$27,$49
              byte $92,$E4,$47,$92,$24,$C9,$8D,$A4,$6D,$92,$1B,$49,$00,$24,$37,$92,$B6,$49,$6E,$04,$00,$00,$DC,$04,$DB,$36,$B4,$11,$00,$00,$70,$21
              byte $29,$5D,$42,$52,$40,$6D,$85,$24,$41,$55,$F5,$09,$99,$56,$ED,$12,$32,$55,$D5,$25,$64,$D4,$56,$4B,$C8,$B4,$6D,$97,$90,$D9,$B6,$2D
              byte $83,$00,$A0,$E4,$96,$00,$68,$92,$0D,$00,$A0,$30,$1B,$00,$A0,$49,$36,$00,$88,$D8,$6C,$C8,$9F,$26,$D9,$B0,$91,$62,$39,$60,$00,$9A
              byte $64,$00,$00,$01,$00,$80,$4B,$95,$0E,$2C,$A9,$AA,$26,$5B,$AA,$B6,$6A,$B6,$44,$6D,$95,$6C,$52,$DB,$56,$DA,$DA,$B6,$6D,$BB,$25,$49
              byte $92,$64,$C9,$0F,$00,$40,$92,$8C,$55,$AB,$24,$19,$D9,$AA,$49,$32,$B6,$6D,$93,$64,$A4,$DA,$26,$C9,$D8,$56,$4D,$92,$91,$24,$99,$24
              byte $43,$DB,$22,$91,$DA,$B7,$52,$D2,$AA,$AD,$DA,$A4,$AD,$AA,$B6,$89,$D4,$AA,$95,$92,$56,$6D,$D5,$26,$6D,$55,$B5,$4D,$DA,$56,$6D,$9B
              byte $B7,$6D,$DB,$16