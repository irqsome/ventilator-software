{{
  SPI RAM Driver.
  Uses 2 bit mode and tries to reduce overhead
  DO NOT USE TOGETHER WITH SD CARD 
}}
CON

'addr_mask = $00FFFFFF
write_cmd = $02
read_cmd = $03

seqmode_read = 1
seqmode_write = 2

VAR

long DOpin, CLKpin, DIpin, CSpin,seqptr
byte seqmode


pub init(DO,CLK,DI,CS)
  if DO <> DI+1
    abort
  DOpin := DO
  CLKpin := CLK
  DIpin := DI
  CSpin := CS            
  seqptr := -1
  seqmode := 0

  outa[CLKpin]~
  dira[CLKpin]~~
  outa[CSpin]~~
  dira[CSpin]~~

  dira[DOpin]~~
  dira[DIpin]~~
  outa[CSpin]~
  writeval($ffff,16) 'reset to SPI mode
  outa[CSpin]~~
  dira[DOpin]~
  outa[CSpin]~
  writeval(%00_00_11_11_11_00_11_11,16) 'set SDI (2-bit) mode (every bit is repeated twice since the ram is now in 1 bit mode)
  outa[CSpin]~~
  dira[DOpin]~~
  outa[CSpin]~
  writeval($0140,16) 'set sequential mode

  !outa[CSpin]
  !outa[CSpin] 
  dira[DOpin]~
  dira[DIpin]~
  

pub write(buffer,address,length)
  if seqmode <> seqmode_write OR seqptr <> address
    outa[CSpin]~~ 

    outa[CSpin]~
    
    dira[DOpin]~~
    dira[DIpin]~~
    outa[CSpin]~
    writeval(((address<<8)|write_cmd)->8,32)
    seqmode := seqmode_write
    seqptr := address

  dira[DOpin]~~
  dira[DIpin]~~
  
  repeat length
    writeval(byte[buffer++],8)

  dira[DOpin]~
  dira[DIpin]~
  
  seqptr += length
pub read(buffer,address,length)
  if seqmode <> seqmode_read OR seqptr <> address
    outa[CSpin]~~
    
    dira[DOpin]~~
    dira[DIpin]~~
    outa[CSpin]~
    writeval(((address<<8)|read_cmd)->8,32)
    seqmode := seqmode_read
    seqptr := address
    skipbyte ' Dummy byte (only required in SDI and SQI)

  seqptr += length
  repeat length
    byte[buffer++] := readval(8)

PRI skipbyte
  dira[DOpin]~
  dira[DIpin]~
  repeat 4
    !outa[CLKpin]
    !outa[CLKpin]

pri readval(bits) | val
  val~ ' clear input
  bits >>= 1
  repeat Bits
    'val := (val << 1) | ina[DOpin]
    'val := (val << 1) | ina[DIpin]
    val := (val << 2) | (ina[DOpin..DIpin])
    outa[CLKpin]~~
    outa[CLKpin]~
  return val

pri writeval(val,bits)
  val <<= (32 - bits) ' pre-align
  bits >>= 1
  repeat bits
    'outa[DOpin] := (val <-= 1) & 1
    'outa[DIpin] := (val <-= 1) & 1
    outa[DOpin..DIpin] := ((val <-= 2)&%11) 
    outa[CLKpin]~~
    outa[CLKpin]~  

pub preparecog
  outa[CSpin]~
  dira[CSpin]~~
  outa[CLKpin]~
  dira[CLKpin]~~

{pub prepare
   outa[CLKpin]~
   dira[CLKpin]~~
   outa[CSpin]~~
   dira[CSpin]~~
pub end
   outa[CSpin]~~
   dira[DIpin]~
   dira[DOpin]~
   dira[CLKpin]~
   dira[CSpin]~
   seqmode := 0}