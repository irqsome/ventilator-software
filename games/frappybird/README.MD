Frappy Bard!
============

## Video
[![](http://img.youtube.com/vi/XhBVP3wZQ7s/0.jpg)](http://www.youtube.com/watch?v=XhBVP3wZQ7s "Video")

## Info
- Type: Game
- Genre: Puzzle
- Author(s): LameStation LLC
- First release: 2014
- Improved version: **Yes**
- Players: 1
- Special requirements: None
- Video formats: NTSC, PAL60. **Anamorphic Widescreen support**
- Inputs: Gamepad
- License: MIT

## Description
This one, I think, speaks for itself.

## Controls
|Gamepad|Action|
|------|-------|
|A|Frap?|
|Select|Toggle anamorphic widescreen|
|Start|Toggle NTSC/PAL|

