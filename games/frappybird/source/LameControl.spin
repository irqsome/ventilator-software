' --------------------------------------------------------------------------------
' LameControl.spin
' Version: 0.6.2-rc2
' Copyright (c) 2015-2016 LameStation LLC
' See end of file for terms of use.
' --------------------------------------------------------------------------------
OBJ
    pin     :   "LamePinout"
    fn      :   "LameFunctions"
    
CON

    
  SNES_R      = %0000000000010000
  SNES_L      = %0000000000100000
  SNES_X      = %0000000001000000
  SNES_A      = %0000000010000000
  SNES_RIGHT  = %0000000100000000
  SNES_LEFT   = %0000001000000000
  SNES_DOWN   = %0000010000000000
  SNES_UP     = %0000100000000000
  SNES_START  = %0001000000000000
  SNES_SELECT = %0010000000000000
  SNES_Y      = %0100000000000000
  SNES_B      = %1000000000000000

    J_U =  SNES_UP
    J_D =  SNES_DOWN
    J_L =  SNES_LEFT
    J_R =  SNES_RIGHT
   
    SW_A =  SNES_A
    SW_B =  SNES_B          
     
DAT

    controls    long    -1
    last        long    -1

PUB Start

    'dira[pin#BUTTON_A..pin#BUTTON_B]~
    'dira[pin#JOY_UP..pin#JOY_RIGHT]~
    
    
PUB Update | nes_bits,i

DIRA [pin#JOY_CLK] := 1 ' output
DIRA [pin#JOY_LCH] := 1 ' output
DIRA [pin#JOY_DATAOUT0] := 0 ' input
DIRA [pin#JOY_DATAOUT1] := 0 ' input
                                       
OUTA [pin#JOY_CLK] := 0
OUTA [pin#JOY_LCH] := 0   
                           
OUTA [pin#JOY_LCH] := 1                                                         
                         
OUTA [pin#JOY_LCH] := 0


nes_bits := 0
nes_bits := INA[pin#JOY_DATAOUT0] | (INA[pin#JOY_DATAOUT1] << 16)

repeat i from 0 to 14
  OUTA [pin#JOY_CLK] := 1 ' JOY_CLK = 1
  OUTA [pin#JOY_CLK] := 0 ' JOY_CLK = 0
  nes_bits := (nes_bits << 1)
  nes_bits := nes_bits | INA[pin#JOY_DATAOUT0] | (INA[pin#JOY_DATAOUT1] << 16)

last  := controls
controls := nes_bits

''Haxx: allow toggling aspect ratio and tv system using $7fff
if (controls & !last) & SNES_SELECT
  byte[$7FFF] ^= 1

if (controls & !last) & SNES_START
  byte[$7FFF] ^= 2

    

' 'On' is a logic low for every control pin coming
' from the hardware, so they must all be inverted.
PUB A
    return ((!controls) & SW_A <> 0)
    
PUB B
    return ((!controls) & SW_B <> 0)

PUB Left
    return ((!controls) & J_L <> 0)
    
PUB Right
    return ((!controls) & J_R <> 0)

PUB Up
    return ((!controls) & J_U <> 0)

PUB Down
    return ((!controls) & J_D <> 0)
    
DAT
    click   byte    0
    
PUB WaitKey
    repeat
        Update
        if A or B
            if not click
                click := 1
                quit
        else
            click := 0
        fn.Sleep(20)


DAT
' --------------------------------------------------------------------------------------------------------
' TERMS OF USE: MIT License
'
' Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
' associated documentation files (the "Software"), to deal in the Software without restriction, including
' without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
' copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
' following conditions:
'
' The above copyright notice and this permission notice shall be included in all copies or substantial
' portions of the Software.
'
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
' IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
' WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
' SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
' --------------------------------------------------------------------------------------------------------
DAT