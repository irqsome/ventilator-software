' /////////////////////////////////////////////////////////////////////////////
' StarBlaster Game
' AUTHOR: Frank Korf
' LAST MODIFIED: 1.29.12
' VERSION 1.0
' COMMENTS: Simple space shooter
' Special thanks to Andre' LaMothe and the Hydra team
'
' CONTROLS: gamepad (must be plugged in)
' Start  = "Start"
' Move = "Dpad"
' Shoot = "A"
' /////////////////////////////////////////////////////////////////////////////

'///////////////////////////////////////////////////////////////////////
' CONSTANTS SECTION ////////////////////////////////////////////////////
'///////////////////////////////////////////////////////////////////////

CON

  _clkmode = xtal1 + pll16x            ' enable external clock and pll times 8
  _xinfreq = 5_000_000       ' set frequency to 10 MHZ plus some error
  _stack = ($3000 + $3000 + 64) >> 2  'accomodate display memory and stack

  ' graphics driver and screen constants
  PARAMCOUNT        = 14        
  OFFSCREEN_BUFFER  = $2000           ' offscreen buffer
  ONSCREEN_BUFFER   = $5000           ' onscreen buffer

  ' size of graphics tile map
  X_TILES           = 16
  Y_TILES           = 12
  
  SCREEN_WIDTH      = 256
  SCREEN_HEIGHT     = 192 

  ' text position constants  
  SCORE_X_POS       = -SCREEN_WIDTH/2 + 10
  SCORE_Y_POS       = SCREEN_HEIGHT/2 - 1*14

  HISCORE_X_POS     = -24
  HISCORE_Y_POS     = SCREEN_HEIGHT/2 - 1*14

  SHIPS_X_POS       = SCREEN_WIDTH/2 - 10/2*12
  SHIPS_Y_POS       = SCREEN_HEIGHT/2 - 1*14

  ' angular constants to make object declarations easier
  ANG_0    = $0000
  ANG_360  = $2000
  ANG_240  = ($2000*2/3)
  ANG_180  = ($2000/2)
  ANG_120  = ($2000/3)
  ANG_90   = ($2000/4)
  ANG_60   = ($2000/6)
  ANG_45   = ($2000/8)
  ANG_30   = ($2000/12)
  ANG_22_5 = ($2000/16)
  ANG_15   = ($2000/24)
  ANG_10   = ($2000/36)
  ANG_5    = ($2000/72)
  
  ' game states
  GAME_STATE_INIT      = 0
  GAME_STATE_MENU      = 1
  GAME_STATE_START     = 2
  GAME_STATE_RUN       = 3
  GAME_STATE_OVER      = 4
  
  'Object Contstants
  OBJECT_SIZE = 3
  _POSITION       'long
  _X = $0         'word
  _Y = $1         'word
  _VEL   = $2     'word
  _VEL_X = $4     'byte
  _VEL_Y = $5     'byte
  _LIFETIME = $3  'word
  
  'Player Constants
  PLAYER_SIZE = OBJECT_SIZE
  PLAYER_SPEED = 3
  RELOAD_TIME = 15
  
  'Bullet Constants
  MAX_BULLETS = 3
  BULLET_SIZE = OBJECT_SIZE + 1
  BULLET_SPEED = 4
  BULLET_LIFETIME = 30
  
  'Mob Constants
  MOB_SIZE = OBJECT_SIZE + 2
  MAX_MOBS = 8
  _AI = $4
  _AI_COUNT = $8
  _AI_STEP = $9
  
  'Explosion Constants
  _EXPLOSION_LIFE = $2
  

'///////////////////////////////////////////////////////////////////////
' VARIABLES SECTION ////////////////////////////////////////////////////
'///////////////////////////////////////////////////////////////////////

VAR
  long  tv_status     '0/1/2 = off/visible/invisible           read-only
  long  tv_enable     '0/? = off/on                            write-only
  long  tv_pins       '%ppmmm = pins                           write-only
  long  tv_mode       '%ccinp = chroma,interlace,ntsc/pal,swap write-only
  long  tv_screen     'pointer to screen (words)               write-only
  long  tv_colors     'pointer to colors (longs)               write-only               
  long  tv_hc         'horizontal cells                        write-only
  long  tv_vc         'vertical cells                          write-only
  long  tv_hx         'horizontal cell expansion               write-only
  long  tv_vx         'vertical cell expansion                 write-only
  long  tv_ho         'horizontal offset                       write-only
  long  tv_vo         'vertical offset                         write-only
  long  tv_broadcast  'broadcast frequency (Hz)                write-only
  long  tv_auralcog   'aural fm cog                            write-only

  word  screen[X_TILES * Y_TILES] ' storage for screen tile map
  long  colors[64]                ' color look up table
  
  word  map_height[64]
  word seed
  
  byte game_state
  byte restart_timer
  
  word score
  word high_score
  
  'two words per star: x and y pos
  word star_list[64]
  
  word camera_x
  word camera_y
  
  'four words for the character
  word player[PLAYER_SIZE]
  word num_ships
  
  'four words per bullet: x and y pos, direction, lifetime
  word  bullet_list[ MAX_BULLETS * BULLET_SIZE ]
  word  num_bullets
  word  bullet_timer
  
  'four words per mob: x and y pos, direction, ai state
  word mob_list[ MAX_MOBS * MOB_SIZE ]
  word num_mobs
  word mob_timer
  
  '
  word explosion_list[ MAX_MOBS * OBJECT_SIZE ]
  
  
'///////////////////////////////////////////////////////////////////////
' OBJECT DECLARATION SECTION ///////////////////////////////////////////
'///////////////////////////////////////////////////////////////////////
OBJ
  tv    : "tv.spin"          ' instantiate a tv object
  gr    : "Fk_graphics_drv_010.spin"    ' instantiate a graphics object
  nes   : "Fk_NESDriver_001.spin"
'///////////////////////////////////////////////////////////////////////
' PUBLIC FUNCTIONS /////////////////////////////////////////////////////
'///////////////////////////////////////////////////////////////////////

PUB start | i, dx, dy

  game_state := GAME_STATE_INIT

  'start tv
  longmove(@tv_status, @tvparams, paramcount)
  tv_screen := @screen
  tv_colors := @colors
  tv.start(@tv_status)

  'init colors
  repeat i from 0 to 64
    colors[i] := $00001010 * (i+4) & $F + $FB060C02

  'init tile screen
  repeat dx from 0 to tv_hc - 1
    repeat dy from 0 to tv_vc - 1
      screen[dy * tv_hc + dx] := onscreen_buffer >> 6 + dy + dx * tv_vc + ((dy & $3F) << 10)

  'start and setup graphics 256x192, with orgin (0,0) at bottom left of screen
  gr.start
  gr.setup(X_TILES, Y_TILES, SCREEN_WIDTH/2, SCREEN_HEIGHT/2, offscreen_buffer)

  ' BEGIN GAME LOOP ////////////////////////////////////////////////////  

  ' infinite loop
  repeat while TRUE
    case game_state
      GAME_STATE_INIT:
        GameInit
        game_state := GAME_STATE_START
      GAME_STATE_START:
        GameStart
        game_state := GAME_STATE_MENU
        i := 0
      GAME_STATE_RUN,GAME_STATE_MENU,GAME_STATE_OVER:

        'clear the offscreen buffer
        gr.clear

        camera_x++
        nes.Update
        if( game_state == GAME_STATE_RUN )
          GameUpdate
        else
          if( nes.Start )
            game_state := GAME_STATE_RUN
            i := 0

        ' RENDERING SECTION (render to offscreen buffer always//////////////

        RenderMap(camera_x,camera_y)

        if( game_state == GAME_STATE_RUN )
          GameRender
        else
       ' draw start game text
          gr.textmode(3,1,6,5)
          gr.colorwidth(2,0)
          if (i > 200)
            if (++i & $8)
              gr.text(0,0,@start_string)
          else
            i++
            if ( game_state == GAME_STATE_MENU )
              gr.text(0,0,@title_string)
            else
              gr.text(0,0,@over_string)

        HudRender
    
        'copy bitmap to display offscreen -> onscreen
        gr.copy(onscreen_buffer)

    ' synchronize to frame rate would go here...

    ' END RENDERING SECTION ///////////////////////////////////////////////

  ' END MAIN GAME LOOP REPEAT BLOCK //////////////////////////////////

'//////Game Functions

PUB GameInit
  restart_timer := $FF
  seed := 1234
  high_score := 0

PUB GameStart
  camera_x := 80
  camera_y := 90
  PrepPlayer
  PrepMap
  PrepBullets
  PrepMobs  
  PrepExplosions
  
PUB GameUpdate
  UpdatePlayer  
  UpdateBullets
  UpdateMobs
  UpdateExplosions
  ProcessCollisions

PUB GameRender
  RenderBullets(camera_x,camera_y)
  RenderMobs(camera_x,camera_y)     
  RenderExplosions( camera_x, camera_y )

  if( restart_timer == $FF )
    gr.colorwidth(2, 0)
    gr.pix( player.WORD[_X] - camera_x, player.WORD[_Y] - camera_y, 0, @pix_player )


PUB HudRender | i
  gr.colorwidth(2, 0)
  ' draw score and num players
  gr.textmode( 2,1,6,0 )
  gr.text(SCORE_X_POS, SCORE_Y_POS, @score_string)
  gr.text(HISCORE_X_POS, HISCORE_Y_POS, @hiscore_string)
  gr.text(SHIPS_X_POS, SHIPS_Y_POS, @ships_string)

  PrintScore( score, SCORE_X_POS + 10, SCORE_Y_POS - 13 )
  PrintScore( high_score, HISCORE_X_POS + 5, HISCORE_Y_POS - 13 )

  ' draw players ships left
  gr.colorwidth(3,0)
  repeat i from 0 to num_ships
    gr.pix( SHIPS_X_POS + i << 4 + 12, SHIPS_Y_POS - 10, 1, @pix_player )


PUB PrintScore( i, x, y )  | t, str
  str := 5
  gr.colorwidth(2,0)
  gr.textmode( 1,1,6,0 )
  repeat t from 0 to 6
    BYTE[ @score_number + str ] := 48 + (i // 10)
    i /= 10
    str--
  gr.text( x,y, @score_number )



'////Player Code
PUB PrepPlayer
  player.WORD[_X] := 0
  player.WORD[_Y] := 100
  player.WORD[_VEL] := $00_00
  num_ships := 2
  score := 0

PUB UpdatePlayer

if( restart_timer == $FF )
  player.WORD[_VEL] := $00_00
  if( nes.Right )
    player.BYTE[_VEL_X] := 2
  if( nes.Left )
    player.BYTE[_VEL_X] := -1
  if( nes.Up )
    player.BYTE[_VEL_Y] := 2
  if( nes.Down )
    player.BYTE[_VEL_Y] := -2
  if( nes.A )
    AddBullet( player.WORD[_X], player.WORD[_Y] )
  
  player.WORD[_X]++
  if( player.WORD[_X] > camera_x )
    player.WORD[_X] := camera_x 
  if( camera_x - player.WORD[_X] > 100 )
    player.WORD[_X] := camera_x - 100
  if( player.WORD[_Y] < 20 )
    player.WORD[_Y] := 20
  if( player.WORD[_Y] > 180 )
    player.WORD[_Y] := 180

  UpdatePos( @player )
elseif( restart_timer == 0 )
  ' if we're out of ships, game over
  restart_timer := $FF
  if( num_ships == 0 )
    game_state := GAME_STATE_OVER
    if( score > high_score )
      high_score := score
  ' else replace the player and reset the timer
    GameStart
  else
    num_ships--
  'subtract a ship
else
  restart_timer --

'/////TERRAIN AND STAR CODE

PUB PrepMap | i , s
  s := 1234
  i := 0 
  repeat 64
    map_height[i] := ?s
    map_height[i] //= 20
    i++
  i := 0
  repeat 66
    map_height[i//64] := ( map_height[ i//64 ] + map_height[ (i+1)//64 ] + map_height[ (i+2)//64 ] ) / 3
    i++
  
  i := 0
  repeat 32
    star_list[i] := ?s
    star_list[i] //= 360
    star_list[i+1] := ?s
    star_list[i+1] //= 180
    i += 2
    
PUB RenderMap(x,y) | i, j, first
  gr.colorwidth(2,1)
  i := 0
  j := ( x / 10 ) // 64
  first := x // 10
  first += 130
  gr.plot( -first, -y + map_height[j] )
  repeat 28
    gr.line( -first + i*10, -y + map_height[j] )
    i++
    j := (j+1) // 64
  
  i := 0
  gr.colorwidth(2, 0)
  repeat 32
    gr.plot( star_list[i] - 180, star_list[i+1] - 90 )
    i += 2


'/////////BULLET CODE
PUB PrepBullets | i
  i := 0
  REPEAT MAX_BULLETS * BULLET_SIZE
    bullet_list[i] := $0000
    i++
  num_bullets := 0
  bullet_timer := 1

PUB AddBullet( x, y ) | i
  i := 0
  if( num_bullets < MAX_BULLETS )
    if( bullet_timer == 0 )
      REPEAT UNTIL bullet_list[i]==0
        i+=BULLET_SIZE 
      bullet_list.WORD[i+_X] := x
      bullet_list.WORD[i+_Y] := y
      bullet_list.WORD[i+_LIFETIME] := BULLET_LIFETIME
      bullet_timer := RELOAD_TIME
            
PUB RenderBullets( x, y ) | i
  gr.colorwidth(2, 1)
  i := 0
  repeat MAX_BULLETS
    if( bullet_list[i] <> 0 )
      gr.plot( bullet_list.WORD[i+_X] - x, bullet_list.WORD[i+_Y] - y) 
    i += BULLET_SIZE

PUB UpdateBullets | i
i := 0
num_bullets := 0
repeat MAX_BULLETS
  if( bullet_list[i] <> 0 )
    bullet_list.WORD[i+_X] += BULLET_SPEED
    bullet_list.WORD[i+_LIFETIME]--
    if( bullet_list.WORD[i+_LIFETIME] == 0 )
      bullet_list[i] := 0
    else
      num_bullets++
  i += BULLET_SIZE
if( bullet_timer > 0 )
  bullet_timer--
    

'/////////MOB CODE
PUB PrepMobs | i
  i := 0
  REPEAT MOB_SIZE * MAX_MOBS
    mob_list[i] := $00_00  
    i++
  mob_timer := 10
  num_mobs := 0

PUB UpdateMobs | i
  i := 0
  num_mobs := 0
  
  repeat MAX_MOBS
    if( mob_list[i] <> 0 )
      UpdateAI( @mob_list[i] )
      UpdatePos( @mob_list[i] )
      mob_list.WORD[i+_LIFETIME]--
      if( mob_list.WORD[i+_LIFETIME] == 0 )
        mob_list[i] := 0
      else
        num_mobs++
    i += MOB_SIZE
    
  mob_timer--
  if( mob_timer == 0 )
    mob_timer := 30
    if( num_mobs < MAX_MOBS )
      AddMob

PUB AddMob | i
  i:=0
  REPEAT UNTIL mob_list[i] == 0
    i += MOB_SIZE  
  mob_list.WORD[i+_X] := camera_x + 200
  mob_list.WORD[i+_Y] := ?seed
  mob_list.WORD[i+_Y] //= 120 
  mob_list.WORD[i+_Y] += 40
  mob_list.WORD[i+_VEL] := WORD[ @mob_movement[0] ] 'vel
  mob_list.WORD[i+_LIFETIME] := 500     'lifetime
  mob_list.WORD[i+_AI] := $00_00                   'ai state
      

PUB UpdateAI( mob_addr ) | count, curr_step
  count := BYTE[mob_addr][_AI_COUNT]
  curr_step := BYTE[mob_addr][_AI_STEP]
  count ++
  if( count > 10 )
    count := 0
    curr_step := (curr_step+1) // 6
    WORD[ mob_addr ][ _VEL ] := WORD[ @mob_movement[ curr_step ] ]
    BYTE[ mob_addr ][ _AI_STEP ] := curr_step
    
  BYTE[ mob_addr ][ _AI_COUNT ] := count


PUB RenderMobs( x, y ) | i
  gr.colorwidth(3, 0)
  i := 0
  repeat MAX_MOBS
    if( mob_list[i] <> 0 )
      gr.pix( mob_list.WORD[i+_X] - x, mob_list.WORD[i+_Y]-y, 4, @pix_tie )
    i += MOB_SIZE

'//////Explosion Code
PUB PrepExplosions | i
  i := 0
  REPEAT OBJECT_SIZE * MAX_MOBS
    explosion_list[i] := $0
    i++

PUB UpdateExplosions | i
  i := 0
  REPEAT MAX_MOBS
    if( explosion_list[i] <> 0 )
      explosion_list[i+_EXPLOSION_LIFE]+=3
      if( explosion_list[i+_EXPLOSION_LIFE] > 30 )
        explosion_list[i] := 0
    i += OBJECT_SIZE

PUB AddExplosion( x, y ) | i
  i := 0
  REPEAT UNTIL explosion_list[i] == 0
    i += OBJECT_SIZE
  explosion_list[i+_X] := x
  explosion_list[i+_Y] := y
  explosion_list[i+_EXPLOSION_LIFE] := 0
  

PUB RenderExplosions(x, y) | i,offset
  gr.colorwidth(1, 1)
  i := 0
  REPEAT MAX_MOBS
    if( explosion_list[i] <> 0 )
      offset := explosion_list[ i + _EXPLOSION_LIFE ]
      gr.vecarc(explosion_list[i+_X]-x,explosion_list[i+_Y]-y,offset/5,offset/5,-(offset<<10+offset<<6),offset,-(offset),@vec_star)
    i += OBJECT_SIZE
    
'//////Object Physics

PUB UpdatePos( obj_addr ) 
  WORD[obj_addr][_X] := (BYTE[obj_addr][_VEL_X]+128)//256 + WORD[obj_addr][_X] -128
  WORD[obj_addr][_Y] := (BYTE[obj_addr][_VEL_Y]+128)//256 + WORD[obj_addr][_Y] -128

'/////////////Collision Detection

PUB ProcessCollisions | i,j,dx,dy,dist
  i := 0
  repeat MAX_BULLETS
    if ( bullet_list[i] <> 0 )
      j := 0
      repeat MAX_MOBS
        if( mob_list[j] <> 0 )
          dx := bullet_list.WORD[i+_X] - mob_list.WORD[j+_X]
          dy := bullet_list.WORD[i+_Y] - mob_list.WORD[j+_Y]
          dist := (dx*dx+dy*dy)
          if( dist < 220 )
            AddExplosion( bullet_list[i+_X], bullet_list[i+_Y] )
            bullet_list[i] := 0
            mob_list[j] := 0
            score ++
        j += MOB_SIZE
    i += BULLET_SIZE
  i := 0
  repeat MAX_MOBS
    if( mob_list[i] <> 0 )
      dx := player.WORD[_X] - mob_list.WORD[i+_X]
      dy := player.WORD[_Y] - mob_list.WORD[i+_Y]
      dist := (dx*dx+dy*dy)
      if( dist < 280 )
        restart_timer := 100
        AddExplosion( player[_X], player[_Y] )
        mob_list[i] := 0
    i += MOB_SIZE

'///////////////////////////////////////////////////////////////////////
' DATA SECTION /////////////////////////////////////////////////////////
'///////////////////////////////////////////////////////////////////////

DAT

' TV PARAMETERS FOR DRIVER /////////////////////////////////////////////

tvparams                long    0               'status
                        long    1               'enable
                        long    %001_0101      'pins
                        long    %0000           'mode
                        long    0               'screen
                        long    0               'colors
                        long    x_tiles         'hc
                        long    y_tiles         'vc
                        long    10              'hx timing stretch
                        long    1               'vx
                        long    0               'ho
                        long    0               'vo
                        long    55_250_000      'broadcast
                        long    0               'auralcog

score_number
  'text'123456' 6 bytes
  byte "xxxxxx",0 

score_string            byte    "Score",0               'text
hiscore_string          byte    "High",0                'text
ships_string            byte    "Ships",0               'text
start_string            byte    "PRESS START",0         'text
title_string            byte    "StarBlaster",0         'text
over_string             byte    "GAME OVER",0

mob_movement
  word $01_00
  word $FE_00
  word $01_01
  word $FE_F0
  word $01_00
  word $FE_00
  
pix_dog                 word                            'dog
                        byte    1,4,0,0
                        word    %%20000022
                        word    %%02222222
                        word    %%02222200
                        word    %%02000200

pix_tie     word                           ' tie fighter in normal wing configuration
                      byte    2,8,3,3                ' 2 words wide (8 pixels) x 8 words high (8 lines), 8x8 sprite
                      word    %%01000000,%%00000010      
                      word    %%10000000,%%00000001
                      word    %%10000111,%%11100001
                      word    %%11111111,%%11111111
                      word    %%11111111,%%11111111
                      word    %%10000111,%%11100001
                      word    %%10000000,%%00000001
                      word    %%01000000,%%00000010

pix_player    word
              byte   1, 8, 0, 3
              word   %%00011110
              word   %%00110000
              word   %%11111000
              word   %%01111111
              word   %%01111111
              word   %%11111000
              word   %%00110000
              word   %%00011110
vec_star                word    $4000+$2000/12*0        'star
                        word    50
                        word    $8000+$2000/12*1
                        word    20
                        word    $8000+$2000/12*2
                        word    50
                        word    $8000+$2000/12*3
                        word    20
                        word    $8000+$2000/12*4
                        word    50
                        word    $8000+$2000/12*5
                        word    20
                        word    $8000+$2000/12*6
                        word    50
                        word    $8000+$2000/12*7
                        word    20
                        word    $8000+$2000/12*8
                        word    50
                        word    $8000+$2000/12*9
                        word    20
                        word    $8000+$2000/12*10
                        word    50
                        word    $8000+$2000/12*11
                        word    20
                        word    $8000+$2000/12*0
                        word    50
                        word    0