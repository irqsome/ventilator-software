' /////////////////////////////////////////////////////////////////////////////
' NES Controller Driver
' AUTHOR: Frank Korf
' LAST MODIFIED: 1.29.09
' VERSION 0.1
' /////////////////////////////////////////////////////////////////////////////
CON

  JOY_CLK = 16
  JOY_LCH = 17
  JOY_DATAOUT0 = 19
  JOY_DATAOUT1 = 18

 ' NES bit encodings for NES gamepad 0
  NES0_RIGHT  = %00000000_00000001
  NES0_LEFT   = %00000000_00000010
  NES0_DOWN   = %00000000_00000100
  NES0_UP     = %00000000_00001000
  NES0_START  = %00000000_00010000
  NES0_SELECT = %00000000_00100000
  NES0_B      = %00000000_01000000
  NES0_A      = %00000000_10000000

VAR 
  long nespad0
        
PUB Update
  nespad0 := Read_Gamepad_0
  
PUB Up : nesbits
  nesbits := ( (nespad0 & NES0_UP) <> 0 )
PUB Down : nesbits
  nesbits := ( (nespad0 & NES0_DOWN) <> 0 )
PUB Left : nesbits
  nesbits := ( (nespad0 & NES0_LEFT) <> 0 )
PUB Right : nesbits
  nesbits := ( (nespad0 & NES0_RIGHT) <> 0 )
PUB Start : nesbits
  nesbits := ( (nespad0 & NES0_START) <> 0 )
PUB Select : nesbits
  nesbits := ( (nespad0 & NES0_SELECT) <> 0 )
PUB A : nesbits
  nesbits := ( (nespad0 & NES0_A) <> 0 )
PUB B : nesbits
  nesbits := ( (nespad0 & NES0_B) <> 0 )

PUB Read_Gamepad_0 : nes_bits   |       i
  DIRA [JOY_CLK] := 1 ' output
  DIRA [JOY_LCH] := 1 ' output
  DIRA [25] := 0 ' input
  DIRA [26] := 0 ' input

  OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
  OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0
  OUTA [JOY_LCH] := 1 ' JOY_SH/LDn = 1
  OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0
  nes_bits := 0
  nes_bits := INA[JOY_DATAOUT0] | (INA[JOY_DATAOUT1] << 8)

  repeat i from 0 to 6
    OUTA [JOY_CLK] := 1 ' JOY_CLK = 1
    OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
    nes_bits := (nes_bits << 1)
    nes_bits := nes_bits | INA[JOY_DATAOUT0] | (INA[JOY_DATAOUT1] << 8)

  nes_bits := (!nes_bits & $FFFF)

DAT