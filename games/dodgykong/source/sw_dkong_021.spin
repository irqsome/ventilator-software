''//////////////////////////////////////////////////////////////////////////////
'' Donkey Kong Remake
'' AUTHOR: Steve Waddicor
'' SOUND:  Eric Moyer
''//////////////////////////////////////////////////////////////////////////////

{{

OPEN DEFECTS:
    Sometimes in one player mode, lives remaining goes from 0 up to 7.
    Sprites tearing?
        
TODO (When and if memory is available):
    Hi score table.
    Attract Mode.

}}

OBJ

  tv    : "sw_dk_tv_drv_022.spin"
  gfx   : "sw_dkong_gfx_011.spin"
  rend  : "sw_dk_gfx_renderer_015.spin"
  hdmf_lite : "EPM_HDMF_Lite_custom_DK_driver_014.spin"      ' HDMF Lite driver

CON
    _clkmode = xtal1 + pll16x
    _xinfreq = 5_000_000 + 0000
    _stack = 76
    '1 cog for game logic (this one)
    '1 cog for TV driver
    '1 cog for sound
    '=5 cogs for rendering
    COGS = 5

CON
    SPRITE_FLAGS                =3
    SPRITE_TILE                 =2
    SPRITE_XPOS                 =1
    SPRITE_YPOS                 =0
    SPRITE_FLAGS_AND_TILE_WORD  =1
    SPRITE_LOCATION_WORD        =0
    SPRITE_INVISIBLE            =%100
    SPRITE_FLIPPED              =%010
    SPRITE_MIRRORED             =%001

CON
    NUMBER_OF_BARRELS           = 10
    NUMBER_OF_LADDERS           = 2
    NUMBER_OF_SPRINGS           = 2
    NUMBER_OF_PIES              = 5
    NUMBER_OF_HAMMERS           = 2

CON
    WORKING_MAP = 0

CON
    #0
    'states where user input accepted
    MARIO_FACING_LEFT
    MARIO_FACING_RIGHT
    MARIO_WALK_LEFT
    MARIO_WALK_RIGHT
    MARIO_CLIMB_UP
    MARIO_CLIMB_DOWN
    'no user input accepted
    MARIO_JUMP_LEFT
    MARIO_JUMP_RIGHT
    MARIO_JUMP_UP_FACING_LEFT
    MARIO_JUMP_UP_FACING_RIGHT
    MARIO_LANDING_LEFT
    MARIO_LANDING_RIGHT
    MARIO_FALLING
    MARIO_DEAD_INIT
    MARIO_DEAD

CON
    #0
    MARIO_NOT_ON_LIFT
    MARIO_LIFT_UP
    MARIO_LIFT_DOWN

CON
    #0
    GAME_TITLE_INIT
    GAME_TITLE
    GAME_START
    GAME_DISPLAY_PLAYER_INIT
    GAME_DISPLAY_PLAYER
    GAME_HOW_HIGH_INIT
    GAME_HOW_HIGH
    GAME_LEVEL_1_INIT
    GAME_LEVEL_2_INIT
    GAME_LEVEL_3_INIT
    GAME_LEVEL_4_INIT
    GAME_LEVEL_1
    GAME_LEVEL_2
    GAME_LEVEL_3
    GAME_LEVEL_4
    GAME_CLIMB_INIT
    GAME_CLIMB_SCENE
    GAME_LEVEL_12_END_SCENE_INIT
    GAME_LEVEL_12_END_SCENE
    GAME_LEVEL_3_END_SCENE_INIT
    GAME_LEVEL_3_END_SCENE
    GAME_LEVEL_123_FOLLOW_ON
    GAME_LEVEL_4_END_SCENE_INIT
    GAME_LEVEL_4_END_SCENE
    GAME_OVER_INIT
    GAME_OVER

CON

    KONG_LEVEL_START_POSE_INIT  = 0
    KONG_LEVEL_START_POSE       = 1
    ' Barrel rolling states i.e. Level 1
    KONG_BR_LEFT                = 2
    KONG_BR_CENTER_1            = 3
    KONG_BR_RIGHT               = 4
    KONG_BR_CENTER_2            = 5
    KONG_BR_GROWL               = 6
    KONG_BR_LEFT_INIT           = 7
    KONG_BR_CENTER_1_INIT       = 8
    KONG_BR_RIGHT_INIT          = 9
    KONG_BR_CENTER_2_INIT       = 10
    KONG_BR_GROWL_INIT          = 11
    ' Normal states i.e. Levels 2-4
    KONG_NORMAL_INIT            = 12
    KONG_NORMAL                 = 13

CON
    #0
    BARREL_STILL
    BARREL_ROLLING_RIGHT
    BARREL_ROLLING_LEFT
    BARREL_FALLING_RIGHT
    BARREL_FALLING_LEFT
    BARREL_BOUNCING_RIGHT
    BARREL_BOUNCING_LEFT
    BARREL_ON_LADDER_RIGHT
    BARREL_ON_LADDER_LEFT
    BARREL_FALLING_DOWN
    BARREL_FALLING_ACROSS
    BARREL_PIE
    FIREBALL_JUMP_LEFT_INIT
    FIREBALL_JUMP_LEFT
    FIREBALL_JUMP_RIGHT_INIT
    FIREBALL_JUMP_RIGHT
    FIREBALL_BOUNCE_LEFT_INIT
    FIREBALL_BOUNCE_RIGHT_INIT
    FIREBALL_BOUNCE_HERE_INIT
    FIREBALL_BOUNCE_LEFT
    FIREBALL_BOUNCE_RIGHT
    FIREBALL_BOUNCE_HERE
    FIREBALL_MOVE_RIGHT
    FIREBALL_UP_LADDER
    FIREBALL_DOWN_LADDER
    FIREBALL_WAITING

CON
    #0
    DRUM_UNLIT
    DRUM_LIT
    DRUM_FLARING
    DRUM_LIT_INIT
    DRUM_FLARING_INIT
    DRUM_LIT_LEVEL3

CON
    PAULINE_STILL_INIT          = 0
    PAULINE_STILL               = 1
    PAULINE_SCREAM_INIT         = 2
    PAULINE_SCREAM              = 3

CON
    LADDER_UP                   = 0
    LADDER_GOING_DOWN           = 1
    LADDER_DOWN                 = 2
    LADDER_GOING_UP             = 3

CON
    #0
    CONVEYOR_RIGHT
    CONVEYOR_LEFT

CON
    #0
    BONUS_NONE
    BONUS_BARREL_BUSTED_INIT
    BONUS_BARREL_BUSTED
    BONUS_ABOUT_TO_DIE
    BONUS_BARREL_HURDLED_INIT
    BONUS_COLLECTABLE_INIT
    BONUS_RIVET_INIT
    BONUS_BARREL_DELAY

VAR
    ' Renderer parameter block
    ' Each renderer copies these values to it's own cog_memory cache
    ' when it's got a little spare time, several times per frame
    ' so these values can be altered on the fly.
    long    rend_cog_number        'each cog gets a different number, starting with zero     write-only
    long    rend_scanline_req_adr  'a pointer to the request to put a line in the scan buffer   write-only
    long    rend_map               'pointer to screen (bytes)                                write-only
    long    rend_tile_table_ptr    'pointer to tile definitions (words)                      write-only
    long    rend_tile_palette_ptr
    long    rend_sprite_table_ptr  'pointer to tile definitions (longs)                      write-only
    long    rend_hotspot_table_ptr
    long    rend_sprite_palette_ptr 'pointer to colors (longs)                               write-only
    long    rend_ht                'horizontal tiles                                         write-only
    long    rend_vt                'vertical tiles                                           write-only
    long    rend_scanline_buffer_adr 'hub memory where renderer puts scanline when requested write-only
    long    rend_cog_count         'total number of cogs                                     write-only
    long    rend_debug_map_ptr
    long    rend_sprites[2]        '%_____ifm_nnnnnnnn_xxxxxxxx_yyyyyyyy    (invisible, flipped, mirrored)
    long    rend_kong_sprites[8]
    long    rend_mario_sprite
    long    rend_sprites_2[16]
    long    rend_heart_umbrella_sprite
    long    rend_hat_sprite
    long    rend_handbag_sprite
    long    rend_oil_drum_sprite
    long    rend_oil_drum_flame_sprite
    long    rend_hammer_sprites[2]
    long    rend_bonus_sprite
    'End of renderer parameter block

VAR
    ' TV parameter block
    long    tv_scanline_request
    long    tv_scanline_buffer
    long    tv_status
    ' End of TV parameter block

VAR
    long  startkey
    long  selectkey
    long  debug

VAR
    byte    game_state
    byte    game_not_seen_climb
    byte    mario_state
    byte    old_mario_state 'EPM
    byte    mario_lift_state
    byte    kong_state
    byte    kong_conveyor_state
    byte    middle_left_conveyor_state
    byte    middle_right_conveyor_state
    byte    bottom_conveyor_state
    byte    drum_state
    byte    pauline_state
    byte    bonus_state
    byte    barrel_state[NUMBER_OF_BARRELS]
    byte    barrel_hurdled[NUMBER_OF_BARRELS]
    byte    ladder_state[NUMBER_OF_LADDERS]
    byte    bonus_freeze
    byte    remove_rivet_x
    byte    remove_rivet_y
    byte    kong_real_x
    byte    kong_real_y
    byte    rivets_left
    byte    player_level[2]
    byte    player_lives[2]
    byte    game_player_up
    byte    fireball_real_y[NUMBER_OF_BARRELS]
    byte    pause
    byte    game_continue_life

    word    ladder_state_count[NUMBER_OF_LADDERS]
    word    spring_state_count[2]
    word    kong_pose_source_ptr
    word    frame
    word    action_hammer_ptr
    word    background_song_ptr

    long    game_two_players
    long    player_score[2]
    long    hi_score
    long    gp
    long    game_state_count
    long    timer
    long    timer_state_count
    long    mario_state_count
    long    hammer_state_count
    long    kong_state_count
    long    kong_conveyor_state_count
    long    conveyor_state_count
    long    pie_state_count
    long    drum_state_count
    long    drum_flicker_count
    long    pauline_state_count
    long    lift_state_count
    long    bonus_state_count
    long    rivet_state_count
    long    sound_state_count
    long    rnd
    long    barrel_anim[NUMBER_OF_BARRELS]
    long    barrel_state_count[NUMBER_OF_BARRELS]
    long    kong_holding_barrel
    long    kong_barrel_releases
    long    spring_real_x[2]
    long    hurdle_count

CON
    NUMBER_OF_SPRITES           = 35

' Level 1 = Barrels
' Level 2 = Springs
' Level 3 = Conveyors
' Level 4 = Rivets

'    Level 1           Level 2           Level 3           Level 4
'  0 Pauline           Pauline           Pauline           Pauline
'  1 Pauline           Pauline           Pauline           Pauline
'  2 Kong              Kong              Kong              Kong
'  3 Kong              Kong              Kong              Kong
'  4 Kong              Kong              Kong              Kong
'  5 Kong              Kong              Kong              Kong
'  6 Kong              Kong              Kong              Kong
'  7 Kong              Kong              Kong              Kong
'  8 Kong                                Ladder            Kong Stars
'  9 Kong                                Ladder
' 10 Mario             Mario             Mario             Mario
' 11 Barrel/Fireball   Barrel/Fireball   Barrel/Fireball   Barrel/Fireball
' 12 Barrel/Fireball   Barrel/Fireball   Barrel/Fireball   Barrel/Fireball
' 13 Barrel/Fireball   Spring            Barrel/Fireball   Barrel/Fireball
' 14 Barrel/Fireball   Spring            Barrel/Fireball   Barrel/Fireball
' 15 Barrel/Fireball   Lift              Barrel/Fireball   Barrel/Fireball
' 16 Barrel/Fireball   Lift              Pie

' 17 Barrel/Fireball   Lift              Pie
' 18 Barrel/Fireball   Lift              Pie
' 19 Barrel/Fireball   Lift              Pie
' 20 Barrel/Fireball   Lift              Pie
' 21 Fixed-Barrel      Winch             Motor
' 22 Fixed-Barrel      Winch             Motor
' 23 Fixed-Barrel      Winch             Motor
' 24 Fixed-Barrel      Winch             Motor
' 25                                     Motor
' 26                                     Motor
' 27 Heart             Heart/Umbrella    Heart/Umbrella    Heart/Umbrella
' 28                   Hat               Hat               Hat
' 29                   Handbag           Handbag           Handbag
' 30 Oil Drum                            Oil Drum
' 31 Oil Drum Flame                      Oil Drum Flame
' 32 Hammer            Hammer            Hammer            Hammer
' 33 Hammer            Hammer            Hammer            Hammer
' 34 Bonus             Bonus             Bonus             Bonus

    'sprite indexes
    PAULINE_SPRITES             = 0
    KONG_SPRITES                = 2
    LADDER_SPRITES              = 8
    KONGS_STAR_SPRITE           = 8
'    MARIO_SPRITE                = 10
    MOVING_BARREL_SPRITES       = 11
    SPRING_SPRITES              = 13
    LIFT_SPRITES                = 15
    PIE_SPRITES                 = 16
    FIXED_BARREL_SPRITES        = 21
    WINCH_SPRITES               = 21
    MOTOR_SPRITES               = 21
'    HEART_SPRITE                = 27
    COLLECTABLE_SPRITES         = 27
'    OIL_DRUM_SPRITE             = 30
    OIL_DRUM_FLAME_SPRITE       = 31
    HAMMER_SPRITES              = 32
    BONUS_SPRITE_INDEX          = 34

PUB Main | i, time, game_counter, total_time, avg_time, t
    'Mark top of stack to cheack later
    long[$7FFC]:=$DEAD_BEEF
    'Start renderers
    rend_tile_table_ptr := gfx.tile_adr
    rend_sprite_table_ptr := gfx.sprite_adr
    rend_hotspot_table_ptr := gfx.sprite_hotspot_adr
    rend_sprite_palette_ptr := gfx.sprite_palette_adr
    rend_tile_palette_ptr := gfx.tile_palette_adr
    SelectMap(WORKING_MAP)
    rend_ht := 32
    rend_vt := 28
    rend_scanline_buffer_adr := @hub_scanline_buffer
    rend_scanline_req_adr := @scanline_req
    rend_cog_count := COGS
    SpriteClear

    rend_debug_map_ptr := 0
    repeat i from 0 to constant(cogs-1)
        rend_cog_number := i
        rend.start(@rend_cog_number)
        'Wait to allow this cog to get the cog number param from the block before changing it.
        repeat 1000

    tv_scanline_request := @scanline_req
    tv_scanline_buffer := @hub_scanline_buffer
    tv.start(@tv_scanline_request)

    hdmf_lite.start(0)
    'time:=CNT
    rnd:=$439845f0
    hi_score:=7650

    'Main Game Loop
    repeat
        'Check stack has not been overrun
        if long[$7FFC] <> $DEAD_BEEF
            DIRA[0]:=1
            OUTA[0]:=1
        'TV will let up know when there is a vsync
        tv_status := 0
        gp := NES_Read_Gamepad

        if not pause
            hdmf_lite.do_playback
            hdmf_lite.SFX_do_playback  'EPM
            case game_state
                GAME_TITLE_INIT:
                    SpriteClear
    '                SelectMap(WORKING_MAP)
                    ClearMap(0)
                    DrawStatus
                    repeat i from 0 to 20
                        DrawString(6,5+i,lookupz(i: @title1,@title2,@title3,@title4,@title5,@blank_line,@title6,@title7,@title8,@title9,@title10,@blank_line,@role1,@credit1,@blank_line,@role2,@credit2,@blank_line,@players,@blank_line,@press_start),8)
                    game_state:=GAME_TITLE
                GAME_TITLE:
                    rend_sprites[0]:= $00_4E_8d_b3 - 16*(game_two_players)<<8
                    'repeat i from constant(5*32*2+1) to constant(17*32*2+1) step 2
                    '    byte[gfx.map_adr][i]:=?rnd&%111
                    if gp & GP_LEFT
                        game_two_players:=FALSE
                    if gp & GP_RIGHT
                        game_two_players:=TRUE    
                    if gp & GP_START
                        game_state:=GAME_START
                GAME_START:
                    player_level[0] := player_level[1] := 0
                    player_score[0] := player_score[1] := 0
                    player_lives[0] := player_lives[1] := 3
                    game_player_up := -game_two_players ' This is zero based.  Pretend player 2 was up last tyo make player 1 up this time.  
                    game_not_seen_climb:=TRUE
                    game_state := GAME_DISPLAY_PLAYER_INIT
                GAME_DISPLAY_PLAYER_INIT:
                    if game_two_players
                        game_player_up^=$01
                        if player_lives[game_player_up]==0                                                 
                            game_player_up^=$01
                            if player_lives[game_player_up]==0
                                game_state:= GAME_OVER_INIT
                        if game_state<>GAME_OVER_INIT
                            SpriteClear
                            ClearMap(2)
                            DrawStatus
                            if game_player_up
                                DrawString(11,14,@player2,4)
                            else
                                DrawString(11,14,@player1,4)    
                            game_state_count:=120
                            game_state:=GAME_DISPLAY_PLAYER
                    elseif player_lives[0]==0
                        game_state:= GAME_OVER_INIT
                    else
                        game_state:=GAME_CLIMB_INIT
                GAME_DISPLAY_PLAYER:
                    if game_state_count--==0
                        ifnot player_level[game_player_up]
                            game_state:=GAME_CLIMB_INIT
                        game_state := GAME_HOW_HIGH_INIT                    
                GAME_CLIMB_INIT:
                    if game_not_seen_climb~
                        SpriteClear
                        game_state_count := 0
                        game_state := GAME_CLIMB_SCENE
                        hdmf_lite.play_song(@_SONG_start,0)
                    else
                        game_state := GAME_HOW_HIGH_INIT
                GAME_CLIMB_SCENE:
                    ClimbScene
                    'EPM - called in main loop now, not necessary.
                    'hdmf_lite.do_playback
                GAME_HOW_HIGH_INIT:
                    SpriteClear
                    SelectMap(WORKING_MAP)
                    ClearMap(0)
                    DrawStatus
                    DrawString(5,26,@how_high,4)
                    repeat i from 0 to player_level[game_player_up]//4
                        DrawKong(13,21-i*4)
                        DrawKongHeight(8,24-i*4,i)
                    game_state := GAME_HOW_HIGH
                    game_state_count := 160
                    hdmf_lite.play_song(@_SONG_how_high,0)
                GAME_HOW_HIGH:
                    game_state_count--
                    if game_state_count == 0
                        game_state := GAME_LEVEL_1_INIT + player_level[game_player_up]//4
                    'EPM - called in main loop now, not necessary.        
                    'hdmf_lite.do_playback
                GAME_LEVEL_1_INIT:
                    InitLevelCommon
                    InitLevel1
                    background_song_ptr:=@_SONG_bg_girders_level
                    hdmf_lite.play_song(background_song_ptr,hdmf_lite#HDMF__FLAG_LOOP_SONG)
                GAME_LEVEL_2_INIT:
                    InitLevelCommon
                    InitLevel2
                    background_song_ptr:=@_SONG_bg_girders_level
                    hdmf_lite.play_song(background_song_ptr,hdmf_lite#HDMF__FLAG_LOOP_SONG)
                GAME_LEVEL_3_INIT:
                    InitLevelCommon
                    InitLevel3
                    background_song_ptr:=@_SONG_bg_girders_level
                    hdmf_lite.play_song(background_song_ptr,hdmf_lite#HDMF__FLAG_LOOP_SONG)
                GAME_LEVEL_4_INIT:
                    InitLevelCommon
                    InitLevel4
                    background_song_ptr:=@_SONG_bg_rivets_level
                    hdmf_lite.play_song(background_song_ptr,hdmf_lite#HDMF__FLAG_LOOP_SONG)
                GAME_LEVEL_1:
                    if BonusLogic
                        if !(rend_sprites.byte[SPRITE_FLAGS]&SPRITE_INVISIBLE)  'NEEDS FIXING!!!!!!!!!!!!!!!!!!!!!
                            MarioLogic
                        if mario_state<>MARIO_DEAD
                            KongLogic
                            'EPM - called in main loop now, not necessary.    
                            'hdmf_lite.do_playback
                            BarrelLogic
                            DrumLogic
                            PaulineLogic
                GAME_LEVEL_2:
                    if BonusLogic
                        if !(rend_sprites.byte[SPRITE_FLAGS]&SPRITE_INVISIBLE)
                            MarioLogic
                        if mario_state<>MARIO_DEAD
                            LiftLogic
                            KongLogic
                            BarrelLogic
                            SpringLogic
                            PaulineLogic
                            CollectableLogic
                GAME_LEVEL_3:
                    if BonusLogic
                        ConveyorLogic
                        if !(rend_sprites.byte[SPRITE_FLAGS]&SPRITE_INVISIBLE)
                            MarioLogic
                        if mario_state<>MARIO_DEAD
                            KongLogic
                            LadderLogic
                            PaulineLogic
                            BarrelLogic
                            DrumLogic
                            CollectableLogic
                GAME_LEVEL_4:
                    if BonusLogic
                        if !(rend_sprites.byte[SPRITE_FLAGS]&SPRITE_INVISIBLE)
                            MarioLogic
                        if mario_state<>MARIO_DEAD
                            KongLogic
                            PaulineLogic
                            BarrelLogic
                            RivetLogic
                            CollectableLogic
     
                GAME_LEVEL_12_END_SCENE_INIT:
                    game_state_count:=0
                    game_state:=GAME_LEVEL_12_END_SCENE
                    hdmf_lite.play_song(@_SONG_level1_complete,0)
                GAME_LEVEL_12_END_SCENE:
                    'EPM - called in main loop now, not necessary.
                    'hdmf_lite.do_playback
                    Level12EndScene
     
                GAME_LEVEL_3_END_SCENE_INIT:
                    game_state_count:=0
                    game_state:=GAME_LEVEL_3_END_SCENE
                    hdmf_lite.play_song(@_SONG_level1_complete,0)
                    hdmf_lite.SFX_stop_walk 'EPM
                GAME_LEVEL_3_END_SCENE:
                    Level3EndScene
     
                GAME_LEVEL_123_FOLLOW_ON:
                    'EPM - called in main loop now, not necessary.    
                    'hdmf_lite.do_playback
                    Level123FollowOn
     
                GAME_LEVEL_4_END_SCENE_INIT:
                    game_state_count:=0
                    game_state:=GAME_LEVEL_4_END_SCENE
                    'hdmf_lite.play_song(@_SONG_level1_complete,0)
                    hdmf_lite.SFX_stop_walk 'EPM
                GAME_LEVEL_4_END_SCENE:
                    Level4EndScene
     
                GAME_OVER_INIT:
                    repeat i from MOVING_BARREL_SPRITES to constant(MOVING_BARREL_SPRITES+9)
                        rend_sprites[i]:= $04_00_10_00
                    game_state_count:=0
                    DrawString(9,18,@game_over_string_1,0)
                    DrawString(9,19,@game_over_string_1,0)
                    DrawString(9,20,@game_over_string_2,0)
                    DrawString(9,21,@game_over_string_1,0)
                    DrawString(9,22,@game_over_string_1,0)
                    game_state:=GAME_OVER
                GAME_OVER:
                    if game_state_count++ == 192
                        game_state:=GAME_TITLE_INIT
     
        if gp & GP_START
            if startkey==0
                startkey:=1
                !pause
        else
            startkey:=0
            
        if game_state<GAME_LEVEL_1 or game_state>GAME_LEVEL_4 or mario_state=>MARIO_FALLING
            pause:=FALSE
        'total_time:=CNT-time
        'debugdec(@timetext,((cnt-time)/game_counter++))
        'debughex(@timetext,$89abcdef)
        'repeat 100000
        repeat until tv_status>0

PRI SpriteClear | i
    repeat i from 0 to constant(NUMBER_OF_SPRITES-1)
        rend_sprites[i]:= $04_00_10_00
    repeat i from 0 to constant(NUMBER_OF_BARRELS-1)
        barrel_state[i] := BARREL_STILL

PRI EndLevelSpriteClear | i
    repeat i from COLLECTABLE_SPRITES to 29
        rend_sprites[i]:= $04_00_10_00
    repeat i from HAMMER_SPRITES to BONUS_SPRITE_INDEX
        rend_sprites[i]:= $04_00_10_00
    repeat i from 0 to constant(NUMBER_OF_BARRELS-1)
        barrel_state[i] := BARREL_STILL
    repeat i from MOVING_BARREL_SPRITES to lookupz(-(player_level[game_player_up]==1):constant(MOVING_BARREL_SPRITES+9),constant(MOVING_BARREL_SPRITES+3))
        rend_sprites[i]:= $04_00_10_00
    repeat i from 13 to 17
        word[rend_map][i] := gfx#TILE_SPACE

PRI InitLevelCommon
    SpriteClear
    pauline_state := PAULINE_SCREAM_INIT
    frame := constant(SPRITE_INVISIBLE<<8)
    kong_state := KONG_LEVEL_START_POSE_INIT
    hammer_state_count:=0
    timer_state_count:=128
    timer := 5000 + player_level[game_player_up]//4*1000
    timer <#= 8000
    bonus_state:=BONUS_NONE
    mario_state:=MARIO_FACING_RIGHT
    old_mario_state := mario_state

CON
    DRAW_END_DRAWING            = 246
    DRAW_USE_TILE               = 247
    DRAW_VERTICALS              = 249
    DRAW_HORIZONTALS            = 251
    DRAW_PLATFORM_UPWARDS       = 252
    DRAW_PLATFORM_FLAT          = 253
    DRAW_PLATFORM_DOWNWARDS     = 254
    DRAW_RECTANGLE              = 255

PRI DrawFromData(p) | tilepos, i, j, start, end, tile, y, slope, x1, y1, x2, y2, opcode
    repeat while byte[p]<>DRAW_END_DRAWING
        opcode := byte[p++]
        case opcode
            DRAW_USE_TILE:
                tile:=byte[p++]
            DRAW_VERTICALS:
                repeat until byte[p]=>DRAW_END_DRAWING
                    tilepos:=(byte[p++]+2+(byte[p]<<5))<<1+gfx.map_adr
                    start:=byte[p++]
                    end:=byte[p++]
                    repeat i from start to end
                        byte[tilepos] := tile
                        tilepos+=64
            DRAW_HORIZONTALS:
                repeat until byte[p]=>DRAW_END_DRAWING
                    start:=byte[p++]
                    end:=byte[p++]
                    tilepos:=(start+2+(byte[p++]<<5))<<1 + gfx.map_adr
                    repeat i from start to end
                        byte[tilepos] := tile
                        tilepos+=2
            DRAW_RECTANGLE:
                x1:=byte[p++]
                y1:=byte[p++]
                x2:=byte[p++]
                y2:=byte[p++]
                repeat i from y1 to y2
                    repeat j from x1 to x2
                        tilepos:=(j+2+(i<<5))<<1 + gfx.map_adr
                        byte[tilepos] := byte[p]
                p++
            DRAW_PLATFORM_UPWARDS,DRAW_PLATFORM_FLAT,DRAW_PLATFORM_DOWNWARDS:
                start:=byte[p++]
                end:=byte[p++]
                y:=byte[p++]
                repeat i from start to end
                    DrawPlatformTile(start++,y)
                    if start//2==0
                        y+=opcode-DRAW_PLATFORM_FLAT

PRI InitLevel1 | i
    ' Barrels & Slopes
    ClearMap(0)
    DrawFromData(@level1def)
    DrawStatus
    DrawBonusBox
    kong_real_x := $3c
    kong_real_y := $33
    rend_mario_sprite                              := $04_00_3c_d7
    rend_sprites[FIXED_BARREL_SPRITES  ]           := $00_17_14_23
    rend_sprites[constant(FIXED_BARREL_SPRITES+1)] := $00_17_14_32
    rend_sprites[constant(FIXED_BARREL_SPRITES+2)] := $00_17_1e_23
    rend_sprites[constant(FIXED_BARREL_SPRITES+3)] := $00_17_1e_32
    rend_sprites[HAMMER_SPRITES]                   := $00_1b_25_4b
    rend_sprites[constant(HAMMER_SPRITES+1)]       := $00_1b_bc_a7
    rend_oil_drum_sprite                           := $00_42_28_d7
    repeat i from 0 to constant(NUMBER_OF_BARRELS-1)
        barrel_state[i] := BARREL_STILL
    game_state := GAME_LEVEL_1

DAT
level2sprites
        long  $04_00_10_c7,$00_39_58_68,$00_39_e4_48,$00_37_78_37
        long  $00_37_88_37,$00_3F_38_64,$00_3F_38_98,$00_3F_38_cc
        long  $00_3F_78_48,$00_3F_78_7c,$00_3F_78_b0,$00_40_38_47
        long  $02_40_38_de,$00_40_78_47,$02_40_78_de
        long  $04_00_10_00,$04_00_10_00
        long  $00_51_1c_67,$00_4f_5c_af,$00_50_e4_47

PRI InitLevel2 | i
    ' Springs
    ClearMap(0)
    DrawFromData(@level2def)
    DrawStatus
    DrawBonusBox
    kong_real_x := $3c
    kong_real_y := $37
    longmove(@rend_mario_sprite,@level2sprites,20)
    {
    rend_mario_sprite                                   := $04_00_10_c7
    rend_sprites[MOVING_BARREL_SPRITES  ]               := $00_39_58_68
    rend_sprites[constant(MOVING_BARREL_SPRITES+1)]     := $00_39_e4_48
    rend_sprites[SPRING_SPRITES]                        := $00_37_78_37
    rend_sprites[constant(SPRING_SPRITES+1)]            := $00_37_88_37
    rend_sprites[LIFT_SPRITES  ]                        := $00_3F_38_64
    rend_sprites[constant(LIFT_SPRITES+1)]              := $00_3F_38_98 '+$34
    rend_sprites[constant(LIFT_SPRITES+2)]              := $00_3F_38_cc
    rend_sprites[constant(LIFT_SPRITES+3)]              := $00_3F_78_48
    rend_sprites[constant(LIFT_SPRITES+4)]              := $00_3F_78_7c
    rend_sprites[constant(LIFT_SPRITES+5)]              := $00_3F_78_b0
    rend_sprites[WINCH_SPRITES  ]                       := $00_40_38_47
    rend_sprites[constant(WINCH_SPRITES+1)]             := $02_40_38_de
    rend_sprites[constant(WINCH_SPRITES+2)]             := $00_40_78_47
    rend_sprites[constant(WINCH_SPRITES+3)]             := $02_40_78_de
    rend_heart_umbrella_sprite                          := $00_51_1c_67 'umbrella
    rend_hat_sprite                                     := $00_4f_5c_af
    rend_handbag_sprite                                 := $00_50_e4_47
    }
    fireball_real_y[0]                   := $68
    fireball_real_y[1]                   := $48
    barrel_state[0]                      := FIREBALL_BOUNCE_LEFT_INIT
    barrel_state[1]                      := FIREBALL_BOUNCE_RIGHT_INIT
    spring_state_count[0]                :=0
    spring_state_count[1]                :=-100
    spring_real_x[0]                     :=0
    spring_real_x[1]                     :=0
    game_state := GAME_LEVEL_2

DAT
level3sprites
        long  $00_47_08_38,$01_47_e8_38,$01_47_6c_60,$00_47_84_60
        long  $00_47_08_b0,$01_47_e8_b0,$00_51_dc_87,$00_4f_54_87
        long  $00_50_8b_d7,$00_42_80_6f,$00_3C_81_5f,$00_1b_24_74
        long  $00_1b_7c_9b

PRI InitLevel3 | i
    ' Conveyors
    ClearMap(7)
    DrawFromData(@level3def)
    DrawStatus
    DrawBonusBox
    kong_real_x := $40
    kong_real_y := $37
    SetKongSprites
    rend_sprites[LADDER_SPRITES  ]           := $00_41_24_4f
    rend_sprites[constant(LADDER_SPRITES+1)] := $00_41_dc_4f
    rend_mario_sprite                        := $04_00_3c_d7
    longmove(@rend_sprites[MOTOR_SPRITES],@level3sprites,13)
{
    rend_sprites[MOTOR_SPRITES  ]            := $00_47_08_38
    rend_sprites[constant(MOTOR_SPRITES+1)]  := $01_47_e8_38
    rend_sprites[constant(MOTOR_SPRITES+2)]  := $01_47_6c_60
    rend_sprites[constant(MOTOR_SPRITES+3)]  := $00_47_84_60
    rend_sprites[constant(MOTOR_SPRITES+4)]  := $00_47_08_b0
    rend_sprites[constant(MOTOR_SPRITES+5)]  := $01_47_e8_b0
    rend_heart_umbrella_sprite               := $00_51_dc_87 'umbrella
    rend_hat_sprite                          := $00_4f_54_87
    rend_handbag_sprite                      := $00_50_8b_d7
    rend_oil_drum_sprite                     := $00_42_80_6f
    rend_oil_drum_flame_sprite               := $00_3C_81_5f
    rend_sprites[HAMMER_SPRITES]             := $00_1b_24_74
    rend_sprites[constant(HAMMER_SPRITES+1)] := $00_1b_7c_9b
}
    ladder_state[0] := LADDER_UP
    ladder_state[1] := LADDER_UP
    ladder_state_count[0] := ?rnd&$ff
    ladder_state_count[1] := ?rnd&$ff
    kong_conveyor_state := middle_left_conveyor_state := middle_right_conveyor_state := bottom_conveyor_state := CONVEYOR_RIGHT
    kong_conveyor_state_count:=0
    conveyor_state_count:=0
    drum_state:=DRUM_LIT_LEVEL3
    drum_state_count:=0
    game_state := GAME_LEVEL_3

PRI InitLevel4 | i
    ' Rivets
    ClearMap(8)
    DrawFromData(@level4def)
    DrawStatus
    DrawBonusBox
    kong_real_x := $80
    kong_real_y := $37
    rend_mario_sprite                        := $04_00_3c_d7
    rend_heart_umbrella_sprite               := $00_51_34_37 'umbrella
    rend_hat_sprite                          := $00_4f_dc_b0
    rend_handbag_sprite                      := $00_50_93_d7
    rend_sprites[HAMMER_SPRITES]             := $00_1b_1c_73
    rend_sprites[constant(HAMMER_SPRITES+1)] := $00_1b_7d_4c
    game_state := GAME_LEVEL_4
    remove_rivet_x := 0
    rivets_left:=8
    rivet_state_count:=30


PRI ClimbScene | remove
    if (game_state_count)//2 == 0
        case game_state_count
            0:
                ClearMap(0)
                DrawStatus
                DrawFromData(@climbdef0)
            64:
                kong_real_y := $d0
                kong_real_x := $8c
                kong_pose_source_ptr:=@kong_back_right_with_pauline
                SetKongSprites
            72..312:
                if game_state_count//16 == 0
                    kong_pose_source_ptr:=@kong_back_right_with_pauline
                elseif game_state_count//16 == 8
                    kong_pose_source_ptr:=@kong_back_left_with_pauline
                if game_state_count//16 == 0 or game_state_count//16 == 8
                    kong_real_y-=4
                    SetKongSprites
                    if game_state_count=>96
                        remove:=(kong_real_y>>3)+3
                        byte[@climbdef72remove]:=remove
                        DrawFromData(@climbdef72e)
                        'DrawHorizontal(14,16,remove,gfx#TILE_SPACE)
                        case remove
                            13,14:DrawFromData(@climbdef72a)
                            17,18:DrawFromData(@climbdef72b)
                            22:DrawFromData(@climbdef72c)
                            26:DrawFromData(@climbdef72d)
            346..389:
                kong_real_y-=lookupz((game_state_count-346)>>1: 3,3,3,3,3,3,3,2,2,2,2,2,2,1,1,1,1,0,0,-1,-1,-1)
                SetKongSprites
            390:
                SoundCrash
                kong_real_y++
                kong_pose_source_ptr:=@kong_center_sad_template
                SetKongSprites
                rend_sprites[PAULINE_SPRITES]             := $00_0f_70_17
                rend_sprites[constant(PAULINE_SPRITES+1)] := $00_10_70_17
                DrawFromData(@climbdef390)
            424..455:
                ClimbSceneKongJump(424)
            456:
                SoundCrash
                DrawFromData(@climbdef456)
            458..489:
                ClimbSceneKongJump(458)
            490:
                SoundCrash
                DrawFromData(@climbdef490)
            492..523:
                ClimbSceneKongJump(492)
            524:
                SoundCrash
                DrawFromData(@climbdef524)
            526..557:
                ClimbSceneKongJump(526)
            558:
                SoundCrash
                DrawFromData(@climbdef558)
            560..591:
                ClimbSceneKongJump(560)
            592:
                SoundCrash
                DrawFromData(@climbdef592)
            770:
                game_state := GAME_HOW_HIGH_INIT
    game_state_count++

PRI ClimbSceneKongJump(start_frame)
    kong_real_y-=lookupz((game_state_count-start_frame)>>1: 1,1,1,1,1,0,1,0,0,-1,0,-1,-1,-1,-1,-1)
    kong_real_x--
    SetKongSprites

PRI Level12EndScene
    case game_state_count
        0:
            EndLevelSpriteClear
            rend_mario_sprite                         := $00_00_94_17
            rend_sprites[constant(PAULINE_SPRITES+1)] := $00_12_70_17
            rend_heart_umbrella_sprite                := $00_52_82_1f
            kong_pose_source_ptr:=@kong_center_sad_template
            SetKongSprites
        32:
            kong_pose_source_ptr:=@kong_right_template
            SetKongSprites
            game_state:=GAME_LEVEL_123_FOLLOW_ON
    game_state_count++

PRI Level3EndScene
    if game_state_count==0
        EndLevelSpriteClear
        rend_sprites[constant(PAULINE_SPRITES+1)] := $00_12_70_17
        rend_heart_umbrella_sprite                := $00_52_82_1f
    ConveyorLogic
    KongLogic
    if kong_real_x==$5c
        game_state:=GAME_LEVEL_123_FOLLOW_ON
        game_state_count:=32

PRI Level123FollowOn
    case game_state_count
        64:
            kong_real_x := $5c
            kong_pose_source_ptr:=@kong_back_right
            SetKongSprites
        72..110:
            if game_state_count//16 == 0
                kong_real_y-=4
                kong_pose_source_ptr:=@kong_back_right
                SetKongSprites
            elseif game_state_count//16 == 8
                kong_real_y-=4
                kong_pose_source_ptr:=@kong_back_left
                SetKongSprites
        111:
            rend_sprites[PAULINE_SPRITES  ]           := $04_00_00_00
            rend_sprites[constant(PAULINE_SPRITES+1)] := $04_00_00_00
            rend_heart_umbrella_sprite                := $00_53_82_1f
        112..160:
            if game_state_count//16 == 0
                kong_real_y-=4
                kong_pose_source_ptr:=@kong_back_right_with_pauline
                SetKongSprites
            elseif game_state_count//16 == 8
                kong_real_y-=4
                kong_pose_source_ptr:=@kong_back_left_with_pauline
                SetKongSprites
        168:
            longfill(@rend_kong_sprites,$04000000,8)
            'kong_pose_source_ptr:=@kong_invisible
            'SetKongSprites
        256:
            AddToScore(timer)
        286:
            player_level[game_player_up]++
            game_state:=GAME_HOW_HIGH_INIT
    game_state_count++

PRI Level4EndScene | i
    case game_state_count
        0:
            EndLevelSpriteClear
            DrawFromData(@level4end1)
            rend_sprites[constant(PAULINE_SPRITES+1)] := $00_12_60_17
            hdmf_lite.play_song(@_SONG_rivet1_completion_1,hdmf_lite#HDMF__FLAG_LOOP_SONG)
        39:
            hdmf_lite.play_song(@_SONG_rivet1_completion_2,hdmf_lite#HDMF__FLAG_LOOP_SONG)
        40..160:
            if game_state_count//16==0
                kong_pose_source_ptr:=@kong_center_roar_l_template
                SetKongSprites
            elseif game_state_count//16==8
                kong_pose_source_ptr:=@kong_center_roar_r_template
                SetKongSprites
        161:
            hdmf_lite.stop_song
            kong_pose_source_ptr:=@kong_center_sad_template
            SetKongSprites
        192:
            hdmf_lite.SFX_start(hdmf_lite#SFX_STATE__DK_FALL)  'EPM
            kong_pose_source_ptr:=@kong_upsidedown_1
            SetKongSprites
        193..287:
            kong_real_y++
            SetKongSprites
        288:
            hdmf_lite.play_song(@_SONG_rivet1_completion_3,0)
            DrawFromData(@level4end2)
            rend_sprites[KONGS_STAR_SPRITE]           := $00_35_80_BF
            rend_sprites[PAULINE_SPRITES]             := $00_0f_60_37
            rend_sprites[constant(PAULINE_SPRITES+1)] := $00_12_60_37
        320:
            rend_mario_sprite                         := $00_00_84_37
        352:
            rend_heart_umbrella_sprite                := $00_52_72_37
        838:
            AddToScore(timer)
        868:
            player_level[game_player_up]++
            game_state:=GAME_HOW_HIGH_INIT

    if game_state_count=>288 and game_state_count=<736
        case game_state_count//24
            0:
                kong_pose_source_ptr:=@kong_upsidedown_2
                SetKongSprites
                rend_sprites[KONGS_STAR_SPRITE] ^= $01_00_00_00
            8:
                kong_pose_source_ptr:=@kong_upsidedown_3
                SetKongSprites
                rend_sprites[KONGS_STAR_SPRITE] ^= $01_00_00_00
            16:
                kong_pose_source_ptr:=@kong_upsidedown_4
                SetKongSprites
                rend_sprites[KONGS_STAR_SPRITE] ^= $01_00_00_00
    game_state_count++

PRI MarioLogic | current_height, platform_height, platform_height2, ladder_top, ladder_bottom, delta, i
    if frame & SPRITE_INVISIBLE<<8
        return
    current_height := rend_mario_sprite.byte[SPRITE_YPOS]
    platform_height := MarioLogicMarioPlatformHeight(current_height)

    'deal with girder inclines and falling
    case mario_state
        MARIO_WALK_LEFT, MARIO_WALK_RIGHT, MARIO_FACING_LEFT, MARIO_FACING_RIGHT:
            case platform_height-rend_mario_sprite.byte[SPRITE_YPOS]
                -1..-2: current_height := --rend_mario_sprite.byte[SPRITE_YPOS]
                        platform_height := MarioLogicMarioPlatformHeight(current_height)
                0:
                1..3:   current_height := ++rend_mario_sprite.byte[SPRITE_YPOS]
                        platform_height := MarioLogicMarioPlatformHeight(current_height)
                other:
                        mario_state_count:=0
                        mario_state:=MARIO_FALLING

    ladder_top := LadderTopHeight(rend_mario_sprite.byte[SPRITE_XPOS],current_height)
    ladder_bottom := LadderBottomHeight(rend_mario_sprite.byte[SPRITE_XPOS],current_height)

    ' decide on next state, and do any unique initialisations for that state
    if (mario_state=<MARIO_CLIMB_DOWN)
        'but only if we have user control
        if (gp&GP_A) and (gp&GP_LEFT) and not hammer_state_count
            mario_state:=MARIO_JUMP_LEFT
            frame := $00d
            mario_state_count:=0
            hdmf_lite.SFX_start(hdmf_lite#SFX_STATE__JUMP) 'EPM

        elseif (gp&GP_A) and (gp&GP_RIGHT) and not hammer_state_count
            mario_state:=MARIO_JUMP_RIGHT
            frame := $10d
            mario_state_count:=0
            hdmf_lite.SFX_start(hdmf_lite#SFX_STATE__JUMP) 'EPM

        elseif (gp & GP_A) and (mario_state==MARIO_FACING_LEFT) and not hammer_state_count
            mario_state:=MARIO_JUMP_UP_FACING_LEFT
            frame := $00d
            mario_state_count:=0
            hdmf_lite.SFX_start(hdmf_lite#SFX_STATE__JUMP) 'EPM

        elseif (gp & GP_A) and (mario_state==MARIO_FACING_RIGHT) and not hammer_state_count
            mario_state:=MARIO_JUMP_UP_FACING_RIGHT
            frame := $10d
            mario_state_count:=0
            hdmf_lite.SFX_start(hdmf_lite#SFX_STATE__JUMP) 'EPM

        elseif (gp & GP_LEFT) and (current_height==platform_height)
            rotate(@mario_state_count,1,0,12)
            if mario_state_count//3 <> 1
                rend_mario_sprite.byte[SPRITE_XPOS]--
 '               platform_height := PlatformHeight(rend_mario_sprite.byte[SPRITE_XPOS]+4,current_height)
 '               platform_height <#= PlatformHeight(rend_mario_sprite.byte[SPRITE_XPOS]-4,current_height)
            mario_state:=MARIO_WALK_LEFT

        elseif (gp & GP_RIGHT) and (current_height==platform_height)
            rotate(@mario_state_count,1,0,12)
            if mario_state_count//3 <> 2
                rend_mario_sprite.byte[SPRITE_XPOS]++
 '               platform_height := PlatformHeight(rend_mario_sprite.byte[SPRITE_XPOS]+4,current_height)
 '               platform_height <#= PlatformHeight(rend_mario_sprite.byte[SPRITE_XPOS]-4,current_height)
            mario_state:=MARIO_WALK_RIGHT

        elseif (gp & GP_UP) and (ladder_top<current_height) and not hammer_state_count
            MarioLogicUpDown(1,platform_height)
            rend_mario_sprite.byte[SPRITE_YPOS]<#=platform_height
            mario_state:=MARIO_CLIMB_UP

        elseif (gp & GP_DOWN) and (ladder_bottom>current_height) and not hammer_state_count
            MarioLogicUpDown(-1,platform_height)
            mario_state:=MARIO_CLIMB_DOWN

        elseif (mario_state==MARIO_WALK_LEFT)
            mario_state:=MARIO_FACING_LEFT

        elseif (mario_state==MARIO_WALK_RIGHT)
            mario_state:=MARIO_FACING_RIGHT

    'do the things that are common between states and that are done every game loop for a state
    if (mario_state==MARIO_CLIMB_DOWN) and (current_height==ladder_bottom-1)
        ' catch mario just before he goes below platform if he's on odd height.
        rend_mario_sprite.byte[SPRITE_YPOS]:=ladder_bottom
        current_height:=ladder_bottom
    case mario_state
        MARIO_CLIMB_UP,MARIO_CLIMB_DOWN :
            case(ladder_bottom-current_height)/2
                0..1 : frame := 6
                other:
                    delta := (current_height-ladder_top)>>1
                    case delta
                        -1000..-1: frame:=6 'negative number might happen with movable ladders?
                        0..1 : frame := 6
                        2..3 : frame := 3
                        4..5 : frame := 5
                        6..7 : frame := $104
                        other : frame := ((delta>>1)&$01)*$100+3
            rend_mario_sprite.byte[SPRITE_XPOS]&=$F8
            rend_mario_sprite.byte[SPRITE_XPOS]+=4
            if current_height==platform_height
                mario_state:=MARIO_FACING_LEFT                              'but, frame is facing away
                case game_state
                    GAME_LEVEL_1, GAME_LEVEL_2:
                        if current_height=<23
                            game_state:=GAME_LEVEL_12_END_SCENE_INIT
                            game_continue_life:=TRUE
                    GAME_LEVEL_3:
                        if current_height=<55
                            game_state:=GAME_LEVEL_3_END_SCENE_INIT
                            game_continue_life:=TRUE

        MARIO_WALK_LEFT, MARIO_WALK_RIGHT, MARIO_FACING_LEFT, MARIO_FACING_RIGHT:
            frame := lookupz(mario_state_count//12: 0,0,0,2,2,2,0,0,0,1,1,1)
            if hammer_state_count
                frame:=frame*2+7+((hammer_state_count>>3) & $01)
            if mario_state==MARIO_WALK_RIGHT or mario_state==MARIO_FACING_RIGHT
                frame |= SPRITE_MIRRORED<<8

        MARIO_FALLING:
            if rend_mario_sprite.byte[SPRITE_YPOS]<platform_height
                rend_mario_sprite.byte[SPRITE_YPOS]+=lookupz(mario_state_count++: -2,-2,-2,-2,-2,-2,-1,-2,-1,-2,-1,-2,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,0,-1,0,-1)+2
            else
                mario_state:=MARIO_DEAD_INIT

        MARIO_JUMP_LEFT:
            if mario_state_count//2==0
                rend_mario_sprite.byte[SPRITE_XPOS]--
                platform_height := MarioLogicMarioPlatformHeight(current_height)
                if game_state==GAME_LEVEL_2
                    case rend_mario_sprite.byte[SPRITE_XPOS]
                        $3f:
                            repeat i from LIFT_SPRITES to constant(LIFT_SPRITES+2)
                                if rend_mario_sprite.byte[SPRITE_YPOS]>byte[@rend_sprites[i]][SPRITE_YPOS] and rend_mario_sprite.byte[SPRITE_YPOS]<byte[@rend_sprites[i]][SPRITE_YPOS]+24
                                    rend_mario_sprite.byte[SPRITE_XPOS]++
                                    mario_state_count:=0
                                    mario_state:=MARIO_FALLING
                        $7f:
                            repeat i from constant(LIFT_SPRITES+3) to constant(LIFT_SPRITES+5)
                                if rend_mario_sprite.byte[SPRITE_YPOS]>byte[@rend_sprites[i]][SPRITE_YPOS] and rend_mario_sprite.byte[SPRITE_YPOS]<byte[@rend_sprites[i]][SPRITE_YPOS]+24
                                    rend_mario_sprite.byte[SPRITE_XPOS]++
                                    mario_state_count:=0
                                    mario_state:=MARIO_FALLING
            if mario_state==MARIO_JUMP_LEFT
                MarioLogicJump(MARIO_LANDING_LEFT,$00e,current_height,platform_height)

        MARIO_JUMP_RIGHT:
            if mario_state_count//2==0
                rend_mario_sprite.byte[SPRITE_XPOS]++
                platform_height := MarioLogicMarioPlatformHeight(current_height)
                if game_state==GAME_LEVEL_2
                    case rend_mario_sprite.byte[SPRITE_XPOS]
                        $30:
                            repeat i from LIFT_SPRITES to constant(LIFT_SPRITES+2)
                                if rend_mario_sprite.byte[SPRITE_YPOS]>byte[@rend_sprites[i]][SPRITE_YPOS] and rend_mario_sprite.byte[SPRITE_YPOS]<byte[@rend_sprites[i]][SPRITE_YPOS]+24
                                    rend_mario_sprite.byte[SPRITE_XPOS]--
                                    mario_state_count:=0
                                    mario_state:=MARIO_FALLING
                        $70:
                            repeat i from constant(LIFT_SPRITES+3) to constant(LIFT_SPRITES+5)
                                if rend_mario_sprite.byte[SPRITE_YPOS]>byte[@rend_sprites[i]][SPRITE_YPOS] and rend_mario_sprite.byte[SPRITE_YPOS]<byte[@rend_sprites[i]][SPRITE_YPOS]+24
                                    rend_mario_sprite.byte[SPRITE_XPOS]--
                                    mario_state_count:=0
                                    mario_state:=MARIO_FALLING
            if mario_state==MARIO_JUMP_RIGHT
                MarioLogicJump(MARIO_LANDING_RIGHT,$10e,current_height,platform_height)

        MARIO_JUMP_UP_FACING_LEFT:
            MarioLogicJump(MARIO_LANDING_LEFT,$00e,current_height,platform_height)

        MARIO_JUMP_UP_FACING_RIGHT:
            MarioLogicJump(MARIO_LANDING_RIGHT,$10e,current_height,platform_height)

        MARIO_LANDING_LEFT:
            MarioLogicLanding($000,MARIO_FACING_LEFT)
        MARIO_LANDING_RIGHT:
            MarioLogicLanding($100,MARIO_FACING_RIGHT)
        MARIO_DEAD_INIT:
            mario_state_count := 256
            mario_state := MARIO_DEAD
            hdmf_lite.stop_song

        MARIO_DEAD:
            case mario_state_count
                321:
                    EndLevelSpriteClear
                    hdmf_lite.play_song(@_SONG_death,0)
                322,354,386,418: frame:=$54
                330,362,394: frame:=$155
                338,370,402: frame:=$254
                346,378,410: frame:=$55
                426: frame:=$156
                554: game_state:= GAME_DISPLAY_PLAYER_INIT
                     game_continue_life:=FALSE
            ++mario_state_count

    'check for hammer collisions
    repeat i from HAMMER_SPRITES to constant(HAMMER_SPRITES+NUMBER_OF_HAMMERS-1)
        if not hammer_state_count and MarioCollision(i)
            hammer_state_count:=831
            action_hammer_ptr:=@rend_sprites[i]
            hdmf_lite.play_song(@_SONG_hammer,hdmf_lite#HDMF__FLAG_LOOP_SONG)

    if hammer_state_count
        long[rend_sprite_palette_ptr][57]:=$07ec68
        long[rend_sprite_palette_ptr][58]:=$07ec68
        if hammer_state_count<358
            long[rend_sprite_palette_ptr][28]:=$055b7d 'yellow hammer
        else
            long[rend_sprite_palette_ptr][28]:=$055b5b 'red hammer   
        if ((hammer_state_count>>3) & $01)==0 or mario_state=>MARIO_JUMP_LEFT
            byte[action_hammer_ptr][SPRITE_YPOS]:=rend_mario_sprite.byte[SPRITE_YPOS]-16
            byte[action_hammer_ptr][SPRITE_XPOS]:=rend_mario_sprite.byte[SPRITE_XPOS]
            word[action_hammer_ptr][SPRITE_FLAGS_AND_TILE_WORD]:=$1b
        else
            byte[action_hammer_ptr][SPRITE_YPOS]:=rend_mario_sprite.byte[SPRITE_YPOS]
            if mario_state==MARIO_FACING_LEFT or mario_state==MARIO_WALK_LEFT
                byte[action_hammer_ptr][SPRITE_XPOS]:=rend_mario_sprite.byte[SPRITE_XPOS]-16
                word[action_hammer_ptr][SPRITE_FLAGS_AND_TILE_WORD]:=$1c
            else
                byte[action_hammer_ptr][SPRITE_XPOS]:=rend_mario_sprite.byte[SPRITE_XPOS]+16
                word[action_hammer_ptr][SPRITE_FLAGS_AND_TILE_WORD]:=$11c
        hammer_state_count--
        if hammer_state_count==0
            byte[action_hammer_ptr][SPRITE_FLAGS]:=SPRITE_INVISIBLE
            action_hammer_ptr:=0
            hdmf_lite.play_song(background_song_ptr,hdmf_lite#HDMF__FLAG_LOOP_SONG)

        rend_mario_sprite.byte[SPRITE_XPOS]#>=25
        rend_mario_sprite.byte[SPRITE_XPOS]<#=231
    else
        long[rend_sprite_palette_ptr][57]:=$076ce8
        long[rend_sprite_palette_ptr][58]:=$076ce8

    rend_mario_sprite.byte[SPRITE_XPOS]#>=20
    rend_mario_sprite.byte[SPRITE_XPOS]<#=236
    rend_mario_sprite.word[SPRITE_FLAGS_AND_TILE_WORD]:=frame

    'EPM: Handle walking SFX on/off
    'If Mario was walking
    if (old_mario_state == MARIO_WALK_LEFT) or  (old_mario_state == MARIO_WALK_RIGHT)
      'If Mario is not walking anymore
      if not((mario_state == MARIO_WALK_LEFT) or  (mario_state == MARIO_WALK_RIGHT))
        'Stop walk sound if it is currently playing (could have stopped because
        '   some other sound, like jump, was started instead)
        hdmf_lite.SFX_stop_walk
    else
      'If Mario is walking now
      if (mario_state == MARIO_WALK_LEFT) or  (mario_state == MARIO_WALK_RIGHT)
        'Start walk sound
        hdmf_lite.SFX_start_walk   
    old_mario_state := mario_state

PRI MarioLogicJump(landing_state,landing_frame,current_height,platform_height)  | i, diff
    'we lookup the y delta for each frame.  By offsetting by one, we ensure that overflow means one will be subtracted.
    rend_mario_sprite.byte[SPRITE_YPOS]-=lookupz(mario_state_count: 3,2,2,2,2,2,2,2,1,2,2,1,2,1,2,1,1,1,2,1,1,1,0,1,1,1,0,1,0,1,0,0,1)-1
    if mario_state_count++ == 21
        hurdle_count :=0
        repeat i from 0 to constant(NUMBER_OF_BARRELS-1)
            if barrel_hurdled[i]~
                ++hurdle_count
        if hurdle_count>0
            rend_bonus_sprite.word[SPRITE_LOCATION_WORD] := rend_mario_sprite.word[SPRITE_LOCATION_WORD]
            bonus_state:=BONUS_BARREL_HURDLED_INIT

    diff:=current_height-platform_height
    if diff=>0 and diff =<1 and mario_state_count>10 'At least x frames into the jump to avoid latching on to rising lifts
        mario_state:=landing_state
        frame:=landing_frame
        mario_state_count:=0
        rend_mario_sprite.byte[SPRITE_YPOS]:=platform_height
    elseif mario_state_count>59
        mario_state_count:=0
        mario_state:=MARIO_FALLING
    elseif rend_mario_sprite.byte[SPRITE_XPOS]<20
        mario_state:=MARIO_JUMP_RIGHT
        frame := $10d
    elseif rend_mario_sprite.byte[SPRITE_XPOS]>236
        mario_state:=MARIO_JUMP_LEFT
        frame := $00d

PRI MarioLogicUpDown(delta,platform_height)
    if (mario_state<>MARIO_CLIMB_UP) and (mario_state<>MARIO_CLIMB_DOWN)
        mario_state_count:=0
    else
        mario_state_count+=delta
    if mario_state_count//5==0
        rend_mario_sprite.byte[SPRITE_YPOS]-=2*delta

PRI MarioLogicLanding(new_frame,new_state)
    'deal with lifts
    rend_mario_sprite.byte[SPRITE_YPOS] := MarioLogicMarioPlatformHeight(rend_mario_sprite.byte[SPRITE_YPOS])
    if mario_state_count==4
        mario_state:=new_state
        frame := new_frame
    else
        mario_state_count++

PRI MarioLogicMarioPlatformHeight(current_height) | platform_height,platform_height2
    platform_height := PlatformHeight(rend_mario_sprite.byte[SPRITE_XPOS]+3,current_height)
    platform_height2 := PlatformHeight(rend_mario_sprite.byte[SPRITE_XPOS]-3,current_height)
    if platform_height2<platform_height
        platform_height:=platform_height2
    return platform_height

PRI KongLogic | i
    'debugdec(@kongstatetext,kong_state)
    kong_state_count++
    case kong_state
        KONG_LEVEL_START_POSE_INIT:
            kong_barrel_releases:=0
            kong_state_count := 0
            kong_pose_source_ptr:=@kong_center_sad_template
            SetKongSprites
            kong_state := KONG_LEVEL_START_POSE

        KONG_LEVEL_START_POSE:
            if kong_state_count == 64
                mario_state:=MARIO_WALK_RIGHT
                mario_state_count := 0
                frame := constant(SPRITE_MIRRORED<<8)
                if not game_continue_life~~
                    --player_lives[game_player_up]
                    DrawLives
                if player_level[game_player_up]//4
                    kong_state := KONG_NORMAL_INIT
                else
                    kong_state := KONG_BR_LEFT_INIT

        KONG_BR_LEFT_INIT:
            ' see if there's a barrel available.
            kong_holding_barrel := -1
            i := 0
            repeat while i<NUMBER_OF_BARRELS and kong_holding_barrel==-1
                if rend_sprites[MOVING_BARREL_SPRITES+i] & constant(SPRITE_INVISIBLE<<24)
                    kong_holding_barrel := i
                i++
            if kong_holding_barrel => 0
                barrel_state[kong_holding_barrel] := BARREL_STILL
                kong_state_count := 0
                kong_pose_source_ptr := @kong_left_template
                SetKongSprites
                kong_state := KONG_BR_LEFT
            'else stay in this state

        KONG_BR_CENTER_1_INIT:
            ' keep barrel type
            'debugdec(@knongbarrelreleasestext,kong_barrel_releases)
            'repeat 1000000
            if kong_barrel_releases&$7 == 0
                rend_sprites[MOVING_BARREL_SPRITES+kong_holding_barrel] := $00_19_3c_32
            else
                rend_sprites[MOVING_BARREL_SPRITES+kong_holding_barrel] := $00_15_3c_32
            fireball_real_y[kong_holding_barrel]:=$32
            if kong_barrel_releases==0
                barrel_state[kong_holding_barrel] := BARREL_FALLING_DOWN
                kong_barrel_releases++
                kong_state := KONG_BR_CENTER_2_INIT
            else
                kong_state := KONG_BR_CENTER_1
            kong_state_count := 0
            kong_pose_source_ptr := @kong_center_barrel_template
            SetKongSprites

        KONG_BR_RIGHT_INIT:
            ' keep barrel type
            rend_sprites[MOVING_BARREL_SPRITES+kong_holding_barrel] -= $00_01_1E_00
            ' and add on x delta
            rend_sprites[MOVING_BARREL_SPRITES+kong_holding_barrel] += $00_00_3C_00
            kong_state_count := 0
            kong_pose_source_ptr := @kong_right_template
            SetKongSprites
            kong_state := KONG_BR_RIGHT

        KONG_BR_CENTER_2_INIT:
            'debugdec(@kongbarreltext, kong_holding_barrel)
            kong_state_count := 0
            kong_pose_source_ptr := @kong_center_sad_template
            SetKongSprites
            kong_state := KONG_BR_CENTER_2

        KONG_BR_GROWL_INIT:
            kong_state_count := 0
            kong_pose_source_ptr := @kong_center_roar_r_template
            SetKongSprites
            kong_state := KONG_BR_GROWL

        KONG_BR_LEFT:
            'debugdec(@kongstatecounttext,kong_state_count)
            if kong_state_count == 24
                kong_state := KONG_BR_CENTER_1_INIT

        KONG_BR_CENTER_1:
            if kong_state_count == 24
                kong_state := KONG_BR_RIGHT_INIT

        KONG_BR_RIGHT:
            if kong_state_count == 24
                kong_state := KONG_BR_CENTER_2_INIT
                barrel_state[kong_holding_barrel]:=BARREL_ROLLING_RIGHT
                kong_holding_barrel := -3
                kong_barrel_releases++

        KONG_BR_CENTER_2:
            if (?rnd & $0000003F)==0
                kong_state := KONG_BR_GROWL_INIT
            elseif kong_state_count == 24
                kong_state := KONG_BR_LEFT_INIT

        KONG_BR_GROWL:
            if kong_state_count>24
                if (?rnd&$0000001F)==0
                    kong_state := KONG_BR_LEFT_INIT

        KONG_NORMAL_INIT:
            kong_state_count:=0
            kong_pose_source_ptr := @kong_center_roar_r_template
            SetKongSprites
            kong_state := KONG_NORMAL

        KONG_NORMAL:
            case kong_state_count
                32,96:
                    kong_pose_source_ptr := @kong_center_roar_l_template
                    SetKongSprites
                64:
                    kong_pose_source_ptr := @kong_center_roar_r_template
                    SetKongSprites
                128:
                    kong_pose_source_ptr := @kong_center_sad_template
                    SetKongSprites
                256:
                    kong_state := KONG_NORMAL_INIT

PRI BarrelLogic | i, this_barrel, platform_height, height_to_fall, anim, ladder_top_height, diff
    'mario := @rend_sprites[MARIO_SPRITE]
    repeat i from 0 to constant(NUMBER_OF_BARRELS-1)
        if barrel_state[i]==BARREL_STILL
            next
        this_barrel := @rend_sprites[MOVING_BARREL_SPRITES+i]

        if HammerCollision(MOVING_BARREL_SPRITES+i)
            byte[this_barrel][SPRITE_FLAGS]:=SPRITE_INVISIBLE
            barrel_state[i]:=BARREL_STILL
            rend_bonus_sprite.word[SPRITE_LOCATION_WORD] := word[this_barrel][SPRITE_LOCATION_WORD]
            bonus_state:=BONUS_BARREL_BUSTED_INIT
            hdmf_lite.SFX_start(hdmf_lite#SFX_STATE__SMASH_BARREL) 'EPM

        platform_height := PlatformHeight(byte[this_barrel][SPRITE_XPOS],fireball_real_y[i])

        if MarioCollision(MOVING_BARREL_SPRITES+i)
            mario_state:=MARIO_DEAD_INIT
        else
            case mario_state
                MARIO_JUMP_LEFT,MARIO_JUMP_RIGHT,MARIO_JUMP_UP_FACING_LEFT,MARIO_JUMP_UP_FACING_RIGHT:
                    if not (byte[this_barrel][SPRITE_FLAGS]&SPRITE_INVISIBLE)
                        diff:=||(rend_mario_sprite.byte[SPRITE_XPOS]-byte[this_barrel][SPRITE_XPOS])
                        if diff<2 and byte[this_barrel][SPRITE_YPOS]==PlatformHeight(rend_mario_sprite.byte[SPRITE_XPOS],rend_mario_sprite.byte[SPRITE_YPOS]) and mario_state_count<21
                            barrel_hurdled[i]:=TRUE

        'check for barrel dissapearing into oil drum
        if byte[this_barrel][SPRITE_XPOS]==$28 and fireball_real_y[i]>200 and barrel_state[i]<>BARREL_STILL and barrel_state[i]<FIREBALL_JUMP_RIGHT_INIT
            if byte[this_barrel][SPRITE_TILE]==$18 and drum_state<>DRUM_FLARING
                drum_state := DRUM_FLARING_INIT
                barrel_state[i]:=FIREBALL_WAITING
                barrel_state_count[i]:=62
            else
                byte[this_barrel][SPRITE_FLAGS]:=SPRITE_INVISIBLE
                barrel_state[i]:=BARREL_STILL
        case barrel_state[i]
            BARREL_ROLLING_RIGHT:
                BarrelLogicBarrelRolling(1,platform_height,this_barrel,i,BARREL_FALLING_RIGHT,BARREL_ON_LADDER_RIGHT)
            BARREL_ROLLING_LEFT:
                BarrelLogicBarrelRolling(-1,platform_height,this_barrel,i,BARREL_FALLING_LEFT,BARREL_ON_LADDER_LEFT)
            BARREL_FALLING_RIGHT:
                BarrelLogicBarrelFalling(1,BARREL_BOUNCING_LEFT,platform_height,this_barrel,i)
            BARREL_FALLING_LEFT:
                BarrelLogicBarrelFalling(-1,BARREL_BOUNCING_RIGHT,platform_height,this_barrel,i)
            BARREL_BOUNCING_RIGHT:
                BarrelLogicBarrelBouncing(1,BARREL_ROLLING_RIGHT,this_barrel,i)
            BARREL_BOUNCING_LEFT:
                BarrelLogicBarrelBouncing(-1,BARREL_ROLLING_LEFT,this_barrel,i)
            BARREL_ON_LADDER_RIGHT:
                BarrelLogicBarrelOnLadder(BARREL_ROLLING_LEFT,this_barrel,i)
            BARREL_ON_LADDER_LEFT:
                BarrelLogicBarrelOnLadder(BARREL_ROLLING_RIGHT,this_barrel,i)
            BARREL_FALLING_DOWN:
                height_to_fall := platform_height-fireball_real_y[i]
                'DrawNumber(0,8,height_to_fall,8,4)
                'DrawNumber(0,9,platform_height,8,4)
                'DrawNumber(0,10,fireball_real_y[i],8,4)
                if height_to_fall=>-1 and height_to_fall=<1 and barrel_state_count[i]>10
                    barrel_state_count[i]:=0
                    if platform_height>200
                        fireball_real_y[i]--
                        byte[this_barrel][SPRITE_TILE] := $18
                        barrel_state[i]:=BARREL_BOUNCING_LEFT
                else
                    ' offset by 2 so that any additional falls will be 2
                    fireball_real_y[i]+=lookupz(barrel_state_count[i]: -2,-2,-2,-2,-2,-2,-1,-2,-1,-2,-1,-2,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,-1,0,-1,0,-1)+2
                    barrel_state_count[i]++
                rotate(@barrel_anim[i],1,0,7)
            FIREBALL_WAITING:
                if barrel_state_count[i]-- == 0
                    barrel_state[i]:=FIREBALL_JUMP_RIGHT_INIT
            FIREBALL_JUMP_RIGHT_INIT:
                BarrelLogicFireballJumpInit(this_barrel,i)
                word[this_barrel][SPRITE_FLAGS_AND_TILE_WORD] := $00_39
                barrel_state[i] := FIREBALL_JUMP_RIGHT
            FIREBALL_JUMP_LEFT_INIT:
                BarrelLogicFireballJumpInit(this_barrel,i)
                word[this_barrel][SPRITE_FLAGS_AND_TILE_WORD] := $01_39
                barrel_state[i] := FIREBALL_JUMP_LEFT
            FIREBALL_JUMP_RIGHT:
                BarrelLogicFireballJump(1,FIREBALL_BOUNCE_RIGHT_INIT,this_barrel,i)
            FIREBALL_JUMP_LEFT:
                BarrelLogicFireballJump(-1,FIREBALL_BOUNCE_LEFT_INIT,this_barrel,i)
            FIREBALL_BOUNCE_LEFT_INIT:
                case ?rnd&$7
                    0..4: barrel_state[i]:=FIREBALL_BOUNCE_LEFT
                    5,6: barrel_state[i]:=FIREBALL_BOUNCE_RIGHT
                    7: barrel_state[i]:=FIREBALL_BOUNCE_HERE
                barrel_state_count[i]:=0
            FIREBALL_BOUNCE_RIGHT_INIT:
                case ?rnd&$7
                    0..4: barrel_state[i]:=FIREBALL_BOUNCE_RIGHT
                    5,6: barrel_state[i]:=FIREBALL_BOUNCE_LEFT
                    7: barrel_state[i]:=FIREBALL_BOUNCE_HERE
                barrel_state_count[i]:=0
            FIREBALL_BOUNCE_HERE_INIT:
                case ?rnd&$7
                    0..3: barrel_state[i]:=FIREBALL_BOUNCE_HERE
                    4,5: barrel_state[i]:=FIREBALL_BOUNCE_LEFT
                    6,7: barrel_state[i]:=FIREBALL_BOUNCE_RIGHT
                barrel_state_count[i]:=0
            FIREBALL_BOUNCE_RIGHT:
                BarrelLogicFireballBounce(1,$13a,$139,FIREBALL_BOUNCE_LEFT,FIREBALL_BOUNCE_RIGHT_INIT,this_barrel,i)
            FIREBALL_BOUNCE_LEFT:
                BarrelLogicFireballBounce(-1,$03a,$039,FIREBALL_BOUNCE_RIGHT,FIREBALL_BOUNCE_LEFT_INIT,this_barrel,i)
            FIREBALL_BOUNCE_HERE:
                BarrelLogicFireballBounce(0,$03a,$039,FIREBALL_BOUNCE_LEFT,FIREBALL_BOUNCE_HERE_INIT,this_barrel,i)
            FIREBALL_UP_LADDER:
                ++barrel_state_count[i]
                if barrel_state_count[i]>35
                    barrel_state_count[i]:=0
                BarrelLogicFireballOnLadder(-1,FIREBALL_BOUNCE_LEFT_INIT,platform_height,this_barrel,i)
            FIREBALL_DOWN_LADDER:
                --barrel_state_count[i]
                if barrel_state_count[i]<0
                    barrel_state_count[i]:=35
                BarrelLogicFireballOnLadder(1,FIREBALL_BOUNCE_RIGHT_INIT,platform_height,this_barrel,i)
        case barrel_state[i]
            BARREL_ROLLING_RIGHT,BARREL_ROLLING_LEFT,BARREL_FALLING_RIGHT,BARREL_FALLING_LEFT,BARREL_BOUNCING_RIGHT,BARREL_BOUNCING_LEFT:
                byte[this_barrel][SPRITE_FLAGS]:=lookupz(barrel_anim[i]>>3:%00,%10,%11,%01)
            BARREL_ON_LADDER_RIGHT,BARREL_ON_LADDER_LEFT,BARREL_FALLING_DOWN:
                if barrel_anim[i]==0
                    byte[this_barrel][SPRITE_TILE]^=%00000011
        if barrel_state[i]=>FIREBALL_JUMP_LEFT_INIT 'any Fireball state
            byte[this_barrel][SPRITE_YPOS] := fireball_real_y[i] - lookupz(barrel_state_count[i]/2: 0,1,1,2,2,2,2,2,2,2,2,2,2,1,1,0,1,0)
        else
            byte[this_barrel][SPRITE_YPOS] := fireball_real_y[i]

PRI BarrelLogicBarrelRolling(delta,platform_height,this_barrel,i,next_falling_state,next_ladder_state)
    if platform_height-fireball_real_y[i] > 3
        barrel_state_count[i]:=0
        barrel_state[i] := next_falling_state
    else
        byte[this_barrel][SPRITE_XPOS]+=delta
        fireball_real_y[i]:=platform_height
    if byte[this_barrel][SPRITE_XPOS]&$07==4
        'check for ladders only when centred over tiles
        if BarrelLadderBottomHeight(byte[this_barrel][SPRITE_XPOS],fireball_real_y[i])>platform_height
            'go down ladder randomly or brown barrel below mario
            if ?rnd//8 == 0 or (byte[this_barrel][SPRITE_TILE]<>$18 and fireball_real_y[i]>PlatformHeight(rend_mario_sprite.byte[SPRITE_XPOS],rend_mario_sprite.byte[SPRITE_YPOS]))
                fireball_real_y[i]++
                byte[this_barrel][SPRITE_TILE]++
                barrel_state[i] := next_ladder_state
    rotate(@barrel_anim[i],delta,0,31)

PRI BarrelLogicBarrelFalling(delta,next_bouncing_state,platform_height,this_barrel,i) | height_to_fall
    height_to_fall := platform_height-fireball_real_y[i]
    'debugdec(@heighttofalltext,height_to_fall)
    if height_to_fall < 0
        barrel_state_count[i]:=0
        barrel_state[i] := next_bouncing_state
    else
        ' offset by 2 so that any additional falls will be 2
        fireball_real_y[i]+=lookupz(barrel_state_count[i]: -2,-2,-2,-2,-2,-2,-2,-2,-2,-1,-2,-2,-1,-1,-2,-1,-1,-1,-1,-2,0,-1,-1,-1,-1,0,-1,-1,0,-1)+2
        byte[this_barrel][SPRITE_XPOS]+=lookupz(barrel_state_count[i]:  1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0,0, 1, 0, 1, 0,0, 1, 0,0, 1)*delta
        barrel_state_count[i]++
    rotate(@barrel_anim[i],delta,0,31)

PRI BarrelLogicBarrelBouncing(delta,next_rolling_state,this_barrel,i)
    if barrel_state_count[i] =< 22
        fireball_real_y[i]-=lookupz(barrel_state_count[i]: 1,0,0,1,0,0,0,0,0,-1,0,0,-1,0,0,-1,0,1,0,0,0,0,-1)
        barrel_state_count[i]++
        byte[this_barrel][SPRITE_XPOS]+=delta
    else
        barrel_state[i]:=next_rolling_state
    rotate(@barrel_anim[i],delta,0,31)

PRI BarrelLogicBarrelOnLadder(next_state,this_barrel,i)
    if fireball_real_y[i] < BarrelLadderBottomHeight(byte[this_barrel][SPRITE_XPOS],fireball_real_y[i])
        fireball_real_y[i]++
    else
        byte[this_barrel][SPRITE_TILE]&=%11111100
        barrel_state[i] := next_state
    rotate(@barrel_anim[i],1,0,7)

PRI BarrelLogicFireballJumpInit(this_barrel,i)
    fireball_real_y[i]:=rend_oil_drum_sprite.byte[SPRITE_YPOS]-6
    byte[this_barrel][SPRITE_XPOS]:=rend_oil_drum_sprite.byte[SPRITE_XPOS]
    barrel_state_count[i]:=0

PRI BarrelLogicFireballJump(delta,next_state,this_barrel,i)
    if barrel_state_count[i]//2 == 0
        fireball_real_y[i]+=lookupz(barrel_state_count[i] >>1: -2,-2,-2,-1,-1,-1,-1,-1,-1,0,-1,0,0,0,0,0,1,0,1,1,1,1,1,1,1,1,2,2,2,2,2)
        byte[this_barrel][SPRITE_XPOS]+=delta
    barrel_state_count[i]++
    if fireball_real_y[i]=>PlatformHeight(byte[this_barrel][SPRITE_XPOS],fireball_real_y[i])
        barrel_state[i] := next_state

PRI BarrelLogicFireballBounce(delta,frame1,frame2,next_reverse_state,next_continue_state,this_barrel,i) | platform_height, anim, ladder_top_height
    platform_height := PlatformHeight(byte[this_barrel][SPRITE_XPOS],fireball_real_y[i])
    anim:=barrel_state_count[i]++ //16
    if anim==0
        word[this_barrel][SPRITE_FLAGS_AND_TILE_WORD]:=frame1
    elseif anim==8
        word[this_barrel][SPRITE_FLAGS_AND_TILE_WORD]:=frame2
    if anim//2
        byte[this_barrel][SPRITE_XPOS]+=delta
        platform_height := PlatformHeight(byte[this_barrel][SPRITE_XPOS],fireball_real_y[i])
        if platform_height-fireball_real_y[i]>3
            byte[this_barrel][SPRITE_XPOS]-=delta
            barrel_state[i] := next_reverse_state
        else
            fireball_real_y[i]:=platform_height
    if barrel_state_count[i]==35
        barrel_state[i]:=next_continue_state
    if byte[this_barrel][SPRITE_XPOS]&$07==4
        'check for ladders only when centred over tiles
        if BarrelLadderBottomHeight(byte[this_barrel][SPRITE_XPOS],fireball_real_y[i])>platform_height
            if ?rnd//3 == 0
                barrel_state_count[i]:=0
                barrel_state[i] := FIREBALL_DOWN_LADDER
        ladder_top_height:=LadderTopHeight(byte[this_barrel][SPRITE_XPOS],fireball_real_y[i])
        if ladder_top_height<platform_height and ladder_top_height>23
            'There is a ladder above, but it isn't Pauline's
            if ?rnd//3 == 0
                barrel_state_count[i]:=0
                barrel_state[i] := FIREBALL_UP_LADDER

PRI BarrelLogicFireballOnLadder(delta,next_state,platform_height,this_barrel,i) | anim
    anim:=barrel_state_count[i]//16
    if anim==0
        byte[this_barrel][SPRITE_TILE]:=$3a
    elseif anim==8
        byte[this_barrel][SPRITE_TILE]:=$39
    if barrel_state_count[i] //4 ==0
        fireball_real_y[i]+=delta
        if fireball_real_y[i] == platform_height
            barrel_state[i]:=next_state

PRI DrumLogic | found,i
    case drum_state
        DRUM_FLARING_INIT:
            drum_state_count:=62
            drum_flicker_count:=?rnd&$b + 4
            rend_oil_drum_flame_sprite := $00_3D_28_c7
            drum_state := DRUM_FLARING
        DRUM_FLARING:
            if drum_flicker_count-- == 0
                rend_oil_drum_flame_sprite ^= $00_03_00_00 'swap between $3d and 3E
                drum_flicker_count:=?rnd&$b + 4
            'debugdec(@barrelstatetext,drum_state_count)
            if drum_state_count-- == 0
                drum_state := DRUM_LIT_INIT
        DRUM_LIT_INIT:
            drum_flicker_count:=?rnd&$b + 4
            rend_oil_drum_flame_sprite := $00_3C_29_c7
            drum_state := DRUM_LIT
        DRUM_LIT:
            if drum_flicker_count-- == 0
                rend_oil_drum_flame_sprite ^= $00_07_00_00 'swap between $3b and 3c
                drum_flicker_count:=?rnd&$b + 4
        DRUM_LIT_LEVEL3:
            if drum_flicker_count-- == 0
                rend_oil_drum_flame_sprite ^= $00_07_00_00 'swap between $3b and 3c
                drum_flicker_count:=?rnd&$b + 4
            if rend_mario_sprite.byte[SPRITE_XPOS]>$80
                case drum_state_count++
                    204:    barrel_state[0]:=FIREBALL_JUMP_RIGHT_INIT
                    268:    barrel_state[1]:=FIREBALL_JUMP_RIGHT_INIT
                    316:    barrel_state[2]:=FIREBALL_JUMP_RIGHT_INIT
                    2156:   barrel_state[3]:=FIREBALL_JUMP_RIGHT_INIT
                    4204:   barrel_state[4]:=FIREBALL_JUMP_RIGHT_INIT
            else
                case drum_state_count++
                    204:    barrel_state[0]:=FIREBALL_JUMP_LEFT_INIT
                    268:    barrel_state[1]:=FIREBALL_JUMP_LEFT_INIT
                    316:    barrel_state[2]:=FIREBALL_JUMP_LEFT_INIT
                    2156:   barrel_state[3]:=FIREBALL_JUMP_LEFT_INIT
                    4204:   barrel_state[4]:=FIREBALL_JUMP_LEFT_INIT
    if drum_state<>DRUM_UNLIT
        if MarioCollision(OIL_DRUM_FLAME_SPRITE)
            mario_state:=MARIO_DEAD_INIT

PRI PaulineLogic
    case pauline_state
        PAULINE_STILL_INIT:
            case game_state
                GAME_LEVEL_1..GAME_LEVEL_3:
                    rend_sprites[PAULINE_SPRITES]   := $00_0f_70_17
                    rend_sprites[PAULINE_SPRITES+1] := $00_10_70_17
                    word[rend_map][15] := gfx#TILE_SPACE
                    word[rend_map][16] := gfx#TILE_SPACE
                    word[rend_map][17] := gfx#TILE_SPACE
                GAME_LEVEL_4:
                    rend_sprites[PAULINE_SPRITES]   := $00_0f_60_17
                    rend_sprites[PAULINE_SPRITES+1] := $00_10_60_17
                    word[rend_map][13] := gfx#TILE_SPACE
                    word[rend_map][14] := gfx#TILE_SPACE
                    word[rend_map][15] := gfx#TILE_SPACE
            pauline_state := PAULINE_STILL
            pauline_state_count := 192
        PAULINE_STILL:
            pauline_state_count--
            if pauline_state_count == 0
                pauline_state := PAULINE_SCREAM_INIT
        PAULINE_SCREAM_INIT:
            case game_state
                GAME_LEVEL_1..GAME_LEVEL_3:
                    rend_sprites[PAULINE_SPRITES]   := $00_0f_70_17
                    rend_sprites[PAULINE_SPRITES+1] := $00_10_70_17
                    word[rend_map][15] := gfx#TILE_HELP
                    word[rend_map][16] := constant(gfx#TILE_HELP+1)
                    word[rend_map][17] := constant(gfx#TILE_HELP+2)
                GAME_LEVEL_4:
                    rend_sprites[PAULINE_SPRITES]   := $00_0f_60_17
                    rend_sprites[PAULINE_SPRITES+1] := $00_10_60_17
                    word[rend_map][13] := gfx#TILE_HELP
                    word[rend_map][14] := constant(gfx#TILE_HELP+1)
                    word[rend_map][15] := constant(gfx#TILE_HELP+2)
            pauline_state := PAULINE_SCREAM
            pauline_state_count := 64
        PAULINE_SCREAM:
            pauline_state_count--
            if pauline_state_count//8 == 0
                rend_sprites[PAULINE_SPRITES+1] ^= $00_01_00_00
            if pauline_state_count == 0
                pauline_state := PAULINE_STILL_INIT

PRI LiftLogic | i, mario_x, mario_y, distance
    if lift_state_count-- == 0
        lift_state_count := 1
        repeat i from LIFT_SPRITES to constant(LIFT_SPRITES+2)
            byte[@rend_sprites[i]][SPRITE_YPOS]--
            if byte[@rend_sprites[i]][SPRITE_YPOS]<58
                byte[@rend_sprites[i]][SPRITE_YPOS]+=$9c
        repeat i from constant(LIFT_SPRITES+3) to constant(LIFT_SPRITES+5)
            byte[@rend_sprites[i]][SPRITE_YPOS]++
            if byte[@rend_sprites[i]][SPRITE_YPOS]>58+$9c
                byte[@rend_sprites[i]][SPRITE_YPOS]-=$9c

    repeat i from WINCH_SPRITES to constant(WINCH_SPRITES)+3
        if WinchCollision(i) and mario_state<MARIO_DEAD_INIT
            mario_state:=MARIO_DEAD_INIT

PRI SpringLogic | i, spring_y
    'There are 25 elements in the bounce YPOS lookup.  Each is matched by 2 added to XPOS = 50 per bounce.
    'So 4 bounces gets across the screen to where we drop.  Drop is 3 pixels per frame.
    repeat i from 0 to constant(NUMBER_OF_SPRINGS-1)
        case spring_state_count[i]
            0..99:
                byte[@rend_sprites[SPRING_SPRITES+i]][SPRITE_YPOS]+=lookupz(spring_state_count[i]//25: -3,-3,-3,-2,-2,-2,-2,-1,-1,0,-1,0,0,1,0,1,1,2,2,2,2,3,3,3,0)
                spring_real_x[i]+=2
                spring_y := byte[@rend_sprites[SPRING_SPRITES+i]][SPRITE_YPOS]
                'EPM - play spring bouncing
                if  spring_y < CONSTANT($37 - 15) '350:35
                  hdmf_lite.PlaySoundFM(2, hdmf_lite#SHAPE_SINE, $310 + 53 * (spring_y - CONSTANT($37 - 19)), CONSTANT( hdmf_lite#SAMPLE_RATE / 10 ), 20, $FFFF_FFFF ) 
            100..198:
                byte[@rend_sprites[SPRING_SPRITES+i]][SPRITE_YPOS]+=2
                'EPM - play spring falling
                hdmf_lite.PlaySoundFM(1, hdmf_lite#SHAPE_SINE, $0700 - (18 *  (spring_state_count[i]-100))  , CONSTANT( hdmf_lite#SAMPLE_RATE / 10 ), 20, $FFFF_FFFF )
            199:
                spring_state_count[i] := -1
                spring_real_x[i] := -7 + ?rnd&$F
                byte[@rend_sprites[SPRING_SPRITES+i]][SPRITE_YPOS]:=$37
        if spring_real_x[i]=>8
            byte[@rend_sprites[SPRING_SPRITES+i]][SPRITE_XPOS] := spring_real_x[i]
            if spring_state_count[i]//32<16
                word[@rend_sprites[SPRING_SPRITES+i]][SPRITE_FLAGS_AND_TILE_WORD]:=$38
            else
                word[@rend_sprites[SPRING_SPRITES+i]][SPRITE_FLAGS_AND_TILE_WORD]:=$37
        else
                word[@rend_sprites[SPRING_SPRITES+i]][SPRITE_FLAGS_AND_TILE_WORD]:=constant(SPRITE_INVISIBLE<<8)
        spring_state_count[i]++

        if MarioCollision(SPRING_SPRITES+i)
            mario_state:=MARIO_DEAD_INIT

PRI Ladderlogic | i
    repeat i from 0 to constant(NUMBER_OF_LADDERS-1)
        case ladder_state[i]
            LADDER_UP:
                case ladder_state_count[i]++
                    0:  SetTile(4+23*i, 8, gfx#TILE_INVISIBLE_LADDER)
                        SetTile(4+23*i, 9, gfx#TILE_INVISIBLE_LADDER)
                    255:
                        if i==0
                            ladder_state_count[i] := 0
                            ladder_state[i] := LADDER_GOING_DOWN
                    273:
                        if i==1
                            ladder_state_count[i] := 0
                            ladder_state[i] := LADDER_GOING_DOWN
            LADDER_GOING_DOWN:
                case ladder_state_count[i]++
                    0:  SetTile(4+23*i, 8, gfx#TILE_SPACE)
                        SetTile(4+23*i, 9, gfx#TILE_SPACE)
                    127:
                        ladder_state_count[i] := 0
                        ladder_state[i] := LADDER_DOWN
                if ladder_state_count[i]//8==0
                    'Move Mario down if he's on ladder
                    if rend_mario_sprite.byte[SPRITE_XPOS]==byte[@rend_sprites[LADDER_SPRITES+i]][SPRITE_XPOS]
                        if rend_mario_sprite.byte[SPRITE_XPOS]=<byte[@rend_sprites[LADDER_SPRITES+i]][SPRITE_XPOS] and rend_mario_sprite.byte[SPRITE_XPOS]=>byte[@rend_sprites[LADDER_SPRITES+i]][SPRITE_XPOS]-15
                            if TileAt(rend_mario_sprite.byte[SPRITE_XPOS],rend_mario_sprite.byte[SPRITE_YPOS])<>gfx#TILE_FULL_LADDER
                                 rend_mario_sprite.byte[SPRITE_YPOS]++
                    'Move ladder down
                    byte[@rend_sprites[LADDER_SPRITES+i]][SPRITE_YPOS]++

            LADDER_DOWN:
                case ladder_state_count[i]++
                    15:
                        ladder_state_count[i] := 0
                        ladder_state[i] := LADDER_GOING_UP
            LADDER_GOING_UP:
                case ladder_state_count[i]++
                    63:
                        ladder_state_count[i] := 0
                        ladder_state[i] := LADDER_UP
                if ladder_state_count[i]//4==0
                    byte[@rend_sprites[LADDER_SPRITES+i]][SPRITE_YPOS]--

PRI ConveyorLogic | pie_available, i
    'move existing pies
    if conveyor_state_count++//2==0
        repeat i from 0 to constant(NUMBER_OF_PIES-1)
            if not byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_FLAGS]&SPRITE_INVISIBLE
                if byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_YPOS]==$af
                    'it's on the bottom conveyor
                    case bottom_conveyor_state
                        CONVEYOR_RIGHT:
                            byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_XPOS]++
                            if byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_XPOS]==$F1
                                byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_FLAGS]:=SPRITE_INVISIBLE
                        CONVEYOR_LEFT:
                            byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_XPOS]--
                            if byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_XPOS]==$0F
                                byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_FLAGS]:=SPRITE_INVISIBLE
                elseif byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_XPOS]<$80
                    'it's on the middle left conveyor
                    case middle_left_conveyor_state
                        CONVEYOR_RIGHT:
                            byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_XPOS]++
                            if byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_XPOS]==$80
                                byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_FLAGS]:=SPRITE_INVISIBLE
                        CONVEYOR_LEFT:
                            byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_XPOS]--
                            if byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_XPOS]==$0F
                                byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_FLAGS]:=SPRITE_INVISIBLE
                else
                    'it's on the middle right conveyor
                     case middle_right_conveyor_state
                        CONVEYOR_RIGHT:
                            byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_XPOS]++
                            if byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_XPOS]==$F1
                                byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_FLAGS]:=SPRITE_INVISIBLE
                        CONVEYOR_LEFT:
                            byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_XPOS]--
                            if byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_XPOS]==$80
                                byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_FLAGS]:=SPRITE_INVISIBLE
        'move mario
        if rend_mario_sprite.byte[SPRITE_YPOS]==$af
            'Mario's on the bottom conveyor
            case bottom_conveyor_state
                CONVEYOR_RIGHT:
                    rend_mario_sprite.byte[SPRITE_XPOS]++
                CONVEYOR_LEFT:
                    rend_mario_sprite.byte[SPRITE_XPOS]--
        elseif rend_mario_sprite.byte[SPRITE_YPOS]==$5f
            if rend_mario_sprite.byte[SPRITE_XPOS]<$80
                'Mario's on the middle left conveyor
                case middle_left_conveyor_state
                    CONVEYOR_RIGHT:
                        rend_mario_sprite.byte[SPRITE_XPOS]++
                    CONVEYOR_LEFT:
                        rend_mario_sprite.byte[SPRITE_XPOS]--
            else
                'Mario's on the middle right conveyor
                 case middle_right_conveyor_state
                    CONVEYOR_RIGHT:
                        rend_mario_sprite.byte[SPRITE_XPOS]++
                    CONVEYOR_LEFT:
                        rend_mario_sprite.byte[SPRITE_XPOS]--


    'move kong and change direction of conveyors
    case kong_conveyor_state
        CONVEYOR_RIGHT:
            if kong_conveyor_state_count//2==0
                kong_real_x++
                SetKongSprites
            kong_conveyor_state_count++
            if kong_conveyor_state_count==128
                middle_left_conveyor_state ^= 1
            if kong_conveyor_state_count==256
                kong_conveyor_state:=CONVEYOR_LEFT
        CONVEYOR_LEFT:
            if kong_conveyor_state_count//2==0
                kong_real_x--
                SetKongSprites
            kong_conveyor_state_count--
            if kong_conveyor_state_count==0
                kong_conveyor_state:=CONVEYOR_RIGHT
                bottom_conveyor_state ^= 1
            if kong_conveyor_state_count==128
                middle_right_conveyor_state ^= 1
    'create pies
    if pie_state_count++ == 125
        pie_available := -1
        repeat i from 0 to constant(NUMBER_OF_PIES-1)
            if byte[@rend_sprites[PIE_SPRITES+i]][SPRITE_FLAGS]&SPRITE_INVISIBLE
                pie_available := i
                quit
        if pie_available=>0
            barrel_state[i+PIE_SPRITES-MOVING_BARREL_SPRITES] := BARREL_PIE
            repeat
                case ?rnd&$3
                    0:
                        if middle_left_conveyor_state==CONVEYOR_RIGHT
                            rend_sprites[PIE_SPRITES+i] := $00_43_10_5f
                            fireball_real_y[i+PIE_SPRITES-MOVING_BARREL_SPRITES] := $5f
                            quit
                    1:
                        if middle_right_conveyor_state==CONVEYOR_LEFT
                            rend_sprites[PIE_SPRITES+i] := $00_43_F0_5f
                            fireball_real_y[i+PIE_SPRITES-MOVING_BARREL_SPRITES] := $5f
                            quit
                    2:
                        if bottom_conveyor_state==CONVEYOR_RIGHT
                            rend_sprites[PIE_SPRITES+i] := $00_43_10_af
                            fireball_real_y[i+PIE_SPRITES-MOVING_BARREL_SPRITES] := $af
                            quit
                    3:
                        if bottom_conveyor_state==CONVEYOR_LEFT
                            rend_sprites[PIE_SPRITES+i] := $00_43_F0_af
                            fireball_real_y[i+PIE_SPRITES-MOVING_BARREL_SPRITES] := $af
                            quit
        pie_state_count := 0

PRI RivetLogic | x, y, i
    if rivets_left == 0
        game_state:=GAME_LEVEL_4_END_SCENE_INIT

    x := rend_mario_sprite.byte[SPRITE_XPOS]
    y := ((rend_mario_sprite.byte[SPRITE_YPOS]-16)/40)*40+55
    'debugdec(string("YT"),y)
    'debugdec(string("tile"),TileAt(x,y))
    if TileAt(x,y)==gfx#TILE_TOP_OF_RIVET
        'store coords to remove once mario steps off
        remove_rivet_x:=x
        remove_rivet_y:=y
    elseif remove_rivet_x>0
        SetTile((remove_rivet_x>>3),(remove_rivet_y>>3),gfx#TILE_SPACE)
        SetTile((remove_rivet_x>>3),(remove_rivet_y>>3)+1,gfx#TILE_SPACE)
        remove_rivet_x:=0
        rivets_left--
        rend_bonus_sprite.word[SPRITE_LOCATION_WORD] := rend_mario_sprite.word[SPRITE_LOCATION_WORD]
        bonus_state:=BONUS_RIVET_INIT

    i:=rivet_state_count++ >>8
    if rivet_state_count&$FF==0 and i<5
        if rend_mario_sprite.byte[SPRITE_XPOS]<$80
            barrel_state[i]:=FIREBALL_BOUNCE_LEFT_INIT
            case ?rnd&3
                0:
                    rend_sprites[MOVING_BARREL_SPRITES+i] := $01_45_d7_5f
                    fireball_real_y[i]:=$5f
                1:
                    rend_sprites[MOVING_BARREL_SPRITES+i] := $01_45_df_87
                    fireball_real_y[i]:=$87
                2:
                    rend_sprites[MOVING_BARREL_SPRITES+i] := $01_45_e7_af
                    fireball_real_y[i]:=$af
                3:
                    rend_sprites[MOVING_BARREL_SPRITES+i] := $01_45_ef_d7
                    fireball_real_y[i]:=$d7
        else
            barrel_state[i]:=FIREBALL_BOUNCE_RIGHT_INIT
            case ?rnd&3
                0:
                    rend_sprites[MOVING_BARREL_SPRITES+i] := $00_45_28_5f
                    fireball_real_y[i]:=$5f
                1:
                    rend_sprites[MOVING_BARREL_SPRITES+i] := $00_45_20_87
                    fireball_real_y[i]:=$87
                2:
                    rend_sprites[MOVING_BARREL_SPRITES+i] := $00_45_18_af
                    fireball_real_y[i]:=$af
                3:
                    rend_sprites[MOVING_BARREL_SPRITES+i] := $00_45_10_d7
                    fireball_real_y[i]:=$d7

PRI BonusLogic
    'in play bonuses
    case bonus_state
        BONUS_BARREL_HURDLED_INIT:
            hdmf_lite.SFX_start(hdmf_lite#SFX_STATE__JUMP_BARREL) 'EPM
            rend_bonus_sprite.byte[SPRITE_YPOS]+=15
            case hurdle_count
                1:
                    rend_bonus_sprite.word[SPRITE_FLAGS_AND_TILE_WORD] := gfx#SPRITE_SCORE_100
                    AddToScore(100)
                2:
                    rend_bonus_sprite.word[SPRITE_FLAGS_AND_TILE_WORD] := gfx#SPRITE_SCORE_300
                    AddToScore(300)
                other:
                    rend_bonus_sprite.word[SPRITE_FLAGS_AND_TILE_WORD] := gfx#SPRITE_SCORE_800
                    AddToScore(800)
            bonus_state_count:=0
            bonus_state:=BONUS_BARREL_DELAY
        BONUS_COLLECTABLE_INIT:
            hdmf_lite.SFX_start(hdmf_lite#SFX_STATE__JUMP_BARREL) 'EPM
            rend_bonus_sprite.word[SPRITE_FLAGS_AND_TILE_WORD] := gfx#SPRITE_SCORE_300
            AddToScore(300)
            bonus_state_count:=0
            bonus_state:=BONUS_BARREL_DELAY
        BONUS_RIVET_INIT:
            hdmf_lite.SFX_start(hdmf_lite#SFX_STATE__JUMP_BARREL) 'EPM
            rend_bonus_sprite.word[SPRITE_FLAGS_AND_TILE_WORD] := gfx#SPRITE_SCORE_100
            AddToScore(100)
            bonus_state_count:=0
            bonus_state:=BONUS_BARREL_DELAY
        BONUS_BARREL_DELAY:
            if bonus_state_count == 64
                rend_bonus_sprite.byte[SPRITE_FLAGS] := SPRITE_INVISIBLE
                bonus_state:=BONUS_NONE
        BONUS_BARREL_BUSTED_INIT:
            bonus_state_count:=0
            bonus_freeze:=TRUE
            bonus_state:=BONUS_BARREL_BUSTED
        BONUS_BARREL_BUSTED:
            case bonus_state_count
                0,16,32: rend_bonus_sprite.word[SPRITE_FLAGS_AND_TILE_WORD] := 74
                8,24,40: rend_bonus_sprite.word[SPRITE_FLAGS_AND_TILE_WORD] := 75
                48: rend_bonus_sprite.word[SPRITE_FLAGS_AND_TILE_WORD] := 76
                60: rend_bonus_sprite.word[SPRITE_FLAGS_AND_TILE_WORD] := 77
                73:
                    rend_bonus_sprite.word[SPRITE_FLAGS_AND_TILE_WORD] := gfx#SPRITE_SCORE_300
                    AddToScore(300)
                    bonus_freeze:=FALSE
                137:
                    rend_bonus_sprite.byte[SPRITE_FLAGS] := SPRITE_INVISIBLE
                    bonus_state:=BONUS_NONE
                    'EPM: no longer necessary, driver now knows walk state and starts sound
                    '     back up on its own.
                    'if mario_state==MARIO_WALK_LEFT or mario_state==MARIO_WALK_RIGHT
                    '    hdmf_lite.SFX_start(hdmf_lite#SFX_STATE__WALK)    
        BONUS_ABOUT_TO_DIE:
            if bonus_state_count==256
                mario_state:=MARIO_DEAD_INIT

    'timer bonus
    if --timer_state_count == 0
        SubFromTimer(100)
        if timer==0
            bonus_state:=BONUS_ABOUT_TO_DIE
            bonus_state_count := 0
        else
            timer_state_count := 128

    bonus_state_count++
    return not bonus_freeze

PRI CollectableLogic | i
    repeat i from COLLECTABLE_SPRITES to constant(COLLECTABLE_SPRITES+2)
        if MarioCollision(i)
            rend_sprites[i]:=$04_00_00_00
            rend_bonus_sprite.word[SPRITE_LOCATION_WORD] := rend_mario_sprite.word[SPRITE_LOCATION_WORD]
            bonus_state:=BONUS_COLLECTABLE_INIT

PRI Rotate(item_ptr,increment,lower,upper)
    ' Rotate a byte value through a range of values.  Useful for sprite animation.
    ' byte_item = pointer to a variable to ,reset,lower,upper)
    ' increment = value to add or subtract
    ' lower     = lower bound
    ' upper     = upper bound
    long[item_ptr]+=increment
    if long[item_ptr]>upper
        long[item_ptr] := lower
    if long[item_ptr]<lower
        long[item_ptr] := upper
    return

PRI TileAt(px,py) | pos
    if py>223
        if py>231
            return gfx#TILE_FULL_PLATFORM ' An empty space to avoid it looking like there is a ladder going down
        return gfx#TILE_SPACE             ' A full platform so that platform_height always returns a value
    pos := (px>>3+(py>>3)*32)<<1
    return byte[rend_map][pos]

PRI SetTile(tx,ty,tile) | pos
    pos := (tx+ty*32)<<1
    byte[rend_map][pos] := tile

PRI TileHeight(tile)
    if (tile=>gfx#TILE_FULL_PLATFORM_LADDER_TILE and tile=<gfx#TILE_START_OF_LADDER_TILES)
        return tile-gfx#TILE_FULL_PLATFORM_LADDER_TILE
    if (tile=>gfx#TILE_FULL_PLATFORM and tile=<gfx#TILE_END_OF_PLATFORM_TILES)
        return tile-gfx#TILE_FULL_PLATFORM
    return 0

PRI PlatformHeight(_x,_y) | background, i, distance
    ' Calculates the position of one pixel above height of the platform where we are.
    ' If we are stood on the platform it's where our feet are.
    ' If our feet are overlapping a platform it's the top of that.(To cope with inclines.)
    ' Otherwise it's the first platform below.
    if game_state == GAME_LEVEL_2
        if _x=>$30 and _x<$40
            repeat i from LIFT_SPRITES to constant(LIFT_SPRITES+2)
                distance := _y-byte[@rend_sprites[i]][SPRITE_YPOS]
                case distance
                    -3..3: return byte[@rend_sprites[i]][SPRITE_YPOS]
        if _x=>$70 and _x<$80
            repeat i from constant(LIFT_SPRITES+3) to constant(LIFT_SPRITES+5)
                distance := _y-byte[@rend_sprites[i]][SPRITE_YPOS]
                case distance
                    -3..3: return byte[@rend_sprites[i]][SPRITE_YPOS]
    repeat
        background := TileAt(_x,_y)
        'debugdec(string("TILE"),background)
        case background
            gfx#TILE_FULL_PLATFORM_LADDER_TILE..gfx#TILE_END_OF_LADDER_TILES:
                'top of platform
                return (_y & $F8)+(background-gfx#TILE_START_OF_LADDER_TILES)
            gfx#TILE_FULL_PLATFORM..gfx#TILE_END_OF_PLATFORM_TILES:
                return (_y & $F8)+(background-gfx#TILE_START_OF_PLAT_TOP_TILES)
            gfx#TILE_SPACE:
                'empty space, we need to look below
                _y+=8                                        'empty space
            gfx#TILE_LEVEL_3_PLATFORM,gfx#TILE_LEVEL_4_PLATFORM,gfx#TILE_BOTTOM_OF_RIVET:
                'full platforms
                return (_y & $F8)-1
            gfx#TILE_END_LOWER_PLAT_LAD_TILES:
                'lower platform - lots of it
                return (_y & $F8)+(background-gfx#TILE_START_OF_LADDER_TILES)           'tile with ladder below
            gfx#TILE_END_OF_LOWER_PLAT_TILES:
                return (_y & $F8)+(background-gfx#TILE_START_OF_LADDER_TILES)           'platform with space below
            gfx#TILE_FULL_LADDER..gfx#TILE_END_LOWER_PLAT_LAD_TILES-1:
                'lower platform - not much
                _y+=8                                        'tile with ladder below
            gfx#TILE_START_OF_PLATFORM_TILES..gfx#TILE_END_OF_LOWER_PLAT_TILES:
                _y+=8                                        'platform with space below
            other:
                _y+=8

PRI LadderTopHeight(_x,_y) | background, current_y, i, distance
    ' Calculates the position of the platform at the top of a ladder.
    ' If we are by a ladder, then this one pixel above the platform at the top of the ladder,
    ' Otherwise it's the same value as PlatformHeight( ).
    ' If it's a broken ladder, then it's still the position of the platform above.
    if game_state == GAME_LEVEL_2
        if _x=>$30 and _x<$40
            repeat i from LIFT_SPRITES to constant(LIFT_SPRITES+2)
                distance := _y-byte[@rend_sprites[i]][SPRITE_YPOS]
                case distance
                    -3..3: return byte[@rend_sprites[i]][SPRITE_YPOS]
        if _x=>$70 and _x<$80
            repeat i from constant(LIFT_SPRITES+3) to constant(LIFT_SPRITES+5)
                distance := _y-byte[@rend_sprites[i]][SPRITE_YPOS]
                case distance
                    -3..3: return byte[@rend_sprites[i]][SPRITE_YPOS]
    current_y := PlatformHeight(_x,_y)
    repeat
        if current_y<0
            ' This must be one of Kong's ladders
            return _y
        background := TileAt(_x,current_y)
        case background
            gfx#TILE_FULL_LADDER..gfx#TILE_END_OF_LADDER_TILES,gfx#TILE_INVISIBLE_LADDER:
                current_y-=8                              'ladders + workaround invisible ladder
            'top of platform
            gfx#TILE_FULL_PLATFORM..gfx#TILE_END_OF_PLATFORM_TILES:
                return (current_y & $F8)+(background-gfx#TILE_START_OF_PLAT_TOP_TILES)
            'empty space
            gfx#TILE_SPACE:
                return current_y                          'empty space
            'full platforms
            gfx#TILE_LEVEL_3_PLATFORM,gfx#TILE_LEVEL_4_PLATFORM:
                current_y-=8
            'lower platform - lots of it
            gfx#TILE_END_OF_LOWER_PLAT_TILES:
                return (current_y & $F8)+(background-gfx#TILE_START_OF_LADDER_TILES) 'platform with space below
            'lower platform - not much
            gfx#TILE_START_OF_PLATFORM_TILES..constant(gfx#TILE_FULL_PLATFORM-2):
                return current_y                          'platform with space below
            other:
                return current_y

PRI LadderBottomHeight(_x,_y) | background, i, distance
    ' Calculates the position of the platform at the bottom of a ladder.
    ' If we are above a ladder, then this one pixel above the platform at the bottom of the ladder,
    ' Otherwise it's the same value as PlatformHeight( ).
    ' If it's a broken ladder, then that doesn't count.  It's still the position of the platform above.
    if game_state == GAME_LEVEL_2
        if _x=>$30 and _x<$40
            repeat i from LIFT_SPRITES to constant(LIFT_SPRITES+2)
                distance := _y-byte[@rend_sprites[i]][SPRITE_YPOS]
                case distance
                    -3..3: return byte[@rend_sprites[i]][SPRITE_YPOS]
        if _x=>$70 and _x<$80
            repeat i from constant(LIFT_SPRITES+3) to constant(LIFT_SPRITES+5)
                distance := _y-byte[@rend_sprites[i]][SPRITE_YPOS]
                case distance
                    -3..3: return byte[@rend_sprites[i]][SPRITE_YPOS]
    _y := PlatformHeight(_x,_y)
    background := TileAt(_x,_y+8)
    if (background==gfx#TILE_FULL_PLATFORM_LADDER_TILE or background==gfx#TILE_FULL_PLATFORM or background==gfx#TILE_LEVEL_3_PLATFORM or background==gfx#TILE_LEVEL_4_PLATFORM) and TileAt(_x,_y+16)<>gfx#TILE_SPACE
        _y+=8
    repeat
        background := TileAt(_x,_y+8)
        'debugdec(@tiletext,background)
        case background
            'empty space
            gfx#TILE_SPACE:
                return _y-8                                         ' empty space
            'full platforms
            gfx#TILE_LEVEL_3_PLATFORM,gfx#TILE_LEVEL_4_PLATFORM:
                return (_y & $F8)+7
            gfx#TILE_FULL_LADDER..gfx#TILE_END_OF_LADDER_TILES,gfx#TILE_INVISIBLE_LADDER:
                _y+=8                                               'ladders + workaround invisible ladder
            gfx#TILE_START_OF_PLATFORM_TILES..gfx#TILE_END_OF_LOWER_PLAT_TILES:
                return (_y & $F8)+(background-gfx#TILE_START_OF_PLATFORM_TILES)                  'platform with space below
            other:          return (_y & $F8)+7

PRI BarrelLadderBottomHeight(_x,_y) | background
    ' Calculates the position of the platform at the bottom of a ladder.
    ' If we are above a ladder, then this one pixel above the platform at the bottom of the ladder,
    ' Otherwise it's the same value as LowPlatformHeight( ).
    ' If it's a broken ladder, then that doesn't count.  It's still the position of the platform above.
    ' This is a variation for barrels, which will include ladders with gaps.
    'debugdec(string("YBEFORE"),_y)
    _y := PlatformHeight(_x,_y)
    'debugdec(string("YAFTER"),_y)
    background := TileAt(_x,_y+8)
    if (background==gfx#TILE_FULL_PLATFORM_LADDER_TILE or background==gfx#TILE_FULL_PLATFORM or background==gfx#TILE_LEVEL_3_PLATFORM or background==gfx#TILE_LEVEL_4_PLATFORM) and TileAt(_x,_y+16)<>gfx#TILE_SPACE
        _y+=8
        'debugdec(string("EXTRA"),0)
    repeat
        background := TileAt(_x,_y+8)
        'debugdec(@tiletext,background)
        case background
            'full platforms
            gfx#TILE_LEVEL_3_PLATFORM,gfx#TILE_LEVEL_4_PLATFORM,gfx#TILE_FULL_PLATFORM_LADDER_TILE,gfx#TILE_FULL_PLATFORM:
                return (_y & $F8)+7
            gfx#TILE_FULL_LADDER..gfx#TILE_END_OF_LADDER_TILES,gfx#TILE_INVISIBLE_LADDER:
                _y+=8                           'ladders + workaround invisible ladder
            gfx#TILE_START_OF_PLATFORM_TILES..gfx#TILE_END_OF_LOWER_PLAT_TILES:
                return (_y & $F8)+(background-gfx#TILE_START_OF_PLATFORM_TILES) 'platform with space below
            other:
                return (_y & $F8)+7

PRI SelectMap(map)
    rend_map := gfx.map_adr+constant(32*28*2)*map

PRI ClearMap(default_palette) | p, i
    p := gfx.map_adr
    repeat i from 0 to constant(28*32-1)
        word[p]:=gfx#TILE_SPACE + default_palette<<8
        p+=2

PRI DrawPlatformTile(x_in_tiles,y_in_pixels) | tilepos, y_in_tile
    tilepos:=(x_in_tiles+2+((y_in_pixels>>3)<<5))<<1 + gfx.map_adr
    y_in_tile:=y_in_pixels&$7
    if y_in_tile==0
            byte[tilepos]:=gfx#TILE_FULL_PLATFORM
    else
        if byte[tilepos]=>gfx#TILE_FULL_LADDER and byte[tilepos]=<gfx#TILE_END_OF_LADDER_TILES
            byte[tilepos]:=gfx#TILE_FULL_PLATFORM_LADDER_TILE+y_in_tile
        else
            byte[tilepos]:=gfx#TILE_FULL_PLATFORM+y_in_tile
        tilepos+=64
        if byte[tilepos]=>gfx#TILE_FULL_LADDER and byte[tilepos]=<gfx#TILE_END_OF_LADDER_TILES
            byte[tilepos]:=gfx#TILE_FULL_LADDER+y_in_tile
        else
            byte[tilepos]:=constant(gfx#TILE_START_OF_PLATFORM_TILES-1)+y_in_tile

PRI DrawString(x,y,text,palette) | pos, i
    pos := x+y*32
    repeat i from 0 to strsize(text)-1
        word[rend_map][pos++] := byte[text][i]-15 + palette<<8

PRI DrawNumber(x,y,number,digits,palette) | pos, i
    pos := x+y*32
    repeat i from 0 to digits-1
        word[rend_map][pos+(digits-1-i)] := number//10 + 33 + palette<<8
        number/=10

PRI DrawKong(x,y) | char, palette, xx, yy, pos
    char := gfx#TILE_KONG_START
    palette := 5<<8
    repeat yy from y to y+3
        pos := x+yy*32
        repeat xx from x to x+5
            word[rend_map][pos++] := char++ +palette

PRI DrawKongHeight(x,y,h) | palette, pos
    palette := 5<<8
    pos := x+y*32
    word[rend_map][pos++] := lookupz(h:46,47,48,45) +palette
    word[rend_map][pos++] := lookupz(h:47,44,47,44) +palette
    word[rend_map][pos++] := lookupz(h:gfx#TILE_SPACE,gfx#TILE_SPACE,gfx#TILE_SPACE,44) +palette
    word[rend_map][pos] := gfx#TILE_SMALL_M +palette

PRI DrawStatus
    DrawString(0,0,lookupz(-game_two_players:string("HI SCORE"),@string1up),1)
    DrawString(29,0,lookupz(-game_two_players:@string1up,@string2up),1)
    AddToScore(0)
    DrawLives
    DrawString(20,2,@lequals,2)

PRI DrawLives | i
    player_lives[game_player_up]#>=0
    player_lives[game_player_up]<#=7
    repeat i from 0 to player_lives[game_player_up]
        word[gfx.map_adr+40][i] := gfx#TILE_MARIO_LIFE+(3<<8)
    word [gfx.map_adr+40][player_lives[game_player_up]] := gfx#TILE_SPACE

PRI DrawBonusBox | pos
    DrawString(26,3,@bonusbox1,0)
    DrawString(26,4,@bonusbox2,0)
    DrawString(26,5,@bonusbox3,0)
    SubFromTimer(0)

PRI SetKongSprites | dest, i, temp, end, block
    block := kong_pose_source_ptr
    dest := @rend_sprites[kong_sprites]
    case game_state
        GAME_CLIMB_SCENE:
            '6 kong + pauline
            end := 6
        GAME_LEVEL_1,GAME_LEVEL_12_END_SCENE,GAME_LEVEL_123_FOLLOW_ON:
            '8 sprites in centre barrel pose
            end := 7
        other:
            'reusing 2 kong sprites on level 3 for ladders
            end := 5
    repeat i from 0 to end
        temp:=long[block]
        byte[@temp] += kong_real_y
        byte[@temp][1] += kong_real_x - $3c
        long[dest]:=temp
        block+=4
        dest+=4

PRI AddToScore(delta) | before
    before := player_score[game_player_up]/10000
    player_score[game_player_up]+=delta
    hi_score#>=player_score[game_player_up]
    if game_two_players
        DrawNumber(0,1,player_score[0],6,4)
        DrawNumber(26,1,player_score[1],6,4)
    else
        DrawNumber(0,1,hi_score,6,4)
        DrawNumber(26,1,player_score[0],6,4)        
    if player_score[game_player_up]/10000<>before
        ++player_lives[game_player_up]
        DrawLives

PRI SubFromTimer(delta)
    timer-=delta
    timer #>= 0
    if timer>900
        DrawNumber(27,4,timer,4,0)
        return
    if timer==900
        background_song_ptr:=@_SONG_out_of_time
        hdmf_lite.play_song(background_song_ptr,hdmf_lite#HDMF__FLAG_LOOP_SONG)
    DrawNumber(27,4,timer,4,9)

PRI MarioCollision(sprite_num) | xd,yd
    if byte[@rend_sprites[sprite_num]][SPRITE_FLAGS]&SPRITE_INVISIBLE==0
        xd:=byte[@rend_sprites[sprite_num]][SPRITE_XPOS]-rend_mario_sprite.byte[SPRITE_XPOS]
        'xd is negative if sprite is to the right of Mario
        if xd>-7 and xd<7
            yd:=byte[@rend_sprites[sprite_num]][SPRITE_YPOS]-rend_mario_sprite.byte[SPRITE_YPOS]
            'yd is negative if sprite is above Mario
            if yd>-7 and yd<7
                return TRUE
    return FALSE

PRI HammerCollision(sprite_num) | xd,yd
    if action_hammer_ptr
        if byte[@rend_sprites[sprite_num]][SPRITE_FLAGS]&SPRITE_INVISIBLE==0
            xd:=byte[@rend_sprites[sprite_num]][SPRITE_XPOS]-byte[action_hammer_ptr][SPRITE_XPOS]
            'xd is negative if sprite is to the right of Hammer
            if xd>-9 and xd<9
                yd:=byte[@rend_sprites[sprite_num]][SPRITE_YPOS]-byte[action_hammer_ptr][SPRITE_YPOS]
                'yd is negative if sprite is above Hammer
                if yd>-7 and yd<12
                    return TRUE
    return FALSE

PRI WinchCollision(sprite_num) | xd,yd
            xd:=byte[@rend_sprites[sprite_num]][SPRITE_XPOS]-rend_mario_sprite.byte[SPRITE_XPOS]
            'xd is negative if sprite is to the right of Lift
            if xd>-14 and xd<14
                yd:=byte[@rend_sprites[sprite_num]][SPRITE_YPOS]-rend_mario_sprite.byte[SPRITE_YPOS]
                'yd is negative if sprite is above Lift
                if yd>-14 and yd<14
                    return TRUE
    return FALSE

PRI SoundCrash | i
  repeat i from 0 to 4
    hdmf_lite.PlaySoundFM(i, hdmf_lite#SHAPE_NOISE, 130 + i * constant((247-130) / 5), hdmf_lite#SAMPLE_RATE/2,  128, $1357_ACDF)

CON
' //////////////////////////////////////////////////////////////////
' NES Game Paddle Read
' //////////////////////////////////////////////////////////////////
' reads both gamepads in parallel encodes 8-bits for each in format
' right game pad #1 [15..8] : left game pad #0 [7..0]

    GP_RIGHT  = %00000001
    GP_LEFT   = %00000010
    GP_DOWN   = %00000100
    GP_UP     = %00001000
    GP_START  = %00010000
    GP_SELECT = %00100000
    GP_B      = %01000000
    GP_A      = %10000000
  JOY_CLK = 16
  JOY_LCH = 17
  JOY_DATAOUT0 = 19
  JOY_DATAOUT1 = 18

PUB NES_Read_Gamepad : nes_bits        |  i

' //////////////////////////////////////////////////////////////////
' NES Game Paddle Read
' //////////////////////////////////////////////////////////////////       
' reads both gamepads in parallel encodes 8-bits for each in format
' right game pad #1 [15..8] : left game pad #0 [7..0]
'
' set I/O ports to proper direction
' P3 = JOY_CLK      (4)
' P4 = JOY_SH/LDn   (5) 
' P5 = JOY_DATAOUT0 (6)
' P6 = JOY_DATAOUT1 (7)
' NES Bit Encoding
'
' RIGHT  = %00000001
' LEFT   = %00000010
' DOWN   = %00000100
' UP     = %00001000
' START  = %00010000
' SELECT = %00100000
' B      = %01000000
' A      = %10000000

' step 1: set I/Os
DIRA [JOY_CLK] := 1 ' output
DIRA [JOY_LCH] := 1 ' output
DIRA [JOY_DATAOUT0] := 0 ' input
DIRA [JOY_DATAOUT1] := 0 ' input1

' step 2: set clock and latch to 0
OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0
'Delay(1)

' step 3: set latch to 1
OUTA [JOY_LCH] := 1 ' JOY_SH/LDn = 1
'Delay(1)

' step 4: set latch to 0
OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0

' step 5: read first bit of each game pad

' data is now ready to shift out
' first bit is ready 
nes_bits := 0

' left controller
nes_bits := INA[JOY_DATAOUT0] | (INA[JOY_DATAOUT1] << 8)

' step 7: read next 7 bits
repeat i from 0 to 6
 OUTA [JOY_CLK] := 1 ' JOY_CLK = 1
 'Delay(1)             
 OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
 nes_bits := (nes_bits << 1)
 nes_bits := nes_bits | INA[JOY_DATAOUT0] | (INA[JOY_DATAOUT1] << 8)

 'Delay(1)             
' invert bits to make positive logic
nes_bits := (!nes_bits & $FFFF)

' //////////////////////////////////////////////////////////////////
' End NES Game Paddle Read
' //////////////////////////////////////////////////////////////////       


{VAR
  long  row
  long  col

PUB DebugInit(map)
    wordfill(map,gfx#TILE_SPACE,32*28)
    DebugReset

PUB DebugReset
  row := 10
  col := 0
}
{PUB DebugHex(title,number) | i, digit
    i:=0
    repeat while byte[title][i]<>0
        debug_map[row*32+col++] := byte[title][i]
        i++
    debug_map[row*32+col++] := 58
    digit := (number >> 28)
    debug_map[row*32+col++] := digit+48-(digit>9)*7
    digit := ((number >> 24) & $F)
    debug_map[row*32+col++] := digit+48-(digit>9)*7
    digit := ((number >> 20) & $F)
    debug_map[row*32+col++] := digit+48-(digit>9)*7
    digit := ((number >> 16) & $F)
    debug_map[row*32+col++] := digit+48-(digit>9)*7
    digit := ((number >> 12) & $F)
    debug_map[row*32+col++] := digit+48-(digit>9)*7
    digit := ((number >> 8) & $F)
    debug_map[row*32+col++] := digit+48-(digit>9)*7
    digit := ((number >> 4) & $F)
    debug_map[row*32+col++] := digit+48-(digit>9)*7
    digit := (number & $F)
    debug_map[row*32+col++] := digit+48-(digit>9)*7
    col:=0
    row+=1
    row<#=27
}
{PUB DebugDec(title,number) | i
    i:=0
    repeat while byte[title][i]<>0
        debug_map[row*32+col++] := byte[title][i]-8
        i++
    debug_map[row*32+col++] := 58-8
    debug_map[row*32+col+9] := number//10 + 40
    number/=10
    debug_map[row*32+col+8] := number//10 + 40
    number/=10
    debug_map[row*32+col+7] := number//10 + 40
    number/=10
    debug_map[row*32+col+6] := number//10 + 40
    number/=10
    debug_map[row*32+col+5] := number//10 + 40
    number/=10
    debug_map[row*32+col+4] := number//10 + 40
    number/=10
    debug_map[row*32+col+3] := number//10 + 40
    number/=10
    debug_map[row*32+col+2] := number//10 + 40
    number/=10
    debug_map[row*32+col+1] := number//10 + 40
    number/=10
    debug_map[row*32+col+0] := number//10 + 40
    col:=0
    row++
    row<#=27
}
DAT
'For some reason the first DAT long is corrupted even before Main is run.
'                        long    0
'Used for Kong doing barrel rolling
kong_center_barrel_template
                        long    $00_22_3c_00            'torso
                        long    $01_20_3c_00            'head
                        long    $00_24_48_00            'right leg
                        long    $01_24_30_00            'left leg
                        long    $01_28_2c_00            'left arm
                        long    $00_28_4c_00            'right arm
                        long    $01_27_2c_00            'left shoulder
                        long    $00_27_4c_00            'right shoulder
kong_center_sad_template
                        long    $00_22_3c_00            'torso
                        long    $01_20_3c_00            'head
                        long    $00_24_48_00            'right leg
                        long    $01_24_30_00            'left leg
                        long    $00_26_31_00            'left arm
                        long    $01_26_47_00            'right arm
                        long    $04_00_00_00            'empty
                        long    $04_00_00_00            'empty
kong_left_template
                        long    $00_2b_3a_00            'torso
                        long    $00_2d_2a_00            'head
                        long    $00_29_4a_00            'right leg
                        long    $00_2a_2a_00            'left leg
                        long    $00_2c_3a_00            'shoulder
                        long    $04_00_00_00            'empty
                        long    $04_00_00_00            'empty
                        long    $04_00_00_00            'empty
kong_right_template
                        long    $01_2b_3c_00            'torso
                        long    $01_2d_4c_00            'head
                        long    $01_29_2c_00            'right leg
                        long    $01_2a_4c_00            'left leg
                        long    $01_2c_3c_00            'shoulder
                        long    $04_00_00_00            'empty
                        long    $04_00_00_00            'empty
                        long    $04_00_00_00            'empty

' Used for normal kong state Levels 2-4
kong_center_roar_r_template
                        long    $00_22_3c_00            'torso
                        long    $01_21_3c_00            'head
                        long    $00_24_48_00            'right leg
                        long    $00_23_30_00            'left leg
                        long    $00_26_31_00            'left arm
                        long    $00_25_4c_00            'right arm
'                        long    $04_00_00_00            'empty
'                        long    $04_00_00_00            'empty
kong_center_roar_l_template
                        long    $00_22_3c_00            'torso
                        long    $01_21_3c_00            'head
                        long    $01_23_48_00            'right leg
                        long    $01_24_30_00            'left leg
                        long    $01_25_2c_00            'left arm
                        long    $01_26_47_00            'right arm
'                        long    $04_00_00_00            'empty
'                        long    $04_00_00_00            'empty
' Back of Kong. Used for climbing
kong_back_right
                        long    $00_33_34_00            'left leg
                        long    $00_34_44_00            'right leg
                        long    $00_2f_34_00            'left shoulder
                        long    $00_30_44_00            'right shoulder
                        long    $00_31_31_00            'left arm
                        long    $00_32_46_00            'right arm
                        long    $04_00_00_00            'empty
                        long    $04_00_00_00            'empty
kong_back_left
                        long    $01_34_34_00            'left leg
                        long    $01_33_44_00            'right leg
                        long    $00_2f_34_00            'left shoulder
                        long    $00_30_44_00            'right shoulder
                        long    $01_32_32_00            'left arm
                        long    $01_31_46_00            'right arm
                        long    $04_00_00_00            'empty
                        long    $04_00_00_00            'empty
kong_back_right_with_pauline
                        long    $00_33_34_00            'left leg
                        long    $00_34_44_00            'right leg
                        long    $00_2f_34_00            'left shoulder
                        long    $00_30_44_00            'right shoulder
                        long    $00_31_31_00            'left arm
                        long    $00_2e_4a_00            'right arm
                        long    $00_13_4b_00            'pauline
                        long    $04_00_00_00            'empty
kong_back_left_with_pauline
                        long    $01_34_34_00            'left leg
                        long    $01_33_44_00            'right leg
                        long    $00_2f_34_00            'left shoulder
                        long    $00_30_44_00            'right shoulder
                        long    $01_32_32_00            'left arm
                        long    $00_2e_4a_00            'right arm
                        long    $01_13_4b_00            'pauline
                        long    $04_00_00_00            'empty
' Kong falling
kong_upsidedown_1
                        long    $02_22_3c_10            'torso
                        long    $02_21_3c_30            'head
                        long    $02_24_48_10            'right leg
                        long    $03_24_30_10            'left leg
                        long    $02_26_31_28            'left arm
                        long    $03_26_47_28            'right arm
'                        long    $04_00_00_00            'empty
'                        long    $04_00_00_00            'empty
kong_upsidedown_2
                        long    $02_22_3c_10            'torso
                        long    $02_1d_3c_30            'head
                        long    $02_24_48_10            'right leg
                        long    $03_24_30_10            'left leg
                        long    $02_26_31_28            'left arm
                        long    $03_26_47_28            'right arm
'                        long    $04_00_00_00            'empty
'                        long    $04_00_00_00            'empty
kong_upsidedown_3
                        long    $02_22_3c_10            'torso
                        long    $02_1e_3c_30            'head
                        long    $02_24_48_10            'right leg
                        long    $03_24_30_10            'left leg
                        long    $02_26_31_28            'left arm
                        long    $03_26_47_28            'right arm
'                        long    $04_00_00_00            'empty
'                        long    $04_00_00_00            'empty
kong_upsidedown_4
                        long    $02_22_3c_10            'torso
                        long    $02_1f_3c_30            'head
                        long    $02_24_48_10            'right leg
                        long    $03_24_30_10            'left leg
                        long    $02_26_31_28            'left arm
                        long    $03_26_47_28            'right arm
'                        long    $04_00_00_00            'empty
'                        long    $04_00_00_00            'empty
{
kong_invisible
                        long    $04_00_00_00            'empty
                        long    $04_00_00_00            'empty
                        long    $04_00_00_00            'empty
                        long    $04_00_00_00            'empty
                        long    $04_00_00_00            'empty
                        long    $04_00_00_00            'empty
                        long    $04_00_00_00            'empty
                        long    $04_00_00_00            'empty
}
DAT
game_over_string_1      byte    "              ",0
game_over_string_2      byte    "  GAME  OVER  ",0

DAT
title1        byte  43,43,32,32,43,43,43,32,43,43,32,32,32,43,43,32,32,43,32,43,0
title2        byte  43,32,43,32,43,32,43,32,43,32,43,32,43,32,32,32,32,43,32,43,0
title3        byte  43,32,43,32,43,32,43,32,43,32,43,32,43,32,43,43,32,43,43,43,0
title4        byte  43,32,43,32,43,32,43,32,43,32,43,32,43,32,32,43,32,32,43,0
title5        byte  43,43,32,32,43,43,43,32,43,43,32,32,32,43,43,32,32,32,43
blank_line    byte  32,0
title6        byte  43,43,32,43,32,43,43,43,43,32,43,32,32,43,32,32,43,43,43,0
title7        byte  43,43,43,43,32,43,43,32,43,32,43,43,32,43,32,43,43,0
title8        byte  43,43,43,32,32,43,43,32,43,32,43,43,43,43,32,43,43,32,43,43,0
title9        byte  43,43,32,43,32,43,43,32,43,32,43,32,43,43,32,43,43,32,32,43,0
title10       byte  43,43,32,43,32,43,43,43,43,32,43,32,32,43,32,32,43,43,43,0

role1         byte  " SOFTWARE PLUMBING:",0
credit1       byte  "   STEVE WADDICOR",0
role2         byte  "  SOUND WRANGLING:",0
credit2       byte  "     ERIC MOYER",0
players       byte  "   PLAYERS: 1 2",0
press_start   byte  "   ",45,"PRESS START",45,0
player1       byte  "PLAYER 1",0
player2       byte  "PLAYER 2",0
string1up     byte  "1UP",0     
string2up     byte  "2UP",0     
stringzero    byte  "000000",0
lequals       byte  "L",46,"01",0
bonusbox1     byte  gfx#TILE_BONUS,gfx#TILE_BONUS+1,gfx#TILE_BONUS+2,gfx#TILE_BONUS+3,gfx#TILE_BONUS+4,gfx#TILE_BONUS+5,0
bonusbox2     byte  gfx#TILE_BONUS+6,20,20,20,20,gfx#TILE_BONUS+7,0
bonusbox3     byte  gfx#TILE_BONUS+8,gfx#TILE_BONUS+9,gfx#TILE_BONUS+9,gfx#TILE_BONUS+9,gfx#TILE_BONUS+9,gfx#TILE_BONUS+10,0
how_high      byte  "HOW HIGH CAN YOU GET ",64,0

DAT
level1def     byte      DRAW_USE_TILE,gfx#TILE_FULL_LADDER
              byte      DRAW_VERTICALS
              'Kong's ladders

              byte      8,0,6
              byte      10,0,6
              byte      16,4,6
              'Other ladders
              byte      23,7,9
              byte      11,7,7
              byte      11,9,10
              byte      4,11,13
              byte      9,11,14
              byte      21,11,11
              byte      21,14,14
              byte      23,16,18
              byte      14,15,18
              byte      8,15,15
              byte      8,18,19
              byte      4,20,22
              byte      12,19,22
              byte      23,24,26
              byte      10,23,23
              byte      10,26,26
              byte      DRAW_USE_TILE,gfx#TILE_INVISIBLE_LADDER
              byte      DRAW_VERTICALS
              byte      21,13,13
              byte      8,17,17
              byte      10,25,25
              'Pauline's platform
              byte      DRAW_PLATFORM_FLAT,11,16,$18
              'Kong's platform
              byte      DRAW_PLATFORM_FLAT,0,17,$34
              byte      DRAW_PLATFORM_DOWNWARDS,18,25,$35
             'Platforms in between
              byte      DRAW_PLATFORM_UPWARDS,2,27,$59
              byte      DRAW_PLATFORM_DOWNWARDS,0,25,$6e
              byte      DRAW_PLATFORM_UPWARDS,2,27,$9b
              byte      DRAW_PLATFORM_DOWNWARDS,0,25,$b0
              'Mario's plaform
              byte      DRAW_PLATFORM_FLAT,0,13,$d8
              byte      DRAW_PLATFORM_UPWARDS,14,27,$d7
              byte      DRAW_END_DRAWING

DAT
level2def
              'platforms
              byte      DRAW_USE_TILE,gfx#TILE_FULL_PLATFORM
              byte      DRAW_HORIZONTALS
              byte      0,27,27
              byte      0,2,25
              byte      0,2,20
              byte      0,2,13
              byte      7,10,13
              byte      8,10,22
              byte      15,17,25
              byte      19,20,24
              byte      22,23,23
              byte      25,27,22
              byte      26,27,19
              byte      23,24,18
              byte      20,21,17
              byte      17,18,16
              byte      23,27,14
              byte      16,18,12
              byte      20,21,11
              byte      23,24,10
              byte      26,27,9
              byte      0,20,7
              byte      0,20,7
              byte      11,16,3
              'Ladders
              byte      DRAW_USE_TILE,gfx#TILE_FULL_LADDER
              byte      DRAW_VERTICALS
              byte      8,0,6
              byte      10,0,6
              byte      16,4,6
              byte      20,8,10
              byte      26,10,13
              byte      17,13,15
              byte      23,15,17
              byte      26,20,21
              byte      1,21,24
              byte      2,14,19
              byte      8,14,21
              byte      10,14,21
              'Poles
              byte      DRAW_USE_TILE,gfx#TILE_POLE
              byte      DRAW_VERTICALS
              byte      4,8,26
              byte      12,8,26
              byte      DRAW_USE_TILE,gfx#TILE_POLE+1
              byte      DRAW_VERTICALS
              byte      5,8,26
              byte      13,8,26
              byte      DRAW_END_DRAWING
DAT
level3def
              'platforms
              byte      DRAW_USE_TILE,gfx#TILE_FULL_PLATFORM
              byte      DRAW_HORIZONTALS
              byte      11,16,3
              byte      1,5,17
              byte      8,18,17
              byte      21,26,17
              byte      0,27,27
              byte      DRAW_USE_TILE,gfx#TILE_LEVEL_3_PLATFORM
              byte      DRAW_HORIZONTALS
              byte      0,27,7
              byte      0,27,12
              byte      0,27,22
              'kong ladders
              byte      DRAW_USE_TILE,gfx#TILE_FULL_LADDER
              byte      DRAW_VERTICALS
              byte      8,0,6
              byte      10,0,6
              byte      16,4,6
              'other ladders
              byte      2,10,11
              byte      25,10,11
              byte      10,13,16
              byte      17,13,16
              byte      8,18,21
              byte      18,18,21
              byte      3,23,26
              byte      10,23,26
              byte      17,23,26
              byte      24,23,26
              byte      3,13,16
              byte      24,13,16
              'trellis
              'byte      DRAW_USE_TILE,gfx#TILE_TRELLIS
              byte      DRAW_RECTANGLE,12,14,15,16,gfx#TILE_TRELLIS
              'byte      12,15,14
              'byte      12,15,15
              'byte      12,15,16
              byte      DRAW_END_DRAWING

DAT
level4def
    'platforms
              byte      DRAW_USE_TILE,gfx#TILE_LEVEL_4_PLATFORM
              byte      DRAW_HORIZONTALS
              byte      0,27,27
              byte      1,26,22
              byte      2,25,17
              byte      3,24,12
              byte      4,23,7
              byte      7,20,3
    'ladders
              byte      DRAW_USE_TILE,gfx#TILE_FULL_LADDER
              byte      DRAW_VERTICALS
              byte      1,23,26
              byte      13,23,26
              byte      26,23,26
              byte      2,18,21
              byte      9,18,21
              byte      18,18,21
              byte      25,18,21
              byte      3,13,16
              byte      13,13,16
              byte      24,13,16
              byte      4,8,11
              byte      8,8,11
              byte      19,8,11
              byte      23,8,11
    'poles
              byte      DRAW_USE_TILE,gfx#TILE_POLE
              byte      DRAW_VERTICALS
              byte      8,4,6
              byte      18,4,6
              byte      DRAW_USE_TILE,gfx#TILE_POLE+1
              byte      DRAW_VERTICALS
              byte      9,4,6
              byte      19,4,6
    'rivets
              byte      DRAW_USE_TILE,gfx#TILE_BOTTOM_OF_RIVET
              byte      DRAW_VERTICALS
              byte      7,7,7
              byte      20,7,7
              byte      7,12,12
              byte      20,12,12
              byte      7,17,17
              byte      20,17,17
              byte      7,22,22
              byte      20,22,22
              byte      DRAW_USE_TILE,gfx#TILE_TOP_OF_RIVET
              byte      DRAW_VERTICALS
              byte      7,6,6
              byte      20,6,6
              byte      7,11,11
              byte      20,11,11
              byte      7,16,16
              byte      20,16,16
              byte      7,21,21
              byte      20,21,21
              byte      DRAW_END_DRAWING

DAT
climbdef0
              byte      DRAW_USE_TILE,gfx#TILE_FULL_LADDER
              byte      DRAW_VERTICALS
              byte      8,0,6
              byte      10,0,6
              byte      16,4,7
              byte      14,7,7
              'Pauline's platform
              byte      DRAW_PLATFORM_FLAT,11,16,$18
              'Kong's platform
              byte      DRAW_PLATFORM_FLAT,0,25,$34
              'Platforms in between
              byte      DRAW_PLATFORM_FLAT,2,27,$4d
              byte      DRAW_PLATFORM_FLAT,0,25,$6e
              byte      DRAW_PLATFORM_FLAT,2,27,$8f
              byte      DRAW_PLATFORM_FLAT,0,25,$b0
              'Mario's plaform
              byte      DRAW_PLATFORM_FLAT,0,27,$d0
                'long ladders
              byte      DRAW_VERTICALS
              byte      16,8,25
              byte      14,8,25
              byte      DRAW_END_DRAWING

climbdef72a   byte      DRAW_PLATFORM_FLAT,14,16,$6e
              byte      DRAW_END_DRAWING

climbdef72b   byte      DRAW_PLATFORM_FLAT,14,16,$8f
              byte      DRAW_END_DRAWING

climbdef72c   byte      DRAW_PLATFORM_FLAT,14,16,$b0
              byte      DRAW_END_DRAWING

climbdef72d   byte      DRAW_PLATFORM_FLAT,14,16,$d0
              byte      DRAW_END_DRAWING

climbdef72e   byte      DRAW_USE_TILE,gfx#TILE_SPACE
              byte      DRAW_HORIZONTALS
              byte      14,16
climbdef72remove byte   0
              byte      DRAW_END_DRAWING

climbdef390   byte      DRAW_USE_TILE,gfx#TILE_SPACE
              byte      DRAW_VERTICALS
              byte      16,7,12
              byte      14,7,12
              byte      DRAW_PLATFORM_FLAT,14,16,$4d
              byte      DRAW_PLATFORM_FLAT,14,16,$34
              byte      DRAW_HORIZONTALS
              byte      24,25,6
              byte      DRAW_PLATFORM_DOWNWARDS,18,25,$35
              byte      DRAW_END_DRAWING

climbdef456   byte      DRAW_RECTANGLE,2,9,21,10,gfx#TILE_SPACE
              byte      DRAW_PLATFORM_UPWARDS,2,27,$59
              byte      DRAW_END_DRAWING

climbdef490   byte      DRAW_RECTANGLE,4,13,25,14,gfx#TILE_SPACE
              byte      DRAW_PLATFORM_DOWNWARDS,0,25,$6e
              byte      DRAW_END_DRAWING

climbdef524   byte      DRAW_RECTANGLE,2,17,25,18,gfx#TILE_SPACE
              byte      DRAW_PLATFORM_UPWARDS,2,27,$9b
              byte      DRAW_END_DRAWING

climbdef558   byte      DRAW_USE_TILE,gfx#TILE_SPACE
              byte      DRAW_HORIZONTALS
              byte      16,25,22
              byte      DRAW_PLATFORM_DOWNWARDS,0,25,$b0
              byte      DRAW_END_DRAWING

climbdef592   byte      DRAW_USE_TILE,gfx#TILE_SPACE
              byte      DRAW_HORIZONTALS
              byte      0,13,26
              byte      DRAW_PLATFORM_FLAT,0,13,$d8
              byte      DRAW_PLATFORM_UPWARDS,14,27,$d7
              byte      DRAW_END_DRAWING

DAT
level4end1    byte      DRAW_RECTANGLE,8,7,19,22,gfx#TILE_SPACE
              byte      DRAW_RECTANGLE,8,23,19,26,gfx#TILE_LEVEL_4_PLATFORM
              byte      DRAW_END_DRAWING

level4end2    byte      DRAW_RECTANGLE,7,3,20,6,gfx#TILE_SPACE
              byte      DRAW_USE_TILE,gfx#TILE_LEVEL_4_PLATFORM
              byte      DRAW_HORIZONTALS
              byte      7,20,7
              byte      DRAW_END_DRAWING

DAT
' There seems to be some pointer leakage here.  Extra long as a patch.
                        long    0
' Hub scanline buffer = 256 pixels, 8 bits per pixel = 2048 bits = 256 bytes = 64 longs
hub_scanline_buffer
colorbar0               long    $0b_0b_0b_0b
colorbar1               long    $0c_0c_0c_0c
colorbar2               long    $0d_0d_0d_0d
colorbar3               long    $0e_0e_0e_0e
colorbar4               long    $1b_0b_1b_1b
colorbar5               long    $1c_0c_1c_1c
colorbar6               long    $1d_0d_1d_1d
colorbar7               long    $1e_0e_1e_1e
colorbar8               long    $2b_0b_2b_2b
colorbar9               long    $2c_2c_2c_2c
colorbar10              long    $2d_2d_2d_2d
colorbar11              long    $2e_2e_2e_2e
colorbar12              long    $3b_3b_3b_3b
colorbar13              long    $3c_3c_3c_3c
colorbar14              long    $3d_3d_3d_3d
colorbar15              long    $3e_3e_3e_3e
colorbar16              long    $4b_4b_4b_4b
colorbar17              long    $4c_4c_4c_4c
colorbar18              long    $4d_4d_4d_4d
colorbar19              long    $4e_4e_4e_4e
colorbar20              long    $5b_5b_5b_5b
colorbar21              long    $5c_5c_5c_5c
colorbar22              long    $5d_5d_5d_5d
colorbar23              long    $5e_5e_5e_5e
colorbar24              long    $6b_6b_6b_6b
colorbar25              long    $6c_6c_6c_6c
colorbar26              long    $6d_6d_6d_6d
colorbar27              long    $6e_6e_6e_6e
colorbar28              long    $7b_7b_7b_7b
colorbar29              long    $7c_7c_7c_7c
colorbar30              long    $7d_7d_7d_7d
colorbar31              long    $7e_7e_7e_7e
colorbar32              long    $8b_8b_8b_8b
colorbar33              long    $8c_8c_8c_8c
colorbar34              long    $8d_8d_8d_8d
colorbar35              long    $8e_8e_8e_8e
colorbar36              long    $9b_9b_9b_9b
colorbar37              long    $9c_9c_9c_9c
colorbar38              long    $9d_9d_9d_9d
colorbar39              long    $9e_9e_9e_9e
colorbar40              long    $ab_ab_ab_ab
colorbar41              long    $ac_ac_ac_ac
colorbar42              long    $ad_ad_ad_ad
colorbar43              long    $ae_ae_ae_ae
colorbar44              long    $bb_bb_bb_bb
colorbar45              long    $bc_bc_bc_bc
colorbar46              long    $bd_bd_bd_bd
colorbar47              long    $be_be_be_be
colorbar48              long    $cb_cb_cb_cb
colorbar49              long    $cc_cc_cc_cc
colorbar50              long    $cd_cd_cd_cd
colorbar51              long    $ce_ce_ce_ce
colorbar52              long    $db_db_db_db
colorbar53              long    $dc_dc_dc_dc
colorbar54              long    $dd_dd_dd_dd
colorbar55              long    $de_de_de_de
colorbar56              long    $eb_eb_eb_eb
colorbar57              long    $ec_ec_ec_ec
colorbar58              long    $ed_ed_ed_ed
colorbar59              long    $ee_ee_ee_ee
colorbar60              long    $fb_fb_fb_fb
colorbar61              long    $fc_fc_fc_fc
colorbar62              long    $fd_fd_fd_fd
colorbar63              long    $fe_fe_fe_fe
' End of scanline buffer

scanline_req            long    0

DAT

_SONG_bg_rivets_level
        byte    $03, $EF, $AD, $79, $35          'Instrument 0
        byte    $FF                              'End of Instrument list
        byte    $00, $00, $93, $07, $0E
        byte    $1C, $00, $75, $07, $0E
        byte    $0E, $00, $75, $07, $0E
        byte    $0E, $00, $75, $01, $01          'Blank note for loop timing
        byte    $ff

_SONG_bg_girders_level
'_song_data
        byte    $03, $EF, $AD, $79, $35          'Instrument 0
        byte    $FF                              'End of Instrument list
        byte    $00, $00, $75, $0A, $10
        byte    $2F, $00, $93, $08, $10
        byte    $20, $00, $AF, $08, $10
        byte    $10, $00, $C4, $08, $10
        byte    $10, $00, $AF, $08, $10
        byte    $10, $00, $AF, $01, $01          'Blank note for loop timing
        byte    $ff

_SONG_start
        byte    $03, $FF, $FF, $FF, $FF          'Instrument 0
        byte    $FF                              'End of Instrument list
        byte    $00, $00, $83, $09, $5C
        byte    $00, $20, $62, $09, $5C
        byte    $5C, $00, $6E, $09, $1F
        byte    $00, $20, $93, $09, $1E
        byte    $1E, $20, $75, $0B, $3E
        byte    $00, $40, $9C, $0B, $3D
        byte    $3E, $00, $62, $09, $3D
        byte    $00, $20, $83, $09, $3D
        byte    $3D, $01, $88, $04, $08
        byte    $00, $20, $B9, $0A, $7A
        byte    $07, $41, $72, $04, $08
        byte    $08, $01, $88, $04, $08
        byte    $08, $01, $72, $04, $08
        byte    $07, $41, $88, $04, $08
        byte    $08, $01, $72, $04, $08
        byte    $08, $01, $88, $04, $08
        byte    $07, $41, $72, $04, $08
        byte    $08, $01, $88, $04, $08
        byte    $08, $01, $72, $04, $08
        byte    $07, $41, $88, $04, $08
        byte    $08, $01, $72, $04, $08
        byte    $08, $01, $88, $04, $08
        byte    $07, $41, $72, $04, $08
        byte    $08, $01, $88, $04, $08
        byte    $08, $01, $72, $04, $08
        byte    $07, $21, $88, $06, $7A
        byte    $00, $40, $C4, $0A, $7A
        byte    $ff

_SONG_hammer
        byte    $04, $EF, $AD, $79, $35          'Instrument 0
        byte    $FF                              'End of Instrument list
        byte    $00, $00, $E9, $08, $09
        byte    $11, $00, $E9, $08, $09
        byte    $09, $00, $E9, $08, $09
        byte    $08, $20, $E9, $08, $09
        byte    $11, $00, $E9, $08, $09
        byte    $12, $01, $26, $08, $11
        byte    $11, $00, $E9, $08, $09
        byte    $11, $01, $26, $08, $11
        byte    $11, $00, $E9, $08, $09
        byte    $11, $01, $26, $08, $09
        byte    $12, $01, $26, $08, $09
        byte    $08, $21, $26, $08, $09
        byte    $09, $01, $26, $08, $09
        byte    $11, $01, $26, $08, $09
        byte    $11, $01, $5D, $08, $11
        byte    $12, $01, $26, $08, $09
        byte    $11, $01, $5D, $08, $11
        byte    $11, $01, $26, $08, $08
        byte    $13, $01, $26, $01, $01          'Blank note for loop timing
        byte    $ff

_SONG_how_high
        byte    $03, $EF, $AD, $79, $35          'Instrument 0
        byte    $FF                              'End of Instrument list
        byte    $00, $00, $AF, $08, $12
        byte    $00, $21, $06, $08, $1B
        byte    $25, $00, $83, $08, $12
        byte    $00, $21, $26, $08, $12
        byte    $12, $01, $5D, $08, $25
        byte    $12, $20, $AF, $08, $12
        byte    $12, $21, $26, $08, $12
        byte    $13, $00, $83, $08, $12
        byte    $00, $21, $06, $08, $12
        byte    $12, $01, $26, $08, $12
        byte    $12, $00, $75, $08, $12
        byte    $00, $20, $E9, $08, $25
        byte    $13, $00, $75, $08, $09
        byte    $09, $00, $83, $08, $09
        byte    $09, $00, $93, $08, $09
        byte    $09, $00, $AF, $08, $09
        byte    $09, $00, $C4, $08, $09
        byte    $09, $00, $DC, $08, $09
        byte    $0A, $00, $E9, $08, $09
        byte    $ff

_SONG_level1_complete
        byte    $02, $EF, $AD, $79, $35          'Instrument 0
        byte    $06, $EF, $AD, $79, $35          'Instrument 1
        byte    $FF                              'End of Instrument list
        byte    $00, $01, $26, $08, $0B
        byte    $00, $21, $5D, $08, $0B
        byte    $0B, $01, $4A, $08, $0B
        byte    $00, $21, $88, $08, $0B
        byte    $0A, $41, $5D, $08, $15
        byte    $00, $61, $B8, $08, $15
        byte    $16, $01, $26, $08, $15
        byte    $00, $21, $5D, $08, $15
        byte    $2B, $01, $26, $08, $0B
        byte    $00, $21, $5D, $08, $0B
        byte    $0A, $41, $4A, $08, $0B
        byte    $00, $61, $88, $08, $0B
        byte    $0B, $01, $5D, $08, $15
        byte    $00, $21, $B8, $08, $15
        byte    $16, $01, $26, $08, $15
        byte    $00, $21, $5D, $08, $15
        byte    $3A, $01, $26, $2F, $20
        byte    $3B, $01, $26, $2F, $20
        byte    $3B, $01, $26, $2F, $20
        byte    $ff

_SONG_death
        byte    $02, $EF, $AD, $79, $35          'Instrument 0
        byte    $04, $FF, $FF, $FF, $FF          'Instrument 1
        byte    $FF                              'End of Instrument list
        byte    $00, $03, $70, $0C, $05
        byte    $05, $05, $27, $0C, $05
        byte    $06, $04, $55, $0C, $05
        byte    $05, $06, $7D, $0C, $05
        byte    $05, $04, $17, $0C, $05
        byte    $06, $06, $20, $0C, $05
        byte    $05, $03, $DC, $0C, $05
        byte    $05, $05, $C8, $0C, $05
        byte    $06, $03, $A4, $0C, $05
        byte    $05, $05, $75, $0C, $05
        byte    $06, $03, $70, $0C, $05
        byte    $05, $05, $27, $0B, $05
        byte    $05, $03, $3F, $0B, $05
        byte    $06, $04, $DD, $0B, $05
        byte    $05, $03, $10, $0B, $05
        byte    $05, $04, $97, $0B, $05
        byte    $06, $02, $E4, $0B, $05
        byte    $05, $04, $55, $0B, $05
        byte    $05, $02, $BA, $0B, $05
        byte    $06, $04, $17, $0B, $05
        byte    $05, $02, $93, $0B, $05
        byte    $05, $03, $DC, $0B, $05
        byte    $06, $02, $6E, $0B, $05
        byte    $05, $03, $A4, $0B, $05
        byte    $06, $02, $4B, $0B, $05
        byte    $05, $03, $70, $0B, $05
        byte    $05, $02, $2A, $0B, $05
        byte    $06, $03, $3F, $0B, $05
        byte    $0A, $00, $93, $28, $15
        byte    $16, $00, $75, $28, $2B
        byte    $00, $20, $AF, $28, $2B
        byte    $2B, $00, $3A, $28, $2B
        byte    $00, $20, $57, $28, $2B
        byte    $2A, $40, $3A, $28, $2B
        byte    $00, $60, $75, $28, $2B
        byte    $ff

_SONG_jumpbarrel
        byte    $03, $EF, $AD, $79, $35          'Instrument 0
        byte    $FF                              'End of Instrument list
        byte    $00, $01, $D2, $0F, $0A
        byte    $0A, $02, $0B, $0F, $0A
        byte    $0A, $02, $4B, $0F, $0A
        byte    $0A, $03, $10, $0F, $14
        byte    $14, $02, $6E, $0F, $14
        byte    $ff

_SONG_rivet1_completion_1
        byte    $03, $EF, $AD, $79, $35          'Instrument 0
        byte    $FF                              'End of Instrument list
        byte    $08, $00, $AF, $08, $09
        byte    $00, $20, $E9, $08, $09
        byte    $ff

_SONG_rivet1_completion_2
        byte    $03, $EF, $AD, $79, $35          'Instrument 0
        byte    $FF                              'End of Instrument list
        byte    $11, $00, $57, $08, $09
        byte    $12, $00, $49, $08, $09
        byte    $ff

'3 insturuments were declared but song only uses 2. Moved inst 2 to inst 1, got rid
'of old (unused) inst 1, and fixed the one note that uses inst 1 to point to it
'accordingly.
_SONG_rivet1_completion_3
        byte    $03, $EF, $AD, $79, $35          'Instrument 0
        byte    $06, $EF, $AD, $79, $35          'Instrument 1
        byte    $FF                              'End of Instrument list
        byte    $00, $00, $E9, $29, $25
        byte    $7B, $00, $3A, $09, $12
        byte    $24, $01, $5D, $09, $23
        byte    $00, $21, $B8, $09, $23
        byte    $23, $01, $88, $09, $23
        byte    $00, $21, $D2, $09, $23
        byte    $23, $01, $B8, $09, $23
        byte    $00, $22, $0B, $09, $23
        byte    $23, $00, $75, $09, $12
        byte    $24, $00, $57, $09, $12
        byte    $00, $22, $4B, $09, $23
        byte    $23, $00, $75, $09, $12
        byte    $00, $22, $2A, $09, $12
        byte    $12, $02, $4B, $09, $12
        byte    $11, $20, $57, $09, $12
        byte    $24, $01, $88, $09, $23
        byte    $00, $20, $62, $09, $12
        byte    $23, $00, $49, $09, $12
        byte    $00, $23, $10, $09, $23
        byte    $23, $02, $BA, $09, $12
        byte    $00, $20, $62, $09, $12
        byte    $12, $02, $4B, $09, $12
        byte    $12, $00, $49, $09, $12
        byte    $23, $00, $41, $09, $12
        byte    $00, $22, $0B, $09, $23
        byte    $23, $00, $52, $09, $12
        byte    $00, $22, $0B, $09, $23
        byte    $23, $00, $57, $09, $12
        byte    $00, $22, $BA, $09, $0E
        byte    $14, $02, $2A, $09, $09
        byte    $0B, $02, $4B, $09, $11
        byte    $05, $20, $49, $09, $12
        byte    $10, $01, $D2, $09, $0B
        byte    $13, $00, $3A, $09, $12
        byte    $00, $20, $57, $09, $12
        byte    $12, $00, $41, $09, $12
        byte    $00, $20, $62, $09, $12
        byte    $27, $00, $68, $09, $12
        byte    $00, $20, $3A, $09, $12
        byte    $ff


_SONG_out_of_time
        byte    $05, $FF, $FF, $FF, $FF          'Instrument 0
        byte    $FF                              'End of Instrument list
        byte    $00, $00, $AF, $07, $09
        byte    $11, $00, $93, $04, $09
        byte    $11, $00, $75, $0A, $09
        byte    $11, $00, $01, $01, $01          'Blank note for loop timing
        byte    $ff

 