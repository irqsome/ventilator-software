'' /////////////////////////////////////////////////////////////////////////////
'' TV Driver for Donkey Kong Remake
'' (C) 2007 Steve Waddicor
'  Derived from SimpleNTSC_256x192x256_colors_002.spin 
'' //////////////////////////////////////////////////////////////////////////////

VAR
  long  cogon, cog


PUB start(tvptr) : okay

'' Start TV driver - starts a cog
'' returns false if no cog available
''
''   tvptr = pointer to TV parameters

  stop
  okay := cogon := (cog := cognew(@entry,tvptr)) > 0


PUB stop

'' Stop TV driver - frees a cog

  if cogon~
    cogstop(cog)

DAT                     org
entry                   jmp     #initialization         'Jump past the constants

' I want to explain some important constants up front, so I'll declare them here.
' Ordinarily they'd be tucked away after the code.  There is also a lot of skipping
' arounf between CON and DAT sections.  I'm declaring them in the order I can best explain them.
'
' I've used some naming conventions.  These prefixes mean:
' NTSC_ :   The value is a constant of the NTSC system. They would probably be the same no matter
'           what resolution or color mode you are using.  You'd probably only change them if you were
'           converting to PAL.  In which case, good luck to you.
' CHOOSE_ : A choice was made for this particular resolution and color depth.  You might change these.
' CALC_     These were calculated from one or more of the choices you made.  Choose again and you may
'           have to change these.

'***************************************************
'* Color Frequency                                 *
'***************************************************
'
' The NTSC output is a signal with various elements that need timing relative to each other.  So we need some
' sort of clock from which to time all the elements.  The frequency of the color carrier is useful.  This is
' 3.579545 Mhz. A cycle of this clock will take 1sec/3.579545MHz = 279.365ns.

CON  NTSC_color_frequency       =     3_579_545
DAT  NTSC_color_freq            long  NTSC_color_frequency

' Additionally this period of 279ns is divided up into 16 by a Phased Locked Loop circuit (PLL), and it is multiples
' this period that the Video Scale Hardware Register (VSCL) is programmed.  This period of 17.460313ns is called a
' "clock".



'***************************************************
'* Horizontal sync                                 *
'***************************************************
'* A hsync takes 10.9us.  That's 10.9us/17.460313ns = 624 clocks (rounded to an integer).
CON  NTSC_hsync_clocks          =               624

' We are going to send each horizontal sync with a single WAITVID.  We'll aproximate the waveform with
' 16 x 4 "color" pixels.  So we need to program the Video Scale register VSCL.  That looks like this:
'
' %xxxx_xxxx_xxxx_________________________ : Not used
' %_______________PPPP_PPPP_______________ : Clocks per pixel (CPP)
' %_________________________FFFF_FFFF_FFFF : Clocks per frame (CPF)
'
' The number of pixels we get is CPF/CPP.  So for this usage CPS is 624/16 = 39.
DAT  NTSC_hsync_VSCL            long  NTSC_hsync_clocks >> 4 << 12 + NTSC_hsync_clocks

' The hsync is made up of 3 different sorts of signal.  We put them into a palette:
' Color0 = Color NTSC_control_signal_palette (yellow hue at zero value) = $8a
' Color1 = Black = $02      
' Color2 = Blanking level = $00
' Color4 = don't care
' Note that the palette is programmed in reverse order:
DAT  NTSC_control_signal_palette long  $00_00_02_8a

' Then we construct a hsync out of 16 pixels of those control signals.
' Note that this is shifted out of the VSU to the right, so LSB goes first.
DAT  NTSC_hsync_pixels          long  %%11_0000_1_2222222_11



'***************************************************
'* Blank lines                                     *
'***************************************************
' The rest of the line after a hsync takes 52.6us.  That's 3008 clocks (to the nearest multiple of 16).
CON  NTSC_active_video_clocks   =     3008

' For blank lines, were' going to programme the VSCL so it slowly outputs 16 pixels over the entire
' length of the active line.  It's all going to be one colour so we don't even need to programme the
' clocks per pixel part, just the clocks per frame.
DAT  NTSC_active_video_VSCL     long  NTSC_active_video_clocks



'***************************************************
'* User graphics lines                             *
'***************************************************
' The important lines at last.  To fit 256 pixels in, we're going to make them 10 clocks each.
' That's 2560 clocks for the user graphics width.  You could use 11 clocks each or some other near value.
' It depends whether you want overscan to the left and right.  
CON CHOOSE_clocks_per_gfx_pixel = 10

' Because we're going to use 256 color mode, we're only going to output 4 pixels per WAITVID.  So that
' decides the number of clocks per frame.  4.
CON CALC_clocks_per_gfx_frame   =     CHOOSE_clocks_per_gfx_pixel*4

' Program the VSCL as before.
DAT CALC_user_data_VSCL         long  CHOOSE_clocks_per_gfx_pixel << 12 + CALC_clocks_per_gfx_frame

' So if we're doing 256 pixels, 4 at a time, that's 256/4 = 64 frames.  64 WAITVIDS.
CON CALC_frames_per_gfx_line    = 256/4

' There is some extra active video that is not used for user graphics.  This is the overscan.
CON CALC_overscan = NTSC_active_video_clocks-CALC_frames_per_gfx_line*CALC_clocks_per_gfx_frame

' It maybe that your TV doesn't centre the picture.  We can tweak the overscan left and right to make
' it balance.  28 works quite well on my TV.  It may well be different for yours.
CON CHOOSE_horizontal_offset    = 0

' So we can work out how much overscan to do left (backporch) and right (frontporch).
CON CALC_backporch = CALC_overscan/2+CHOOSE_horizontal_offset
CON CALC_frontporch = CALC_overscan - CALC_overscan/2-CHOOSE_horizontal_offset

DAT

'***************************************************
'* The code                                        *
'***************************************************
' Start of real code

initialization          ' VCFG: setup Video Configuration register and 3-bit tv DAC pins to output
                        movs    VCFG, #%0111_0000       ' VCFG'S = pinmask (pin31: 0000_0111 : pin24)
                        movd    VCFG, #1                ' VCFG'D = pingroup (grp. 3 i.e. pins 24-31)

                        movi    VCFG, #%0_11_111_000    ' baseband video on bottom nibble, 2-bit color, enable chroma on broadcast & baseband
                                                        ' %0_xx_x_x_x_xxx : Not used
                                                        ' %x_10_x_x_x_xxx : Composite video to top nibble, broadcast to bottom nibble
                                                        ' %x_xx_1_x_x_xxx : 4 color mode
                                                        ' %x_xx_x_1_x_xxx : Enable chroma on broadcast
                                                        ' %x_xx_x_x_1_xxx : Enable chroma on baseband
                                                        ' %x_xx_x_x_x_000 : Broadcast Aural FM bits (don't care)

                        or      DIRA, tvport_mask       ' set DAC pins to output

                        ' 
                        or      DIRA, #1                ' enable debug LED
                        'mov     OUTA, #1                ' light up debug LED

                        ' CTRA: setup Frequency to Drive Video
                        movi    CTRA,#%00001_111        ' pll internal routed to Video, PHSx+=FRQx (mode 1) + pll(16x)
                        mov     r1, NTSC_color_freq     ' r1: Color frequency in Hz (3.579_545MHz)
                        rdlong  v_clkfreq, #0           ' copy clk frequency. (80Mhz)
                        mov     r2, v_clkfreq           ' r2: CLKFREQ (80MHz)
                        call    #dividefract            ' perform r3 = 2^32 * r1 / r2
                        mov     v_freq, r3              ' v_freq now contains frqa.       (191)
                        mov     FRQA, r3                ' set frequency for counter
'-----------------------------------------------------------------------------
'                       'Set up parameters
                        '  PAR points to a small block:
                        '    Long 0: Next line to be displayed.   TV Driver -> Caller
                        '    Long 1: Address of scanline buffer.  TV Driver <- Caller
                        '                          
                        mov     next_line_adr,PAR
                        rdlong  next_line_adr, next_line_adr
                        mov     scanline_buffer_adr,PAR
                        add     scanline_buffer_adr,#4
                        rdlong  scanline_buffer_adr, scanline_buffer_adr
'-----------------------------------------------------------------------------
frame_loop
                        mov     current_line,#0
                        'NTSC has 244 "visible" lines, but some of that is overscan etc.
                        '  so pick a number of lines for user graphics e.g. 192
                        '  then display (244-192)/2 = 26 lines of overscan before
                        '  and after the user display.
                        mov     line_loop, #15          '(10 so far)
                        'There's always a horizontal sync at the start of each line.  The clever stuff is
                        '  in the constants described earlier.
:vert_back_porch        mov     VSCL, NTSC_hsync_VSCL
                        waitvid NTSC_control_signal_palette, hsync
                        'This is just a blank line.  I've made it brown just to show it's there
                        '  but you'd probably want it black.  Notice how the whole line (apart from the
                        '  hsyc is output with one WAITVID.
                        mov     VSCL, NTSC_active_video_VSCL
                        waitvid black_border_in_color0, #0
                        djnz    line_loop, #:vert_back_porch
'-----------------------------------------------------------------------------
                        'Time to do the user graphics lines.  We're having 192 of them.
                        mov     line_loop, #224         '(234 so far)
                        'hsync                        
user_graphics_lines     mov     VSCL, NTSC_hsync_VSCL
                        waitvid NTSC_control_signal_palette, hsync
                        'The overscan before the user graphics.  Green in this case. 
                        mov     VSCL,#CALC_backporch
                        waitvid black_border_in_color0, #0 

                        'And now at long last, the user graphics.  Program the VSCL for
                        '  4 pixels per frame.  
                        mov     VSCL, CALC_user_data_VSCL

                        ' The meat of the driver.  Transfer the scanbuffer data to the TV.
                        mov     ptr,scanline_buffer_adr
                        mov     tile_loop,#64
:loop                   rdlong  pixels,ptr
                        add     ptr,#4
                        waitvid pixels,#%%3210
                        djnz    tile_loop,#:loop

                        'Even before we do the overscan, we can inform the caller that the
                        '  we are ready for the next line
                        add     current_line,#1
                        cmp     current_line,#224  wz,wc
              if_e      mov     current_line,#0
                        wrlong  current_line,next_line_adr
                        
                        'We've finished drawing a user graphics line, whether it was color
                        '  bar or flag.  Now do the overscan bit.  This time in blue.                        
end_of_flag_line        mov    VSCL,#CALC_frontporch
                        waitvid black_border_in_color0 , border 
                        'loop
                        djnz    line_loop, #user_graphics_lines
'-----------------------------------------------------------------------------
                        'We've finished the visible part of the screen so tell the app
                        'that there's a vsync
                        mov     r0,PAR
                        add     r0,#8
                        wrlong  vsync_flag,r0                        
'-----------------------------------------------------------------------------
                        'Overscan at the bottom of screen.  Same as top of screen, but
                        '  this time in magenta.
                        mov     line_loop, #5          '(244)
                        'hsync
vert_front_porch        mov     VSCL, NTSC_hsync_VSCL
                        waitvid NTSC_control_signal_palette, hsync
                        mov     VSCL, NTSC_active_video_VSCL
                        waitvid black_border_in_color0, #0
                        djnz    line_loop, #vert_front_porch
'-----------------------------------------------------------------------------
                        'This is the vertical sync.  It consists of 3 sections of 6 lines each.
                        mov     line_loop, #6           '(250)
:vsync_higha            mov     VSCL, NTSC_hsync_VSCL
                        waitvid NTSC_control_signal_palette, vsync_high_1
                        mov     VSCL, NTSC_active_video_VSCL
                        waitvid NTSC_control_signal_palette, vsync_high_2
                        djnz    line_loop, #:vsync_higha
'-----------------------------------------------------------------------------
                        mov     line_loop, #6           '(256)
:vsync_low              mov     VSCL, NTSC_hsync_VSCL
                        waitvid NTSC_control_signal_palette, vsync_low_1
                        mov     VSCL, NTSC_active_video_VSCL
                        waitvid NTSC_control_signal_palette, vsync_low_2
                        djnz    line_loop, #:vsync_low
'-----------------------------------------------------------------------------
                        mov     line_loop, #6           '(250)
:vsync_highb            mov     VSCL, NTSC_hsync_VSCL
                        waitvid NTSC_control_signal_palette, vsync_high_1
                        mov     VSCL, NTSC_active_video_VSCL
                        waitvid NTSC_control_signal_palette, vsync_high_2
                        djnz    line_loop, #:vsync_highb
'-----------------------------------------------------------------------------
                        'mov     OUTA, #0

                        jmp     #frame_loop

' /////////////////////////////////////////////////////////////////////////////
' dividefract:
' Perform 2^32 * r1/r2, result stored in r3 (useful for TV calc)
' This is taken from the tv driver.
' NOTE: It divides a bottom heavy fraction e.g. 1/2 and gives the result as a 32-bit fraction.
' /////////////////////////////////////////////////////////////////////////////
dividefract                                     
                        mov     r0,#32+1
:loop                   cmpsub  r1,r2           wc
                        rcl     r3,#1
                        shl     r1,#1
                        djnz    r0,#:loop
dividefract_ret         ret                             '+140


' Video (TV) Registers
tvport_mask             long    %0111_0000<<8
v_freq                  long    0
v_clkfreq               long    0


'Pixel streams
'  These are shifted out of the VSU to the right, so lowest bits are actioned first.
'
hsync                   long    %%11_0000_1_2222222_11  ' Used with NTSC_control_signal_palette so:
                                                        '      0 = blanking level
                                                        '      1 = Black
                                                        '      2 = Color NTSC_control_signal_palette (yellow at zero value)
vsync_high_1            long    %%11111111111_222_11
vsync_high_2            long    %%1111111111111111
vsync_low_1             long    %%22222222222222_11
vsync_low_2             long    %%1_222222222222222
border                  long    %%0000000000000000

' Some unimportant irrelevant constants for generating demo user display etc.
line_loop               long    0
tile_loop               long    0
vsync_flag              long    $00000001

'Palettes
'  These are always 4 colors (or blanking level) stored in reverse order:
'                               Color3_Color2_Color1_Color0
'
black_border_in_color0  long    $00_00_00_02

pixels                  res     1
scanline_buffer_adr     res     1
ptr                     res     1
next_line_adr           res     1
current_line            res     1

' General Purpose Registers
r0                      res     1
r1                      res     1
r2                      res     1
r3                      res     1

                        fit

                                   