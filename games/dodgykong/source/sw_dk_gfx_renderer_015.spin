'' /////////////////////////////////////////////////////////////////////////////
'' Graphics Renderer for Donkey Kong Remake
'' (C) Steve Waddicor
'' //////////////////////////////////////////////////////////////////////////////

CON
  param_count   = 13
  sprites       = 35

VAR
  long  cog
  long  renderer_ptr_block

PUB start(renderer_ptr) : okay

'' Start renderer - starts a cog
'' returns false if no cog available

  renderer_ptr_block := renderer_ptr
  okay := (cog := cognew(@entry,renderer_ptr)) > 0

DAT

'******************************
'* Assembly language renderer *
'******************************

                        org
'
'
' Entry
'
entry
                        rdlong  rend_cog_number,PAR
                        mov     current_line,rend_cog_number  'start rendering from the top

do_frame                'Load parameters
                        mov     r0,PAR
                        movd    :load_dest,#renderer_params+1
                        mov     count,#param_count-1+sprites
:load                   add     r0,#4
                        '       ___
:load_dest              rdlong  0-0,r0
                        '       ^^^ iterates through renderer_params array.
                        add     :load_dest,inc_dest
                        djnz    count,#:load

do_line                 movd    scan_line_store1,#cog_scanline_buffer
                        movd    scan_line_store2,#cog_scanline_buffer+1
                        mov     hub_map_ptr,rend_map    'point to first tile (upper-leftmost)

                        ' Init tiles loop
                        mov     r0,current_line   'then move down to current line first tile     r0=tile_row
                        shr     r0,#3             '8 lines per tile
                        shl     r0,#6             '32 tiles per row + 32 palettes
                        add     hub_map_ptr,r0

                        mov     r0,current_line  'line offset within tile      r0=tile_line
                        and     r0,#$07          '8 lines per tile
                        shl     r0,#1            '2 bytes per line
                        mov     tile_table,rend_tile_table_ptr 'get a local copy of tiletable address
                        add     tile_table,r0    'offset it to read correct tile_line

                        mov     last_tile,#$1FF

                        mov     x,rend_ht               'set horizontal tiles

do_tile                 rdword  tile,hub_map_ptr        'read tile

                        ' Get tile palette
                        mov     r0,tile
                        and     r0,constantFF00
                        shr     r0,#6
                        add     r0,rend_tile_palette_ptr
                        rdlong  tile_palette,r0
                        cmp     tile_palette,last_tile_palette  wc,wz
              if_e      jmp     #:same_palette
                        mov     last_tile_palette,tile_palette

                        ' Prepare fast palette lookup
                        movd    :store_in_palette,#tile_palette_table
                        mov     r0,tile_palette         'r0=$AABBCCDD
                        mov     count,#4
:outer                  rol     r0,#8                   'r0=$BBCCDDAA
                        mov     r1,r0                   'r1=$BBCCDDAA
                        and     r1,#$FF                 'r1=$000000AA
                        mov     r2,tile_palette         'r2=$AABBCCDD
                        rol     r2,#8                   'r2=$BBCCDDAA
                        mov     count2,#4
:inner                  rol     r2,#8                   'r2=$CCDDAABB
                        mov     r3,r2                   'r3=$CCDDAABB
                        and     r3,constantFF00         'r3=$0000AA00
                        or      r3,r1                   'r3=$0000AAAA first time around, then change first AA in inner loop and last AA more slowly in outer loop
:store_in_palette       mov     0-0,r3
                        add     :store_in_palette,inc_dest
                        djnz    count2,#:inner
                        djnz    count,#:outer

                        ' Get tile pixels
:same_palette           and     tile,#$FF
                        cmp     tile,last_tile  wc,wz
              if_e      jmp     #scan_line_store1
                        mov     last_tile,tile
                        
                        shl     tile,#4                 '16 bytes per tile
                        add     tile,tile_table         'convert to pointer
                        rdword  pixels1,tile            'Pixels 1 = %%xxxxxxxx01234567          16 bits

                        mov     pixels2,pixels1         'Pixels 2 = %%xxxxxxxx01234567          16 bits
                        shr     pixels1,#8              'Pixels 1 = %%xxxxxxxxxxxx0123           8 bits

                        movs    :offset0a,#tile_palette_table
                        movs    :offset1a,#tile_palette_table


                        mov     shifted4,pixels1
                        and     shifted4,#%%0033        'Shifted4 = %%xxxxxxxxxxxxxxx23
                        add     :offset0a,shifted4

                        mov     shifted3,pixels1
                        and     shifted3,#%%3300        'Shifted3 = %%xxxxxxxxxxxxx01xx
                        shr     shifted3,#4             'Shifted3 = %%xxxxxxxxxxxxxxx01
                        add     :offset1a,shifted3

:offset0a               mov     mix1,0-0
                        shl     mix1,#16
:offset1a               or      mix1,0-0

                        movs    :offset0b,#tile_palette_table
                        movs    :offset1b,#tile_palette_table

                        mov     shifted4,pixels2
                        and     shifted4,#%%0033        'Shifted4 = %%xxxxxxxxxxxxxxx67
                        add     :offset0b,shifted4

                        mov     shifted3,pixels2
                        and     shifted3,#%%3300        'Shifted3 = %%xxxxxxxxxxxxx45xx
                        shr     shifted3,#4             'Shifted3 = %%xxxxxxxxxxxxxxx45
                        add     :offset1b,shifted3

:offset0b               mov     mix2,0-0
                        shl     mix2,#16
:offset1b               or      mix2,0-0

' mov     mix1,rend_cog_number
' shl     mix1,#1
' add     mix1,black
' mov     mix2,mix1
                        

scan_line_store1        mov     0-0,mix1    'dest starts at cog_scanline_buffer
                        add     scan_line_store1,inc_dest_2 'self-modify dest+2

scan_line_store2        mov     0-0,mix2    'dest starts at cog_scanline_buffer
                        add     scan_line_store2,inc_dest_2 'self-modify dest+2
                        add     hub_map_ptr,#2          'point to next tile (one instruction between reads)
                        djnz    x,#do_tile   wc         'another tile?

                        ' We've finished rendering the tiles
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                        ' Render sprites
                        mov     this_sprite,#rend_sprites 'first sprites in array are drawn first
                        mov     outa,#0
sprite_loop
                        ' Verification: if the TV is already asking for this scanline or more
                        ' then panic, go to red alert with the debug LED and skip straight to TV output.
                        ' If the LED does light you need to consider optimising or allocating more
                        ' rendering cogs
{                       call    #checktv
        if_ae           mov     outa,#1
        if_ae           jmp     #start_tv_copy
}
                        'get the sprite attibutes in sprite_attr which we'll use for the rest of the routine
                        movs    :sprite_attr_load,this_sprite
:sprite_attr_load       mov     sprite_attr,0-0

                        ' don't draw it if it has the invisible flag
                        test    sprite_attr,attr_invisible wz
        if_nz           jmp     #next_sprite

                        ' get the tile number
                        mov     tile_number,sprite_attr
                        shr     tile_number,#16
                        and     tile_number,#$FF

                        ' get the hotspot
                        shl     tile_number,#1         ' two bytes per hotspot
                        mov     hub_source,rend_hotspot_table_ptr ' use hub_source for hotspot
                        add     hub_source,tile_number
                        rdword  r1,hub_source
                        mov     sprite_x,r1             ' we use it later.  Don't care that it has y in upper byte
                        shr     r1,#8

                        ' find the y-span and reject if current-line isn't in it.
                        mov     r0,sprite_attr                                       'r0=sprite-y
                        and     r0,#$FF
                        subs    r0,r1                   'add in the y hotspot

                        mov     draw_y,current_line
                        subs    draw_y,r0  wc,wz
        if_b            jmp     #next_sprite
                        cmp     draw_y,#16 wc,wz
        if_ae           jmp     #next_sprite

                        ' get the palette
                        shl     tile_number,#1          '4 bytes in a palette (we already shifted 1)
                        mov     hub_source,rend_color_table_ptr ' use hub_source for palette
                        add     hub_source,tile_number
                        rdlong  palette+1,hub_source    'palette+0 is always transparent (0)
                        mov     palette+2,palette+1
                        mov     palette+3,palette+1
                        shr     palette+1,#16
                        shr     palette+2,#8
                        and     palette+1,#$FF
                        and     palette+2,#$FF
                        and     palette+3,#$FF

                        ' palette for mirrored sprites too
                        mov     palette_mirrored+1,palette+1   'palette+0 is always transparent (0)
                        shl     palette_mirrored+1,#24
                        mov     palette_mirrored+2,palette+2
                        shl     palette_mirrored+2,#24
                        mov     palette_mirrored+3,palette+3
                        shl     palette_mirrored+3,#24

                        ' get the pixel data
                        shl     tile_number,#4          ' 64 bytes in a sprite
                        mov     hub_source,rend_sprite_table_ptr 'reuse hub_source for pixel data
                        add     hub_source,tile_number

                        ' adjust draw-y if sprite is flipped
                        test    sprite_attr,attr_flipped wz
              if_e      jmp     #:not_flipped
                        mov     r0,#15
                        sub     r0,draw_y
                        mov     draw_y,r0

                        ' add offset for y position in the tile
:not_flipped            shl     draw_y,#2
                        add     hub_source,draw_y

                         ' put pixel colours in own longs
                        rdlong  r1,hub_source           'pixels(r1) = %%0123456789abcdef

'***************************************************************************************************
                        'Code for converting 4 color sprites to hi-color
                        'This section is for NORMAL sprites, there was a similar section for mirrored sprites earlier
                        mov     count2,#4
                        test    sprite_attr,attr_mirrored wz
              if_nz     jmp     #mirrored
                        ' initialise self modifiers
normal                  movd    :shift_color,#pixel_color_list4
                        movd    :shift_mask,#pixel_mask_list4
                        movd    :mix_color,#pixel_color_list4
                        movd    :mix_mask,#pixel_mask_list4
                        ' loop init
:outer_loop             mov     count,#4                                        '4*4=16 pixels
:inner_loop             mov     r0,r1                                'get a single pixel
                        and     r0,#%%0000000000000003 wc,wz
                        add     r0,#palette                          'and get the adress of it's palette color
                        movs    :lookup_color,r0                     '...which is the source of the copy
              if_e      mov     r3,#$00
              if_ne     mov     r3,#$FF
:lookup_color           mov     r2,0-0                                       'destination is pixel_color_list
                        ' we now have color and mask
:shift_color            shl     0-0,#8          'pixel_color_list4->pixel_color_list1
:shift_mask             shl     0-0,#8          'pixel_mask_list4->pixel_mask_list1
:mix_color              or      0-0,r2       'pixel_color_list4->pixel_color_list1
:mix_mask               or      0-0,r3        'pixel_mask_list4->pixel_mask_list1
                        shr     r1,#2                                           'r1 = pixels
                        ' inner_loop
                        djnz    count,#:inner_loop
                        ' point to next color and mask words (work from end to beginning)
                        sub     :shift_color,inc_dest
                        sub     :shift_mask,inc_dest
                        sub     :mix_color,inc_dest
                        sub     :mix_mask,inc_dest
                        ' outer_loop
                        djnz    count2,#:outer_loop
                        jmp     #shift_prep
'***************************************************************************************************
                        'Code for converting 4 color sprites to hi-color
                        'This section is for MIRRORED sprites, there was a similar section for normal sprites earlier
                        ' initialise self modifiers
mirrored                movd    :shift_color,#pixel_color_list1
                        movd    :shift_mask,#pixel_mask_list1
                        movd    :mix_color,#pixel_color_list1
                        movd    :mix_mask,#pixel_mask_list1
                        ' loop init
:outer_loop             mov     count,#4                                        '4*4=16 pixels
:inner_loop             mov     r0,r1                                'get a single pixel
                        and     r0,#%%0000000000000003 wc,wz
                        add     r0,#palette_mirrored                 'and get the adress of it's palette color
                        movs    :lookup_color,r0                     '...which is the source of the copy
              if_e      mov     r3,#$00000000                                   'r3=mask
              if_ne     mov     r3,mirrored_mask
:lookup_color           mov     r2,0-0                                       'destination is pixel_color_list
                        ' we now have color and mask
:shift_color            shr     0-0,#8          'pixel_color_list4->pixel_color_list1
:shift_mask             shr     0-0,#8          'pixel_mask_list4->pixel_mask_list1
:mix_color              or      0-0,r2       'pixel_color_list4->pixel_color_list1  r2=color
:mix_mask               or      0-0,r3        'pixel_mask_list4->pixel_mask_list1
                        shr     r1,#2                                           'r1=pixels
                        ' inner_loop
                        djnz    count,#:inner_loop
                        ' point to next color and mask words (work from end to beginning)
                        add     :shift_color,inc_dest
                        add     :shift_mask,inc_dest
                        add     :mix_color,inc_dest
                        add     :mix_mask,inc_dest
                        ' outer_loop
                        djnz    count2,#:outer_loop
'***************************************************************************************************
                        ' work out where the sprite starts in the scanline.
shift_prep              ' sprite_x holds the hotspot (y hotspot also in upper byte, but get's chopped in AND later anyway
                        mov     r0,sprite_attr
                        shr     r0,#8                   ' r0 = xpos
                        sub     r0,sprite_x             ' r0 = xpos - hotspot_x
                        mov     sprite_x,r0
                        shr     r0,#2
                        and     r0,#%00111111
                        mov     cog_dest,#cog_scanline_buffer
                        add     cog_dest,r0
                        'calculate where the sprite stars within a frame
                        and     sprite_x,#%00000011 wc,wz

                        mov     shifted1,pixel_color_list1
                        mov     shifted2,pixel_color_list2
                        mov     shifted3,pixel_color_list3
                        mov     shifted4,pixel_color_list4
                        mov     shifted_mask1,pixel_mask_list1
                        mov     shifted_mask2,pixel_mask_list2
                        mov     shifted_mask3,pixel_mask_list3
                        mov     shifted_mask4,pixel_mask_list4

              if_ne     jmp     #:not0
                        ' not shifted
                        mov     shifted5,#0
                        mov     shifted_mask5,#0
                        jmp     #:end_case

:not0                   cmp     sprite_x,#1 wc,wz
              if_ne     jmp     #:not1

                        ' 1 shift
                        shl     shifted1,#8                                     ' shifts are the oposite way around
                        shl     shifted2,#8                                     ' because pchip is littleendian
                        shl     shifted3,#8
                        shl     shifted4,#8
                        shr     pixel_color_list1,#24
                        shr     pixel_color_list2,#24
                        shr     pixel_color_list3,#24
                        shr     pixel_color_list4,#24

                        shl     shifted_mask1,#8
                        shl     shifted_mask2,#8
                        shl     shifted_mask3,#8
                        shl     shifted_mask4,#8
                        shr     pixel_mask_list1,#24
                        shr     pixel_mask_list2,#24
                        shr     pixel_mask_list3,#24
                        shr     pixel_mask_list4,#24

                        jmp     #:common_to_123_shifts

                        ' 2 shifts
:not1                   cmp     sprite_x,#2  wc,wz
              if_ne     jmp     #:not2

                        shl     shifted1,#16
                        shl     shifted2,#16
                        shl     shifted3,#16
                        shl     shifted4,#16
                        shr     pixel_color_list1,#16
                        shr     pixel_color_list2,#16
                        shr     pixel_color_list3,#16
                        shr     pixel_color_list4,#16

                        shl     shifted_mask1,#16
                        shl     shifted_mask2,#16
                        shl     shifted_mask3,#16
                        shl     shifted_mask4,#16
                        shr     pixel_mask_list1,#16
                        shr     pixel_mask_list2,#16
                        shr     pixel_mask_list3,#16
                        shr     pixel_mask_list4,#16
                        jmp     #:common_to_123_shifts

:not2                   'must be 3 shifts
                        shl     shifted1,#24
                        shl     shifted2,#24
                        shl     shifted3,#24
                        shl     shifted4,#24
                        shr     pixel_color_list1,#8
                        shr     pixel_color_list2,#8
                        shr     pixel_color_list3,#8
                        shr     pixel_color_list4,#8

                        shl     shifted_mask1,#24
                        shl     shifted_mask2,#24
                        shl     shifted_mask3,#24
                        shl     shifted_mask4,#24
                        shr     pixel_mask_list1,#8
                        shr     pixel_mask_list2,#8
                        shr     pixel_mask_list3,#8
                        shr     pixel_mask_list4,#8

:common_to_123_shifts
                        or      shifted2,pixel_color_list1
                        or      shifted3,pixel_color_list2
                        or      shifted4,pixel_color_list3
                        mov     shifted5,pixel_color_list4
                        or      shifted_mask2,pixel_mask_list1                     'masks are 0 for background, 1 for sprite
                        or      shifted_mask3,pixel_mask_list2
                        or      shifted_mask4,pixel_mask_list3
                        mov     shifted_mask5,pixel_mask_list4

:end_case               movd    :write_it,cog_dest
                        movs    :write_it,#shifted1
                        movd    :mask_it,cog_dest
                        movs    :mask_it,#shifted_mask1
                        mov     count,#5
:mask_it                andn    0-0,0-0
:write_it               or      0-0,0-0
                        add     :write_it,inc_dest
                        add     :write_it,#1
                        add     :mask_it,inc_dest
                        add     :mask_it,#1
                        djnz    count,#:mask_it

next_sprite             add     this_sprite,#1
                        cmp     this_sprite,#rend_sprites_end wz
              if_ne     jmp     #sprite_loop

'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

                        ' We've finished rendering the line and we didn't have a hissy fit panic.
                        ' We need to wait here for the line we just prepared to be requested by the TV driver
wait_for_the_request
                        call    #checktv
              if_b      jmp     #wait_for_the_request

                        ' Our line has been requested so copy it to hub scan line buffer
start_tv_copy           movs    :nextcopy, #cog_scanline_buffer
                        mov     r0, rend_scanline_buffer
                        mov     count, #64              ' length of visible scanline buffer in longs
:nextcopy               mov     r1,cog_scanline_buffer
                        add     :nextcopy, #1
                        wrlong  r1, r0
                        add     r0, #4
                        djnz    count, #:nextcopy

                        ' We've handed over the scanline to the TV driver, so do another.
allocate_next_job       add     current_line,rend_cog_count 'we only do every Nth line
                        ' Degrade gracefully when there's too much work to do.
'                        call    #checktv
'              if_ae     jmp     #allocate_next_job
                       ' Wrap around.
                        cmp     current_line,#224 wz,wc  'cmpsub will subtract 224 if current_line is greater than 224.
              if_b      jmp     #do_line
                        sub     current_line,#224
                        jmp     #do_frame


'-------------------------------------------------------------------
checktv                 ' Tf we're currently rendering a low numbered line when a high one is being requested
                        ' then that's OK because we wrapped around.



                        rdlong  scanline_req,rend_scanline_req_adr
                        cmp     scanline_req,#210 wc,wz
              if_be     jmp     #:checkit
                        cmp     current_line,#7 wc,wz
              if_be     cmp     scanline_req, current_line wz
                        'b = normal
                        'z = doit
                        'a = err
              if_be     jmp     #checktv_ret

:checkit                cmp     scanline_req, current_line wz, wc    'set carry if request is less than current.
                        'b = normal
                        'z = doit
                        'a = err

                        ' At this point, we could skip drawing sprites or something like that and go
                        ' straight to image output and still stay synced with the tv driver
                        ' We must be quick though!
                        'jmp #start_tv_copy
checktv_ret             ret
'-------------------------------------------------------------------

' Initialized data
inc_dest                long    1 << 9 << 0
inc_dest_2              long    1 << 9 << 1
attr_mirrored           long    $01000000
attr_flipped            long    $02000000
attr_invisible          long    $04000000
constantFF00            long    $FF00
'black                   long    $02020202

DAT 'Uninitiaised data - always keep it at the end of file to avoid nasty gotcha with RES.

'tiles
tile_palette            long    0

tile_palette_table      long    $00005252   '
                        long    $0000dd52
                        long    $00000552
                        long    $00003b52

                        long    $000052dd
                        long    $0000dddd
                        long    $000005dd
                        long    $00003bdd

                        long    $00005205
                        long    $0000dd05
                        long    $00000505
                        long    $00003b05

                        long    $0000523b
                        long    $0000dd3b
                        long    $0000053b
                        long    $00003b3b

'sprites
palette                 long    $00000000
                        long    $0000000b
                        long    $0000005b
                        long    $00000006

palette_mirrored        long    $00000000
                        long    $0b000000
                        long    $5b000000
                        long    $06000000

mirrored_mask           long    $FF000000

' Parameter buffer
renderer_params
rend_cog_number         res     1
rend_scanline_req_adr   res     1
rend_map                res     1
rend_tile_table_ptr     res     1
rend_tile_palette_ptr   res     1
rend_sprite_table_ptr   res     1
rend_hotspot_table_ptr  res     1
rend_color_table_ptr    res     1               'rend_colors
rend_ht                 res     1               '
rend_vt                 res     1
rend_scanline_buffer    res     1
rend_cog_count          res     1
rend_debugging          res     1
rend_sprites            res     35
rend_sprites_end
' End of parameter buffer

' Cog scanline buffer = 256 pixels, 8 bits per pixel = 2048 bits = 256 bytes = 64 longs
cog_scanline_buffer     res     64
' End of scanline buffer

count                   res     1
count2                  res     1
hub_map_ptr             res     1
current_line            res     1
tile_table              res     1

draw_y                  res     1

sprite_x                res     1
sprite_attr             res     1
this_sprite             res     1
scanline_req            res     1
x                       res     1
tile                    res     1
pixels1                 res     1
pixels2                 res     1
mix1            
shifted1                res     1
mix2          
shifted2                res     1
shifted3                res     1
shifted4                res     1
shifted5                res     1
r0                      res     1
r1                      res     1
r2                      res     1
r3                      res     1
tile_number             res     1
hub_source              res     1
cog_dest                res     1

pixel_color_list1       res     1
pixel_color_list2       res     1
pixel_color_list3       res     1
pixel_color_list4       res     1

pixel_mask_list1        res     1
pixel_mask_list2        res     1
pixel_mask_list3        res     1
pixel_mask_list4        res     1

shifted_mask1           res     1
shifted_mask2           res     1
shifted_mask3           res     1
shifted_mask4           res     1
shifted_mask5           res     1

last_tile_palette       res     1
last_tile               res     1

                        fit 