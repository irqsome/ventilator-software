'//////////////////////////////////////////////////////////////////////////////
' Donkey Kong Remake
' AUTHOR: Steve Waddicor
'//////////////////////////////////////////////////////////////////////////////

PUB map_adr
  return @map_def

PUB tile_adr
  return @tile_def

PUB sprite_adr
  return @sprite_def

PUB sprite_hotspot_adr
  return @sprite_hotspot_def

PUB sprite_palette_adr
  return @sprite_palette_def

PUB tile_palette_adr
  return @tile_palette_def

DAT

map_def

' tiles_blank
    byte    72, 1, 73, 1, 32, 1, 83, 1, 67, 1, 79, 1, 82, 1, 69, 1, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0,167, 3,167, 3,167, 3, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 49, 1, 85, 1, 80, 1
    byte    48, 4, 48, 4, 48, 4, 48, 4, 48, 4, 48, 4, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 48, 4, 48, 4, 48, 4, 48, 4, 48, 4, 48, 4
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 76, 2, 61, 2, 48, 2, 49, 2, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0
    byte    32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0, 32, 0

CON
    TILE_TRELLIS                    = 0
    TILE_MARIO_LIFE                 = 2
    TILE_BONUS                      = 18
    TILE_SMALL_M                    = 1
    TILE_SPACE                      = 17
    TILE_POLE                       = TILE_SPACE+3
    TILE_HELP                       = TILE_POLE+2
    TILE_INVISIBLE_LADDER           = TILE_HELP+3
    TILE_LEVEL_4_PLATFORM           = 28
    TILE_LEVEL_3_PLATFORM           = 29
    TILE_KONG_START                 = 76
    TILE_BOTTOM_OF_RIVET            = 100
    TILE_FULL_LADDER                = 101
    TILE_END_LOWER_PLAT_LAD_TILES   = TILE_FULL_LADDER+7
    TILE_FULL_PLATFORM_LADDER_TILE  = TILE_FULL_LADDER+8
    TILE_START_OF_LADDER_TILES      = TILE_FULL_LADDER+9
    TILE_END_OF_LADDER_TILES        = TILE_FULL_LADDER+15
    TILE_TOP_OF_RIVET               = TILE_FULL_LADDER+16
    TILE_START_OF_PLATFORM_TILES    = TILE_FULL_LADDER+17
    TILE_END_OF_LOWER_PLAT_TILES    = TILE_FULL_LADDER+23
    TILE_FULL_PLATFORM              = TILE_FULL_LADDER+24
    TILE_START_OF_PLAT_TOP_TILES    = TILE_FULL_LADDER+25
    TILE_END_OF_PLATFORM_TILES      = TILE_FULL_LADDER+31

    SPRITE_SCORE_100    = 87
    SPRITE_SCORE_200    = 54
    SPRITE_SCORE_300    = 68
    SPRITE_SCORE_500    = 69
    SPRITE_SCORE_800    = 70

DAT
tile_def
' 4 colour 8x8 tiles = 16 bytes each.

' TileNumber: 5
    word    %%23000032
    word    %%33300333
    word    %%03333330
    word    %%00322300
    word    %%00322300
    word    %%03333330
    word    %%33300333
    word    %%23000032
' TileNumber: 6
    word    %%00000000
    word    %%00000000
    word    %%11110110
    word    %%11011011
    word    %%11011011
    word    %%11011011
    word    %%11011011
    word    %%00000000
' TileNumber: 7
    word    %%00222200
    word    %%02131110
    word    %%03331311
    word    %%01133300
    word    %%00211210
    word    %%03222211
    word    %%00222233
    word    %%01100110
' TileNumber: 8 - Corner at bottom of bonus box.  Altered to make bars wider.
    word    %%00111103
    word    %%01100003
    word    %%11000003
    word    %%11000003
    word    %%11000003
    word    %%11000000
    word    %%11003333
    word    %%11033300
' TileNumber: 9 - BO
    word    %%33330003
    word    %%33003033
    word    %%33330033
    word    %%33003033
    word    %%33330003
    word    %%00000000
    word    %%33333333
    word    %%00000000
' TileNumber: 10 - ON
    word    %%33300333
    word    %%30330333
    word    %%30330333
    word    %%30330333
    word    %%33300333
    word    %%00000000
    word    %%33333333
    word    %%00000000
' TileNumber: 11 - NU
    word    %%00330330
    word    %%30330330
    word    %%33330330
    word    %%03330330
    word    %%00330033
    word    %%00000000
    word    %%33333333
    word    %%00000000
' TileNumber: 12 - US
    word    %%03300333
    word    %%03303300
    word    %%03303333
    word    %%03300003
    word    %%33003333
    word    %%00000000
    word    %%33333333
    word    %%00000000
' TileNumber: 13 - Corner at top of bonus box.  Altered to make bars wider.
    word    %%30111100
    word    %%00000110
    word    %%30000011
    word    %%30000011
    word    %%00000011
    word    %%00000011
    word    %%33330011
    word    %%00333011
' TileNumber: 14 - Vertical bar at side of bonus box.  Altered to make bars wider.
    word    %%11033300
    word    %%11033300
    word    %%11033300
    word    %%11033300
    word    %%11033300
    word    %%11033300
    word    %%11033300
    word    %%11033300
' TileNumber: 15 - Vertical bar at side of bonus box.  Altered to make bars wider.
    word    %%00333011
    word    %%00333011
    word    %%00333011
    word    %%00333011
    word    %%00333011
    word    %%00333011
    word    %%00333011
    word    %%00333011

' TileNumber: 16 - Corner at bottom of bonus box.  Altered to make bars wider.
    word    %%11003333
    word    %%01100000
    word    %%00111111
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
' TileNumber: 17
    word    %%33333333
    word    %%00000000
    word    %%11111111
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
' TileNumber: 18 - Corner at bottom of bonus box.  Altered to make bars wider.
    word    %%33330011
    word    %%00000110
    word    %%11111100
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
' TileNumber: 19
    word    %%00000000
    word    %%00000000
    word    %%00000111
    word    %%00000101
    word    %%00000111
    word    %%00000100
    word    %%00000100
    word    %%00000000
' TileNumber: 20
    word    %%00000000
    word    %%00000000
    word    %%00100101
    word    %%00100101
    word    %%10100101
    word    %%10100101
    word    %%10011001
    word    %%00000000

' TileNumber: 21
    word    %%00000000
    word    %%00000000
    word    %%11000111
    word    %%01000100
    word    %%11100110
    word    %%00100100
    word    %%11100111
    word    %%00000000
' TileNumber: 24
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
' TileNumber: 22
    word    %%00000000
    word    %%00000000
    word    %%01001011
    word    %%01101010
    word    %%01011010
    word    %%01001010
    word    %%01001011
    word    %%00000000
' TileNumber: 23
    word    %%00000000
    word    %%00000000
    word    %%10000000
    word    %%01000000
    word    %%01000000
    word    %%01000000
    word    %%10000000
    word    %%00000000


' TileNumber: 25
    word    %%00000002
    word    %%00000002
    word    %%00000002
    word    %%00000002
    word    %%00000002
    word    %%00000002
    word    %%00000002
    word    %%00000002
' TileNumber: 26
    word    %%20000000
    word    %%20000000
    word    %%20000000
    word    %%20000000
    word    %%20000000
    word    %%20000000
    word    %%20000000
    word    %%20000000
' TileNumber: 27
    word    %%11001011
    word    %%11111011
    word    %%11001011
    word    %%00001011
    word    %%00000001
    word    %%00000000
    word    %%00000000
    word    %%00000000
' TileNumber: 28
    word    %%11011000
    word    %%00011000
    word    %%11011000
    word    %%00011000
    word    %%11011000
    word    %%00001110
    word    %%00000000
    word    %%00000000
' TileNumber: 29
    word    %%11100011
    word    %%11010111
    word    %%11010111
    word    %%11010110
    word    %%11100110
    word    %%11000100
    word    %%11000000
    word    %%00001100
' TileNumber: 30 Invisible Ladder
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
' TileNumber: 31
    word    %%00011000
    word    %%00011000
    word    %%00011000
    word    %%00011000
    word    %%00011000
    word    %%00000000
    word    %%00011000
    word    %%00000000
' TileNumber: 32
    word    %%00010100
    word    %%00010100
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
' TileNumber: 33
    word    %%33333333
    word    %%22222222
    word    %%33000033
    word    %%30000003
    word    %%30000003
    word    %%33000033
    word    %%33333333
    word    %%22222222
' TileNumber: 34
    word    %%33333333
    word    %%22222222
    word    %%20000002
    word    %%20000002
    word    %%20000002
    word    %%20000002
    word    %%22222222
    word    %%33333333
' TileNumber: 35
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%01111110
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
{
' TileNumber: 36
    word    %%00001100 'cut
    word    %%00011000
    word    %%00110000
    word    %%00100000
    word    %%00110000
    word    %%00011000
    word    %%00001100
    word    %%00000000
' TileNumber: 37
    word    %%00110000 'cut
    word    %%00011000
    word    %%00001100
    word    %%00000100
    word    %%00001100
    word    %%00011000
    word    %%00110000
    word    %%00000000
' TileNumber: 38
    word    %%00000000 'cut
    word    %%00000000
    word    %%00000000
    word    %%01111110
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
}
' TileNumber: 53  - =
    word    %%00000000 'cut
    word    %%00000000
    word    %%01111110
    word    %%00000000
    word    %%01111110
    word    %%00000000
    word    %%00000000
    word    %%00000000
' TileNumber: 39
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00001100
    word    %%00001100
    word    %%00000000
' TileNumber: 40   - 0
    word    %%00011100
    word    %%00100110
    word    %%01100011
    word    %%01100011
    word    %%01100011
    word    %%00110010
    word    %%00011100
    word    %%00000000
' TileNumber: 41
    word    %%00001100
    word    %%00011100
    word    %%00001100
    word    %%00001100
    word    %%00001100
    word    %%00001100
    word    %%00111111
    word    %%00000000
' TileNumber: 42
    word    %%00111110
    word    %%01100011
    word    %%00000111
    word    %%00011110
    word    %%00111100
    word    %%01110000
    word    %%01111111
    word    %%00000000
' TileNumber: 43
    word    %%00111111
    word    %%00000110
    word    %%00001100
    word    %%00011110
    word    %%00000011
    word    %%01100011
    word    %%00111110
    word    %%00000000
' TileNumber: 44
    word    %%00001110
    word    %%00011110
    word    %%00110110
    word    %%01100110
    word    %%01111111
    word    %%00000110
    word    %%00000110
    word    %%00000000
' TileNumber: 45
    word    %%01111110
    word    %%01100000
    word    %%01111110
    word    %%00000011
    word    %%00000011
    word    %%01100011
    word    %%00111110
    word    %%00000000
' TileNumber: 46
    word    %%00011110
    word    %%00110000
    word    %%01100000
    word    %%01111110
    word    %%01100011
    word    %%01100011
    word    %%00111110
    word    %%00000000
' TileNumber: 47
    word    %%01111111
    word    %%01100011
    word    %%00000110
    word    %%00001100
    word    %%00011000
    word    %%00011000
    word    %%00011000
    word    %%00000000
' TileNumber: 48
    word    %%00111100
    word    %%01100010
    word    %%01110010
    word    %%00111100
    word    %%01001111
    word    %%01000011
    word    %%00111110
    word    %%00000000
' TileNumber: 49  - 9
    word    %%00111110
    word    %%01100011
    word    %%01100011
    word    %%00111111
    word    %%00000011
    word    %%00000110
    word    %%00111100
    word    %%00000000
' TileNumber: 50
    word    %%00000000
    word    %%00011000
    word    %%00011000
    word    %%00000000
    word    %%00011000
    word    %%00011000
    word    %%00000000
    word    %%00000000
' TileNumber: 0
    word    %%00000000
    word    %%00111100
    word    %%01100110
    word    %%01101110
    word    %%01110110
    word    %%01100110
    word    %%00111100
    word    %%00000000
' TileNumber: 1
    word    %%00000000
    word    %%00011000
    word    %%00111000
    word    %%00011000
    word    %%00011000
    word    %%00011000
    word    %%01111110
    word    %%00000000
' TileNumber: 2
    word    %%00000000
    word    %%00111100
    word    %%01100110
    word    %%00001100
    word    %%00011000
    word    %%00110000
    word    %%01111110
    word    %%00000000
' TileNumber: 3
    word    %%00000000
    word    %%01111110
    word    %%01100000
    word    %%01111100
    word    %%00000110
    word    %%01100110
    word    %%00111100
    word    %%00000000
' TileNumber: 4
    word    %%00000000
    word    %%01111110
    word    %%00000110
    word    %%00001100
    word    %%00011000
    word    %%00110000
    word    %%00110000
    word    %%00000000
{' TileNumber: -
    word    %%00222222 'cut
    word    %%02000000
    word    %%20022220
    word    %%20002200
    word    %%20002200
    word    %%20002200
    word    %%02000000
    word    %%00222222
' TileNumber: -
    word    %%22222200 'cut
    word    %%00000020
    word    %%22022002
    word    %%22222002
    word    %%20202002
    word    %%20202002
    word    %%00000020
    word    %%22222200
}

{
' TileNumber: -
    word    %%00110110 'cut
    word    %%00110110
    word    %%00010010
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
}
' TileNumber: 55
    word    %%00111110
    word    %%01100011
    word    %%00000011
    word    %%00001110
    word    %%00011000
    word    %%00000000
    word    %%00011000
    word    %%00000000
{
' TileNumber: -
    word    %%00100100 'cut
    word    %%00110110
    word    %%00110110
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
}
' TileNumber: 57  - A
    word    %%00011100
    word    %%00110110
    word    %%01100011
    word    %%01100011
    word    %%01111111
    word    %%01100011
    word    %%01100011
    word    %%00000000
' TileNumber:
    word    %%01111110
    word    %%01100011
    word    %%01100011
    word    %%01111110
    word    %%01100011
    word    %%01100011
    word    %%01111110
    word    %%00000000
' TileNumber:
    word    %%00011110
    word    %%00110011
    word    %%01100000
    word    %%01100000
    word    %%01100000
    word    %%00110011
    word    %%00011110
    word    %%00000000
' TileNumber:
    word    %%01111100
    word    %%01100110
    word    %%01100011
    word    %%01100011
    word    %%01100011
    word    %%01100110
    word    %%01111100
    word    %%00000000
' TileNumber:
    word    %%00111111
    word    %%00110000
    word    %%00110000
    word    %%00111110
    word    %%00110000
    word    %%00110000
    word    %%00111111
    word    %%00000000
' TileNumber:
    word    %%01111111
    word    %%01100000
    word    %%01100000
    word    %%01111110
    word    %%01100000
    word    %%01100000
    word    %%01100000
    word    %%00000000
' TileNumber:
    word    %%00011111
    word    %%00110000
    word    %%01100000
    word    %%01100111
    word    %%01100011
    word    %%00110011
    word    %%00011111
    word    %%00000000
' TileNumber:
    word    %%01100011
    word    %%01100011
    word    %%01100011
    word    %%01111111
    word    %%01100011
    word    %%01100011
    word    %%01100011
    word    %%00000000
' TileNumber:
    word    %%00111111
    word    %%00001100
    word    %%00001100
    word    %%00001100
    word    %%00001100
    word    %%00001100
    word    %%00111111
    word    %%00000000
' TileNumber:
    word    %%00000011
    word    %%00000011
    word    %%00000011
    word    %%00000011
    word    %%00000011
    word    %%01100011
    word    %%00111110
    word    %%00000000
' TileNumber:
    word    %%01100011
    word    %%01100110
    word    %%01101100
    word    %%01111000
    word    %%01111100
    word    %%01101110
    word    %%01100111
    word    %%00000000
' TileNumber:
    word    %%00110000
    word    %%00110000
    word    %%00110000
    word    %%00110000
    word    %%00110000
    word    %%00110000
    word    %%00111111
    word    %%00000000
' TileNumber:
    word    %%01100011
    word    %%01110111
    word    %%01111111
    word    %%01111111
    word    %%01101011
    word    %%01100011
    word    %%01100011
    word    %%00000000
' TileNumber:
    word    %%01100011
    word    %%01110011
    word    %%01111011
    word    %%01111111
    word    %%01101111
    word    %%01100111
    word    %%01100011
    word    %%00000000
' TileNumber:
    word    %%00111110
    word    %%01100011
    word    %%01100011
    word    %%01100011
    word    %%01100011
    word    %%01100011
    word    %%00111110
    word    %%00000000
' TileNumber:
    word    %%01111110
    word    %%01100011
    word    %%01100011
    word    %%01100011
    word    %%01111110
    word    %%01100000
    word    %%01100000
    word    %%00000000
' TileNumber:
    word    %%00111110
    word    %%01100011
    word    %%01100011
    word    %%01100011
    word    %%01101111
    word    %%01100110
    word    %%00111101
    word    %%00000000
' TileNumber:
    word    %%01111110
    word    %%01100011
    word    %%01100011
    word    %%01100111
    word    %%01111100
    word    %%01101110
    word    %%01100111
    word    %%00000000
' TileNumber:
    word    %%00111100
    word    %%01100110
    word    %%01100000
    word    %%00111110
    word    %%00000011
    word    %%01100011
    word    %%00111110
    word    %%00000000
' TileNumber:
    word    %%00111111
    word    %%00001100
    word    %%00001100
    word    %%00001100
    word    %%00001100
    word    %%00001100
    word    %%00001100
    word    %%00000000
' TileNumber:
    word    %%01100011
    word    %%01100011
    word    %%01100011
    word    %%01100011
    word    %%01100011
    word    %%01100011
    word    %%00111110
    word    %%00000000
' TileNumber:
    word    %%01100011
    word    %%01100011
    word    %%01100011
    word    %%01110111
    word    %%00111110
    word    %%00011100
    word    %%00001000
    word    %%00000000
' TileNumber:
    word    %%01100011
    word    %%01100011
    word    %%01101011
    word    %%01111111
    word    %%01111111
    word    %%00110110
    word    %%00100010
    word    %%00000000
' TileNumber:
    word    %%01100011
    word    %%01110111
    word    %%00111110
    word    %%00011100
    word    %%00111110
    word    %%01110111
    word    %%01100011
    word    %%00000000
' TileNumber:
    word    %%00110011
    word    %%00110011
    word    %%00010010
    word    %%00011110
    word    %%00001100
    word    %%00001100
    word    %%00001100
    word    %%00000000
' TileNumber:        Z -
    word    %%01111111
    word    %%00000111
    word    %%00001110
    word    %%00011100
    word    %%00111000
    word    %%01110000
    word    %%01111111
    word    %%00000000
' TileNumber: 83          Top line of Kong
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000003
    word    %%00000033
    word    %%00000033
    word    %%00000333
' TileNumber:
    word    %%00033330
    word    %%00333333
    word    %%03333333
    word    %%33333330
    word    %%33322233
    word    %%33222323
    word    %%33222323
    word    %%33223223
' TileNumber: -
    word    %%00033330
    word    %%00333333
    word    %%03322223
    word    %%33222222
    word    %%32222222
    word    %%32221112
    word    %%33221102
    word    %%33323333
' TileNumber: -
    word    %%03333000
    word    %%33333300
    word    %%32222330
    word    %%22222233
    word    %%22222223
    word    %%11122223
    word    %%01122233
    word    %%33322333
' TileNumber: -
    word    %%03333000
    word    %%33333300
    word    %%33333330
    word    %%03333333
    word    %%33222333
    word    %%32322233
    word    %%32322233
    word    %%32232233
' TileNumber: -
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%30000000
    word    %%33000000
    word    %%33300000
    word    %%33300000






' TileNumber: 89              2nd line of Kong
    word    %%00000333
    word    %%00003333
    word    %%00003333
    word    %%00033333
    word    %%00033333
    word    %%00333333
    word    %%00333333
    word    %%03333333
' TileNumber: -
    word    %%33332323
    word    %%33333222
    word    %%33332222
    word    %%33322222
    word    %%33322222
    word    %%33322222
    word    %%33302222
    word    %%33003321
' TileNumber: -
    word    %%33232200
    word    %%32222222
    word    %%22222222
    word    %%22222222
    word    %%22222222
    word    %%22222211
    word    %%22201110
    word    %%11000111
' TileNumber: -
    word    %%02232233
    word    %%22222223
    word    %%22111122
    word    %%21111112
    word    %%11111011
    word    %%01110001
    word    %%00111011
    word    %%01111122
' TileNumber: -
    word    %%32323333
    word    %%22233333
    word    %%22233333
    word    %%22233333
    word    %%22233333
    word    %%22333333
    word    %%23330333
    word    %%23333033
' TileNumber: -
    word    %%33330000
    word    %%33330000
    word    %%33333000
    word    %%33333000
    word    %%33333300
    word    %%33333300
    word    %%33333300
    word    %%33333330



' TileNumber: 95            3rd line of Kong
    word    %%03333333
    word    %%03333333
    word    %%03333333
    word    %%03333333
    word    %%00333333
    word    %%00333333
    word    %%00033333
    word    %%00003333
' TileNumber: -
    word    %%30030332
    word    %%30333233
    word    %%30323232
    word    %%30322323
    word    %%33032222
    word    %%33333222
    word    %%33323332
    word    %%33222233
' TileNumber: -
    word    %%22221111
    word    %%33322222
    word    %%32233333
    word    %%22322322
    word    %%22222233
    word    %%22332233
    word    %%22222333
    word    %%33333222
' TileNumber: -
    word    %%11122223
    word    %%22222333
    word    %%33333232
    word    %%23232322
    word    %%32222222
    word    %%32233222
    word    %%33222222
    word    %%22333333
' TileNumber: -
    word    %%33300033
    word    %%23233303
    word    %%32323303
    word    %%23223303
    word    %%22233033
    word    %%23333333
    word    %%33332333
    word    %%33222233
' TileNumber: -
    word    %%33333330
    word    %%33333330
    word    %%33333330
    word    %%33333330
    word    %%33333300
    word    %%33333300
    word    %%33333000
    word    %%33330000



' TileNumber: 101           Last line of Kong
    word    %%00000333
    word    %%00000032
    word    %%00000003
    word    %%00000000
    word    %%00000000
    word    %%00000022
    word    %%00000223
    word    %%00000222
' TileNumber: -
    word    %%23222223
    word    %%32223333
    word    %%23223333
    word    %%32222333
    word    %%02222223
    word    %%33222233
    word    %%23333333
    word    %%22222333
' TileNumber: -
    word    %%32323232
    word    %%23232323
    word    %%32323232
    word    %%33222322
    word    %%33333232
    word    %%33330000
    word    %%33333000
    word    %%33330000
' TileNumber: -
    word    %%32322323
    word    %%23223232
    word    %%32232323
    word    %%22322233
    word    %%32233333
    word    %%00003333
    word    %%00033333
    word    %%00003333
' TileNumber: -
    word    %%32222232
    word    %%33332223
    word    %%33332232
    word    %%33322223
    word    %%32222220
    word    %%33222233
    word    %%33333332
    word    %%33322222
' TileNumber: -
    word    %%33300000
    word    %%23000000
    word    %%30000000
    word    %%00000000
    word    %%00000000
    word    %%22000000
    word    %%32200000
    word    %%22200000


' TileNumber: 107
    word    %%31122113
    word    %%21122112
    word    %%01111110
    word    %%01111110
    word    %%31111110
    word    %%31111113
    word    %%31111113
    word    %%21111112
' TileNumber: 108 - Ladder and platform.  Altered to make ladder poles and platform cross members wider
    word    %%11000011
    word    %%11111111
    word    %%11000011
    word    %%11000011
    word    %%11000011
    word    %%11111111
    word    %%11000011
    word    %%11000011
' TileNumber:  - Ladder and platform.  Altered to make ladder poles and platform cross members wider
    word    %%22222222
    word    %%11111111
    word    %%11000011
    word    %%11000011
    word    %%11000011
    word    %%11111111
    word    %%11000011
    word    %%11000011
' TileNumber:  - Ladder and platform.  Altered to make ladder poles and platform cross members wider
    word    %%33333333
    word    %%22222222
    word    %%11000011
    word    %%11000011
    word    %%11000011
    word    %%11111111
    word    %%11000011
    word    %%11000011
' TileNumber:  - Ladder and platform.  Altered to make ladder poles and platform cross members wider
    word    %%33000033
    word    %%33333333
    word    %%22222222
    word    %%11000011
    word    %%11000011
    word    %%11111111
    word    %%11000011
    word    %%11000011
' TileNumber:  - Ladder and platform.  Altered to make ladder poles and platform cross members wider
    word    %%33300333
    word    %%33000033
    word    %%33333333
    word    %%22222222
    word    %%11000011
    word    %%11111111
    word    %%11000011
    word    %%11000011
' TileNumber:  - Ladder and platform.  Altered to make ladder poles and platform cross members wider
    word    %%03333330
    word    %%33300333
    word    %%33000033
    word    %%33333333
    word    %%22222222
    word    %%11111111
    word    %%11000011
    word    %%11000011
' TileNumber:  - Ladder and platform.  Altered to make ladder poles and platform cross members wider
    word    %%00333300
    word    %%03333330
    word    %%33300333
    word    %%33000033
    word    %%33333333
    word    %%22222222
    word    %%11000011
    word    %%11000011
' TileNumber:  - Ladder and platform.  Altered to make ladder poles and platform cross members wider
    word    %%22222222
    word    %%00333300
    word    %%03333330
    word    %%33300333
    word    %%33000033
    word    %%33333333
    word    %%22222222
    word    %%11000011
' TileNumber: 116 - Ladder and platform.  Altered to make ladder poles and platform cross members wider
    word    %%33333333
    word    %%22222222
    word    %%00333300
    word    %%03333330
    word    %%33300333
    word    %%33000033
    word    %%33333333
    word    %%22222222
' TileNumber:  - Ladder and platform.  Altered to make ladder poles and platform cross members wider
    word    %%11000011
    word    %%33333333
    word    %%22222222
    word    %%00333300
    word    %%03333330
    word    %%33300333
    word    %%33000033
    word    %%33333333
' TileNumber:  - Ladder and platform.  Altered to make ladder poles and platform cross members wider
    word    %%11000011
    word    %%11111111
    word    %%33333333
    word    %%22222222
    word    %%00333300
    word    %%03333330
    word    %%33300333
    word    %%33000033
' TileNumber:  - Ladder and platform.  Altered to make ladder poles and platform cross members wider
    word    %%11000011
    word    %%11111111
    word    %%11000011
    word    %%33333333
    word    %%22222222
    word    %%00333300
    word    %%03333330
    word    %%33300333
' TileNumber:  - Ladder and platform.  Altered to make ladder poles and platform cross members wider
    word    %%11000011
    word    %%11111111
    word    %%11000011
    word    %%11000011
    word    %%33333333
    word    %%22222222
    word    %%00333300
    word    %%03333330
' TileNumber:  - Ladder and platform.  Altered to make ladder poles and platform cross members wider
    word    %%11000011
    word    %%11111111
    word    %%11000011
    word    %%11000011
    word    %%11000011
    word    %%33333333
    word    %%22222222
    word    %%00333300
' TileNumber:  - Ladder and platform.  Altered to make ladder poles and platform cross members wider
    word    %%11000011
    word    %%11111111
    word    %%11000011
    word    %%11000011
    word    %%11000011
    word    %%11111111
    word    %%33333333
    word    %%22222222
' TileNumber: 123 - Ladder and platform.  Altered to make ladder poles and platform cross members wider
    word    %%11000011
    word    %%11111111
    word    %%11000011
    word    %%11000011
    word    %%11000011
    word    %%11111111
    word    %%11000011
    word    %%33333333
' TileNumber: 124
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%22222222
' TileNumber: 125 - Platform and space.  Altered to make platform cross members wider
    word    %%22222222
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
' TileNumber:  - Platform and space.  Altered to make platform cross members wider
    word    %%33333333
    word    %%22222222
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
' TileNumber:  - Platform and space.  Altered to make platform cross members wider
    word    %%33000033
    word    %%33333333
    word    %%22222222
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
' TileNumber:  - Platform and space.  Altered to make platform cross members wider
    word    %%33300333
    word    %%33000033
    word    %%33333333
    word    %%22222222
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
' TileNumber:  - Platform and space.  Altered to make platform cross members wider
    word    %%03333330
    word    %%33300333
    word    %%33000033
    word    %%33333333
    word    %%22222222
    word    %%00000000
    word    %%00000000
    word    %%00000000
' TileNumber:  - Platform and space.  Altered to make platform cross members wider
    word    %%00333300
    word    %%03333330
    word    %%33300333
    word    %%33000033
    word    %%33333333
    word    %%22222222
    word    %%00000000
    word    %%00000000
' TileNumber:  - Platform and space.  Altered to make platform cross members wider
    word    %%22222222
    word    %%00333300
    word    %%03333330
    word    %%33300333
    word    %%33000033
    word    %%33333333
    word    %%22222222
    word    %%00000000
' TileNumber: 132 - Platform and space.  Altered to make platform cross members wider
    word    %%33333333
    word    %%22222222
    word    %%00333300
    word    %%03333330
    word    %%33300333
    word    %%33000033
    word    %%33333333
    word    %%22222222
' TileNumber:  - Platform and space.  Altered to make platform cross members wider
    word    %%00000000
    word    %%33333333
    word    %%22222222
    word    %%00333300
    word    %%03333330
    word    %%33300333
    word    %%33000033
    word    %%33333333
' TileNumber:  - Platform and space.  Altered to make platform cross members wider
    word    %%00000000
    word    %%00000000
    word    %%33333333
    word    %%22222222
    word    %%00333300
    word    %%03333330
    word    %%33300333
    word    %%33000033
' TileNumber:  - Platform and space.  Altered to make platform cross members wider
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%33333333
    word    %%22222222
    word    %%00333300
    word    %%03333330
    word    %%33300333
' TileNumber:  - Platform and space.  Altered to make platform cross members wider
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%33333333
    word    %%22222222
    word    %%00333300
    word    %%03333330
' TileNumber:  - Platform and space.  Altered to make platform cross members wider
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%33333333
    word    %%22222222
    word    %%00333300
' TileNumber:  - Platform and space.  Altered to make platform cross members wider
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%33333333
    word    %%22222222
' TileNumber: 139 - Platform and space.  Altered to make platform cross members wider
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%00000000
    word    %%33333333

tile_palette_def
    long    $42dc4b4c '0 = Level 1 and 2 main palette - Black,Cyan,red,red
    long    $a2acaaa5 '1 = Black,Green, ---, Grey
    long    $e2ebebeb '2 = Level blue
    long    $020b5b7e '3 = Mario
    long    $0205fdfb '4 = Black,White,Black,Black
    long    $02575e5b '5 = Kong palette
    long    $026c6b0d '6 = Fixed barrels
    long    $02065d5b '7 = Level 3 main palette - Black,White,Yellow,Orange
    long    $026dfdfb '8 = Level 4 main palette - Black,Yellow,Blue,Blue
    long    $024cfdfb '9 = Bonus short time - red

sprite_palette_def
    long    $0b5b7e 'SpriteNumber:0
    long    $0b5b7e 'SpriteNumber:1
    long    $0b5b7e 'SpriteNumber:2
    long    $0b5b7e 'SpriteNumber:3
    long    $0b5b7e 'SpriteNumber:4
    long    $0b5b7e 'SpriteNumber:5
    long    $0b5b7e 'SpriteNumber:6
    long    $0b5b7e 'SpriteNumber:7
    long    $0b5b7e 'SpriteNumber:8
    long    $0b5b7e 'SpriteNumber:9
    long    $0b5b7e 'SpriteNumber:10
    long    $0b5b7e 'SpriteNumber:11
    long    $0b5b7e 'SpriteNumber:12
    long    $0b5b7e 'SpriteNumber:13
    long    $0b5b7e 'SpriteNumber:14

    long    $3c6c06 'SpriteNumber:15  'Pauline head
    long    $3c060c 'SpriteNumber:16  'Pauline legs
    long    $3c060c 'SpriteNumber:17  'Pauline legs
    long    $3c060c 'SpriteNumber:18  'Pauline atanding
    long    $3c060c 'SpriteNumber:19  'Pauline carried

    long    $6c6b6a 'SpriteNumber:20  'Normal barrel palette
    long    $6c6b0d 'SpriteNumber:21  'Normal barrel palette
    long    $6c6b0d 'SpriteNumber:22  'Normal barrel palette
    long    $6c6b0d 'SpriteNumber:23  'Normal barrel palette

    long    $9b029c 'SpriteNumber:24  'Skull barrel palette
    long    $9c023c 'SpriteNumber:25  'Skull barrel palette
    long    $9c023c 'SpriteNumber:26  'Skull barrel palette

    long    $055b5b 'SpriteNumber:27  'Hammer palette
    long    $055b5b 'SpriteNumber:28  'Hammer palette

    long    $575d5b 'SpriteNumber:29  'Kong head palette - has white eyes and teeth - tweaked white down a littel to stop glaring teeth
    long    $575d5b 'SpriteNumber:30  'Kong head palette - has white eyes and teeth - tweaked white down a littel to stop glaring teeth
    long    $575d5b 'SpriteNumber:31  'Kong head palette - has white eyes and teeth - tweaked white down a littel to stop glaring teeth
    long    $575d5b 'SpriteNumber:32  'Kong head palette - has white eyes and teeth - bright white eyes
    long    $565d5b 'SpriteNumber:33  'Kong head palette - has white eyes and teeth - tweaked white down a littel to stop glaring teeth
    long    $7b5d5b 'SpriteNumber:34  'Kong body palette
    long    $7b5d5b 'SpriteNumber:35  'Kong body palette
    long    $7b5d5b 'SpriteNumber:36  'Kong body palette
    long    $7b5d5b 'SpriteNumber:37  'Kong body palette
    long    $7b5d5b 'SpriteNumber:38  'Kong body palette
    long    $7b5d5b 'SpriteNumber:39  'Kong body palette
    long    $7b5d5b 'SpriteNumber:40  'Kong body palette
    long    $7b5d5b 'SpriteNumber:41  'Kong body palette
    long    $7b5d5b 'SpriteNumber:42  'Kong body palette
    long    $7b5d5b 'SpriteNumber:43  'Kong body palette
    long    $7b5d5b 'SpriteNumber:44  'Kong body palette
    long    $575d5b 'SpriteNumber:45  'Kong head palette - has white eyes and teeth - bright white eyes

    long    $7b5d5b 'SpriteNumber:46  'Kong body palette
    long    $7b5d5b 'SpriteNumber:47  'Kong body palette
    long    $7b5d5b 'SpriteNumber:48  'Kong body palette
    long    $7b5d5b 'SpriteNumber:49  'Kong body palette
    long    $7b5d5b 'SpriteNumber:50  'Kong body palette
    long    $7b5d5b 'SpriteNumber:51  'Kong body palette
    long    $7b5d5b 'SpriteNumber:52  'Kong body palette
    long    $070707 'SpriteNumber:53  'Kong's stars
    long    $070707 'SpriteNumber:54  'Bonus 200
    long    $6bcb1d 'SpriteNumber:55  'Spring open
    long    $6bcb1d 'SpriteNumber:56  'Spring closed

    long    $076ce8 'SpriteNumber:57  'Fire palette
    long    $076ce8 'SpriteNumber:58  'Fire palette
    long    $076ce8 'SpriteNumber:59  'Fire palette
    long    $076ce8 'SpriteNumber:60  'Fire palette
    long    $076ce8 'SpriteNumber:61  'Fire palette
    long    $076ce8 'SpriteNumber:62  'Fire palette

    long    $dc4b4c 'SpriteNumber:63  'Lift
    long    $026a7e 'SpriteNumber:64  'Winch
    long    $060606 'SpriteNumber:65  'Ladder

    long    $0bcc06 'SpriteNumber:66  'Oil drum palette

    long    $0c6e6c 'SpriteNumber:67  'Pie
    long    $070707 'SpriteNumber:68  'Bonus 300
    long    $070707 'SpriteNumber:69  'Bonus 500
    long    $070707 'SpriteNumber:70  'Bonus 800
    long    $5bcc1d 'SpriteNumber:71  'Motor
    long    $5bcc1d 'SpriteNumber:72  'Motor
    long    $5bcc1d 'SpriteNumber:73  'Motor
    long    $0b5b7e 'SpriteNumber:74  'Barrel smashing anim
    long    $0b5b7e 'SpriteNumber:75  'Barrel smashing anim
    long    $0b5b7e 'SpriteNumber:76  'Barrel smashing anim
    long    $0b5b7e 'SpriteNumber:77  'Barrel smashing anim
    long    $02dc06 'SpriteNumber:78  'Hi Score Entry Cursor

    long    $3c060c 'SpriteNumber:79  'Umbrella
    long    $3c060c 'SpriteNumber:80  'Hat
    long    $3c060c 'SpriteNumber:81  'Handbag

    long    $4c4d4d 'SpriteNumber:82  'Heart
    long    $4c4d4d 'SpriteNumber:83  'Broken heart
    long    $0b5b7e 'SpriteNumber:84
    long    $0b5b7e 'SpriteNumber:85
    long    $0b5b7e 'SpriteNumber:86
    long    $070707 'SpriteNumber:87  'Bonus
{
    long    $070707 'SpriteNumber:88
    long    $070707 'SpriteNumber:89
    long    $070707 'SpriteNumber:90
    long    $070707 'SpriteNumber:91
}

sprite_def

' TileNumber:  0
    long    %%0000000222200000
    long    %%0000222222220000
    long    %%0000003311110000
    long    %%0003331331331000
    long    %%0033313311331000
    long    %%0001111333311100
    long    %%0000333333300000
    long    %%0000001111110000
    long    %%0000011221111000
    long    %%0000022322111000
    long    %%0000222221111000
    long    %%0000222233312000
    long    %%0000222223322000
    long    %%0000022202222000
    long    %%0000011100111000
    long    %%0000111101111000

' TileNumber:  1
    long    %%0000000222200000
    long    %%0000222222220000
    long    %%0000003331110000
    long    %%0003331311331000
    long    %%0033313311331000
    long    %%0001111333311100
    long    %%0000333333300000
    long    %%0330001122111100
    long    %%0333111222111133
    long    %%0331322232110333
    long    %%0010222222220033
    long    %%0011222222222000
    long    %%0011222222222200
    long    %%0011222000221110
    long    %%0000000000001110
    long    %%0000000000011100

' TileNumber:  2
    long    %%0000000000000000
    long    %%0000002222000000
    long    %%0002222222200000
    long    %%0000033111100000
    long    %%0033313313310000
    long    %%0333133113310000
    long    %%0011113333111000
    long    %%0003333333000000
    long    %%0000302111110000
    long    %%0003331111113000
    long    %%0000331111223300
    long    %%0000222222222211
    long    %%0000222222222211
    long    %%0000022200222211
    long    %%0000001110000001
    long    %%0000011110000000

' TileNumber:  3
    long    %%0003300000000000
    long    %%0003322222200000
    long    %%0001112222100000
    long    %%0011111111110000
    long    %%0011111111110000
    long    %%0011133333311100
    long    %%0001121111211110
    long    %%0001121111211110
    long    %%0002222112222000
    long    %%0022222222222000
    long    %%0022222222222000
    long    %%0002222222222000
    long    %%0001111022220000
    long    %%0000000211120000
    long    %%0000001111110000
    long    %%0000001111100000

' TileNumber:  4
    long    %%0000000000000000
    long    %%0000011333330000
    long    %%0000112111121300
    long    %%0031112211222130
    long    %%0331222222222200
    long    %%0332222222222220
    long    %%0022222222222220
    long    %%0022222222222220
    long    %%0002222222222220
    long    %%0000222202222200
    long    %%0000111122222000
    long    %%0000011122222000
    long    %%0000000211120000
    long    %%0000000111110000
    long    %%0000001111110000
    long    %%0000001111100000

' TileNumber:  5
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0002222112200000
    long    %%0022222222220000
    long    %%0222222222222100
    long    %%2222222222222210
    long    %%2222222222222210
    long    %%0022222022222211
    long    %%0333111122222233
    long    %%0000000002222200
    long    %%0000000022222200
    long    %%0000000021112000
    long    %%0000000111110000
    long    %%0000000011100000
    long    %%0000000000000000

' TileNumber:  6
    long    %%0000000000000000
    long    %%0000022222200000
    long    %%0000012222100000
    long    %%0000111111110000
    long    %%0000111111110000
    long    %%0000033333300000
    long    %%0001121111211000
    long    %%0011121111211100
    long    %%0111221111221110
    long    %%3112222112222113
    long    %%3332222222222333
    long    %%0002222222222000
    long    %%0002222222222000
    long    %%0002222002222000
    long    %%0000222222220000
    long    %%0001111111111000

' TileNumber:  7
    long    %%0000000333200000
    long    %%0000222333220000
    long    %%0000003333110000
    long    %%0003331111331000
    long    %%0033313111331000
    long    %%0001111111311100
    long    %%0000333111100000
    long    %%0000002111110000
    long    %%0000012211110000
    long    %%0000022321110000
    long    %%0000222222220000
    long    %%0000222222222000
    long    %%0000222222222000
    long    %%0000022202222000
    long    %%0000011100111000
    long    %%0000111101111000

' TileNumber:  8
    long    %%0000000222200000
    long    %%0000222222220000
    long    %%0000003311110000
    long    %%0003331331331000
    long    %%0033313311331000
    long    %%0001111333311100
    long    %%0000333333300000
    long    %%0000001221110000
    long    %%3331111111111000
    long    %%3331111111111000
    long    %%0333222222112000
    long    %%0000222222222000
    long    %%0000222222222000
    long    %%0000022202222000
    long    %%0000011100111000
    long    %%0000111101111000

' TileNumber:  9
    long    %%0000000333200000
    long    %%0000222333220000
    long    %%0000003333110000
    long    %%0003331111331000
    long    %%0033313111331000
    long    %%0001111111111100
    long    %%0000333111100000
    long    %%0000002111110000
    long    %%0000002221110000
    long    %%0000022321110000
    long    %%0010022222220000
    long    %%0011222222222000
    long    %%0011222222222200
    long    %%0011222000222110
    long    %%0000000000001110
    long    %%0000000000011100

' TileNumber: 10
    long    %%0000000222200000
    long    %%0000222222220000
    long    %%0000003311110000
    long    %%0003331331331000
    long    %%0033313311331000
    long    %%0001111333311100
    long    %%0000333333300000
    long    %%0000001221100000
    long    %%3333111111110000
    long    %%3333111111110000
    long    %%0333022221120000
    long    %%0011222222222000
    long    %%0011222222222200
    long    %%0011222000222110
    long    %%0000000000001110
    long    %%0000000000011100

' TileNumber: 11
    long    %%0000003330000000
    long    %%0000003332000000
    long    %%0002223332200000
    long    %%0000031111100000
    long    %%0033311113310000
    long    %%0333131113310000
    long    %%0011111113111000
    long    %%0003331111000000
    long    %%0000011111110000
    long    %%0000022111110000
    long    %%0000223211220000
    long    %%0000222222222211
    long    %%0000222222222211
    long    %%0000022200222211
    long    %%0000001110000001
    long    %%0000011110000000

' TileNumber: 12
    long    %%0000000000000000
    long    %%0000002222000000
    long    %%0002222222200000
    long    %%0000033111100000
    long    %%0033313313310000
    long    %%0333133113310000
    long    %%0011113333111000
    long    %%0003333333000000
    long    %%3330012111110000
    long    %%3333111111110000
    long    %%0333111111220000
    long    %%0000222222222211
    long    %%0000222222222211
    long    %%0000022200222211
    long    %%0000001110000001
    long    %%0000011110000000

' TileNumber: 13
    long    %%0000000000000000
    long    %%0000002222000000
    long    %%0002222222200000
    long    %%0000033111100000
    long    %%0033313313310000
    long    %%0333133113310000
    long    %%0011113333111000
    long    %%3303333333000330
    long    %%3310011221111333
    long    %%0111112221110300
    long    %%0000222321100000
    long    %%1000222222201100
    long    %%1122222222211110
    long    %%1122222222221011
    long    %%0000000022220000
    long    %%0000000002200000

' TileNumber: 14
    long    %%0000000222200000
    long    %%0000222222220000
    long    %%0000003311110000
    long    %%0003331331331000
    long    %%0033313311331000
    long    %%0001111333311100
    long    %%0000333333331110
    long    %%0011111112111111
    long    %%3333111221111011
    long    %%3330022232112333
    long    %%0000022222222030
    long    %%0000222222222000
    long    %%0102112222222000
    long    %%0112111222220030
    long    %%0011211122203300
    long    %%0001101110030033

' TileNumber: 15
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000022222200
    long    %%0000000222222220
    long    %%0000002222333300
    long    %%0220002232332330
    long    %%0022000223333300
    long    %%0002222233333000
    long    %%0222221133300000
    long    %%0022221113310000
    long    %%0202001111110000

' TileNumber: 16
    long    %%0000001111100000
    long    %%0000000111111220
    long    %%0000000333111200
    long    %%0000011111110000
    long    %%0001111111112200
    long    %%1122111111111200
    long    %%0112221111111000
    long    %%0011122111111000
    long    %%0033111222111000
    long    %%0333301112222000
    long    %%0033000011111000
    long    %%0003300003330000
    long    %%0000000003033300
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 17 - $11
    long    %%0000011111110000
    long    %%0000110333311000
    long    %%0001101111111100
    long    %%0222011111112200
    long    %%0000011111111200
    long    %%0000111111111000
    long    %%0000111111112200
    long    %%0002221112222111
    long    %%0001122222111103
    long    %%0011111111133330
    long    %%0000333000033300
    long    %%0003330000003000
    long    %%0000333000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 18 - $12
    long    %%0000011111110000
    long    %%0001113333011000
    long    %%0011011111101100
    long    %%2221111111111222
    long    %%0020111111110200
    long    %%0011111111111100
    long    %%0011111111111100
    long    %%0011111111111100
    long    %%0022211111222200
    long    %%0111222222221110
    long    %%0001111111111000
    long    %%0000333003030000
    long    %%0003303003033000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 19 - $13
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000011111110000
    long    %%0000111111111000
    long    %%0022222222211100
    long    %%0111111111121100
    long    %%1133212211112220
    long    %%2233322222111110
    long    %%0333322222111111
    long    %%0333302200211222
    long    %%0033000000013300
    long    %%0000000000033300
    long    %%0000000000333300
    long    %%0000000003333000
    long    %%0000000000333000

' TileNumber: 20 - $14
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000001111000000
    long    %%0000111221110000
    long    %%0000122223310000
    long    %%0001222223321000
    long    %%0001222222221000
    long    %%0001222222221000
    long    %%0001222222221000
    long    %%0000122222210000
    long    %%0000111221110000
    long    %%0000001111000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
' TileNumber: 21 - $15
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000222222220000
    long    %%0032211111122300
    long    %%0231111111111320
    long    %%0132222222222310
    long    %%0132222222222310
    long    %%0132222222222310
    long    %%0132222222222310
    long    %%0231111111111320
    long    %%0032211111122300
    long    %%0000222222220000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 22 - $16
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000111111110000
    long    %%0031122222211300
    long    %%0132222222222310
    long    %%0131111111111310
    long    %%0131111111111310
    long    %%0131111111111310
    long    %%0131111111111310
    long    %%0132222222222310
    long    %%0031122222211300
    long    %%0000111111110000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 23
    long    %%0000000000000000
    long    %%0000021111200000
    long    %%0000333333330000
    long    %%0000212222120000
    long    %%0002212222122000
    long    %%0002112222112000
    long    %%0002112222112000
    long    %%0002112222112000
    long    %%0002112222112000
    long    %%0002112222112000
    long    %%0002112222112000
    long    %%0002212222122000
    long    %%0000212222120000
    long    %%0000333333330000
    long    %%0000011111200000
    long    %%0000000000000000

' TileNumber: 24 - $18
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000003333000000
    long    %%0000331111330000
    long    %%0000311111130000
    long    %%0003111111113000
    long    %%0003111111113000
    long    %%0003111111113000
    long    %%0003122111113000
    long    %%0000322111130000
    long    %%0000331111330000
    long    %%0000003333000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
' TileNumber: 25 - $19
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000222222220000
    long    %%0032211111122300
    long    %%0231111111111320
    long    %%0132222222222310
    long    %%0132222222222310
    long    %%0132222222222310
    long    %%0132222222222310
    long    %%0231111111111320
    long    %%0032211111122300
    long    %%0000222222220000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 26
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000111111110000
    long    %%0031122222211300
    long    %%0132222222222310
    long    %%0131111111111310
    long    %%0131111111111310
    long    %%0131111111111310
    long    %%0131111111111310
    long    %%0132222222222310
    long    %%0031122222211300
    long    %%0000111111110000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 27
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000120000000
    long    %%0000111111310000
    long    %%0001333333331000
    long    %%0001333333332000
    long    %%0001333333332000
    long    %%0000333333320000
    long    %%0000000120000000
    long    %%0000000120000000
    long    %%0000000120000000
    long    %%0000000120000000

' TileNumber: 28
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000033333000000
    long    %%0000313333300000
    long    %%0000333333300000
    long    %%0002313333322222
    long    %%0000313333300000
    long    %%0100313333300000
    long    %%0010313333300100
    long    %%0100033333001000
    long    %%1011100000010010
    long    %%0000000000000000

' TileNumber: 29 - $1d
    long    %%0003333333333000
    long    %%0033323332233300
    long    %%0033222322111130
    long    %%0332222221111113
    long    %%2322112221111113
    long    %%2321111221111113
    long    %%2331131221311113
    long    %%2332112332111133
    long    %%2222222222222222
    long    %%2221111111111122
    long    %%2211311131113113
    long    %%2313331133333333
    long    %%2333333333333132
    long    %%2211311131113222
    long    %%3222111111111223
    long    %%3332222222222233

' TileNumber: 30 - $1e
    long    %%0003333333333000
    long    %%0033323332233300
    long    %%0033222322111130
    long    %%0332222221311113
    long    %%2322112221111113
    long    %%2323111221111113
    long    %%2331111221111113
    long    %%2332112332111133
    long    %%2222222222222222
    long    %%2221111111111122
    long    %%2211311131113113
    long    %%2313331133333333
    long    %%2333333333333132
    long    %%2211311131113222
    long    %%3222111111111223
    long    %%3332222222222233

' TileNumber: 31 - $1f
    long    %%0003333333333000
    long    %%0033323332233300
    long    %%0033222322111130
    long    %%0332222221111113
    long    %%2322132221111113
    long    %%2321111221111313
    long    %%2331111221111113
    long    %%2332112332111133
    long    %%2222222222222222
    long    %%2221111111111122
    long    %%2211311131113113
    long    %%2313331133333333
    long    %%2333333333333132
    long    %%2211311131113222
    long    %%3222111111111223
    long    %%3332222222222233

' TileNumber: 32 - $20
    long    %%0000033333330000
    long    %%0000333333333000
    long    %%0003322333223300
    long    %%0033222232222330
    long    %%2332221121122232
    long    %%2333221323122332
    long    %%2333222222222332
    long    %%2222222333222222
    long    %%2222222222222222
    long    %%2223333333333322
    long    %%2232222222222232
    long    %%3322222222222223
    long    %%3322222222222223
    long    %%3333222222222333
    long    %%3333333333333333
    long    %%3333333333333333

' TileNumber: 33
    long    %%0000033333330000
    long    %%0000333333333000
    long    %%0003322333223300
    long    %%0033222232222330
    long    %%2332221121122232
    long    %%2333221323122332
    long    %%2333222222222332
    long    %%2222222333222222
    long    %%2211222222222112
    long    %%2111311131113111
    long    %%2313331333133313
    long    %%2211311131113112
    long    %%3222211121112223
    long    %%3333222222222333
    long    %%3333333333333333
    long    %%3333333333333333

' TileNumber: 34
    long    %%3212121331212123
    long    %%3122222112222213
    long    %%1222222112222221
    long    %%1223222112223221
    long    %%1222221221222221
    long    %%3111112222111113
    long    %%3312222222222133
    long    %%0312121221212130
    long    %%0031212123121300
    long    %%0013313133133100
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 35
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000111133100
    long    %%0000011333333300
    long    %%0000133333333300
    long    %%0000133333333130
    long    %%0000333333333333
    long    %%0000333333333333
    long    %%0003333333333333
    long    %%0222333333330000
    long    %%2223222333300000
    long    %%2222223222200000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 36
    long    %%0000133333000000
    long    %%0000313330000000
    long    %%0000133330000000
    long    %%0000133330000000
    long    %%0000313310000000
    long    %%0000133331000000
    long    %%0000333333100000
    long    %%0003333333310000
    long    %%0033333333331000
    long    %%0033333333331000
    long    %%3333333333331000
    long    %%0333333333330000
    long    %%0033333333330000
    long    %%0002322333223200
    long    %%0002232222322120
    long    %%0000222222222212

' TileNumber: 37
    long    %%0000002222000000
    long    %%0000022122220000
    long    %%0000221222122200
    long    %%0000011121222200
    long    %%2000022212221230
    long    %%2200002222211330
    long    %%2230000001133330
    long    %%3333333331133333
    long    %%2333333333133333
    long    %%2333333333313333
    long    %%2333333333333330
    long    %%3333333333333300
    long    %%3333333333330000
    long    %%3333333333300000
    long    %%3333333330000000
    long    %%3333333000000000

' TileNumber: 38
    long    %%0000000000200000
    long    %%0000000002200000
    long    %%0000000002200000
    long    %%0000000133300000
    long    %%0000011333200000
    long    %%0000133333200000
    long    %%0011333333200000
    long    %%0133333333300000
    long    %%3333313333300000
    long    %%3333331133300000
    long    %%3333333111300000
    long    %%3333333311102220
    long    %%0333333333332222
    long    %%0033333333333232
    long    %%0003333333333322
    long    %%0000003333333230

' TileNumber: 39
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%2000000000000000
    long    %%2201110000000000
    long    %%2233311000000000
    long    %%3313331100000000
    long    %%2333333110000000
    long    %%2333333311000000
    long    %%2333333331100000
    long    %%3333333333100000
    long    %%3333333333100000
    long    %%3333333333100000
    long    %%3133333333310000
    long    %%3113333333310000

' TileNumber: 40
    long    %%0311333333310000
    long    %%0331333333110000
    long    %%0331333333100000
    long    %%0333233333100000
    long    %%0033333333100000
    long    %%2211133333000000
    long    %%2221133333000000
    long    %%0021233330000000
    long    %%0022323300000000
    long    %%0023211300000000
    long    %%0022110000000000
    long    %%0222100000000000
    long    %%0220000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 41
    long    %%0000000000000000
    long    %%1000000000000000
    long    %%3300000000000000
    long    %%3330000000000000
    long    %%3333000000000000
    long    %%3333000000000000
    long    %%3333000000000000
    long    %%3333110000000000
    long    %%3333311332000000
    long    %%3333333332100000
    long    %%3333333322100000
    long    %%3333333322100000
    long    %%3333333332100000
    long    %%0000333232000000
    long    %%0002223222000000
    long    %%0021212220000000

' TileNumber: 42
    long    %%0000000033333333
    long    %%0000000033333333
    long    %%0000000333333333
    long    %%0000000333333333
    long    %%0000003333333332
    long    %%0000033333333321
    long    %%0000033333333022
    long    %%0000333333330332
    long    %%2222333333303333
    long    %%2222233333003333
    long    %%0022333330033333
    long    %%0022233330033333
    long    %%0222222300003333
    long    %%2221222002123323
    long    %%2212220021232222
    long    %%0222000021222222

' TileNumber: 43
    long    %%3311333333333331
    long    %%3113333333333333
    long    %%1133333333333333
    long    %%2233333333333333
    long    %%2233333333333333
    long    %%2111333333333333
    long    %%1111113333333333
    long    %%2211111133333333
    long    %%3321111111333333
    long    %%3333111111133333
    long    %%3333331111133333
    long    %%3333000000003333
    long    %%3333000000000333
    long    %%3223300000000000
    long    %%2222300000000000
    long    %%2222200000000000

' TileNumber: 44
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%1113000000000000
    long    %%3331111000000000
    long    %%3333333110000000
    long    %%3333333331000000
    long    %%3333113333100000
    long    %%3333313333310000
    long    %%3333313333310000
    long    %%3333313333331000
    long    %%3333313333331000
    long    %%3333313333331000
    long    %%3333113333331100
    long    %%3333133333333100
    long    %%3331133333333110

' TileNumber: 45
    long    %%0000000333333000
    long    %%0000003333333330
    long    %%0000002223333333
    long    %%0000022222233333
    long    %%0000021122223333
    long    %%0003323122233223
    long    %%0022222222332222
    long    %%0222222223332222
    long    %%0222222233333233
    long    %%2222222223332233
    long    %%2333332222322333
    long    %%0222223222223333
    long    %%0022222322233333
    long    %%0002222222233333
    long    %%0000002222333333
    long    %%0000000003333333

' TileNumber: 46 - $2e
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%1111110000000000
    long    %%0333331110000000
    long    %%0033333331110000
    long    %%0033333333331100
    long    %%0033333333333310
    long    %%0033333333133331
    long    %%0033000031333333
    long    %%0000000001333330
    long    %%0000003333333300
    long    %%0000221113333000
    long    %%0000221122100000
    long    %%0000222222000000

' TileNumber: 47 - $2f
    long    %%0000000000000113
    long    %%0000000000011133
    long    %%0000000000113333
    long    %%0000000000133333
    long    %%0000000001133333
    long    %%0000000011333333
    long    %%0000000111133333
    long    %%0000001113313333
    long    %%0000003333331333
    long    %%0000003333333333
    long    %%0000003333333333
    long    %%0000003333333333
    long    %%0000033333333333
    long    %%0000003333333333
    long    %%0000033333333333
    long    %%0000033333333333

' TileNumber: 48 - $30
    long    %%3310000000000000
    long    %%3331100000000000
    long    %%3333310000000000
    long    %%3333310000000000
    long    %%3333331000000000
    long    %%3333333100000000
    long    %%3333311330000000
    long    %%3333133333000000
    long    %%3331333333000000
    long    %%3333333333000000
    long    %%3333333333000000
    long    %%3333333333000000
    long    %%3333333333300000
    long    %%3333333333000000
    long    %%3333333333300000
    long    %%3333333333300000

' TileNumber: 49 - $31
    long    %%0000000000232220
    long    %%0000000003333322
    long    %%0000000003333212
    long    %%0000000033333320
    long    %%0000000333333300
    long    %%0000003333333300
    long    %%0000003333333000
    long    %%0000033333333000
    long    %%0000033333330000
    long    %%0000033313300000
    long    %%0000033313000000
    long    %%0000033330000000
    long    %%0000003330000000
    long    %%0000003330000000
    long    %%0000000330000000
    long    %%0000000030000000

' TileNumber: 50 - $32
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0022322330000000
    long    %%0002233333330000
    long    %%0002333333333300
    long    %%0000333333333330
    long    %%0000033333333333
    long    %%0000003333113333
    long    %%0000000111133333
    long    %%0000000333333333
    long    %%0000000333333330
    long    %%0000000333333300
    long    %%0000000333333000
    long    %%0000000333300000

' TileNumber: 51 - $33
    long    %%0000013333333333
    long    %%0000133333333333
    long    %%0000133133333333
    long    %%0001333313333333
    long    %%0011333331333311
    long    %%0013333333111111
    long    %%0033333333311100
    long    %%0003333333333000
    long    %%0000333333333000
    long    %%0000333333333000
    long    %%0000033333333300
    long    %%0000003333333330
    long    %%0000003332222230
    long    %%0000033222111200
    long    %%0000322211111000
    long    %%0000222111110000

' TileNumber: 52 - $34
    long    %%3333333333310000
    long    %%3333333333331000
    long    %%3333333313333100
    long    %%3333333133333310
    long    %%3333331333333310
    long    %%1111113333333330
    long    %%1111333333333300
    long    %%0000332223333000
    long    %%0000021112230000
    long    %%0000011111222000
    long    %%0000001111122000
    long    %%0000000111112200
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 53 - $35
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000030
    long    %%0000000000000300
    long    %%0000000000003000
    long    %%0300000000000000
    long    %%0030000000000000
    long    %%0003000300000003
    long    %%0000000030030030
    long    %%0000000003030300
    long    %%0000000000030000
    long    %%0030300033333330
    long    %%0000000000030000
    long    %%0000030003030300
    long    %%0000300030000030

{' TileNumber: 54
    long    %%1111111111111111  'cut
    long    %%1111111111111111
    long    %%1111111111111111
    long    %%1111111111111111
    long    %%1111111111111111
    long    %%1111111111111111
    long    %%1111111111111111
    long    %%1111111111111111
    long    %%1111111111111111
    long    %%1111111111111111
    long    %%1111111111111111
    long    %%1111111111111111
    long    %%1111111111111111
    long    %%1111111111111111
    long    %%1111111111111111
    long    %%1111111111111111
}
' TileNumber: 88
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0011100011000110
    long    %%0100010100101001
    long    %%0000010100101001
    long    %%0000100100101001
    long    %%0001000100101001
    long    %%0010000100101001
    long    %%0111110011000110
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000





' TileNumber: 55 - $37
    long    %%0000000000000000
    long    %%0033333333333300
    long    %%0003333333333000
    long    %%0000000110000000
    long    %%0000001331000000
    long    %%0000011221100000
    long    %%0000110330110000
    long    %%0001100220011000
    long    %%0013100330013100
    long    %%0001100220011000
    long    %%0000110330110000
    long    %%0000011221100000
    long    %%0000001331000000
    long    %%0000000110000000
    long    %%0003333333333000
    long    %%0033333333333300

' TileNumber: 56 - $38
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0033333333333300
    long    %%0003333333333000
    long    %%0000000110000000
    long    %%0000111331110000
    long    %%0111100330011110
    long    %%1310000330000131
    long    %%0111100330011110
    long    %%0000111331110000
    long    %%0000000110000000
    long    %%0003333333333000
    long    %%0033333333333300

' TileNumber: 57 - $39
    long    %%0000000000000000
    long    %%0000020000200020
    long    %%0000203000000000
    long    %%0000020020000030
    long    %%0000022002000300
    long    %%0003002022020020
    long    %%0000002222200200
    long    %%0000023223202000
    long    %%0000223332220000
    long    %%0002233113322000
    long    %%0002321123332000
    long    %%0002321123332000
    long    %%0002231133322000
    long    %%0000223333220000
    long    %%0000222222220000
    long    %%0000002222000000

' TileNumber: 58 - $3a
    long    %%0000000000002000
    long    %%0000000020000000
    long    %%0000000203020200
    long    %%0003002200220000
    long    %%0000022002200203
    long    %%0000022322202000
    long    %%0030232233222000
    long    %%0002223333320000
    long    %%0002333113320200
    long    %%0022311111322000
    long    %%0023121121332000
    long    %%0023121121332000
    long    %%0022311113322000
    long    %%0002333333220000
    long    %%0002223332200000
    long    %%0000022222000000

' TileNumber: 59
    long    %%0000000000000000
    long    %%0000000100000000
    long    %%0000000000203000
    long    %%0000002000000200
    long    %%0000000030000003
    long    %%0003000003003000
    long    %%0000020020200200
    long    %%0000000003002300
    long    %%0000000022023000
    long    %%0000000230022000
    long    %%0002102220312300
    long    %%0032032332033130
    long    %%0023203233023330
    long    %%0323021112331200
    long    %%0233203110213000
    long    %%0033322123113000

' TileNumber: 60
    long    %%0000000000000000
    long    %%0000002000100300
    long    %%0000020000001000
    long    %%0000000002000000
    long    %%0000000300000030
    long    %%0000000030030000
    long    %%0000230000200003
    long    %%0000020003200020
    long    %%0000200022000220
    long    %%0002300232000200
    long    %%0022000033202200
    long    %%0322000233232200
    long    %%0222200231222000
    long    %%0031202312223200
    long    %%0033132132313200
    long    %%0003122132113000

' TileNumber: 61
    long    %%0000000200000001
    long    %%0002000020000200
    long    %%1000200032000200
    long    %%0000200223002230
    long    %%0002302232022320
    long    %%0023222232233200
    long    %%0022322332332020
    long    %%0023233332320200
    long    %%2022331333322000
    long    %%2302333133322000
    long    %%0232313133332203
    long    %%0023331111332220
    long    %%2022331311323202
    long    %%2222231331132232
    long    %%0323011333133320
    long    %%0233313333113220

' TileNumber: 62
    long    %%0020000000200020
    long    %%0200000002000030
    long    %%0020000000000000
    long    %%3003002000000000
    long    %%0000020000020002
    long    %%0020032000200000
    long    %%0220033200230000
    long    %%0232023320320003
    long    %%0023221322322000
    long    %%2002333133332000
    long    %%0302333111332000
    long    %%0022331111332000
    long    %%0021311111132020
    long    %%2023111311132220
    long    %%0302113333111300
    long    %%0221113333313320

' TileNumber: 63 - $3F
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%3333333333333333
    long    %%2222222222222222
    long    %%0033330000333300
    long    %%0333333003333330
    long    %%3330033333300333
    long    %%3300003333000033
    long    %%3333333333333333
    long    %%2222222222222222
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 64 - $40
    long    %%3333333333333333
    long    %%3333333223333333
    long    %%3131332112331313
    long    %%3333332112333333
    long    %%2222333223332222
    long    %%1111233333321111
    long    %%3331123333211333
    long    %%1111112222111111
    long    %%3333333333333333
    long    %%3132222222222313
    long    %%3332111313312333
    long    %%3132113133112313
    long    %%3332131331112333
    long    %%3132222222222313
    long    %%3333333333333333
    long    %%0333333333333330

' TileNumber: 65 - $41
    long    %%0001100000011000
    long    %%0001111111111000
    long    %%0001100000011000
    long    %%0001100000011000
    long    %%0001100000011000
    long    %%0001111111111000
    long    %%0001100000011000
    long    %%0001100000011000
    long    %%0001100000011000
    long    %%0001111111111000
    long    %%0001100000011000
    long    %%0001100000011000
    long    %%0001100000011000
    long    %%0001111111111000
    long    %%0001100000011000
    long    %%0001100000011000

' TileNumber: 66
    long    %%1311111111111111
    long    %%0131111111111110
    long    %%0131111111111110
    long    %%0131111111111110
    long    %%0232222222222220
    long    %%0131111111111110
    long    %%0131333133133110
    long    %%0131313133133110
    long    %%0131333133133310
    long    %%0131111111111110
    long    %%0232222222222220
    long    %%0131111111111110
    long    %%0131331331331310
    long    %%0131111111111110
    long    %%0131111111111110
    long    %%1311111111111111

' TileNumber: 67 - $43
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000033200000
    long    %%0000332322220000
    long    %%0003222223323200
    long    %%0322332322232220
    long    %%3223223222322222
    long    %%1111111111111111
    long    %%1111111111111111
    long    %%0111111111111110
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
{
' TileNumber: 68
    long    %%0001100000000000 'cut
    long    %%0011111100000000
    long    %%0111111111100000
    long    %%1111111111111100
    long    %%0001111111111111
    long    %%0003322111111111
    long    %%0003322222111111
    long    %%0000232222222111
    long    %%0000033222220000
    long    %%0003332322203200
    long    %%0033233332023320
    long    %%0033332322033220
    long    %%0023223222002200
    long    %%0032232220000000
    long    %%0003322220000000
    long    %%0000222200000000

' TileNumber: 69 - $45
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0002222200000000
    long    %%0022333322000200
    long    %%0223333332202002
    long    %%0233212133220200
    long    %%0233212113320220
    long    %%0023111133320222
    long    %%0223311333202232
    long    %%0223333333322322
    long    %%0222333333333220
    long    %%0222223333332200
    long    %%0002222333322020
    long    %%0000002222220000

' TileNumber: 70
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0002222200000000
    long    %%0022333322000000
    long    %%0223333332200000
    long    %%0231212133220200
    long    %%0231212113320020
    long    %%0023111113320220
    long    %%0223111113322222
    long    %%2233311133322320
    long    %%2233333333333200
    long    %%2223333333322000
    long    %%0222333332220000
    long    %%0002222222000000
}
' TileNumber: 89
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0111110011000110
    long    %%0000100100101001
    long    %%0001000100101001
    long    %%0011100100101001
    long    %%0000010100101001
    long    %%0000010100101001
    long    %%0111100011000110
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 90
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0111110011000110
    long    %%0100000100101001
    long    %%0111100100101001
    long    %%0000010100101001
    long    %%0000010100101001
    long    %%0100010100101001
    long    %%0011100011000110
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 91
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0011100011000110
    long    %%0100010100101001
    long    %%0100010100101001
    long    %%0011100100101001
    long    %%0100010100101001
    long    %%0100010100101001
    long    %%0011100011000110
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000






' TileNumber: 71 - $47
    long    %%0000003333333000
    long    %%0000333333333000
    long    %%0003331111333000
    long    %%0033311122133000
    long    %%0033111222113000
    long    %%0033112221113000
    long    %%0033312211133000
    long    %%0003331111333000
    long    %%0000333333333000
    long    %%0000003333333000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 72
    long    %%0000003333333000
    long    %%0000333333333000
    long    %%0003331111333000
    long    %%0033311111133000
    long    %%0033122222213000
    long    %%0033222222213000
    long    %%0033311111133000
    long    %%0003331111333000
    long    %%0000333333333000
    long    %%0000003333333000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 73
    long    %%0000003333333000
    long    %%0000333333333000
    long    %%0003331111333000
    long    %%0033312211133000
    long    %%0033112221113000
    long    %%0033111222113000
    long    %%0033311122133000
    long    %%0003331111333000
    long    %%0000333333333000
    long    %%0000003333333000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 74
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000001111000000
    long    %%0000012222100000
    long    %%0000120000210000
    long    %%0001200000021000
    long    %%0012000000002100
    long    %%0012000000002100
    long    %%0012000000002100
    long    %%0012000000002100
    long    %%0001200000021000
    long    %%0000120000210000
    long    %%0000012222100000
    long    %%0000001111000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 75
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000110000000
    long    %%0000001221000000
    long    %%0000012332100000
    long    %%0000123003210000
    long    %%0000123003210000
    long    %%0000012332100000
    long    %%0000001221000000
    long    %%0000000110000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 76
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000110000000
    long    %%0000001221000000
    long    %%0000012332100000
    long    %%0000012332100000
    long    %%0000001221000000
    long    %%0000000110000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 77
    long    %%0000000000000000
    long    %%3000000300000003
    long    %%0100000100000010
    long    %%0020000200000200
    long    %%0002000200002000
    long    %%0000200000020000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%3122000300002213
    long    %%0000000000000000
    long    %%0000200000020000
    long    %%0002000100002000
    long    %%0020000100000200
    long    %%0100000200000010
    long    %%3000000300000003
    long    %%0000000000000000

' TileNumber: 78 - $4E
    long    %%3333333333333333
    long    %%3222222222222223
    long    %%3222222222222223
    long    %%3220000000000223
    long    %%3220000000000223
    long    %%3220000000000223
    long    %%3220000000000223
    long    %%3220000000000223
    long    %%3220000000000223
    long    %%3220000000000223
    long    %%3220000000000223
    long    %%3220000000000223
    long    %%3220000000000223
    long    %%3220000000000223
    long    %%3222222222222223
    long    %%3333333333333333

' TileNumber: 79 - $4F
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000222001000
    long    %%0000002222222100
    long    %%0000022222221000
    long    %%0000022222220000
    long    %%0000333333333000
    long    %%0022222222222220
    long    %%0122211122221121
    long    %%0011100011110010
    long    %%0000000000000000

' TileNumber: 80 - $50
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000011111000000
    long    %%0000100000100000
    long    %%0001000000010000
    long    %%0001011111010000
    long    %%0000111111100000
    long    %%0001212221210000
    long    %%0001121112110000
    long    %%0001112221110000
    long    %%0001111111110000
    long    %%0000000000000000

' TileNumber: 81 - $ 51
    long    %%0000000330000000
    long    %%0000012112100000
    long    %%0001122112211000
    long    %%0011221111221100
    long    %%0112221111222110
    long    %%1122211111122211
    long    %%1122211111122211
    long    %%1102210200122011
    long    %%1000200200020002
    long    %%0000000200000000
    long    %%0000000200000000
    long    %%0000000100000000
    long    %%0000000100000000
    long    %%0000000101000000
    long    %%0000000010000000
    long    %%0000000000000000

' TileNumber: 82 - $52
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0001111000111100
    long    %%0011111101111110
    long    %%0111111111111111
    long    %%0111111111111111
    long    %%0111111111111111
    long    %%0111111111111111
    long    %%0011111111111110
    long    %%0011111111111110
    long    %%0001111111111100
    long    %%0000111111111000
    long    %%0000001111100000
    long    %%0000000111000000
    long    %%0000000010000000
    long    %%0000000000000000

' TileNumber: 83
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0011110000111100
    long    %%0111111001111110
    long    %%1111110011111111
    long    %%1111111001111111
    long    %%1111111100111111
    long    %%1111111001111111
    long    %%0111110011111110
    long    %%0111100111111110
    long    %%0011110011111100
    long    %%0001111001111000
    long    %%0000011100100000
    long    %%0000001001000000
    long    %%0000000100000000
    long    %%0000000000000000

' TileNumber: 84
    long    %%0002022222200000
    long    %%0000222222220000
    long    %%0003131331313000
    long    %%0001333333331000
    long    %%0011311331131100
    long    %%0311331111331130
    long    %%3330033223300333
    long    %%3110121111210113
    long    %%0111122112211110
    long    %%0001232222321000
    long    %%0000222222220011
    long    %%0002222222222111
    long    %%0111222222221111
    long    %%0111112000221110
    long    %%0001110000000000
    long    %%0000000000000000

' TileNumber: 85
    long    %%0000000033000000
    long    %%0000000333300000
    long    %%0111100113000000
    long    %%1111100120110000
    long    %%1111001110111000
    long    %%1112222111333120
    long    %%0222232223313322
    long    %%0022222112113122
    long    %%0022221112133322
    long    %%0022222110113122
    long    %%0222232221313322
    long    %%0222222113333120
    long    %%1112201110111000
    long    %%1111000110110000
    long    %%0111100113300000
    long    %%0011100033000000

' TileNumber: 86
    long    %%0000000003333300
    long    %%0000000030000030
    long    %%0000000300000003
    long    %%0000000030000030
    long    %%0001111003333300
    long    %%0021111110000000
    long    %%0222220110000000
    long    %%0222200000000000
    long    %%0222220000030000
    long    %%2222232100133002
    long    %%2222222213133020
    long    %%2222222123133322
    long    %%0232221113113322
    long    %%0331111113331122
    long    %%3331111133133122
    long    %%3331111211111120

' TileNumber: 87
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0010001110001110
    long    %%0110010001010001
    long    %%0010010001010001
    long    %%0010010001010001
    long    %%0010010001010001
    long    %%0010010001010001
    long    %%0111001110001110
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
{
' TileNumber: 88
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0011100011000110
    long    %%0100010100101001
    long    %%0000010100101001
    long    %%0000100100101001
    long    %%0001000100101001
    long    %%0010000100101001
    long    %%0111110011000110
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 89
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0111110011000110
    long    %%0000100100101001
    long    %%0001000100101001
    long    %%0011100100101001
    long    %%0000010100101001
    long    %%0000010100101001
    long    %%0111100011000110
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 90
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0111110011000110
    long    %%0100000100101001
    long    %%0111100100101001
    long    %%0000010100101001
    long    %%0000010100101001
    long    %%0100010100101001
    long    %%0011100011000110
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000

' TileNumber: 91
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0011100011000110
    long    %%0100010100101001
    long    %%0100010100101001
    long    %%0011100100101001
    long    %%0100010100101001
    long    %%0100010100101001
    long    %%0011100011000110
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
    long    %%0000000000000000
}
sprite_hotspot_def
    byte    8,15    'SpriteNumber:0 First Mario
    byte    8,15    'SpriteNumber:1
    byte    8,15    'SpriteNumber:2
    byte    8,15    'SpriteNumber:3
    byte    8,15    'SpriteNumber:4
    byte    8,15    'SpriteNumber:5
    byte    8,15    'SpriteNumber:6
    byte    8,15    'SpriteNumber:7
    byte    8,15    'SpriteNumber:8
    byte    8,15    'SpriteNumber:9

    byte    8,15    'SpriteNumber:10
    byte    8,15    'SpriteNumber:11
    byte    8,15    'SpriteNumber:12
    byte    8,15    'SpriteNumber:13
    byte    8,15    'SpriteNumber:14 Last Mario
    byte    8,28    'SpriteNumber:15 Pauline head
    byte    8,12    'SpriteNumber:16 Pauline legs
    byte    8,12    'SpriteNumber:17 Pauline legs
    byte    8,12    'SpriteNumber:18 Pauline standing
    byte    8,24    'SpriteNumber:19 Pauline carried

    byte    8,12    'SpriteNumber:20 First Barrel
    byte    8,12    'SpriteNumber:21
    byte    8,12    'SpriteNumber:22
    byte    8,14    'SpriteNumber:23
    byte    8,12    'SpriteNumber:24
    byte    8,12    'SpriteNumber:25
    byte    8,12    'SpriteNumber:26 Last Barrel
    byte    8,15    'SpriteNumber:27 Hammer up
    byte    8,15    'SpriteNumber:28 Hammer down
    byte    8,31    'SpriteNumber:29 Kong head

    byte    8,31    'SpriteNumber:30 Kong head
    byte    8,31    'SpriteNumber:31 Kong head
    byte    8,31    'SpriteNumber:32 Kong head
    byte    8,31    'SpriteNumber:33 Kong head
    byte    8,15    'SpriteNumber:34 Kong body
    byte    8,15    'SpriteNumber:35 Kong body
    byte    8,15    'SpriteNumber:36 Kong body
    byte    8,31    'SpriteNumber:37 Kong body
    byte    8,27    'SpriteNumber:38 Kong body
    byte    8,31    'SpriteNumber:39 Kong body

    byte    8,17    'SpriteNumber:40 Kong body - Arm holding barrel
    byte    8,15    'SpriteNumber:41 Kong body
    byte    8,15    'SpriteNumber:42 Kong body
    byte    8,15    'SpriteNumber:43 Kong body
    byte    8,31    'SpriteNumber:44 Kong body
    byte    8,31    'SpriteNumber:45 Kong head
    byte    8,31    'SpriteNumber:46
    byte    8,31    'SpriteNumber:47
    byte    8,31    'SpriteNumber:48
    byte    8,35    'SpriteNumber:49 Kong back left arm straight

    byte    8,35    'SpriteNumber:50 Kong back right arm bent
    byte    8,15    'SpriteNumber:51 Kong back view left leg
    byte    8,15    'SpriteNumber:52 Kong back view right leg
    byte    8,15    'SpriteNumber:53 Kong's stars
    byte    8,12    'SpriteNumber:54
    byte    8,15    'SpriteNumber:55 Spring open
    byte    8,15    'SpriteNumber:56 Spring closed
    byte    8,15    'SpriteNumber:57
    byte    8,15    'SpriteNumber:58
    byte    8,15    'SpriteNumber:59 Oil drum flame

    byte    8,15    'SpriteNumber:60 Oil drum flame
    byte    8,15    'SpriteNumber:61 Oil drum flame
    byte    8,15    'SpriteNumber:62 Oil drum flame
    byte    8,3     'SpriteNumber:63 Lift
    byte    8,15    'SpriteNumber:64 Winch
    byte    8,15    'SpriteNumber:65 Ladder
    byte    8,15    'SpriteNumber:66 Oil drum
    byte    8,11    'SpriteNumber:67 Pie
    byte    8,12    'SpriteNumber:68
    byte    8,12    'SpriteNumber:69

    byte    8,12    'SpriteNumber:70
    byte    0,0     'SpriteNumber:71 Motor
    byte    0,0     'SpriteNumber:72 Motor
    byte    0,0     'SpriteNumber:73 Motor
    byte    8,12    'SpriteNumber:74
    byte    8,12    'SpriteNumber:75
    byte    8,12    'SpriteNumber:76
    byte    8,12    'SpriteNumber:77
    byte    0,0     'SpriteNumber:78
    byte    8,15    'SpriteNumber:79 Umbrella

    byte    8,15    'SpriteNumber:80 Hat
    byte    8,15    'SpriteNumber:81 Handbag
    byte    8,31    'SpriteNumber:82 Heart
    byte    8,31    'SpriteNumber:83 Broken Heart
    byte    8,15    'SpriteNumber:84
    byte    8,15    'SpriteNumber:85
    byte    8,15    'SpriteNumber:86
    byte    8,12    'SpriteNumber:87 Bonus
{
    byte    8,12    'SpriteNumber:88 Bonus
    byte    8,12    'SpriteNumber:89 Bonus

    byte    8,12    'SpriteNumber:90 Bonus
    byte    8,12    'SpriteNumber:91 Bonus
}
                    