'///////////////////////////////////////////////////////////////////////
' 
'' Hydra Dense Music Format (HDMF) Lite Custom DK Driver 
'' AUTHOR: Eric Moyer
'' LAST MODIFIED: 05.26.07
'' VERSION 1.0
''
'' For the latest version search the parallax forums at:
''    http://forums.parallax.com 
''                                                 
''
'' //////////////////////////////////////////////////////////////////////
''
''  Revision History
''
''  Rev  Date      Description
''  ---  --------  ---------------------------------------------------
''  1.0  05-30-07  Customize for DK
''                 Add a second playback engine
''                 Add a pass through PlaySoundFM call
''  1.1  05-31-07  Remove second playback engine
''                 Add SFX engine
''  1.3  06-02-07  Add SFX_stop_walk / SFX_start_walk calls
''  1.3B 06-02-07  Define SHAPE_SINE (for export)
''
'' //////////////////////////////////////////////////////////////////////  
''
''  Open issues:
''     Known bug in cnt math can cause music playback to stop when cnt wraps.
''
''  Documentation:
''  ==============
''  Please refer to the 'HDMF User's Manual' for extensive documentation on this
''  driver, the creation of HDMF assets (i.e. music data), and operation of the
''  'HDMF Converter' Windows application which can be used to covert MIDI data into
''  HDMF assets.      
''        
''  NOTE: This is a single cog driver designed for use in games with simple music
''        requirements where only a single playback cog is available.  If  more
''        than 1 cog is available please use the standard HDMF Driver which has
''        far superior playback capabilities.
''
'' //////////////////////////////////////////////////////////////////////  

'///////////////////////////////////////////////////////////////////////
' CONSTANTS SECTION ////////////////////////////////////////////////////
'///////////////////////////////////////////////////////////////////////

CON
  
  HDMF_TIMEBASE_TICK     = 80_000_000 / 100         ' HDMF uses a 10 mesc tick (100 ticks per second).
                                                    ' HDMF_TICK is expressed in system clock ticks
                                                    '     so at 80MHz is 80,000,000 / 100
                                                       
  HDMF_DURATION_TICK     = snd#SAMPLE_RATE / 100    ' HDMF duration is also 100 ticks per second
                                                    ' DURATION_TICK is expressed in sound driver sample
                                                    '     rate ticks so is SAMPLE_RATE / 100
                                                       
  'Player states   
  HDMF__STATE_IDLE        = %00000000  
  HDMF__STATE_START_SONG  = %00000001
  HDMF__STATE_PLAY_FETCH  = %00000100
  HDMF__STATE_PLAY_WAIT   = %00000101
  HDMF__CHECK_IS_PLAYING  = %00000100

  'Player flags
  HDMF__FLAG_LAST_NOTE_5_BYTES = %10000000  'Internal flag
  HDMF__FLAG_FIRST_NOTE        = %01000000  'Internal flag
  HDMF__FLAG_LOOP_SONG         = %00000001  'User flag
  
  HDMF__USER_FLAG_MASK         = %00000001  


  HDMF_MAX_INSTRUMENTS    = 8
  HDMF_BYTES_PER_NOTE_MAX = 6


  'Sound Effects ====================================================================================
  'SFX States
  SFX_STATE__IDLE         = 0
  SFX_STATE__JUMP_BARREL  = 1
  SFX_STATE__WALK         = 2
  SFX_STATE__WALK_STOP    = 3
  SFX_STATE__SMASH_BARREL = 4
  SFX_STATE__JUMP         = 5
  SFX_STATE__DK_FALL      = 6

  SFX_DK_FALL_START_FREQ      = $0749
  SFX_DK_FALL_END_FREQ        = $0100

  SFX_WALK_START_FREQ         = $0700
  
  SFX_JUMP_BARREL_NOTE_DELAY  = 5

  SFX_JUMP_FREQUENCY_LOW      = $0380
  SFX_JUMP_FREQUENCY_HIGH     = $04a0
  SFX_JUMP_OSCILLATION_STEP   = 110

  SFX_SMASH_FREQUENCY_HIGH    = $0700
  SFX_SMASH_FREQUENCY_LOW     = $0200

  '==================================================================================================     
  
  'For now just pass through the constants that DK needs
  SHAPE_NOISE  = snd#SHAPE_NOISE
  SHAPE_SQUARE = snd#SHAPE_SQUARE
  SHAPE_SINE   = snd#SHAPE_SINE    
  SAMPLE_RATE  = snd#SAMPLE_RATE
  NOTE_C5 = snd#NOTE_C5
  NOTE_Db2 = snd#NOTE_Db2
  NOTE_Fs5 = snd#NOTE_Fs5
  NOTE_E6 = snd#NOTE_E6
  NOTE_Ab5 = snd#NOTE_Ab5

  'Songs played on the HDMF2 engine will have this offset added to their channel number
  HDMF2_CHANNEL_OFFSET = 3
OBJ

  snd    : "NS_sound_drv_052_22khz_16bit_stereo.spin"   'Sound driver

VAR

  long hdmf1_state
  long hdmf1_i
  long hdmf1_song_ptr
  long hdmf1_song_start_ptr
  long hdmf1_note_start_ptr
  byte hdmf1_flags
  long hdmf1_current_note_time
  long hdmf1_note_delta_time
  byte hdmf1_note_data[HDMF_BYTES_PER_NOTE_MAX]
  byte hdmf1_instrument_wave[HDMF_MAX_INSTRUMENTS]
  long hdmf1_instrument_envelope[HDMF_MAX_INSTRUMENTS] 

  long hdmf1_P_freq                                  
  long hdmf1_P_duration
  long hdmf1_P_channel
  long hdmf1_P_volume
  long hdmf1_P_time
  long hdmf1_P_instrument

  byte hdmf1_sfx_state
  word hdmf1_sfx_counter
  long hdmf1_sfx_frequency
  byte hdmf1_sfx_volume
  byte hdmf1_sfx_ascend
  byte hdmf1_sfx_frequency_offset
  byte hdmf1_sfx_walking
  byte hdmf1_sfx_done

'  long debug_data_ptr
  long note_number 
'  long debug_data[7]

  long random_var                                         ' global random variable



PUB SFX_start(sfx_state_arg)
  ' initialize random variable
  random_var := cnt*171732
  random_var := ?random_var
  
  hdmf1_sfx_state   := sfx_state_arg
  hdmf1_sfx_counter := 0
  

PUB SFX_stop_walk
  ' Stop walking sound gracefully, between chirps
  hdmf1_sfx_walking := false  

PUB SFX_start_walk
  ' Stop walking sound gracefully, between chirps
  hdmf1_sfx_state := SFX_STATE__WALK
  hdmf1_sfx_counter := 0   
  hdmf1_sfx_walking := true
    
PUB SFX_get_state
  return hdmf1_sfx_state
  
PUB SFX_do_playback

  ++hdmf1_sfx_counter
  hdmf1_sfx_done := false

  '/////////////////////////////////////////////////////////////////////////////////////// 
  case hdmf1_sfx_state
    '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++   
    'SFX_STATE__IDLE:
      'Do nothing

    '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++     
    SFX_STATE__JUMP_BARREL:
      hdmf1_sfx_frequency := 0
      case hdmf1_sfx_counter
        1:
          hdmf1_sfx_frequency := $01D2
        CONSTANT(SFX_JUMP_BARREL_NOTE_DELAY*1+1):
          hdmf1_sfx_frequency := $020B
        CONSTANT(SFX_JUMP_BARREL_NOTE_DELAY*2+1):
          hdmf1_sfx_frequency := $024B
        CONSTANT(SFX_JUMP_BARREL_NOTE_DELAY*3+1):
          hdmf1_sfx_frequency := $0310
        CONSTANT(SFX_JUMP_BARREL_NOTE_DELAY*5+1):
          hdmf1_sfx_frequency := $026E
        CONSTANT(SFX_JUMP_BARREL_NOTE_DELAY*6+1):
          hdmf1_sfx_done := true
      if hdmf1_sfx_frequency
         snd.PlaySoundFM(5, snd#SHAPE_SAWTOOTH, hdmf1_sfx_frequency, CONSTANT( snd#SAMPLE_RATE / 2 ), 120, $3579_ADEF )   
         
    '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  
    SFX_STATE__WALK:
      'Init sound      
      if hdmf1_sfx_counter == 1
         'Start at normal frequency plus some pseudo random variation
         'EPM: Optimized random number call for code space (converted function call into inline math)
         '     OLD code: hdmf1_sfx_frequency  := SFX_WALK_START_FREQ + Rand_Range(-50,50)
         hdmf1_sfx_frequency  := CONSTANT(SFX_WALK_START_FREQ + (-50)) + ((?random_var & $7FFFFFFF) // CONSTANT(50 - (-50) + 1))
  
      'Drop frequency
      hdmf1_sfx_frequency -= 90   

      'Play, pause, then restart  
      if hdmf1_sfx_counter < 6
         snd.PlaySoundFM(5, snd#SHAPE_SINE, hdmf1_sfx_frequency, CONSTANT( snd#SAMPLE_RATE / 10 ), 30, $FFFF_FFFF )
         snd.PlaySoundFM(4, snd#SHAPE_SINE, hdmf1_sfx_frequency>>1, CONSTANT( snd#SAMPLE_RATE / 10 ), 20, $FFFF_FFFF )
      elseif hdmf1_sfx_counter == 15
         hdmf1_sfx_done := true  
     

    '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++   
    SFX_STATE__SMASH_BARREL:
      'Init sound
      if hdmf1_sfx_counter == 1 
        hdmf1_sfx_frequency := SFX_SMASH_FREQUENCY_LOW

      if hdmf1_sfx_counter & $03 == $03
        hdmf1_sfx_frequency := hdmf1_sfx_frequency * 107 / 100
        snd.PlaySoundFM(5, snd#SHAPE_SINE, hdmf1_sfx_frequency, CONSTANT( snd#SAMPLE_RATE / 10 ), 30, $FFFF_FFFF )
      if hdmf1_sfx_counter & $0F == $0F
        hdmf1_sfx_frequency := hdmf1_sfx_frequency * 91 / 100
      if hdmf1_sfx_frequency > SFX_SMASH_FREQUENCY_HIGH
        hdmf1_sfx_done := true    

    '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++   
    SFX_STATE__JUMP:
      'Init sound
      if hdmf1_sfx_counter == 1 
        hdmf1_sfx_volume    := 127 'Must start odd, so ends on 1 after stepping by -2
        hdmf1_sfx_frequency := SFX_JUMP_FREQUENCY_LOW
        hdmf1_sfx_ascend    := true
        hdmf1_sfx_frequency_offset := 0

      'Vary frequency
      if hdmf1_sfx_ascend 
        if( hdmf1_sfx_frequency += SFX_JUMP_OSCILLATION_STEP) > SFX_JUMP_FREQUENCY_HIGH
          hdmf1_sfx_ascend := false
      else
        if( hdmf1_sfx_frequency -= SFX_JUMP_OSCILLATION_STEP) < SFX_JUMP_FREQUENCY_LOW
          hdmf1_sfx_ascend := true

      'Decay volume
      hdmf1_sfx_volume -= 3
      hdmf1_sfx_frequency_offset += 2
      snd.PlaySoundFM(5, snd#SHAPE_SINE, hdmf1_sfx_frequency + hdmf1_sfx_frequency_offset, CONSTANT( snd#SAMPLE_RATE / 10 ), hdmf1_sfx_volume, $FFFF_FFFF )  

      'End when volume dies out
      if hdmf1_sfx_volume =< 3
        hdmf1_sfx_done := true     

    '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++   
    SFX_STATE__DK_FALL:
      'Init sound
      if hdmf1_sfx_counter == 1
         hdmf1_sfx_frequency  := SFX_DK_FALL_START_FREQ
      snd.PlaySoundFM(5, snd#SHAPE_SAWTOOTH, hdmf1_sfx_frequency, CONSTANT( snd#SAMPLE_RATE / 10 ), 128, $FFFF_FFFF )
      
      'Drop frequency               
      hdmf1_sfx_frequency := (hdmf1_sfx_frequency * 98)/100
      
      'Play until end frequency reached
      if hdmf1_sfx_frequency  =<  SFX_DK_FALL_END_FREQ
        hdmf1_sfx_done := true  

  '///////////////////////////////////////////////////////////////////////////////////////        

  'If the currently playing sound is finished
  if hdmf1_sfx_done
    if hdmf1_sfx_walking
       'Still walking, start walking sound
       hdmf1_sfx_state   := SFX_STATE__WALK 
       hdmf1_sfx_counter := 0
    else
       'No longer walking, go to idle
       hdmf1_sfx_state :=  SFX_STATE__IDLE            
  
'PUB Rand_Range(rstart, rend) : r_delta
'  ' returns a random number from [rstart to rend] inclusive
'  r_delta := rend - rstart + 1
'  result := rstart + ((?random_var & $7FFFFFFF) // r_delta)
'   
'  return result
PUB start(arg_debug_data_ptr)
'' Starts the driver
''
  snd.start(10)
  hdmf1_state       := HDMF__STATE_IDLE
  hdmf1_sfx_walking := false
  hdmf1_sfx_counter := 0
  hdmf1_sfx_state   := SFX_STATE__IDLE   
'  debug_data_ptr := arg_debug_data_ptr
'  DIRA[0]        := 1 'Enable debug LED as output

  'Play startup ping
  snd.PlaySoundFM(0, snd#SHAPE_SINE, 600, CONSTANT( snd#SAMPLE_RATE * 2 ), 255, $3579_ADEF )
  
PUB stop
'' Stops the driver. Frees all used cogs.
''
  snd.stop
  
PUB play_song(song_address, user_flags)
'' Starts song playback.
''
''   song_address:    The starting address of the song data (in RAM).
''                    EEPROM playback is not supported in the Lite deiver.  If EEPROM
''                    playback is needed use the standard HDMF driver.
''
''   user_flags:      Playbak flags.  Multiple flags can be OR'd together.
''                    Supported flags:
''                       HDMF__FLAG_LOOP_SONG   Loop the song forever. ( Use stop_song() to stop it )
''
  hdmf1_state := HDMF__STATE_START_SONG
  hdmf1_song_start_ptr := song_address
  hdmf1_flags := (user_flags & HDMF__USER_FLAG_MASK) 'Set new user flags
          
PUB stop_song
'' Stops a song in progress.                   
''
  hdmf1_state := HDMF__STATE_IDLE
  
PUB song_is_playing
'' Returns 'true' if a song is currently playing.
'' Returns 'false' otherwise.
''
  if hdmf1_state & HDMF__CHECK_IS_PLAYING
    return (true)
  else
    return (false)
    
PUB do_playback 
'' Processes music playack.
''
'' This function should be called AT LEAST once in your main game loop.  For improved
'' playback performace call do_playback multiple times (as evenly distributed in time
'' as possible) throughout your main code loop.
''
'' NOTE: If you dont call do_playback() you'll never hear anything.
''
    
  'START SONG >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  if hdmf1_state == HDMF__STATE_START_SONG
   
    'Init flags
    hdmf1_flags &= !(HDMF__FLAG_LAST_NOTE_5_BYTES)
    hdmf1_flags |= HDMF__FLAG_FIRST_NOTE
    note_number := 0
    
    'Read instruments
    hdmf1_song_ptr := hdmf1_song_start_ptr
    hdmf1_i := 0
    repeat while (hdmf1_i =< HDMF_MAX_INSTRUMENTS - 1) and (byte[hdmf1_song_ptr] <> $ff) 
      hdmf1_instrument_wave[hdmf1_i] := byte[hdmf1_song_ptr]
      ++hdmf1_song_ptr
      hdmf1_instrument_envelope[hdmf1_i] := byte[hdmf1_song_ptr] | byte[hdmf1_song_ptr+1] << 8 | byte[hdmf1_song_ptr+2] << 16 | byte[hdmf1_song_ptr+3] << 24
      hdmf1_song_ptr += 4
      'Increment instrument index
      ++hdmf1_i      
      
    'skip over 'end of instruments' 0xff
    ++hdmf1_song_ptr
    
    hdmf1_state := HDMF__STATE_PLAY_FETCH
    hdmf1_note_start_ptr := hdmf1_song_ptr
   
  'FETCH NOTE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
  elseif hdmf1_state == HDMF__STATE_PLAY_FETCH
    'Fetch song data from RAM
    if hdmf1_flags & HDMF__FLAG_LAST_NOTE_5_BYTES
      'One byte of note data already in buffer.  Read 5 more.
      repeat hdmf1_i from 1 to 5
        hdmf1_note_data[hdmf1_i] := byte[hdmf1_song_ptr]
        hdmf1_song_ptr++
    else
      'Read 6 new bytes of note data
      repeat hdmf1_i from 0 to 5
        hdmf1_note_data[hdmf1_i] := byte[hdmf1_song_ptr]  
        hdmf1_song_ptr++

'    if (debug_data_ptr) and (note_number == 0)
'      long[debug_data_ptr+constant(7*4)] := hdmf1_song_ptr
      
    hdmf1_note_delta_time := hdmf1_note_data[0] * HDMF_TIMEBASE_TICK  
   
    'Init the note timebase on the first note played
    if hdmf1_flags & HDMF__FLAG_FIRST_NOTE
      hdmf1_current_note_time := cnt
      hdmf1_flags &= !HDMF__FLAG_FIRST_NOTE
     
    'If not end of song
    if (hdmf1_note_data[0] <> $ff)
      
      hdmf1_P_time := hdmf1_current_note_time + hdmf1_note_delta_time
      hdmf1_P_channel := (hdmf1_note_data[1] & $e0) >> 5
      hdmf1_P_freq := (((hdmf1_note_data[1] & $1f) << 8) | hdmf1_note_data[2] ) << 1
      hdmf1_P_volume := hdmf1_note_data[3] << 3
      hdmf1_P_instrument := (hdmf1_note_data[3] & $e0) >> 5
   
      if hdmf1_note_data[4] & $80
        hdmf1_P_duration := ((hdmf1_note_data[4] & $7f) << 8 | hdmf1_note_data[5]) * HDMF_DURATION_TICK
        hdmf1_flags &= !HDMF__FLAG_LAST_NOTE_5_BYTES
      else
        hdmf1_P_duration := hdmf1_note_data[4] * HDMF_DURATION_TICK
        'Note was only 5 bytes long, so save the 6th byte for the next note
        hdmf1_note_data[0] := hdmf1_note_data[5]
        hdmf1_flags |= HDMF__FLAG_LAST_NOTE_5_BYTES
   
      'Enter the wait state (note will play when its scheduled time arrives)
      hdmf1_state := HDMF__STATE_PLAY_WAIT
    else
      'Song over.
      if hdmf1_flags & HDMF__FLAG_LOOP_SONG
        'Looping.  Restart song from beginning.
        hdmf1_song_ptr := hdmf1_note_start_ptr
        hdmf1_state := HDMF__STATE_PLAY_FETCH
        hdmf1_flags &= !HDMF__FLAG_LAST_NOTE_5_BYTES
        hdmf1_flags |= HDMF__FLAG_FIRST_NOTE   
      else
        'Done. Enter idle state.
        hdmf1_state := HDMF__STATE_IDLE
   
  'WAIT NOTE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  elseif hdmf1_state == HDMF__STATE_PLAY_WAIT    
    'If not waiting to cross end of counter wrap
    if not (((hdmf1_current_note_time>>1) > $40000000) and ((hdmf1_P_time>>1) < $40000000) and ((cnt>>1) > $40000000))
'      OUTA [0] := !INA[0] 'Toggle debug LED 
      'If note's scheduled play time is reached
      if ((cnt>>1) => (hdmf1_P_time>>1))
        snd.PlaySoundFM(hdmf1_P_channel, hdmf1_instrument_wave[hdmf1_P_instrument], hdmf1_P_freq, hdmf1_P_duration, hdmf1_P_volume, hdmf1_instrument_envelope[hdmf1_P_instrument] )
        hdmf1_current_note_time := hdmf1_P_time
        'Enter the fetch state to fetch the next note
        hdmf1_state := HDMF__STATE_PLAY_FETCH
   
{
        if (debug_data_ptr) and (note_number == 0)
          'Debug enabled.  Dump debug data.
          long[debug_data_ptr+constant(0*4)] := hdmf1_note_data[0] << 24 | hdmf1_note_data[1] << 16 | hdmf1_note_data[2] << 8 | hdmf1_note_data[3]
          long[debug_data_ptr+constant(1*4)] := hdmf1_note_data[4] << 24 | hdmf1_note_data[5] << 16
          long[debug_data_ptr+constant(2*4)] := hdmf1_P_time
          long[debug_data_ptr+constant(3*4)] := hdmf1_P_channel
          long[debug_data_ptr+constant(4*4)] := hdmf1_P_freq
          long[debug_data_ptr+constant(5*4)] := hdmf1_P_volume
          long[debug_data_ptr+constant(6*4)] := hdmf1_P_duration
}
        ++note_number

PUB PlaySoundFM(arg_channel, arg_shape, arg_freq, arg_duration, arg_volume, arg_amp_env)
'' This is a pass-thru.  The parameters are passed unchanged to the snd driver.
''
  snd.PlaySoundFM(arg_channel, arg_shape, arg_freq, arg_duration, arg_volume, arg_amp_env)