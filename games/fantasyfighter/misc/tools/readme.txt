' ------------------------------------------------------------------------
' Fantasy Fighter Data Builder Utilities - v0.1
' ------------------------------------------------------------------------

In order to edit the .gbr (font/gfx) and .gbm (map) resources you will need to unzip the gbmb18 and gbtd22 applications in the /gfx folder.

The game_data.txt file is what the FFDataBuilder utility uses to 'build' the data files.  See the file for documentation on how to use the commands.

The FFDataBuilder application uses components from the Graphics Utilities source, released at the following location:

http://forums.parallax.com/forums/default.aspx?f=33&m=377666

If you experience problems, please check this thread to ensure that you have the latest source/build.

--trodoss 