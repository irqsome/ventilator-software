{{ TITLE: TBD

 DESCRIPTION:
  Tile based adventure. In the spirit of "The Legend of Zelda".
  

 VERSION: 0.9
 AUTHOR: Jesse Druehl
 LAST MODIFIED: 09/09/09
 COMMENTS:

   PC = Player Character
   See end of file for terms of use license.

   Currently uses HEL graphics engine 4.0

 TODO:

   • Write custom multi-cog graphics driver, need more sprites and more colors. Need suitable text display.
     Improve graphics.
     
   • Improve datastructure usage, use encode and decode. Consider alignment.
     Use external data file format for game data.

   • Add multiple weapon support including projectiles.     
   • Develop game quest and storyline.
   • Find an NES gamepad and support it.
   • Add basic synth sound.
   • (Maybe) add SD card support for more game data.
   

 CHANGES:

   v0.9 9/09/09
           • Just for the heck of it, skipped 0.8. version 0.9 on 9/9/09
           • Re-formatted some code portions.
           • Added and edited comments to reflect code usage.
           • Fixed a bug with debug mode locking the game up.
           • Included version of HEL GFX 4 (no changes yet).

   v0.7 8/27/09
           • Removed item tiles from base tilemap, items are now loaded dynamically like monsters from DAT section.
           • Item data includes palette.
           • Item data is saved on screen load so items do not respawn.
           • Added item modifier field, for example: Gems use modifier field for the value of the gem.
           • Added +1 sword                                                                                                            Secrets... will be slightly hidden in final.
  
   v0.6 8/26/09
           • Re-factored Update_Monster method to behave like Update_Player.
           • Improved monster AI.
           • Added PC animations.
           • Added damage from monsters.
           • Added sword sprites and animation.
           • Added damage from PC to monsters.
           • Monsters are loaded dynamically for the next screen.
           • Added +1 max HP item.
  
   v0.5 8/20/09
           • Dynamic loading of screen offsets.
           • PC movement between screens is controlled by walking off screen (limited by walls)
              and lookup into screen_data for next screen number.
              Screens numbered 0..n from left to right, top to bottom.
           • Added item pickup. Added health and gem items. 
  
   v0.4 8/16/09 • Large tilemap support, currently 64x80 tiles divided into 30 10x12 screens
   v0.3 8/16/09 • Basic monster AI, universal Check_Move method.
   v0.2 8/15/09 • Added monster datastructure and load screen state.
   v0.1 8/14/09 • Added tile based collision detection

 CONTROLS:

   ▶Keyboard only◀
   
   ↑   
  ← →  Move player character                                                   
   ↓

 <CTRL> or <SPACE>  -  Attack
  <I>               -  Pause game and display status/inventory
  
}} 
CON

  _clkmode = xtal1 + pll16x       ' enable external clock range 5-10MHz and pll times 16
  _xinfreq = 5_000_000
  _stack   = 128                  ' accomodate display memory and stack

  DEBUG = false

  ' Player startup stats
  PC_START_HP     = 3
  PC_START_MAX_HP = 3
  PC_START_AP     = 1
  PC_START_TX     = 1
  PC_START_TY     = 1
  PC_START_INV    = %00000000
  INITIAL_SCREEN  = 0
  
  
  ' control key constants for keyboard interface
  KB_LEFT_ARROW  = $C0
  KB_RIGHT_ARROW = $C1
  KB_UP_ARROW    = $C2
  KB_DOWN_ARROW  = $C3
  KB_ESC         = $CB
  KB_SPACE       = $20
  KB_ENTER       = $0D
  KB_LEFT_CTRL   = $F2
  KB_RIGHT_CTRL  = $F3
  KB_I           = $69
   
  'encode each universal input id as a bit
  IID_NULL  =      $00 ' null input
  IID_START =      $01
  IID_ESC   =      $02
  IID_FIRE  =      $04
  IID_RIGHT =      $08
  IID_LEFT  =      $10
  IID_UP    =      $20
  IID_DOWN  =      $40
  IID_I     =      $80               

  #0
  DIR_UP
  DIR_DOWN
  DIR_LEFT
  DIR_RIGHT

  MAX_NUM_MONSTERS = 7
  MONSTER_SIZE     = 9

  'Indexes into monster datarecord
  #0
  MON_TX
  MON_TY
  MON_X
  MON_Y
  MON_DIR
  MON_TYPE
  MON_HP
  MON_FRAME
  MON_STATUS
 
  #0
  MON_TYPE_BAT       'Bouncy bat
  MON_TYPE_BATR      'Random bouncy bat
  MON_TYPE_BATRT     'Random bat
  MON_TYPE_WASP      'Hunter wasp
  
  'MON_STATUS bits for masking
  MON_SID_SHOW   = $01 'Alive/displayed
  MON_SID_HIT    = $02 'Currently hit 
  MON_SID_MOVING = $04 'Moving (for inter-tile motion)
  MON_SID_RES3   = $08 'Reserved
  MON_SID_RES4   = $10
  MON_SID_RES5   = $20
  MON_SID_RES6   = $40
  MON_SID_RES7   = $80

  'PC inventory flags
  INV_ID_SWORD_1 = $01
  INV_ID_RES1    = $02
  INV_ID_RES2    = $04
  INV_ID_RES3    = $08
  INV_ID_RES4    = $10
  INV_ID_RES5    = $20
  INV_ID_RES6    = $40
  INV_ID_RES7    = $80
  

  TILEMAP_WIDTH = 64                'UPDATE THESE WHEN TILEMAP SIZE CHANGES!!!!
  TILEMAP_HEIGHT = 80
  
  'TILEMAP_BOUNDARY = 0              'Tile indices less than or equal to this block movement of PC and monsters.

  SCREEN_DATA_SIZE = 4               'Size of screen data record.
  SCREEN_TILE_WIDTH = 11             'Width of screen in tiles including seperator tiles.
  SCREEN_TILE_HEIGHT = 13            'Height of screen in tiles including seperator tiles.
  SCREENS_ACROSS = 5                 'Number of screens in a row of tile map.

  'Tile and Item numbers (tile index)
  {
  TILE_BLANK      = 0
  TILE_BLOCK1     = 1
  TILE_BLOCK2     = 2
  TILE_FIRE       = 3
  ITEM_HEALTH     = 4
  ITEM_GEM        = 5
  'ITEM_GEM3       = 6
  ITEM_SWORD_1    = 7
  ITEM_HPMAX_ADD  = 8
 }
  #0
  TILE_BLANK     
  TILE_BLOCK1    
  TILE_BLOCK2    
  TILE_FIRE      
  ITEM_HEALTH    
  ITEM_GEM       
  ITEM_GEM3     
  ITEM_SWORD_1   
  ITEM_HPMAX_ADD 
  TILE_NUMBERS              'Index of number 0 (start of numbers) FIX THIS! make it independent.

  'Items
  MAX_NUM_ITEMS = 25
  ITEM_SIZE     = 6
  
  'Indices into items datarecord
  #0
  ITEM_PALETTE
  ITEM_TILENUM
  ITEM_TX
  ITEM_TY
  ITEM_MODIFIER
  ITEM_FLAGS
  
  STATUS_WIDTH     = 6
  STATUS_HEIGHT    = 6

  STATUS_X         = 2
  STATUS_Y         = 2

  PLAYER_ATTACK_CYCLES          = 3
  ATTACK_LOCK_CYCLES            = 4
  
  PLAYER_HIT_CYCLES             = 75
  MONSTER_HIT_CYCLES            = 4  

VAR

' tile engine data structure pointers (can be changed in real-time by app!)
long tile_map_base_ptr_parm       ' base address of the tile map
long tile_bitmaps_base_ptr_parm   ' base address of the tile bitmaps
long tile_palettes_base_ptr_parm  ' base address of the palettes

long tile_map_sprite_cntrl_parm   ' pointer to the value that holds various "control" values for the tile map/sprite engine
                                  ' currently, encodes width of map, and number of sprites to process up to 8 in following format
                                  ' $xx_xx_ss_ww, xx is don't care/unused
                                  ' ss = number of sprites to process 1..8
                                  ' ww = the number of "screens" or multiples of 16 that the tile map is
                                  ' eg. 0 would be 16 wide (standard), 1 would be 32 tiles, 2 would be 64 tiles, etc.
                                  ' this allows multiscreen width playfields and thus large horizontal/vertical scrolling games
                                  ' note that the final size is always a power of 2 multiple of 16

long tile_sprite_tbl_base_ptr_parm ' base address of sprite table


' real-time engine status variables, these are updated in real time by the
' tile engine itself, so they can be monitored outside in SPIN/ASM by game
long tile_status_bits_parm      ' vsync, hsync, etc.

' format of tile_status_bits_parm, only the Vsync status bit is updated
'
' byte 3 (unused)|byte 2 (line)|   byte 1 (tile postion)    |                     byte 0 (sync and region)      |
'|x x x x x x x x| line 8-bits | row 4 bits | column 4-bits |x x x x | region 2-bits | hsync 1-bit | vsync 1-bit|
'   b31..b24         b23..b16      b15..b12     b11..b8                    b3..b2          b1            b0
' Region 0=Top Overscan, 1=Active Video, 2=Bottom Overscan, 3=Vsync
' NOTE: In this version of the tile engine only VSYNC and REGION are valid 


'Game vars
long game_state                                         ' Main state engine, well, state
long pitac, moved, dice, player_hit, player_hit_c       ' Inter-Tile Animation Counter for PC and various PC related flags and counters
long pc_attacking, pc_att_ctr, att_lock_ctr
long player_x, player_y, player_tx, player_ty, player_dir   ' Position of player in tilespace and direction
long player_hp, player_max_hp, player_ap                ' Hit points and max hit points
long player_gems                                        ' Money
long screen, screen_num                                 ' Screen number
long scr_up, scr_down, scr_left, scr_right
long screen_offset                                      ' Offset into tilemap for current screen
long mon_count                                          ' Number of monsters in screen
long curr_count                                         ' saves counter
long kb_keys                                            ' Keyboard state
word status_buf[STATUS_WIDTH * STATUS_HEIGHT]           ' Saves screen tiles when displaying status screen. 
byte disp_status                                        ' Is status screen displayed?

byte monsters[MAX_NUM_MONSTERS * MONSTER_SIZE]          ' Monster Data-structure
                                                        ' FIX THIS! need to keep track of monsters across screens
long monsters_dirty                                     ' Flag indicating monster states have changed, save back to datarecord.
long monster_hit_c                                      ' Lock-out counter for attacking monsters (cannot hit monster again within this number of cycles)
                                                        ' FIX THIS add to monster datarecord?

long item_count                                         ' Number of items in screen
byte items[MAX_NUM_ITEMS * ITEM_SIZE]                   ' Items datastructure
byte item_mod_value                                     ' Item modifier value (I.E. gem value, attack bonus for magic weapons)
long items_dirty                                        ' Flag indicating item states have changed, save back to datarecord.

long seq[3]                                             ' Array for game sequences
long seqi, seql                                         ' Game sequence control
byte inventory                                          ' Inventory bitmap FIX THIS! should be an array with pointers maybe, quantities.

OBJ

gfx:            "JD_HEL_GFX_ENGINE_040.SPIN"
kb:             "Keyboard.spin"
term:           "Parallax Serial Terminal.spin"

PUB Main 

  'Keyboard setup FIX THIS add constants for pins and typematic stuff. Use comboKeyboard and add event notifiers.
  kb.startx(8, 9, %0_000_100, %00_01000)
  
  ' set up tile and sprite engine data before starting it, so no ugly startup
  ' points ptrs to actual memory storage for tile engine
  ' point everything to the "tile_data" object's data which are retrieved by "getter" functions
  tile_map_base_ptr_parm        := @_tile_maps   
  tile_bitmaps_base_ptr_parm    := @_tile_bitmaps
  tile_palettes_base_ptr_parm   := @_tile_palette_map
   
  ' these control the sprite engine
  tile_map_sprite_cntrl_parm    := $00_00_00_02
  tile_sprite_tbl_base_ptr_parm := @sprite_tbl[0]
  tile_status_bits_parm         := 0
   
  ' launch a COG with ASM video driver
  gfx.start(@tile_map_base_ptr_parm)
  
  'Start debug serial interface
  if(DEBUG)
    term.start(115_200)

  'INIT State 
  game_state := GAME_INIT
  
  'Here we go! 
  Game

CON
'Game state machine states
  #0
  GAME_INIT
  GAME_TEST
  GAME_TITLE
  GAME_LOAD_SCREEN
  GAME_SCREEN
  GAME_DYING
  GAME_DEAD
  GAME_RESTART
  GAME_FINISH
  
PUB Game | i, ii, mon_data_ptr, item_data_ptr, tile_index, first_load

  ''Main state machine
  repeat while true

    'Read keyboard
    kb_keys := Read_keyboard(TRUE)
  
    case game_state
      'INIT game, load important stuff, FIX THIS add title state
      GAME_INIT:

        first_load := TRUE
        pitac := 0
        player_hit := FALSE
        moved := FALSE
        pc_attacking := FALSE
        att_lock_ctr := 0
        
        player_tx := PC_START_TX
        player_ty := PC_START_TY
        player_dir := DIR_RIGHT
        player_max_hp := PC_START_MAX_HP
        player_hp := PC_START_HP
        player_ap := PC_START_AP
        player_gems := 0
        inventory := PC_START_INV
        Equip
        items_dirty    := FALSE
        monsters_dirty := FALSE
        monster_hit_c := MONSTER_HIT_CYCLES

        disp_status := FALSE

        dice := cnt + 78263472
        ?dice

        screen_num := INITIAL_SCREEN
               
        game_state := GAME_LOAD_SCREEN


      GAME_LOAD_SCREEN:
      
        'Save item data
        if(NOT first_load AND items_dirty AND item_count > 0)
          repeat i from 0 to item_count-1
            'Save item values (index+1 to skip past screen item count)
            byte[item_data_ptr][ITEM_PALETTE +1 +(i*ITEM_SIZE)]    := items[i*ITEM_SIZE+ITEM_PALETTE]           
            byte[item_data_ptr][ITEM_TILENUM +1 +(i*ITEM_SIZE)]    := items[i*ITEM_SIZE+ITEM_TILENUM] 
            byte[item_data_ptr][ITEM_TX +1 +(i*ITEM_SIZE)]         := items[i*ITEM_SIZE+ITEM_TX]      
            byte[item_data_ptr][ITEM_TY +1 +(i*ITEM_SIZE)]         := items[i*ITEM_SIZE+ITEM_TY]       
            byte[item_data_ptr][ITEM_MODIFIER +1 +(i*ITEM_SIZE)]   := items[i*ITEM_SIZE+ITEM_MODIFIER]
            byte[item_data_ptr][ITEM_FLAGS +1 +(i*ITEM_SIZE)]      := items[i*ITEM_SIZE+ITEM_FLAGS]   
           
{          BROKEN           
          'Save monster data
          if(monsters_dirty)
            if(mon_count > 0)
              repeat i from 0 to mon_count-1
                'Lookup values (index+1 to skip past screen monster count)
                'byte[mon_data_ptr][MON_TX +1 +(i*MONSTER_SIZE)]         :=  monsters[i*MONSTER_SIZE+MON_TX]    
                'byte[mon_data_ptr][MON_TY +1 +(i*MONSTER_SIZE)]         :=  monsters[i*MONSTER_SIZE+MON_TY]    
                'byte[mon_data_ptr][MON_DIR +1 +(i*MONSTER_SIZE)]        :=  monsters[i*MONSTER_SIZE+MON_DIR]   
                byte[mon_data_ptr][MON_TYPE +1 +(i*MONSTER_SIZE)]       :=  monsters[i*MONSTER_SIZE+MON_TYPE]  
                byte[mon_data_ptr][MON_HP +1 +(i*MONSTER_SIZE)]         :=  monsters[i*MONSTER_SIZE+MON_HP]    
                'byte[mon_data_ptr][MON_FRAME +1 +(i*MONSTER_SIZE)]      :=  monsters[i*MONSTER_SIZE+MON_FRAME] 
                byte[mon_data_ptr][MON_STATUS +1 +(i*MONSTER_SIZE)]     :=  monsters[i*MONSTER_SIZE+MON_STATUS]
                'byte[mon_data_ptr][MON_MOVED +1 +(i*MONSTER_SIZE)]      :=  monsters[i*MONSTER_SIZE+MON_MOVED] 
}
            
{        Load screen numbers which connect to current screen

                              scr_up
                                 ↑
                       scr_left ←‣→ scr_right
                                 ↓
                              scr_down
}
        first_load := FALSE
        
        scr_up                  := word[@screen_data][(SCREEN_DATA_SIZE * screen_num) + 0]
        scr_down                := word[@screen_data][(SCREEN_DATA_SIZE * screen_num) + 1]
        scr_left                := word[@screen_data][(SCREEN_DATA_SIZE * screen_num) + 2]
        scr_right               := word[@screen_data][(SCREEN_DATA_SIZE * screen_num) + 3]

        'Lookup offset into tilemap
        screen_offset           := Get_Screen_Offset(screen_num)

        'Load secret sequences. SHHH!
        load_seq
        
        'Load Screen
        tile_map_base_ptr_parm := @_tile_maps + screen_offset

        player_x := player_tx*16+16
        player_y := player_ty*16+16
        sprite_tbl[0] := (player_y << 24) + (player_x << 16) + (0 << 8) + ($01)     ' sprite 0 state: y=xx, x=$xx, z=$xx, enabled/disabled
        sprite_tbl[1] := @PC_down_0 ' sprite 0 bitmap ptr        
        

        'Load monsters
        monsters_dirty := FALSE 
        mon_data_ptr := @monster_data
        mon_count := byte[mon_data_ptr]

        'Skip to correct monster data for screen.
        ii := 0
        repeat while ii < screen_num
          mon_data_ptr += MONSTER_SIZE * mon_count + 1
          mon_count := byte[mon_data_ptr]
          ii++


        if(mon_count > 0)
          repeat i from 0 to mon_count-1
            'Lookup values (index+1 to skip past screen monster count)
            monsters[i*MONSTER_SIZE+MON_TX]               := byte[mon_data_ptr][MON_TX +1 +(i*MONSTER_SIZE)]
            monsters[i*MONSTER_SIZE+MON_TY]               := byte[mon_data_ptr][MON_TY +1 +(i*MONSTER_SIZE)]
            monsters[i*MONSTER_SIZE+MON_X]                := monsters[i*MONSTER_SIZE+MON_TX]*16+16
            monsters[i*MONSTER_SIZE+MON_Y]                := monsters[i*MONSTER_SIZE+MON_TY]*16+16          
            monsters[i*MONSTER_SIZE+MON_DIR]              := byte[mon_data_ptr][MON_DIR +1 +(i*MONSTER_SIZE)]
            monsters[i*MONSTER_SIZE+MON_TYPE]             := byte[mon_data_ptr][MON_TYPE +1 +(i*MONSTER_SIZE)]
            monsters[i*MONSTER_SIZE+MON_HP]               := byte[mon_data_ptr][MON_HP +1 +(i*MONSTER_SIZE)]
            monsters[i*MONSTER_SIZE+MON_FRAME]            := byte[mon_data_ptr][MON_FRAME +1 +(i*MONSTER_SIZE)]
            monsters[i*MONSTER_SIZE+MON_STATUS]           := byte[mon_data_ptr][MON_STATUS +1 +(i*MONSTER_SIZE)]
             
        'Update number of sprites in sprite control, +2 to always include PC and weapon sprite
        tile_map_sprite_cntrl_parm.byte[1] := mon_count + 2
        
'****************************ITEMS*****************************************************************************************

        'Gather item data
        items_dirty := FALSE 
        item_data_ptr := @item_data
        item_count := byte[item_data_ptr]

        'Skip to correct item data for screen.
        ii := 0
        repeat while ii < screen_num
          item_data_ptr += ITEM_SIZE * item_count + 1
          item_count := byte[item_data_ptr]
          ii++

        'Load items
        if(item_count > 0)
          repeat i from 0 to item_count-1
            'Lookup values (index+1 to skip past screen item count)
            items[i*ITEM_SIZE+ITEM_PALETTE]         := byte[item_data_ptr][ITEM_PALETTE +1 +(i*ITEM_SIZE)]
            items[i*ITEM_SIZE+ITEM_TILENUM]         := byte[item_data_ptr][ITEM_TILENUM +1 +(i*ITEM_SIZE)]
            items[i*ITEM_SIZE+ITEM_TX]              := byte[item_data_ptr][ITEM_TX +1 +(i*ITEM_SIZE)] 
            items[i*ITEM_SIZE+ITEM_TY]              := byte[item_data_ptr][ITEM_TY +1 +(i*ITEM_SIZE)]           
            items[i*ITEM_SIZE+ITEM_MODIFIER]        := byte[item_data_ptr][ITEM_MODIFIER +1 +(i*ITEM_SIZE)]
            items[i*ITEM_SIZE+ITEM_FLAGS]           := byte[item_data_ptr][ITEM_FLAGS +1 +(i*ITEM_SIZE)]

           'Draw item tile
            if(items[i*ITEM_SIZE+ITEM_FLAGS] == 1)
              word[tile_map_base_ptr_parm][ (items[i*ITEM_SIZE+ITEM_TX]) + ((items[i*ITEM_SIZE+ITEM_TY]) * TILEMAP_WIDTH)] := (items[i*ITEM_SIZE+ITEM_PALETTE] << 8) + items[i*ITEM_SIZE+ITEM_TILENUM]
            
'***************************END ITEMS**************************************************************************************
            
        game_state := GAME_SCREEN
        
                                                                        

      GAME_SCREEN:
          
        'Lock framerate FIX THIS, add independent delays or velocity deltas for PC, monsters and special stuff like projectiles. 
        waitcnt(cnt + 5*666_666)
        
        'FIX THIS, make pause/status state toggle  
        if(kb.keystate(KB_I) == FALSE)
          
          if(disp_status == 1)
            disp_status := 0
             Restore_Screen
            
          'Run Normally
          Update_Player

          'Update each monster for the current screen
          repeat i from 0 to mon_count-1
            Update_Monster(i)
            
        'Pause game and display status screen
        else
          if(disp_status == 0)
            disp_status := 1
            Draw_Status
        
PUB Update_Player | item_num
'Check input and Animate PC sprite between map tiles.

  'Handle player hit
  if(player_hit == TRUE)
    if(player_hit_c == 0)
      player_hp -= 1
    if(player_hp =< 0)
      game_state := GAME_INIT
      return
    Adventure_ti_palette_map.byte[3] := Adventure_ti_palette_map.byte[roll(1,4)-1]
    if(++player_hit_c == PLAYER_HIT_CYCLES)    
      player_hit_c := 0
      player_hit := FALSE
      Adventure_ti_palette_map.byte[3] := $4b

  
  'Update player tile position and flag for animation 
  if(kb_keys & IID_UP AND NOT moved)
    player_dir := DIR_UP
    sprite_tbl[1] := @PC_up_0
    if( Check_Move(player_tx, player_ty, player_dir) )
      player_ty -= 1
      moved := TRUE              
               
  if(kb_keys & IID_DOWN AND NOT moved)
    player_dir := DIR_DOWN
    sprite_tbl[1] := @PC_down_0
    if( Check_Move(player_tx, player_ty, player_dir) )
      player_ty += 1
      moved := TRUE
                
  if(kb_keys & IID_LEFT AND NOT moved)
    player_dir := DIR_LEFT
    sprite_tbl[1] := @PC_left_0
    if( Check_Move(player_tx, player_ty, player_dir) )
      player_tx -= 1
      moved := TRUE
                
  if(kb_keys & IID_RIGHT AND NOT moved)
    player_dir := DIR_RIGHT
    sprite_tbl[1] := @PC_right_0
    if( Check_Move(player_tx, player_ty, player_dir) )
      player_tx += 1
      moved := TRUE


  'Handle PC attack
  if(kb_keys & IID_FIRE)
    if(moved == FALSE AND pc_attacking == FALSE AND att_lock_ctr == 0)
      pc_attacking := TRUE
      pc_att_ctr := PLAYER_ATTACK_CYCLES
      att_lock_ctr := ATTACK_LOCK_CYCLES
  else
    if(att_lock_ctr > 0)
      att_lock_ctr--
    
  if(pc_attacking)
    if(pc_att_ctr-- == 0)
      pc_attacking := FALSE
          
    case player_dir
      DIR_UP:
        sprite_tbl[1] := @PC_up_A
        'Weapon Sprite
        sprite_tbl[2] := ((player_y-16) << 24) + ((player_x) << 16) + (0 << 8) + ($01)    
        sprite_tbl[3] := @PC_sword_up 
   
      DIR_DOWN:
        sprite_tbl[1] := @PC_down_A
        'Weapon Sprite
        sprite_tbl[2] := ((player_y+16) << 24) + ((player_x) << 16) + (0 << 8) + ($01)    
        sprite_tbl[3] := @PC_sword_down
    
      DIR_LEFT:
        sprite_tbl[1] := @PC_left_A
        'Weapon Sprite
        sprite_tbl[2] := ((player_y) << 24) + ((player_x-16) << 16) + (0 << 8) + ($01)    
        sprite_tbl[3] := @PC_sword_left
   
      DIR_RIGHT:
        sprite_tbl[1] := @PC_right_A
        'Weapon Sprite
        sprite_tbl[2] := ((player_y) << 24) + ((player_x+16) << 16) + (0 << 8) + ($01)    
        sprite_tbl[3] := @PC_sword_right

  else
    sprite_tbl[2] := 0   'Disable weapon sprite
    case player_dir
      DIR_UP:
        sprite_tbl[1] := @PC_up_0
   
      DIR_DOWN:
        sprite_tbl[1] := @PC_down_0
        
      DIR_LEFT:
        sprite_tbl[1] := @PC_left_0
   
      DIR_RIGHT:
        sprite_tbl[1] := @PC_right_0
        
  Check_S(player_tx, player_ty, player_dir)

  'Animate PC
  if(moved)
    pitac++
    case player_dir
     
      DIR_UP:
        player_y -= 2
        case pitac
          0..1:
            sprite_tbl[1] := @PC_up_0
          2..3:
            sprite_tbl[1] := @PC_up_1
          4..5:
            sprite_tbl[1] := @PC_up_2
          6..7:
            sprite_tbl[1] := @PC_up_3
          8:
            sprite_tbl[1] := @PC_up_0
      
      DIR_DOWN:
        player_y += 2
        case pitac
          0..1:
            sprite_tbl[1] := @PC_down_0
          2..3:
            sprite_tbl[1] := @PC_down_1
          4..5:
            sprite_tbl[1] := @PC_down_2
          6..7:
            sprite_tbl[1] := @PC_down_3
          8:
            sprite_tbl[1] := @PC_down_0
      
      DIR_RIGHT:
        player_x += 2
        case pitac
          0..1:
            sprite_tbl[1] := @PC_right_0
          2..3:
            sprite_tbl[1] := @PC_right_1
          4..5:
            sprite_tbl[1] := @PC_right_2
          6..7:
            sprite_tbl[1] := @PC_right_3
          8:
            sprite_tbl[1] := @PC_right_0
      
      DIR_LEFT:
        player_x -= 2
        case pitac
          0..1:
            sprite_tbl[1] := @PC_left_0
          2..3:
            sprite_tbl[1] := @PC_left_1
          4..5:
            sprite_tbl[1] := @PC_left_2
          6..7:
            sprite_tbl[1] := @PC_left_3
          8:
            sprite_tbl[1] := @PC_left_0


    'Update PC Sprite position  
    sprite_tbl[0] := ((player_y) << 24) + ((player_x) << 16) + (0 << 8) + ($01)
  
    if(pitac == 8)     'PC is on next tile
      pitac := 0
      moved := FALSE

      'Pickup Items or handle special tiles (like fire)
      item_num := Check_Items(player_tx, player_ty)
      case item_num
      
        ITEM_HEALTH:
          player_hp := player_hp + item_mod_value <# player_max_hp  'Limit max hp (limit will not work with += opr ???)
          item_mod_value := 0

        ITEM_HPMAX_ADD:
          player_max_hp += 1
          player_hp := player_max_hp

        ITEM_GEM:
          player_gems += item_mod_value
          item_mod_value := 0

        ITEM_SWORD_1:
          inventory |= INV_ID_SWORD_1
          Equip

        TILE_FIRE:
          'Damage player
          player_hit := TRUE

      'PC walked off screen, load next screen and reset PC position    
      if(player_tx < 0 OR player_tx > 9 OR player_ty < 0 OR player_ty > 11)
        case player_dir
         
          DIR_UP:
            screen_num := scr_up
            player_ty := 11
             
          DIR_DOWN:
            screen_num := scr_down
            player_ty := 0
             
          DIR_RIGHT:
            screen_num := scr_right
            player_tx := 0
            
          DIR_LEFT:
            screen_num := scr_left
            player_tx := 9
            
        game_state := GAME_LOAD_SCREEN

PUB Equip
  'Equip inventory
  if(inventory & INV_ID_SWORD_1)
    Adventure_ti_palette_map.byte[2] := $CB
    player_ap := 2
  else
    Adventure_ti_palette_map.byte[2] := $06
    
PUB Check_Items(p_tx, p_ty) : item_num | i

 'Check if PC is picking up an item. Update items data. Overwrite with blank tile and return tile index.
  item_num := word[tile_map_base_ptr_parm][p_tx + (p_ty * TILEMAP_WIDTH)] & $00_FF
  if(item_num > 3)
    word[tile_map_base_ptr_parm][p_tx + (p_ty * TILEMAP_WIDTH)] := $00_00
    
    'Update items array
    if(item_count > 0)
      repeat i from 0 to item_count-1
        ' If item is available (visible)
        if(items[i*ITEM_SIZE+ITEM_FLAGS] == 1)
          'Look for item in items array with current XY tile coords
          if(p_tx == items[i*ITEM_SIZE+ITEM_TX] AND p_ty == items[i*ITEM_SIZE+ITEM_TY])
           'Turn off available flag
            items[i*ITEM_SIZE+ITEM_FLAGS] := 0
            item_mod_value := items[i*ITEM_SIZE+ITEM_MODIFIER]

      items_dirty := TRUE        'Indicate item changes, triggers item datarecords to be saved.

PUB Check_S(p_tx, p_ty, p_dir) | tile_data
  'Secret stuff goes here
  tile_data := word[tile_map_base_ptr_parm][p_tx + (p_ty * TILEMAP_WIDTH)] & $00_FF

  case screen_num
    1:
      if(p_tx==6 AND p_ty==10)
        word[tile_map_base_ptr_parm][3 + (3 * TILEMAP_WIDTH)] := $00_00
    3:
      if(p_tx == 4 AND p_ty == 2 AND p_dir == DIR_UP)
        if(pc_attacking)
          word[tile_map_base_ptr_parm][4 + (1 * TILEMAP_WIDTH)] := $03 << 8 + TILE_FIRE
          word[tile_map_base_ptr_parm][4 + (4 * TILEMAP_WIDTH)] := $00_00

    8:
      if(not pc_attacking)
        seql := false
      if(pc_attacking and not seql)
        case seqi
          0:
            if(seq[0] == sprite_tbl[2])
              seqi++
              seql := true
          1:
            if(seq[1] == sprite_tbl[2])
              seqi++
              seql := true
            else
              seqi := 0
          2:
            if(seq[2] == sprite_tbl[2])
              seqi := 0
              word[tile_map_base_ptr_parm][2 + (5 * TILEMAP_WIDTH)] := $00_00
              word[tile_map_base_ptr_parm][3 + (5 * TILEMAP_WIDTH)] := $00_00
            else
              seqi := 0

PUB load_seq | i

  case screen_num
    8:
      seq[0] := $60600001
      seq[1] := $40600001
      seq[2] := $80600001
      seqi := 0
            
PUB Draw_Status | hp_ones, hp_tens, hpm_ones, hpm_tens, gem_ones, gem_tens, gem_hundreds, i, ii, cc

  'Hide sprites
  tile_map_sprite_cntrl_parm.byte[1] := 0

  'Calculate positions
  hp_tens := player_hp / 10
  hp_ones := player_hp // 10
  hpm_tens := player_max_hp / 10
  hpm_ones := player_max_hp // 10
  gem_hundreds := player_gems / 100
  gem_tens := (player_gems // 100) / 10
  gem_ones := (player_gems // 100) // 10

  'Save tilemap under status display
  cc := 0
  repeat i from 0 to STATUS_HEIGHT-1
    repeat ii from 0 to STATUS_WIDTH-1
      status_buf[cc++] := word[tile_map_base_ptr_parm][(ii+STATUS_X) + ((i+STATUS_Y) * TILEMAP_WIDTH)]

  'Draw Status Box
  repeat i from 0 to STATUS_HEIGHT-1
    repeat ii from 0 to STATUS_WIDTH-1
      word[tile_map_base_ptr_parm][(ii+STATUS_X) + ((i+STATUS_Y) * TILEMAP_WIDTH)] := $00_00

  'Draw health symbol
  word[tile_map_base_ptr_parm][STATUS_X + 0 + ((STATUS_Y + 1) * TILEMAP_WIDTH)] := $02 << 8 + ITEM_HEALTH
  'Draw health tens
  word[tile_map_base_ptr_parm][STATUS_X + 1 + ((STATUS_Y + 1) * TILEMAP_WIDTH)] := TILE_NUMBERS + hp_tens
  'Draw health ones
  word[tile_map_base_ptr_parm][STATUS_X + 2 + ((STATUS_Y + 1) * TILEMAP_WIDTH)] := TILE_NUMBERS + hp_ones
  'Draw /
  word[tile_map_base_ptr_parm][STATUS_X + 3 + ((STATUS_Y + 1) * TILEMAP_WIDTH)] := TILE_NUMBERS + 10
  'Draw max health ones
  word[tile_map_base_ptr_parm][STATUS_X + 4 + ((STATUS_Y + 1) * TILEMAP_WIDTH)] := TILE_NUMBERS + hpm_tens
  'Draw max ones
  word[tile_map_base_ptr_parm][STATUS_X + 5 + ((STATUS_Y + 1) * TILEMAP_WIDTH)] := TILE_NUMBERS + hpm_ones


  'Draw gem symbol
  word[tile_map_base_ptr_parm][STATUS_X + 0 + ((STATUS_Y + 2) * TILEMAP_WIDTH)] := $01 << 8 + ITEM_GEM
  'Draw gem hundreds
  word[tile_map_base_ptr_parm][STATUS_X + 1 + ((STATUS_Y + 2) * TILEMAP_WIDTH)] := TILE_NUMBERS + gem_hundreds
  'Draw gem tens
  word[tile_map_base_ptr_parm][STATUS_X + 2 + ((STATUS_Y + 2) * TILEMAP_WIDTH)] := TILE_NUMBERS + gem_tens
  'Draw gem ones
  word[tile_map_base_ptr_parm][STATUS_X + 3 + ((STATUS_Y + 2) * TILEMAP_WIDTH)] := TILE_NUMBERS + gem_ones

  if(inventory & INV_ID_SWORD_1)
    word[tile_map_base_ptr_parm][STATUS_X + 1 + ((STATUS_Y + 4) * TILEMAP_WIDTH)] := $05 << 8 + ITEM_SWORD_1

PUB Restore_Screen | i, ii, cc

  'Restore sprites
  tile_map_sprite_cntrl_parm.byte[1] := mon_count +2
  cc := 0
  repeat i from 0 to STATUS_HEIGHT-1
    repeat ii from 0 to STATUS_WIDTH-1
      word[tile_map_base_ptr_parm][(ii+STATUS_X) + ((i+STATUS_Y) * TILEMAP_WIDTH)] := status_buf[cc++]

  
    
PUB Update_Monster(mon_index) | dx,dy,dist,tx,ty,x,y,m_dir,m_type,hp,frame,status,m_ptr,enabled
'Beginnings of monster AI
'FIX THIS. Seperate into logical methods. ADD status checks. ADD combat.

  tx := monsters[mon_index*MONSTER_SIZE+MON_TX]    
  ty := monsters[mon_index*MONSTER_SIZE+MON_TY]
  x := monsters[mon_index*MONSTER_SIZE+MON_X]    
  y := monsters[mon_index*MONSTER_SIZE+MON_Y]
               
  m_dir := monsters[mon_index*MONSTER_SIZE+MON_DIR]          
  m_type := monsters[mon_index*MONSTER_SIZE+MON_TYPE]
  hp := monsters[mon_index*MONSTER_SIZE+MON_HP]
  frame := monsters[mon_index*MONSTER_SIZE+MON_FRAME]
  status := monsters[mon_index*MONSTER_SIZE+MON_STATUS]

  if(NOT monsters[mon_index*MONSTER_SIZE+MON_STATUS] & MON_SID_SHOW)
    return

'* COMBAT *************************************************************
  'Handle PC hit by monster
  if(player_hit == FALSE AND tx == player_tx AND ty == player_ty AND frame > 3)
    player_hit := TRUE

    
  'Handle monster hit by PC
  if(pc_attacking AND NOT (status & MON_SID_HIT))
    case player_dir
      DIR_UP:
        if(tx == player_tx AND (ty == player_ty-1 OR ty == player_ty))
          hp -= player_ap
          status |= MON_SID_HIT
          monsters_dirty := TRUE

     
      DIR_DOWN:
        if(tx == player_tx AND (ty == player_ty+1 OR ty == player_ty))
          hp -= player_ap
          status |= MON_SID_HIT
          monsters_dirty := TRUE

          
      DIR_LEFT:
        if((tx == player_tx-1 OR tx == player_tx) AND ty == player_ty)
          hp -= player_ap
          status |= MON_SID_HIT
          monsters_dirty := TRUE

          
      DIR_RIGHT:
        if((tx == player_tx+1 OR tx == player_tx) AND ty == player_ty)
          hp -= player_ap
          status |= MON_SID_HIT
          monsters_dirty := TRUE

  'Add color change to indicate damage and hit timeout here.
  if(status & MON_SID_HIT)
    monster_hit_c++

  'Reset hit timeout and status.
  if(monster_hit_c == MONSTER_HIT_CYCLES)
    status ^= MON_SID_HIT
    monster_hit_c := 0

  if(hp =< 0)
  ' Dead, disable
    status ^= MON_SID_SHOW

'* END COMBAT *********************************************************
{
  term.str(string("tx: "))
  term.dec(tx)
  term.newline

  term.str(string("ty: "))
  term.dec(ty)
  term.newline

  term.str(string("x: "))
  term.dec(x)
  term.newline

  term.str(string("y: "))
  term.dec(y)
  term.newline
}  

  'Monster AI behaviors. FIX THIS! Seperate behaviors from monster type.
  if((NOT status & MON_SID_MOVING) AND frame == 0)
    case m_type
     
      'Simple bat AI, runs back and forth between obstructions or screen bounds  
      MON_TYPE_BAT:   
        case m_dir      
          DIR_UP:
            if(Check_Move(tx,ty,m_dir) AND ty > 0)
              ty -= 1
              status |= MON_SID_MOVING
            else
              m_dir := DIR_DOWN
          DIR_DOWN:
            if(Check_Move(tx,ty,m_dir) AND ty < 11)
              ty += 1
              status |= MON_SID_MOVING
            else
              m_dir := DIR_UP
          DIR_LEFT:
            if(Check_Move(tx,ty,m_dir) AND tx > 0)
              tx -= 1
              status |= MON_SID_MOVING
            else
              m_dir := DIR_RIGHT 
          DIR_RIGHT:
            if(Check_Move(tx,ty,m_dir) AND tx < 9)
              tx += 1
              status |= MON_SID_MOVING
            else
              m_dir := DIR_LEFT
              
    'Simple bat AI, turns randomly when obstruction is hit.  
      MON_TYPE_BATR:      
        case m_dir
              
          DIR_UP:        
            if(Check_Move(tx,ty,m_dir) AND ty > 0)
              ty -= 1
              status |= MON_SID_MOVING
            else
              m_dir := roll(1,4)-1
              
          DIR_DOWN:
            if(Check_Move(tx,ty,m_dir) AND ty < 11)
              ty += 1
              status |= MON_SID_MOVING
            else
              m_dir := roll(1,4)-1
              
          DIR_LEFT:
            if(Check_Move(tx,ty,m_dir) AND tx > 0)
              tx -= 1
              status |= MON_SID_MOVING
            else
              m_dir := roll(1,4)-1
               
          DIR_RIGHT:
            if(Check_Move(tx,ty,m_dir) AND tx < 9)
              tx += 1
              status |= MON_SID_MOVING
            else
              m_dir := roll(1,4)-1
     
    'Simple bat AI, turns randomly on tile boundaries.  
      MON_TYPE_BATRT:

        m_dir := roll(1,4)-1
         
        case m_dir      

          DIR_UP:
            if(Check_Move(tx,ty,m_dir) AND ty > 0)
              ty -= 1
              status |= MON_SID_MOVING
            else
              m_dir := DIR_DOWN
          DIR_DOWN:
            if(Check_Move(tx,ty,m_dir) AND ty < 11)
              ty += 1
              status |= MON_SID_MOVING
            else
              m_dir := DIR_UP
          DIR_LEFT:
            if(Check_Move(tx,ty,m_dir) AND tx > 0)
              tx -= 1
              status |= MON_SID_MOVING
            else
              m_dir := DIR_RIGHT 
          DIR_RIGHT:
            if(Check_Move(tx,ty,m_dir) AND tx < 9)
              tx += 1
              status |= MON_SID_MOVING
            else
              m_dir := DIR_LEFT

     'Hunter wasp AI, Attacks player. FIX THIS, make behavior distance based and more random. Obstacle avoidance?
      MON_TYPE_WASP:
       
        dx := player_tx-tx
        dy := player_ty-ty
        dist := (dx*dx + dy*dy)
        
        if (player_tx > tx)
          m_dir := DIR_RIGHT
        elseif (player_tx < tx)
          m_dir := DIR_LEFT
       
        if (player_ty > ty)
          m_dir := DIR_DOWN
        elseif (player_ty < ty)
          m_dir := DIR_UP
           
        case m_dir      
          DIR_UP:
            if(Check_Move(tx,ty,m_dir) AND ty > 0)
              ty -= 1
              status |= MON_SID_MOVING
            
          DIR_DOWN:
            if(Check_Move(tx,ty,m_dir) AND ty < 11)
              ty += 1
              status |= MON_SID_MOVING
              
          DIR_LEFT:
            if(Check_Move(tx,ty,m_dir) AND tx > 0)
              tx -= 1
              status |= MON_SID_MOVING
 
          DIR_RIGHT:
            if(Check_Move(tx,ty,m_dir) AND tx < 9)
              tx += 1
              status |= MON_SID_MOVING

  'FIX THIS make speed dependent on monster type, or monster datarecord  
  if(status & MON_SID_MOVING)
    case m_dir    
      DIR_UP:
        y -= 1      
      DIR_DOWN:
        y += 1      
      DIR_RIGHT:
        x += 1      
      DIR_LEFT:
        x -= 1

        
  'Animate monster sprite

  case m_type
    MON_TYPE_BAT..MON_TYPE_BATRT:
      m_ptr := @_bat
    MON_TYPE_WASP:
      if(m_dir == DIR_UP OR m_dir == DIR_RIGHT)
        m_ptr := @_wasp_right
      else
        m_ptr := @_wasp_left
      
  if(frame => 0 AND frame =< 3)
    sprite_tbl[2*mon_index +5] := m_ptr
    
  if(frame => 4 AND frame =< 7)
    sprite_tbl[2*mon_index +5] := m_ptr+1*128
    
  if(frame => 8 AND frame =< 11)
    sprite_tbl[2*mon_index +5] := m_ptr+2*128
    
  if(frame > 12)
    sprite_tbl[2*mon_index +5] := m_ptr+3*128

  'Cycle Pixel Frames
  if(++frame == 16)
    frame := 0
    if(status & MON_SID_MOVING)
      status := status ^ MON_SID_MOVING

  'Save monster data for next cycle.
  monsters[mon_index*MONSTER_SIZE+MON_X] := x    
  monsters[mon_index*MONSTER_SIZE+MON_Y] := y
  monsters[mon_index*MONSTER_SIZE+MON_TX] := tx    
  monsters[mon_index*MONSTER_SIZE+MON_TY] := ty            
  monsters[mon_index*MONSTER_SIZE+MON_DIR] := m_dir         
  monsters[mon_index*MONSTER_SIZE+MON_TYPE] := m_type
  monsters[mon_index*MONSTER_SIZE+MON_HP] := hp
  monsters[mon_index*MONSTER_SIZE+MON_FRAME] := frame
  monsters[mon_index*MONSTER_SIZE+MON_STATUS] := status

  'Update monster sprite. POSSIBLE BUG HERE, INISIBLE MONSTERS!
  sprite_tbl[2*mon_index +4] := (y << 24) + (x << 16) + (0 << 8) + (status & MON_SID_SHOW)


PUB Check_Move(tx,ty,dir) : tile_index
  'Check if sprite can move and return tile index or FALSE of blocked. FIX THIS!

  case dir
    DIR_UP:
      tile_index := word[tile_map_base_ptr_parm][tx + ((ty-1) * TILEMAP_WIDTH)] & $00_FF    
    DIR_DOWN:
      tile_index := word[tile_map_base_ptr_parm][tx + ((ty+1) * TILEMAP_WIDTH)] & $00_FF    
    DIR_RIGHT:
      tile_index := word[tile_map_base_ptr_parm][(tx+1) + (ty * TILEMAP_WIDTH)] & $00_FF    
    DIR_LEFT:
      tile_index := word[tile_map_base_ptr_parm][(tx-1) + (ty * TILEMAP_WIDTH)] & $00_FF

  'If tile index is greater than boundary then it is passable
  'MAKE SURE to order tiles correctly
  if(tile_index <> 1)
    return TRUE
  else
    return FALSE
      
PUB Check_Combat

PUB roll(number_of_dice, number_of_sides) : retval
  retval := 0
  repeat number_of_dice
    retval += ||(dice?//number_of_sides) + 1

PUB Read_Keyboard(device_present) : keyboard_state  | kbx, kby
  ' universal controller interface for keyboard version 2.0
  ' now supports the x,y fields in bytes 2,3
   
  ' make sure the device is present
  if (device_present==FALSE)
    return 0
   
  ' reset state vars
  keyboard_state := 0
  kbx := 0
  kby := 0
   
  ' first control id's
  ' set ESC id
  if (kb.keystate(KB_ESC))
    keyboard_state |= IID_ESC         
   
  ' set start id
  if (kb.keystate(KB_SPACE) or kb.keystate(KB_ENTER) or kb.keystate(KB_LEFT_CTRL) or kb.keystate(KB_RIGHT_CTRL) )
    keyboard_state |= IID_START
    
  ' set fire id
  if (kb.keystate(KB_SPACE) or kb.keystate(KB_LEFT_CTRL) or kb.keystate(KB_RIGHT_CTRL) )
    keyboard_state |= IID_FIRE
   
  ' now directionals
  if (kb.keystate(KB_LEFT_ARROW))  
    keyboard_state |= IID_LEFT
    kbx := -2
    
  if (kb.keystate(KB_RIGHT_ARROW))
    keyboard_state |= IID_RIGHT
    kbx := 2
   
  if (kb.keystate(KB_UP_ARROW))    
    keyboard_state |= IID_UP
    kby := -2
   
  if (kb.keystate(KB_DOWN_ARROW))  
    keyboard_state |= IID_DOWN
    kby := 2
   
  ' merge x,y into packet
  keyboard_state := keyboard_state | ((kby & $00FF) << 24) | ((kbx & $00FF) << 16)
   
  return keyboard_state

PUB Get_Screen_Offset(scr_number) : offset | row, column
  'Calculate tile map offset for screen number scr_num
  offset := 2 * TILEMAP_WIDTH + 2      'Allow for screen seperator tiles
  row := scr_number / SCREENS_ACROSS
  column := scr_number // SCREENS_ACROSS
  if(row > 0)
    offset += (row * SCREEN_TILE_HEIGHT * TILEMAP_WIDTH) * 2
    
  if(column > 0)
    offset += (column * SCREEN_TILE_WIDTH) * 2
                                                         
DAT

' Game Data
'########################################################################################################################
'Screen datarecords
'Screens do not have to be next to one another in tilemap. You may jump around, even link multiple screens to one.
'Currently one a many-to-one is possible, not one-to-many since screen load triggers when PC walks off screen.
'FIX THIS! make it possible that 2 or more exits on one side may lead to different screens.

'                   screen up     screen down    screen left    screen right
screen_data
{screen 0}    word     0,              0,             0,             1
{screen 1}    word     0,              0,             0,             2
{screen 2}    word     0,              7,             1,             3
{screen 3}    word     0,              0,             2,             4
{screen 4}    word     0,              9,             3,             0
{screen 5}    word     0,              0,             0,             6
{screen 6}    word     6,              6,             6,             6
{screen 7}    word     2,              0,             6,             8
{screen 8}    word     0,              0,             7,             9
{screen 9}    word     4,              0,             8,             0
{screen 10}   word     0,              0,             0,             0
{screen 11}   word     0,              0,             0,             0
{screen 12}   word     0,              0,             0,             0
{screen 13}   word     0,              0,             0,             0
{screen 14}   word     0,              0,             0,             0
{screen 15}   word     0,              0,             0,             0
{screen 16}   word     0,              0,             0,             0
{screen 17}   word     0,              0,             0,             0
{screen 18}   word     0,              0,             0,             0
{screen 19}   word     0,              0,             0,             0
{screen 20}   word     0,              0,             0,             0
{screen 21}   word     0,              0,             0,             0
{screen 22}   word     0,              0,             0,             0
{screen 23}   word     0,              0,             0,             0
{screen 24}   word     0,              0,             0,             0
{screen 25}   word     0,              0,             0,             0
{screen 26}   word     0,              0,             0,             0
{screen 27}   word     0,              0,             0,             0
{screen 28}   word     0,              0,             0,             0
{screen 29}   word     0,              0,             0,             0               


'Monster datarecords, per screen  FIX THIS! combine status elements into one byte and check bits.

'                Number
'                              TX   TY    X Y       Direction        Type                HP   Anim Frame     STATUS
monster_data                                                                                   
{screen 0}    byte 3                                                                            
              byte             1,    10  ,0,0,      DIR_RIGHT,        MON_TYPE_BAT,      1,       0,        %00000001
              byte             6,    4   ,0,0,      DIR_LEFT,         MON_TYPE_BAT,      1,       0,        %00000001
              byte             3,    5   ,0,0,      DIR_UP,           MON_TYPE_BAT,      1,       0,        %00000001

{screen 1}    byte 2
              byte             3,    1   ,0,0,      DIR_DOWN,         MON_TYPE_BAT,      2,       0,        %00000001
              byte             8,    1   ,0,0,      DIR_DOWN,         MON_TYPE_BATr,     2,       0,        %00000001
              
{screen 2}    byte 2
              byte             1,    1   ,0,0,      DIR_RIGHT,        MON_TYPE_BATr,     2,       0,        %00000001
              byte             5,    7   ,0,0,      DIR_UP,           MON_TYPE_BATrt,    2,       0,        %00000001
              
{screen 3}    byte 2
              byte             6,    6   ,0,0,      DIR_RIGHT,        MON_TYPE_BATrt,    2,       0,        %00000001
              byte             5,    4   ,0,0,      DIR_UP,           MON_TYPE_BATrt,    2,       0,        %00000001
              
{screen 4}    byte 3
              byte             8,    1   ,0,0,      DIR_DOWN,         MON_TYPE_WASP,     3,       0,        %00000001
              byte             1,    1   ,0,0,      DIR_DOWN,         MON_TYPE_WASP,     3,       0,        %00000001
              byte             4,    2   ,0,0,      DIR_UP,           MON_TYPE_WASP,     3,       0,        %00000001
              
{screen 5}    byte 0
{screen 6}    byte 0
{screen 7}    byte 0

{screen 8}    byte 3
              byte             4,    3   ,0,0,      DIR_RIGHT,        MON_TYPE_WASP,     3,       0,        %00000001
              byte             4,    5   ,0,0,      DIR_RIGHT,        MON_TYPE_WASP,     3,       0,        %00000001
              byte             4,    7   ,0,0,      DIR_RIGHT,        MON_TYPE_WASP,     3,       0,        %00000001

{screen 9}    byte 4
              byte             4,    10  ,0,0,      DIR_UP,           MON_TYPE_WASP,     3,       0,        %00000001
              byte             8,    6   ,0,0,      DIR_UP,           MON_TYPE_WASP,     3,       0,        %00000001
              byte             1,    5   ,0,0,      DIR_UP,           MON_TYPE_WASP,     3,       0,        %00000001
              byte             5,    7   ,0,0,      DIR_UP,           MON_TYPE_WASP,     3,       0,        %00000001
              
{screen 10}   byte 0
{screen 11}   byte 0
{screen 12}   byte 0
{screen 13}   byte 0
{screen 14}   byte 0
{screen 15}   byte 0
{screen 16}   byte 0
{screen 17}   byte 0
{screen 18}   byte 0
{screen 19}   byte 0
{screen 20}   byte 0
{screen 21}   byte 0
{screen 22}   byte 0
{screen 23}   byte 0
{screen 24}   byte 0
{screen 25}   byte 0
{screen 26}   byte 0
{screen 27}   byte 0
{screen 28}   byte 0
{screen 29}   byte 0                        

'Item datarecords, per screen
'                Number    Palette,   Tile,          TX    TY    Modifier    Flags

item_data
{screen 0}    byte 2
              byte           $03,   ITEM_GEM,        1,    3,       1,         1
              byte           $04,   ITEM_GEM,        6,    10,      5,         1
              
{screen 1}    byte 2
              byte           $02,   ITEM_HEALTH,     1,    1,       1,         1
              byte           $06,   ITEM_GEM,        5,    10,      5,         1

{screen 2}    byte 3
              byte           $02,   ITEM_HEALTH,     1,    1,       1,         1
              byte           $03,   ITEM_GEM,        7,    7,       1,         1
              byte           $03,   ITEM_GEM,        8,    7,       1,         1
              
{screen 3}    byte 7
              byte           $02,   ITEM_HPMAX_ADD,  4,    5,       1,         1
              byte           $03,   ITEM_GEM,        5,    4,       1,         1
              byte           $04,   ITEM_GEM,        6,    4,       5,         1
              byte           $03,   ITEM_GEM,        6,    5,       1,         1
              byte           $03,   ITEM_GEM,        3,    6,       1,         1
              byte           $06,   ITEM_GEM,        5,    6,       10,        1
              byte           $03,   ITEM_GEM,        4,    7,       1,         1
              
{screen 4}    byte 6
              byte           $03,   ITEM_GEM,        4,    7,       5,         1
              byte           $03,   ITEM_GEM,        1,    8,       5,         1
              byte           $02,   ITEM_HPMAX_ADD,  4,    3,       1,         1
              byte           $04,   ITEM_GEM,        3,    2,       10,        1
              byte           $04,   ITEM_GEM,        4,    2,       10,        1
              byte           $04,   ITEM_GEM,        5,    2,       10,        1
              
{screen 5}    byte 0
              
{screen 6}    byte 0
{screen 7}    byte 0

{screen 8}    byte 1
              byte           $05,   ITEM_SWORD_1,    1,    5,       1,         1
              
{screen 9}    byte 5
              byte           $04,   ITEM_GEM,        3,    10,      5,         1
              byte           $04,   ITEM_GEM,        5,    10,      5,         1
              byte           $02,   ITEM_HPMAX_ADD,  8,    7,       1,         1
              byte           $03,   ITEM_GEM,        1,    1,       1,         1
              byte           $06,   ITEM_GEM,        3,    6,       10,        1
              
{screen 10}   byte 0
{screen 11}   byte 0
{screen 12}   byte 0
{screen 13}   byte 0
{screen 14}   byte 0
{screen 15}   byte 0
{screen 16}   byte 0
{screen 17}   byte 0
{screen 18}   byte 0
{screen 19}   byte 0
{screen 20}   byte 0
{screen 21}   byte 0
{screen 22}   byte 0
{screen 23}   byte 0
{screen 24}   byte 0
{screen 25}   byte 0
{screen 26}   byte 0
{screen 27}   byte 0
{screen 28}   byte 0
{screen 29}   byte 0


'End Game Data
'########################################################################################################################                                     

' sprite table, 8 sprites, 2 LONGs per sprite, 16 LONGs total length

              ' sprite 0 header
sprite_tbl    long $00_00_00_00  ' state/control word: y,x,z,state
              long $00_00_00_00  ' bitmap ptr

              ' sprite 1 header
              long $00_00_00_00  ' state/control word: y,x,z,state
              long $00_00_00_00  ' bitmap ptr

              ' sprite 2 header
              long $00_00_00_00  ' state/control word: y,x,z,state
              long $00_00_00_00  ' bitmap ptr

              ' sprite 3 header
              long $00_00_00_00  ' state/control word: y,x,z,state
              long $00_00_00_00  ' bitmap ptr

              ' sprite 4 header
              long $00_00_00_00  ' state/control word: y,x,z,state
              long $00_00_00_00  ' bitmap ptr

              ' sprite 5 header
              long $00_00_00_00  ' state/control word: y,x,z,state
              long $00_00_00_00  ' bitmap ptr

              ' sprite 6 header
              long $00_00_00_00  ' state/control word: y,x,z,state
              long $00_00_00_00  ' bitmap ptr

              ' sprite 7 header
              long $00_00_00_00  ' state/control word: y,x,z,state
              long $00_00_00_00  ' bitmap ptr

' end sprite table

' sprite bitmap table
' each bitmap is 16x16 pixels, 1 x 16 longs
' bitmaps are reflected left to right, so keep that in mind
' they are numbered for reference only and any bitmap can be assigned to any sprite thru the use of the
' sprite pointer in the sprite header, this allows easy animation without data movement
' additionally, each sprite needs a "mask" to help the rendering engine, computation of the mask is
' too time sensitive, thus the mask must follow immediately after the sprite

sprite_bitmaps          long

              
' Extracted from file "PC2.bmp" at tile=(0, 0), size=(16, 16), palette index=(0)
PC_up_0    LONG
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_3_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_3_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_3_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_1_1_1_2_2_2_0_0_0_0
              LONG %%0_0_0_0_1_3_1_1_1_3_3_2_0_0_0_0
              LONG %%0_0_0_2_1_3_1_1_1_1_3_2_0_0_0_0
              LONG %%0_0_0_0_2_3_3_3_3_3_2_2_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_2_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_0_0_0_1_0_0_0_0_0_0
              LONG %%0_0_0_0_3_1_0_0_0_1_3_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_3_1_1_0_0_0_1_1_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(0, 0), size=(16, 16), palette index=(0)
PC2_tx0_ty0_mask
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_0_0_0_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(1, 0), size=(16, 16), palette index=(1)
PC_up_1    LONG
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_3_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_3_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_3_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_1_1_1_2_2_2_0_0_0_0
              LONG %%0_0_0_0_1_3_1_1_1_3_3_2_0_0_0_0
              LONG %%0_0_0_2_1_3_1_1_1_1_3_2_0_0_0_0
              LONG %%0_0_0_0_2_3_3_3_3_3_2_2_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_2_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_1_0_0_0_0_0_0
              LONG %%0_0_0_3_1_1_0_0_0_1_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_1_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(1, 0), size=(16, 16), palette index=(1)
PC2_tx1_ty0_mask
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(2, 0), size=(16, 16), palette index=(2)
PC_up_2    LONG
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_3_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_3_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_3_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_1_1_1_2_2_2_0_0_0_0
              LONG %%0_0_0_0_1_3_1_1_1_3_3_2_0_0_0_0
              LONG %%0_0_0_2_1_3_1_1_1_1_3_2_0_0_0_0
              LONG %%0_0_0_0_2_3_3_3_3_3_2_2_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_2_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_0_0_0_1_0_0_0_0_0_0
              LONG %%0_0_0_0_3_1_0_0_0_1_3_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_3_1_1_0_0_0_1_1_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(2, 0), size=(16, 16), palette index=(2)
PC2_tx2_ty0_mask
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_0_0_0_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(3, 0), size=(16, 16), palette index=(3)
PC_up_3    LONG
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_3_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_3_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_3_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_1_1_1_2_2_2_0_0_0_0
              LONG %%0_0_0_0_1_3_1_1_1_3_3_2_0_0_0_0
              LONG %%0_0_0_2_1_3_1_1_1_1_3_2_0_0_0_0
              LONG %%0_0_0_0_2_3_3_3_3_3_2_2_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_2_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_1_1_3_0_0_0_0_0
              LONG %%0_0_0_0_0_1_0_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_3_1_0_0_0_1_1_3_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_1_1_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(3, 0), size=(16, 16), palette index=(3)
PC2_tx3_ty0_mask
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(0, 1), size=(16, 16), palette index=(4)
PC_down_0    LONG
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_0_0_2_0_0_0_0
              LONG %%0_0_0_2_2_2_2_2_1_0_0_2_0_0_0_0
              LONG %%0_0_0_2_3_3_3_2_1_3_0_2_0_0_0_0
              LONG %%0_0_0_2_3_3_3_2_1_1_2_2_2_0_0_0
              LONG %%0_0_0_2_2_3_2_2_1_1_1_2_1_0_0_0
              LONG %%0_0_0_0_2_2_2_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_2_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_0_0_0_1_0_0_0_0_0_0
              LONG %%0_0_0_0_3_1_0_0_0_1_3_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_3_1_1_0_0_0_1_1_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(0, 1), size=(16, 16), palette index=(4)
PC2_tx0_ty1_mask
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_3_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_0_0_3_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_0_0_0_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(1, 1), size=(16, 16), palette index=(5)
PC_down_1    LONG
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_0_0_2_0_0_0_0
              LONG %%0_0_0_2_2_2_2_2_1_0_0_2_0_0_0_0
              LONG %%0_0_0_2_3_3_3_2_1_3_0_2_0_0_0_0
              LONG %%0_0_0_2_3_3_3_2_1_1_2_2_2_0_0_0
              LONG %%0_0_0_2_2_3_2_2_1_1_1_2_1_0_0_0
              LONG %%0_0_0_0_2_2_2_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_2_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_0_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_3_1_0_0_0_1_1_3_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_1_1_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(1, 1), size=(16, 16), palette index=(5)
PC2_tx1_ty1_mask
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_3_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_0_0_3_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(2, 1), size=(16, 16), palette index=(6)
PC_down_2    LONG
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_0_0_2_0_0_0_0
              LONG %%0_0_0_2_2_2_2_2_1_0_0_2_0_0_0_0
              LONG %%0_0_0_2_3_3_3_2_1_3_0_2_0_0_0_0
              LONG %%0_0_0_2_3_3_3_2_1_1_2_2_2_0_0_0
              LONG %%0_0_0_2_2_3_2_2_1_1_1_2_1_0_0_0
              LONG %%0_0_0_0_2_2_2_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_2_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_0_0_0_1_0_0_0_0_0_0
              LONG %%0_0_0_0_3_1_0_0_0_1_3_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_3_1_1_0_0_0_1_1_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(2, 1), size=(16, 16), palette index=(6)
PC2_tx2_ty1_mask
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_3_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_0_0_3_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_0_0_0_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(3, 1), size=(16, 16), palette index=(7)
PC_down_3    LONG
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_0_0_2_0_0_0_0
              LONG %%0_0_0_2_2_2_2_2_1_0_0_2_0_0_0_0
              LONG %%0_0_0_2_3_3_3_2_1_3_0_2_0_0_0_0
              LONG %%0_0_0_2_3_3_3_2_1_1_2_2_2_0_0_0
              LONG %%0_0_0_2_2_3_2_2_1_1_1_2_1_0_0_0
              LONG %%0_0_0_0_2_2_2_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_2_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_1_0_0_0_0_0_0
              LONG %%0_0_0_3_1_1_0_0_0_1_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_1_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(3, 1), size=(16, 16), palette index=(7)
PC2_tx3_ty1_mask
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_3_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_0_0_3_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(0, 2), size=(16, 16), palette index=(8)
PC_left_0    LONG
              LONG %%0_0_0_0_0_0_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_1_0_0_0_0_0_0_0_0_2
              LONG %%0_0_0_0_0_3_1_1_0_0_0_0_0_0_2_0
              LONG %%0_0_2_2_2_2_2_1_0_0_0_0_0_2_0_0
              LONG %%0_0_2_3_3_3_2_1_3_0_2_0_2_0_0_0
              LONG %%0_0_2_3_3_3_2_1_1_1_1_2_0_0_0_0
              LONG %%0_0_2_2_3_2_2_1_1_1_2_1_2_0_0_0
              LONG %%0_0_0_2_2_2_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_1_1_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_1_0_0_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_3_1_0_0_0_0_1_3_0_0_0_0_0
              LONG %%0_0_0_1_1_0_0_0_0_1_1_0_0_0_0_0
              LONG %%0_0_3_1_1_0_0_0_0_1_1_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(0, 2), size=(16, 16), palette index=(8)
PC2_tx0_ty2_mask
              LONG %%0_0_0_0_0_0_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_0_0_0_0_0_0_3
              LONG %%0_0_0_0_0_3_3_3_0_0_0_0_0_0_3_0
              LONG %%0_0_3_3_3_3_3_3_0_0_0_0_0_3_0_0
              LONG %%0_0_3_3_3_3_3_3_3_0_3_0_3_0_0_0
              LONG %%0_0_3_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_3_3_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_0_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_3_3_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_3_3_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_3_3_3_0_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(1, 2), size=(16, 16), palette index=(9)
PC_left_1    LONG
              LONG %%0_0_0_0_0_0_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_1_0_0_0_0_0_0_0_0_2
              LONG %%0_0_0_0_0_3_1_1_0_0_0_0_0_0_2_0
              LONG %%0_0_2_2_2_2_2_1_0_0_0_0_0_2_0_0
              LONG %%0_0_2_3_3_3_2_1_3_0_2_0_2_0_0_0
              LONG %%0_0_2_3_3_3_2_1_1_1_1_2_0_0_0_0
              LONG %%0_0_2_2_3_2_2_1_1_1_2_1_2_0_0_0
              LONG %%0_0_0_2_2_2_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_1_1_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_3_1_0_0_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_1_1_0_0_1_1_0_0_0_0_0_0_0
              LONG %%0_0_3_1_1_0_0_1_1_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(1, 2), size=(16, 16), palette index=(9)
PC2_tx1_ty2_mask
              LONG %%0_0_0_0_0_0_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_0_0_0_0_0_0_3
              LONG %%0_0_0_0_0_3_3_3_0_0_0_0_0_0_3_0
              LONG %%0_0_3_3_3_3_3_3_0_0_0_0_0_3_0_0
              LONG %%0_0_3_3_3_3_3_3_3_0_3_0_3_0_0_0
              LONG %%0_0_3_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_3_3_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_0_0_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_0_0_3_3_0_0_0_0_0_0_0
              LONG %%0_0_3_3_3_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(2, 2), size=(16, 16), palette index=(10)
PC_left_2    LONG
              LONG %%0_0_0_0_0_0_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_1_0_0_0_0_0_0_0_0_2
              LONG %%0_0_0_0_0_3_1_1_0_0_0_0_0_0_2_0
              LONG %%0_0_2_2_2_2_2_1_0_0_0_0_0_2_0_0
              LONG %%0_0_2_3_3_3_2_1_3_0_2_0_2_0_0_0
              LONG %%0_0_2_3_3_3_2_1_1_1_1_2_0_0_0_0
              LONG %%0_0_2_2_3_2_2_1_1_1_2_1_2_0_0_0
              LONG %%0_0_0_2_2_2_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_1_1_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_3_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_3_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_3_0_1_1_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(2, 2), size=(16, 16), palette index=(10)
PC2_tx2_ty2_mask
              LONG %%0_0_0_0_0_0_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_0_0_0_0_0_0_3
              LONG %%0_0_0_0_0_3_3_3_0_0_0_0_0_0_3_0
              LONG %%0_0_3_3_3_3_3_3_0_0_0_0_0_3_0_0
              LONG %%0_0_3_3_3_3_3_3_3_0_3_0_3_0_0_0
              LONG %%0_0_3_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_3_3_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(3, 2), size=(16, 16), palette index=(11)
PC_left_3    LONG
              LONG %%0_0_0_0_0_0_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_1_0_0_0_0_0_0_0_0_2
              LONG %%0_0_0_0_0_3_1_1_0_0_0_0_0_0_2_0
              LONG %%0_0_2_2_2_2_2_1_0_0_0_0_0_2_0_0
              LONG %%0_0_2_3_3_3_2_1_3_0_2_0_2_0_0_0
              LONG %%0_0_2_3_3_3_2_1_1_1_1_2_0_0_0_0
              LONG %%0_0_2_2_3_2_2_1_1_1_2_1_2_0_0_0
              LONG %%0_0_0_2_2_2_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_1_1_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(3, 2), size=(16, 16), palette index=(11)
PC2_tx3_ty2_mask
              LONG %%0_0_0_0_0_0_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_0_0_0_0_0_0_3
              LONG %%0_0_0_0_0_3_3_3_0_0_0_0_0_0_3_0
              LONG %%0_0_3_3_3_3_3_3_0_0_0_0_0_3_0_0
              LONG %%0_0_3_3_3_3_3_3_3_0_3_0_3_0_0_0
              LONG %%0_0_3_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_3_3_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(0, 3), size=(16, 16), palette index=(12)
PC_right_0    LONG
              LONG %%0_0_0_0_0_0_0_0_0_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_1_3_0_0_0_0_0
              LONG %%2_0_0_0_0_0_0_0_0_1_3_0_0_0_0_0
              LONG %%0_2_0_0_0_0_0_0_1_1_3_0_0_0_0_0
              LONG %%0_0_2_0_0_0_0_0_1_2_2_2_2_2_0_0
              LONG %%0_0_0_2_0_2_0_3_1_2_3_3_3_2_0_0
              LONG %%0_0_0_0_2_1_1_1_1_2_3_3_3_2_0_0
              LONG %%0_0_0_2_1_2_1_1_1_2_2_3_2_2_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_2_2_2_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_1_2_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_1_0_0_0_0
              LONG %%0_0_0_0_0_3_1_0_0_0_0_1_3_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_0_0_1_1_0_0_0
              LONG %%0_0_0_0_3_1_1_0_0_0_0_1_1_3_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(0, 3), size=(16, 16), palette index=(12)
PC2_tx0_ty3_mask
              LONG %%0_0_0_0_0_0_0_0_0_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_3_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_3_3_3_0_0_0_0_0
              LONG %%0_3_0_0_0_0_0_0_3_3_3_0_0_0_0_0
              LONG %%0_0_3_0_0_0_0_0_3_3_3_3_3_3_0_0
              LONG %%0_0_0_3_0_3_0_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_0_0_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_3_3_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_3_3_0_0_0
              LONG %%0_0_0_0_3_3_3_0_0_0_0_3_3_3_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(1, 3), size=(16, 16), palette index=(13)
PC_right_1    LONG
              LONG %%0_0_0_0_0_0_0_0_0_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_1_3_0_0_0_0_0
              LONG %%2_0_0_0_0_0_0_0_0_1_3_0_0_0_0_0
              LONG %%0_2_0_0_0_0_0_0_1_1_3_0_0_0_0_0
              LONG %%0_0_2_0_0_0_0_0_1_2_2_2_2_2_0_0
              LONG %%0_0_0_2_0_2_0_3_1_2_3_3_3_2_0_0
              LONG %%0_0_0_0_2_1_1_1_1_2_3_3_3_2_0_0
              LONG %%0_0_0_2_1_2_1_1_1_2_2_3_2_2_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_2_2_2_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_1_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_0_0_1_3_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_0_0_1_1_0_0_0
              LONG %%0_0_0_0_0_0_3_1_1_0_0_1_1_3_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(1, 3), size=(16, 16), palette index=(13)
PC2_tx1_ty3_mask
              LONG %%0_0_0_0_0_0_0_0_0_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_3_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_3_3_3_0_0_0_0_0
              LONG %%0_3_0_0_0_0_0_0_3_3_3_0_0_0_0_0
              LONG %%0_0_3_0_0_0_0_0_3_3_3_3_3_3_0_0
              LONG %%0_0_0_3_0_3_0_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_0_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_0_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_0_0_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_0_0_3_3_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_3_3_3_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(2, 3), size=(16, 16), palette index=(14)
PC_right_2    LONG
              LONG %%0_0_0_0_0_0_0_0_0_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_1_3_0_0_0_0_0
              LONG %%2_0_0_0_0_0_0_0_0_1_3_0_0_0_0_0
              LONG %%0_2_0_0_0_0_0_0_1_1_3_0_0_0_0_0
              LONG %%0_0_2_0_0_0_0_0_1_2_2_2_2_2_0_0
              LONG %%0_0_0_2_0_2_0_3_1_2_3_3_3_2_0_0
              LONG %%0_0_0_0_2_1_1_1_1_2_3_3_3_2_0_0
              LONG %%0_0_0_2_1_2_1_1_1_2_2_3_2_2_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_2_2_2_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_1_2_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_3_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_3_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_3_1_1_0_3_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(2, 3), size=(16, 16), palette index=(14)
PC2_tx2_ty3_mask
              LONG %%0_0_0_0_0_0_0_0_0_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_3_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_3_3_3_0_0_0_0_0
              LONG %%0_3_0_0_0_0_0_0_3_3_3_0_0_0_0_0
              LONG %%0_0_3_0_0_0_0_0_3_3_3_3_3_3_0_0
              LONG %%0_0_0_3_0_3_0_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_0_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(3, 3), size=(16, 16), palette index=(15)
PC_right_3    LONG
              LONG %%0_0_0_0_0_0_0_0_0_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_1_3_0_0_0_0_0
              LONG %%2_0_0_0_0_0_0_0_0_1_3_0_0_0_0_0
              LONG %%0_2_0_0_0_0_0_0_1_1_3_0_0_0_0_0
              LONG %%0_0_2_0_0_0_0_0_1_2_2_2_2_2_0_0
              LONG %%0_0_0_2_0_2_0_3_1_2_3_3_3_2_0_0
              LONG %%0_0_0_0_2_1_1_1_1_2_3_3_3_2_0_0
              LONG %%0_0_0_2_1_2_1_1_1_2_2_3_2_2_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_2_2_2_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_1_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(3, 3), size=(16, 16), palette index=(15)
PC2_tx3_ty3_mask
              LONG %%0_0_0_0_0_0_0_0_0_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_3_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_3_3_3_0_0_0_0_0
              LONG %%0_3_0_0_0_0_0_0_3_3_3_0_0_0_0_0
              LONG %%0_0_3_0_0_0_0_0_3_3_3_3_3_3_0_0
              LONG %%0_0_0_3_0_3_0_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(0, 4), size=(16, 16), palette index=(16)
PC_up_A    LONG
              LONG %%0_0_0_2_2_2_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_1_1_3_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_0_3_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_3_3_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_3_1_1_1_2_2_2_0_0_0_0
              LONG %%0_0_0_0_1_3_1_1_1_3_3_2_0_0_0_0
              LONG %%0_0_0_0_1_3_1_1_1_1_3_2_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_2_2_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_2_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_0_0_0_1_3_0_0_0_0_0
              LONG %%0_0_0_0_3_1_0_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_3_1_3_0_0_0_0
              LONG %%0_0_0_3_1_1_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(0, 4), size=(16, 16), palette index=(16)
PC2_tx0_ty4_mask
              LONG %%0_0_0_3_3_3_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_0_0_0_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(1, 4), size=(16, 16), palette index=(17)
PC_down_A    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_2_2_2_2_2_1_0_0_0_0_0_0_0
              LONG %%0_0_0_2_3_3_3_2_1_3_3_0_0_0_0_0
              LONG %%0_0_0_2_3_3_3_2_1_3_1_1_0_0_0_0
              LONG %%0_0_0_2_2_3_2_2_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_2_2_2_3_3_3_1_1_0_0_0_0
              LONG %%0_0_0_0_0_2_1_0_1_1_3_1_0_0_0_0
              LONG %%0_0_0_0_3_1_0_0_0_1_3_1_1_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_1_2_2_2_0_0_0
              LONG %%0_0_0_3_1_1_0_0_0_1_1_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(1, 4), size=(16, 16), palette index=(17)
PC2_tx1_ty4_mask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_3_3_0_3_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_3_3_3_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_3_3_3_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_3_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(2, 4), size=(16, 16), palette index=(18)
PC_left_A    LONG
              LONG %%0_0_0_0_0_0_0_0_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_2_2_2_2_2_1_0_0_0_0_0_0
              LONG %%0_0_0_0_2_3_3_3_2_1_3_0_0_0_2_0
              LONG %%0_0_0_0_2_3_3_3_2_1_1_3_1_1_2_2
              LONG %%0_0_0_0_2_2_3_2_2_1_1_1_1_1_2_0
              LONG %%0_0_0_0_0_2_2_2_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_2_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_1_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_3_1_0_0_0_0_1_3_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_0_0_1_1_0_0_0
              LONG %%0_0_0_0_3_1_1_0_0_0_0_1_1_3_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(2, 4), size=(16, 16), palette index=(18)
PC2_tx2_ty4_mask
              LONG %%0_0_0_0_0_0_0_0_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_0_0_0_3_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_3_3_3_3
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_3_3_3_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_3_0_0_0_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_3_3_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_0_0_3_3_0_0_0
              LONG %%0_0_0_0_3_3_3_0_0_0_0_3_3_3_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(3, 4), size=(16, 16), palette index=(19)
PC_right_A    LONG
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_2_2_2_2_2_0_0_0_0
              LONG %%0_2_0_0_0_3_1_2_3_3_3_2_0_0_0_0
              LONG %%2_2_1_1_3_1_1_2_3_3_3_2_0_0_0_0
              LONG %%0_2_1_1_1_1_1_2_2_3_2_2_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_2_2_2_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_2_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_1_0_0_0_0_0_0
              LONG %%0_0_0_3_1_0_0_0_0_1_3_0_0_0_0_0
              LONG %%0_0_0_1_1_0_0_0_0_1_1_0_0_0_0_0
              LONG %%0_0_3_1_1_0_0_0_0_1_1_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(3, 4), size=(16, 16), palette index=(19)
PC2_tx3_ty4_mask
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_3_0_0_0_0
              LONG %%0_3_0_0_0_3_3_3_3_3_3_3_0_0_0_0
              LONG %%3_3_3_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_3_3_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_0_3_3_0_0_0_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_3_0_0_0_0_0_0
              LONG %%0_0_0_3_3_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_3_3_0_0_0_0_3_3_0_0_0_0_0
              LONG %%0_0_3_3_3_0_0_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(0, 5), size=(16, 16), palette index=(20)
PC_sword_up    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_2_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(0, 5), size=(16, 16), palette index=(20)
PC2_tx0_ty5_mask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(1, 5), size=(16, 16), palette index=(21)
PC_sword_down    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(1, 5), size=(16, 16), palette index=(21)
PC2_tx1_ty5_mask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(2, 5), size=(16, 16), palette index=(22)
PC_sword_left    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%2_2_2_2_2_2_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(2, 5), size=(16, 16), palette index=(22)
PC2_tx2_ty5_mask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%2_2_2_2_2_2_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(3, 5), size=(16, 16), palette index=(23)
PC_sword_right    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_2_2_2_2_2_2
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "PC2.bmp" at tile=(3, 5), size=(16, 16), palette index=(23)
PC2_tx3_ty5_mask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_3_3_3_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              

_bat        
' Extracted from file "bats.bmp" at tile=(1, 0), size=(16, 16), palette index=(1)
bat0          LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_1_0_0_0_0_0_0_0_0_0_0_0_0_1_0
              LONG %%0_1_0_0_0_0_0_0_0_0_0_0_0_0_1_0
              LONG %%1_1_0_0_1_0_0_0_0_0_0_1_0_0_1_1
              LONG %%1_1_0_0_0_1_0_0_0_0_1_1_0_0_1_1
              LONG %%1_1_0_0_0_1_1_0_0_1_1_0_0_0_1_1
              LONG %%1_1_1_0_0_1_3_1_1_3_1_0_0_1_1_1
              LONG %%1_1_1_0_1_1_1_1_1_1_1_1_0_1_1_0
              LONG %%1_1_1_1_1_0_2_0_0_2_0_1_1_1_1_1
              LONG %%0_0_0_1_0_0_0_0_0_0_0_0_0_1_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "bats.bmp" at tile=(1, 0), size=(16, 16), palette index=(1)
bats_tx1_ty0_mask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_3_0_0_0_0_0_0_0_0_0_0_0_0_3_0
              LONG %%0_3_0_0_0_0_0_0_0_0_0_0_0_0_3_0
              LONG %%3_3_0_0_3_0_0_0_0_0_0_3_0_0_3_3
              LONG %%3_3_0_0_0_3_0_0_0_0_3_3_0_0_3_3
              LONG %%3_3_0_0_0_3_3_0_0_3_3_0_0_0_3_3
              LONG %%3_3_3_0_0_0_3_3_3_3_3_0_0_3_3_3
              LONG %%3_3_3_0_3_3_3_3_3_3_3_3_0_3_3_0
              LONG %%3_3_3_3_3_0_3_0_0_3_0_3_3_3_3_3
              LONG %%0_0_0_3_0_0_0_0_0_0_0_0_0_3_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "bats.bmp" at tile=(2, 0), size=(16, 16), palette index=(2)
bat1          LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_0_0_0_0_0_0_1_0_0_0_0
              LONG %%0_0_0_0_0_1_0_0_0_0_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_1_1_3_1_0_0_0_0_0
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_1_0_0_0
              LONG %%0_1_1_1_1_0_2_0_0_2_0_1_1_1_1_0
              LONG %%1_1_1_0_0_0_0_0_0_0_0_0_0_1_1_1
              LONG %%1_1_0_0_0_0_0_0_0_0_0_0_0_0_1_0
              LONG %%0_1_1_0_0_0_0_0_0_0_0_0_0_1_1_1
              LONG %%0_1_1_0_0_0_0_0_0_0_0_0_0_1_1_0
              LONG %%0_1_1_1_1_0_0_0_0_0_0_1_1_1_1_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "bats.bmp" at tile=(2, 0), size=(16, 16), palette index=(2)
bats_tx2_ty0_mask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_0_0_0_0_0_0_3_0_0_0_0
              LONG %%0_0_0_0_0_3_0_0_0_0_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_3_3_3_3_0_0_0
              LONG %%0_3_3_3_3_0_3_0_0_3_0_3_3_3_3_0
              LONG %%3_3_3_0_0_0_0_0_0_0_0_0_0_3_3_3
              LONG %%3_3_0_0_0_0_0_0_0_0_0_0_0_0_3_0
              LONG %%0_3_3_0_0_0_0_0_0_0_0_0_0_3_3_3
              LONG %%0_3_3_0_0_0_0_0_0_0_0_0_0_3_3_0
              LONG %%0_3_3_3_3_0_0_0_0_0_0_3_3_3_3_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "bats.bmp" at tile=(3, 0), size=(16, 16), palette index=(3)
bat2          LONG %%0_0_0_0_1_0_0_0_0_0_0_1_0_0_0_0
              LONG %%0_0_0_0_0_1_0_0_0_0_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_1_1_3_1_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_1_1_0_2_0_0_2_0_1_1_0_0_0
              LONG %%0_0_0_1_1_0_0_0_0_0_0_1_1_0_0_0
              LONG %%0_0_1_1_1_0_0_0_0_0_0_1_1_1_0_0
              LONG %%0_0_0_1_1_0_0_0_0_0_0_1_1_0_0_0
              LONG %%0_1_1_1_0_0_0_0_0_0_0_0_1_1_1_0
              LONG %%0_0_1_1_0_0_0_0_0_0_0_0_1_1_0_0
              LONG %%0_1_1_1_0_0_0_0_0_0_0_0_1_1_1_0
              LONG %%0_0_1_0_0_0_0_0_0_0_0_0_0_1_0_0
              LONG %%0_1_1_0_0_0_0_0_0_0_0_0_0_1_1_0
              LONG %%0_0_1_0_0_0_0_0_0_0_0_0_0_1_0_0
              LONG %%0_1_0_0_0_0_0_0_0_0_0_0_0_0_1_0

' Extracted from file "bats.bmp" at tile=(3, 0), size=(16, 16), palette index=(3)
bats_tx3_ty0_mask
              LONG %%0_0_0_0_3_0_0_0_0_0_0_3_0_0_0_0
              LONG %%0_0_0_0_0_3_0_0_0_0_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_3_3_0_3_0_0_3_0_3_3_0_0_0
              LONG %%0_0_0_3_3_0_0_0_0_0_0_3_3_0_0_0
              LONG %%0_0_3_3_3_0_0_0_0_0_0_3_3_3_0_0
              LONG %%0_0_0_3_3_0_0_0_0_0_0_3_3_0_0_0
              LONG %%0_3_3_3_0_0_0_0_0_0_0_0_3_3_3_0
              LONG %%0_0_3_3_0_0_0_0_0_0_0_0_3_3_0_0
              LONG %%0_3_3_3_0_0_0_0_0_0_0_0_3_3_3_0
              LONG %%0_0_3_0_0_0_0_0_0_0_0_0_0_3_0_0
              LONG %%0_3_3_0_0_0_0_0_0_0_0_0_0_3_3_0
              LONG %%0_0_3_0_0_0_0_0_0_0_0_0_0_3_0_0
              LONG %%0_3_0_0_0_0_0_0_0_0_0_0_0_0_3_0

' Extracted from file "bats.bmp" at tile=(4, 0), size=(16, 16), palette index=(4)
bat3          LONG %%0_0_0_0_1_0_0_0_0_0_0_1_0_0_0_0
              LONG %%0_0_0_0_0_1_0_0_0_0_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_3_1_1_3_1_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_1_0_2_0_0_2_0_1_0_0_0_0
              LONG %%0_0_0_1_1_1_0_0_0_0_1_1_1_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_1_1_1_0_0_0_0_1_1_1_0_0_0
              LONG %%0_0_0_0_1_1_1_0_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_1_0_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_0_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_0_0_0_0_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "bats.bmp" at tile=(4, 0), size=(16, 16), palette index=(4)
bats_tx4_ty0_mask
              LONG %%0_0_0_0_3_0_0_0_0_0_0_3_0_0_0_0
              LONG %%0_0_0_0_0_3_0_0_0_0_3_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_0_3_0_0_3_0_3_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_0_3_3_3_0_0_0
              LONG %%0_0_0_0_3_3_0_0_0_0_3_3_0_0_0_0
              LONG %%0_0_0_3_3_3_0_0_0_0_3_3_3_0_0_0
              LONG %%0_0_0_0_3_3_3_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_3_3_3_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_3_3_3_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_3_3_3_0_0_0_0
              LONG %%0_0_0_0_0_3_3_0_0_3_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0


_wasp_left LONG

wasp_left_0   LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_0_0_0_0_0_0_0_0
              LONG %%0_2_2_2_0_0_2_2_2_0_0_0_0_0_0_0
              LONG %%0_2_2_2_2_0_2_2_2_0_0_0_0_0_0_0
              LONG %%0_0_2_2_2_2_2_2_2_0_0_0_0_0_0_0
              LONG %%0_0_0_2_2_2_2_2_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_2_2_2_2_0_0_1_1_0_0_0
              LONG %%0_0_0_0_0_0_2_2_2_1_1_1_2_2_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_1_2_2_1_0
              LONG %%0_0_1_1_1_1_1_1_3_1_0_1_1_1_1_0
              LONG %%0_1_1_1_1_3_0_3_0_3_0_0_0_0_0_0
              LONG %%1_1_1_1_1_0_3_0_3_0_3_0_0_0_0_0
              LONG %%1_1_1_0_0_0_0_3_0_3_0_0_0_0_0_0
              LONG %%1_1_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "wasp.bmp" at tile=(0, 0), size=(16, 16), palette index=(0)
wasp_tx0_ty0_mask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_3_3_3_0_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_3_3_3_3_0_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_3_3_3_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_3_3_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_3_3_3_0_0_3_3_0_0_0
              LONG %%0_0_0_0_0_0_3_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_3_3_3_0
              LONG %%0_0_3_3_3_3_3_3_3_3_0_3_3_3_3_0
              LONG %%0_3_3_3_3_3_0_3_0_3_0_0_0_0_0_0
              LONG %%3_3_3_3_3_0_3_0_3_0_3_0_0_0_0_0
              LONG %%3_3_3_0_0_0_0_3_0_3_0_0_0_0_0_0
              LONG %%3_3_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "wasp.bmp" at tile=(1, 0), size=(16, 16), palette index=(1)
wasp_left_1    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_2_2_2_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_2_2_2_2_0_0_0_0_0_0_0_0_0
              LONG %%2_2_2_0_0_2_2_2_0_0_0_0_0_0_0_0
              LONG %%0_2_2_2_2_2_2_2_2_0_0_1_1_0_0_0
              LONG %%0_0_0_0_2_2_2_2_2_1_1_1_2_2_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_1_2_2_1_0
              LONG %%0_0_1_1_1_1_1_1_3_1_0_1_1_1_1_0
              LONG %%0_1_1_1_1_3_0_3_0_3_0_0_0_0_0_0
              LONG %%1_1_1_1_1_0_3_0_3_0_3_0_0_0_0_0
              LONG %%1_1_1_0_0_0_0_3_0_3_0_0_0_0_0_0
              LONG %%1_1_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "wasp.bmp" at tile=(1, 0), size=(16, 16), palette index=(1)
wasp_tx1_ty0_mask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_3_3_3_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_3_0_0_0_0_0_0_0_0_0
              LONG %%3_3_3_0_0_3_3_3_0_0_0_0_0_0_0_0
              LONG %%0_3_3_3_3_3_3_3_3_0_0_3_3_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_3_3_3_0
              LONG %%0_0_3_3_3_3_3_3_3_3_0_3_3_3_3_0
              LONG %%0_3_3_3_3_3_0_3_0_3_0_0_0_0_0_0
              LONG %%3_3_3_3_3_0_3_0_3_0_3_0_0_0_0_0
              LONG %%3_3_3_0_0_0_0_3_0_3_0_0_0_0_0_0
              LONG %%3_3_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "wasp.bmp" at tile=(2, 0), size=(16, 16), palette index=(2)
wasp_left_2    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_2_2_2_2_2_0_0_0_0_1_1_0_0_0
              LONG %%2_2_2_2_2_2_2_2_2_1_1_1_2_2_0_0
              LONG %%2_2_2_2_2_1_1_1_1_1_1_1_2_2_1_0
              LONG %%0_0_1_1_1_1_1_1_3_1_0_1_1_1_1_0
              LONG %%0_1_1_1_1_3_0_3_0_3_0_0_0_0_0_0
              LONG %%1_1_1_1_1_0_3_0_3_0_3_0_0_0_0_0
              LONG %%1_1_1_0_0_0_0_3_0_3_0_0_0_0_0_0
              LONG %%1_1_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "wasp.bmp" at tile=(2, 0), size=(16, 16), palette index=(2)
wasp_tx2_ty0_mask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_3_3_3_3_3_0_0_0_0_3_3_0_0_0
              LONG %%3_3_3_3_3_3_3_3_3_3_3_3_3_3_0_0
              LONG %%3_3_3_3_3_3_3_3_3_3_3_3_3_3_3_0
              LONG %%0_0_3_3_3_3_3_3_3_3_0_3_3_3_3_0
              LONG %%0_3_3_3_3_3_0_3_0_3_0_0_0_0_0_0
              LONG %%3_3_3_3_3_0_3_0_3_0_3_0_0_0_0_0
              LONG %%3_3_3_0_0_0_0_3_0_3_0_0_0_0_0_0
              LONG %%3_3_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "wasp.bmp" at tile=(3, 0), size=(16, 16), palette index=(3)
wasp_left_3    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_2_2_2_2_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_2_2_2_2_0_0_0_0_0_0_0_0_0
              LONG %%2_2_2_0_0_2_2_2_0_0_0_0_0_0_0_0
              LONG %%0_2_2_2_2_2_2_2_2_0_0_1_1_0_0_0
              LONG %%0_0_0_0_2_2_2_2_2_1_1_1_2_2_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_1_2_2_1_0
              LONG %%0_0_1_1_1_1_1_1_3_1_0_1_1_1_1_0
              LONG %%0_1_1_1_1_3_0_3_0_3_0_0_0_0_0_0
              LONG %%1_1_1_1_1_0_3_0_3_0_3_0_0_0_0_0
              LONG %%1_1_1_0_0_0_0_3_0_3_0_0_0_0_0_0
              LONG %%1_1_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "wasp.bmp" at tile=(3, 0), size=(16, 16), palette index=(3)
wasp_tx3_ty0_mask
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_3_3_3_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_3_3_0_0_0_0_0_0_0_0_0
              LONG %%3_3_3_0_0_3_3_3_0_0_0_0_0_0_0_0
              LONG %%0_3_3_3_3_3_3_3_3_0_0_3_3_0_0_0
              LONG %%0_0_0_0_3_3_3_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_0_0_3_3_3_3_3_3_3_3_3_3_0
              LONG %%0_0_3_3_3_3_3_3_3_3_0_3_3_3_3_0
              LONG %%0_3_3_3_3_3_0_3_0_3_0_0_0_0_0_0
              LONG %%3_3_3_3_3_0_3_0_3_0_3_0_0_0_0_0
              LONG %%3_3_3_0_0_0_0_3_0_3_0_0_0_0_0_0
              LONG %%3_3_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%3_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

_wasp_right LONG

wasp_right_0  LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_2_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_0_2_2_2_0
              LONG %%0_0_0_0_0_0_0_2_2_2_0_2_2_2_2_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_2_2_2_0_0
              LONG %%0_0_0_0_0_0_0_2_2_2_2_2_2_0_0_0
              LONG %%0_0_0_1_1_0_0_2_2_2_2_0_0_0_0_0
              LONG %%0_0_2_2_1_1_1_2_2_2_0_0_0_0_0_0
              LONG %%0_1_2_2_1_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_1_1_1_1_0_1_3_1_1_1_1_1_1_0_0
              LONG %%0_0_0_0_0_0_3_0_3_0_3_1_1_1_1_0
              LONG %%0_0_0_0_0_3_0_3_0_3_0_1_1_1_1_1
              LONG %%0_0_0_0_0_0_3_0_3_0_0_0_0_1_1_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_1_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "wasp.bmp" at tile=(0, 0), size=(16, 16), palette index=(0)

              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_0_3_3_3_0
              LONG %%0_0_0_0_0_0_0_3_3_3_0_3_3_3_3_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_0_0_0_0_3_3_3_3_3_3_0_0_0
              LONG %%0_0_0_3_3_0_0_3_3_3_3_0_0_0_0_0
              LONG %%0_0_3_3_3_3_3_3_3_3_0_0_0_0_0_0
              LONG %%0_3_3_3_3_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_3_3_3_3_0_3_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_0_0_0_3_0_3_0_3_3_3_3_3_0
              LONG %%0_0_0_0_0_3_0_3_0_3_0_3_3_3_3_3
              LONG %%0_0_0_0_0_0_3_0_3_0_0_0_0_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "wasp.bmp" at tile=(1, 0), size=(16, 16), palette index=(1)
wasp_right_1           LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_2_2_2_0
              LONG %%0_0_0_0_0_0_0_0_0_2_2_2_2_0_0_0
              LONG %%0_0_0_0_0_0_0_0_2_2_2_0_0_2_2_2
              LONG %%0_0_0_1_1_0_0_2_2_2_2_2_2_2_2_0
              LONG %%0_0_2_2_1_1_1_2_2_2_2_2_0_0_0_0
              LONG %%0_1_2_2_1_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_1_1_1_1_0_1_3_1_1_1_1_1_1_0_0
              LONG %%0_0_0_0_0_0_3_0_3_0_3_1_1_1_1_0
              LONG %%0_0_0_0_0_3_0_3_0_3_0_1_1_1_1_1
              LONG %%0_0_0_0_0_0_3_0_3_0_0_0_0_1_1_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_1_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "wasp.bmp" at tile=(1, 0), size=(16, 16), palette index=(1)

              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_3_3_3_3_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_3_0_0_3_3_3
              LONG %%0_0_0_3_3_0_0_3_3_3_3_3_3_3_3_0
              LONG %%0_0_3_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_3_3_3_3_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_3_3_3_3_0_3_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_0_0_0_3_0_3_0_3_3_3_3_3_0
              LONG %%0_0_0_0_0_3_0_3_0_3_0_3_3_3_3_3
              LONG %%0_0_0_0_0_0_3_0_3_0_0_0_0_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "wasp.bmp" at tile=(2, 0), size=(16, 16), palette index=(2)
wasp_right_2           LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_1_1_0_0_0_0_2_2_2_2_2_0_0
              LONG %%0_0_2_2_1_1_1_2_2_2_2_2_2_2_2_2
              LONG %%0_1_2_2_1_1_1_1_1_1_1_2_2_2_2_2
              LONG %%0_1_1_1_1_0_1_3_1_1_1_1_1_1_0_0
              LONG %%0_0_0_0_0_0_3_0_3_0_3_1_1_1_1_0
              LONG %%0_0_0_0_0_3_0_3_0_3_0_1_1_1_1_1
              LONG %%0_0_0_0_0_0_3_0_3_0_0_0_0_1_1_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_1_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "wasp.bmp" at tile=(2, 0), size=(16, 16), palette index=(2)

              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_3_0_0_0_0_3_3_3_3_3_0_0
              LONG %%0_0_3_3_3_3_3_3_3_3_3_3_3_3_3_3
              LONG %%0_3_3_3_3_3_3_3_3_3_3_2_2_2_3_3
              LONG %%0_3_3_3_3_0_3_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_0_0_0_3_0_3_0_3_3_3_3_3_0
              LONG %%0_0_0_0_0_3_0_3_0_3_0_3_3_3_3_3
              LONG %%0_0_0_0_0_0_3_0_3_0_0_0_0_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "wasp.bmp" at tile=(3, 0), size=(16, 16), palette index=(3)
wasp_right_3           LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_2_2_2_2_0
              LONG %%0_0_0_0_0_0_0_0_0_2_2_2_2_0_0_0
              LONG %%0_0_0_0_0_0_0_0_2_2_2_0_0_2_2_2
              LONG %%0_0_0_1_1_0_0_2_2_2_2_2_2_2_2_0
              LONG %%0_0_2_2_1_1_1_2_2_2_2_2_0_0_0_0
              LONG %%0_1_2_2_1_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_1_1_1_1_0_1_3_1_1_1_1_1_1_0_0
              LONG %%0_0_0_0_0_0_3_0_3_0_3_1_1_1_1_0
              LONG %%0_0_0_0_0_3_0_3_0_3_0_1_1_1_1_1
              LONG %%0_0_0_0_0_0_3_0_3_0_0_0_0_1_1_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_1_1
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "wasp.bmp" at tile=(3, 0), size=(16, 16), palette index=(3)

              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_3_3_3_3_0
              LONG %%0_0_0_0_0_0_0_0_0_3_3_3_3_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_3_3_0_0_3_3_3
              LONG %%0_0_0_3_3_0_0_3_3_3_3_3_3_3_3_0
              LONG %%0_0_3_3_3_3_3_3_3_3_3_3_0_0_0_0
              LONG %%0_3_3_3_3_3_3_3_3_3_3_0_0_0_0_0
              LONG %%0_3_3_3_3_0_3_3_3_3_3_3_3_3_0_0
              LONG %%0_0_0_0_0_0_3_0_3_0_3_3_3_3_3_0
              LONG %%0_0_0_0_0_3_0_3_0_3_0_3_3_3_3_3
              LONG %%0_0_0_0_0_0_3_0_3_0_0_0_0_3_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_3_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_3
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              




_tile_maps    LONG

Adventure_tilemap    WORD
              WORD      $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 0
              WORD      $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 1
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 2
              WORD      $00_00, $01_01, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $01_01, $01_01, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 3
              WORD      $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $01_01, $01_01, $00_00, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $01_01, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 4
              WORD      $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 5
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $00_00, $01_01, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 6
              WORD      $00_00, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 7
              WORD      $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 8
              WORD      $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 9
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $01_01, $01_01, $06_03, $06_03, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 10
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $01_02, $06_03, $06_03, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 11
              WORD      $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 12
              WORD      $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 13
              WORD      $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 14
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $06_03, $06_03, $01_01, $00_00, $00_00, $01_01, $06_03, $06_03, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 15
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $06_03, $06_03, $01_01, $00_00, $00_00, $01_01, $06_03, $06_03, $01_01, $00_00, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $01_01, $00_00, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 16
              WORD      $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $06_03, $06_03, $01_01, $00_00, $00_00, $01_01, $06_03, $06_03, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $01_01, $07_03, $01_01, $00_00, $00_00, $05_01, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 17
              WORD      $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $07_03, $07_03, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $01_01, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 18
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $01_01, $00_00, $01_01, $07_03, $07_03, $01_01, $00_00, $05_01, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 19
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $07_03, $07_03, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 20
              WORD      $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $01_01, $07_03, $01_01, $00_00, $00_00, $05_01, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 21
              WORD      $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $06_03, $06_03, $01_01, $00_00, $00_00, $01_01, $06_03, $06_03, $01_01, $00_00, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $01_01, $00_00, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $00_00, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 22
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $06_03, $06_03, $01_01, $00_00, $00_00, $01_01, $06_03, $06_03, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $01_01, $01_01, $01_01, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 23
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $06_03, $06_03, $01_01, $00_00, $00_00, $01_01, $06_03, $06_03, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 24
              WORD      $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 25
              WORD      $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 26
              WORD      $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 27
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 28
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 29
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 30
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 31
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 32
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 33
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 34
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 35
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 36
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 37
              WORD      $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 38
              WORD      $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 39
              WORD      $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 40
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 41
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 42
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 43
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 44
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 45
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 46
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 47
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 48
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 49
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 50
              WORD      $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 51
              WORD      $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 52
              WORD      $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 53
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 54
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 55
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 56
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 57
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 58
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 59
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 60
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 61
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 62
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 63
              WORD      $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 64
              WORD      $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 65
              WORD      $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 66
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 67
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 68
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 69
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 70
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 71
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 72
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 73
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 74
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 75
              WORD      $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 76
              WORD      $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $01_01, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 77
              WORD      $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 78
              WORD      $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00, $00_00 ' Row 79


_tile_bitmaps    LONG

_tile_blank    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

_tile_block1   LONG
              LONG %%1_1_0_1_1_1_1_1_1_1_1_0_1_1_1_1
              LONG %%3_3_2_2_2_2_2_0_0_3_2_2_2_2_0_2
              LONG %%0_3_2_3_3_2_3_2_1_3_2_0_0_3_3_2
              LONG %%3_3_3_0_2_3_0_2_1_3_3_2_3_2_3_2
              LONG %%1_3_3_2_3_3_2_2_1_3_1_3_3_1_3_2
              LONG %%3_3_2_3_2_3_0_2_1_3_3_3_2_3_3_2
              LONG %%0_3_0_3_3_3_3_3_1_3_1_3_3_3_3_0
              LONG %%1_3_0_3_0_3_1_0_1_0_0_3_0_3_1_1
              LONG %%1_3_2_2_2_2_2_2_1_3_2_2_2_2_2_2
              LONG %%3_1_3_3_3_3_3_2_1_3_2_3_3_3_3_2
              LONG %%0_3_0_2_3_2_0_2_1_3_3_3_0_2_3_2
              LONG %%3_3_2_3_3_2_3_2_1_3_1_3_2_3_0_2
              LONG %%1_3_3_3_2_3_3_2_1_3_3_3_3_2_3_2
              LONG %%3_0_3_3_0_3_0_2_1_0_3_3_0_3_3_0
              LONG %%1_3_1_0_1_3_1_1_1_1_0_3_0_3_1_1
              LONG %%1_1_1_1_1_0_1_1_1_1_1_1_1_1_1_1

_tile_block2   LONG
              LONG %%1_1_0_1_1_1_1_1_1_1_1_0_1_1_1_1
              LONG %%3_3_2_2_2_2_2_0_0_3_2_2_2_2_0_2
              LONG %%0_3_2_3_3_2_3_2_1_3_2_0_0_3_3_2
              LONG %%3_3_3_0_2_3_0_2_1_3_3_2_3_2_3_2
              LONG %%1_3_3_2_3_3_2_2_1_3_1_3_3_1_3_2
              LONG %%3_3_2_3_2_3_0_2_1_3_3_3_2_3_3_2
              LONG %%0_3_0_3_3_3_3_3_1_3_1_3_3_3_3_0
              LONG %%1_3_0_3_0_3_1_0_1_0_0_3_0_3_1_1
              LONG %%1_3_2_2_2_2_2_2_1_3_2_2_2_2_2_2
              LONG %%3_1_3_3_3_3_3_2_1_3_2_3_3_3_3_2
              LONG %%0_3_0_2_3_2_0_2_1_3_3_3_0_2_3_2
              LONG %%3_3_2_3_3_2_3_2_1_3_1_3_2_3_0_2
              LONG %%1_3_3_3_2_3_3_2_1_3_3_3_3_2_3_2
              LONG %%3_0_3_3_0_3_0_2_1_0_3_3_0_3_3_0
              LONG %%1_3_1_0_1_3_1_1_1_1_0_3_0_3_1_1
              LONG %%1_1_1_1_1_0_1_1_1_1_1_1_1_1_1_1

_tile_fire     LONG
              LONG %%1_1_1_1_1_3_2_2_1_1_1_2_1_1_2_1
              LONG %%1_2_1_1_1_2_1_1_2_2_3_1_1_2_1_2
              LONG %%1_1_1_1_3_1_1_1_2_1_2_2_2_2_3_1
              LONG %%1_2_2_3_2_2_1_2_2_1_2_2_1_3_1_1
              LONG %%1_2_2_1_2_2_1_2_1_2_1_2_2_1_1_1
              LONG %%1_1_1_3_1_3_3_1_1_2_1_1_1_1_2_1
              LONG %%3_3_1_1_1_1_2_2_1_1_1_2_1_2_2_1
              LONG %%3_1_3_3_1_3_1_3_1_1_1_1_1_1_1_2
              LONG %%1_1_1_1_1_2_2_2_2_2_1_2_1_3_3_2
              LONG %%1_1_2_2_2_1_2_2_2_3_2_2_1_1_3_2
              LONG %%1_1_2_1_2_3_1_2_3_2_1_3_3_2_2_2
              LONG %%1_1_1_2_1_1_1_1_1_1_3_3_2_1_2_1
              LONG %%1_1_2_1_3_2_3_1_3_2_3_2_2_2_3_2
              LONG %%1_1_1_1_2_1_3_3_1_1_1_2_2_1_1_1
              LONG %%1_1_1_2_2_1_2_1_3_1_2_1_1_1_1_1
              LONG %%3_2_1_1_2_1_2_1_1_1_1_1_2_2_1_3







' Extracted from file "Adventure_tiles_001_NB_Working.bmp" at tile=(4, 0), size=(16, 16), palette index=(2)
Adventure_ti_tx4_ty0_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_0_1_1_1_0_0_0_0_0
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_1_1_2_2_1_1_1_2_1_1_1_0_0_0
              LONG %%0_0_1_1_1_1_1_1_1_1_1_1_1_0_0_0
              LONG %%0_0_1_1_1_1_1_1_1_1_1_1_1_0_0_0
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "Adventure_tiles_001_NB_Working.bmp" at tile=(6, 0), size=(16, 16), palette index=(3)
Adventure_ti_tx6_ty0_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_1_3_1_1_1_1_1_3_1_0_0_0_0
              LONG %%0_0_1_1_1_3_3_3_3_3_1_1_1_0_0_0
              LONG %%0_1_3_1_3_1_1_1_1_1_3_1_3_1_0_0
              LONG %%0_0_1_3_1_1_1_1_1_1_1_3_1_0_0_0
              LONG %%0_0_0_1_3_3_3_3_3_3_3_1_0_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_3_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "Adventure_tiles_001_NB_Working.bmp" at tile=(5, 0), size=(16, 16), palette index=(4)
Adventure_ti_tx5_ty0_bitmap    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_2_1_2_2_1_2_2_0_0_0_0
              LONG %%0_0_0_0_2_3_2_2_1_2_2_3_2_0_0_0
              LONG %%0_0_0_2_1_1_3_3_3_3_3_2_1_2_0_0
              LONG %%0_0_2_3_1_3_1_1_1_1_1_3_2_3_2_0
              LONG %%0_0_0_1_3_1_1_1_1_1_1_1_3_2_0_0
              LONG %%0_0_0_0_2_3_3_3_3_3_3_3_2_0_0_0
              LONG %%0_0_0_0_0_2_1_1_1_1_1_2_0_0_0_0
              LONG %%0_0_0_0_0_0_2_1_1_1_3_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_2_1_3_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_3_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "Adventure_tiles_001_NB_Working.bmp" at tile=(7, 0), size=(16, 16), palette index=(5)
_tile_shield0 LONG
              LONG %%2_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_2_3_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_3_3_3_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_3_2_3_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_3_2_3_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_3_2_3_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_3_2_3_0_0_0_0_0_0_0_0
              LONG %%0_2_0_0_0_0_3_2_3_0_0_0_0_3_0_0
              LONG %%0_2_0_0_0_0_0_3_2_3_0_0_1_3_0_0
              LONG %%0_2_0_0_2_0_0_0_3_0_3_1_3_0_0_0
              LONG %%0_2_0_2_2_2_0_0_0_3_3_3_0_0_0_0
              LONG %%0_2_0_0_2_0_0_0_0_1_3_1_1_0_0_0
              LONG %%0_2_0_0_0_0_0_0_1_3_0_1_1_1_0_0
              LONG %%0_2_0_0_0_0_0_3_3_0_0_0_1_1_2_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_2_2_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_2

Plus_Max_HP   LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_0_1_1_1_0_0_0_0_0
              LONG %%0_0_0_1_1_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_1_1_1_2_1_1_1_1_1_1_1_0_0_0
              LONG %%0_0_1_1_1_2_1_1_2_1_1_1_1_0_0_0
              LONG %%0_0_1_1_1_2_1_2_2_2_1_1_1_0_0_0
              LONG %%0_0_0_1_1_2_1_1_2_1_1_1_0_0_0_0
              LONG %%0_0_0_1_1_2_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0



'Number tiles
tile_num_0    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "Adventure_tiles_001_NB_Working.bmp" at tile=(1, 3), size=(16, 16), palette index=(31)
tile_num_1    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "Adventure_tiles_001_NB_Working.bmp" at tile=(2, 3), size=(16, 16), palette index=(32)
tile_num_2    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "Adventure_tiles_001_NB_Working.bmp" at tile=(3, 3), size=(16, 16), palette index=(33)
tile_num_3    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "Adventure_tiles_001_NB_Working.bmp" at tile=(4, 3), size=(16, 16), palette index=(34)
tile_num_4    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_1_1_1_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_1_1_1_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "Adventure_tiles_001_NB_Working.bmp" at tile=(5, 3), size=(16, 16), palette index=(35)
tile_num_5    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "Adventure_tiles_001_NB_Working.bmp" at tile=(6, 3), size=(16, 16), palette index=(36)
tile_num_6    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_0_0_0_0_0
              LONG %%0_0_0_0_1_0_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "Adventure_tiles_001_NB_Working.bmp" at tile=(7, 3), size=(16, 16), palette index=(37)
tile_num_7    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "Adventure_tiles_001_NB_Working.bmp" at tile=(8, 3), size=(16, 16), palette index=(38)
tile_num_8    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_0_0_0_0_1_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0

' Extracted from file "Adventure_tiles_001_NB_Working.bmp" at tile=(9, 3), size=(16, 16), palette index=(39)
tile_num_9    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_1_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_1_1_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_0_0_0_0_0_1_0_0_0_0
              LONG %%0_0_0_0_0_1_1_0_0_0_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0


tile_slash    LONG
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_1_1_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_1_1_1_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_1_1_1_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_1_1_1_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_1_1_1_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_1_1_1_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_1_1_1_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_1_1_1_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_1_1_1_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_1_1_1_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_1_1_1_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_1_1_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              LONG %%0_0_0_0_0_0_0_0_0_0_0_0_0_0_0_0
              

' 6 palettes extracted from file "Adventure_tiles_001_NB_Working.bmp" using palette color map 0
_tile_palette_map    LONG
Adventure_ti_palette_map    LONG
              LONG $4b_06_7c_02 ' palette index 0
              LONG $05_04_03_02 ' palette index 1
              LONG $07_05_5B_02 ' palette index 2
              LONG $9d_8b_9b_02 ' palette index 3
              LONG $05_4A_5b_02 ' palette index 4
              LONG $CB_05_03_02 ' sword +1      5
              
              LONG $6B_5A_4A_02 ' fire          6
              LONG $FC_Dc_Db_02 ' water         7
              LONG $3B_04_5C_02 ' wasp          8

{{

┌──────────────────────────────────────────────────────────────────────────────────────┐
│                           TERMS OF USE: MIT License                                  │                                                            
├──────────────────────────────────────────────────────────────────────────────────────┤
│Permission is hereby granted, free of charge, to any person obtaining a copy of this  │
│software and associated documentation files (the "Software"), to deal in the Software │ 
│without restriction, including without limitation the rights to use, copy, modify,    │
│merge, publish, distribute, sublicense, and/or sell copies of the Software, and to    │
│permit persons to whom the Software is furnished to do so, subject to the following   │
│conditions:                                                                           │                                            │
│                                                                                      │                                               │
│The above copyright notice and this permission notice shall be included in all copies │
│or substantial portions of the Software.                                              │
│                                                                                      │                                                │
│THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,   │
│INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A         │
│PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT    │
│HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION     │
│OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE        │
│SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                │
└──────────────────────────────────────────────────────────────────────────────────────┘
}}