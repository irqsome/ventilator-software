' --------------------------------------------------------------------------------
' LameControl.spin
' Version: 0.6.2-rc2
' Copyright (c) 2015-2016 LameStation LLC
' See end of file for terms of use.
' --------------------------------------------------------------------------------
OBJ
    pin     :   "LamePinout"
    fn      :   "LameFunctions"
    
CON

    J_U = |< pin#JOY_UP
    J_D = |< pin#JOY_DOWN
    J_L = |< pin#JOY_LEFT
    J_R = |< pin#JOY_RIGHT
   
    SW_A = |< pin#BUTTON_A
    SW_B = |< pin#BUTTON_B
     
DAT

    controls    long    0

PUB Start

    dira[pin#BUTTON_A..pin#BUTTON_B]~
    dira[pin#JOY_UP..pin#JOY_RIGHT]~
    
    
PUB Update
    controls := ina
    

' 'On' is a logic low for every control pin coming
' from the hardware, so they must all be inverted.
PUB A
    return ((!controls) & SW_A <> 0)
    
PUB B
    return ((!controls) & SW_B <> 0)

PUB Left
    return (controls & J_L <> 0)
    
PUB Right
    return (controls & J_R <> 0)

PUB Up
    return (controls & J_U <> 0)

PUB Down
    return (controls & J_D <> 0)
    
DAT
    click   byte    0
    
PUB WaitKey
    repeat
        Update
        if A or B
            if not click
                click := 1
                quit
        else
            click := 0
        fn.Sleep(20)


DAT
' --------------------------------------------------------------------------------------------------------
' TERMS OF USE: MIT License
'
' Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
' associated documentation files (the "Software"), to deal in the Software without restriction, including
' without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
' copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
' following conditions:
'
' The above copyright notice and this permission notice shall be included in all copies or substantial
' portions of the Software.
'
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
' IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
' WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
' SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
' --------------------------------------------------------------------------------------------------------
DAT
