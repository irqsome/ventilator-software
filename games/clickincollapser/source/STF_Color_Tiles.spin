'' Clickin' Collapser Video Driver
'' Based on the NTSC Spectrum-like TV Video Driver
'' ──────────────────────────────────────────────────────────────────────────────────
'' Version story:
''
''      2008-04-02  2.1  Clickin' Collapser Version     (Spork Frog)
''      2007-11-25  2.0  Tiled Version                  (Spork Frog)
''      2007-12-05  1.0  First version                  (José Luis Cebrián)
''
'' ──────────────────────────────────────────────────────────────────────────────────
'' This code is in the public domain. Feel free to use it in any way you like.

CON

        ' 80Mhz is *required* in order to output pixels fast enough

        _CLKMODE                = xtal1 + pll16x
        _XINFREQ                = 5_000_000

        ' Border size (192 + both borders should equal 244)

        BorderTop               = 30
        BorderBottom            = 22

        ' Border offset, to center the image (positive values move the screen to the right)

        BorderOffset            = 9

        ' Video Generator Configuration for NTSC output on Hydra
        '
        ' • VMode          - Video mode (10 for Composite Mode 1, baseband on pins 0-3)
        ' • CMode          - Two (0) colour mode
        ' • Chroma1        - 1 to enable chroma on broadcast signal
        ' • Chroma0        - 1 to enable chroma on baseband signal
        ' • AuralSub - Used for audio generation (not used)
        ' • VGroup         - Select pin group (011 for pins 24 to 31)
        ' • Pins           - Select pin mask  (3 lower bits on, Hydra DAC is on pins 24 to 26)


                                       '  ┌─────────────────────────────────────────  VMode
                                       '  │  ┌──────────────────────────────────────  CMode
                                       '  │  │ ┌────────────────────────────────────  Chroma1
                                       '  │  │ │ ┌──────────────────────────────────  Chroma0
                                       '  │  │ │ │ ┌────────────────────────────────  AuralSub
                                       '  │  │ │ │ │               ┌────────────────  VGroup
                                       '  │  │ │ │ │               │     ┌──────────  Pins
        VCFG_NTSC               =      %0_11_0_1_1_000_00000000000_101_0_01110000

        ' Port mask (Hydra DAC on pins 24 to 26)

        PortMask                =      %0000_0000_0000_0000_0111_0000_0000_0001

        ' Counter Module Configuration

        ' • CTRMode        - Operating mode (0001 for Video Mode)
        ' • PLLDiv         - Divisor for the VCO frequency (111: use VCO value as-is)

                                       '┌───────────  CTRMode
                                       '│     ┌─────  PLLDiv
        CTRA_NTSC               =      %00001_111

        ' NTSC Color frequency in Hz
        '
        ' This is the 'base' clock rate for all our NTSC timings. At start, the
        ' driver will program the FRQA register to output at this rate. Our base
        ' clock value is 1/16 of the NTSC clock rate, or approximately 0.01746 µs

        NTSC_ClockFreq          =      3_579_545

        ' NTSC Timings table
        '
        '                                               Time        Clocks      Output
        '               Total horizontal timing:        63.5 µs       3637
        '                 Horizontal blanking period:   10.9 µs        624
        '                   Front porch:                 1.5 µs         86 *    Black ($02)
        '                   Synchronizing pulse:         4.7 µs        269 *    Blank ($00)
        '                   Back porch:                  4.7 µs        269
        '                     Breeze away:               0.6 µs         34 *    Black ($02)
        '                     Colour burst:              2.5 µs        143 *    Y Hue ($8A)
        '                     Wait to data:              1.6 µs         92 *    Black ($02)
        '                 Visible line                  52.6 µs       3008
        '                   Left border                  3.8 µs        224 *    Black ($00)
        '                   Pixel data                  44.7 µs       2560
        '                     Character (x32)            1.4 µs         80 *    Data
        '                   Right border                 4.1 µs        224 *    Black ($00)
        '                 Half visible line ¹           20.8 µs       1192
        '
        ' Lines marked with * are the actual parts of a visible line as sent to the TV.
        '
        ' ¹ Note that NTSC requires 242.5 lines per frame. The remaining line (the "half line")
        '   should have a length of 3637/2 = 1818 clocks (that is, a 624-clocks HSync followed
        '   by about 1194 clocks of visible data. The value used here has been fine-tuned a bit.

        VSCL_FrontPorch                         =       86
        VSCL_SynchronizingPulse                 =       269
        VSCL_BackPorch                          =       34+143
        VSCL_BreezeAway                         =       34
        VSCL_ColourBurst                        =       143
        VSCL_WaitToData                         =       92
        VSCL_VisibleLine                        =       3008
        VSCL_HalfLine                           =       1192                    ' NTSC Half line
        VSCL_PixelData                          =       2560
        VSCL_LeftBorder                         =       224 + BorderOffset
        VSCL_RightBorder                        =       224 - BorderOffset
        VSCL_Character                          =       (10 << 12) +   80     ' Eight pixels

VAR
        ' Parameter block for the assembler code
        long          gScreenPTR
        byte          cog

PUB Start(pScreen)
'' Starts the TV driver and begins the output of NTSC video.
'' Uses a Cog.
''
'' Parameters:
''     pScreen         → Base address of the tile map

  gScreenPtr := pScreen
  cog := cognew(@Entry, @gScreenPTR)

pub Stop
  cogstop(cog)

DAT

              org       $000

Entry         jmp       #StartDriver

        ' ─────────────────────────────────────────
        '  Data section
        ' ─────────────────────────────────────────

              ' Colors used in waitvid

              COLOR_SYNC                        long $00
              COLOR_BLACK                       long $02
              COLOR_YHUE                        long $8A
              COLOR_BORDER                      long $02

              PIXEL_DATA                        long %11111111
                                                long %11111111
                                                long %11000011
                                                long %11000011
                                                long %11000011
                                                long %11000011
                                                long %11111111
                                                long %11111111

              ' The following constants are too big to use in-place, so we need to
              ' reserve some registers to put them here

              _ScreenSize                       long 792                       ' 792 Bytes of tilemap data
              _VCFG_NTSC                        long VCFG_NTSC
              _PortMask                         long PortMask
              _NTSC_ClockFreq                   long NTSC_ClockFreq
              _VSCL_Character                   long VSCL_Character
              _VSCL_VisibleLine                 long VSCL_VisibleLine
              _VSCL_PixelData                   long VSCL_PixelData
              _VSCL_HalfLine                    long VSCL_HalfLine

              ' 16 color palette table

              PALETTE           long $00005B5B, $00008D8D, $0000ABAB, $00000B0B, $00000B0B, $00002A2A, $00000202
                                long $0000045B, $0000048D, $000004AB, $0000040B, $0000040B, $0000042A, $00000402
        ' ─────────────────────────────────────────
        '  Code section
        ' ─────────────────────────────────────────

StartDriver

        ' Configure the Cog generators

              mov       VCFG, _VCFG_NTSC                ' Configure the Video Generator
              or        DIRA, _PortMask                 ' Setup the port mask for Hydra DAC access
              movi      CTRA, #CTRA_NTSC                ' Setup the Counter Module Generator A

              mov       R1, _NTSC_ClockFreq             ' R1 := NTSC Clock Frequency in Hz
              rdlong    R2, #0                          ' R2 := Current CPU Clock Frequency in Hz
              call      #Divide                         ' R3 := R1÷R2 (fractional part)
              mov       FRQA, R3                        ' Setup the Counter Module Generator frequency

        ' Frame loop

:Frame
              mov       LineCounter, #244               ' LineCounter := Number of vertical lines
              mov       YCoord, #0                      ' YCoord := Row number in graphics memory, if visible
              mov       CharacterRows, #0

              mov       CurrRow, #1

        '     Copy the parameter block to local variables in Cog memory

              mov       R0, PAR
              rdlong    AttribPtr, R0
              mov       TilePtr, AttribPtr
              add       TilePtr, _ScreenSize

              rdword    R3, TilePtr                     ' R3 := Cursor location
              mov       CursorX, R3
              mov       CursorY, CursorX
              shr       CursorX, #8
              and       CursorY, #$FF

        ' Visible line loop

:ScanLine     call      #HSync

        '     Check if the current line is in the top or bottom border

              cmp       LineCounter, #BorderBottom wc
        if_c  jmp       #:EmptyLine
              cmp       LineCounter, #244-BorderTop wc
        if_nc jmp       #:EmptyLine

        '     Draw a data line

              mov       VSCL, #VSCL_LeftBorder
              waitvid   COLOR_BORDER, #0                ' Output the left border

        '     Character rendering loop

              mov       VSCL, _VSCL_Character
              mov       R0, #32                         ' R0 := Character counter
              mov       R1, AttribPtr                   ' R1 := Pointer to attribute area
:Character    rdbyte    R2, R1                          ' R2 := Tile description long palettes
              cmp       CursorX, R0 wz
        if_z  cmp       CursorY, CurrRow wz
        if_z  add       R2, #$07
              add       R2, #PALETTE                    ' R3 := Address in COG ram of palette entry to use
              movs      :PalSet, R2                     ' Set the next statement to fetch color data
              add       R1, #1                          ' Advance the attribute pointer for the next character
              mov       R4, #PIXEL_DATA
              add       R4, CharacterRows
              movs      :PixSet, R4
:PalSet       mov       COLORS, 0                       ' Load color data
:PixSet       mov       PIXELS, 0
              waitvid   COLORS, PIXELS                  ' Output our video
              djnz      R0, #:Character                 ' and loop

              mov       VSCL, #VSCL_RightBorder
              waitvid   COLOR_BORDER, #0                ' Output the right border

         '     Calculate the address of the next attribute line

              add       CharacterRows, #1
              cmp       CharacterRows, #8 wz
        if_z  mov       CharacterRows, #0               ' Advance to the next line of attributes if the sub-character counter
        if_z  add       AttribPtr, #33                  ' reaches the end of character, and reset it to 8
        if_z  add       CurrRow, #1
              djnz      LineCounter, #:ScanLine         ' Next line (note that here LineCounter is always > 0)

        '     Empty (border only) lines

:EmptyLine    mov       VSCL, _VSCL_VisibleLine         ' Output an entire visible line of BORDER color
              waitvid   COLOR_BORDER, #0
              djnz      LineCounter, #:ScanLine         ' Next line: note that this may be the last one

        '     Output an extra empty half line to correctly output 262.5 lines

              call      #HSync
              mov       VSCL, _VSCL_HalfLine
              waitvid   COLOR_BORDER, #0

         '     VSync

              call      #VSyncHigh                      '   VSync procedure:    6 lines of HSync-only values
              call      #VSyncLow                       '                       6 lines inverted from the previous ones
              call      #VSyncHigh                      '                       6 lines more of HSync-only values

              jmp       #:Frame                         ' Next frame

        ' ─────────────────────────────────────────
        '  Synchronization subroutines
        ' ─────────────────────────────────────────

HSync         mov       VSCL, #VSCL_FrontPorch
              waitvid   COLOR_BLACK, #0
              mov       VSCL, #VSCL_SynchronizingPulse
              waitvid   COLOR_SYNC, #0
              mov       VSCL, #VSCL_BreezeAway
              waitvid   COLOR_BLACK, #0
              mov       VSCL, #VSCL_ColourBurst
              waitvid   COLOR_YHUE, #0
              mov       VSCL, #VSCL_WaitToData
              waitvid   COLOR_BLACK, #0
HSync_Ret     ret

VSyncHigh     mov       R0, #6
:Loop         mov       VSCL, #VSCL_FrontPorch
              waitvid   COLOR_BLACK, #0
              mov       VSCL, #VSCL_SynchronizingPulse
              waitvid   COLOR_SYNC, #0
              mov       VSCL, #VSCL_BackPorch           ' BackPorch = BreezeAway + ColourBurst + WaitToData
              waitvid   COLOR_BLACK, #0
              mov       VSCL, _VSCL_VisibleLine
              waitvid   COLOR_BLACK, #0
              djnz      R0, #:Loop
VSyncHigh_Ret ret

VSyncLow      mov       R0, #6
:Loop         mov       VSCL, #VSCL_FrontPorch
              waitvid   COLOR_SYNC, #0
              mov       VSCL, #VSCL_SynchronizingPulse
              waitvid   COLOR_BLACK, #0
              mov       VSCL, #VSCL_BackPorch           ' BackPorch = BreezeAway + ColourBurst + WaitToData
              waitvid   COLOR_SYNC, #0
              mov       VSCL, _VSCL_VisibleLine
              waitvid   COLOR_SYNC, #0
              djnz      R0, #:Loop
VSyncLow_Ret  ret

        ' ─────────────────────────────────────────
        '  Utility subroutines
        ' ─────────────────────────────────────────

        ' Divide R1 by R2 and return the result in R3 with 32 bits of decimal precision
        ' Input:  R1 → Dividend
        '         R2 → Divisor (it is required that R1 < R2)
        ' Output: R3 → (R1/R2) << 32

Divide        mov       R0, #33
:Loop         cmpsub    R1, R2 wc
              rcl       R3, #1
              shl       R1, #1
              djnz      R0, #:Loop
Divide_Ret    ret


        ' ─────────────────────────────────────────
        '  Uninitialized data
        ' ─────────────────────────────────────────

              R0                                res  1
              R1                                res  1
              R2                                res  1
              R3                                res  1
              R4                                res  1
              YCoord                            res  1

              COLORS                            res  1                  ' Used in the inner loop
              PIXELS                            res  1

              LineCounter                       res  1
              CharacterRows                     res  1
              TilePtr                           res  1
              AttribPtr                         res  1
              ScreenPtr                         res  1

              CurrRow                           res  1

              PAddr                             res  1
              PCtr                              res  1

              CursorX                           res  1
              CursorY                           res  1
