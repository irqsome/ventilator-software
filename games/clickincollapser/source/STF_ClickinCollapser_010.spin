'Clickin' Collapser, a Clickomania clone for Hydra, by Spork Frog.
'Version 1.0
'Released to the public domain.

CON
        _CLKMODE                = xtal1 + pll16x
        _XINFREQ                = 5_000_000

    GP_RIGHT  = %00000001
    GP_LEFT   = %00000010
    GP_DOWN   = %00000100
    GP_UP     = %00001000
    GP_START  = %00010000
    GP_SELECT = %00100000
    GP_B      = %01000000
    GP_A      = %10000000

    
  JOY_CLK = 16
  JOY_LCH = 17
  JOY_DATAOUT0 = 19
  JOY_DATAOUT1 = 18

'┌──────────────────────────────────────────┐
'│         Format "Building Blocks"         │
'└──────────────────────────────────────────┘
'             III ZZZ GGG P S FFFFFF BBBBB
  CHAR2    =  %000_000_000_0_0_000010_00000     'Fixed Width (size includes sign and special symbols)
  CHAR3    =  %000_000_000_0_0_000011_00000
  CHAR4    =  %000_000_000_0_0_000100_00000
  CHAR5    =  %000_000_000_0_0_000101_00000
  CHAR6    =  %000_000_000_0_0_000110_00000
  CHAR7    =  %000_000_000_0_0_000111_00000
  CHAR8    =  %000_000_000_0_0_001000_00000
  CHAR9    =  %000_000_000_0_0_001001_00000
  CHAR10   =  %000_000_000_0_0_001010_00000
  CHAR11   =  %000_000_000_0_0_001011_00000
  CHAR12   =  %000_000_000_0_0_001100_00000
  CHAR13   =  %000_000_000_0_0_001101_00000
  CHAR14   =  %000_000_000_0_0_001110_00000
  CHAR15   =  %000_000_000_0_0_001111_00000
  CHAR16   =  %000_000_000_0_0_010000_00000
  CHAR17   =  %000_000_000_0_0_010001_00000
  CHAR18   =  %000_000_000_0_0_010010_00000
  CHAR19   =  %000_000_000_0_0_010011_00000
  CHAR20   =  %000_000_000_0_0_010100_00000
  CHAR21   =  %000_000_000_0_0_010101_00000
  CHAR22   =  %000_000_000_0_0_010110_00000
  CHAR23   =  %000_000_000_0_0_010111_00000
  CHAR24   =  %000_000_000_0_0_011000_00000
  CHAR25   =  %000_000_000_0_0_011001_00000
  CHAR26   =  %000_000_000_0_0_011010_00000
  CHAR27   =  %000_000_000_0_0_011011_00000
  CHAR28   =  %000_000_000_0_0_011100_00000
  CHAR29   =  %000_000_000_0_0_011101_00000
  CHAR30   =  %000_000_000_0_0_011110_00000
  CHAR31   =  %000_000_000_0_0_011111_00000
  CHAR32   =  %000_000_000_0_0_100000_00000
  CHAR33   =  %000_000_000_0_0_100001_00000
  CHAR34   =  %000_000_000_0_0_100010_00000
  CHAR35   =  %000_000_000_0_0_100011_00000
  CHAR36   =  %000_000_000_0_0_100100_00000
  CHAR37   =  %000_000_000_0_0_100101_00000
  CHAR38   =  %000_000_000_0_0_100110_00000
  CHAR39   =  %000_000_000_0_0_100111_00000
  CHAR40   =  %000_000_000_0_0_101000_00000
  CHAR41   =  %000_000_000_0_0_101001_00000
  CHAR42   =  %000_000_000_0_0_101010_00000
  CHAR43   =  %000_000_000_0_0_101011_00000
  CHAR44   =  %000_000_000_0_0_101100_00000
  CHAR45   =  %000_000_000_0_0_101101_00000
  CHAR46   =  %000_000_000_0_0_101110_00000
  CHAR47   =  %000_000_000_0_0_101111_00000
  CHAR48   =  %000_000_000_0_0_110000_00000
  CHAR49   =  %000_000_000_0_0_110001_00000

  SPCPAD   =  %000_000_000_0_1_000000_00000     'Space padded

  PLUS     =  %000_000_000_1_0_000000_00000     'Show plus sign '+' for num > 0

  COMMA    =  %000_000_001_0_0_000000_00000     'Comma delimiter
  USCORE   =  %000_000_010_0_0_000000_00000     'Underscore delimiter

  HEXCHAR  =  %011_000_000_0_0_000000_00000     'Hexadecimal prefix '$'
  BINCHAR  =  %100_000_000_0_0_000000_00000     'Binary prefix '%'

  GROUP2   =  %000_001_000_0_0_000000_00000     'Group digits
  GROUP3   =  %000_010_000_0_0_000000_00000
  GROUP4   =  %000_011_000_0_0_000000_00000
  GROUP5   =  %000_100_000_0_0_000000_00000
  GROUP6   =  %000_101_000_0_0_000000_00000
  GROUP7   =  %000_110_000_0_0_000000_00000
  GROUP8   =  %000_111_000_0_0_000000_00000


'┌──────────────────────────────────────────┐
'│        Common Decimal Formatters         │
'└──────────────────────────────────────────┘

  DEC      =  %000_000_000_0_0_000000_01010     'Decimal, variable widths

  DDEC     =  DEC+GROUP3+COMMA                  'Decimal, variable widths, delimited with commas

  DEC2     =  DEC+CHAR2                         'Decimal, fixed widths, zero padded
  DEC3     =  DEC+CHAR3
  DEC4     =  DEC+CHAR4
  DEC5     =  DEC+CHAR5
  DEC6     =  DEC+CHAR6
  DEC7     =  DEC+CHAR7
  DEC8     =  DEC+CHAR8
  DEC9     =  DEC+CHAR9
  DEC10    =  DEC+CHAR10
  DEC11    =  DEC+CHAR11

  SDEC3    =  DEC3+SPCPAD                       'Decimal, fixed widths, space padded
  SDEC4    =  DEC4+SPCPAD
  SDEC5    =  DEC5+SPCPAD
  SDEC6    =  DEC6+SPCPAD
  SDEC7    =  DEC7+SPCPAD
  SDEC8    =  DEC8+SPCPAD
  SDEC9    =  DEC9+SPCPAD
  SDEC10   =  DEC10+SPCPAD
  SDEC11   =  DEC11+SPCPAD

  DSDEC6   =  SDEC6+GROUP3+COMMA                'Decimal, fixed widths, space padded, delimited with commas
  DSDEC7   =  SDEC7+GROUP3+COMMA
  DSDEC8   =  SDEC8+GROUP3+COMMA
  DSDEC9   =  SDEC9+GROUP3+COMMA
  DSDEC10  =  SDEC10+GROUP3+COMMA
  DSDEC11  =  SDEC11+GROUP3+COMMA
  DSDEC12  =  DEC+CHAR12+SPCPAD+GROUP3+COMMA
  DSDEC13  =  DEC+CHAR13+SPCPAD+GROUP3+COMMA
  DSDEC14  =  DEC+CHAR14+SPCPAD+GROUP3+COMMA

obj
  tv: "STF_Color_Tiles"
  rnd: "RealRandom"
  splash: "STF_ClickinCollapser_Splash"
  mouse: "SNESmouse"

  numstr: "Numbers"

var
  long x, y
  long nes
  word looking[792]
  word where
  byte farright
  byte input
  byte mouseb
  long mousex, mousey
  long mousedx, mousedy
  long totscore

pub Main
  numstr.Init
  repeat
    Start
    mouse.stop
    tv.Stop
    rnd.stop

pub Start
  mouse.start(JOY_CLK,JOY_LCH,JOY_DATAOUT0,JOY_DATAOUT1,60,2)
  splash.start  
  nes := readnes
  mouseb := mouse.buttons & %11
  input := 0
  repeat while not input
    nes := readnes & %10010000
    mouseb := (mouse.buttons & %11)
    if mouseb or nes
      input := 1
  repeat while (readnes & %10011111) or (mouse.buttons & %11)
  splash.stop

  rnd.start

  farright := 30

  repeat x from 0 to 791
    byte[@attr + x] := (rnd.random & %11)
  repeat x from 31 to 792 step 33
    byte[@attr + x] := $06
    byte[@attr + x + 1] := $06

  repeat x from 0 to 23
    byte[@attr + 31 + (x * 33)] := $06

  x := 16
  y := 12
  mousex := 16 << 4
  mousey := 12 << 4
  nes := 0
  SetCursor(x, y)

  tv.start(@attr)

  repeat
    nes := readnes
    mouseb := mouse.buttons & %11
    input := 0
    repeat while not input
      nes := readnes & %10011111
      mouseb := (mouse.buttons & %11)
      mousedx := mouse.delta_x
      mousedy := mouse.delta_y
      if mouseb or nes or mousedx or mousedy
        input := 1
    if (nes & GP_RIGHT)
      x -= 1
    if (nes & GP_LEFT)
      x += 1
    if (nes & GP_UP)
      y -= 1
    if (nes & GP_DOWN)
      y += 1
    if mousedx
      mousex := mousex - mousedx
      if mousex > 512
        mousex := 512
      if mousex < 1
        mousex := 1
      x := mousex >> 4
    if mousedy
      mousey := mousey - mousedy
      if mousey > 384
        mousey := 384
      if mousey < 1
        mousey := 1
      y := mousey >> 4
    if y < 1
      y := 1
    if y > 24
      y := 24
    if x < 2
      x := 2
    if x > 32
      x := 32
    if (nes & GP_A) or mouseb
      totscore += ClearSection(x, y)
      DropBlocks
      ShiftLeft
      if not CheckEnd
        DispScore
        waitcnt(400_000_000 + cnt)
        return
    SetCursor(x, y)
    repeat while (readnes & %10001111) or (mouse.buttons & %11)


pub SetCursor(ax, ay)
  word[@cursor] := (ax << 8) | ay
  return

pub ClearSection(bx, by) | color, addr, baseaddr, score
  addr := @attr + (32 - x) + ((y - 1) * 33)
  score := 0
  baseaddr := addr
  color := byte[addr]
  if not (HasNeighbors(addr, color, bx)) or (color == $06)
    return 0
  'byte[addr] := $06
  ClearList
  looking[1] := addr
  where := 2
  repeat while (where <> 0)'(HasNeighbors(baseaddr, color))
    if ((byte[addr + 33] == color) and (not (InList(addr + 33)))) and ((addr + 33) < @cursor)
      addr += 33
      looking[where] := addr
      where += 1
    elseif ((byte[addr - 1] == color) and (not (InList(addr - 1))))
      addr -= 1
      looking[where] := addr
      where += 1
    elseif ((byte[addr - 33] == color) and (not (InList(addr - 33)))) and ((addr - 33) => @attr)
      addr -= 33
      looking[where] := addr
      where += 1
    elseif ((byte[addr + 1] == color) and (not (InList(addr + 1))))
      addr += 1
      looking[where] := addr
      where += 1
    else
      byte[addr] := $06
      where --
      score += 1
      addr := looking[where]
  byte[baseaddr] := $06
  return ((score - 3) * (score - 3))

pub HasNeighbors(addra, c, xd)
  if (byte[addra - 33] == c) or ((byte[addra + 33] == c) and ((addra + 33) < @cursor)) or (byte[addra - 1] == c) or (byte[addra + 1] == c)
    return 1
  return 0

pub InList(addrf) | ed
  repeat ed from 0 to where
    if addrf == looking[ed]
      return 1
  return 0

pub ClearList | as
  repeat as from 0 to 791
    looking[as] := $00_00

pub DropBlocks | xa, ya, za
  repeat xa from 0 to (farright + 1)
    repeat ya from 23 to 1
      if not SomeAbove(xa, ya)
        quit
      if byte[@attr + xa + (ya * 33)] == $06
        repeat za from (ya - 1) to 0
          if byte[@attr + xa + (za * 33)] < $06
            byte[@attr + xa + (ya * 33)] :=  byte[@attr + xa + (za * 33)]
            byte[@attr + xa + (za * 33)] := $06
            quit

pub SomeAbove(cxc, cyc)
  repeat cyc from cyc to 0
    if byte[@attr + cxc + (cyc * 33)] <> $06
      return 1
  return 0

pub ShiftLeft | xc, xcb, yc, rec, movas
  movas := 1
  repeat while movas
    movas := 0
    repeat xc from 0 to farright
      rec := 0
      repeat yc from 0 to 23
        if byte[@attr + xc + (yc * 33)] == $06
          rec += 1
      if rec == 24
        movas := 1
        farright -= 1
        repeat xcb from xc to 30
          repeat yc from 0 to 23
            byte[@attr + xcb + (yc * 33)] := byte[@attr + xcb + 1 + (yc * 33)]

pub CheckEnd | axa, aya
  repeat axa from 0 to (farright + 1)
    repeat aya from 23 to 0
      if (HasNeighbors(@attr + axa + (aya * 33), byte[@attr + axa + (aya * 33)], 0)) and (byte[@attr + axa + (aya * 33)] <> $06)
        return 1
  return 0

pub DispScore | bxb, byb, bzb, strptra, threebit
  strptra := numstr.ToStr(totscore, DEC7) + 1
  repeat bxb from 0 to 5
    threebit := word[((byte[strptra + bxb] - 48) << 1) + @numbers]
    threebit <<= 1
    repeat byb from 0 to 5
      repeat bzb from 0 to 2
        if (threebit & %1000000000000000)
          byte[@attr + (4 * bxb) + bzb + (byb * 33)] := $00
        threebit <<= 1

pub readnes

return mouse.padstate >< 8

dat

attr

byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
byte $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00

'             X  Y
cursor  word $00_00

numbers word %0111101101101111 '0
        word %0010010010010010 '1
        word %0111001111100111 '2
        word %0111001011001111 '3
        word %0101101111001001 '4
        word %0111100111001111 '5
        word %0111100111101111 '6
        word %0111001001001001 '7
        word %0111101111101111 '8
        word %0111101111001111 '9