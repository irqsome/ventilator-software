mkdir -p sdbins
cd source
rm ../sdbins/*
bstc -Ocg -e -o ../sdbins/RANQUEST ranquest.spin
mv ../sdbins/RANQUEST.eeprom ../sdbins/RANQUEST.BIN
bstc -Ocg -e -o ../sdbins/RANQ ranq_w.spin
mv ../sdbins/RANQ.eeprom ../sdbins/RANQ.WDT
cp ./stuff/* ../sdbins/
cd ..