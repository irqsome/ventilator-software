Mapper for Ranquest

On the main part of screen is the map screen that
can be modifed. Any modification made is automatically
saved to disk. 

The bottom of the screen has the graphics tiles
you can choose from to place on the map. To cycle
through the different tiles, hit the box marked
"Sw". There is no indicator to mark which tile is
currently selected.

To change which graphic tile set is displayed for
that map, hit the "Tile Set" button. This will
cycle between the 3 graphic tile sets.

The upper right hand corner shows the world of the
entire game. The white box shows which screen map
is currently on screen to edit. The blue box shows 
which screen the player will warp to if they hit a 
door tile. To change the screen the player will warp 
to, hit the "Warp" button and select a map location on 
the world map. If the mouse is clicked outside the 
world map, the operation will be cancelled. Every 
map has a warp location, even if there is no door.

When a player warps to a new screen, they have a tile
marked where that player will start. On the screen map
this is indicated by a gray box marked "PL". Under this
icon will also be a normal graphics tile for the map. To
change the starting location, hit the button marked "Player"
and click on a location in the screen map. Every screen
will have a player start location.

To place a treasure chest on the screen, click on the button
marked "Treasure" and then click on a location on the screen
map of where the treasure will show up. The treasure will only
show up in the game when that screen is marked to have a treasure
chest. Every screen will have a treasure chest location. The 
location of the chest is marked on the screen map with a gray
box marked "TR".

On the right hand side under the world map there are two check
boxes. The check box marked Trsre will indicate if a treasure
chest can appear on the screen. Some screens have this unchecked
so a chest can never appear on the screen (such as the castle
where you start, the Inns, and a few other places). The Enemy
check box will indicate if the screen can have enemies on it or
not (example would be on the start screen, we don't want enemies
on it, nor at the Inns).

The data files from the game to use the mapper are:
ranq1.til, ranq2.til, ranq3.til, tiles1.dat, tiles2.dat, 
tiles3.dat, and ranquest.map

The only file that is modified by the mapper is ranquest.map.
After changes have been made, this file can be copied back
onto the SD card to use in the game.