'' Renderer for 8 Bit sprite/tile driver
'' JT Cook
''
'' Specs:
'' Tilemap of 32x24 tiles
'' 8 Sprites per screen
''  can be scrolled off screen on the Y axis (Sprite Y = 16 is the same as Screen Y = 0)
''  Sprites can be mirrored/flippd on the X and Y axis
CON
  SCANLINE_BUFFER = $7F00
  Last_Scanline = 191 ''final scanline for a frame
  sprite_adrs            = SCANLINE_BUFFER-20 'Address of where sprites are stored
  sprite_x_adr           = SCANLINE_BUFFER-24 'X location of 8 sprites
  sprite_y_adr           = SCANLINE_BUFFER-32 'Y location of 8 sprites
  sprite_image           = SCANLINE_BUFFER-40 'address of images of 8 sprites
  sprite_x_flip          = SCANLINE_BUFFER-48 'flag to flip sprite on x axis
  sprite_y_flip          = SCANLINE_BUFFER-49 'flag to flip sprite on y axis

PUB start(param)
'' Start Rendering Engine
  cognew(@Entry, param)

PUB Return_Address ''used to get address where assembly code is so we can re-purpose space
    return(@Entry)

DAT
        org
Entry    
        mov Temp_Adr, Par  ''read parameter
        rdlong cognumber, Temp_Adr ''grab which COG number it is
        add Temp_Adr, #4   '' add 4 to address for total # of cogs
        rdlong cogtotal, Temp_Adr ''grab total number of rendering COGs
        
        rdlong Tile_Map_Adr, tilemap_adr ''read address of tile map
'        rdlong Tiles_Adr, tile_adr ''read address of where tiles are at

''Main loop for renderer
Main_Loop
''wait until we hit scanline 0 so we can start with a fresh frame
        mov currentscanline, cognumber ''reset current scanline for COG
        rdlong Tiles_Adr, tile_adr ''read address of where tile graphics are stored
        rdlong Sprites_Adr, sprite_adr
        rdlong obpp_tile_adrs, obp_adr ''read address for 1bpp        
          ''read this each frame incase address changes 
:waitloop
        rdlong currentrequest, request_scanline wz
if_nz   jmp #:waitloop
Render_Tiles
        movd :write_scanline, #scanbuffer   'location of destination of local scanline buffer
                
        mov Buffer_Number, #0 ''reset scanline buffer location
        ''set character rendering attributes
        mov Temp_Toggle, #0 ''this is used to toggle left or right tile grab
        mov Temp_Row, currentscanline ''grab current scanline(for character row)
        and Temp_Row, #7   ''mask off all but 7 bits to get current tile row
        mov Temp_Row2, Temp_Row ''copy row info for 1bpp tiles        
        shl Temp_Row, #3   ''multiply by 8 (each row is 8 bytes)
        ''mov Temp_Col, #0   ''current column we are going to render
        ''grab start position for tile map
        mov Temp_Adr2, currentscanline ''grab current scanline(for character map)

        shr Temp_Adr2, #3    ''divide by 8(each row is 8 pixels)
        shl Temp_Adr2, #5    ''then multiply by 32 (32 tiles per row)        
        add Temp_Adr2, Tile_Map_Adr ''grab tilemap address
        mov Temp_1bpp, #0 're-purpose for 1bpp tile rendering
:Render_Four
        cmp Temp_Toggle, #1 wz ''check to see if we need new address or not
        if_e jmp #:Finish_4   ''process last 4 pixels
        rdbyte Temp_Adr, Temp_Adr2 ''grab character number
        ''if greater than 127, grab from 1 bit characters
        cmp Temp_Adr, #127 wz,wc
        if_a jmp #:OBPP_Character ''if > 127, render 1bpp characters
        shl Temp_Adr, #6 ''multiply by 64(each char is 64 bytes)
        add Temp_Adr, Temp_Row   ''grab exact tile row address
        add Temp_Adr, Tiles_Adr  '' add address for current tile
        rdlong Four_Pixels, Temp_Adr ''copy tile pixels over to background
'write pixels to internal scanline buffer
:write_scanline
        mov scanbuffer, Four_Pixels ''write Four Pixel buffer to scanline buffer  
        add :write_scanline, destination_increment  ''move to next index in scanline buffer
        xor Temp_Toggle, #1 ''toggle switch        
        add Buffer_Number, #1            ''add location to scanline buffer
        cmp Buffer_Number, #64 wz,wc     ''if below 64, keep looping
        if_b jmp #:Render_Four           ''keep drawing pixels
'        jmp #scanline_finished            ''finish scanline, copy to TV
        jmp #Start_Sprites             ''finish scanline for tiles, now render sprites
'''''''''''''''''
'' Finish up rendering the last 4 pixels        
:Finish_4
   add Temp_Adr2, #1   ''ready next character to grab
   cmp Temp_1bpp, #1 wz,wc
   if_e jmp #:Finish_4_1bpp ''if > 127, render 1bpp characters
   add Temp_Adr, #4 'grab next four bytes
   rdlong Four_Pixels, Temp_Adr ''copy tile pixels over to background
   jmp #:write_scanline   
''''''''''''''''''''''''''
'' Finish up rendering the last 4 pixels for 1pp character
:Finish_4_1bpp
   and Single_Pixel,#$0F        
   shl Single_Pixel, #4   ''toggle which 4 pixels to grab
   mov Temp_1bpp, #0 'clear that it is a 1bpp tile
   jmp #:render_1bpp
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'' Render One bit per pixel tiles
:OBPP_Character
''onebpp_tile_adrs
        sub Temp_Adr, #128 'get to the 8bit characters
        shl Temp_Adr, #3 ''multiply by 8(each char is 8 bytes)
        add Temp_Adr, Temp_Row2   ''grab exact tile row address
        add Temp_Adr, obpp_tile_adrs ''get correct address
        mov Temp_1bpp, #1 'mark that it is a 1bpp tile        
        rdbyte Single_Pixel, Temp_Adr ''grab character
{        
        cmp Temp_Toggle, #1 wz,wc ''check to see if we need new address or not
        if_e and Single_Pixel,#$0F        
        if_e shl Single_Pixel, #4   ''toggle which 4 pixels to grab
        if_e add Temp_Adr2, #1   ''ready next character to grab
}        
:render_1bpp
        mov Four_Pixels, #0 ''clear 4 pixel buffer
        ''pixel 1
        test Single_Pixel, #$10 wz
        if_ne or Four_Pixels, #$07
        if_e or Four_Pixels, #$02
        rol Four_Pixels, #8 ''move all pixels left
        ''pixel 2        
        test Single_Pixel, #$20 wz       
        if_ne or Four_Pixels, #$07
        if_e or Four_Pixels, #$02                        
        rol Four_Pixels, #8 ''move all pixels left
        ''pixel 3
        test Single_Pixel, #$40 wz       
        if_ne or Four_Pixels, #$07
        if_e or Four_Pixels, #$02        
        rol Four_Pixels, #8 ''move all pixels left
        ''pixel 4        
        test Single_Pixel, #$80 wz
        if_ne or Four_Pixels, #$07
        if_e or Four_Pixels, #$02
'        xor Temp_Toggle, #1 ''toggle switch for half character
        jmp #:write_scanline        
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'' Render sprites        
Start_Sprites
        MOV Sprite_Number, #8 ''set number of sprites
        ADD currentscanline, #16 ''add 16 so we can scroll sprite off screen (SpriteY 16 = Screen Pixel 0)
Render_Sprites
        SUB Sprite_Number, #1 ''get current sprite number
        mov Buffer_Number,#0 ''reset number of times this loops for scanline drawer
''First check Y coordinate of sprite to see if it is even visable
        mov Temp_Var, sprite_y  ''grab y location of 1st sprite
        SUB Temp_Var, Sprite_Number ''find current sprite
        rdbyte Temp_Row,Temp_Var  ''grab y location of 1st sprite
        cmp currentscanline, Temp_Row wz, wc''check current scanline
        if_b jmp #sprite_check ''sprite is not on this line, skip it
        MOV Temp_Adr,currentscanline ''find scanline of sprite we are working on
        SUB Temp_Adr, Temp_Row       ''subtract current Y location to get the correct scanline of sprite graphic        
        ''check sprite flipping on Y axis
        rdbyte Temp_Var, sprite_yflip ''read byte to toggle sprite y flip
        ROR Temp_Var, Sprite_Number ''find correct byte for sprite
        AND Temp_Var, #1 wz           ''check to see if we flip sprite
        if_z jmp #:Fin_Adr            ''if we don't flip the sprite, skip that process
        MOV Temp_Var, #15 ''setup for sprite Y flip
        SUB Temp_Var, Temp_Adr ''find new scanline
        MOV Temp_Adr, Temp_Var
:Fin_Adr
        shl Temp_Adr, #4 ''*16 because each row is 16 pixels
        add Temp_Row, #15  ''since sprite is 16 pixels tall, add that and recheck bounderies
        cmp currentscanline, Temp_Row wz, wc''check current scanline
        if_a jmp #sprite_check ''sprite is still not on this line, skip it

''Now check X coordinates of sprite
        mov Temp_Var, sprite_x ''grab x location for 1st sprite
        sub Temp_Var, Sprite_Number ''grab x location for current sprite
        rdbyte Temp_Var, Temp_Var  ''grab x location of 1st sprite(resuse the variable since we won't need it anymore)
        mov Temp_Var_1, Temp_Var       ''copy x location of 1st sprite
        SHR Temp_Var, #2               ''divide by 4 (since we write pixels 4 bytes at a time)
        ''we will write to both normal and x-mirrored destinations because a cmp would take the same amount of time
        movd write_scanline, #scanbuffer   'location of destination of local scanline buffer
        movd write_scanline_mirror, #scanbuffer   'location of destination of local scanline buffer
        AND Temp_Var_1, #2 ''grab what we are masking
        ''we will write to both normal and x-mirrored destinations because a cmp would take the same amount of time
        movs read_scanline, #scanbuffer 'location of local scanline buffer that we will read from
        add read_scanline, Temp_Var
        movs read_scanline_mirror, #scanbuffer 'location of local scanline buffer that we will read from
        add read_scanline_mirror, Temp_Var          
        SHL Temp_Var, #9 ''shift for proper destination increment (*512)
        ''we will write to both normal and x-mirrored destinations because a cmp would take the same amount of time
        add write_scanline, Temp_Var  ''move to next index in scanline buffer
        add write_scanline_mirror, Temp_Var  ''move to next index in scanline buffer
        mov Temp_Var, Temp_Var_1 ''copy offset
        ''check to see if we need 4 or 5 chunks of background (will still work with 5, maybe delete this)
        mov Temp_Col, #4
        cmp Temp_Var, #0 wz,wc
        if_ne add Temp_Col, #1
        ''Use a mask to determine edges of sprites, then shift the mask every pixel
        MOV Temp_Row, #$0F ''setup edge mask, this is used
        SHL Temp_Row, Temp_Var ''clear the edge
        ''Adjust mask for right end of sprite
        MOV Temp_Var, #4
        SUB Temp_Var, Temp_Var_1
        add Temp_Adr, Sprites_Adr ''grab address for sprite graphics

        MOV Temp_Var_2, sprite_num     ''grab address for selected sprite image
        sub Temp_Var_2, Sprite_Number  ''adjust for current sprite
        rdbyte Temp_Var_1, Temp_Var_2  ''grab sprite image number

        SHL Temp_Var_1, #8 ''multiply by 256 since that is the size of each sprite
        add Temp_Adr, Temp_Var_1 ''add the current selected sprite to the graphics address
        rdbyte Temp_Var_2, sprite_xflip ''read byte to toggle sprite x flip
        ROR Temp_Var_2, Sprite_Number ''find correct bit for current sprite
        AND Temp_Var_2, #1 wz
        if_nz jmp #Mirror_Sprite ''if it is mirrored, do that

Normal_Sprite         
 ''Draw sprite, X-Axis not mirrored ------------------------------------------------------------------
        rdlong Sprite_Pixels, Temp_Adr ''grab pixel data
        ROL Sprite_Pixels, #8''ready 1st pixel
        MOV Temp_Var_2, #4 ''this is used for counting pixels and to deside if we need to load next batch

''read background        
''Pixel buffer is messed up -- pixels are mirrored.
'' eg: pixel x locations   [100,101,102,103]  are stored in memory [103,102,101,100]
read_scanline
        mov Four_Pixels, scanbuffer
''write 4 pixels, loop unrolled for speed        
:Pix_4
        'ROR Four_Pixels,#8
        shr Temp_Row, #1 WC ''shift out mask
        if_nc jmp #:Pix_3   ''if mask for this pixel is zero, skip it     
        ROR Sprite_Pixels, #8
        MOV Temp_Var_1, Sprite_Pixels
        ''check to see if we run out of pixels on sprite
        SUB Temp_Var_2, #1 WZ
        if_z call #loadsprite 
        ''check to see if pixel is transparent
        AND Temp_Var_1, #$FF WZ 
        if_z jmp #:Pix_3  ''if pixel is transparent, keep background intact
        AND Four_Pixels, AND_Mask ''Clear the pixel we are going to write to        
        OR Four_Pixels, Temp_Var_1 ''write pixel 
                        
:Pix_3
        ROR Four_Pixels,#8
        shr Temp_Row, #1 WC
        if_nc jmp #:Pix_2        
        ROR Sprite_Pixels, #8
        MOV Temp_Var_1, Sprite_Pixels
        ''check to see if we run out of pixels on sprite
        SUB Temp_Var_2, #1 WZ
        if_z call #loadsprite 
        ''check to see if pixel is transparent
        AND Temp_Var_1, #$FF WZ
        if_z jmp #:Pix_2  ''if pixel is transparent, keep background intact
        AND Four_Pixels, AND_Mask ''Clear the pixel we are going to write to        
        OR Four_Pixels, Temp_Var_1

:Pix_2
        ROR Four_Pixels,#8
        shr Temp_Row, #1 WC
        if_nc jmp #:Pix_1        
        ROR Sprite_Pixels, #8
        MOV Temp_Var_1, Sprite_Pixels
        ''check to see if we run out of pixels on sprite
        SUB Temp_Var_2, #1 WZ
        if_z call #loadsprite 
        ''check to see if pixel is transparent
        AND Temp_Var_1, #$FF WZ
        if_z jmp #:Pix_1  ''if pixel is transparent, keep background intact
        AND Four_Pixels, AND_Mask ''Clear the pixel we are going to write to        
        OR Four_Pixels, Temp_Var_1

:Pix_1
        ROR Four_Pixels,#8
        shr Temp_Row, #1 WC
        if_nc jmp #:Pix_End        
        ROR Sprite_Pixels, #8
        MOV Temp_Var_1, Sprite_Pixels
        ''check to see if we run out of pixels on sprite
        SUB Temp_Var_2, #1 WZ
        if_z call #loadsprite 
        ''check to see if pixel is transparent
        AND Temp_Var_1, #$FF WZ
        if_z jmp #:Pix_End  ''if pixel is transparent, keep background intact
        AND Four_Pixels, AND_Mask ''Clear the pixel we are going to write to        
        OR Four_Pixels, Temp_Var_1
:Pix_End
        ROR Four_Pixels, #8
        mov Temp_Row,#$0F ''reset edge mask
        cmp Buffer_Number, #3 wz, wc ''if we are in the final section, create mask for right edge of sprite
        if_e SHR Temp_Row, Temp_Var ''setup right edge mask

write_scanline
        mov scanbuffer, Four_Pixels ''write Four Pixel buffer to scanline buffer 
        add write_scanline, destination_increment  ''move to next index in scanline buffer
        add Buffer_Number, #1            ''add location to scanline buffer we are writing to
        add read_scanline, #1           ''add location to scanline buffer we are reading from
        cmp Buffer_Number, Temp_Col wz,wc     ''if below 4, keep looping (4 pixel * 4 loops = 16 total pixels)
        if_b jmp #read_scanline           ''keep drawing pixels
sprite_check
        CMP Sprite_Number, #0 wz ''are there any more sprites to draw?
        if_nz jmp #Render_Sprites ''if so work on them
        jmp #scanline_finished            ''finish scanline, copy to TV
        
''load next 4 pixels of sprite                                    
loadsprite
        ADD Temp_Adr, #4 ''grab next set of pixels
        rdlong Sprite_Pixels, Temp_Adr
        mov Temp_Var_2, #4
        ROL Sprite_Pixels, #8 ''Ready 1st pixel        
loadsprite_ret ret                                     
        
Mirror_Sprite
 ''Draw sprite, mirrored on X axis ------------------------------------------------------------------
 ''The main difference is we start grabbing pixels at the right end of the sprite, move the sprite pixel pointer
 ''  left, then ROL sprite buffer instead of ROR(also omit the inital ROR at start of 4 pixel buffer)
        ADD Temp_Adr, #12 ''start at end of sprite(+16) then subtract 4 since we read in longs 
        rdlong Sprite_Pixels, Temp_Adr ''grab pixel data
        ''ROL Sprite_Pixels, #8''ready 1st pixel
        MOV Temp_Var_2, #4 ''this is used for counting pixels and to deside if we need to load next batch

''read background        
''Pixel buffer is messed up -- pixels are mirrored.
'' eg: pixel x locations   [100,101,102,103]  are stored in memory [103,102,101,100]
read_scanline_mirror
        mov Four_Pixels, scanbuffer
''write 4 pixels, loop unrolled for speed        
:Pix_4_mirror
        'ROR Four_Pixels,#8
        shr Temp_Row, #1 WC ''shift out mask
        if_nc jmp #:Pix_3_mirror   ''if mask for this pixel is zero, skip it     
        ROL Sprite_Pixels, #8
        MOV Temp_Var_1, Sprite_Pixels
        ''check to see if we run out of pixels on sprite
        SUB Temp_Var_2, #1 WZ
        if_z call #loadsprite_mirror 
        ''check to see if pixel is transparent
        AND Temp_Var_1, #$FF WZ 
        if_z jmp #:Pix_3_mirror  ''if pixel is transparent, keep background intact
        AND Four_Pixels, AND_Mask ''Clear the pixel we are going to write to        
        OR Four_Pixels, Temp_Var_1 ''write pixel 
                        
:Pix_3_mirror
        ROR Four_Pixels,#8
        shr Temp_Row, #1 WC
        if_nc jmp #:Pix_2_mirror        
        ROL Sprite_Pixels, #8
        MOV Temp_Var_1, Sprite_Pixels
        ''check to see if we run out of pixels on sprite
        SUB Temp_Var_2, #1 WZ
        if_z call #loadsprite_mirror 
        ''check to see if pixel is transparent
        AND Temp_Var_1, #$FF WZ
        if_z jmp #:Pix_2_mirror  ''if pixel is transparent, keep background intact
        AND Four_Pixels, AND_Mask ''Clear the pixel we are going to write to        
        OR Four_Pixels, Temp_Var_1

:Pix_2_mirror
        ROR Four_Pixels,#8
        shr Temp_Row, #1 WC
        if_nc jmp #:Pix_1_mirror        
        ROL Sprite_Pixels, #8
        MOV Temp_Var_1, Sprite_Pixels
        ''check to see if we run out of pixels on sprite
        SUB Temp_Var_2, #1 WZ
        if_z call #loadsprite_mirror 
        ''check to see if pixel is transparent
        AND Temp_Var_1, #$FF WZ
        if_z jmp #:Pix_1_mirror  ''if pixel is transparent, keep background intact
        AND Four_Pixels, AND_Mask ''Clear the pixel we are going to write to        
        OR Four_Pixels, Temp_Var_1

:Pix_1_mirror
        ROR Four_Pixels,#8
        shr Temp_Row, #1 WC
        if_nc jmp #:Pix_End_mirror        
        ROL Sprite_Pixels, #8
        MOV Temp_Var_1, Sprite_Pixels
        ''check to see if we run out of pixels on sprite
        SUB Temp_Var_2, #1 WZ
        if_z call #loadsprite_mirror 
        ''check to see if pixel is transparent
        AND Temp_Var_1, #$FF WZ
        if_z jmp #:Pix_End_mirror  ''if pixel is transparent, keep background intact
        AND Four_Pixels, AND_Mask ''Clear the pixel we are going to write to        
        OR Four_Pixels, Temp_Var_1
:Pix_End_mirror
        ROR Four_Pixels, #8
        mov Temp_Row,#$0F ''reset edge mask
        cmp Buffer_Number, #3 wz, wc ''if we are in the final section, create mask for right edge of sprite
        if_e SHR Temp_Row, Temp_Var ''setup right edge mask


write_scanline_mirror
        mov scanbuffer, Four_Pixels ''write Four Pixel buffer to scanline buffer 
        add write_scanline_mirror, destination_increment  ''move to next index in scanline buffer
        add Buffer_Number, #1            ''add location to scanline buffer we are writing to
        add read_scanline_mirror, #1           ''add location to scanline buffer we are reading from
        cmp Buffer_Number, Temp_Col wz,wc     ''if below 4, keep looping (4 pixel * 4 loops = 16 total pixels)
        if_b jmp #read_scanline_mirror           ''keep drawing pixels
        CMP Sprite_Number, #0 wz ''are there any more sprites to draw?
        if_nz jmp #Render_Sprites ''if so work on them
        jmp #scanline_finished      ''finish scanline, copy to TV

''load next 4 pixels of sprite (Mirrored on X-axis                                    
loadsprite_mirror
        SUB Temp_Adr, #4 ''grab next set of pixels
        rdlong Sprite_Pixels, Temp_Adr
        mov Temp_Var_2, #4
        ''ROL Sprite_Pixels, #8 ''Ready 1st pixel        
loadsprite_mirror_ret ret                                     

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''scanline rendering is finished, wait for request from TV driver
scanline_finished
        SUB currentscanline,#16 ''correct scanline value that we changed during sprite rendering routines                
'' wait until TV requests the scanline we rendered
:wait
        rdlong currentrequest, request_scanline
        cmps currentrequest, currentscanline wz, wc
if_b    jmp #:wait

'' copy scanline
start_tv_copy
        movs :nextcopy, #scanbuffer
        mov Temp_Adr, display_base
        mov Buffer_Number, #64

:nextcopy
        mov Temp_Var,scanbuffer
        add :nextcopy, #1
        wrlong Temp_Var, Temp_Adr
        add Temp_Adr, #4
        djnz Buffer_Number, #:nextcopy                      
scanlinedone
        ' Line is done, increment to the next one this cog will handle                        
        add currentscanline, cogtotal
        cmp currentscanline, #Last_Scanline wc, wz
if_be   jmp #Render_Tiles
        ' The screen is completed, jump back to main loop a wait for next frame                        
        jmp #Main_Loop


           


display_base            long SCANLINE_BUFFER    ''scanline address
request_scanline        long SCANLINE_BUFFER-4  ''next scanline to render
tilemap_adr             long SCANLINE_BUFFER-8  ''address of tile map
tile_adr                long SCANLINE_BUFFER-12 ''address of tiles
obp_adr                 long SCANLINE_BUFFER-50 ''address of 1bpp tiles
sprite_adr              long sprite_adrs ''address for sprite graphics 
sprite_x                long sprite_x_adr ''x locations of 8 sprites
sprite_y                long sprite_y_adr ''y locations of 8 sprites
sprite_num              long sprite_image ''selected sprite image for each of the 8 sprites
sprite_xflip            long sprite_x_flip ''flag to flip sprite on x axis
sprite_yflip            long sprite_y_flip ''flag to flip sprite on y axis
destination_increment   long 512
destination_increment2   long 1024
AND_Mask                long $FF_FF_FF_00
'Test_Mask               long $0B_0B_0B_0B
Buffer_Number res        1  ''location in scanline buffer
ret_adr       res        1  ''return address     
Single_Pixel res         1  ''single pixel value
Four_Pixels res          1  ''value stored for 4 pixels
Sprite_Pixels res        1  ''loading four pixels from srpite
Temp_Toggle res          1  ''used to toggle left or right side of tile
Temp_Adr res             1  ''temp address holder
Temp_Adr2 res            1  ''temp address holder
Temp_Row res             1  ''current tile row
Temp_Row2 res            1  ''current row for 1bpp tiles
Temp_1bpp res            1  '' used to toggle 1bpp tiles
Temp_Col res             1  ''current tile column
Temp_Var res             1  ''temp value holder
Temp_Var_1 res           1  ''temp value holder
Temp_Var_2 res           1  ''temp value holder
Tile_Map_Adr res         1  ''address of tile map
obpp_tile_adrs res       1  ''address of 1bpp tile
Tiles_Adr res            1  ''address for sprites
Sprites_Adr res          1  ''loading four pixels from srpite
Sprite_Number res        1  ''current sprite we are working with out of 8
cognumber res            1  ''which COG this rendering COG is
cogtotal  res            1  ''total number of rendering COGs
currentscanline res      1 ''current scanline that TV driver is rendering
currentrequest res       1 ''next scanline to render
scanbuffer res          65 ''Scanline buffer + 1 extra
Test_Loc