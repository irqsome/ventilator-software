'' Ranquest - End game sequence (when you win the game)
'' JT Cook - http://www.avalondreams.com
'' Save binary as ranq.wdt
'' 
''
''THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          
''WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR        
''COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,  
''ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                       
''
''
''

CON
  screen_cap=0 'enable screen capture - 0-off, 1-on
  PAL=%0001
  NTSC=%0000
 ' joypad bit encodings
  JOY0_RIGHT  = %00000000_00000001
  JOY0_LEFT   = %00000000_00000010
  JOY0_DOWN   = %00000000_00000100
  JOY0_UP     = %00000000_00001000
  JOY0_START  = %00000000_00010000
  JOY0_SELECT = %00000000_00100000
  JOY0_B      = %00000000_01000000
  JOY0_A      = %00000000_10000000

'{ C3 setup
  _clkmode = xtal1 + pll16x     
  _xinfreq = 5_000_000        
  _memstart = $10      ' memory starts $10 in!!! (this took 2 headaches to figure out)  
  Key_Pins    = 26   ''pin location for keyboard
  Use_Key = 1 'set 1 because we want to use the keyboard and joypad
  Video_Pins = %001_0101   ''pin location for video out
  TV_Type = NTSC ''video type - NTSC

  spiDO     = 21
  spiClk    = 24
  spiDI     = 20
  spiCS     = 25

  NesBasePin = 16
  Audio_Pin_L = 11
  Audio_Pin_R = 10


 ''used for video driver
 SCANLINE_BUFFER = $7F00           
 request_scanline       = SCANLINE_BUFFER-4  'address of scanline buffer for TV driver
 tilemap_adr            = SCANLINE_BUFFER-8  'address of tile map
 tile_adr               = SCANLINE_BUFFER-12 'address of tiles
 border_color           = SCANLINE_BUFFER-16 'address of border color 
 sprite_adr             = SCANLINE_BUFFER-20  'address of where sprites are stored
 sprite_x_adr           = SCANLINE_BUFFER-24 'X location of 8 sprites
 sprite_y_adr           = SCANLINE_BUFFER-32 'Y location of 8 sprites
 sprite_number          = SCANLINE_BUFFER-40 'address of images of 8 sprites
 sprite_x_flip          = SCANLINE_BUFFER-48 'flag to flip sprite on x axis
 sprite_y_flip          = SCANLINE_BUFFER-49 'flag to flip sprite on y axis
 onebpp_tile_adr        = SCANLINE_BUFFER-50 'tile map for 1bpp tiles

 max_mount_time = 25 'max amount of tries it will try to mount SD card
OBJ
  tv_game_gfx : "JTC_Tile_Drv.spin"       'JTC's Tile Driver
  fat  : "SD-MMC_FATEngine_Slim"               ' Modified SD routines
  SN : "SNEcog" ' SNEcog - SN76489 emulator V0.6 (C) 2011 by Johannes Ahlebrand

VAR
   long Tile_Map_Adr ''address to tilemap for graphics driver                                                   
   long joypad ''joy pad
   long joypadold ''old status of joypad
   long rand ''randomizer
   long ioControl[2] ''used for SD card routines
   byte player_dir
   byte player_frame
   byte player_ani_counter
   byte toggle_text

   ''sound variables
   long Soundfx_ptr '' used to play sound effects
   byte Soundfx_duration ''used to tell how long to play a sound
   byte Soundfx_number ''which sound effect is playing
   byte eb1 ''keep 4 byte aligned
   byte eb2 ''keep 4 byte aligned

PUB start | n, tile_test  
  Long[Tile_Adr]:=@TileFile 'grab address of tile graphics
  Long[Sprite_Adr]:=@SpriteFile 'grab address of sprite graphics
  Long[onebpp_tile_adr]:=@Char_Data 'grab 1bpp address
  Tile_Map_Adr:=LONG[CONSTANT($7F00-8)] 'grab address of tile map
  'sn.start(Audio_Pin, 0, false)                  ' Start the emulated SN chip in one cog. DONT use shadow registers                                                                                  

  MountCard 'run through SD Card mounting routines
  ''look for file pal.tv, if not found, start NTSC driver
  \fat.openFile(string("pal.tv"),"r")
  if fat.partitionError
    tile_test:=NTSC
  else
    tile_test:=PAL
    

  tv_game_gfx.Set_Border_Color($02) 'set border color
       
 ''set up graphics driver
 tv_game_gfx.start(Video_Pins,tile_test,4) 'start graphics driver
    sn.start(Audio_Pin_L, Audio_Pin_R, false)       ' Start the emulated SN chip in one cog. DONT use shadow registers
  ''move all sprites off screen, set default sprite orientation
  repeat n from 0 to 7
    BYTE[sprite_y_adr-n]:=0
    BYTE[sprite_y_flip]:=$0
    BYTE[sprite_x_flip]:=$0 
  ''Fill the screen with a blank tile (space)
  Clear_Screen
  DrawMap
  Place_NPC
  Toggle_text:=0
  player_frame:=0
  player_ani_counter:=0
  'put in a small delay to allow the TV to sync
  repeat n from 0 to CONSTANT(4*60)
   Update_Frame ''wait for vertical retrace
  'run animation
  repeat
   Update_Frame ''wait for vertical retrace
   Walk_Player

PUB Update_Frame
'' Handle vsync and sound
   tv_game_gfx.Wait_Vsync ''wait for vertical retrace
   process_soundfx ''handle playing of soundfx if there are any to be played

PUB Process_SoundFX | value, duration, wave_type, volume
  if Soundfx_ptr > 0''make sure there is a sound to be played
    if Soundfx_duration<1 ''grab next sound tone
     value:=(byte[Soundfx_ptr++]<<8) | byte[Soundfx_ptr++]
     wave_type:=byte[Soundfx_ptr]>>4
     volume:=byte[Soundfx_ptr++]&$F
     Soundfx_Duration:=byte[Soundfx_ptr++]
     'sn.play(channel, note, volume (0-high, 15-off) )
     if(value<$FFFF)
       ''are we playing noise channel or square wave
       case wave_type
        0: ''square wave channel
          sn.play(3, 7, $0F)
          sn.play(2, value, volume)       
        1:''noise channel
          sn.play(3, 7, volume)
          sn.play(2, value, $0F)
     ''once we hit value 255, stop sound and quit
     else
       sn.play(3,0,$0F)
       sn.play(2,0,$0F)
       Soundfx_ptr:=0
       Soundfx_number:=0
    ''count down sound duration
    else
     Soundfx_duration--

PUB Play_Sound(snd_number) | adr_difference
  ''play a sound effect

  ''make sure we only play a sound with a higher priority
  '' lower the number, the lower the priority
  if(snd_number>Soundfx_number)
   ''reset sound values
   Soundfx_number:=snd_number ''set new sound effect number that is player
   Soundfx_duration:=0
   ''use this to get the difference from the compiler to binary
'  adr_difference:=@sword_flash-LONG[@Sound_Ptr_Location][1]
'  Soundfx_ptr:=LONG[@Sound_Ptr_Location][snd_number]+adr_difference   
'{ 
'  Int_To_String(7,10, Soundfx_ptr)

   case snd_number
{
    1: 'sword
     Soundfx_ptr:=@sword_flash
    2: 'sword hit bad guy 
     Soundfx_ptr:=@Bad_guy_hit
    3: 'kill bad guy      
     Soundfx_ptr:=@Bad_guy_die     
    4: 'picks up a single coin
     Soundfx_ptr:=@Coin_grab
    5: 'picks up a coin bag
     Soundfx_ptr:=@Multi_Coin_grab
    6: 'drinks potion
     Soundfx_ptr:=@Drink_Potion
    7: 'player is hurt
     Soundfx_ptr:=@Player_Hit
}     
    8: 'sound played when each text character is displayed
     Soundfx_ptr:=@Text_Sound
    9: 'chest drop
     Soundfx_ptr:=@Chest_drop       
PUB Check_Tap_Key(key_hit)
  ''check joy pad 1
  if(key_hit<$0100)
   if(((joypad & key_hit & $00FF) & ((joypadold & key_hit & $00FF)^ key_hit)) <> 0)
    return 1
  ''check joy pad 2
  if(key_hit>$00FF)
   if(((joypad & key_hit & $FF00) & ((joypadold & key_hit & $FF00)^ key_hit)) <> 0)
    return 1
  else
    return 0
PUB Print_String(x,y,adr) | n, text_adr, char_value
'' prints a string
  n:=0
  text_adr:=adr
  char_value:=32
  
  repeat until char_value < 1
   char_value:=byte[text_adr+n]
   char_value&=127
   if(char_value<>0)
     tv_game_gfx.Place_Tile_XY(x+n,y,(char_value)+128 )''write characer
     if(char_value>32)
      Play_Sound(8)
      repeat 10
         Update_Frame
     n++
PUB Int_To_String(x,y, i) | t, c_value, max_c
' does an sprintf(str, "%05d", i); job
 if i<10
'  x+=1
  max_c:=0
 elseif i<100
  x+=1
  max_c:=1
 else
  x+=2
  max_c:=2  
 repeat t from 0 to max_c
  c_value := (i // 10) + 48
  tv_game_gfx.Place_Tile_XY(x,y,(c_value)+128)
  i/=10
  x--      
PUB Place_NPC

   'player
   BYTE[CONSTANT(sprite_x_adr)]:=CONSTANT((7*16)+8)
   BYTE[CONSTANT(sprite_y_adr)]:=CONSTANT(12*16)
   BYTE[CONSTANT(sprite_number)]:=byte[@p_frame+player_frame]

   'guard 1
   BYTE[CONSTANT(sprite_x_adr-1)]:=CONSTANT(5*16)
   BYTE[CONSTANT(sprite_y_adr-1)]:=CONSTANT(6*16)
   BYTE[CONSTANT(sprite_number-1)]:=9
   'guard 2
   BYTE[CONSTANT(sprite_x_adr-2)]:=CONSTANT((4+6)*16)
   BYTE[CONSTANT(sprite_y_adr-2)]:=CONSTANT(6*16)   
   BYTE[CONSTANT(sprite_number-2)]:=9
   'guard 3
   BYTE[CONSTANT(sprite_x_adr-4)]:=CONSTANT(5*16)
   BYTE[CONSTANT(sprite_y_adr-4)]:=CONSTANT(10*16)
   BYTE[CONSTANT(sprite_number-4)]:=9
   'guard 4
   BYTE[CONSTANT(sprite_x_adr-5)]:=CONSTANT((4+6)*16)
   BYTE[CONSTANT(sprite_y_adr-5)]:=CONSTANT(10*16)   
   BYTE[CONSTANT(sprite_number-5)]:=9

   'King
   BYTE[CONSTANT(sprite_x_adr-3)]:=CONSTANT((7*16)+8)
   BYTE[CONSTANT(sprite_y_adr-3)]:=CONSTANT(6*16)   
   BYTE[CONSTANT(sprite_number-3)]:=10

PUB DrawTreasure(tr_number) | x,y
 x:=byte[@tr_location+(tr_number<<1)]
 y:=byte[@tr_location+(tr_number<<1)+1]
 DrawTile(x,y,9)
PUB Walk_Player | n, n2
   if(BYTE[CONSTANT(sprite_y_adr)]>CONSTANT(7*16))
      BYTE[CONSTANT(sprite_y_adr)]-=1
      player_ani_counter++
      if(player_ani_counter>6)
         player_ani_counter:=0
         ''handle player animation
         player_frame++
         player_frame&=$3 'mask since we only have 4 animation frames
         BYTE[CONSTANT(sprite_number)]:=byte[@p_frame+player_frame]
   else
     if(Toggle_Text==0)      
      Toggle_Text:=1
      repeat n from 0 to 5
         DrawTreasure(n)
         Play_Sound(9)
        repeat n2 from 0 to 30
         Update_Frame ''wait for vertical retrace

      Print_String( 2,0,string(" You found all 6 treasures"))
      Print_String( 2,1,string("and have saved the kingdom!"))
      Print_String( 2,3,string("   Today you are a hero."))
      
PUB DrawMap | x,y, n
   n:=0
   repeat y from 0 to 11
     repeat x from 0 to 15
        DrawTile(x,y,byte[@MapScreen+n])
        'DrawTile(1,1,1)
        n++
PUB DrawTile(x,y,tile) | temp_tile, Tile_Map_a 
   temp_tile:=tile<<2
   Tile_Map_a:=LONG[tilemap_adr] +(y<<6)+(x<<1)
   byte[Tile_Map_a]:=byte[@Tile_16x16+(temp_tile++)]
   byte[Tile_Map_a+1]:=byte[@Tile_16x16+(temp_tile++)]
   byte[Tile_Map_a+32]:=byte[@Tile_16x16+(temp_tile++)]
   byte[Tile_Map_a+33]:=byte[@Tile_16x16+(temp_tile)]
  
  
PUB Clear_Screen
  BYTEFILL(Long[tilemap_adr], CONSTANT(128+32), CONSTANT(32*24))
  
PUB MountCard | fin, n

  'repeat until n2 == 1
   fin:=0   
   repeat n from 0 to max_mount_time
    'fin:=  \fsrw.mount(spiSel,spiClr,spiCnt,spiDI,spiDO,spiClk) ' Attempt to mount SD card
    if \fat.FATEngineStart(spiDO,spiClk,spiDI,spiCS,-1,-1,-1,-1,-1) <> true
      ''debug.str(string(13,"start fail",13))
      next
    fin := \fat.mountPartition(0)
    if fat.partitionError
      ''debug.str(string("mount fail",13))
      ''debug.str(fin)
      ''debug.tx(13)
      next
    quit

DAT
TileFile file "castle.til"
SpriteFile file "castle.spr"

''player walking frame
p_frame
byte 3,4,5,4

''build tiles
Tile_16x16
byte 0,0,0,0 ''0 - black tile
byte 1,1,1,1 ''1 - floor
byte 2,2,2,2 ''2 - steps
byte 4,5,4,5 ''3 - rug left side
byte 5,5,5,5 ''4 - rug
byte 5,4,5,4 ''5 - rug right side
byte 3,6,3,6 ''6 - rug left side steps
byte 6,6,6,6 ''7 - rug steps
byte 6,3,6,3 ''8 - right right side steps
byte 32,33,34,35 '' 9 - treasure chest
byte 7,8,9,10 '' 10 - back wall
byte 11,12,13,14 ''11 - wall upper left corner
byte 16,17,18,19 '' 12 - wall left side
byte 20,21,22,23 '' 13 - wall upper right corner
byte 24,25,26,27 '' 14 - wall right side
byte 28,29,0,0   '' 15 - wall bottom left
byte 29,29,0,0   '' 16 - wall bottom
byte 29,30,0,0   '' 17 - wall bottom right

''map of castle
''16x12
MapScreen
byte 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
byte 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
byte 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
byte 0, 0,11,10,10,10,10,10,10,10,10,10,10,13, 0, 0
byte 0, 0,12, 1, 1, 1, 3, 4, 4, 5, 1, 1, 1,14, 0, 0
byte 0, 0,12, 1, 1, 1, 3, 4, 4, 5, 1, 1, 1,14, 0, 0
byte 0, 0,12, 2, 2, 2, 6, 7, 7, 8, 2, 2, 2,14, 0, 0
byte 0, 0,12, 1, 1, 1, 3, 4, 4, 5, 1, 1, 1,14, 0, 0
byte 0, 0,12, 1, 1, 1, 3, 4, 4, 5, 1, 1, 1,14, 0, 0
byte 0, 0,12, 1, 1, 1, 3, 4, 4, 5, 1, 1, 1,14, 0, 0
byte 0, 0,12, 1, 1, 1, 3, 4, 4, 5, 1, 1, 1,14, 0, 0
byte 0, 0,15,16,16,16, 3, 4, 4, 5,16,16,16,17, 0, 0

''x,y locations of treasure
tr_location
byte  4,4
byte  4,7
byte  4,10
byte 11,10
byte 11,7
byte 11,4
Char_Data
'misc characters
byte 255,255,255,255,255,255,255,255 'solid block - 0
byte 0,16,170,198,130,130,254,0 ' empty crown - 1
byte 0,16,186,254,254,254,254,0 ' full crown - 2
byte 56,84,214,254,130,68,56,0 ' smiley face - 3
byte 56,68,130,130,130,68,56,0 ' empty circle -4
byte 56,100,242,226,242,100,56,0 'half filled circle -5
byte 56,124,254,254,254,124,56,0 'filled circle -6
byte 0,0,0,0,0,0,0,0 ' nothing - 7
byte 0,0,0,0,0,0,0,0 ' nothing - 8
byte 0,0,0,0,0,0,0,0 ' nothing - 9
byte 0,0,0,0,0,0,0,0 ' nothing - 10
byte 0,0,0,0,0,0,0,0 ' nothing - 11              
byte 0,0,0,0,0,0,0,0 ' nothing - 12
byte 0,0,0,0,0,0,0,0 ' nothing - 13
byte 0,0,0,0,0,0,0,0 ' nothing - 14
byte 0,0,0,0,0,0,0,0 ' nothing - 15
byte 0,0,0,0,0,0,0,0 ' nothing - 16
byte 0,0,0,0,0,0,0,0 ' nothing - 17
byte 0,0,0,0,0,0,0,0 ' nothing - 18              
byte 0,0,0,0,0,0,0,0 ' nothing - 19
byte 0,0,0,0,0,0,0,0 ' nothing - 20
byte 0,0,0,0,0,0,0,0 ' nothing - 21
byte 0,0,0,0,0,0,0,0 ' nothing - 22
byte 0,0,0,0,0,0,0,0 ' nothing - 23
byte 0,0,0,0,0,0,0,0 ' nothing - 24
byte 0,0,0,0,0,0,0,0 ' nothing - 25              
byte 0,0,0,0,0,0,0,0 ' nothing - 26
byte 0,0,0,0,0,0,0,0 ' nothing - 27
byte 0,0,0,0,0,0,0,0 ' nothing - 28
byte 0,0,0,0,0,0,0,0 ' nothing - 29
byte 0,0,0,0,0,0,0,0 ' nothing - 30
byte 0,0,0,0,0,0,0,0 ' nothing - 31
'enter, special characters
byte 0,0,0,0,0,0,0,0 ' Enter/Clear - 32              
byte 24,24,24,24,0,0,24,0 ' ! - 33
byte 102,102,102,0,0,0,0,0 ' " - 34
byte 102,102,255,102,255,102,102,0 ' # - 35
byte 24,62,96,60,6,124,24,0 '$ - 36
byte 98,102,12,24,48,102,70,0' % - 37
byte 60,102,60,56,103,102,63,0 ' & - 38
byte 6,12,24,0,0,0,0,0 ' ' - 39              
byte 12,24,48,48,48,24,12,0 ' ( - 40
byte 48,24,12,12,12,24,48,0 ' ) - 41
byte 0,102,60,255,60,102,0,0 ' * - 42
byte 0, 24,24,126,24,24,0,0 ' +  - 43
byte 0,0,0,0,0,24,24,48 ' ,  44
byte 0,0,0,126,0,0,0,0 ' - - 45
byte 0,0,0,0,0,24,24,0 ' . - 46              
byte 0,3,6,12,24,48,96,0 ' / - 47
'Numbers 0-9
byte 60,102,110,118,102,102,60,0 ' 0 - 48
byte 24,24,56,24,24,24,126,0 ' 1 - 49
byte 60,102,6,12,48,96,126,0 ' 2 - 50
byte 60,102,6,28,6,102,60,0 ' 3 - 51
byte 6,14,22,102,127,6,6,0 ' 4 - 52
byte 126,96,124,6,6,102,60,0 ' 5 - 53              
byte 60,102,96,124,102,102,60,0 ' 6 - 54
byte 126,102,12,12,12,12,12,0 ' 7 - 55
byte 60,102,102,60,102,102,60,0 ' 8 - 56
byte 60,102,102,62,6,102,60,0 ' 9 - 57
'special characters
byte 0,0,24,0,0,24,0,0 ' : - 58
byte 0,0,24,0,0,24,24,48 ' ; - 59
byte 14,24,48,96,48,24,14,0 ' < - 60
byte 0,0,126,0,126,0,0,0 ' = - 61
byte 112,24,12,6,12,24,112,0 ' > - 62
byte 60,102,6,12,24,0,24,0 ' ? - 63
byte 60,102,110,110,96,98,60,0 ' @ - 64
'A-Z upper case
byte 24,60,102,126,102,102,102,0 ' A - 65
byte 124,102,102,124,102,102,124,0 ' B - 66
byte 60,102,96,96,96,102,60,0 ' C - 67
byte 120,108,102,102,102,108,120,0 ' D - 68
byte 126,96,96,120,96,96,126,0 ' E - 69
byte 126,96,96,120,96,96,96,0 ' F - 70
byte 60,102,96,110,102,102,60,0 ' G - 71
byte 102,102,102,126,102,102,102,0 ' H - 72
byte 60,24,24,24,24,24,60,0 'I - 73
byte 30,12,12,12,12,108,56,0 ' J - 74
byte 102,108,120,112,120,108,102,0 ' K - 75
byte 96,96,96,96,96,96,126,0 ' L - 76
byte 99,119,127,107,99,99,99,0 ' M - 77
byte 102,118,126,110,102,102,102,0 ' N - 78
byte 60,102,102,102,102,102,60,0 ' O - 79
byte 124,102,102,124,96,96,96,0 ' P - 80
byte 60,102,102,102,102,102,60,14 ' Q - 81
byte 124,102,102,124,120,108,102,0 ' R - 82
byte 60,102,96,60,6,102,60,0 ' S - 83
byte 126,24,24,24,24,24,24,0 ' T - 84
byte 102,102,102,102,102,102,60,0 ' U - 85
byte 102,102,102,102,102,60,24,0 ' V - 86
byte 99,99,99,107,127,119,99,0 ' W - 87
byte 102,102,60,24,60,102,102,0 ' X - 88
byte 102,102,102,60,24,24,24,0 ' Y - 89
byte 126,6,12,24,48,112,126,0 ' Z - 90
'special characters
byte 60,48,48,48,48,48,60,0 ' [ - 91
byte 0,96,48,24,12,6,3,0 ' \ - 92
byte 60,12,12,12,12,12,60,0 ' ] - 93
byte 24,60,102,0,0,0,0,0 ' ^ - 94
byte 0,0,0,0,0,0,255,0 ' _ - 95
byte 96,48,24,0,0,0,0,0 ' ` - 96
'a-z lower case
byte 0,0,60,6,62,102,62,0 'a - 97
byte 0,96,96,124,102,102,124,0 ' b - 98
byte 0,0,60,96,96,96,60,0 ' c - 99
byte 0,6,6,62,102,102,62,0 ' d - 100
byte 0,0,60,102,126,96,60,0 ' e - 101
byte 0,14,24,62,24,24,24,0 ' f - 102
byte 0,0,62,102,102,62,6,124 ' g - 103
byte 0,96,96,124,102,102,102,0 ' h - 104
byte 0,24,0,56,24,24,60,0 ' i -105
byte 0,6,0,6,6,6,6,60 ' j -106
byte 0,96,96,108,120,108,102,0 ' k - 107
byte 0,56,24,24,24,24,60,0 ' l - 108
byte 0,0,102,127,127,107,99,0 ' m - 109
byte 0,0,124,102,102,102,102,0 ' n - 110
byte 0,0,60,102,102,102,60,0 ' o - 111
byte 0,0,124,102,102,124,96,96 ' p - 112
byte 0,0,62,102,102,62,6,6 ' q - 113
byte 0,0,124,102,96,96,96,0 ' r - 114
byte 0,0,62,96,60,6,124,0 ' s - 115
byte 0,24,126,24,24,24,14,0 ' t - 116
byte 0,0,102,102,102,102,62,0 ' u - 117
byte 0,0,102,102,102,60,24,0 ' v - 118
byte 0,0,99,107,127,62,54,0 ' w - 119
byte 0,0,102,60,24,60,102,0 ' x - 120
byte 0,0,102,102,102,62,12,120' y - 121
byte 0,0,126,12,24,48,126,0 ' z - 122
'special characters
byte 28,48,48,224,48,48,28,0 ' { - 123
byte 24,24,24,24,24,24,24,24 ' | - 124
byte 56,12,12,7,12,12,56,0 ' } - 125
byte 54,108,0,0,0,0,0,0 ' ~ - 126

'sound that plays for each letter displayed on the text screen
Text_Sound
byte 0, 200,$04,1, 0,144,$00,1, 255,255,255,255

Chest_drop ''sound when a treasure chest is set down
byte 0,4,$10,2, 0,4,$16,2, 0,4,$1A,2, 255,255,255,255 'chest
                         