'' Gamepad driver - interfaces joypads and keyboard into one interface
''
OBJ   keyboard  : "comboKeyboard.spin"
CON
  ''keyboard key codes assigned to joypad keys
  ''To find the key codes for the keyboard, look at the bottom of the source code in comboKeyboard.spin
  ''player 1
  Key0_Up = $C2    ''Up arrow
  Key0_Down = $C3  ''Down arrow
  Key0_Left = $C0  ''Left arrow
  Key0_Right = $C1 ''Right arrow
  Key0_B = $20     ''Space
  Key0_A = $F4     ''Left Alt
'  Key0_B = $F5 ''Right Alt
'  Key0_A = $F3 ''Right Ctrl
  Key0_Start = $0D ''Enter
  Key0_Select = $09 ''Tab  
  ''player 2 (use upper case when mapping keys)
  Key1_Up = $74    ''T
  Key1_Down = $67  ''G
  Key1_Left = $66  ''F
  Key1_Right = $68 ''H
  Key1_B = $F4     ''Left Alt
  Key1_A = $F2     ''Left Ctrl
  ''Hydra Joy settings 
  JOY_RIGHT  = %00000001
  JOY_LEFT   = %00000010
  JOY_DOWN   = %00000100
  JOY_UP     = %00001000
  JOY_START  = %00010000
  JOY_SELECT = %00100000
  JOY_B      = %01000000
  JOY_A      = %10000000
VAR
  byte Use_Keyboard 
PUB Start(pingroup,key_on, key_ptr)
'' this will give us the option to load the keyboard driver or not
 Use_Keyboard:=key_on '1-yes 0-no
 ''if we want to use keyboard driver, then start it
 if Use_Keyboard==1
   keyboard.start(pingroup, key_ptr)
PUB Read_Gamepad(NesBasePin) : nes_bits   |  i, joy0, joy1, joy0_fix, joy1_fix ,BaseKeyPadPin

  ' set I/O ports to proper direction
  ' P3 = JOY_CLK    (4)   23
  ' P4 = JOY_SH/LDn (5)   24
  ' P5 = JOY_DATAOUT0 (6) 25
  ' P6 = JOY_DATAOUT1 (7) 26
  ' NES Bit Encoding
  BaseKeyPadPin := NesBasePin

  DIRA [NesBasePin] := 1 ' output
  DIRA [BaseKeyPadPin+1] := 1 ' output
  DIRA [BaseKeyPadPin+2] := 0 ' input
  DIRA [BaseKeyPadPin+3] := 0 ' input

  OUTA [BaseKeyPadPin] := 0 ' JOY_CLK = 0
  OUTA [BaseKeyPadPin+1] := 0 ' JOY_SH/LDn = 0
  OUTA [BaseKeyPadPin+1] := 1 ' JOY_SH/LDn = 1
  OUTA [BaseKeyPadPin+1] := 0 ' JOY_SH/LDn = 0
  nes_bits := 0
  nes_bits := INA[BaseKeyPadPin+3] | (INA[BaseKeyPadPin+2] << 8)

  repeat i from 0 to 6
   OUTA [BaseKeyPadPin] := 1 ' JOY_CLK = 1
   OUTA [BaseKeyPadPin] := 0 ' JOY_CLK = 0
   nes_bits := (nes_bits << 1)
   nes_bits := nes_bits | INA[BaseKeyPadPin+3] | (INA[BaseKeyPadPin+2] << 8)


' nes_bits := (!nes_bits & $FFFF)      
  '' set all keyboard buttons as clear
  joy0:=$00
  joy1:=$00

  if Use_Keyboard
   'if controller 1 is not plugged in, clear it out
    if((nes_bits & $00FF) == $0000)
      nes_bits|=$00FF

    'if controller 2 is not plugged in, clear it out
    if (nes_bits & $FF00) == $0000
      nes_bits|=$FF00

  
   ''handle keyboard
   if keyboard.present
     ''Player 1----------
     if keyboard.keystate(Key0_Right)
      joy0|=JOY_RIGHT
     if keyboard.keystate(Key0_Left)
      joy0|=JOY_LEFT
     if keyboard.keystate(Key0_Down)
      joy0|=JOY_DOWN
     if keyboard.keystate(Key0_Up)
      joy0|=JOY_UP
     if keyboard.keystate(Key0_Start)
      joy0|=JOY_Start
     if keyboard.keystate(Key0_Select)
      joy0|=JOY_Select
     if keyboard.keystate(Key0_B)
      joy0|=JOY_B
     if keyboard.keystate(Key0_A)
      joy0|=JOY_A
{
     ''Player 2----------
     if keyboard.keystate(Key1_Right)
      joy1|=JOY_RIGHT
     if keyboard.keystate(Key1_Left)
      joy1|=JOY_LEFT
     if keyboard.keystate(Key1_Down)
      joy1|=JOY_DOWN
     if keyboard.keystate(Key1_Up)
      joy1|=JOY_UP
     if keyboard.keystate(Key1_B)
      joy1|=JOY_B
     if keyboard.keystate(Key1_A)
      joy1|=JOY_A
}      
  ''combine the keyboard and joystick
  
' nes_bits :=( (!nes_bits) | (joy0 | (joy1<<8))) & $FFFF
 nes_bits :=( (!nes_bits) | (joy0) ) & $FFFF

{
PUB NES_Delay  | ii, iii
''    repeat ii from 0 to 5000
      iii:= 5*675
}      