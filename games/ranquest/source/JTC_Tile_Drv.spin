''8-Bit Game Graphics tile driver
'' JT Cook
''

CON
  X_Length = 32 ''number of tiles that run horizontally across screen
  Y_Length = 24 ''number of tiles that run vertically across screen

' constants
 SCANLINE_BUFFER = $7F00           
 request_scanline       = SCANLINE_BUFFER-4  'address of scanline buffer for TV driver
 tilemap_adr            = SCANLINE_BUFFER-8  'address of tile map
 tile_adr               = SCANLINE_BUFFER-12 'address of tiles
 border_color           = SCANLINE_BUFFER-16 'address of border color 
 sprite_adr             = SCANLINE_BUFFER-20  'address of where sprites are stored
 sprite_x_adr           = SCANLINE_BUFFER-24 'X location of 8 sprites
 sprite_y_adr           = SCANLINE_BUFFER-32 'Y location of 8 sprites
 sprite_number          = SCANLINE_BUFFER-40 'address of images of 8 sprites
 sprite_x_flip          = SCANLINE_BUFFER-48 'flag to flip sprite on x axis
 sprite_y_flip          = SCANLINE_BUFFER-49 'flag to flip sprite on y axis

 x_tiles = 16 '*16=240
 y_tiles = 12 '*16=160


OBJ
  tv    : "JB_tv_02.spin"             ' tv driver 256 pixel scanline
  gfx   : "JTC_Tile_Renderer.spin"    ' graphics engine

VAR
   byte Tile_Map[X_Length*Y_Length] ''tile map
   'byte Tiles[(64)*Num_Of_Tiles] ''number of tiles (8x8 pixels = 64)

   long cog_number ''used for rendering engine
   long cog_total  ''used for rendering engine  
   'long border_color ''used for borders
''used for TV driver
   long tv_status      '0/1/2 = off/visible/invisible           read-only
   long tv_enable      '0/? = off/on                            write-only
   long tv_pins        '%ppmmm = pins                           write-only
   long tv_mode        '%ccinp = chroma,interlace,ntsc/pal,swap write-only
   long tv_screen      'pointer to screen (words)               write-only
   long tv_colors      'pointer to colors (longs)               write-only               
   long tv_hc          'horizontal cells                        write-only
   long tv_vc          'vertical cells                          write-only
   long tv_hx          'horizontal cell expansion               write-only
   long tv_vx          'vertical cell expansion                 write-only
   long tv_ho          'horizontal offset                       write-only
   long tv_vo          'vertical offset                         write-only
   long tv_broadcast   'broadcast frequency (Hz)                write-only
   long tv_auralcog    'aural fm cog                            write-only
''used to stop and start tv driver

PUB tv_start
  tv.start(@tvparams)

PUB tv_stop
   tv.stop
   
PUB start(video_pins,NorP, cog_num)      | i
  DIRA[0] := 1
  outa[0] := 0

  long[tilemap_adr] := @Tile_Map  'address of tile map
  long[@tvparams+8]:=video_pins ''map pins for video out
  long[@tvparams+12]:=NorP ''NTSC or PAL
  if NorP&1 'PAL?
    long[@tvparams+24] := 12 'set hx for PAL
  else
    long[@tvparams+24] := 10 'set hx for NTSC
  ' Boot requested number of rendering cogs:
  ' this must be at least 4, anything less will not be enough cogs  
  'cog_total := 3
  cog_total :=cog_num
  cog_number := 0
  repeat
    gfx.start(@cog_number)
    repeat 10000 ' Allow some time for previous cog to boot up before setting 'cog_number' again
    cog_number++
  until cog_number == cog_total  
  long[border_color]:=$02 ''default border color
  'start tv driver
  tv.start(@tvparams)

PUB Wait_Vsync ''wait until frame is done drawing
    repeat while long[$7F00-4] <> 191
PUB Set_Border_Color(bcolor) | i ''set the color for border around screen
    long[border_color]:=bcolor
PUB Place_Tile_XY(x_location, y_location, tile)
'' Place a tile in the tilemap
 ''x_location - select x position of tile
 ''y_location - select y position of tile
 ''tile - which tile will occupy location
   y_location<<=5 '' multiply by 32 since there are 32 tiles per row
   x_location+=y_location ''get final address
   Tile_Map[x_location]:=tile
          
DAT
tvparams                long    0               'status
                        long    1               'enable
                        long    %011_0000       'pins ' PROTO/DEMO BOARD = %001_0101 ' HYDRA = %011_0000
                        long    0               'mode - default to NTSC
                        long    x_tiles         'hc
                        long    y_tiles         'vc
                        long    10              'hx
                        long    1               'vx
                        long    0               'ho
                        long    0               'vo
                        long    50_000_000'_xinfreq<<4  'broadcast
                        long    0               'auralcog
                        long    SCANLINE_BUFFER
                        long    border_color 'pointer to border colour
                        long    request_scanline
disp_ptr                long    0
VSync                   long    0
                        

nextline                long    0                        