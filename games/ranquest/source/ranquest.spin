'' Ranquest (C)2011 Jay Cook
'' Game: Jay Cook - http://www.avalondreams.com
'' Music: John Wedgeworth
''
''THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          
''WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR        
''COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,  
''ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                       
''
'' Other credits:
'' TV driver that supports either NTSC or PAL by Parallax and Jim Bagley
'' SNEcog - SN76489 emulator V0.6 (C) 2011 by Johannes Ahlebrand
'' C3 SD Card driver by David Betz,Mike Green,Tomas Rokicki, and Jonathan Dummer
''
'' Game Requirements: a C3, NES joypad adapter with NES joypad or keyboard, micro SD card, TV
''
'' Controls: use NES pad or keyboard to control your character
'' Walk using the arrow keys or the control pad
'' Thrust sword / Action button - Space bar or the B - button on the control pad
'' Pause - Enter or Start
''
'' To play the game with a PAL tv, create a text file, name it PAL.TV, place it on the root of the
''   SD card
''
'' 8 Sprites on screen/scanline that flip on both X/Y axis
'' 32x24 non-scrolling tile background
'' TV/Rendering driver takes 5 cogs (NOTE: May be possible to run in 4 cogs)
'' Tiles 0-127 - 8 bit tiles
'' Tiles 128-255 - 1 bit tiles (black and white - mainly intended for text)
'' --------------------------------------------------
''
''
'' Site that shows colors for TV driver - http://www.rayslogic.com/propeller/Programming/TV_Colors.htm
''
'' map file ---
''  16x11 map (32x22 tiles)
''  each screen is 200 bytes: 176 bytes of map, 24 bytes trailing for extra info
''
'' Sprites: 16x16 pixels, 1 byte per pixel, each sprite is 256 bytes
'' Tiles 8x8 pixels, 1 byte per pixel, each tile is 64 bytes  
'' Updated to run on El'Jugador

CON
' screen_cap=0 'enable screen capture - 0-off, 1-on
  PAL=%0001
  NTSC=%0000
 ' joypad bit encodings
  JOY0_RIGHT  = %00000000_00000001
  JOY0_LEFT   = %00000000_00000010
  JOY0_DOWN   = %00000000_00000100
  JOY0_UP     = %00000000_00001000
  JOY0_START  = %00000000_00010000
  JOY0_SELECT = %00000000_00100000
  JOY0_B      = %00000000_01000000
  JOY0_A      = %00000000_10000000


  _clkmode = xtal1 + pll16x     
  _xinfreq = 5_000_000
  _stack=200
  _memstart = $10      ' memory starts $10 in!!! (this took 2 headaches to figure out)  
  Key_Pins  = 8   ''pin location for keyboard
  Use_Key   = 1 'set 1 because we want to use the keyboard and joypad

  Video_Pins = %001_0101   ''pin location for video out
  TV_Type = NTSC ''video type - NTSC

  spiDO     = 21
  spiClk    = 24
  spiDI     = 20
  spiCS     = 25

  NesBasePin = 16
  Audio_Pin_L = 11
  Audio_Pin_R = 10

 ''used for video driver
 SCANLINE_BUFFER = $7F00           
 request_scanline       = SCANLINE_BUFFER-4  'address of scanline buffer for TV driver
 tilemap_adr            = SCANLINE_BUFFER-8  'address of tile map
 tile_adr               = SCANLINE_BUFFER-12 'address of tiles
 border_color           = SCANLINE_BUFFER-16 'address of border color 
 sprite_adr             = SCANLINE_BUFFER-20  'address of where sprites are stored
 sprite_x_adr           = SCANLINE_BUFFER-24 'X location of 8 sprites
 sprite_y_adr           = SCANLINE_BUFFER-32 'Y location of 8 sprites
 sprite_number          = SCANLINE_BUFFER-40 'address of images of 8 sprites
 sprite_x_flip          = SCANLINE_BUFFER-48 'flag to flip sprite on x axis
 sprite_y_flip          = SCANLINE_BUFFER-49 'flag to flip sprite on y axis
 onebpp_tile_adr        = SCANLINE_BUFFER-50 'tile map for 1bpp tiles
 max_mount_time = 25 'max amount of tries it will try to mount SD card
 ScreenFileSize = 200 'how many bytes per screen in world
 max_16x16_tiles = 54 'how many combined 16x16 tiles
 max_tiles = 64*(98)      'max number of graphic tiles
 max_sprites=256*23   'max number of sprites
 max_sword_time=20 'how many frames do we show the sword
 flyback_timer=7 'how long enemy will fly back after it is hit
 flash_timer=6 'how many frames until hit/flash animation is changed
 inv_timer=80 'how long player is temp invincible after a hit
 treasure_tile=47 'tile number for treasure chest
 end_castle=$44 'final screen where you take the treasures to
 text_offset=128''where to start text tiles
 text_blank=128+32 'blank tile

OBJ
  'debug : "Simple_Serial"                                                            
  tv_game_gfx : "JTC_Tile_Drv.spin"       'JTC's Tile Driver
  joy   : "gamepad_keyboard"      'joy/keyboard driver
  fat  : "SD-MMC_FATEngine_Slim"               ' Modified SD routines
  SN : "SNEcog" ' SNEcog - SN76489 emulator V0.6 (C) 2011 by Johannes Ahlebrand
  ''dip into assembly routines to repurpose memory that was loaded into COGs
  jtrdr : "JTC_Tile_Renderer"               ' rendering routines
VAR
'   long Tile_Map_Adr ''address to tilemap for graphics driver
   'long ioControl[2] ''used for SD card routines   
   long joypad ''joy pad
   long joypadold ''old status of joypad
   long rand ''randomizer
   long guy_gold ''how much gold player is holding   

   byte player_frame 'which sprite frame  0-3
   byte player_direction 'which direction 0-3
   byte player_dir_offset 'used to select player sprite
   byte player_ani_counter 'used as delay for animation
   ''
   byte MapXY 'location on world map $XY broken up with into 2 hex digits
   byte diagnal'used to handle diagnal control
   byte player_x_old 'prior location for player sprite
   byte player_y_old 'prior location for player sprite
   ''
   byte Tile_16x16[(4*max_16x16_tiles)]' data that builds a 16x16 from 4 8x8 tiles
   byte treasure_found 'how many treasure chests you have found
   byte enemy_on_map   ''current enemy on the map, if the two don't match, load a new one
   byte waterfall_timer
   byte Sword_Y
   byte ScreenMap[((16*11)+6)] 'map on the screen
   byte b1 ''keey everything 4 byte alligned
   byte Music_state 'is music playing?
   long waterfall_frame
   long CRC_Debug '' debug
   'byte waterfall_frame
   byte treasure_location[6] ''each treasure X/Y map location
   byte current_tile_set ''used to determine if we need to load a new tileset or not
   byte empty1 ''keep 4 byte alligned  
   byte TileFile[max_tiles] '8x8 tiles
   byte SpriteFile[max_sprites] '16x16 sprites

   'byte b1 ''keep everything 4 byte aligned
   'byte b2 
   'byte b3
   'byte b4

   ''
   byte sword_timer 'used to count down time player has sword out
   byte sword_hit   'make sure sword doesn't hit same object twice while sword is out
   byte guy_hp      'how much life you have
   byte old_MapXY
   ''
   byte guy_hit_timer 'timer for player to fly back, highest two bits are for direction
   byte guy_hit_type  '1-player flying back, 2-player invincible after hit, highest two bits are for direction
   byte guy_flash     'toggles flashing of guy after hit
   byte player_y
   ''handle enemy setup
   byte enemy_x[6]
   byte enemy_x_old[6]   
   byte enemy_y[6]
   byte enemy_y_old[6]   
   byte enemy_dir[6] 'timer:=(enemy_dir[n]&$F)  dir:=enemy_dir[n]>>4
   byte enemy_type[6] 'type 0-none,1-pig guy,6-one gold, 7-5 gold, 8-life, 9-flash
   byte enemy_hit_timer[6] 'timer for enemy to fly back, highest two bits are for direction
   byte enemy_move_timer[6] 'timer until next animation
   byte enemy_hp[6] 'hit points, highest bit is for speed(1-fast, 0-slow)
   byte enemy_ani_timer[6] 'time until next movement
   byte enemy_per_screen[100] 'this is where the game stores how many enemies and which one
                              '$00-$0F - number of enemies (4 bits)
                              '$10-$1F - type of enemy     (4 bits)
   byte enemy_memory_map[6] 'this will hold the last 6 screens in memory and remember how
   byte enemy_memory_num[6] 'many enemies were on the screen
   byte enemy_change_dir[6] 'timer to determine when an enemy will change direction

   ''sound variables
   long Soundfx_ptr '' used to play sound effects
   byte Soundfx_duration ''used to tell how long to play a sound
   byte Soundfx_number ''which sound effect is playing
   byte Start_Game_Var ''used in waitkey
   'byte eb1 ''keep 4 byte aligned
   byte music_note_delay
   byte music_volume[4] ''set volume of notes being played
   long music_location[2] ''address of song
   long music_note_length[2] ''length of note being played
   long music_bass_start ''location of bass
   long music_ptr                   
PUB start | n, tile_test  
  Long[Tile_Adr]:=@TileFile 'grab address of tile graphics
  Long[Sprite_Adr]:=@SpriteFile 'grab address of sprite graphics
  'Long[onebpp_tile_adr]:=@Char_Data 'grab 1bpp address

  'debug.init(31,30,9600)
  'debug.str(string(13,"START",13))
  tv_game_gfx.Set_Border_Color($02) 'set border color
  'fsrw.start(@ioControl)                                  ' Start I2C/SPI driver

  ''look for file pal.tv, if not found, start NTSC driver



   MountCard 'run through SD Card mounting routines
  Music_state:=0 'music stopped
  \fat.openFile(string("pal.tv"),"r")
  if fat.partitionError
    tile_test:=NTSC
  else
    tile_test:=PAL
    
   
  ''set up graphics driver
  ''last variable sets how many rendering COGS we will use (4 best, 3 may show drop off)
  tv_game_gfx.start(Video_Pins,tile_test,3) 'start graphics driver               

  sn.start(Audio_Pin_L, Audio_Pin_R, false)                   ' Start the emulated SN chip in one cog. DONT use shadow registers


  
  ''move all sprites off screen, set default sprite orientation
  repeat n from 0 to 7
    BYTE[sprite_y_adr-n]:=0 
    BYTE[sprite_y_flip]:=$0
    BYTE[sprite_x_flip]:=$0
  ''Fill the screen with a blank tile (space)
  current_tile_set:=99 ''set tile set to one that does not exist
  ''make a blank tile to fill the screen before we load everything
  ''address for 1bpp tiles
  Long[onebpp_tile_adr]:=jtrdr.Return_Address
  LONG[Long[onebpp_tile_adr]+CONSTANT(((text_blank -128)*8)+0)]:=0
  LONG[Long[onebpp_tile_adr]+CONSTANT(((text_blank -128)*8)+4)]:=0
  Clear_Screen
  Load_Font ''load up 1bpp tiles
  

   ''load keyboard driver
  LoadFile(string("ranq.kbd")) 
  ReadFile(1228, 0, @TileFile, 160) '160
  joy.start(Key_Pins,Use_Key, @TileFile) 'start gamepad/keyboard driver

  Start_Game_Var:=1
  Do_Title ''show title screen
  Load_Music 'load game music
  Clear_Screen  
  enemy_on_map:=99 ' reset type of enemy on screen
  Random_treasure ''randomize 6 treasure  
  Start_Game_Var:=0
  Populate_Map ' fill world with random enemies      
  guy_gold:=0 'starting gold
  treasure_found:=0 'starting amount of treasure found    
  ''start map - start at castle
  MapXY:=$44 'front of castle
  Start_Game 'setup variables for a new game or coming back from death
  Do_Textbox(826) ''Intro text part 1
  Wait_Key ''wait for key hit
  Do_Textbox(904) ''Intro text part 2
  Wait_Key ''wait for key hit
  repeat
   Update_Frame ''wait for vertical retrace, process sound

   ''read gamepad    
   joypad := joy.Read_Gamepad(NesBasePin)
   ''grab prior sprite locations
   player_x_old:=BYTE[sprite_x_adr]
   player_y_old:=player_y
   'can't move player if we are thrusting the sword or flying back from hit --------------
   if((sword_timer<1) AND ((guy_hit_type&$3F)<>1)) 
   ''check for sword thrust
   ''check diagnals first
    if((joypad & JOY0_UP) <> 0) AND ((joypad & JOY0_LEFT) <> 0)
     diagnal^=1 ''toggle which direction player will be moving     
     if(diagnal) ''up
      player_y--
      player_direction:=1
     else ''left
      BYTE[sprite_x_adr]--
      player_direction:=2      
     'handle graphics
     BYTE[sprite_number]:=BYTE[@player_animation+player_frame]+player_dir_offset
     Run_Player_Animation                  
    elseif((joypad & JOY0_UP) <> 0) AND ((joypad & JOY0_RIGHT) <> 0)
     diagnal^=1 ''toggle which direction player will be moving
     if(diagnal) ''up
      player_y--
      player_direction:=1
     else ''right
      BYTE[sprite_x_adr]++
      player_direction:=3
     'handle graphics
     BYTE[sprite_number]:=BYTE[@player_animation+player_frame]+player_dir_offset
     Run_Player_Animation                  
    elseif((joypad & JOY0_DOWN) <> 0) AND ((joypad & JOY0_LEFT) <> 0)
     diagnal^=1 ''toggle which direction player will be moving
     if(diagnal) ''down
      player_y++
      player_direction:=0
     else ''left
      BYTE[sprite_x_adr]--
      player_direction:=2
     'handle graphics
     BYTE[sprite_number]:=BYTE[@player_animation+player_frame]+player_dir_offset
     Run_Player_Animation                  
    elseif((joypad & JOY0_DOWN) <> 0) AND ((joypad & JOY0_RIGHT) <> 0)
     diagnal^=1 ''toggle which direction player will be moving
     if(diagnal) ''down
      player_y++
      player_direction:=0
     else ''right
      BYTE[sprite_x_adr]++
      player_direction:=3
     'handle graphics
     BYTE[sprite_number]:=BYTE[@player_animation+player_frame]+player_dir_offset
     Run_Player_Animation                  
      
   ''no diagnals
    elseif((joypad & JOY0_DOWN) <> 0)
     player_y++
     player_direction:=0
     player_dir_offset:=0     
     BYTE[sprite_x_flip]|=1
     BYTE[sprite_number]:=BYTE[@player_animation+player_frame]+player_dir_offset
     Run_Player_Animation               
    elseif((joypad & JOY0_UP) <> 0)
     player_y--
     player_direction:=1
     player_dir_offset:=3     
     BYTE[sprite_x_flip]|=1
     BYTE[sprite_number]:=BYTE[@player_animation+player_frame]+player_dir_offset
     Run_Player_Animation                  
    elseif((joypad & JOY0_LEFT) <> 0)
     BYTE[sprite_x_adr]-=1
     BYTE[sprite_x_flip]|=1     
     player_direction:=2
     player_dir_offset:=6
     BYTE[sprite_number]:=BYTE[@player_animation+player_frame]+player_dir_offset
     Run_Player_Animation                     
    elseif((joypad & JOY0_RIGHT) <> 0)
     BYTE[sprite_x_adr]+=1
     'BYTE[sprite_x_flip]&=CONSTANT($FF-$1)
      UnFlip_Sprite_X(0)     
     player_direction:=3
     player_dir_offset:=6
     BYTE[sprite_number]:=BYTE[@player_animation+player_frame]+player_dir_offset
     Run_Player_Animation
    if(Check_Tap_Key(JOY0_B) <> 0)
      Play_Sound(1)
     sword_timer:=max_sword_time
     sword_hit:=0 'reset hit value, sword just came out so we didn't hit anything yet
     n:=(player_dir_offset/3)
     BYTE[sprite_number]:=9+n
     ''find player direction based on sprite direction
     if (BYTE[sprite_x_flip]&1 <> 1) 'if sprite is flipped account for that
      n++
     ''sprite info for sword
     case n
      0: 'facing down
       BYTE[CONSTANT(sprite_x_adr-1)]:=BYTE[sprite_x_adr]+4
       BYTE[CONSTANT(sprite_y_adr-1)]:=player_y+16
       BYTE[CONSTANT(sprite_number-1)]:=17
       BYTE[sprite_y_flip]&=CONSTANT($FF-$2)
       'BYTE[sprite_x_flip]&=CONSTANT($FF-$2)
       UnFlip_Sprite_X(1)
      1: 'facing up
       BYTE[CONSTANT(sprite_x_adr-1)]:=BYTE[sprite_x_adr]+4
       BYTE[CONSTANT(sprite_y_adr-1)]:=player_y-16
       BYTE[CONSTANT(sprite_number-1)]:=17
       BYTE[sprite_y_flip]|=2
       'BYTE[sprite_x_flip]&=CONSTANT($FF-$2)
       UnFlip_Sprite_X(1)
      2: 'facing left
       BYTE[CONSTANT(sprite_x_adr-1)]:=BYTE[sprite_x_adr]-16
       BYTE[CONSTANT(sprite_y_adr-1)]:=player_y+5
       ''keeps sword on the screen if sword x is less than 0
       if(BYTE[sprite_x_adr]<16)
        BYTE[CONSTANT(sprite_x_adr-1)]:=0 
       BYTE[CONSTANT(sprite_number-1)]:=18
       BYTE[sprite_y_flip]&=CONSTANT($FF-$2)
       'BYTE[sprite_x_flip]|=2
       Flip_Sprite_X(1)
      3: 'facing right
       BYTE[CONSTANT(sprite_x_adr-1)]:=BYTE[sprite_x_adr]+16
       BYTE[CONSTANT(sprite_y_adr-1)]:=player_y+5
       BYTE[CONSTANT(sprite_number-1)]:=18
       'BYTE[sprite_x_flip]&=CONSTANT($FF-$2)
       UnFlip_Sprite_X(1)
       BYTE[sprite_y_flip]&=CONSTANT($FF-$2)

   ''sword is out, still have time left on sword-----------------
   elseif(sword_timer>0 and (guy_hit_type&$3F)<>1)  'sword is out, still have time left on sword
    Check_Sword_Hit'check enemy hit
    sword_timer--
    ''sword time up, reset player, move sword off screen
    if(sword_timer==0)
     BYTE[CONSTANT(sprite_y_adr-1)]:=0 'move sword sprite off screen
     'reset player sprite
'     player_frame:=1     
     BYTE[sprite_number]:=BYTE[@player_animation+player_frame]+player_dir_offset
   ''player is flying back from hit-----------------------    
   else
    guy_hit_timer--
    player_x_old:=BYTE[sprite_x_adr]
    player_y_old:=player_y
    n:=0
    'thrust player
    case (guy_hit_type>>6) 'get direction
       0: 'fly down
         player_y+=4
         if(player_y>191)      
           n:=1         
       1: 'fly up
         player_y-=4         
         if(player_y<32) 
           n:=1                
       2: 'fly left
         if(BYTE[sprite_x_adr]>3)
           BYTE[sprite_x_adr]-=4
           if(BYTE[sprite_x_adr]<4)
            n:=1
         else
            n:=1                
       3: 'fly right
         BYTE[sprite_x_adr]+=4
         if(BYTE[sprite_x_adr]>CONSTANT(256-16-4))
           n:=1                
    if(Check_Wall_Hit (BYTE[sprite_x_adr], player_y, (guy_hit_type>>6)) > 0)
     n:=1
    if(n==1)
      BYTE[sprite_x_adr]:=player_x_old
      player_y:=player_y_old
     'guy_hit_timer:=0 'reset hit timer if it hits a wall
    ''if timer runs out, remove direction portion of timer
    if(guy_hit_timer==0)
       guy_hit_type:=2
       guy_hit_timer:=inv_timer 'make player temp inivincible       
         
{                   
   if(screen_cap== 1 AND (joypad & JOY0_SELECT) <> 0)
       Screen_Grab
}
   ''handle player being temp invincible after hit
   if((guy_hit_type&$3F)==2)
      guy_hit_timer--
      if(guy_hit_timer==0)
       guy_hit_type:=0
   guy_flash^=1 'toggles flashing of player after they are hit
   if(guy_hit_type&$3F<>0 AND guy_flash)
     BYTE[sprite_y_adr]:=CONSTANT(192+16)'move player sprite off screen  
   else 
     BYTE[sprite_y_adr]:=player_y'place player Y coords

   Check_guy_hit ''check hit detection between player and other sprites

   Handle_Enemy ''move enemies
   Do_Waterfall ''animate waterfall   
   ''pause game
   if(Check_Tap_Key(JOY0_START) <> 0)
    Pause_game
   joypadold:=joypad ''buffer joypad info for checking taps
   
PUB Do_Title | Tile_Map_A
     LoadFile(string("ranq.ttl")) 
     ReadFile(CONSTANT(32*24), 0, LONG[tilemap_adr], 204) '204
      joypad:=$FFFF ''buffer joypad info for checking taps
      Wait_Key
PUB Flip_Sprite_X(sprite_nbr)
          BYTE[sprite_x_flip]|=(1<<sprite_nbr)
PUB UnFlip_Sprite_X(sprite_nbr)
        BYTE[sprite_x_flip]&=($FF-(1<<sprite_nbr))    
PUB Start_Game | n
  Start_Music ''start up the music
''when a game is started or a player has come back from a death
  player_frame:=0 'reset player animation
  player_direction:=0 'reset player direction
  player_dir_offset:=0 'reset offset
  player_ani_counter:=0 'reset animation counter
  diagnal:=0 'reset diagnal controller handler
  BYTE[sprite_x_flip]|=1'set player sprite flip to normal
  'sword_timer:=max_sword_time
  LoadSprites  'load player sprites
   BYTE[sprite_number]:=BYTE[@player_animation+player_frame]+player_dir_offset
   guy_hp:=6 'starting life
   guy_hit_timer:=0 'player is not flying back from hit
   guy_hit_type:=0 'player is not flying back from hit
   guy_flash:=0 'toggles flashing of player after they are hit   
   ''fill up map que with fake map
   repeat n from 0 to 5
      enemy_memory_map[n]:=$FF
   old_MapXY:=$FF 'also for map que 
  LoadMap(MapXY)
 ''player start   
  BYTE[sprite_x_adr]:=(ScreenMap[constant(176+3)]&$F0)
  player_y:=((ScreenMap[constant((16*11)+3)]&$0F)<<4)+32
   
  DrawMap
  Print_String(12,0,string("Ranquest"))
  Print_String(12,1,string("Gold:"))
  Print_String(22,0,string("Treasures"))
  Print_String(01,0,string("- Life -"))
  Draw_Treasures
  Draw_Life
  Draw_Gold
PUB Update_Sound
   process_soundfx ''handle playing of soundfx if there are any to be played
   Play_Music
PUB Update_Frame
'' Handle vsync and sound
   tv_game_gfx.Wait_Vsync ''wait for vertical retrace
   process_soundfx ''handle playing of soundfx if there are any to be played
   Play_Music
PUB Fade_Screen | n, n2, m1,m2, temp_music
 ''move all sprites off screen
   repeat n from 0 to 7
     BYTE[sprite_y_adr-n]:=0
   temp_music:=music_state
   music_state:=0
   repeat n2 from 0 to 9
    repeat n from 0 to CONSTANT(max_tiles-1)
        'TileFile[n]&=7 'turn everything grey scale
        TileFile[n]-=1 'darken everything one shade
        'if(TileFile[n]&$0F<$0A)
        if(TileFile[n]&$07<$02)
         TileFile[n]:=$02
    repeat m1 from 0 to 3
     Update_Frame
   current_tile_set:=99 ''since we erased the tile set, reset it
  'clear graphics tiles from map/screen
   BYTEFILL(Long[tilemap_adr]+CONSTANT(32*2), text_blank,CONSTANT(32*22))
   music_state:=temp_music
PUB Red_Screen | n
 ''move all sprites off screen
'  repeat n from 0 to 7
'    BYTE[sprite_y_adr-n]:=0
   'turn all the tiles red
    repeat n from 0 to CONSTANT(max_tiles-1)
        TileFile[n]&=$0F 'remove color value
        TileFile[n]|=$B0 'add red color
PUB Player_Died | n , sprite_timer

  Play_Sound(99)''stop sound
  Music_state:=0 'stop music
  update_sound
    ''move all the non player sprites off screen
    repeat n from 1 to 7
      BYTE[sprite_y_adr-n]:=0
    BYTE[sprite_number]:=1 'make player face forward
  ''load sprite of dead player
  LoadFile(string("ranq.spr"))
  ReadFile(CONSTANT((4*256)-1), CONSTANT(27*256), @SpriteFile+CONSTANT(256*2), 152)

' BYTEFILL(Long[tilemap_adr]+CONSTANT(0+8), text_blank, 5)          
' Int_To_String(9,0, CRC_Debug)'draw text   

    Red_Screen 'tint all the tiles red
    Play_Sound(9)
    sprite_timer:=0
    repeat n from 0 to 3
      repeat sprite_timer from 0 to 20
        Update_Frame ''wait for vertical retrace, process sound
      BYTE[sprite_number]++

    Print_String(8,6, string("You have perished"))
    Print_String(12,19,string("Hit Start"))
    Wait_Key ''wait for key hit
    Fade_Screen
    n:=(?rand & $01) +(?rand & $01)'random which Inn to start at
    MapXY:=byte[@Inn_Locations+n]
    Start_Game 'setup variables for a new game or coming back from death    
    Do_Textbox(736) ''draw text when you come back
    Wait_Key ''wait for key hit
    joypad := joy.Read_Gamepad(NesBasePin) 'so it does not read the start key
PUB Check_guy_hit | x1,x2,y1,y2, n', bgx1, bgx2, bgy1, bgy2
 ''do player to object hit detection
 ''get coords for player
 x1:=BYTE[sprite_x_adr] +2
 x2:=x1+12
 y1:=player_y+2
 y2:=y1+13
     '0 - no enemy
     '1 - pig guy
     '2 - skull bat
     '3 - ghost
     '6 - one gold
     '7 - five gold
     '8 - life vile
     '9 - flash
    'run through all the sprites
 repeat n from 0 to 5
  if(enemy_type[n]>0)
    ''check to see if we run into a guy/object
    if(x1<(enemy_x[n]+15) AND x2>(enemy_x[n]+2))
      if(y1<(enemy_y[n]+15) AND y2>(enemy_y[n]+2))
        case enemy_type[n]
        
          1,2,3: 'enemies
          ''make sure guy isn't already hit
           if(guy_hit_type==0)
            if(sword_timer>0)  'sword is out, hide it
              BYTE[CONSTANT(sprite_y_adr-1)]:=220
              sword_timer:=0
              BYTE[sprite_number]:=BYTE[@player_animation+player_frame]+player_dir_offset              
            ''make player fly back
            guy_hit_timer:=flyback_timer
            guy_hit_type:=1|((enemy_dir[n]>>4)<<6)
            ''damage player
            if(guy_hp>0)
             guy_hp--
            Play_Sound(7)
            Draw_Life
            
          6: 'one gold
            guy_gold+=1
            Draw_Gold
            Play_Sound(4)
          7: '5 gold
            guy_gold+=5            
            Draw_Gold
            Play_Sound(5)
          8: 'life vile
            if(guy_hp<6)
              guy_hp++
            Draw_Life
            Play_Sound(6)

        ''remove item from map
        if(enemy_type[n]>5)
            enemy_type[n]:=0
            enemy_y[n]:=0
            BYTE[CONSTANT(sprite_y_adr-2)-n]:=enemy_y[n]
        
PUB Check_Sword_Hit| sw_x1, sw_x2, sw_y1, sw_y2, n, enemy_number, bit_number
    sw_x1:=BYTE[CONSTANT(sprite_x_adr-1)]
    sw_y1:=BYTE[CONSTANT(sprite_y_adr-1)]
    'get bounding box coords for sword
    case BYTE[CONSTANT(sprite_number-1)] 'use sword graphic to determine direction
     17: 'facing up/down
       sw_x1+=2
       sw_x2:=(sw_x1+4)
       sw_y2:=sw_y1+16
     18: 'facing left/right
       sw_y1+=2
       sw_y2:=(sw_y1+4)
       sw_x2:=sw_x1+16   
    n:=(player_dir_offset/3) 'sword direction
    if (BYTE[sprite_x_flip]&1 <> 1) 'if sprite is flipped account for that
      n++
    bit_number:=1
 repeat enemy_number from 0 to 5
  ''make sure they aren't already flying back
  '' and make sure sword doesn't hit same object twice while sword is out
   if ((enemy_hit_timer[enemy_number] & $3F)==0 AND !sword_hit&bit_number)
    'check hit detection against bad guys
    if( enemy_type[enemy_number]>0 AND enemy_type[enemy_number]<3)
      if(sw_x1=<(enemy_x[enemy_number]+15) AND sw_x2=>(enemy_x[enemy_number]+2))
       if(sw_y1=<(enemy_y[enemy_number]+15) AND sw_y2=>(enemy_y[enemy_number]+2))
          sword_hit|=bit_number 'mark enemy so we can't hit it again
          enemy_hp[enemy_number]--
          ''if we kill an enemy, choose nothing or a random object
          if ((enemy_hp[enemy_number]&$7F)<1)
                 ''flash when enemy is killed
                 enemy_type[enemy_number]:=9
                 enemy_dir[enemy_number]:=0
                 enemy_move_timer[enemy_number]:=flash_timer
                 enemy_hit_timer[enemy_number]:=0
                 Play_Sound(3)
          else
            enemy_hit_timer[enemy_number]:=flyback_timer|(n<<6) 'timer for enemy to fly back and direction
            Play_Sound(2)
   bit_number<<=1 'ready next enemy                               
PUB Handle_Enemy | n,temp_x, temp_y, temp_dir, temp_timer, wall_hit, temp_frame, spr_number, temp_num
{
   byte enemy_x[6]
   byte enemy_x_old[6]   
   byte enemy_y[6]
   byte enemy_y_old[6]   
   byte enemy_dir[6]
   byte enemy_type[6] 'type 0-none, 1-gold 1, 2-gold 5, 3-flash, 4-pig guy
        enemy_hit_timer[enemy_number]:=flyback_timer 'timer for enemy to fly back
        enemy_hp[enemy_number] 'hit points                    
                 
}
'loop through all the enemies
repeat n from 0 to 5
 '''''''''''''''''''''''''''''
 ''is enemy flying back after hit?
 if((enemy_hit_timer[n] & $3F)>0)
  enemy_hit_timer[n]--
  temp_dir:=(enemy_hit_timer[n]>>6)'get flyback direction
  enemy_x_old[n]:=enemy_x[n]
  enemy_y_old[n]:=enemy_y[n]
  wall_hit:=0
   'thrust enemy bakwards
   case temp_dir
       0: 'fly down
         enemy_y[n]+=4
         if(enemy_y[n]>191)      
           wall_hit:=1         
       1: 'fly up
         enemy_y[n]-=4
         if(enemy_y[n]<32) 
           wall_hit:=1                
       2: 'fly left
         if(enemy_x[n]>3)
           enemy_x[n]-=4
           if(enemy_x[n]<4)
            wall_hit:=1
         else
            wall_hit:=1                
       3: 'fly right
         enemy_x[n]+=4
         if(enemy_x[n]>CONSTANT(256-16-4))
           wall_hit:=1                
   ''check wall hit for pig guy
   if(Check_Wall_Hit (enemy_x[n], enemy_y[n], temp_dir) <> 0 AND enemy_type[n]==1)
     wall_hit:=1
   if(wall_hit==1)
     enemy_x[n]:=enemy_x_old[n]
     enemy_y[n]:=enemy_y_old[n]
     'enemy_hit_timer[n]:=0 'reset hit timer if it hits a wall
  ''update sprite location
   BYTE[CONSTANT(sprite_x_adr-2)-n]:=enemy_x[n]
   BYTE[CONSTANT(sprite_y_adr-2)-n]:=enemy_y[n]
 '''''''''''''''''''''''''''''            
  'enemy not flying back and moving like normal
 else  
  ''handle movement
   temp_timer:=(enemy_dir[n]&$F)
   temp_dir:=enemy_dir[n]>>4
   enemy_x_old[n]:=enemy_x[n]
   enemy_y_old[n]:=enemy_y[n]
  '''''''''''''''''''''''''''''''''''''''''
  ''figure out what enemy is on the screen
  case(enemy_type[n])
   ''enemies - all enemies use the same type of movement calcs
   1,2,3:
    ''timer and direction are built in together
    wall_hit:=0
    enemy_ani_timer[n]++ 'time until next movement
    if(enemy_hp[n]&$80)''if it is a fast enemy, speed it up
     enemy_ani_timer[n]++
    if((enemy_ani_timer[n]&$7F)>6)
     ''enemy done walking path, change direction
     if(enemy_change_dir[n]==0)
        temp_dir:=(?rand & $03)'random direction
        enemy_change_dir[n]:=byte[@bad_guy_path+(?rand & $03)]   
     else
      enemy_change_dir[n]--    
     enemy_ani_timer[n]&=$80 ''clear animation timer
     enemy_ani_timer[n]^=$80 '' 2 movements frames before we change animation frame
     if(enemy_ani_timer[n]&$80)''toggle animation frame
      temp_timer^=1
     ''pig guy
     case(enemy_type[n])
       1:
   '     move pig guy - include screen borders
         case temp_dir
          0: ''facing down
           enemy_y[n]+=2
           if(enemy_y[n]>191) 
             wall_hit:=1
          1: ''facing up
           enemy_y[n]-=2
           if(enemy_y[n]<32)
             wall_hit:=1
          2: ''facing left
           enemy_x[n]-=2
           if(enemy_x[n]<4)
             wall_hit:=1
          3: ''facing right
           enemy_x[n]+=2
           if(enemy_x[n]>CONSTANT(256-16-4))
             wall_hit:=1
        ''check if they hit a wall
        if(Check_Wall_Hit (enemy_x[n], enemy_y[n], temp_dir) <> 0)
          wall_hit:=1
        '' hit a wall, put them back, change direction
        if(wall_hit>0)
           enemy_x[n]:=enemy_x_old[n]
           enemy_y[n]:=enemy_y_old[n]
           temp_dir:=byte[@turn_enemy_right+temp_dir] 'lookup table to have a enemy rotate right if it hits a wall
       2,3:
    '    move skull bat in diagnals - include screen borders
         case temp_dir
          0: ''facing up\right
           enemy_y[n]-=2
           enemy_x[n]+=2
          1: ''facing down\right
           enemy_y[n]+=2
           enemy_x[n]+=2
           wall_hit:=1
          2: ''facing up\left
           enemy_y[n]-=2
           enemy_x[n]-=2
          3: ''facing down\left
           enemy_y[n]+=2
           enemy_x[n]-=2
        ''check screen borders
        'if(enemy_y[n]<32 OR enemy_y[n]>191 OR enemy_x[n]>CONSTANT(256-16-4) OR enemy_x[n]<4)
        if(enemy_y[n]<32 OR enemy_y[n]>191)
          enemy_x[n]:=enemy_x_old[n]
          enemy_y[n]:=enemy_y_old[n]
          temp_dir^=1
        if(enemy_x[n]>CONSTANT(256-16-4) OR enemy_x[n]<4)
          enemy_x[n]:=enemy_x_old[n]
          enemy_y[n]:=enemy_y_old[n]
          temp_dir^=2
    
     enemy_dir[n]:=(temp_dir<<4)|temp_timer

   '''''''''''''''''''''''''''''''''''''''''
   'flash after something is killed
   9:
     if enemy_move_timer[n]>0
      enemy_move_timer[n]--
     else
      enemy_move_timer[n]:=flash_timer
      temp_timer++
      enemy_dir[n]:=(temp_dir<<4)|temp_timer
      ''finished with flash animation
      if(temp_timer>2)
        enemy_type[n]:=(?rand & $0F)
        ''turns out to be a random object
        if enemy_type[n]>12
            enemy_type[n]-=7
        ''otherwise it is a blank object
        else
          enemy_type[n]:=0      
  '''''''''''''''''''''''''''''''''''''''''
   other:'debug
    wall_hit:=wall_hit
  ''''''''''''''''''''''''''''''''''''''''''''
  '' update animation
  BYTE[CONSTANT(sprite_x_adr-2)-n]:=enemy_x[n]
  BYTE[CONSTANT(sprite_y_adr-2)-n]:=enemy_y[n]
  temp_num:=n+2
  spr_number:=CONSTANT(sprite_number-2)-n ''sprite number for this object
  'get animation frame
' temp_frame:=byte[@pig_frame_lut+temp_dir]+temp_timer
   case enemy_type[n]
     'no enemy
     0:BYTE[CONSTANT(sprite_y_adr-2)-n]:=0 'move sprite off screen
     'pig guy
     1: BYTE[spr_number]:=19+byte[@pig_frame_lut+temp_dir]
       ''if it is side view, then we will update frame
       if(temp_dir>1)
         BYTE[spr_number]+=temp_timer
        if(temp_dir<3)
          'BYTE[sprite_x_flip]|=($4<<n)
          Flip_Sprite_X(temp_num)
        else
          'BYTE[sprite_x_flip]&=($FF-($4<<n))
          UnFlip_Sprite_X(temp_num)
       else '' top/bottom view, we want to flip sprite on X axis
         ''function size with below code 1108, without below code 1080
         if(temp_timer&1)
           'BYTE[sprite_x_flip]&=($FF-($4<<n))
           UnFlip_Sprite_X(temp_num)
         else
           'BYTE[sprite_x_flip]|=($4<<n)
           Flip_Sprite_X(temp_num)
     'skull bat
     2: BYTE[spr_number]:=19+temp_timer
       if(temp_dir<2)
         'BYTE[sprite_x_flip]&=($FF-($4<<n))
         UnFlip_Sprite_X(temp_num)
       else
         'BYTE[sprite_x_flip]|=($4<<n)
         Flip_Sprite_X(temp_num)
     'ghost
     3: temp_frame:=(temp_dir&$01)
       BYTE[spr_number]:=19+temp_frame
       if(temp_dir&2)
         'BYTE[sprite_x_flip]&=($FF-($4<<n))
         UnFlip_Sprite_X(temp_num)
       else
         'BYTE[sprite_x_flip]|=($4<<n)
         Flip_Sprite_X(temp_num)         
     'one gold
     6: BYTE[spr_number]:=14
     'five gold
     7: BYTE[spr_number]:=15
     'life vile
     8: BYTE[spr_number]:=16
     'flash
     9:BYTE[spr_number]:=byte[@flash_gfx+temp_timer]
   ''toggle sprite flipping for right or left facing sprite   
PUB CheckScreenBorders | temp_x, temp_y
   ''check top of screen
   temp_x:=MapXY>>4
   temp_y:=MapXY&$F   
   ''check top of screen
   if  player_y<32
      if(temp_y>0)
        temp_y--
        MapXY:=(temp_x<<4)|temp_y
        LoadMap(MapXY)
        Scroll_Screen(0) ''pan screen down         
        DrawMap
      else
       player_y:=32              
   ''check bottom of screen
   if  player_y>CONSTANT(192)
      if(temp_y<9)
        temp_y++
        MapXY:=(temp_x<<4)|temp_y
        LoadMap(MapXY)
        Scroll_Screen(1) ''pan screen up         
        DrawMap
      else
       player_y:=CONSTANT(192)                      
   ''check left of screen
   if  BYTE[sprite_x_adr]<1
      if(temp_x>0)
        temp_x--
        MapXY:=(temp_x<<4)|temp_y
        LoadMap(MapXY)
        Scroll_Screen(2) ''pan screen right         
        DrawMap
      else
       BYTE[sprite_x_adr]:=CONSTANT(1)                      
   ''check right of screen      
   if  BYTE[sprite_x_adr]>CONSTANT(256-16-1)
      if(temp_x<9)
        temp_x++
        MapXY:=(temp_x<<4)|temp_y      
        LoadMap(MapXY)
        Scroll_Screen(3) ''pan screen left         
        DrawMap                
      else
       BYTE[sprite_x_adr]:=CONSTANT(256-16-1)                      
PUB Check_Wall_Result (wall_hit) | n
 ''what to do with player and where the two dots are at
 ''if one dot is open and the other dot hits a wall, we can slide the player in the
 '' direction of the open dot for smoother gameplay
 case wall_hit
  $11: ''both dots hit a wall
   BYTE[sprite_x_adr]:=player_x_old
   player_y:=player_y_old
  $01: ''only first dot hit the wall, move right
   ''up or down - move right
   if(player_direction==0 OR player_direction==1)
    BYTE[sprite_x_adr]++
    player_y:=player_y_old
   ''left or right - move down
   else
    BYTE[sprite_x_adr]:=player_x_old
    player_y++
  $10: ''only second dot hit the wall, move left
   ''up or down - move right
   if(player_direction==0 OR player_direction==1)
    BYTE[sprite_x_adr]--
    player_y:=player_y_old
   ''left or right - move down
   else
    BYTE[sprite_x_adr]:=player_x_old
    player_y--
  ''anything else will be a special case, like warp or treasure
  other:
    ''treat block as a wall to the player
    if(wall_hit>0)
         player_y:=player_y_old
         BYTE[sprite_x_adr]:=player_x_old
    ''hit a warp tile
    if((wall_hit & $F0) == $20 OR (wall_hit & $0F) ==$02)
      ''found all the treasures, load end sequence
       if(MapXY==end_castle) AND (treasure_found==6)
        Fade_Screen ''Transition for entering/exiting screen
        'LoadFile(string("ranq.wdt"))
        'fsrw.bootSDCard
        fat.bootPartition(string("ranq.wdt"))
       ''tries to enter castle without all the treasures
       elseif(MapXY==end_castle)
         Do_Textbox(1163)
         Wait_Key ''wait for key hit
         DrawMap
       else
        MapXY:=ScreenMap[constant((16*11)+2)]
        player_y:=MapXY/10
        player_y_old:=(MapXY-(player_y*10))<<4
        MapXY:=player_y | player_y_old
        Fade_Screen ''Transition for entering/exiting screen
        LoadMap(MapXY)''load map
        DrawMap''draw new map
       
        ''player start 
        BYTE[sprite_x_adr]:=(ScreenMap[constant(176+3)]&$F0)
        player_y:=((ScreenMap[constant((16*11)+3)]&$0F)*16)+32
        'BYTE[sprite_x_adr]:=CONSTANT(10*16)
        'player_y:=CONSTANT(7*16)
        player_y_old:=player_y
'       player_x_old:=BYTE[sprite_x_adr]
    ''picked up a treasure
    elseif((wall_hit & $F0) == $30 OR (wall_hit & $0F) ==$03)
      treasure_found++ 'add treasure to counter
      Draw_Treasures 'update find on screen
      'replace treasure tile with grass/ground tile
      repeat n from 0 to CONSTANT((16*10)-1)
          if ScreenMap[n]==treasure_tile
             ScreenMap[n]:=0
      'now that treasure has been claimed, move it out of world
      repeat n from 0 to 5
        if(treasure_location[n]==MapXY)
           treasure_location[n]:=$FF ''move treasure out of world
           Do_Textbox(LONG[@text_pointers+(n<<2)]) ''draw text of treasure we picked up
           Wait_Key ''wait for key hit
      if(treasure_found==6)
        Do_Textbox(450) ''draw text that all the treasure was found
        Wait_Key ''wait for key hit
      DrawMap
    ''hit the Inn doors
    elseif((wall_hit & $F0) == $40 OR (wall_hit & $0F) ==$04)
      Enter_Inn
      DrawMap
    ''healer
    elseif((wall_hit & $F0) == $50 OR (wall_hit & $0F) ==$05)
      Do_Textbox(1073)
      Wait_Key ''wait for key hit
      DrawMap
      guy_hp:=6
      Draw_Life      
      Play_Sound(6)

      
PUB Check_Wall_Hit (guy_x, guy_y, guy_dir) | x0,x1, y0,y1, n, wall1,wall2
 ''make sure we aren't trying to read tiles off screen
  if(guy_y>191)
     guy_y:=191
 ''idea is to check two dots infront of the direction the object is moving
 x0:=guy_x
 y0:=guy_y-32
 x1:=x0
 y1:=y0
 ''direction 1=up, 0=down, 2=left, 3=right
 case guy_dir
  1:''up 
   x0+=2
   x1+=13
   y0+=10
   y1+=10

  0:''down
   x0+=2
   x1+=13
   y0+=15
   y1+=15
  
  2:''left
   y0+=10
   y1+=15
  
  3:''right
   x0+=15
   x1+=15
   y0+=10
   y1+=15
         
 ''check dot 1
 x0>>=4 'x/=16
 y0>>=4 'y/=16
 n:=(y0<<4)+x0 '(y0*16)+x0
 wall1:=BYTE[@wall_data+ScreenMap[n]]

 ''check dot 2
 x1>>=4 'x/=16
 y1>>=4 'y/=16
 n:=(y1<<4)+x1 '(y1*16)+x1
 wall2:=BYTE[@wall_data+ScreenMap[n]]
 ''combine the two dots, return what we found
 wall1|=wall2<<4
 return(wall1)
            
PUB Run_Player_Animation | n
   player_ani_counter++
   if(player_ani_counter>6)
    player_ani_counter:=0
    ''handle player animation
    player_frame++
    player_frame&=$3 'mask since we only have 4 animation frames
  ''also handle colision detection
  n:=Check_Wall_Hit(BYTE[sprite_x_adr], player_y,player_direction)
  Check_Wall_Result(n) ''check what to do with the result of the hit detection
  CheckScreenBorders ''check screen borders
PUB Process_SoundFX | value, duration, wave_type, volume
  if Soundfx_ptr > 0''make sure there is a sound to be played
    if Soundfx_duration<1 ''grab next sound tone
     value:=(byte[Soundfx_ptr++]<<8) | byte[Soundfx_ptr++]
     wave_type:=byte[Soundfx_ptr]>>4
     volume:=byte[Soundfx_ptr++]&$F
     Soundfx_Duration:=byte[Soundfx_ptr++]
     'sn.play(channel, note, volume (0-high, 15-off) )
     if(value<$FFFF)
       ''are we playing noise channel or square wave
       case wave_type
        0: ''square wave channel
          sn.play(3, 7, $0F)
          sn.play(2, value, volume)       
        1:''noise channel
          sn.play(3, 7, volume)
          sn.play(2, value, $0F)
     ''once we hit value 255, stop sound and quit
     else
       sn.play(3,0,$0F)
       sn.play(2,0,$0F)
       Soundfx_ptr:=0
       Soundfx_number:=0
    ''count down sound duration
    else
     Soundfx_duration--
  
PUB Play_Sound(snd_number) | adr_difference
  ''play a sound effect

  ''make sure we only play a sound with a higher priority
  '' lower the number, the lower the priority
  if(snd_number>Soundfx_number)
   ''reset sound values
   Soundfx_number:=snd_number ''set new sound effect number that is player
   Soundfx_duration:=0
   ''use this to get the difference from the compiler to binary
'  adr_difference:=@sword_flash-LONG[@Sound_Ptr_Location][1]
'  Soundfx_ptr:=LONG[@Sound_Ptr_Location][snd_number]+adr_difference   
'{ 
'  Int_To_String(7,10, Soundfx_ptr)

   case snd_number
    1: 'sword
     Soundfx_ptr:=@sword_flash
    2: 'sword hit bad guy 
     Soundfx_ptr:=@Bad_guy_hit
    3: 'kill bad guy      
     Soundfx_ptr:=@Bad_guy_die     
    4: 'picks up a single coin
     Soundfx_ptr:=@Coin_grab
    5: 'picks up a coin bag
     Soundfx_ptr:=@Multi_Coin_grab
    6: 'drinks potion
     Soundfx_ptr:=@Drink_Potion
    7: 'player is hurt
     Soundfx_ptr:=@Player_Hit
    8: 'sound played when each text character is displayed
     Soundfx_ptr:=@Text_Sound
    9: 'player dies
     Soundfx_ptr:=@Player_die
    99: 'stop sound
     Soundfx_ptr:=@Sound_Stop  
'  Int_To_String(7,11, Soundfx_ptr)
'} 
PUB Check_Tap_Key(key_hit)
  ''check joy pad 1
' if(key_hit<$0100)
   if(((joypad & key_hit & $00FF) & ((joypadold & key_hit & $00FF)^ key_hit)) <> 0)
    return 1
   return 0
PUB Enter_Inn | x,choice_value, n, Tile_Map_a, Sub_Value
 ''handle the Inn screens
 Do_Textbox(544)
 Tile_Map_a:=LONG[tilemap_adr]                    

 n:=0
 choice_value:=1
 repeat until n<>0
   ''read gamepad  
   Update_Frame
   joypadold:=joypad ''buffer joypad info for checking taps
   joypad := joy.Read_Gamepad(NesBasePin)
   if(Check_Tap_Key(CONSTANT(JOY0_LEFT|JOY0_RIGHT)) <> 0)
     choice_value^=1
     Play_Sound(8)
     
   'place arrow
   Byte[Tile_Map_a+CONSTANT((32*15)+9+1)]:=text_blank
   Byte[Tile_Map_a+CONSTANT((32*15)+9+8+1)]:=text_blank
   x:=CONSTANT((32*15)+9+8+1)
   if(choice_value==0)
      x:=CONSTANT((32*15)+9+1)
   Byte[Tile_Map_a+x]:=CONSTANT(3+128) ''move arrow

   if(Check_Tap_Key(CONSTANT(JOY0_B|JOY0_START)) <> 0)
     n:=1

 joypadold:=joypad ''buffer joypad info for checking taps
 ''if the player chooses yes
 if(choice_value==0)
  if(guy_gold>19) ''make sure player has enough gold
       guy_gold-=20 ''subtract gold
       Draw_Gold
       Fade_Screen ''Transition for entering/exiting screen
       guy_hp:=6
       Play_Sound(6)
       LoadMap(MapXY)''load map
       Draw_Life
  ''not enough gold
  else
       Do_Textbox(633)
       Wait_Key ''wait for key hit    

PUB DrawMap | X,Y, n, temp_tile, Tile_Map_a
''Draw map using 16x11 tiles with 2x2 tile lookup table
 X:=0
 Y:=1 'make room for status bar
 n:=0
 repeat until n==Constant(16*11)
   temp_tile:=(ScreenMap[n++]<<2)
   ''throw out invalid tiles
   if(temp_tile>CONSTANT(max_16x16_tiles<<2))
      temp_tile:=0
   Tile_Map_a:=LONG[tilemap_adr] +(Y<<6)+(X<<1)  
   byte[Tile_Map_a]:=Tile_16x16[temp_tile++]
   byte[Tile_Map_a+1]:=Tile_16x16[temp_tile++]
   byte[Tile_Map_a+32]:=Tile_16x16[temp_tile++]
   byte[Tile_Map_a+33]:=Tile_16x16[temp_tile]
      
   X++
   if(X>15)
     X:=0
     Y++
PUB Scroll_Screen(Scroll_dir) | scroller, x,y, noffset1, noffset2, map_offset, temp_tile, tile_offset
  ''16x11 32x22
  ''0 -pan screen down, 1-pan screen up, 2-pan screen right, 3-pan screen left
  ''choose the direction to scroll
'      player_y:=CONSTANT(192) 'bottom of screen
'      player_y:=32 ' top of screen
'      BYTE[sprite_x_adr]:=CONSTANT(256-16-1) 'right hand side of screen
'      BYTE[sprite_x_adr]:=1'left hand side of screen
  ''move all other sprites off screen
  repeat x from 0 to 5
     BYTE[sprite_y_adr-2-x]:=0
  ''handle screen scrolling
  case Scroll_dir
   ''pan screen down (enter from top)
   0:
    map_offset:=CONSTANT((16*11)+16) 'where to start reading info from new map
    repeat scroller from 0 to 21
      Update_Frame ''wait for vertical retrace, process sound
      Update_Frame ''wait for vertical retrace, process sound     
      noffset2:=LONG[tilemap_adr] + CONSTANT((21*32)+64)
      noffset1:=LONG[tilemap_adr] + CONSTANT(((21*32)-32)+64)
      ''move player sprite
      player_y+=8
      if(player_y>CONSTANT(192))
        player_y:=CONSTANT(192)
      BYTE[sprite_y_adr]:=player_y'place player Y coords            
      Update_Frame ''wait for vertical retrace, process sound
      repeat y from 0 to 20
        repeat x from 0 to 7
          long[noffset2]:=long[noffset1]
          noffset2+=4
          noffset1+=4
       noffset1-=(64)
       noffset2-=(64)
      ''copy tile info from new map
      ''actual map is made up of 16x16 tiles
      noffset1:=LONG[tilemap_adr] + CONSTANT(64)      
      if(scroller&1)
        tile_offset:=0
        map_offset-=16 ''move up a line        
      else
        tile_offset:=2      
        map_offset-=32
      repeat x from 0 to 15       
       temp_tile:=(ScreenMap[map_offset++]<<2)+tile_offset
       byte[noffset1++]:=Tile_16x16[temp_tile++]
       byte[noffset1++]:=Tile_16x16[temp_tile]
   ''pan screen up (enter from bottom)       
   1:
    map_offset:=0 'where to start reading info from new map
    repeat scroller from 0 to 21
      Update_Frame ''wait for vertical retrace, process sound
      Update_Frame ''wait for vertical retrace, process sound 
      noffset2:=LONG[tilemap_adr] + CONSTANT(64)
      noffset1:=LONG[tilemap_adr] + CONSTANT(32+64)
      ''move player sprite
      player_y-=8
      if(player_y<32)
        player_y:=32       
      BYTE[sprite_y_adr]:=player_y'place player Y coords            
      Update_Frame ''wait for vertical retrace, process sound
      repeat y from 0 to 20
        repeat x from 0 to 7
          long[noffset2]:=long[noffset1]
          noffset2+=4
          noffset1+=4
      ''copy tile info from new map
      ''actual map is made up of 16x16 tiles
      noffset1:=LONG[tilemap_adr] + CONSTANT((21*32)+64)      
      tile_offset:=0      
      if(scroller&1)
        map_offset-=16
        tile_offset:=2
      repeat x from 0 to 15       
       temp_tile:=(ScreenMap[map_offset++]<<2)+tile_offset
       byte[noffset1++]:=Tile_16x16[temp_tile++]
       byte[noffset1++]:=Tile_16x16[temp_tile]
   ''pan screen right (enter from left)
   2:
    map_offset:=CONSTANT(15+161+16) 'where to start reading info from new map
    repeat scroller from 0 to 31
      Update_Frame ''wait for vertical retrace, process sound
      Update_Frame ''wait for vertical retrace, process sound 
      noffset2:=LONG[tilemap_adr] + CONSTANT(64+31)
      noffset1:=LONG[tilemap_adr] + CONSTANT(64+30)
      ''move player sprite
      if(BYTE[sprite_x_adr]<CONSTANT(256-16-1))
       BYTE[sprite_x_adr]+=8
      else
        BYTE[sprite_x_adr]:=CONSTANT(256-16-1)      
      Update_Frame ''wait for vertical retrace, process sound
      ''unrolled for better performance
      repeat y from 0 to 21
        repeat x from 0 to 6
          byte[noffset2--]:=byte[noffset1--]
          byte[noffset2--]:=byte[noffset1--]
          byte[noffset2--]:=byte[noffset1--]                    
          byte[noffset2--]:=byte[noffset1--]
       byte[noffset2--]:=byte[noffset1--]
       byte[noffset2--]:=byte[noffset1--]
       byte[noffset2--]:=byte[noffset1--]                    
                    
       noffset1+=63
       noffset2+=63
      ''copy tile info from new map
      ''actual map is made up of 16x16 tiles
      noffset1:=LONG[tilemap_adr] + CONSTANT(64)      
      if(scroller&1)
        map_offset-=CONSTANT(160+16)
        tile_offset:=0
      else
        map_offset-=CONSTANT(161+16)      
        tile_offset:=1      
      repeat x from 0 to 10       
       temp_tile:=(ScreenMap[map_offset]<<2)+tile_offset
       map_offset+=16
       byte[noffset1]:=Tile_16x16[temp_tile]
       noffset1+=32
       byte[noffset1]:=Tile_16x16[temp_tile+2]
       noffset1+=32
   ''pan screen left (enter from right)
   3:
    map_offset:=CONSTANT(159+16) 'where to start reading info from new map
    repeat scroller from 0 to 31
      Update_Frame ''wait for vertical retrace, process sound
      Update_Frame ''wait for vertical retrace, process sound 
      noffset2:=LONG[tilemap_adr] + CONSTANT(64)
      noffset1:=LONG[tilemap_adr] + CONSTANT(64+1)
      ''move player sprite
      if(BYTE[sprite_x_adr]>CONSTANT(8))
       BYTE[sprite_x_adr]-=8
      else
        BYTE[sprite_x_adr]:=CONSTANT(1)      
      Update_Frame ''wait for vertical retrace, process sound
      ''unrolled for better performance      
      repeat y from 0 to 21          
        repeat x from 0 to 6
          byte[noffset2++]:=byte[noffset1++]
          byte[noffset2++]:=byte[noffset1++]
          byte[noffset2++]:=byte[noffset1++]
          byte[noffset2++]:=byte[noffset1++]
        byte[noffset2++]:=byte[noffset1++]
        byte[noffset2++]:=byte[noffset1++]
        byte[noffset2++]:=byte[noffset1++]
       noffset1+=1
       noffset2+=1
      ''copy tile info from new map
      ''actual map is made up of 16x16 tiles
      noffset1:=LONG[tilemap_adr] + CONSTANT(64+31)      
      if(scroller&1)
        map_offset-=CONSTANT(160+16)
        tile_offset:=1
      else
        map_offset-=CONSTANT(159+16)      
        tile_offset:=0      
      repeat x from 0 to 10       
       temp_tile:=(ScreenMap[map_offset]<<2)+tile_offset
       map_offset+=16
       byte[noffset1]:=Tile_16x16[temp_tile]
       noffset1+=32
       byte[noffset1]:=Tile_16x16[temp_tile+2]
       noffset1+=32
PUB Save_enemy_number(MapLocation) | n, enemy_cnt
'  byte enemy_memory_map[6] 'this will hold the last 6 screens in memory and remember how
'  byte enemy_memory_num[6] 'many enemies were on the screen

 'move que down to make room for current screen
 repeat n from 5 to 1
   enemy_memory_map[n]:=enemy_memory_map[n-1]
   enemy_memory_num[n]:=enemy_memory_num[n-1]
 enemy_cnt:=0
 ''check for enemies
 repeat n from 0 to 5
   if(enemy_type[n]>0 AND enemy_type[n]<4)''make sure we only count enemies
     enemy_cnt++
 enemy_memory_map[0]:=MapLocation
 enemy_memory_num[0]:=enemy_cnt
 
PUB Load_enemy_number(MapLocation) | n, n2, map_num, enemy_num
''check to see if this map is already in que
 enemy_num:=$FF ''default for map not being in que
 
 repeat n from 0 to 5
   'if we find a match
   if(enemy_memory_map[n]==MapLocation)
      ''grab screen info
      map_num:=enemy_memory_map[n]
      enemy_num:=enemy_memory_num[n]
      ''erase current part of que and move everything else up
      if(n<5)
       repeat n2 from n to 4
        enemy_memory_map[n2]:=enemy_memory_map[n2+1]
        enemy_memory_num[n2]:=enemy_memory_num[n2+1]
    quit
       
 return enemy_num                                  
PUB LoadMap(MapLocation) | X,Y, file_status, temp_map
''only do this if we are in the game
if(Start_Game_Var==0)
  ''get current location of player
   player_x_old:=BYTE[sprite_x_adr]
   player_y_old:=player_y     
    
  ''save current enemy info to map que
  if(old_MapXY<>$FF)
    Save_enemy_number(old_MapXY)  
  old_MapXY:=MapLocation
  
''Load screen from map file on SD card
X:=MapLocation>>4
  Y:=MapLocation&$F
  temp_map:=((Y*10)+X)
  MapLocation:=temp_map * ScreenFileSize

  'load map file
  repeat file_status from 0 to 40
   LoadFile(string("ranquest.map"))
  'ReadFile(number_of_bytes, data_offset, data_ptr, do_crc)
   ReadFile(Constant((16*11)+6), MapLocation, @ScreenMap, -1)

   if(CRC_Debug==((ScreenMap[Constant((16*11)+6)])<<1&$FF))
     quit

  ''crc check failed after 20 attempts, no reason to beleive it will pass after this
 
  if(file_status>38)
      File_Corrupt
      Abort

{
 BYTEFILL(Long[tilemap_adr]+CONSTANT(0+0), text_blank, 12)          
 Int_To_String(0,0, (CRC_Debug))'draw text
 Int_To_String(5,0, (ScreenMap[Constant((16*11)+6)]<<1))'draw text   

}
''if we have started game, also load tiles, enemies, and check for treasure
if(Start_Game_Var==0)  
  'load the tileset for that map
  LoadTiles(ScreenMap[Constant((16*11)+5)])

  ''see if treasure is on this map
  repeat X from 0 to 5
   if(old_MapXY==treasure_location[X]) 
    ''Place treasure chest into map
    X:=ScreenMap[Constant((16*11)+4)]>>4
    Y:=ScreenMap[Constant((16*11)+4)]&$F
    ScreenMap[(Y<<4)+X]:=treasure_tile
  'Int_To_String(1,1, ScreenMap[Constant(16*11)] )'draw value if enemy can be on the screen
  Load_Enemies(temp_map)'load up the enemies for the screen
PUB Populate_Map | map_n, n
' this is where we place enemies in maps
'$00-$0F - number of enemies (4 bits)
'$10-$1F - type of enemy     (4 bits)                     
'(?rand & $0F)
 ''set the pig guys first (fill the whole map)
 repeat map_n from 0 to 99
   'this is where the game stores how many enemies and which one
   enemy_per_screen[map_n]:=(byte[@enemy_on_screen_lut+((?rand & $03)>>1)])
   enemy_per_screen[map_n]|=$10 ' pig guy

 ''now fill half the world with skull bats
 repeat n from 0 to 49
   map_n:=$FF ' so we can run randomzer
   ''we will randomize the map number, make sure it is in limits and it doesn't already have a skull bat
   repeat while (map_n>99) OR (enemy_per_screen[map_n]&$F0 == $20)
     map_n:=(?rand & $7F) 'mask off number so it is 0-127
   enemy_per_screen[map_n]&=$0F 'mask off enemy that is there
   enemy_per_screen[map_n]|=$20 'set skull bat

 ''now fill some screens with the ghost
 repeat n from 0 to 14
   map_n:=$FF ' so we can run randomzer
   ''we will randomize the map number, make sure it is in limits and it doesn't already have a ghost
   repeat while (map_n>99) OR (enemy_per_screen[map_n]&$F0 == $30)
     map_n:=(?rand & $7F) 'mask off number so it is 0-127
   enemy_per_screen[map_n]&=$0F 'mask off enemy that is there
   enemy_per_screen[map_n]|=$30 'set skull bat
   
PUB Load_Enemies(map_nbr) | n, temp_num, temp_enemy, temp_x, temp_y, wall1
  'check que to see if map is in there

  wall1:=Load_enemy_number(MapXY)
  
  if(wall1==$FF) 
   'grab number of enemies
   temp_num:=enemy_per_screen[map_nbr]&$0F
  else  
   temp_num:=wall1

  ''no enemies can be on this screen
  if(ScreenMap[Constant(16*11)]==0)
    temp_num:=0
    
  'grab enemy type
  temp_enemy:=((enemy_per_screen[map_nbr])>>4)
  ''load up the bad guy sprites if they are different from what is already loaded up
  if(temp_enemy <> enemy_on_map)
   ''move enemy sprites off screen
   repeat n from 2 to 7
     BYTE[sprite_y_adr-n]:=0
   LoadEnemySprites(temp_enemy)

  'clear out prior enemies/items
  repeat n from 0 to 5
       enemy_type[n]:=0
          
 'make sure there are enemies on the screen
 if(temp_num>0)
  repeat n from 0 to (temp_num-1)   
   'put up an enemy
   enemy_type[n]:=temp_enemy
   wall1:=1
   ''make sure we don't place an enemy on a wall
   repeat while wall1<>0
     ''map is 16x11
     '' we only want to grab 12x7 - leave 2 tiles on each border open
     temp_x:=((?rand & $08) + (?rand & $02) + (?rand & $01))+2
     temp_y:=((?rand & $04) + (?rand & $02))+2
     ''check to make sure we are not on a wall
     wall1:=BYTE[@wall_data+ScreenMap[((temp_y<<4)+temp_x)]]
   enemy_x[n]:=temp_x<<4 'rand 16 *16
   enemy_y[n]:=(temp_y<<4)+32 'rand 11 *16   
   enemy_dir[n]:=((?rand & $03)<<4)
   enemy_change_dir[n]:=byte[@bad_guy_path+(?rand & $03)]   
   enemy_hit_timer[n]:=0 'timer for enemy to fly back
   enemy_hp[n]:=3 | (?rand & $80)'randomize if bad guy is fast or not                    
   enemy_ani_timer[n]:=$FF 'time until next movement           
PUB Load_Font
  LoadFile(string("ranq.fnt"))        
  ReadFile(CONSTANT(126*8), 0, Long[onebpp_tile_adr], 123) '123

PUB Load_Music
   ''load keyboard driver
 music_ptr:=fat.Return_Address 
 LoadFile(string("ranq.mus"))
 ReadFile(1060, 0, music_ptr, 19) '19
 music_bass_start:=384  
PUB Start_Music
  Music_state:=1 'start music
  music_location[0]:=music_ptr ''grab address of song
  music_location[1]:=music_ptr+LONG[@music_bass_start] ''grab address of song  
  music_note_length[0]:=0 ''grab next note
  music_note_length[1]:=0 ''grab next note
PUB Play_Music | music_note, music_hold, note_delay, n
 if(Music_state)
  repeat n from 0 to 1
   if(music_note_length[n]<1)
     music_note:=WORD[music_location[n]]
     ''if we reach the end of the song, start over
     if(music_note==$FFFF)
        if(n==0)
          music_location[n]:=music_ptr ''grab address of song
        else
          music_location[n]:=music_ptr+LONG[@music_bass_start] ''grab address of song        
        music_note:=WORD[music_location[n]]
     music_hold:=music_note&$8000 'used to string notes together
     music_note:=(music_note&$3FF) ''clip music notes, freq is only 0-1023
     music_location[n]+=2
     music_note_Length[n]:=WORD[music_location[n]]
     music_location[n]+=2     

     'set note fading
     if(music_note>0)         
          music_note_delay:=4'4, 8
       if(music_hold)
          music_note_delay:=32' '16 '8 '255
       if(n==0)
          music_volume[n]:=4'2
          'music_note_delay:=8
          music_note_delay:=255
       else
            music_volume[n]:=4    
       music_volume[n+2]:=music_note_delay
       'sn.play(channel, note, volume (0-high, 15-off) )
       sn.play(n, music_note, music_volume[n])
     else
       music_volume[n]:=15
       sn.setVolume(n, 15)
 
   music_note_length[n]--

   ''fade note
   if(music_volume[n+2]<1)
    music_volume[n+2]:=music_note_delay
    if (music_volume[n] += 1) > 15                  
        music_volume[n] := 15
    sn.setVolume(n, music_volume[n])
   music_volume[n+2]--
 else
       sn.setVolume(0, 15)
       sn.setVolume(1, 15) 
PUB LoadSprites
 LoadFile(string("ranq.spr"))
'ReadFile(number_of_bytes, data_offset, data_ptr, do_crc)
 ReadFile(Constant(max_sprites-1), 0, @SpriteFile, 179) ' CRC - 179
PUB LoadEnemySprites(sprite_set) | max_size, sprite_offset, crc_number
  LoadFile(string("ranq.spr"))
  ''choose which enemy to load up
  case sprite_set
      'pig guy
      1: sprite_offset:=CONSTANT(19*256)'pig guy
        max_size:=CONSTANT((4*256)-1)'how much data
        crc_number:=223 'CRC - 223
      'skull bat
      2: sprite_offset:=CONSTANT(25*256)
        max_size:=CONSTANT((2*256)-1)'how much data
        crc_number:=107 'CRC - 107
      'ghost
      3: sprite_offset:=CONSTANT(23*256)
        max_size:=CONSTANT((2*256)-1)'how much data
        crc_number:=27 'CRC - 27
      'player dying
      4:
        sprite_offset:=CONSTANT(27*256)
        max_size:=CONSTANT((4*256)-1)'how much data
        crc_number:=-1
      
  'load enemy sprites 
 'ReadFile(number_of_bytes, data_offset, data_ptr, do_crc)
  ReadFile(max_size, sprite_offset, @SpriteFile+CONSTANT(max_sprites-(256*4)), crc_number)

PUB LoadTiles(tile_set) | crc_count
  ''load graphics tiles
 if(byte[tile_set]<>current_tile_set)
  current_tile_set:=byte[tile_set]
  case(tile_set)
    1: LoadFile(string("ranq1.til"))  ''245
       crc_count:=245
    2: LoadFile(string("ranq2.til"))   ''54
       crc_count:=54
    3: LoadFile(string("ranq3.til"))  ''159
       crc_count:=159    
'ReadFile(number_of_bytes, data_offset, data_ptr, do_crc)
  ReadFile(Constant(max_tiles-1), 0, @TileFile, crc_count)
''debug - draw CRC
' BYTEFILL(Long[tilemap_adr]+CONSTANT(0+8), text_blank, 5)          
' Int_To_String(9,0, CRC_Debug)'draw text   

  ''load tile data that builds a 16x16 tile from 4 8x8 tiles
  case(tile_set)
    1: LoadFile(string("rqtiles1.dat"))  ''83
       crc_count:=83    
    2: LoadFile(string("rqtiles2.dat"))  ''3
       crc_count:=3    
    3: LoadFile(string("rqtiles3.dat"))  ''17
       crc_count:=17    
  ReadFile(Constant((max_16x16_tiles*4)-1), 0, @Tile_16x16, crc_count)


' BYTEFILL(Long[tilemap_adr]+CONSTANT(0+17), text_blank, 3)          
' Int_To_String(17,0, CRC_Debug)'draw text   

PUB MountCard | fin, n

  'repeat until n2 == 1
   fin:=0   
   repeat n from 0 to max_mount_time
    'fin:=  \fsrw.mount(spiSel,spiClr,spiCnt,spiDI,spiDO,spiClk) ' Attempt to mount SD card
    if \fat.FATEngineStart(spiDO,spiClk,spiDI,spiCS,-1,-1,-1,-1,-1) <> true
      ''debug.str(string(13,"start fail",13))
      next
    fin := \fat.mountPartition(0)
    if fat.partitionError
      ''debug.str(string("mount fail",13))
      ''debug.str(fin)
      ''debug.tx(13)
      next
    quit
    
   'if fin <0
    'Print_String(0,1,string("Test idk"))
   'else
    'Clear_Screen
    'quit

   'if fin >-1
    'quit
   
PUB Random_treasure | x,y,q1,q2,tr_check
   ''randomly place the 6 treasures
   ''we want to divide the treasure into 4 quads first, then randomly place the other 2
  tr_check:=0
  repeat while tr_check==0
    ''quad 1 - upper left
    treasure_location[0]:=(get_treasure_rand<<4)|get_treasure_rand
    ''quad 2 - upper right
    treasure_location[1]:=((get_treasure_rand+5)<<4)|get_treasure_rand
    ''quad 3 - lower left
    treasure_location[2]:=(get_treasure_rand<<4)|(get_treasure_rand+5)
    ''quad 4 - lower right
    treasure_location[3]:=((get_treasure_rand+5)<<4)|(get_treasure_rand+5)    

    ''treasure 5, random quad
    q1:=(?rand & $03)
    get_treasure_r2(q1,4)

    ''treasure 6, random quad, not same quad as treasure 5
    q2:=q1 ''temp for while loop
    repeat while q2==q1 ''make sure we are not in the same quad as treasure 5
       q2:=(?rand & $03)
    get_treasure_r2(q2,5)
    ''check to make sure treasure chests aren't on a screen they can't be on
    y:=0
    ''check to make sure each screen that has chest can have one
    repeat x from 0 to 5
      loadmap(treasure_location[x])
      y+=ScreenMap[Constant((16*11)+1)]
    ''if all the screens can have chests, then we can move on
    '' otherwise re-randomize the treasures
    if(y==6)
       tr_check:=1      
PUB get_treasure_rand | n
   ''make sure we fall in the limits each quad is 5x5
    n:=9
    repeat while n>4
     n:=(?rand & $07)
    return(n)
PUB get_treasure_r2 (quad, treasure) | x1,y1
'' get treasures 5 and 6
    x1:=0
    y1:=0    
    if(quad&1)
       x1:=5
    if(quad&2)
       y1:=5
    treasure_location[treasure]:=treasure_location[quad]''temp for while loop       
    repeat while treasure_location[treasure]==treasure_location[quad] 
      treasure_location[treasure]:=((get_treasure_rand+x1)<<4)|(get_treasure_rand+y1)
PUB Draw_Treasures| n
  ''draw treasures that you have picked up
  BYTEFILL(Long[tilemap_adr]+CONSTANT(32+24), CONSTANT(128+1), 6)   
  if(treasure_found>0)
   BYTEFILL(Long[tilemap_adr]+CONSTANT(32+24)+(6-treasure_found), CONSTANT(128+2), treasure_found)    
PUB Draw_Life
  ''draw empty life circles
  BYTEFILL(Long[tilemap_adr]+CONSTANT(32+2), CONSTANT(128+4), 6)
  ''fill up life circles with amount of life player has
  if(guy_hp>0)
   BYTEFILL(Long[tilemap_adr]+CONSTANT(32+2), CONSTANT(128+6), guy_hp)
  else ''show death screen
    Player_Died 
PUB Draw_Gold
  ''cap gold at 999
  if(guy_gold>999)
    guy_gold:=999 
  'erase prior line
  BYTEFILL(Long[tilemap_adr]+CONSTANT(32+17), text_blank, 3)          
  Int_To_String(17,1, guy_gold)'draw gold    
PUB Print_String(x,y,adr) | n, text_adr, char_value
'' prints a string
  n:=0
  text_adr:=adr
  char_value:=32
  
  repeat until char_value < 1
   char_value:=byte[text_adr+n]
   char_value&=127
   if(char_value<>0)
     tv_game_gfx.Place_Tile_XY(x+n,y,(char_value)+text_offset )''write characer
     n++
PUB Int_To_String(x,y, i) | t, c_value, max_c
' does an sprintf(str, "%05d", i); job
 if i<10
  max_c:=0
 elseif i<100
  max_c:=1

 else
  max_c:=2
{
  'debug - remove 1000s place
 elseif i<1000
  max_c:=2
 elseif i<10000
  max_c:=3
 else
  max_c:=4
}
 x+=max_c
 repeat t from 0 to max_c
  c_value := (i // 10) + 48
  tv_game_gfx.Place_Tile_XY(x,y,(c_value)+text_offset)
  i/=10
  x--      
PUB Pause_game
 Do_Textbox(16) ''offset for first text box
 Wait_Key ''wait for key hit
PUB Clear_Screen
  BYTEFILL(Long[tilemap_adr], text_blank, CONSTANT(32*24))          
PUB Do_Waterfall
'animate waterfall
'-waterfall tiles - 80,81,82, destination for waterfall tile:91
'5120, 5184, 5248
 waterfall_timer++
 waterfall_timer&=7
 if waterfall_timer==0
{   '57 bytes
   waterfall_frame++
   if(waterfall_frame>2)
     waterfall_frame:=0
   BYTEMOVE(@TileFile+CONSTANT(91<<6), @TileFile+((80+waterfall_frame)<<6), 64)
}
'{  '52 bytes
   waterfall_frame+=64
   if(waterfall_frame>5248)
     waterfall_frame:=5120
   BYTEMOVE(@TileFile+CONSTANT(91<<6), @TileFile+waterfall_frame, 64)
'}
PUB Do_Textbox(txt_offset) | x,y, cntr, n
'14x6 (7th line to tell if it is done or more text)
'128 solid block, 159 - black block

 'grab sword location
 Sword_Y:=BYTE[sprite_y_adr-1]
 'move all sprites off screen
 repeat n from 0 to 7
   BYTE[sprite_y_adr-n]:=0

 'draw borders
 BYTEFILL(Long[tilemap_adr]+CONSTANT((32*8)+8), 128, 16)
 BYTEFILL(Long[tilemap_adr]+CONSTANT((32*17)+8), 128, 16)
  x:=Long[tilemap_adr]+CONSTANT((32*9)+8)
  repeat cntr from 0 to 7
    byte[x]:=128
    x+=15
    byte[x]:=128
    x+=17
 ''clear out center of block
 Clear_TextBox
  ''load up text file
  LoadFile(string("ranq.txr"))
  x:=0
  y:=0
  fat.fileSeek(txt_offset)'go to text we want to read
  cntr:=0
  repeat 
    fat.readData(@n,1)
    if n==94 OR n==0 '94=^
     quit
    ''setup text in text box
    if(n>31 AND x<14)
     ''draw text
     tv_game_gfx.Place_Tile_XY(x+9,y+10,(n+text_offset))''write character
     ''play sound, skip the sound if it is a space
     if(n>32)
      Play_Sound(8)
      repeat 7
         Update_Frame
         
     ''advance cursor
     x++
      
    ''handle line feed
    if n==13
       x:=0
       y++
    ''make sure we don't draw past text box
    if(y>5)
      quit
    cntr++
    ''make sure we do not get stuck in the loop
    if(cntr>10000)
       quit
  fat.closeFile

PUB Wait_Key | n
n:=0
repeat until n<>0
 ''read gamepad    
 if(Start_Game_Var)
   rand++
 else
   Update_Frame
 joypadold:=joypad ''buffer joypad info for checking taps
 joypad := joy.Read_Gamepad(NesBasePin)
 if(Check_Tap_Key(CONSTANT(JOY0_B|JOY0_START)) <> 0)
  n:=1

if(Start_Game_Var==0)
 ''redraw screen
 DrawMap
 ''replace sprites
  ''replace player and sword
  BYTE[sprite_y_adr]:=player_y
  BYTE[sprite_y_adr-1]:=Sword_Y
  'replace enemy sprites
  repeat n from 0 to 5
   if enemy_type[n]>0
    BYTE[sprite_y_adr-2-n]:=enemy_y[n]
  ''get joypad info
  'joypad := joy.Read_Gamepad

joypadold:=joypad ''buffer joypad info for checking taps
PUB Clear_TextBox | n, adr
  adr:=Long[tilemap_adr]+CONSTANT((32*9)+9)
  repeat n from 0 to 7
   BYTEFILL(adr, text_blank, 14)
   adr+=32

PUB LoadFile(file_name) | f_status, fr

   
''try to load file 10 times, if it finds it once, quit, if not then quit program
   'repeat n from 0 to 10
    f_status:=0
    fr := \fat.openFile(file_name,"R")
    if fat.partitionError
      repeat
       'debug.str(string("file "))
       'debug.str(file_name)
       'debug.str(string(" not found!",13))
       ''debug.str(fr)
       ''debug.tx(13)
   
     
Pub ReadFile(number_of_bytes, data_offset, data_ptr, do_crc)|data_byte,n,crc_count, crc_check, next_frame
''number_of_bytes - how many bytes we will read
''data_offset - where in the data file we start reading data
''data_ptr - where we will put our data
''do_crc - if -1, do not do CRC check, if anything higher, that will be the CRC
  crc_check:=0
  next_frame:=cnt+(clkfreq/60) 'this is used to keep updating the frame so audio does not stutter
  'loop 40 times until we get a crc check that matches
  repeat crc_check from 0 to 40
   crc_count:=0
   fat.fileSeek(data_offset)
   ''read data
   repeat n from 0 to number_of_bytes
    ifnot fat.readData(@data_byte,1)
     quit
    'if(data_byte>-1)
    data_byte&=$FF
    BYTE[data_ptr+n]:=data_byte
    crc_count+=data_byte
    ''after so many bytes, update frame
    if((cnt-next_frame) > 0)
           Update_Sound ''process sound
           next_frame:=cnt+(clkfreq/60)
   crc_count&=$FF
   'check to see if we pass the crc check
   if((crc_count)==do_crc OR do_crc==-1)
'     Print_String(0,0,string("CRC pass"))
      CRC_Debug:=crc_count
      quit
  fat.closeFile
  
  ''crc check failed after 20 attempts, no reason to beleive it will pass after this
  if(crc_check>38)
      File_Corrupt
      Abort

PUB File_Corrupt
      Print_String(0,1,string("File data corrupt "))

'PUB Screen_Grab | current_line, pixel_adr, cur_pixel, pixel_4,packet
'{
  '' Stop the TV driver, send 16 bits of X pixel width, send 16 bits of Y pixel
  ''    height, send the screen data, then when all is finished send $FF to tell
  ''    client no more pixel data is coming. After that is done start up the TV
  ''    driver.
{  
 if(screen_cap)'' make sure serial driver is loaded up
  tv_game_gfx.tv_stop
'  uart.start(31, 30, 115200) 'start serial driver for screen capture
  ''send X screen width - 16 bits big endian
  uart.tx($01) ''256
  uart.tx($00)
  ''send Y screen length - 16 bits big endian
  uart.tx($00) ''192
  uart.tx($C0)
  
  ''send screen data   
  current_line:=0
  repeat 192
    long[request_scanline]:= current_line
    pixel_adr:=SCANLINE_BUFFER
    ''pixel_adr+=(255-32)
    ''read scanline and send scanline
    repeat 256
     cur_pixel:=byte[pixel_adr] 
     pixel_adr+=1
     uart.tx(cur_pixel) ' - send byte
     {
     '' this will loop until we receive a valid byte
     repeat
       packet:=uart.rxcheck
     while packet==-1  'wait until valid byte is received
      }
   current_line+=1
  uart.tx($FF) ' - all done
'  uart.stop 'stop UART driver  
  ''start tv driver up again
  tv_game_gfx.tv_start
}     
'}
DAT
''text file info
''16 - pause
''54 - treasure 1
''120 - treasure 2
''186 - treasure 3
''251 - treasure 4
''318 - treasure 5
''383 - treasure 6
text_pointers 'pointers to text file
long 54,120,186,251,318,383
Inn_Locations 'map locations of the Inns
byte $17,$20,$85,0
player_animation 'walking animation
byte 0,1,2,1
''4 8x8 tiles combined into a 16x16 tile
turn_enemy_right 'lookup table to have a enemy rotate right if it hits a wall
byte  2,3,1,0
pig_frame_lut 'look up table for pig animation for which sprite to use
byte 0,1,2,2
bad_guy_path 'lookup table to deterimine how long a bad guy will walk until he changes direction
byte  10, 14, 17,20
enemy_on_screen_lut
byte 4,5,6,0
flash_gfx 'flash animation after enemy is killed
byte 12,13,12,0                  

''determines if we can walk through a tile or not
'' 0-walk, 1-wall,
wall_data
byte 0 ''0 -grass
byte 1 ''1- tree, upper left
byte 1 ''2- tree, upper right
byte 1 ''3- tree lower left
byte 1 ''4- tree lower right
byte 1 ''5- curved rock
byte 1 ''6- curved rock
byte 1 ''7- water
byte 1 ''8- rock upper left corner
byte 1 ''9- rock upper right corner
byte 1 ''10- rock lower left corner
byte 1 ''11- rock lower right corner
byte 1 ''12- rock vertical with wall top
byte 1 ''13- rock vertical with wall bottom
byte 1 ''14 - horizontl rock with wall left
byte 1 ''15 - horizontl rock with wall right
byte 1 ''16 - rock as wall/mountain
byte 1 ''17 - lower left corner with rock and grass
byte 1 ''18 - lower right corner with rock and grass
byte 1 ''19 - wall left side
byte 1 ''20 - wall right side
byte 1 ''21 - wall corner lower left side
byte 1 ''22 - wall right lower right side
byte 1 ''23 - rock odd angel lower left
byte 1 ''24 - rock odd angel lower right
byte 1 ''25 - rock odd angel lower left w/ grass
byte 1 ''26 - rock odd angel lower right w/ grass
byte 1 ''27 - rock lower left corner
byte 1 ''28 - rock as wall/mountain with angle in lower left w/ water
byte 1 ''29 - rock as wall/mountain with angle in lower left w/ grass
byte 1 ''30 - rock as wall/mountain with angle in lower right w/ water
byte 1 ''31 - rock as wall/mountain with angle in lower right w/ grass
byte 1 ''32 - rock lower right corner
byte 1 ''33- tree, upper left, against another tree
byte 1 ''34- tree, upper right, against another tree
byte 1 ''35- tree, upper left, against a rock
byte 1 ''36- tree, upper right, against a rock
byte 0 ''37- ladder
byte 1 ''38- rock wall
byte 1 ''39 - castle wall
byte 1 ''40 - curved wall left
byte 1 ''41 - curved wall right
byte 1 ''42 - curved wall left base
byte 1 ''43 - castle wall right base
byte 2 ''44 - cave entrance
byte 1 ''45 - castle window
byte 1 ''46 - waterfall  
byte 3 ''47 - treasure
byte 2 ''48 - door
byte 1 ''49 - wall
byte 0 ''50 - ground
byte 2 ''51 - door
byte 4 ''52 - INN door
byte 5 ''53 - Healer


''Sound data - high byte value, low byte value, wavetype & volume, duration
'' wave type are bits 4-7, volume are bits 0-3
''255,255,255,255 ends sequence
Sword_flash ''sound when player uses sword
byte 0,4,$10,3, 0,2,$14,6, 0,1,$18,6, 255,255,255,255

''sound that happens when you hit a bad guy
Bad_guy_hit
byte 0,8,$10,3, 0,12,$10,2, 0,16,$10,1, 255,255,255,255

''sound that happens when you kill a bad guy
Bad_guy_die
byte 0,8,$10,6, 0,12,$10,4, 0,16,$10,2, 255,255,255,255

''sound that happens when player it hurt
Player_hit
byte $01,$68,$05,1, $01,$E0,$03,2, 2,50,$00,1, 8,0,$03,2, 255,255,$00,255
'byte $01,$68,$05,1, $01,$E0,$03,2, 0,50,$10,1, 0,75,$13,2, 255,255,$00,255

'picks up a single coin
Coin_grab
byte 0,200,$04,1, 0,144,$00,3, 0,144,$06,2, 255,255,255,255

'picks up a coin bag
Multi_Coin_grab
byte 0, 200,$04,1, 0,144,$00,1, 0,200,$04,1, 0,144,$00,1, 0,200,$04,1, 0,144,$00,1, 255,255,255,255

'sound for picking up potion
Drink_Potion
byte $01,$2C,$04,1, $01,$2C,$00,2, $01,$5E,$0C,2, 0,200,$04,1, 0,200,$00,2, 0,250,$0C,2, 0,100,$04,1, 0,100,$00,2, 0,150,$0C,1, 255,255,255,255

'sound that plays for each letter displayed on the text screen
Text_Sound
byte 0, 200,$04,1, 0,144,$00,1, 255,255,255,255

''Tune that plays when player dies  G-571, E-679, A-1017
Player_die
'byte $02,$3B,$02,16, $02,$A7,$02,16, $03,$F9,$02,32, 255,255,255,255
byte $02,$3B,$02,8, $02,$A7,$02,8, $03,$F9,$02,24, 255,255,255,255

'stop sound
Sound_Stop
byte 255,255,255,255