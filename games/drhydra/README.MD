Dr. Hydra
=========

## Video
TODO

## Info
- Type: Game
- Genre: Puzzle
- Author(s): Remi 'Remz' Veilleux, Louis-Philippe 'FoX' Guilbert
- First release: 2006
- Improved version: No
- Players: 2
- Special requirements: None
- Video formats: NTSC
- Inputs: 2x Gamepad. Keyboard

## Description
Basically Dr. Mario in anything but name, except with a nice archeology theme.
Includes single player, 2-player and player vs. computer modes.


## Controls
|Gamepad|Player 1's Keyboard|Player 2's Keyboard|Action|
|------|--------------------|-------------------|------|
|B/Y|Y/G|Num ./Num 0|Rotate bone|
|D-pad|WASD|Arrow Keys|Move bone|
|B|Y|Num .|Menu: confirm|
|Y|G|Num 0|Menu: back|

Note: Remember that Z and Y are swapped on QWERTZ keyboards!


