''*******************************
''* PROPMIND.spin v1.0   9/2019 *               
''* Author: Werner L. Schneider *
''*******************************
''
'' Based on Code from Propeller Game Engine Demo (C) 2013 Marco Maccaferri 
'' Based on Code from Marko Lukat
'' Based on PS/2 Keyboard Driver v1.0.1 by Chip Gracey Copyright (c) 2004 Parallax, Inc.
'' Based on AYcog - AY3891X / YM2149F emulator V0.8 (C) 2012 Johannes Ahlebrand                                 
''
''+------------------------------------------------------------------------------------------------------------------------------+
''|                                   TERMS OF USE: Parallax Object Exchange License                                             |
''+------------------------------------------------------------------------------------------------------------------------------+
''|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    |
''|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
''|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
''|is furnished to do so, subject to the following conditions:                                                                   |
''|                                                                                                                              |
''|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
''|                                                                                                                              |
''|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
''|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
''|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
''|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
''+------------------------------------------------------------------------------------------------------------------------------+
''
''
CON

    _XINFREQ = 5_000_000
    _CLKMODE = XTAL1 + PLL16X

    timerpin   = 2                      ' P0 used for Timer

    right      = 10                     ' P10 Audio Right
    left       = 11                     ' P11 Audio Left

    kbd        = 8                     ' P26 Keyboard Data                                                                    Q
    kbc        = 9                     ' P27 Keyboard Clock

    scl        = 28                     ' P28 SCL Propeller EEPROM
    sda        = 29                     ' P29 SDA       "

    JOY_CLK = 16
    JOY_LCH = 17
    JOY_DATAOUT0 = 19
    JOY_DATAOUT1 = 18
    

OBJ

    ay1:     "AYCOG"                    ' AY3891X Emulator     need 1 Cog 

    data:    "GRAPHICS"                 ' Tiles + Sprites      Main Cog

    'i2c:     "I2CSPIN"                  ' EEPROM Access        Main Cog

    prng:    "JKISS32"                  ' Pseudo Random        Main Cog 

    kb:      "KEYBOARD"                 ' Keyboard Driver      need 1 Cog

    play:    "PLAY"                     ' Sound Driver         need 1 Cog

    rr:      "REALRANDOM"               ' Random Generator     need 1 Cog at Startup, then release Cog

    render:  "RENDERER_320x240"         ' Renderer             need 3 Cog's

    driver:  "TV_DRIVER"               ' VGA Driver           need 1 Cog

    
VAR

    long frame_buffer[80]
    long link

    long ay1_regs           

    long ptr
    long ticks
    long score

    long pad_state,pad_prev,pad_press
    
    byte music
    byte color
    byte column
    byte row
    byte samecolors
    
    byte arrow_y
    byte arrow_d
    byte arrow_move

    byte minutes
    byte seconds

    byte tmp[16]
    byte buffer[128]

    byte riddle[4]
    byte riddle_c[4]
    byte player[4]
    
    byte green[4]
    byte white[4]
    byte all[4]
        
    byte res_x[4]
    byte res_y[4]


PUB start | i, y, state, finish, mel, wait
    init

    samecolors := 1

    music := 1
    
    state := 0

    repeat

        flip
        NES_read_Gamepad

        case state

            0:

                intro

                state := 1

            1:

                color := 1
                column := 0
                row := 0
                
                arrow_y := ((color-1) * 16) + 80
                arrow_d := 2                        ' Down
                arrow_move := 0
                
                repeat i from 0 to 3
                    riddle[i] := 0
                    player[i] := 0

                initBoard
                
                printArrow

                wait := 100

                state := 2

            
            2:

                state := 2

                if wait > 0
                    wait--
                    if wait == 0

                        cycleRiddle 

                        state := 3

                        wait := 50

            3:

                state := 3

                if wait > 0
                    wait--
                    if wait == 0
                        startTimer(timerpin)
                        play.melody(@melody1, music)

                if play.stat == 0
                    play.melody(@melody1, music)

                moveArrow

                selectColumn

                blinkColumn

                if wait == 0
                    printTime

                checkMusic

                if kb.keystate($20) or ((pad_state&NES0_A) and not (pad_state&NES0_SELECT))              ' Space 

                    setColor

                if kb.keystate($72) or ((pad_state&NES0_B) and (pad_state&NES0_SELECT))              ' R 

                    play.melody_stop

                    stopTimer(timerpin)

                    sprite_hide(1)

                    state := 1

                if kb.keystate($0D) or ((pad_state&NES0_START) and not (pad_state&NES0_SELECT))         ' Enter 

                    if player[0] <> 0 and player[1] <> 0 and player[2] <> 0 and player[3] <> 0

                        play.tone(@toneRowFull, music)

                        state := 4

                    else

                        play.tone(@toneError, music)


                if kb.keystate($CB)  or ((pad_state&NES0_START) and (pad_state&NES0_SELECT))             ' Esc 

                    play.melody_stop

                    stopTimer(timerpin)

                    sprite_hide(0)
                    sprite_hide(1)

                    wordfill(@map1, $0000, render#MAP_SIZE_WORD)

                    repeat
                      NES_read_gamepad
                      ifnot kb.keystate($CB)  or ((pad_state&NES0_START) and (pad_state&NES0_SELECT))
                        quit

                    state := 0

                if kb.keystate($DC)              ' Print Screen  

                    printRiddle

                    repeat

                        printTime 

                        ifnot kb.keystate($DC)   ' Released Print Screen  
                            quit
                            
                    printHide    


            4:

                sprite_hide(1)

                if testRow

                    printRiddle

                    state := 6

                else

                    repeat i from 0 to 3
                        player[i] := 0

                    column := 0
                        
                    if row == 11

                        state := 5

                        printRiddle

                        endGame

                    else

                        row++

                        updateColumn

                        printRow

                        state := 3
                    
            5:

                ticks := phsa
                seconds := ticks // 60

                if (seconds & 1) == 0

                    printxy(0, 7, $0, $52)

                else

                    printxy(0, 7, $7, $52)

                if kb.keystate($72) or ((pad_state&NES0_B) and (pad_state&NES0_SELECT))        ' R 

                    play.melody_stop

                    stopTimer(timerpin)

                    sprite_hide(1)

                    state := 1

                if kb.keystate($CB)  or ((pad_state&NES0_START) and (pad_state&NES0_SELECT))  ' Esc 

                    play.melody_stop

                    stopTimer(timerpin)

                    sprite_hide(0)
                    sprite_hide(1)

                    wordfill(@map1, $0000, render#MAP_SIZE_WORD)

                    state := 0

            6:

                score := ticks + ((row + 1) << 12)  

                if enterName

                    state := 0

                else

                    state := 5


PRI init | i, magic, points, addr

    rr.start                                                            ' Start Real Random, uses a COG
    repeat while rr.random == 0                                         ' Wait for Real Random to warm up

    prng.start                                                          ' Start the Pseudo Random Number generator
                                                                        ' (Uses default seed values)
    prng.seed (rr.random, rr.random, rr.random, rr.random, rr.random)   ' Seed the PRNG with real random numbers
    rr.stop                                                             ' Free Real Random COG

    kb.start(kbd, kbc)                                                  ' Start Keyboard Driver

    waitcnt((clkfreq / 1000) * 1000 + cnt)  ' wait 1000 ms

    link{0} := @frame_buffer{0} << 16 | @frame_buffer{0}
    driver.start(@link{0})

    render.start(@frame_buffer, @link, @map1, data.get_tile_def, data.get_palette_def, data.get_sprite_def, @sprite_state)

    ay1_regs := ay1.start(right, left)                                  ' Start the emulated AY chip in one cog
    ay1.resetRegisters                                                  ' Reset all AY registers

    play.start(ay1_Regs)

    'i2c.start(scl, sda)

    'i2c.read(1, $7F00, @magic)                                          ' read test byte to see if MAP stored
    points := $1000 + 60
    {                                               
    if magic <> 45
        magic := 45
        i2c.write(1, $7F00, @magic)
        i2c.write(12, $7F10, @strMe)
        i2c.write(4, $7F1C, @points)
        addr := $7F20
        repeat i from 0 to 5
            points := $FFFF
            i2c.write(12, addr, @strNoName) 
            addr += 12
            i2c.write(4, addr, @points)
            addr += 4
      }


DAT
''********************************************************************
''* Functions for Time                                               *
''********************************************************************
''
PRI startTimer(pin)                            ' Code-Snippet from Jon McPhalen, It spares me a Cog ;-) 

''  Starts free-running oscillator/counter
''  -- pin will output 1Hz
''  -- phsa holds seconds count

    ctra := (%01010 << 26) | pin               ' ctra is pos-edge counter on pin
    frqa := 1
    phsa := 0  '-1  

    ctrb := (%00100 << 26) | pin               ' ctrb is 1Hz oscillator on pin
    frqb := ($8000_0000 / clkfreq) << 1                       
    dira[pin] := 1


PRI stopTimer(pin)

    ctra := 0
    ctrb := 0
    phsa := 0
    dira[pin] := 0
    

PRI printTime 

    ticks := phsa
    minutes := ticks / 60
    seconds := ticks // 60

    dec2(8, 1, $0, minutes) 
    dec2(constant(8+3), 1, $0, seconds) 


DAT
''********************************************************************
''* Functions for Riddle                                             *
''********************************************************************
''
PRI cycleRiddle | rounds, wait

    rounds := (prng.random & $0F) + 15        ' Random 15 - 30

    repeat rounds

        createRiddle
        printRiddle

        play.noise(@noise1)
        
        wait := ((prng.random & $03) + 1) * 100  ' Random 100 - 300   

        waitcnt((clkfreq / 1000) * wait + cnt)   ' wait 100 - 800 ms


    createRiddle
    printHide


PRI printRiddle

    printPin(26, 1, riddle[0])
    printPin(28, 1, riddle[1])
    printPin(30, 1, riddle[2])
    printPin(32, 1, riddle[3])


PRI printHide

    printPin(26, 1, 9)
    printPin(28, 1, 9)
    printPin(30, 1, 9)
    printPin(32, 1, 9)


PRI createRiddle | i, rcolumn, newcolor

    repeat i from 0 to 3
        riddle[i] := 0

    rcolumn := 0
    repeat
        newcolor := (prng.random & $7) + 1        ' Random 1 - 8

        if testColor(newcolor)
            riddle[rcolumn] := newcolor
            rcolumn++

        if rcolumn == 4
            quit


PRI testColor(rcolor) | i, count

    count := 0
    repeat i from 0 to 3
        if riddle[i] == rcolor
            count++

    if count > samecolors-1
        return false

    return true


DAT
''********************************************************************
''* Functions for Column                                             *
''********************************************************************
''
PRI setColor | i, count

    count := 0
    repeat i from 0 to 3
        if player[i] == color
            count++

    if count > samecolors-1
        play.tone(@toneError, music)
        return

    play.tone(@toneSet, music)

    player[column] := color        

    printPin(pin_x[row] + (column * 2) , pin_y[row], player[column])

    column++
    if column == 4
        column := 0

    dec(9, 27, $0, column+1)

    repeat
        NES_read_Gamepad
        ifnot kb.keystate($20) or ((pad_state&NES0_A) and not (pad_state&NES0_SELECT)) ' Space 
            quit
        printTime
        blinkColumn


PRI selectColumn

        if kb.keystate($C0) or ((pad_state&NES0_LEFT) and not (pad_state&NES0_SELECT)) ' Left 
            if column == 0
                column := 3
            else
                column--
            updateColumn
            repeat
                NES_read_Gamepad                
                ifnot kb.keystate($C0) or ((pad_state&NES0_LEFT) and not (pad_state&NES0_SELECT))
                    quit
                printTime
                blinkColumn
                
        elseif kb.keystate($C1) or ((pad_state&NES0_RIGHT) and not (pad_state&NES0_SELECT)) ' Right
            if column == 3
                column := 0
            else
                column++
            updateColumn
            repeat
                NES_read_Gamepad
                ifnot kb.keystate($C1) or ((pad_state&NES0_RIGHT) and not (pad_state&NES0_SELECT))
                    quit
                printTime
                blinkColumn
                

PRI updateColumn

    dec(9, 27, $0, column+1)
    
    printPin(pin_x[row], pin_y[row], player[0])
    printPin(pin_x[row] + 2, pin_y[row], player[1])
    printPin(pin_x[row] + 4, pin_y[row], player[2])
    printPin(pin_x[row] + 6, pin_y[row], player[3])

    if player[column] <> 0
        sprite_show(1, 1, 0, (pin_x[row] + (column * 2)) * 8, pin_y[row] * 8)     ' Sprite 1 = Select
    else
        sprite_show(1, 1, 1, (pin_x[row] + (column * 2)) * 8, pin_y[row] * 8)     ' Sprite 1 = Select
    

PRI blinkColumn

    if (seconds & 1) == 0

        if player[column] <> 0
            sprite_show(1, 1, 0, (pin_x[row] + (column * 2)) * 8, pin_y[row] * 8)     ' Sprite 1 = Select ? Black
        else
            sprite_show(1, 1, 1, (pin_x[row] + (column * 2)) * 8, pin_y[row] * 8)     ' Sprite 1 = Select ? White

    else

        sprite_hide(1)


DAT
''********************************************************************
''* Functions for Arrow                                              *
''********************************************************************
''
PRI moveArrow

    if arrow_move == 0

        if kb.keystate($C2) or ((pad_state&NES0_UP) and not (pad_state&NES0_SELECT))   ' Up 
            arrow_d := 1
            printArrow
            if color > 1
                color--
                dec(9, 28, $0, color)
                arrow_move := 4 

        elseif kb.keystate($C3) or ((pad_state&NES0_DOWN) and not (pad_state&NES0_SELECT)) ' Down
            arrow_d := 2
            printArrow
            if color < 8
                color++
                dec(9, 28, $0, color)
                arrow_move := 4 

    else
        if arrow_d == 1                   ' Up
            arrow_y -= 4

        elseif arrow_d == 2               ' Down
            arrow_y += 4

        printArrow
        arrow_move--


PRI printArrow

    sprite_show(0, 0, 0, 20, arrow_y)         ' Sprite 0 = Arrow


DAT
''********************************************************************
''* Functions for Row                                                *
''********************************************************************
''
PRI testRow | i, rs

    repeat i from 0 to 3

        riddle_c[i] := riddle[i]
        green[i] := 0
        white[i] := 0
        all[i] := 0

    repeat i from 0 to 3

        if player[i] == riddle_c[i]

            riddle_c[i] := 0
            green[i] := 1

    if green[0] == 0

        if player[0] == riddle_c[1] or player[0] == riddle_c[2] or player[0] == riddle_c[3] 

            white[0] := 2  

    if green[1] == 0
        
        if player[1] == riddle_c[0] or player[1] == riddle_c[2] or player[1] == riddle_c[3] 

            white[1] := 2  

    if green[2] == 0

        if player[2] == riddle_c[0] or player[2] == riddle_c[1] or player[2] == riddle_c[3] 

            white[2] := 2  

    if green[3] == 0

        if player[3] == riddle_c[0] or player[3] == riddle_c[1] or player[3] == riddle_c[2] 

            white[3] := 2  

    rs := 0
    repeat i from 0 to 3
        if green[i] <> 0
            all[rs] := green[i]
            rs++
    repeat i from 0 to 3
        if white[i] <> 0
            all[rs] := white[i]
            rs++

    res_x[0] := pin_x[row] + 9
    res_x[1] := pin_x[row] + 10
    res_x[2] := pin_x[row] + 8
    res_x[3] := pin_x[row] + 9

    res_y[0] := pin_y[row]
    res_y[1] := pin_y[row]
    res_y[2] := pin_y[row] + 1
    res_y[3] := pin_y[row] + 1

    rs := 0
    repeat i from 0 to 3
        if all[i] <> 0
            ptr := get_tilemap_address(res_x[rs], res_y[rs])
            if rs < 2
                word[ptr] := data#PIN_L_BLACK + all[i] 
            else
                word[ptr] := data#PIN_R_BLACK + all[i]
            rs++

    if green[0] == 1 and green[1] == 1 and green[2] == 1 and green[3] == 1
        return true

    return false             


PRI printRow

    dec(9, 29, $0, row+1)
    print($0, $20)
    

DAT
''********************************************************************
''* Functions for Intro                                              *
''********************************************************************
''
PRI intro | i, x, y, offs, da, bit, seed, addr, points, line, counter, toggle, key, col, rdy, wait

    printFrame(0, 0, 40, 29)

    ptr := get_tilemap_address(3, 2)
    i := data#HAT 
    repeat y from 0 to 4
        repeat x from 0 to 6
            word[ptr] := i++
            ptr += 2
        ptr += 66          '66


    repeat i from 0 to 4
        ptr := get_tilemap_address(14, 2+i)
        offs := @propmind + (i*4)
        da := long[offs]
        repeat 23
            da <-= 1
            bit := da & 1
            if bit <> 0
                seed := (prng.random & $7)         ' Random 0 - 7

                word[ptr] := data#RED + seed     
                ptr += 2
            else
                ptr += 2

    addr := $7F10
    {repeat i from 1 to 7
        printxy(9, 8+i, i-1, $30+i)
        print(i-1, $2E)                            ' .
        'i2c.read(12, addr, @tmp)
        tmp[12] := 0
        strxy(11, 8+i, i-1, @tmp)
        addr += 12
        'i2c.read(4, addr, @points)
        ticks := points & $FFF
        line := (points >> 12) 
        minutes := ticks / 60
        seconds := ticks // 60

        if ticks <> $FFF
            dec2(24, 8+i, i-1, minutes)
            print(i-1, $3A)                        ' :
            dec2(27, 8+i, i-1, seconds)
            dec(30, 8+i, i-1, line)

        addr += 4}

    line := 18
    repeat x from 15 to 13
        ptr := get_tilemap_address(x, line)
        repeat i from 0 to 12
            word[ptr] := line5[i]
            word[ptr+80] := line6[i]
            ptr += 2
        line += 2

    printPin(16 , 18, 1)
    printPin(18 , 18, 2)
    printPin(20 , 18, 3)
    printPin(22 , 18, 4)

    repeat x from 15 to 17 step 2
       printPin(x , 20, 2)
    printPin(19 , 20, 5)
    printPin(21 , 20, 6)

    repeat x from 14 to 18 step 2
       printPin(x , 22, 8)
    printPin(20 , 22, 7)

    printTilexy(7, 18, data#ARROW_1)
    printTilexy(7, 19, data#ARROW_2)
    printTilexy(7, 22, data#ARROW_3)
    printTilexy(7, 23, data#ARROW_4)

    strxy(30, 18, $0, @strSingle)
    strxy(30, 20, $0, @strDouble)
    strxy(30, 22, $0, @strTriple)

    updateSelect

    strxy(0, 29, $8, @strVer1)
    strxy(13, 29, $0, @strNoName)
    strxy(31, 29, $8, @strVer2)

    ifnot (kb.present)                                     ' Keyboard present???
        clrTmp($20)
        tmp[12] :=  0

        counter := toggle := 0                             ' No
        repeat
            counter++
            if counter == 80
                counter := 0
                toggle ^= 1
                if toggle == 0
                    strxy(14, 26, $7, @strNoKeyb)
                    play.error(@toneNoKb)
                else
                    strxy(14, 26, $9, @tmp)
            flip

    strxy(11, 26, $0, @strPress)
    flip
    
    kb.clearkeys

    col := 0
    rdy := 0
    wait := 0

    repeat

        repeat 100   

            if wait <> 0
                wait--

            key := kb.key
            NES_read_Gamepad
            if key == $53 or key == $73 or pad_state&NES0_START                      ' "S" or "s"

                play.tone(@toneRowFull, music)

                rdy := 1
                quit


            elseif (key == $C2 or pad_state&NES0_UP) and wait == 0                  ' Up

                if samecolors == 1
                    samecolors := 3
                else
                    samecolors--    
                updateSelect
                play.tone(@toneSet, music)
                wait := 25

            elseif (key == $C3 or pad_state&NES0_DOWN) and wait == 0                  ' Down
                if samecolors == 3
                    samecolors := 1
                else
                    samecolors++    
                updateSelect
                play.tone(@toneSet, music)
                wait := 25
            elseif key == $DC
              driver.toggleformat

            waitcnt((clkfreq / 1000) * 10 + cnt)  ' wait 10 ms

        col++
        if col == 9
            col := 0
        strxy(11, 26, col, @strPress)

'        flip

        if rdy == 1
            quit

    wordfill(@map1, $0000, render#MAP_SIZE_WORD)


PRI updateSelect

    printSelect(9, 18, 0)
    printSelect(9, 20, 0)
    printSelect(9, 22, 0)

    printSelect(9, (samecolors * 2) + 16, 1)


PRI printSelect(x, y, p) | pin

    pin := data#SELECT_F + (p * 4)
    ptr := get_tilemap_address(x, y)

    word[ptr] := pin++
    word[ptr+2] := pin++
    word[ptr+80] := pin++ 
    word[ptr+82] := pin 


DAT
''********************************************************************
''* Miscellaneous Functions                                          *
''********************************************************************
''
PRI flip

    repeat
    until link{0} == render#vres                        ' last line has been fetched


PRI endGame | i, freq

    play.melody_stop

    freq := $80

    repeat i from 0 to 115

        ay1.setVolume(1, $F)
        ay1.setFreq(1, freq)

        freq += $10

        waitcnt((clkfreq / 1000) * 10 + cnt)  ' wait 10 ms

    ay1.setVolume(1, $0)


PRI checkMusic

    if kb.keystate($D0) or ((pad_state&NES0_A) and (pad_state&NES0_SELECT))            ' F1
        if music == 1
            play.melody_stop
            music := 0
            repeat
                printTime
                NES_read_Gamepad
                ifnot kb.keystate($D0) or ((pad_state&NES0_A) and (pad_state&NES0_SELECT))
                    quit
        else
            play.melody_stop
            music := 1
            play.melody(@melody1, music)
            repeat
                printTime
                NES_read_Gamepad
                ifnot kb.keystate($D0) or ((pad_state&NES0_A) and (pad_state&NES0_SELECT))
                   quit


PRI initBoard | i, col, line

    ptr := get_tilemap_address(25, 1)
    repeat i from 0 to 12
        word[ptr] := line1[i] 
        ptr += 2

    ptr := get_tilemap_address(25, 2)
    repeat i from 0 to 12
        word[ptr] := line2[i]
        ptr += 2

    ptr := get_tilemap_address(24, 3)
    repeat i from 0 to 12
        word[ptr] := line3[i]
        ptr += 2

    ptr := get_tilemap_address(24, 4)
    repeat i from 0 to 12
        word[ptr] := line4[i]
        ptr += 2

    line := 5
    repeat col from 23 to 12
        ptr := get_tilemap_address(col, line)
        repeat i from 0 to 12
            word[ptr] := line5[i]
            word[ptr+80] := line6[i]
            ptr += 2
        line += 2

    repeat i from 1 to 8
        printPin(0, 8+(i * 2), i)

    strxy(0, 0, $4, @strVer1)
    strxy(14, 0, $4, @strVer2)

    strxy(0, 1, $0, @strTime)

    strxy(0, 2, $0, @strInfo1)
    strxy(0, 3, $0, @strInfo2)
    strxy(0, 4, $0, @strInfo3)
    strxy(0, 5, $0, @strInfo4)
    strxy(0, 6, $0, @strInfo5)
    strxy(0, 7, $0, @strInfo6)
    strxy(0, 8, $0, @strInfo7)

    printTilexy(30, 24, data#PIN_GREEN) 
    strxy(32, 24, $0, @strInfo8)
    printTilexy(39, 24, data#HOOK_OK) 
    strxy(32, 25, $0, @strInfo9)
    printTilexy(39, 25, data#HOOK_OK) 

    printTilexy(30, 27, data#PIN_WHITE) 
    strxy(32, 27, $0, @strInfo8)
    printTilexy(39, 27, data#HOOK_OK) 
    strxy(32, 28, $0, @strInfo9)
    printTilexy(39, 28, data#HOOK_WRONG) 

    strxy(0, 27, $0, @strColumn)
    dec(9, 27, $0, column+1)

    strxy(0, 28, $0, @strColor)
    dec(9, 28, $0, color)

    strxy(0, 29, $0, @strRow)
    printRow


PRI enterName | i, npos, key, addr, points, smaller, st

    '' Highscore disabled right now, thus
    return false

    addr := $7F10
    repeat i from 0 to 6
        'i2c.read(16, addr, @buffer + (i * 16))
        addr += 16

    smaller := 255
    addr := $7F1C
    repeat i from 0 to 6
        'i2c.read(4, addr, @points)
        addr += 16

        if score =< points
            smaller := i
            quit

    if smaller == 255

        play.melody_stop

        play.melody(@melodyWin, 1)

        repeat
            if play.stat == 0
                quit

        return false

    play.melody_stop

    play.melody(@melodyWin, 1)

    repeat
        if play.stat == 0
            quit

    printFrame(10, 11, 20, 8)
    strxy(12, 13, $0, @strEnter)

    npos := 14
    printxy(npos, 16, $8, $7F)
    flip

    kb.clearkeys

    clrTmp($20)
    tmp[12] := $00

    repeat

        key := kb.getkey

        if key <> 0

            case key

                $C8:                               ' Backsapce

                    play.tone(@toneSet, 0)

                    if npos > 14
                        printxy(npos, 16, $0, $20)
                        npos--
                        tmp[npos-14] := $20
                        printxy(npos, 16, $8, $7F)

                $0D:                               ' Enter

                    play.tone(@toneRowFull, music)
                    printxy(npos, 16, $0, $20)
                    quit

                $20, $30..$39, $41..$5A, $61..$7A:      ' 0-9, A-Z, ,a-z 

                    play.tone(@toneSet, 0)

                    if npos < 26
                        printxy(npos, 16, $0, key)
                        tmp[npos-14] := key
                        npos++
                        printxy(npos, 16, $8, $7F)

    st := 0
    addr := $7F10

    if smaller == 0

        'i2c.write(12, addr, @tmp)
        addr += 12
        'i2c.write(4, addr, @score)
        addr += 4

        repeat i from 0 to 5
            'i2c.write(16, addr, @buffer + (i * 16))
            addr += 16         

    else
        repeat smaller
            'i2c.write(16, addr, @buffer + (st * 16))
            addr += 16         
            st++

        'i2c.write(12, addr, @tmp)
        addr += 12
        'i2c.write(4, addr, @score)
        addr += 4

        repeat 6-st
            'i2c.write(16, addr, @buffer + (st * 16))
            addr += 16         
            st++

    sprite_hide(0)
    sprite_hide(1)

    return true


PRI clrTmp(char)

    bytefill(@tmp, char, 16)


PRI printPin(x, y, p) | pin

    pin := data#PIN_BLACK_1 + (p * 4)
    ptr := get_tilemap_address(x, y)

    word[ptr] := pin++
    word[ptr+2] := pin++
    word[ptr+80] := pin++ 
    word[ptr+82] := pin 


PRI printFrame(x, y, w, h)

    ptr := get_tilemap_address(x, y)
    repeat w
        printTile(data#FRAME_1)

    ptr += (40 - w) * 2
    repeat h-2
        printTile(data#FRAME_1)
        repeat w-2
            printTile(data#FONT_SPACE)
        printTile(data#FRAME_1)
        ptr += (40 - w) * 2
    repeat w
        printTile(data#FRAME_2)


PRI printTile(tile)

    word[ptr] := tile 
    ptr += 2


PRI printTilexy(x, y, tile)

    ptr := get_tilemap_address(x, y)
    word[ptr] := tile 


PRI print(colour, char)

    case char
        $30..$39: char := char - $30 + data#FONT_0
        $41..$5A: char := char - $41 + data#FONT_A
        $61..$7A: char := char - $61 + data#FONT_A
        $3A: char := data#FONT_COLON 
        $2F: char := data#FONT_SLASH 
        $3C: char := data#FONT_LEFT 
        $3E: char := data#FONT_RIGHT 
        $28: char := data#FONT_UP 
        $29: char := data#FONT_DN 
        $2E: char := data#FONT_DOT
        $2D: char := data#FONT_MINUS
        $21: char := data#FONT_EXC
        $22: char := data#FONT_QUOT
        $7F: char := data#FONT_CURSOR 
        other: char := data#FONT_SPACE

    word[ptr] := colour << 8 | char
    ptr += 2


PRI printxy(x, y, colour, char)

    ptr := get_tilemap_address(x, y)
    print(colour, char)
    

PRI str(colour, stringptr)

    repeat strsize(stringptr)
        print(colour, byte[stringptr++])


PRI strxy(x, y, colour, stringptr)

    ptr := get_tilemap_address(x, y)
    str(colour, stringptr)


PRI bin(x, y, val, digits) | v, c, optr
  
    optr := get_tilemap_address(x, y)

    val <<= 32 - digits
    repeat digits
        v := (val <-= 1) & 1 + "0"
        c := v - $30 + data#FONT_0
        word[optr] := c
        optr += 2


PRI hex(x, y, val, digits) | v, c, optr

    optr := get_tilemap_address(x, y)

    val <<= (8 - digits) << 2
    repeat digits

        v := lookupz((val <-= 4) & $f : "0".."9", "A".."F") 

        if v > $39
            c := v - $41 + data#FONT_A
            word[optr] := c
            optr += 2
        else
            c := v - $30 + data#FONT_0
            word[optr] := c
            optr += 2


PRI dec(x, y, colour, val) | col

    ptr := get_tilemap_address(x, y)
    prn(colour, val)


PRI dec2(x, y, colour, val) | col

    ptr := get_tilemap_address(x, y)
    if val < 10
        word[ptr] := colour << 8 | data#FONT_0
        ptr += 2
    prn(colour, val)


PRI prn(colour, val) | dig
    dig := 48 + (val // 10)
    val := val/10
    if val > 0
        prn(colour, val)
    word[ptr] := colour << 8 | dig - $30 + data#FONT_0
    ptr += 2


PRI get_tilemap_address(sx, sy)

    return @map1 + ((sy * 40 + sx) * 2)
    

PRI sprite_show(si, st, sp, sx, sy) | t

    t := (st << SPRITE_TILE_SHIFT) | (sp << SPRITE_PALETTE_SHIFT) | (sy << SPRITE_Y_SHIFT) | sx
    long[@sprite_state + (si * 4)] := t


PRI sprite_hide(si)

    long[@sprite_state + (si * 4)] := $FFFFFFFF


CON

    SPRITE_TILE_SHIFT    = 24
    SPRITE_PALETTE_SHIFT = 18
    SPRITE_Y_SHIFT       = 9


DAT

map1           word 0 [render#MAP_SIZE_WORD]   ' 40 x 30 Words               2400 Bytes

sprite_state   long $FFFFFFFF [2]              ' 2 Sprites                      8 Bytes


DAT


dummy          long 0

'                    M       I       N       D
propmind       long %10001_0_11111_0_10001_0_11110_000000000
               long %11011_0_00100_0_11001_0_10001_000000000
               long %10101_0_00100_0_10101_0_10001_000000000
               long %10001_0_00100_0_10011_0_10001_000000000
               long %10001_0_11111_0_10001_0_11110_000000000


strVer1        byte "PROPMIND V1.0", 0
strVer2        byte "WS 9/2019", 0

strNoKeyb      byte "NO KEYBOARD!", 0

strMe          byte "Werner      ", 0

strNoName      byte "                  ", 0

strSingle      byte "SINGLE", 0
strDouble      byte "DOUBLE", 0
strTriple      byte "TRIPLE", 0

strPress       byte "PRESS ", $22, "S", $22, " TO START", 0

strEnter       byte "ENTER YOUR NAME:", 0


strTime        byte "TIME  : 00:00", 0
strInfo1       byte "<>    : SELECT COLUMN", 0
strInfo2       byte "()    : SELECT COLOR", 0
strInfo3       byte "SPACE : SET COLOR", 0
strInfo4       byte "ENTER : SUBMIT ROW", 0
strInfo5       byte "F1    : MUSIC ON/OFF", 0
strInfo6       byte "R     : RESTART", 0
strInfo7       byte "ESC   : TITLE SCREEN", 0
strInfo8       byte "COLOR", 0
strInfo9       byte "COLUMN", 0


strColumn      byte "COLUMN :", 0
strColor       byte "COLOR  :", 0
strRow         byte "ROW    :", 0


line1          byte data#LEFTU, data#PIN_BLACK_1, data#PIN_BLACK_2, data#PIN_BLACK_1, data#PIN_BLACK_2, data#PIN_BLACK_1, data#PIN_BLACK_2, data#PIN_BLACK_1, data#PIN_BLACK_2, data#BIGU[3], data#RIGHTU
line2          byte data#LEFTL, data#PIN_BLACK_3, data#PIN_BLACK_4, data#PIN_BLACK_3, data#PIN_BLACK_4, data#PIN_BLACK_3, data#PIN_BLACK_4, data#PIN_BLACK_3, data#PIN_BLACK_4, data#BIGL[3], data#RIGHTL 
line3          byte data#LEFTU, data#BIGU[11], data#RIGHTU   
line4          byte data#LEFTL, data#BIGL[11], data#RIGHTL   
line5          byte data#LEFTU, data#PIN_BLACK_1, data#PIN_BLACK_2, data#PIN_BLACK_1, data#PIN_BLACK_2, data#PIN_BLACK_1, data#PIN_BLACK_2, data#PIN_BLACK_1, data#PIN_BLACK_2, data#BIGU, data#PIN_L_BLACK[2], data#RIGHTU
line6          byte data#LEFTL, data#PIN_BLACK_3, data#PIN_BLAcK_4, data#PIN_BLACK_3, data#PIN_BLAcK_4, data#PIN_BLACK_3, data#PIN_BLAcK_4, data#PIN_BLACK_3, data#PIN_BLAck_4, data#PIN_R_BLACK[2], data#BIGL, data#RIGHTL



pin_x          byte 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24
pin_y          byte 27, 25, 23, 21, 19, 17, 15, 13, 11, 9,  7,  5


DAT

toneNoKb

               word $0738
               word $080F
               word $090F
               word $0A0F
               word $0104
               word $00FF
               word $0304
               word $02F0
               word $0504
               word $04E8
               word $C000
               word $0800
               word $0900
               word $0A00
               word $FFFF   


DAT

toneError

               word $090F
               word $0304
               word $02F0
               word $A000
               word $0900
               word $FFFF   


DAT

toneRowFull

               word $0300
               word $02E0
               word $090F
               word $8300

               word $0300
               word $02D0
               word $090F
               word $8300

               word $0300
               word $02C0
               word $090F
               word $8300

               word $0300
               word $02A0
               word $090F
               word $8400

               word $0300
               word $0280
               word $090F
               word $8400

               word $0900
               word $FFFF   


DAT

toneSet

               word $0300
               word $02C0
               word $090F
               word $8500
               word $0900
               word $FFFF   


DAT

noise1

               word $061E
               word $0A0F
               word $8200
               word $0A00
               word $8100
               word $0A00
               word $FFFF


DAT

melody1
               
               word $0800
               word $8F00
               word $0C0A

               word $0103
               word $0057
               word $0810
               word $0D09
               word $8E00

               word $0102
               word $00A6
               word $0810
               word $0D09
               word $8E00

               word $0102
               word $003A
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0103
               word $0057
               word $0810
               word $0D09
               word $8E00

               word $0102
               word $00A6
               word $0810
               word $0D09
               word $8E00

               word $0102
               word $003A
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0102
               word $00F9
               word $0810
               word $0D09
               word $8E00

               word $0102
               word $003A
               word $0810
               word $0D09
               word $8E00

               word $0102
               word $003A
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0102
               word $00F9
               word $0810
               word $0D09
               word $8E00

               word $0102
               word $003A
               word $0810
               word $0D09
               word $8E00

               word $0102
               word $003A
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0103
               word $0057
               word $0810
               word $0D09
               word $8E00

               word $0102
               word $00A6
               word $0810
               word $0D09
               word $8E00

               word $0102
               word $0080
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0103
               word $0057
               word $0810
               word $0D09
               word $8E00

               word $0102
               word $00A6
               word $0810
               word $0D09
               word $8E00

               word $0102
               word $0080
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0102
               word $00F9
               word $0810
               word $0D09
               word $8E00

               word $0102
               word $00F9
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0102
               word $00F9
               word $0810
               word $0D09
               word $8E00

               word $0102
               word $00F9
               word $0810
               word $0D09
               word $8E00

               word $0102
               word $00A6
               word $0810
               word $0D09
               word $8E00

               word $0102
               word $00CF
               word $0810
               word $0D09
               word $8E00

               word $0102
               word $00F9
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $00AB
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $0053
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $001D
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0101
               word $00AB
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $0053
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $001D
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0101
               word $007C
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $001D
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $001D
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0101
               word $007C
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $001D
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $001D
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0101
               word $00AB
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $0053
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $0040
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0101
               word $00AB
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $0053
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $0040
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0101
               word $007C
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $007C
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0101
               word $007C
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $007C
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $0053
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $0067
               word $0810
               word $0D09
               word $8E00

               word $0101
               word $007C
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $00D5
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $00A9
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $008E
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0100
               word $00D5
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $00A9
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $008E
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0100
               word $00BE
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $008E
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $008E
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0100
               word $00BE
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $008E
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $008E
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0100
               word $00D5
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $00A9
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $00A0
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0100
               word $00D5
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $00A9
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $00A0
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0100
               word $00BE
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $00BE
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0100
               word $00BE
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $00BE
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $00A9
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $00B3
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $00BE
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $006A
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $0054
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $0047
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0100
               word $006A
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $0054
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $0047
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0100
               word $005F
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $0047
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $0047
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0100
               word $005F
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $0047
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $0047
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0100
               word $006A
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $0054
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $0050
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0100
               word $006A
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $0054
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $0050
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0100
               word $005F
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $005F
               word $0810
               word $0D09
               word $8E00
               word $8F00

               word $0100
               word $005F
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $005F
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $0054
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $0059
               word $0810
               word $0D09
               word $8E00

               word $0100
               word $005F
               word $0810
               word $0D09
               word $8E00
               word $0800
               word $FFFF

DAT

melodyWin

               word $0800
               word $0C12               
               word $0C1E
               word $0D00
               word $0102
               word $00F9
               word $0810
               word $0D00
               word $99F3
               word $0102
               word $003A
               word $0810
               word $0D00
               word $99F3
               word $0102
               word $003A
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00FC
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $99F3
               word $0102
               word $003A
               word $0810
               word $0D00
               word $99F2
               word $0101
               word $007C
               word $0810
               word $0D00
               word $B37D
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $A6B8
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00AB
               word $0810
               word $0D00
               word $99F3
               word $0101
               word $007C
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00AB
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00AB
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $007C
               word $0810
               word $0D00
               word $99F3
               word $0101
               word $00FC
               word $0810
               word $0D00
               word $8D2E
               word $0102
               word $003A
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00FC
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00FC
               word $0810
               word $0D00
               word $99F3
               word $0102
               word $00F9
               word $0810
               word $0D00
               word $99F3
               word $0102
               word $003A
               word $0810
               word $0D00
               word $99F3
               word $0102
               word $003A
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00FC
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $99F3
               word $0102
               word $003A
               word $0810
               word $0D00
               word $99F2
               word $0101
               word $007C
               word $0810
               word $0D00
               word $B37D
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $A6B8
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00AB
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $007C
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00C5
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00AB
               word $0810
               word $0D00
               word $8D2E
               word $0101
               word $00FC
               word $0810
               word $0D00
               word $A6B8
               word $0102
               word $003A
               word $0810
               word $0D00
               word $8D2E
               word $0102
               word $003A
               word $0810
               word $0D00
               word $B000
               word $FFFF

CON
  NES0_RIGHT    = %00000000_00000001
  NES0_LEFT     = %00000000_00000010
  NES0_DOWN     = %00000000_00000100
  NES0_UP       = %00000000_00001000
  NES0_START    = %00000000_00010000
  NES0_SELECT   = %00000000_00100000
  NES0_B        = %00000000_01000000
  NES0_A        = %00000000_10000000


PUB NES_Read_Gamepad                   |  i,nes_bits
' //////////////////////////////////////////////////////////////////
' NES Game Pad Read
' //////////////////////////////////////////////////////////////////       
' reads both gamepads in parallel encodes 8-bits for each in format
' right game pad #1 [15..8] : left game pad #0 [7..0]
'
' set I/O ports to proper direction
' P3 = JOY_CLK      (4)
' P4 = JOY_SH/LDn   (5) 
' P5 = JOY_DATAOUT0 (6)
' P6 = JOY_DATAOUT1 (7)
' NES Bit Encoding
'
' RIGHT  = %00000001
' LEFT   = %00000010
' DOWN   = %00000100
' UP     = %00001000
' START  = %00010000
' SELECT = %00100000
' B      = %01000000
' A      = %10000000

' step 1: set I/Os
DIRA [JOY_CLK] := 1 ' output
DIRA [JOY_LCH] := 1 ' output
DIRA [JOY_DATAOUT0] := 0 ' input
DIRA [JOY_DATAOUT1] := 0 ' input

' step 2: set clock and latch to 0
OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0
'Delay(1)

' step 3: set latch to 1
OUTA [JOY_LCH] := 1 ' JOY_SH/LDn = 1
'Delay(1)                            

' step 4: set latch to 0
OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0

' data is now ready to shift out, clear storage
nes_bits := 0

' step 5: read 8 bits, 1st bits are already latched and ready, simply save and clock remaining bits
repeat i from 0 to 7

 nes_bits := (nes_bits << 1)
 nes_bits := nes_bits | INA[JOY_DATAOUT0] | (INA[JOY_DATAOUT1] << 8)

 OUTA [JOY_CLK] := 1 ' JOY_CLK = 1
 'Delay(1)             
 OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
 
 'Delay(1)             

' invert bits to make positive logic
nes_bits := (!nes_bits & $FFFF)

pad_prev := pad_state
pad_state := nes_bits
pad_press := (pad_state^pad_prev)&pad_state
          
' //////////////////////////////////////////////////////////////////
' End NES Game Pad Read
' //////////////////////////////////////////////////////////////////
   
      