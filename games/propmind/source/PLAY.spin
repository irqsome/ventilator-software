''*******************************
''* PLAY.spin v1.0       3/2019 *
''* Author: Werner L. Schneider *
''*******************************
''
''
VAR

    long cog                     ' cog flag/id

    'DO NOT REARRANGE LONGS
    long ay1_regs                ' 8 contiguous longs
    long buffA_ptr
    long buffB_ptr
    long buffC_ptr
    long flagA
    long flagB
    long flagC
    long wait_val
    

PUB start(ayptr)
    stop

    ay1_Regs := ayptr

    wait_val := 80_000 / 23          ' 3.478

    cog := cognew(@entry, @ay1_regs) + 1


PUB stop
    if cog
        cogstop(cog~ - 1)
    longfill(@ay1_regs, 0, 8)


PUB melody(offs, m)

    if m == 0
        return

    if m == 1                       
        byte[ay1_Regs+7] := $1C
                               
    if flagA == 0
        buffA_ptr := offs
        flagA := 1


PUB melody_stop

    flagA := 0


PUB tone(offs, m)

    if m == 0
        byte[ay1_Regs+7] := $1D
    else
        byte[ay1_Regs+7] := $1C

    if flagB == 0
        buffB_ptr := offs
        flagB := 1


PUB noise(offs)

    if flagC == 0
        buffC_ptr := offs
        flagC := 1


PUB stat

    return flagA


PUB wait(w)

    wait_val := w

    
PUB error(offs)

    if flagB == 0
        buffB_ptr := offs
        flagB := 1



    
DAT

'***********************************
'* Assembly language "play" driver *
'***********************************

                        org     
'
' Entry
'

entry                   movd      :par, #_ay1ptr
                        mov       t1, par
                        mov       t2, #8
:par                    mov       0, t1
                        add       :par, dlsb
                        add       t1, #4
                        djnz      t2, #:par

                        mov       _waitA, #0
                        mov       _waitB, #0
                        mov       _waitC, #0
 
                        rdlong    _ay, _ay1ptr

'*********************************************************************************************************

bigloop                rdlong     _wdelay, _waitptr

'*********************************************************************************************************

entry1                  rdlong    t1, _flagptrA          ' Read Ch A Flag
                        cmp       t1, #0            wz
             if_z       jmp       #entry2     
                        cmp       t1, #1            wz 
             if_z       add       t1, #1
             if_z       wrlong    t1, _flagptrA
             if_z       rdlong    _buffA, _buffptrA

                        cmp       _waitA, #0        wz
             if_nz      jmp       #entry2

                        rdword    _wrd, _buffA
                        add       _buffA, #2
                        cmp       _wrd, endmask     wz
             if_z       jmp       #readyA

                        cmp       _wrd, n7fff       wc
             if_nc      and       _wrd, n7fff
             if_nc      mov       _waitA, _wrd
             if_nc      jmp       #entry2

                        mov       _reg, _wrd
                        shr       _reg, #8
                        and       _reg, #$1F

                        mov       _data, _wrd
                        and       _data, #$FF              

                        mov       _ayA, _ay
                        add       _ayA, _reg
                        wrbyte    _data, _ayA            ' Write Byte to AyBuffer

'*********************************************************************************************************

entry2                  rdlong    t1, _flagptrB          ' Read Ch B Flag
                        cmp       t1, #0            wz
             if_z       jmp       #entry3     
                        cmp       t1, #1            wz 
             if_z       add       t1, #1
             if_z       wrlong    t1, _flagptrB
             if_z       rdlong    _buffB, _buffptrB

                        cmp       _waitB, #0        wz
             if_nz      jmp       #entry3

                        rdword    _wrd, _buffB
                        add       _buffB, #2
                        cmp       _wrd, endmask     wz
             if_z       jmp       #readyB

                        cmp       _wrd, n7fff       wc
             if_nc      and       _wrd, n7fff
             if_nc      mov       _waitB, _wrd
             if_nc      jmp       #entry3

                        mov       _reg, _wrd
                        shr       _reg, #8
                        and       _reg, #$1F

                        mov       _data, _wrd
                        and       _data, #$FF              

                        mov       _ayB, _ay
                        add       _ayB, _reg
                        wrbyte    _data, _ayB            ' Write Byte to AyBuffer

'*********************************************************************************************************

entry3                  rdlong    t1, _flagptrC          ' Read Ch C Flag
                        cmp       t1, #0            wz
             if_z       jmp       #entry4     
                        cmp       t1, #1            wz 
             if_z       add       t1, #1
             if_z       wrlong    t1, _flagptrC
             if_z       rdlong    _buffC, _buffptrC

                        cmp       _waitC, #0        wz
             if_nz      jmp       #entry4

                        rdword    _wrd, _buffC
                        add       _buffC, #2
                        cmp       _wrd, endmask     wz
             if_z       jmp       #readyC

                        cmp       _wrd, n7fff       wc
             if_nc      and       _wrd, n7fff
             if_nc      mov       _waitC, _wrd
             if_nc      jmp       #entry3

                        mov       _reg, _wrd
                        shr       _reg, #8
                        and       _reg, #$1F

                        mov       _data, _wrd
                        and       _data, #$FF              

                        mov       _ayC, _ay
                        add       _ayC, _reg
                        wrbyte    _data, _ayC            ' Write Byte to AyBuffer

 '*********************************************************************************************************

entry4                  mov       _delay, cnt             ' wait 1 second
                        add       _delay, _wdelay
                        waitcnt   _delay, _wdelay        

                        cmp       _waitA, #0        wz
             if_nz      sub       _waitA, #1           
                        cmp       _waitB, #0        wz
             if_nz      sub       _waitB, #1           
                        cmp       _waitC, #0        wz
             if_nz      sub       _waitC, #1           
                        jmp       #bigloop

'*********************************************************************************************************

readyA                  mov       t1, #0
                        wrlong    t1, _flagptrA
                        jmp       #entry2

'*********************************************************************************************************

readyB                  mov       t1, #0
                        wrlong    t1, _flagptrB
                        jmp       #entry3

'*********************************************************************************************************

readyC                  mov       t1, #0
                        wrlong    t1, _flagptrC
                        jmp       #entry4

'*********************************************************************************************************

                        ' Data

dlsb                    long      1 << 9

endmask                 long      $FFFF

n7fff                   long      $7FFF


'
' Uninitialized data
'
t1                      res       1
t2                      res       1

_delay                  res       1
_wdelay                 res       1

_ay1ptr                 res       1               ' 8 params
_buffptrA               res       1
_buffptrB               res       1
_buffptrC               res       1
_flagptrA               res       1
_flagptrB               res       1
_flagptrC               res       1
_waitptr                res       1

_wrd                    res       1
_reg                    res       1
_data                   res       1

_ay                     res       1
_ayA                    res       1
_ayB                    res       1
_ayC                    res       1
_waitA                  res       1
_waitB                  res       1
_waitC                  res       1
_buffA                  res       1
_buffB                  res       1
_buffC                  res       1