''*******************************
''* GRAPHICS.spin               *               
''* Author: Werner L. Schneider *
''*******************************
''
'' Based on Code from Marco Maccaferri 
''
''+------------------------------------------------------------------------------------------------------------------------------+
''|                                 Propeller Game Engine Demo (C) 2013 Marco Maccaferri                                         |
''+------------------------------------------------------------------------------------------------------------------------------+
''|                                   TERMS OF USE: Parallax Object Exchange License                                             |
''+------------------------------------------------------------------------------------------------------------------------------+
''|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    |
''|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
''|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
''|is furnished to do so, subject to the following conditions:                                                                   |
''|                                                                                                                              |
''|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
''|                                                                                                                              |
''|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
''|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
''|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
''|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
''+------------------------------------------------------------------------------------------------------------------------------+
''
''
PUB get_palette_def

    return @palette_def


PUB get_tile_def

    return @tiles_def


PUB get_sprite_def

    return @sprites_def


DAT

'' Palette definitions
palette_def

        long    $5D_BD_04_02
        long    $DD_AD_8E_0C
        long    $7B_0B_07_3E      ' FC = White
        long    $02_02_02_02

        long    $5D_BD_04_02
        long    $DD_AD_8E_0C
        long    $7B_0B_5D_3E      ' 30 = Green
        long    $02_02_02_07

        long    $5D_BD_04_02
        long    $DD_AD_8E_0C
        long    $7B_0B_0C_3E      ' 0C = Blue
        long    $02_02_02_02

        long    $5D_BD_04_02
        long    $DD_AD_8E_0C
        long    $7B_0B_8E_3E      ' F0 = Yellow
        long    $02_02_02_02

        long    $5D_BD_04_02
        long    $DD_AD_8E_0C
        long    $7B_0B_AD_3E      ' D0 = Orange
        long    $02_02_02_02

        long    $5D_BD_04_02
        long    $DD_AD_8E_0C
        long    $7B_0B_DD_3E      ' CC = Magenta
        long    $02_02_02_02

        long    $5D_BD_04_02
        long    $DD_AD_8E_0C
        long    $7B_0B_3E_3E      ' 3C = Cyan
        long    $02_02_02_02

        long    $5D_BD_04_02
        long    $DD_AD_8E_0C
        long    $7B_0B_BD_3E      ' C0 = Red
        long    $02_02_02_02

        long    $5D_BD_04_02
        long    $DD_AD_8E_0C
        long    $7B_0B_04_3E      ' 54 = Grey
        long    $02_02_02_02


DAT


'' Tiles definitions
tiles_def

        '' name='Font Space'
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=LEFTU
        long    $0000000A
        long    $00000001
        long    $0000001A
        long    $000000AA
        long    $00000AA1
        long    $00000A11
        long    $0000A11A
        long    $000011AA

        '' name=LEFTL
        long    $00011AA1
        long    $0001AA11
        long    $001AA11A
        long    $00AA11AA
        long    $0AA11AA1
        long    $0A11AA11
        long    $A11AA11A
        long    $11111111

        '' name=RIGHTU
        long    $AAAAAAAA
        long    $AA11AA11
        long    $A11AA110
        long    $11AA11A0
        long    $1AA11A00
        long    $AA11AA00
        long    $A11AA000
        long    $11AA1000

        '' name=RIGHTL
        long    $1AA10000
        long    $AA110000
        long    $A1100000
        long    $11A00000
        long    $1A000000
        long    $AA000000
        long    $A0000000
        long    $10000000

        '' name=BIGU
        long    $AAAAAAAA
        long    $AA11AA11
        long    $A11AA11A
        long    $11AA11AA
        long    $1AA11AA1
        long    $AA11AA11
        long    $A11AA11A
        long    $11AA11AA

        '' name=BIGL
        long    $1AA11AA1
        long    $AA11AA11
        long    $A11AA11A
        long    $11AA11AA
        long    $1AA11AA1
        long    $AA11AA11
        long    $A11AA11A
        long    $11111111





        '' name=PIN_L_BLACK
        long    $AAAAAAAA
        long    $AA11AA11
        long    $A00AA11A
        long    $000011AA
        long    $00001AA1
        long    $A001AA11
        long    $A11AA11A
        long    $11AA11AA

        '' name=PIN_L_GREEN
        long    $AAAAAAAA
        long    $AA11AA11
        long    $A33AA11A
        long    $333311AA
        long    $33331AA1
        long    $A331AA11
        long    $A11AA11A
        long    $11AA11AA

        '' name=PIN_L_WHITE
        long    $AAAAAAAA
        long    $AA11AA11
        long    $A99AA11A
        long    $999911AA
        long    $99991AA1
        long    $A991AA11
        long    $A11AA11A
        long    $11AA11AA

        '' name=PIN_R_BLACK
        long    $1AA11AA1
        long    $AA11AA11
        long    $A11AA00A
        long    $11AA0000
        long    $1AA10000
        long    $AA11A001
        long    $A11AA11A
        long    $11111111

        '' name=PIN_R_GREEN
        long    $1AA11AA1
        long    $AA11AA11
        long    $A11AA33A
        long    $11AA3333
        long    $1AA13333
        long    $AA11A331
        long    $A11AA11A
        long    $11111111

        '' name=PIN_R_WHITE
        long    $1AA11AA1
        long    $AA11AA11
        long    $A11AA99A
        long    $11AA9999
        long    $1AA19999
        long    $AA11A991
        long    $A11AA11A
        long    $11111111

        '' name=PIN_GREEN
        long    $1AA11AA1
        long    $AA11AA11
        long    $A113311A
        long    $113333AA
        long    $1A3333A1
        long    $AA133A11
        long    $A11AA11A
        long    $11AA11AA

        '' name=PIN_WHITE
        long    $1AA11AA1
        long    $AA11AA11
        long    $A119911A
        long    $119999AA
        long    $1A9999A1
        long    $AA199A11
        long    $A11AA11A
        long    $11AA11AA





        '' name=PIN_BLACK_1
        long    $AAAAAAAA
        long    $AA11AA11
        long    $A11AA100
        long    $11AA0000
        long    $1AA00000
        long    $AA100000
        long    $A1000000
        long    $11000000

        '' name=PIN_BLACK_2
        long    $AAAAAAAA
        long    $AA11AA11
        long    $001AA11A
        long    $000011AA
        long    $00000AA1
        long    $00000A11
        long    $0000001A
        long    $000000AA

        '' name=PIN_BLACK_3
        long    $1A000000
        long    $AA000000
        long    $A1100000
        long    $11A00000
        long    $1AA10000
        long    $AA11AA00
        long    $A11AA11A
        long    $11111111

        '' name=PIN_BLACK_4
        long    $000000A1
        long    $00000011
        long    $0000011A
        long    $000001AA
        long    $00001AA1
        long    $0011AA11
        long    $A11AA11A
        long    $11111111

        '' name=PIN_RED_1
        long    $AAAAAAAA
        long    $AA11AA11
        long    $A11AA122
        long    $11AA2222
        long    $1AA22222
        long    $AA122222
        long    $A1222222
        long    $11222222

        '' name=PIN_RED_2
        long    $AAAAAAAA
        long    $AA11AA11
        long    $221AA11A
        long    $222211AA
        long    $22222AA1
        long    $22222A11
        long    $2222221A
        long    $222222AA

        '' name=PIN_RED_3
        long    $1A222222
        long    $AA222222
        long    $A1122222
        long    $11A22222
        long    $1AA12222
        long    $AA11AA22
        long    $A11AA11A
        long    $11111111

        '' name=PIN_RED_4
        long    $222222A1
        long    $22222211
        long    $2222211A
        long    $222221AA
        long    $22221AA1
        long    $2211AA11
        long    $A11AA11A
        long    $11111111

        '' name=PIN_GREEN_1
        long    $AAAAAAAA
        long    $AA11AA11
        long    $A11AA133
        long    $11AA3333
        long    $1AA33333
        long    $AA133333
        long    $A1333333
        long    $11333333

        '' name=PIN_GREEN_2
        long    $AAAAAAAA
        long    $AA11AA11
        long    $331AA11A
        long    $333311AA
        long    $33333AA1
        long    $33333A11
        long    $3333331A
        long    $333333AA

        '' name=PIN_GREEN_3
        long    $1A333333
        long    $AA333333
        long    $A1133333
        long    $11A33333
        long    $1AA13333
        long    $AA11AA33
        long    $A11AA11A
        long    $11111111

        '' name=PIN_GREEN_4
        long    $333333A1
        long    $33333311
        long    $3333311A
        long    $333331AA
        long    $33331AA1
        long    $3311AA11
        long    $A11AA11A
        long    $11111111

        '' name=PIN_BLUE_1
        long    $AAAAAAAA
        long    $AA11AA11
        long    $A11AA144
        long    $11AA4444
        long    $1AA44444
        long    $AA144444
        long    $A1444444
        long    $11444444

        '' name=PIN_BLUE_2
        long    $AAAAAAAA
        long    $AA11AA11
        long    $441AA11A
        long    $444411AA
        long    $44444AA1
        long    $44444A11
        long    $4444441A
        long    $444444AA

        '' name=PIN_BLUE_3
        long    $1A444444
        long    $AA444444
        long    $A1144444
        long    $11A44444
        long    $1AA14444
        long    $AA11AA44
        long    $A11AA11A
        long    $11111111

        '' name=PIN_BLUE_4
        long    $444444A1
        long    $44444411
        long    $4444411A
        long    $444441AA
        long    $44441AA1
        long    $4411AA11
        long    $A11AA11A
        long    $11111111

        '' name=PIN_YELLOW_1
        long    $AAAAAAAA
        long    $AA11AA11
        long    $A11AA155
        long    $11AA5555
        long    $1AA55555
        long    $AA155555
        long    $A1555555
        long    $11555555

        '' name=PIN_YELLOW_2
        long    $AAAAAAAA
        long    $AA11AA11
        long    $551AA11A
        long    $555511AA
        long    $55555AA1
        long    $55555A11
        long    $5555551A
        long    $555555AA

        '' name=PIN_YELLOW_3
        long    $1A555555
        long    $AA555555
        long    $A1155555
        long    $11A55555
        long    $1AA15555
        long    $AA11AA55
        long    $A11AA11A
        long    $11111111

        '' name=PIN_YELLOW_4
        long    $555555A1
        long    $55555511
        long    $5555511A
        long    $555551AA
        long    $55551AA1
        long    $5511AA11
        long    $A11AA11A
        long    $11111111

        '' name=PIN_ORANGE_1
        long    $AAAAAAAA
        long    $AA11AA11
        long    $A11AA166
        long    $11AA6666
        long    $1AA66666
        long    $AA166666
        long    $A1666666
        long    $11666666

        '' name=PIN_ORANGE_2
        long    $AAAAAAAA
        long    $AA11AA11
        long    $661AA11A
        long    $666611AA
        long    $66666AA1
        long    $66666A11
        long    $6666661A
        long    $666666AA

        '' name=PIN_ORANGE_3
        long    $1A666666
        long    $AA666666
        long    $A1166666
        long    $11A66666
        long    $1AA16666
        long    $AA11AA66
        long    $A11AA11A
        long    $11111111

        '' name=PIN_ORANGE_4
        long    $666666A1
        long    $66666611
        long    $6666611A
        long    $666661AA
        long    $66661AA1
        long    $6611AA11
        long    $A11AA11A
        long    $11111111

        '' name=PIN_MAGENTA_1
        long    $AAAAAAAA
        long    $AA11AA11
        long    $A11AA177
        long    $11AA7777
        long    $1AA77777
        long    $AA177777
        long    $A1777777
        long    $11777777

        '' name=PIN_MAGENTA_2
        long    $AAAAAAAA
        long    $AA11AA11
        long    $771AA11A
        long    $777711AA
        long    $77777AA1
        long    $77777A11
        long    $7777771A
        long    $777777AA

        '' name=PIN_MAGENTA_3
        long    $1A777777
        long    $AA777777
        long    $A1177777
        long    $11A77777
        long    $1AA17777
        long    $AA11AA77
        long    $A11AA11A
        long    $11111111

        '' name=PIN_MAGENTA_4
        long    $777777A1
        long    $77777711
        long    $7777711A
        long    $777771AA
        long    $77771AA1
        long    $7711AA11
        long    $A11AA11A
        long    $11111111

        '' name=PIN_CYAN_1
        long    $AAAAAAAA
        long    $AA11AA11
        long    $A11AA188
        long    $11AA8888
        long    $1AA88888
        long    $AA188888
        long    $A1888888
        long    $11888888

        '' name=PIN_CYAN_2
        long    $AAAAAAAA
        long    $AA11AA11
        long    $881AA11A
        long    $888811AA
        long    $88888AA1
        long    $88888A11
        long    $8888881A
        long    $888888AA

        '' name=PIN_CYAN_3
        long    $1A888888
        long    $AA888888
        long    $A1188888
        long    $11A88888
        long    $1AA18888
        long    $AA11AA88
        long    $A11AA11A
        long    $11111111

        '' name=PIN_CYAN_4
        long    $888888A1
        long    $88888811
        long    $8888811A
        long    $888881AA
        long    $88881AA1
        long    $8811AA11
        long    $A11AA11A
        long    $11111111

        '' name=PIN_WHITE_1
        long    $AAAAAAAA
        long    $AA11AA11
        long    $A11AA199
        long    $11AA9999
        long    $1AA99999
        long    $AA199999
        long    $A1999999
        long    $11999999

        '' name=PIN_WHITE_2
        long    $AAAAAAAA
        long    $AA11AA11
        long    $991AA11A
        long    $999911AA
        long    $99999AA1
        long    $99999A11
        long    $9999991A
        long    $999999AA

        '' name=PIN_WHITE_3
        long    $1A999999
        long    $AA999999
        long    $A1199999
        long    $11A99999
        long    $1AA19999
        long    $AA11AA99
        long    $A11AA11A
        long    $11111111

        '' name=PIN_WHITE_4
        long    $999999A1
        long    $99999911
        long    $9999911A
        long    $999991AA
        long    $99991AA1
        long    $9911AA11
        long    $A11AA11A
        long    $11111111

        '' name=PIN_X_1
        long    $AAAAAAAA
        long    $AA11AA11
        long    $A11AA100
        long    $11AA0000
        long    $1AA00099
        long    $AA100990
        long    $A1000000
        long    $11000000

        '' name=PIN_X_2
        long    $AAAAAAAA
        long    $AA11AA11
        long    $001AA11A
        long    $000011AA
        long    $99000AA1
        long    $09900A11
        long    $0990001A
        long    $099000AA

        '' name=PIN_X_3
        long    $1A000000
        long    $AA000009
        long    $A1100009
        long    $11A00000
        long    $1AA10009
        long    $AA11AA00
        long    $A11AA11A
        long    $11111111

        '' name=PIN_X_4
        long    $990000A1
        long    $90000011
        long    $9000011A
        long    $000001AA
        long    $90001AA1
        long    $0011AA11
        long    $A11AA11A
        long    $11111111



        '' name=0
        long    $00999000
        long    $09909900
        long    $99000990
        long    $99090990
        long    $99000990
        long    $09909900
        long    $00999000
        long    $00000000

        '' name=1
        long    $00099000
        long    $00999000
        long    $00099000
        long    $00099000
        long    $00099000
        long    $00099000
        long    $09999990
        long    $00000000

        '' name=2
        long    $09999900
        long    $99000990
        long    $00000990
        long    $00099900
        long    $00990000
        long    $09900990
        long    $99999990
        long    $00000000

        '' name=3
        long    $09999900
        long    $99000990
        long    $00000990
        long    $00999900
        long    $00000990
        long    $99000990
        long    $09999900
        long    $00000000

        '' name=4
        long    $00099900
        long    $00999900
        long    $09909900
        long    $99009900
        long    $99999990
        long    $00009900
        long    $00099990
        long    $00000000

        '' name=5
        long    $99999990
        long    $99000000
        long    $99000000
        long    $99999900
        long    $00000990
        long    $99000990
        long    $09999900
        long    $00000000

        '' name=6
        long    $00999000
        long    $09900000
        long    $99000000
        long    $99999900
        long    $99000990
        long    $99000990
        long    $09999900
        long    $00000000

        '' name=7
        long    $99999990
        long    $99000990
        long    $00009900
        long    $00099000
        long    $00990000
        long    $00990000
        long    $00990000
        long    $00000000

        '' name=8
        long    $09999900
        long    $99000990
        long    $99000990
        long    $09999900
        long    $99000990
        long    $99000990
        long    $09999900
        long    $00000000

        '' name=9
        long    $09999900
        long    $99000990
        long    $99000990
        long    $09999990
        long    $00000990
        long    $00009900
        long    $09999000
        long    $00000000













        '' name=A-Z2_1
        long    $00999000
        long    $09909900
        long    $99000990
        long    $99999990
        long    $99000990
        long    $99000990
        long    $99000990
        long    $00000000

        '' name=A-Z2_2
        long    $99999900
        long    $09900990
        long    $09900990
        long    $09999900
        long    $09900990
        long    $09900990
        long    $99999900
        long    $00000000

        '' name=A-Z2_3
        long    $00999900
        long    $09900990
        long    $99000000
        long    $99000000
        long    $99000000
        long    $09900990
        long    $00999900
        long    $00000000

        '' name=A-Z2_4
        long    $99999000
        long    $09909900
        long    $09900990
        long    $09900990
        long    $09900990
        long    $09909900
        long    $99999000
        long    $00000000

        '' name=A-Z2_5
        long    $99999990
        long    $09900090
        long    $09909000
        long    $09999000
        long    $09909000
        long    $09900090
        long    $99999990
        long    $00000000

        '' name=A-Z2_6
        long    $99999990
        long    $09900090
        long    $09909000
        long    $09999000
        long    $09909000
        long    $09900000
        long    $99990000
        long    $00000000

        '' name=A-Z2_7
        long    $00999900
        long    $09900990
        long    $99000000
        long    $99000000
        long    $99009990
        long    $09900990
        long    $00999090
        long    $00000000

        '' name=A-Z2_8
        long    $99000990
        long    $99000990
        long    $99000990
        long    $99999990
        long    $99000990
        long    $99000990
        long    $99000990
        long    $00000000

        '' name=A-Z2_9
        long    $00999900
        long    $00099000
        long    $00099000
        long    $00099000
        long    $00099000
        long    $00099000
        long    $00999900
        long    $00000000

        '' name=A-Z2_10
        long    $00099990
        long    $00009900
        long    $00009900
        long    $00009900
        long    $99009900
        long    $99009900
        long    $09999000
        long    $00000000

        '' name=A-Z2_11
        long    $99900990
        long    $09900990
        long    $09909900
        long    $09999000
        long    $09909900
        long    $09900990
        long    $99900990
        long    $00000000

        '' name=A-Z2_12
        long    $99990000
        long    $09900000
        long    $09900000
        long    $09900000
        long    $09900090
        long    $09900990
        long    $99999990
        long    $00000000

        '' name=A-Z2_13
        long    $99000990
        long    $99909990
        long    $99999990
        long    $99999990
        long    $99090990
        long    $99000990
        long    $99000990
        long    $00000000

        '' name=A-Z2_14
        long    $99000990
        long    $99900990
        long    $99990990
        long    $99099990
        long    $99009990
        long    $99000990
        long    $99000990
        long    $00000000

        '' name=A-Z2_15
        long    $09999900
        long    $99000990
        long    $99000990
        long    $99000990
        long    $99000990
        long    $99000990
        long    $09999900
        long    $00000000

        '' name=A-Z2_16
        long    $99999900
        long    $09900990
        long    $09900990
        long    $09999900
        long    $09900000
        long    $09900000
        long    $99990000
        long    $00000000

        '' name=A-Z2_17
        long    $09999900
        long    $99000990
        long    $99000990
        long    $99000990
        long    $99000990
        long    $99009990
        long    $09999900
        long    $00009990

        '' name=A-Z2_18
        long    $99999900
        long    $09900990
        long    $09900990
        long    $09999900
        long    $09909900
        long    $09900990
        long    $99900990
        long    $00000000

        '' name=A-Z2_19
        long    $00999900
        long    $09900990
        long    $00990000
        long    $00099000
        long    $00009900
        long    $09900990
        long    $00999900
        long    $00000000

        '' name=A-Z2_20
        long    $09999990
        long    $09999990
        long    $09099090
        long    $00099000
        long    $00099000
        long    $00099000
        long    $00999900
        long    $00000000

        '' name=A-Z2_21
        long    $99000990
        long    $99000990
        long    $99000990
        long    $99000990
        long    $99000990
        long    $99000990
        long    $09999900
        long    $00000000

        '' name=A-Z2_22
        long    $99000990
        long    $99000990
        long    $99000990
        long    $99000990
        long    $99000990
        long    $09909900
        long    $00999000
        long    $00000000

        '' name=A-Z2_23
        long    $99000990
        long    $99000990
        long    $99000990
        long    $99090990
        long    $99090990
        long    $99999990
        long    $09909900
        long    $00000000

        '' name=A-Z2_24
        long    $99000990
        long    $99000990
        long    $09909900
        long    $00999000
        long    $09909900
        long    $99000990
        long    $99000990
        long    $00000000

        '' name=A-Z2_25
        long    $09900990
        long    $09900990
        long    $09900990
        long    $00999900
        long    $00099000
        long    $00099000
        long    $00999900
        long    $00000000

        '' name=A-Z2_26
        long    $99999990
        long    $99000990
        long    $90009900
        long    $00099000
        long    $00990090
        long    $09900990
        long    $99999990
        long    $00000000




        '' name=COLON
        long    $00000000
        long    $00000000
        long    $00099000
        long    $00099000
        long    $00000000
        long    $00099000
        long    $00099000
        long    $00000000

        '' name=SLASH
        long    $00000990
        long    $00009900
        long    $00099000
        long    $00990000
        long    $09900000
        long    $99000000
        long    $90000000
        long    $00000000

        '' name=LEFT
        long    $00000000
        long    $00000000
        long    $00090000
        long    $00990000
        long    $09999990
        long    $00990000
        long    $00090000
        long    $00000000

        '' name=RIGHT
        long    $00000000
        long    $00000000
        long    $00009000
        long    $00009900
        long    $09999990
        long    $00009900
        long    $00009000
        long    $00000000

        '' name=UP
        long    $00000000
        long    $00009000
        long    $00099900
        long    $00999990
        long    $00009000
        long    $00009000
        long    $00009000
        long    $00000000

        '' name=DN
        long    $00000000
        long    $00009000
        long    $00009000
        long    $00009000
        long    $00999990
        long    $00099900
        long    $00009000
        long    $00000000

        '' name=DOT
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00099000
        long    $00099000
        long    $00000000

        '' name=MINUS
        long    $00000000
        long    $00000000
        long    $00000000
        long    $09999990
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=EXC
        long    $00099000
        long    $00999900
        long    $00999900
        long    $00099000
        long    $00099000
        long    $00000000
        long    $00099000
        long    $00000000

        '' name=QUOT
        long    $09900990
        long    $09900990
        long    $00900900
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=CURSOR
        long    $99999999
        long    $99999999
        long    $99999999
        long    $99999999
        long    $99999999
        long    $99999999
        long    $99999999
        long    $99999999




        '' name=FRAME_1
        long    $AAAAAAAA
        long    $AA11AA11
        long    $A11AA11A
        long    $11AA11AA
        long    $1AA11AA1
        long    $AA11AA11
        long    $A11AA11A
        long    $11111111

        '' name=FRAME_2
        long    $AAAAAAAA
        long    $AA11AA11
        long    $A11AA11A
        long    $11AA11AA
        long    $1AA11AA1
        long    $AA11AA11
        long    $11111111
        long    $00000000

        '' name=RED
        long    $00222000
        long    $02222200
        long    $22222220
        long    $22222220
        long    $22222220
        long    $02222200
        long    $00222000
        long    $00000000

        '' name=GREEN
        long    $00333000
        long    $03333300
        long    $33333330
        long    $33333330
        long    $33333330
        long    $03333300
        long    $00333000
        long    $00000000

        '' name=BLUE
        long    $00444000
        long    $04444400
        long    $44444440
        long    $44444440
        long    $44444440
        long    $04444400
        long    $00444000
        long    $00000000

        '' name=YELLOW
        long    $00555000
        long    $05555500
        long    $55555550
        long    $55555550
        long    $55555550
        long    $05555500
        long    $00555000
        long    $00000000

        '' name=ORANGE
        long    $00666000
        long    $06666600
        long    $66666660
        long    $66666660
        long    $66666660
        long    $06666600
        long    $00666000
        long    $00000000

        '' name=MAGENTA
        long    $00777000
        long    $07777700
        long    $77777770
        long    $77777770
        long    $77777770
        long    $07777700
        long    $00777000
        long    $00000000

        '' name=CYAN
        long    $00888000
        long    $08888800
        long    $88888880
        long    $88888880
        long    $88888880
        long    $08888800
        long    $00888000
        long    $00000000

        '' name=WHITE
        long    $00999000
        long    $09999900
        long    $99999990
        long    $99999990
        long    $99999990
        long    $09999900
        long    $00999000
        long    $00000000



        '' name=SELECT_1
        long    $00000099
        long    $00009900
        long    $00090000
        long    $00090000
        long    $00900000
        long    $00900000
        long    $00900000
        long    $00900000

        '' name=SELECT_2
        long    $99000000
        long    $00990000
        long    $00009000
        long    $00009000
        long    $00000900
        long    $00000900
        long    $00000900
        long    $00000900

        '' name=SELECT_3
        long    $00090000
        long    $00090000
        long    $00009900
        long    $00000099
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=SELECT_4
        long    $00009000
        long    $00009000
        long    $00990000
        long    $99000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=SELECT_5
        long    $00000099
        long    $00009900
        long    $00090000
        long    $00090099
        long    $00900999
        long    $00900999
        long    $00900999
        long    $00900999

        '' name=SELECT_6
        long    $99000000
        long    $00990000
        long    $00009000
        long    $99009000
        long    $99900900
        long    $99900900
        long    $99900900
        long    $99900900

        '' name=SELECT_7
        long    $00090099
        long    $00090000
        long    $00009900
        long    $00000099
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=SELECT_8
        long    $99009000
        long    $00009000
        long    $00990000
        long    $99000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HOOK_1
        long    $00000030
        long    $00000030
        long    $00000300
        long    $30000300
        long    $03003000
        long    $00303000
        long    $00030000
        long    $00000000

        '' name=HOOK_2
        long    $20000020
        long    $02000200
        long    $00202000
        long    $00020000
        long    $00202000
        long    $02000200
        long    $20000020
        long    $00000000

        '' name=ARROW_1
        long    $00090000
        long    $00999000
        long    $09999900
        long    $99999990
        long    $00090000
        long    $00090000
        long    $00090000
        long    $00090000

        '' name=ARROW_2
        long    $00090000
        long    $00090000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=ARROW_3
        long    $00000000
        long    $00000000
        long    $00090000
        long    $00090000
        long    $00090000
        long    $00090000
        long    $00090000
        long    $00090000

        '' name=ARROW_4
        long    $99999990
        long    $09999900
        long    $00999000
        long    $00090000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000








        '' name=HAT_1
        long    $00000000
        long    $02222222
        long    $22222222
        long    $22222222
        long    $22222222
        long    $00022222
        long    $00000222
        long    $00000000

        '' name=HAT_2
        long    $00000000
        long    $22200000
        long    $22222222
        long    $22222222
        long    $22222222
        long    $22222200
        long    $22000000
        long    $00000000

        '' name=HAT_3
        long    $00000000
        long    $00000000
        long    $22222222
        long    $22222222
        long    $22222000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_4
        long    $00000000
        long    $00022000
        long    $22222222
        long    $00022000
        long    $00022000
        long    $000BB000
        long    $000BB000
        long    $00BBBB00

        '' name=HAT_5
        long    $00000022
        long    $00222222
        long    $22222222
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_6
        long    $22222222
        long    $22222222
        long    $22222222
        long    $00000222
        long    $00000002
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_7
        long    $22222220
        long    $22222222
        long    $22222222
        long    $22222222
        long    $22222220
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_8
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_9
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000004
        long    $00000044
        long    $00004444

        '' name=HAT_10
        long    $00000000
        long    $00000000
        long    $00000044
        long    $00004455
        long    $44455555
        long    $44555555
        long    $45555555
        long    $55555555

        '' name=HAT_11
        long    $00BBBB00
        long    $000BB000
        long    $55666655
        long    $56666665
        long    $66666666
        long    $66666666
        long    $66666666
        long    $66666666

        '' name=HAT_12
        long    $00000000
        long    $00000000
        long    $44000000
        long    $55440000
        long    $55555444
        long    $55555544
        long    $55555554
        long    $55555555

        '' name=HAT_13
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $40000000
        long    $44000000
        long    $44440000

        '' name=HAT_14
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000

        '' name=HAT_15
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000004
        long    $00000004
        long    $00000044
        long    $00000044

        '' name=HAT_16
        long    $00044445
        long    $00444455
        long    $04444455
        long    $44444455
        long    $44444555
        long    $44445555
        long    $44455555
        long    $44455555

        '' name=HAT_17
        long    $55555556
        long    $55555566
        long    $55555566
        long    $55555566
        long    $55555566
        long    $55555566
        long    $55555666
        long    $55555666

        '' name=HAT_18
        long    $66666666
        long    $66666666
        long    $66666666
        long    $66666666
        long    $66666666
        long    $66666666
        long    $66666666
        long    $66666666

        '' name=HAT_19
        long    $65555555
        long    $66555555
        long    $66555555
        long    $66555555
        long    $66555555
        long    $66555555
        long    $66655555
        long    $66655555

        '' name=HAT_20
        long    $54444000
        long    $55444400
        long    $55444440
        long    $55444444
        long    $55544444
        long    $55554444
        long    $55555444
        long    $55555444

        '' name=HAT_21
        long    $00000000
        long    $00000000
        long    $00000000
        long    $00000000
        long    $40000000
        long    $40000000
        long    $44000000
        long    $44000000

        '' name=HAT_22
        long    $00000444
        long    $00004444
        long    $00004444
        long    $00044444
        long    $00044444
        long    $00044444
        long    $00044444
        long    $00044444

        '' name=HAT_23
        long    $44555555
        long    $44555555
        long    $44555555
        long    $44555555
        long    $44555555
        long    $44555555
        long    $44444444
        long    $44444444

        '' name=HAT_24
        long    $55555666
        long    $55555666
        long    $55555666
        long    $55555666
        long    $55555666
        long    $55555666
        long    $44444444
        long    $44444444

        '' name=HAT_25
        long    $66666666
        long    $66666666
        long    $66666666
        long    $66666666
        long    $66666666
        long    $66666666
        long    $44444444
        long    $44444444

        '' name=HAT_26
        long    $66655555
        long    $66655555
        long    $66655555
        long    $66655555
        long    $66655555
        long    $66655555
        long    $44444444
        long    $44444444

        '' name=HAT_27
        long    $55555544
        long    $55555544
        long    $55555544
        long    $55555544
        long    $55555544
        long    $55555544
        long    $44444444
        long    $44444444

        '' name=HAT_28
        long    $44400000
        long    $44440000
        long    $44440000
        long    $44444000
        long    $44444000
        long    $44444000
        long    $44444000
        long    $44444000

        '' name=HAT_29
        long    $00044444
        long    $00444444
        long    $00444555
        long    $00444444
        long    $00044444
        long    $00000004
        long    $00000000
        long    $00000000

        '' name=HAT_30
        long    $44444445
        long    $45555555
        long    $55555555
        long    $45555555
        long    $44444444
        long    $44444444
        long    $00004444
        long    $00000000

        '' name=HAT_31
        long    $55555555
        long    $55555555
        long    $55555555
        long    $55555555
        long    $45555555
        long    $44444444
        long    $44444444
        long    $00044444

        '' name=HAT_32
        long    $55555555
        long    $55555555
        long    $55555555
        long    $55555555
        long    $55555555
        long    $44444444
        long    $44444444
        long    $44444444

        '' name=HAT_33
        long    $55555555
        long    $55555555
        long    $55555555
        long    $55555555
        long    $55555554
        long    $44444444
        long    $44444444
        long    $44444000

        '' name=HAT_34
        long    $54444444
        long    $55555554
        long    $55555555
        long    $55555554
        long    $44444444
        long    $44444444
        long    $44440000
        long    $00000000

        '' name=HAT_35
        long    $44444000
        long    $44444400
        long    $55544400
        long    $44444400
        long    $44444000
        long    $40000000
        long    $00000000
        long    $00000000






DAT


'' Sprites definitions
sprites_def


        '' name=ARROW
        long    $00000000, $00000000
        long    $00000000, $00000000
        long    $00000000, $00000000
        long    $00009000, $00000000
        long    $00099000, $00000000
        long    $00999000, $00000000
        long    $09999000, $00000000
        long    $99999999, $99999000
        long    $99999999, $99999000
        long    $09999000, $00000000
        long    $00999000, $00000000
        long    $00099000, $00000000
        long    $00009000, $00000000
        long    $00000000, $00000000
        long    $00000000, $00000000
        long    $00000000, $00000000


        '' name=SELECT
        long    $00000000, $00000000
        long    $00000000, $00000000
        long    $00000000, $00000000
        long    $00000000, $00000000
        long    $000000CC, $CC000000
        long    $00000CC0, $0CC00000
        long    $00000000, $0CC00000
        long    $00000000, $0CC00000
        long    $00000000, $CC000000
        long    $0000000C, $C0000000
        long    $0000000C, $C0000000
        long    $00000000, $00000000
        long    $0000000C, $C0000000
        long    $00000000, $00000000
        long    $00000000, $00000000
        long    $00000000, $00000000




CON

'' Tiles
FONT_SPACE     = 0
LEFTU          = 1 
LEFTL          = 2
RIGHTU         = 3
RIGHTL         = 4
BIGU           = 5
BIGL           = 6
PIN_L_BLACK    = 7
PIN_L_GREEN    = 8
PIN_L_WHITE    = 9
PIN_R_BLACK    = 10
PIN_R_GREEN    = 11
PIN_R_WHITE    = 12
PIN_GREEN      = 13
PIN_WHITE      = 14
PIN_BLACK_1    = 15
PIN_BLACK_2    = 16
PIN_BLACK_3    = 17
PIN_BLACK_4    = 18
PIN_RED_1      = 19
PIN_GREEN_1    = 23
PIN_BLUE_1     = 25
PIN_YELLOW_1   = 27
PIN_ORANGE_1   = 35
PIN_MAGENTA_1  = 39
PIN_CYAN_1     = 43
PIN_WHITE_1    = 47
PIN_X_1        = 51
FONT_0         = 55
FONT_A         = 65
FONT_COLON     = 91
FONT_SLASH     = 92
FONT_LEFT      = 93
FONT_RIGHT     = 94
FONT_UP        = 95
FONT_DN        = 96
FONT_DOT       = 97
FONT_MINUS     = 98
FONT_EXC       = 99
FONT_QUOT      = 100
FONT_CURSOR    = 101
FRAME_1        = 102
FRAME_2        = 103
RED            = 104
GREEN          = 105
BLUE           = 106
YELLOW         = 107
ORANGE         = 108
MAGENTA        = 109
CYAN           = 110
WHITE          = 111
SELECT_F       = 112
SELECT_T       = 116
HOOK_OK        = 120
HOOK_WRONG     = 121
ARROW_1        = 122
ARROW_2        = 123
ARROW_3        = 124
ARROW_4        = 125
HAT            = 126


'' Sprites

ARROW         = 0
SELECT        = 1