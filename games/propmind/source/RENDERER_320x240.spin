''*******************************
''* RENDERER_320x240.spin       *               
''* Author: Werner L. Schneider *
''*******************************
''
'' Changes:
''
'' Remove Scrolling
'' Remove Horizontal/Vertical Sprite-Flip
''
'' Based on Code from Propeller Game Engine Graphics Renderer (C) 2013 Marco Maccaferri
''
''+------------------------------------------------------------------------------------------------------------------------------+
''|                                   TERMS OF USE: Parallax Object Exchange License                                             |
''+------------------------------------------------------------------------------------------------------------------------------+
''|Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    |
''|files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    |
''|modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software|
''|is furnished to do so, subject to the following conditions:                                                                   |
''|                                                                                                                              |
''|The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.|
''|                                                                                                                              |
''|THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          |
''|WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         |
''|COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   |
''|ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         |
''+------------------------------------------------------------------------------------------------------------------------------+
''
''
CON

    hres    = 320
    vres    = 232 
    htiles  = hres/8
    vtiles  = vres/8
  
    H_TILES = 40
    V_TILES = 30

    MAP_SIZE_LONG = H_TILES * V_TILES / 2
    MAP_SIZE_WORD = H_TILES * V_TILES
    MAP_SIZE_BYTE = H_TILES * V_TILES * 2
  
    ROW_SIZE_LONG = H_TILES / 2
    ROW_SIZE_WORD = H_TILES
    ROW_SIZE_BYTE = H_TILES * 2
  
    SPRITE_TILE_MASK     = %0_0_111111_000000_000000000_000000000
    SPRITE_TILE_SHIFT    = 24
    SPRITE_PALETTE_MASK  = %0_0_000000_111111_000000000_000000000
    SPRITE_PALETTE_SHIFT = 18
    SPRITE_Y_MASK        = %0_0_000000_000000_111111111_000000000
    SPRITE_Y_SHIFT       = 9
    SPRITE_X_MASK        = %0_0_000000_000000_000000000_111111111
    SPRITE_X_SHIFT       = 0

    COGS = 3

    MAX_SPRITES = 2

    PARAM_COUNT = 4 


VAR

    long rend_tile_map
    long rend_tile_table
    long rend_sprite_table
    long rend_palette_table
    long rend_sprite_state


PUB start(p_sbuf, p_blnk, p_tile_map, p_tile_table, p_palette_table, p_sprite_table, p_sprite_state)

    base := p_sbuf                       ' scanline buffer
    blnk := p_blnk                       ' frame indicator

    rend_tile_map := p_tile_map
    rend_tile_table := p_tile_table
    rend_sprite_table := p_sprite_table
    rend_palette_table := p_palette_table
    rend_sprite_state := p_sprite_state

    longfill(rend_sprite_state, $FFFFFFFF, MAX_SPRITES)

    repeat offset from 0 to COGS-1
        cognew(@renderer, @rend_tile_map)
        waitcnt(cnt + 10000)


DAT             org     0

renderer        rdlong  a, blnk
                cmp     a, #vres wz
        if_ne   jmp     #$-2                    ' waiting for last line to be fetched

:vsync
                mov     lcnt, offset
                mov     last_palette, #$1FF

                rdlong  a, blnk
                cmp     a, #0 wz
        if_ne   jmp     #$-2

                ' begin parameters copy
                movd    :par, #tile_map
                mov     a, par
                mov     b, #PARAM_COUNT
:par            rdlong  0, a
                add     :par, inc_dest
                add     a, #4
                djnz    b, #:par
                ' end parameters copy

                ' begin sprite state copy
                mov     a, par
                add     a, #16
                rdlong  b, a
                movd    :par2, #sprites
                mov     x, #MAX_SPRITES
:sread          rdlong  a, b
:par2           mov     0, a
                add     :par2, inc_dest
                add     b, #4
                djnz    x, #:sread
                ' end sprite state copy

                rdlong  a, blnk
                cmp     a, #0 wz
        if_ne   jmp     #$-2                    ' waiting for end of vertical blank

:loop           mov     last_tile, last_sprite

                mov     hub_map_ptr, tile_map
                mov     a, lcnt
                shr     a, #3                   ' 8 lines per tile

                shl     a, #4                   ' 40 tiles per row                   changed by ws
                add     hub_map_ptr, a
                shl     a, #2                   
                add     hub_map_ptr, a

                mov     hub_tile_ptr, tile_table
                mov     a, lcnt                 ' line offset within tile
                and     a, #$07                 ' 8 lines per tile
                shl     a, #2                   ' 4 bytes per line
                add     hub_tile_ptr, a

                mov     ecnt, #40                                                   'changed by ws
                movd    :scanline_store1,#sbuf
                movd    :scanline_store2,#sbuf+1
                
:l2             rdword  tile, hub_map_ptr          ' read tile
                cmp     tile, last_tile wz
        if_e    jmp     #:same_tile
                mov     last_tile, tile

                mov     temp, tile
                shr     temp, #8
                cmp     temp, last_palette wz
        if_ne   call    #pload

                and     tile, #$FF
                shl     tile, #5                  ' 32 bytes per tile
                add     tile, hub_tile_ptr        ' convert to pointer

                rdlong  pixels1, tile
                
                ' begin pixel rendering
                mov     colors1, #0
                mov     colors2, #0

                mov     count, #4
                mov     a, pixels1
                and     a, mask15
                add     a, p2inc
                movs    :mv1, a
                rol     colors2, #8
:mv1            or      colors2, 0-0
                shr     a, #16
                movs    :mv2, a
                rol     colors1, #8
:mv2            or      colors1, 0-0
                shr     pixels1, #4
                djnz    count, #$-11
                ' end pixel rendering

:same_tile
:scanline_store1
                mov     0-0,colors1    'dest starts at cog_scanline_buffer
                add     :scanline_store1,inc_dest_2 'self-modify dest+2

:scanline_store2
                mov     0-0,colors2    'dest starts at cog_scanline_buffer
                add     :scanline_store2,inc_dest_2 'self-modify dest+2

                add     hub_map_ptr, #2
                djnz    ecnt, #:l2

                ' begin sprite rendering
                movs    :next_sprite, #sprites
                mov     scnt, #MAX_SPRITES

:next_sprite    mov     tile, 0-0
                cmp     tile, last_sprite wz
        if_e    jmp     #:skip

                mov     y, tile
                shr     y, #9
                and     y, #$1FF

                mov     a, lcnt
                subs    a, y  wc,wz
        if_b    jmp     #:skip
                cmp     a,#16 wc,wz
        if_ae   jmp     #:skip

                mov     hub_source, sprite_table
                and     a, #$0F                 ' 16 lines per tile
                shl     a, #3                   ' 8 bytes per line
                add     hub_source, a

                movs    :src1,#sbuf
                movs    :src2,#sbuf+1
                mov     a, tile
                and     a, #$1F8                ' x-position mask
                shr     a, #2
                add     :src1, a
                add     :src2, a

                movd    :dst1,#sbuf
                movd    :dst2,#sbuf+1
                shl     a, #9
                add     :dst1, a
                add     :dst2, a

                mov     x, tile
                and     x, #$07                 ' x-position mask
                mov     ccnt, #8
                sub     ccnt, x
                shl     x, #2

                mov     tile_ptr, tile
                and     tile_ptr, tile_mask
                shr     tile_ptr, #17           ' 128 bytes per tile
                add     tile_ptr, hub_source    ' convert to pointer

                mov     temp, tile              ' load palette
                shr     temp, #SPRITE_PALETTE_SHIFT
                and     temp, #$3F
                cmp     temp, last_palette wz
        if_ne   call    #pload

                mov     ecnt, #16
                mov     pixels2, #0

:sloop          rdlong  temp, tile_ptr

:src1           mov     colors1, 0-0
:src2           mov     colors2, 0-0

                mov     pixels1, temp
                mov     a, #32
                sub     a, x

                ' begin sprite pixel rendering
                shr     pixels1, x
                or      pixels1, pixels2

                mov     pixels2, temp
                shl     pixels2, a

                mov     count, #4
                mov     a, pixels1
                and     a, mask15
                add     a, p2inc
                movs    :st1, a
                rol     colors2, #8
                test    pixels1, mask1 wz
        if_nz   and     colors2, longmask
:st1    if_nz   or      colors2, 0-0
                shr     a, #16
                movs    :st2, a
                rol     colors1, #8
                test    pixels1, mask5 wz
        if_nz   and     colors1, longmask
:st2    if_nz   or      colors1, 0-0
                shr     pixels1, #4
                djnz    count, #$-15
                ' end sprite pixel rendering

:dst1           mov     0-0, colors1
:dst2           mov     0-0, colors2

                add     :src1, #2
                add     :src2, #2
                add     :dst1, inc_dest_2
                add     :dst2, inc_dest_2

                add     tile_ptr, #4

                cmp     x, #0 wz
        if_z    mov     pixels2, #0
                mov     temp, #0

                sub     ecnt, ccnt  wc,wz
                mov     ccnt, #8
        if_be   jmp     #:skip
                cmp     ecnt, #8  wc,wz
        if_ae   jmp     #:sloop
                jmp     #:sloop+1

:skip           add     :next_sprite, #1
                djnz    scnt, #:next_sprite
                ' end sprite rendering

                rdlong  a, blnk
                cmp     a, lcnt wz
        if_ne   jmp     #$-2                    ' waiting for line to be fetched

                ' begin scanline buffer copy
                mov     sbuf_ptr, base

                wrlong  sbuf, sbuf_ptr          ' Thanks Marco!
                add     sbuf_ptr, #4            '
                wrlong  sbuf + 1, sbuf_ptr      '
                add     sbuf_ptr, #4            '
                wrlong  sbuf + 2, sbuf_ptr      '
                add     sbuf_ptr, #4            '
                wrlong  sbuf + 3, sbuf_ptr      '
                add     sbuf_ptr, #4            '
                wrlong  sbuf + 4, sbuf_ptr      '
                add     sbuf_ptr, #4            '
                wrlong  sbuf + 5, sbuf_ptr      '
                add     sbuf_ptr, #4            '
                wrlong  sbuf + 6, sbuf_ptr      '
                add     sbuf_ptr, #4            '
                wrlong  sbuf + 7, sbuf_ptr      '
                add     sbuf_ptr, #4            '
                wrlong  sbuf + 8, sbuf_ptr      '
                add     sbuf_ptr, #4            '
                wrlong  sbuf + 9, sbuf_ptr      '
                add     sbuf_ptr, #4            '

                movd    :wr0, #sbuf_ptr -1      ' last long in cog buffer
                movd    :wr1, #sbuf_ptr -2      ' second-to-last long in cog buffer
                add     sbuf_ptr, #70 * 4 -1    ' last byte in hub buffer (8n + 7)           changed by ws 64 to 70
                movi    sbuf_ptr, #70 - 2       ' add magic marker                           changed by ws 64 to 70
:wr0            wrlong  0-0, sbuf_ptr           ' |
                sub     :wr0, inc_dest_2        ' |
                sub     sbuf_ptr, i2s7 wc       ' |
:wr1            wrlong  0-0, sbuf_ptr           ' |
                sub     :wr1, inc_dest_2        ' |
        if_nc   djnz    sbuf_ptr, #:wr0         ' sub #7/djnz (Thanks Phil!)
                ' end scanline buffer copy

                add     lcnt, #COGS
                cmp     lcnt, #vres  wc,wz
        if_b    jmp     #:loop

                jmp     #:vsync

                ' begin palette load
pload           mov     last_palette, temp
                shl     temp, #4                  ' 16 bytes per palette
                add     temp, palette_table

                rdlong  colors1, temp
                movd    :pstore1, #palette
                add     temp, #4
                rdlong  colors2, temp
                movd    :pstore2, #palette+4
                add     temp, #4
                rdlong  colors3, temp
                movd    :pstore3, #palette+8
                add     temp, #4
                rdlong  colors4, temp
                movd    :pstore4, #palette+12

                mov     count, #4

:pl             mov     a, colors1
                and     a, #$FF
:pstore1        mov     0-0, a
                shr     colors1, #8
                add     :pstore1, inc_dest

                mov     a, colors2
                and     a, #$FF
:pstore2        mov     0-0, a
                shr     colors2, #8
                add     :pstore2, inc_dest

                mov     a, colors3
                and     a, #$FF
:pstore3        mov     0-0, a
                shr     colors3, #8
                add     :pstore3, inc_dest

                mov     a, colors4
                and     a, #$FF
:pstore4        mov     0-0, a
                shr     colors4, #8
                add     :pstore4, inc_dest

                djnz    count, #:pl

pload_ret       ret
                ' end palette load

' initialised data and/or presets

blnk            long    -4
base            long    +0
offset          long    0

mask1           long    $0_0_0_0_0_0_0_F
mask5           long    $0_0_0_F_0_0_0_0
mask15          long    $0_0_0_F_0_0_0_F

longmask        long    $FF_FF_FF_00
tile_mask       long    SPRITE_TILE_MASK

inc_dest        long    1 << 9 << 0
inc_dest_2      long    1 << 9 << 1
i2s7            long    2 << 23 | 7
p2inc           long    palette << 16 | palette

last_sprite     long    $FFFFFFFF

' uninitialised data and/or temporaries

a               res     1
b               res     1
x               res     1
y               res     1
tile            res     1
tile_ptr        res     1
last_tile       res     1
last_palette    res     1
palette         res     16                      ' palette buffer
pixels1         res     1
pixels2         res     1
temp            res     1
colors1         res     1
colors2         res     1
colors3         res     1
colors4         res     1
count           res     1

ecnt            res     1                       ' element count
lcnt            res     1                       ' line counter
scnt            res     1                       ' scanlines
ccnt            res     1

hub_map_ptr     res     1
hub_tile_ptr    res     1
hub_sprite_ptr  res     1
hub_source      res     1

' parameters

tile_map        res     1
tile_table      res     1
sprite_table    res     1
palette_table   res     1
sprites         res     MAX_SPRITES             ' sprites state buffer

                res     4
sbuf            res     80                      ' scanline buffer
sbuf_ptr        res     4

                fit     $1F0