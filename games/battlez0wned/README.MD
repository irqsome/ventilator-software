Battlez0wned
============

## Video
TODO

## Info
- Type: Game
- Genre: 3D Shooter
- Author(s): Michael Park
- First release: 2007
- Improved version: No
- Players: 1 (well...)
- Special requirements: None
- Video formats: NTSC
- Inputs: Gamepad
- License: Public Domain / MIT

## Description
A _Battlezone_ clone. Would be siginificantly less boring if one could network two machines together...


## Controls
|Gamepad|Action|
|------|-------|
|B or Y|Shoot|
|Select|Toggle steering mode|
|Start|Hold to control other tank|

## TODOs
- Add some way to utilize the multiplayer mode - maybe add a splitscreen mode?
