'' Gamepad driver - interfaces joypads and keyboard into one interface
'' Hybrid key conversion could use some optimizing 
OBJ''   keyboard  : "comboKeyboard.spin"
CON
  ''keyboard key codes assigned to joypad keys
  ''To find the key codes for the keyboard, look at the bottom of the source code in comboKeyboard.spin
  ''player 1
  Key0_Up = $C2    ''Up arrow
  Key0_Down = $C3  ''Down arrow
  Key0_Left = $C0  ''Left arrow
  Key0_Right = $C1 ''Right arrow
  Key0_B = $C4     ''Home
  Key0_A = $C5     ''End
  Key0_Start = $0D ''Enter
  Key0_Select = $CA ''Insert
  
  Hydra = 0
  Hybrid = 1

  '' joypad serial shifter settings
  JOY_CLK = 16
  JOY_LCH = 17
  JOY_DATA0 = 19
  JOY_DATA1 = 18

  ''Hydra Joy settings 
  JOY_RIGHT  = %00000001
  JOY_LEFT   = %00000010
  JOY_DOWN   = %00000100
  JOY_UP     = %00001000
  JOY_START  = %00010000
  JOY_SELECT = %00100000
  JOY_B      = %01000000
  JOY_A      = %10000000
          
                       

PUB Read_Gamepad : nes_bits   |  i, joy0
  DIRA [JOY_CLK] := 1 ' output
  DIRA [JOY_LCH] := 1 ' output
  DIRA [JOY_DATA0] := 0 ' input

  OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
  NES_Delay
  OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0
  NES_Delay
  OUTA [JOY_LCH] := 1 ' JOY_SH/LDn = 1
  NES_Delay
  OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0
  joy0:=INA[JOY_DATA0] 
  repeat i from 0 to 6
    NES_Delay
    OUTA [JOY_CLK] := 1 ' JOY_CLK = 1
    NES_Delay
    OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
    joy0:=joy0<<1
    joy0:=joy0 | INA[JOY_DATA0]


   {
  ''handle keyboard
  if keyboard.present 
    if keyboard.keystate(Key0_Right)
     joy0&=!JOY_RIGHT
    if keyboard.keystate(Key0_Left)
     joy0&=!JOY_LEFT
    if keyboard.keystate(Key0_Down)
     joy0&=!JOY_DOWN
    if keyboard.keystate(Key0_Up) 
     joy0&=!JOY_UP
    if keyboard.keystate(Key0_Start)
     joy0&=!JOY_Start
    if keyboard.keystate(Key0_Select)
     joy0&=!JOY_Select
    if keyboard.keystate(Key0_B)
     joy0&=!JOY_B
    if keyboard.keystate(Key0_A)
     joy0&=!JOY_A             }
  ''combine the two controllers 
  nes_bits :=!(joy0 | $FF00) & $FFFF
PUB read
'' Reads the NES state and sends it back to caller
  return(Read_Gamepad)


PUB button(nes_button)
'' Return TRUE/FALSE if sent button is down
  return(nes_button & Read_Gamepad)

PUB NES_Delay  | ii, iii
''    repeat ii from 0 to 5000
      iii:= 5*675

PUB Start
'' Used to start keyboard driver
'keyboard.start(8)     