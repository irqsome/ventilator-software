''Video Blackjack Demo for TV     *
' Alpha version 1 (not really tested very well yet...)
' no mouse support yet, just use keyboard:
'   i = insert coin
'   b = bet 1
'   d = deal
'Copyright 2007 Raymond Allen
'Ported from Javascript Applet: http://www.rayslogic.com/Applets/BlackJack/blackjack.htm
'Uses:
'  playing card framework:  http://www.rayslogic.com/propeller/Programming/RaysStuff.htm
'  2-bit bitmaps:  http://www.rayslogic.com/propeller/Programming/2BitBitmap.htm




CON

  _clkmode = xtal1 + pll16x
  _xinfreq = 5_000_000
  
    'number of custom 16x16 characters              
  nuchars = 80+84   '!!!!! you must have the correct # here for alignment later

  'for display
  cols = 42      
  rows = 28
  screensize = cols * rows
  lastrow = screensize - cols
  tv_count = 14

  'for cards
  nCardsInDeck=52
  minShoe=20

  
VAR

  'variables for display
  long  col, row, color, flag                         

    'for custom characters
  word user_charbase

    'for drawing buttons
  word ptr
  byte boxcolor

  'for playing cards
  byte nDecks
  byte cards[nCardsInDeck-1]
  word nCardsLeft   

OBJ

  tv : "TV"
  kb : "Keyboard"
  'mouse : "mouse" 


PUB main|i,j
  '64 byte align the user characters
  user_charbase := @uchar & $FFC0 'destination
  'user_charbase_offset := user_charbase-@uchar  
  longmove(user_charbase,@uchar,16*nuchars)
    
  'start TV
  'start(12)
  tv_pins := (12 & $38) << 1 | (12 & 4 == 4) & %0101        'Note:  basepin:=12 
  tv_screen := @screen
  tv_colors := @colors  
  tv.start(@tv_params)
  color:=3  'set color to white on green   
  CLS 'clear screen

  'start mouse
  'start mouse and set bound parameters
  {mouse.start(24, 25)          'use pins 24 and 25
  mouse.bound_limits(0, 0, 0, cols*16, rows*16, 0) 'for pixel level resolution
  mouse.bound_scales(4, -4, 0)
  mouse.bound_preset(1,1,0)' (512, 384, 0)}

  'start the keyboard
  kb.start(8, 9)

    
 'Start
  Load
  
  'Machine Loop
  repeat
    'check keyboard and mouse
    if (kb.gotkey)
      RND1^=(cnt&$FF)  'add some chaos to the RNG
      CASE (kb.getkey)
        "I","i":
           Insert
        "B","b":
           Bet1
        "M","m":
           BetMax
        "D","d":
           Deal
        "S","s":
           Stand
        "H","h":
           Hit
        "O","o":
           DDown
        "P","p":
           Split      
      kb.clearkeys

    'update mouse
    'if (mouse.button(0))  'until left-button down

           
    'update blinking messages
    if ((cnt-messageBlinkCNT)>(CLKFREQ))    
      messageBlinkCount++
      messageBlinkCNT:=cnt'-=CLKFREQ/2
      case MachineState
        0:  'waiting for credit
          if (messageID==0) AND (messageBlinkCount>4)
            messageID:=1
        1:  'waiting for bet
          if ((messageID==0) AND (messageBlinkCount>8))
             messageID:=2  'show message if user does nothing
          if (((messageID==5) OR (messageID==6) OR (messageID==9)) AND (messageBlinkCount>2))
             messageID:=0
             messageBlinkCount:=0 'show message if user does nothing
          if (((messageID==7) OR (messageID==10)) AND(messageBlinkCount>2))
             messageID:=5
             messageBlinkCount:=0 'show message if user does nothing
          if (((messageID==8) OR (messageID==11) OR (messageID==12)) AND (messageBlinkCount>2))
             messageID:=6
             messageBlinkCount:=0  'show message if user does nothing
        2:  'waiting for deal
          if ((messageID==0) AND (messageBlinkCount>8))
             messageID:=3  'show message if user does nothing
        3,5: 'waiting for stand
          if ((messageID==0)AND(messageBlinkCount>8))
             messageID:=4  'show message if user does nothing
        4:  'dealer drawing cards
          evaluate2
          
      'show current message
      if (messageBlink==0)
        if (messageID<5)
          ShowMessage(0)
        messageBlink~~
      else
        ShowMessage(messageID)
        messageBlink~

PRI split
  'Handles the Split button
  if (machinestate<>3)
    return 0
  if (nplayercards<>2)
    return 0
  if (splitting>0)
    alert(String("You are already split!"))
    return 0
  if (((playercards[1]/4)+1)<>((playercards[2]/4)+1))
          alert(string("You can only split pairs!"))
          return 0
  if (credits<currentbet)
          alert (string("You don't have enough credits to split!"))
          return 0
  splitting:=1    
  splitblackjack:=0
  playerblackjack:=0
  splitsixcard:=0
  credits-=currentbet
  alert(String("Split:  Playing split cards one at a time."))', each with a bet of "+currentbet);
  splitcard:=playercards[2]
  playercards[2]:=drawcard
  PaintCard(7*(2-1),10*0+2,playercards[2])  
  'document.player2.src=cardimages[playercards[2]].src;
  machinestate:=5
  messageID:=0
  messageBlinkCount:=0
  evaluates


PRI ddown
  'Handle the Double Down Button
  if (splitting==1)
    alert(string("You can't double on a split here."))
    return 0
  if (machinestate<>3)
    return 0
  if (nplayercards<>2)
    return 0
  if ((playertotal<>10) AND (playertotal<>11))
          alert (string("You can only double on 10's and 11's!"))
          return 0
  if (credits<currentbet)
          alert (string("You don't have enough credits to double the bet!"))
          return 0
  '//double down
  credits-=currentbet
  currentbet+=currentbet
  alert(string("Double Down: Your bet is increased."))' to "+currentbet);
  hit
  stand


PRI EvaluateS|playeraces, i, cardnumber
  '//evaluate first split hand
  '//before player stands
  '//calculate dealer and player totals, check for blackjacks and busts
  playeraces:=0
  playertotal:=0
  '//add up value of cards except aces
  repeat i from 1 to nPlayercards
    cardnumber:=1+playercards[i]/4
    case (cardnumber)
      2..9:
        playertotal+=cardnumber
      1:
        playeraces++
      other:
        playertotal+=10
  '//factor in aces
  case (playeraces)
    0:
    1:
      playertotal+=11
      if (playertotal>21)
        playertotal-=10
    2:
      playertotal+=12
      if (playertotal>21)
        playertotal-=10
    3:
      playertotal+=13
      if (playertotal>21)
        playertotal-=10
    4:
      playertotal+=14
      if (playertotal>21)
        playertotal-=10
        
  updatetext
        
  '//check for player blackjack
  if ((nplayercards==2)AND(playertotal==21))      
    '//document.dealer1.src=cardimages[dealercards[1]].src;   //revised per Joe Schumer
    messageID:=11
    alert (String("First split hand is a BlackJack!"))
    splitblackjack:=1
    stand
    return
                
        
  '//check for player bust
  if (playertotal>21)
    '//alert("bust");
    '//document.dealer1.src=cardimages[dealercards[1]].src;   //revised per Joe Schumer
    messageID:=7
    alert (String("First split hand busts."))
    stand
    return
                
  '//check for six card win
  if (nplayercards==6)
    messageID:=12
    splitsixcard:=1
    alert (String("First split hand is a six card win!"))          
    stand
    return 


PRI Evaluate2|cardnumber,dealeraces, i, dblackjack, hiddencard
  '//after player stands
  '//calculate dealer totals, check for blackjacks and busts
  '//dealer
  dealeraces:=0
  dealertotal:=0
  '//add up value of cards except aces
  repeat i from 1 to ndealercards
     cardnumber:=1+dealercards[i]/4
     Case (cardnumber)
       2..9:
         dealertotal+=cardnumber
       1:
         dealeraces++
       other:
         dealertotal+=10

  '//factor in aces
  case (dealeraces)
    0:
    1:
      dealertotal+=11
      if (dealertotal>21)
        dealertotal-=10
    2:
      dealertotal+=12
      if (dealertotal>21)
        dealertotal-=10
    3:
      dealertotal+=13
      if (dealertotal>21)
        dealertotal-=10
    4:
      playertotal+=14
      if (dealertotal>21)
        dealertotal-=10
   
  '//check for dealer blackjack for split cases
  dblackjack:=0
  hiddencard:=1+dealercards[1]/4
  if (((cardnumber==1)And(hiddencard>9))OR((cardnumber>9)AND(hiddencard==1)))
     dblackjack:=1

  updatetext
  '//check for dealer bust
  if (dealertotal>21)
    '//alert("bust");
    if (splitting==0)
      '//the normal case
      messageID:=8
      credits+=currentbet+currentbet  '  //give the player his due
    else 
      '//player has a split hand
      if (splittotal>21)
        '//first hand busted, but second wins
        messageID:=8
        credits+=currentbet+currentbet  '//give the player his due
        if (playerblackjack==1)
          messageID:=11
        startover
        return 
      if (playertotal<22)
        '//in split situations ...
        messageID:=8
        credits+=currentbet+currentbet+currentbet+currentbet  '//give the player his due
        if (playerblackjack==1)
          messageID:=11
        alert (string("Both split hands win on dealer bust!"))
      else   '//only the first hand won, player busted
        messageID:=7
        credits+=currentbet+currentbet
        alert (string("First split hand wins on dealer bust!"))
                    
    startover
    return 


  '//check for split blackjacks
  if ((playerblackjack==1)AND(splitblackjack==1))
    '//no need to go any further
    if (dblackjack==1)
      '//double push     !!!!!!!!!  This case is not possible, game already over before split...
      alert (string("BlackJack's all around... Push."))
      credits+=currentbet+currentbet
      messageID:=9
      startover
      return 
    else
      '//double win
      alert (string("Double blackjacks both win!"))
      credits+=currentbet+currentbet+currentbet+currentbet
      messageID:=11
      startover
      return


  '//check for dealer stand
  if ((dealertotal>16)OR(ndealercards==6))               '//can't draw more than six cards!!
    if ((splitting>0)AND(splittotal<22))
      '//see if first split hand wins
      if ((splittotal>dealertotal)OR(splitsixcard==1)OR(splitblackjack==1))   '//player win
        credits+=currentbet+currentbet  '//give the player his due
        alert(string("First split hand wins"))' with "+splittotal+"!");
      else 
        if (dealertotal>splittotal)    '//dealer win
          alert(string("First split hand loses"))' with "+splittotal+".")}
        else 
          if (dealertotal==splittotal)   '//push
            alert(string("First split hand is a push"))' with "+splittotal+".");
            credits+=currentbet
     
    '//check for player busts (can only get here when split)
    if (playertotal>21)
      messageID:=7
      startover
      return 
     
    '//see if player wins
    if ((playertotal>dealertotal)OR(playerblackjack==1))   '//player win
      messageID:=6
      credits+=currentbet+currentbet  '//give the player his due
      if (playerblackjack==1)
        messageID:=11
      startover
      return 
    if (dealertotal>playertotal)   '//dealer win
      messageID:=5
      startover
      return
    if (dealertotal==playertotal)  '//push
      messageID:=9
      credits+=currentbet
      startover
      return 
        
  '//dealer draws a card
  ndealercards++
  dealercards[ndealercards]:=drawcard
  PaintCard(7*(ndealercards-1),10*1+2,dealercards[ndealercards])


PRI Hit
  '//give player a new card
  if ((machinestate<>3)AND(machinestate<>5))
    return            
  messageBlinkCount:=0
  messageID:=0
  nplayercards++
  playercards[nplayercards]:=drawcard
  PaintCard(7*(nplayercards-1),10*0+2,playercards[nplayercards])
  if (machinestate==3)
    evaluate1
  else 
    evaluates

PRI Stand
  '//player stands  
  '//check for stand on first split hand
  if (machinestate==5)'   //stand on first split hand detected
    messageID:=0
    messageBlinkCount:=0
    machinestate:=3
    splittotal:=playertotal
    '//clean up player cards
    wordfill(@screen+2*cols*2, $220, cols*4*2)
    playercards[1]:=splitcard
    'document.player1.src=cardimages[playercards[1]].src;
    PaintCard(7*0,10*0+2,playercards[1]) 
    playercards[2]:=drawcard
    nplayercards:=2
    'document.player2.src=cardimages[playercards[2]].src;
    PaintCard(7*1,10*0+2,playercards[2])
    'document.player3.src="cardblank.gif";
    'document.player4.src="cardblank.gif";
    'document.player5.src="cardblank.gif";
    'document.player6.src="cardblank.gif";
    alert(string("Now playing the second card of split!"))
    evaluate1
    return 
  if (machinestate<>3)
    return
  machinestate:=4
  messageID:=0
  '//show dealer card
  'document.dealer1.src=cardimages[dealercards[1]].src;
  PaintCard(7*0,10*1+2,dealercards[1]) 
        
PRI StartOver|i
  '//game has ended
  '//starting over
  ShowMessage(MessageID)
  splitting:=0
  splitblackjack:=0
  playerblackjack:=0
  splittotal:=0
  nplayercards:=0
  ndealercards:=0
  splitsixcard:=0
  messageBlinkCount:=0
  currentbet:=0
  if (credits>0)
    machinestate:=1
  else
    machinestate:=0

  if (nCardsLeft<minShoe)
    Shuffle  
    ''//mark all cards as unused
    'repeat i from 0 to cards-1
    '  cardused[i]:=0
    'cardsleft:=cards 
  updatetext

        
PRI Alert(pStr)
  'show long message regarding double or split
  col:=0
  row:=20
  color:=4
  str(pStr)
  'wait a couple second
  waitcnt(cnt+clkfreq*2)
  'erase message
  col:=0
  row:=20
  color:=3
  repeat cols
    out(" ")
  


PRI Evaluate1|cardnumber,dblackjack,hiddencard,playeraces,i
        '//before player stands
        '//calculate dealer and player totals, check for blackjacks and busts
        '//dealer
  cardnumber:=1+(dealercards[2]/4)
  Case (cardnumber)
    2..9:
      dealertotal:=cardnumber
    1:
      dealertotal:=11
    other:
      dealertotal:=10
        
  '//check for dealer blackjack
  dblackjack:=0
  hiddencard:=1+dealercards[1]/4
  if (((cardnumber==1)AND(hiddencard>9))OR((cardnumber>9)AND(hiddencard==1)))
    dblackjack:=1
   
  playeraces:=0
  playertotal:=0
  '//add up value of cards except aces
  repeat i from 1 to nplayercards
     cardnumber:=1+playercards[i]/4
     Case (cardnumber)
       2..9:
         playertotal+=cardnumber
       1:
         playeraces++
       other:
         playertotal+=10
  '//factor in aces
  Case (playeraces)
    '0:
    1:
      playertotal+=11
      if (playertotal>21)
        playertotal-=10
    2:
      playertotal+=12
      if (playertotal>21)
        playertotal-=10
    3:
      playertotal+=13
      if (playertotal>21)
        playertotal-=10
    4:
      playertotal+=14
      if (playertotal>21)
        playertotal-=10
  updatetext
        
  '//check for player blackjack
  if ((nplayercards==2)AND(playertotal==21))      
     if (splitting==1)
        playerblackjack:=1
        messageID:=11
        Stand
        return 
     'document.dealer1.src=cardimages[dealercards[1]].src;   //revised per Joe Schumer
     PaintCard(7*0,10*1+2,dealercards[1])
     if (dblackjack==0)
        messageID:=11
        credits+=currentbet+currentbet 
        '//process with split
        StartOver
        return
     else 
        '//two blackjacks!!! =push
        messageID:=9
        credits+=currentbet
        StartOver
        return

                
  '//now handle dealer blackjack
  if (dblackjack==1)
    if (splitting==1)
      if (splitblackjack==1)
        alert(String("First split hand is a push with two BlackJacks"))
        credits+=currentbet
      else 
        alert(String("First split hand loses with dealer BlackJack"))
   
    'document.dealer1.src=cardimages[dealercards[1]].src;
    PaintCard(7*0,10*1+2,dealercards[1]) 
    messageID:=10
    StartOver
    return

  '//check for player bust
  if (playertotal>21)
    if (splitting==1)
      if (splittotal>21)
        alert (String("Both spit hands bust."))
        StartOver'    //need this?
        return 
      else 
        machinestate:=4 '  //need dealer to draw to see if first split wins
        PaintCard(7*0,10*1+2,dealercards[1]) 
        'document.dealer1.src=cardimages[dealercards[1]].src;
        return

            
    '//alert("bust");
    'document.dealer1.src=cardimages[dealercards[1]].src;   //revised per Joe Schumer
    PaintCard(7*0,10*1+2,dealercards[1])
    messageID:=7
    StartOver
    return

                
  '//check for six card win
  if (nplayercards==6)
    'document.dealer1.src=cardimages[dealercards[1]].src;   //revised per Joe Schumer
    PaintCard(7*0,10*1+2,dealercards[1])
    messageID:=12
    credits+=currentbet+currentbet         
    StartOver
    return



PRI Deal|i,j
  if (machinestate<>2)
    return 
  machinestate:=3
  messageID:=0
  messageBlinkCount:=0    
        
  '//clean up cards
  wordfill(@screen+2*cols*2, $220, cols*4*2)
  wordfill(@screen+12*cols*2, $220, cols*4*2)
  repeat i from 0 to 1
    repeat j from 0 to 1
      'print blank cards slowly
       PaintBlankCard(7*j,10*i+2,@BlankCardEdges,11)
       waitcnt(cnt+clkfreq/4)
              
  '//////////draw first two dealer cards
  '//draw first card
  dealercards[1]:=DrawCard
        '//hide the first card
        'don't show first card
        'PaintBlankCard(7*1+3,10*0+2,@BlankCardEdges,11) 
  '//draw second card
  dealercards[2]:=drawcard
  '//show second card
  PaintCard(7*1,10*1+2,dealercards[2])
  ndealercards:=2
        
  '//////////draw first two player cards
  '//draw first card
  playercards[1]:=drawcard
  '//show the first card
  PaintCard(7*0,10*0+2,playercards[1])
  '//draw second card
  playercards[2]:=drawcard
  'show second card
  PaintCard(7*1,10*0+2,playercards[2])
  nplayercards:=2
        
  '//evaluate dealer and player cards
  Evaluate1
        
  '//update the textbox
  updatetext


PRI BetMax
  'bet maximum allowed up to credits available
  if (((machinestate==1)OR(machinestate==2))AND(currentbet<maximumbet)AND(credits>0))
      if ((credits+currentbet)>maximumbet)'{           //take some
        credits:=credits-maximumbet+currentbet
        currentbet:=maximumbet          
      else '{           //take it all
         currentbet+=credits
         credits:=0
      machinestate:=2
      messageID:=0
      messageBlinkCount:=0
  updatetext

PRI Bet1
  'bet one credit
  if (((machinestate==1)OR(machinestate==2))AND(currentbet<maximumbet)AND(credits>0))
    currentbet++
    credits--
    machinestate:=2
    messageID:=0
    messageBlinkCount:=0
  updatetext

        
PRI Insert
  'insert a coin for a credit
  credits++
  insertedcoins++
  if (machinestate==0)
    machinestate:=1
    messageID:=0
    messageBlinkCount:=0
  UpdateText
       
PRI ShowMessage(ID)
  'show blinking messages just below cards, above buttons
  col:=10
  row:=20
  color:=3
  Case ID
    0:  'blank message
      str(String("                      "))
    1:
      str(string("    Insert Coin       "))
    2:
      str(string("Press Bet 1 or Bet Max"))
    3:
      str(string("     Press Deal       "))
    4:
      str(string("  Press Hit or Stand  "))
    5:
      str(string("    Dealer Wins       "))
    6:
      str(string("    Player Wins       "))                 
    7:
      str(string("   Player Busts       "))
    8:
      str(string("   Dealer Busts       "))
    9:
      str(string("         Push         "))
    10:
      str(string(" Dealer BlackJack!    "))
    11:
      str(string(" Player BlackJack!    "))
    12:
      str(string("    Six Card Win!     "))

                 
PRI Load
  'start up cards  
  nDecks:=1 
  Shuffle 'initshoe
   
  'Set up screen
  'cls'out(0) 'clear screen  
  color:=3
  col:=0
  row:=0
  str(STRING("Player:"))
  col:=0
  row:=10
  str(STRING("Dealer:"))

  'draw buttons
  Bitmap2Bit(@Buttons, 0, 23, 42, 2, 15)

  'set state
  MachineState:=0
  messageID:=0
  messageBlinkCNT:=cnt
  messageBlinkCount:=0
  messageBlink~~

  'show text
  UpdateText




PRI UpdateText
  'update text at bottom of screen
  row:=26
  col:=0
  color:=3
  str(string("Bet: "))
  dec(currentbet)
  if (splitting<>0)
    str(string(" each"))
  col:=13
  str(string("Credits: "))
  dec(credits)
  col:=30
  str(string("Shoe: "))
  dec(nCardsLeft)
  
       
PRI Shuffle|i  'InitShoe|i
  'initialize shoe
  nCardsLeft:=nCardsInDeck*nDecks
  repeat i from 1 to nCardsInDeck
    cards[i-1]:=nDecks
    
PRI DrawCard:i
  'pick a card from shoe
  if (nCardsLeft=<0)
    Shuffle'InitShoe  'really should't get here...
  repeat
    i:=RND(nCardsInDeck)
  until cards[i]>0
  cards[i]--
  nCardsLeft--


PRI RND(n):x|r2,r5            'x[i]=(192+179*x[i-1]) mod 577; x[1]=238
    'a basic LCG psuedo-random number generator
    'http://en.wikipedia.org/wiki/Linear_congruential_generator
    'generates a number 0..576 used to return number 0..n
    'n must be much less than 577
   x:=(192+179*RND1)//577
   RND1:=x
   x:=x*n/577
   

PRI str(stringptr)

'' Print a zero-terminated string

  repeat strsize(stringptr)
    out(byte[stringptr++])


PRI dec(value) | i

'' Print a decimal number

  if value < 0
    -value
    out("-")

  i := 1_000_000_000

  repeat 10
    if value => i
      out(value / i + "0")
      value //= i
      result~~
    elseif result or i == 1
      out("0")
    i /= 10


PRI hex(value, digits)

'' Print a hexadecimal number

  value <<= (8 - digits) << 2
  repeat digits
    out(lookupz((value <-= 4) & $F : "0".."9", "A".."F"))


PRI bin(value, digits)

'' Print a binary number

  value <<= 32 - digits
  repeat digits
    out((value <-= 1) & 1 + "0")


PRI out(c) | i, k

'' Output a character
''
''     $00 = clear screen
''     $01 = home
''     $05 = USER 1-BIT CHARACTER
''     $08 = backspace
''     $09 = tab (8 spaces per)
''     $0A = set X position (X follows)
''     $0B = set Y position (Y follows)
''     $0C = set color (color follows)
''     $0D = return
''  others = printable characters

  case flag
    $00: case c
           $00: wordfill(@screen, $220, screensize)
                col := row := 0
           $01: col := row := 0
           $05: flag:=c
           $08: if col
                  col--
           $09: repeat
                  print(" ")
                while col & 7
           $0A..$0C: flag := c
                     return
           $0D: newline
           other: print(c)
    $0A: col := c // cols
    $0B: row := c // rows
    $0C: color := c & 7
    $05: uPrint(c,row,col++)
         if ++col == cols
           newline
  flag := 0

PRI CLS 'clear screen
  out(0)


PRI print(c)

  screen[row * cols + col] := (color << 1 + c & 1) << 10 + $200 + c & $FE
  screen[(row+1) * cols + col] := (color << 1 + c & 1) << 10 + $200 + c & $FE +1 
  if ++col == cols
    newline

PRI uPrint(c,ncol,nrow)
  uPrintTop(c,ncol,nrow)
  uPrintBottom(c,ncol,nrow+1)
  
PRI uPrintTop(c,ncol,nrow)
  'print top part of a character
  screen[nrow * cols + ncol] := (color << 1 + c & 1) << 10 + user_charbase>>6 + c & $FE

PRI uPrintBottom(c,ncol,nrow)
  'print bottom part of a character  
  screen[nrow * cols + ncol] := (color << 1 + c & 1) << 10 + user_charbase>>6 + c & $FE +1 

PRI Bitmap2Bit(pBitmap, xPos, yPos, xSize, ySize, clr)|c,i,j,BmpAddress
  row:=yPos
  col:=xPos
  c:=0
  BmpAddress:=pBitmap+user_charbase-@uchar
  repeat j from 0 to (ySize-1)
    repeat i from 0 to (xSize-1)
      screen[row * cols + col] := (clr) << 10 + BmpAddress>>6 +c
      'Print2Bit(c,clr,pBitmap)
      c++
      col++
    row++
    col:=xPos

PRI PaintCard(xPos,yPos,card)|rank,suit,i,j       'card is 0..51
  PaintBlankCard(xPos,yPos,@CardEdges,12)
  'dissect card number to get suit, rank
  suit:=card//4'13    '0=diamond, 1=heart, 2=club, 3=spade
  rank:=card/4'13   '0=ace, 1=2 ... 9=10, 10=Jack, 11=Queen, 12=King
  'pick color
  color:=1+suit/2  '2=red on white, 3=black on white
  'draw rank
  uPrintTop(12+rank,xPos+1,yPos+1)
  uPrintBottom(12+rank,xPos+5,yPos+6)
  'draw small suit
  uPrintTop(4+suit,xPos+1,yPos+2)
  uPrintBottom(4+suit,xPos+5,yPos+5)
  'draw at center top&bottom
  case rank
    1,2:  '2,9
      uPrintTop(suit,xPos+3,yPos+2)
      uPrintBottom(suit,xPos+3,yPos+5)
  'draw at left&right, top&bottom
  case rank
    3,4,5,6,7,8,9:  '4,5,6,9,10
      uPrintTop(suit,xPos+2,yPos+2)
      uPrintBottom(suit,xPos+2,yPos+5)
      uPrintTop(suit,xPos+4,yPos+2)
      uPrintBottom(suit,xPos+4,yPos+5)  
  'draw single at middle center 
  case rank
    0,2,4,8:  'Ace, 3,5,9
      uPrint(suit+8,xPos+3,yPos+3)
  'draw single at top center 
  case rank
    6,7,9:  '7,8,10
      uPrint(suit+8,xPos+3,yPos+2)
  'draw single at bottom center 
  case rank
    7,9:  '8,10
      uPrint(suit+8,xPos+3,yPos+4)      
  'draw single center left&right
  case rank
    5,6,7:  '6,7
      uPrint(suit+8,xPos+2,yPos+3)
      uPrint(suit+8,xPos+4,yPos+3)
  'draw both left&right center
  case rank
    8,9:      '9,10
      uprint(suit,xPos+2,yPos+3)
      uPrint(suit,xPos+4,yPos+3)
  'draw face cards faces
  case rank
    10:  'Jack
      Bitmap2Bit(@Jack, xPos+2, yPos+2, 3, 4, 12+color)
    11:  'Queen
      Bitmap2Bit(@Queen, xPos+2, yPos+2, 3, 4, 12+color)
    12:  'King
      Bitmap2Bit(@King, xPos+2, yPos+2, 3, 4, 12+color)
          
  'draw face cards suits
  case rank
    10,11:  'Jack, Queen
      uPrintBottom(suit,xPos+2,yPos+5)
      uPrintTop(suit,xPos+4,yPos+2)
    12: 'King
      uPrintTop(suit,xPos+2,yPos+2)
      uPrintBottom(suit,xPos+4,yPos+5)      
      
         
  
     



PRI PaintBlankCard(xPos,yPos,pCard,clr)|i,j,BmpAddress,c     
  c:=0
  row:=yPos
  col:=xPos
  BmpAddress:=pCard+user_charbase-@uchar
  'draw top edge
  screen[row * cols + col++] := (clr) << 10 + BmpAddress>>6 + c++
  repeat i from 1 to 5
    screen[row * cols + col++] := (clr) << 10 + BmpAddress>>6 + c
  screen[row * cols + col] := (clr) << 10 + BmpAddress>>6 + (++c)
  'draw middle
  c++
  repeat i from 1 to 6
    col:=xPos 
    row++
    screen[row * cols + col++] := (clr) << 10 + BmpAddress>>6 + c
    repeat j from 1 to 5
      screen[row * cols + col++] := (clr) << 10 + BmpAddress>>6 + c+1
    screen[row * cols + col] := (clr) << 10 + BmpAddress>>6 + c+2   
  'draw bottom
  col:=xPos
  row++
  c+=3
  screen[row * cols + col++] := (clr) << 10 + BmpAddress>>6 + c++
  repeat i from 1 to 5
    screen[row * cols + col++] := (clr) << 10 + BmpAddress>>6 + c
  screen[row * cols + col] := (clr) << 10 + BmpAddress>>6 + (++c)


    
PRI newline | i

  col := 0
  row+=2
  if (row == rows)
    row--
    wordmove(@screen, @screen[cols], lastrow)   'scroll lines
    wordfill(@screen[lastrow], $220, cols)      'clear new line


DAT

'Blackjack vars:
messageID byte 0        '//the message # that is showing (starts blank)
messageBlink byte 0  '//used to make message flash
messageBlinkCount byte 0  '//counts number of flashes
credits byte 0
insertedcoins byte 0
dealertotal byte 0
playertotal byte 0
currentbet byte 0
maximumbet byte 5
machinestate byte 0  '//0=waiting for credit, 1=waiting for bet, 2=waiting for deal, 3=waiting for stand
dealercards byte 0[5]
playercards byte 0[5]
ndealercards byte 0
nplayercards byte 0
splitting byte 0  '//flag to say when a spit is in progress
splitcard byte 0 '//the second card of the split
splittotal byte 0 '//value of first split had
splitblackjack byte 0
playerblackjack byte 0
splitsixcard byte 0

messageBlinkCNT long 0

  'input to TV driver

  
tv_params long
                        long    0               'status
                        long    1               'enable
tv_pins                 long    0               'pins
                        long    %00010          'mode        (%00000 NON-INTERLACED, %00010 FOR INTERLACED)
tv_screen               long    0               'screen
tv_colors               long    0               'colors
                        long    cols            'hc
                        long    rows            'vc
                        long    4               'hx           (9 for 20)
                        long    1               'vx          (1 NON-INTERLACED 14, 2 FOR INTERLACED 14)
                        long    0               'ho
                        long    0               'vo
                        long    0               'broadcast
                        long    0               'auralcog

long
screen word 0[screensize]                       'screen buffer

colors long  'set up colors                    'NOTE:  need two longs to define each screen text color (due to character interleaving)                                                                                                 
                        long $05_5B_05_5B       'NOTE:  last color here (LSB) is the overall background
                        long $05_05_5B_5B
                        long $BB_06_BB_06     '1: red on white
                        long $BB_BB_06_06
                        long $02_06_02_06     '2: black on white
                        long $02_02_06_06
                        long $8E_5B_8E_5B     '3: Yellow on Green
                        long $8E_8E_5B_5B
                        long $BE_5B_BE_5B     '4: Red on Green
                        long $BE_BE_5B_5B
                        long 0                       '4-color colors
                        long $06_02_BB_5B     'NOTE:  color 1 on right side (LSB), color 4 on left side (MSB)
                        long $5B_02_02_06
                        long $8E_BB_02_06 
                        long $8E_02_BB_06
                        long $BA_02_04_5B    'simple box color 1
                        long $04_5B_5B_5B  




RND1 word 238

padding LONG  7[16] 'alignment padding for the following user defined characters     

uchar long
'0,1: diamond, heart
        LONG %00001010_10100001_00101010_10000000
        LONG %00101010_10101101_11101010_10100000
        LONG %10101010_10111101_11111010_10101000
        LONG %10101010_11111111_11111110_10101000
        LONG %10101011_11111111_11111111_10101000
        LONG %10101011_11111111_11111111_10101000
        LONG %10101111_11111111_11111111_11101000
        LONG %00111111_11111111_11111111_11110000
        LONG %00101111_11111111_11111111_11100000
        LONG %00001011_11111111_11111111_10000000
        LONG %00000011_11111111_11111111_00000000
        LONG %00000000_11111111_11111100_00000000
        LONG %00000000_00111111_11110000_00000000
        LONG %00000000_00001111_11000000_00000000
        LONG %00000000_00000011_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000011_00000000_00000000
        LONG %00000000_00001111_11000000_00000000
        LONG %00000000_00111111_11110000_00000000
        LONG %00000000_11111111_11111100_00000000
        LONG %00000011_11111111_11111111_00000000
        LONG %00001011_11111111_11111111_10000000
        LONG %00101111_11111111_11111111_11100000
        LONG %00111111_11111111_11111111_11110000
        LONG %10101111_11111111_11111111_11101000
        LONG %10101011_11111111_11111111_10101000
        LONG %10101011_11111111_11111111_10101000
        LONG %10101010_11111111_11111110_10101000
        LONG %10101010_10111101_11111010_10101000
        LONG %00101010_10101101_11101010_10100000
        LONG %00001010_10100001_00101010_10000000
'2,3:  club, spade
        LONG %00000000_00000111_01000000_00000000
        LONG %00000000_00011111_11010000_00000000
        LONG %00000000_01111111_11110100_00000000
        LONG %00000000_11111111_11111100_00000000
        LONG %00000010_11111111_11111110_00000000
        LONG %00001010_10111111_11111010_10000000
        LONG %00101111_11111111_11111111_11100000
        LONG %00111111_11111111_11111111_11110000
        LONG %11111111_11111111_11111111_11111100
        LONG %11111111_11111111_11111111_11111100
        LONG %11111111_11111111_11111111_11111100
        LONG %10111111_11110011_00111111_11111000
        LONG %00101111_11000011_00001111_11100000
        LONG %00000000_00001111_11000000_00000000
        LONG %00000000_00111111_11110000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00111111_11110000_00000000
        LONG %00000000_00001111_11000000_00000000
        LONG %00101111_11000011_00001111_11100000
        LONG %10111111_11110011_00111111_11111000
        LONG %11111111_11111111_11111111_11111100
        LONG %11111111_11111111_11111111_11111100
        LONG %11111111_11111111_11111111_11111100
        LONG %00111111_11111111_11111111_11110000
        LONG %00101111_11111111_11111111_11100000
        LONG %00001010_10111111_11111010_10000000
        LONG %00000010_11111111_11111110_00000000
        LONG %00000000_11111111_11111100_00000000
        LONG %00000000_01111111_11110100_00000000
        LONG %00000000_00011111_11010000_00000000
        LONG %00000000_00000111_01000000_00000000
 '4,5:  small diamond, heart
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00101010_01101010_00000000
        LONG %00000000_10101011_01111010_10000000
        LONG %00000000_10101111_11111110_10000000
        LONG %00000000_10111111_11111111_10000000
        LONG %00000000_10111111_11111111_10000000
        LONG %00000000_00111111_11111111_00000000
        LONG %00000000_00001111_11111100_00000000
        LONG %00000000_00000011_11110000_00000000
        LONG %00000000_00000000_11000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_11000000_00000000
        LONG %00000000_00000011_11110000_00000000
        LONG %00000000_00001111_11111100_00000000
        LONG %00000000_00111111_11111111_00000000
        LONG %00000000_10111111_11111111_10000000
        LONG %00000000_10111111_11111111_10000000
        LONG %00000000_10101111_11111110_10000000
        LONG %00000000_10101011_01111010_10000000
        LONG %00000000_00101010_01101010_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
'6,7:  small club, spade
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000001_01010000_00000000
        LONG %00000000_00000101_11010100_00000000
        LONG %00000000_00000111_11110100_00000000
        LONG %00000000_00001111_11111100_00000000
        LONG %00000000_01111011_11111011_01000000
        LONG %00000001_11111111_11111111_11010000
        LONG %00000001_11111111_11111111_11010000
        LONG %00000001_11111111_11111111_11010000
        LONG %00000000_01111100_11001111_01000000
        LONG %00000000_00000011_11110000_00000000
        LONG %00000000_00000011_11110000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000011_11110000_00000000
        LONG %00000000_00000011_11110000_00000000
        LONG %00000000_01111100_11001111_01000000
        LONG %00000001_11111111_11111111_11010000
        LONG %00000001_11111111_11111111_11010000
        LONG %00000001_11111111_11111111_11010000
        LONG %00000000_01111011_11111011_01000000
        LONG %00000000_00001111_11111100_00000000
        LONG %00000000_00000111_11110100_00000000
        LONG %00000000_00000101_11010100_00000000
        LONG %00000000_00000001_01010000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
'8,9: 1diamond, 1heart
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00001010_10100001_00101010_10000000
        LONG %00101010_10101101_11101010_10100000
        LONG %10101010_10111101_11111010_10101000
        LONG %10101010_11111111_11111110_10101000
        LONG %10101011_11111111_11111111_10101000
        LONG %10101011_11111111_11111111_10101000
        LONG %10101111_11111111_11111111_11101000
        LONG %00111111_11111111_11111111_11110000
        LONG %00101111_11111111_11111111_11100000
        LONG %00001011_11111111_11111111_10000000
        LONG %00000011_11111111_11111111_00000000
        LONG %00000000_11111111_11111100_00000000
        LONG %00000000_00111111_11110000_00000000
        LONG %00000000_00001111_11000000_00000000
        LONG %00000000_00000011_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
'10,11: 1club, 1spade
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000101_01000000_00000000
        LONG %00000000_00010111_01010000_00000000
        LONG %00000000_01011111_11010100_00000000
        LONG %00000000_01111111_11110100_00000000
        LONG %00000000_11111111_11111100_00000000
        LONG %00000010_10111111_11111010_00000000
        LONG %00001111_11111111_11111111_11000000
        LONG %00111111_11111111_11111111_11110000
        LONG %01111111_11111111_11111111_11110100
        LONG %11111111_11111111_11111111_11111100
        LONG %11111111_11111111_11111111_11111100
        LONG %10111111_11111011_10111111_11111000
        LONG %10101111_11100011_00101111_11101000
        LONG %00101010_10000111_01001010_10100000
        LONG %00000000_00011111_11010000_00000000
        LONG %00000000_00101010_10100000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000
        LONG %00000000_00000000_00000000_00000000

'12,13:  Ace, 2
        LONG %00000000_00101010_11101000_00000000
        LONG %00000000_10101010_11101010_10000000
        LONG %00000000_10100000_01001010_10000000
        LONG %00000000_10100101_01011010_10000000
        LONG %00000000_10100101_01010000_00000000
        LONG %00000000_10111111_00010100_00000000
        LONG %00000000_00111111_10010100_00000000
        LONG %00000000_00111111_10010100_00000000
        LONG %00000000_01011010_10100101_01000000
        LONG %00000000_01010101_11111101_01000000
        LONG %00000000_11110101_01111111_11000000
        LONG %00000101_11110101_01011111_11010000
        LONG %00000101_11100000_00001011_11010000
        LONG %00010101_11111010_10101111_11010100
        LONG %00010101_11111010_10101111_11010100
        LONG %00010101_01010000_00000101_01010100
        LONG %00010101_01010000_00000101_01010100
        LONG %00010111_11111010_10101111_01010100
        LONG %00010111_11111010_10101111_01010100
        LONG %00000111_11100000_00001011_01010000
        LONG %00000111_11110101_01011111_01010000
        LONG %00000011_11111101_01011111_00000000
        LONG %00000001_01111111_01010101_00000000
        LONG %00000001_01011010_10100101_00000000
        LONG %00000000_00010110_11111100_00000000
        LONG %00000000_00010110_11111100_00000000
        LONG %00000000_00010100_11111110_00000000
        LONG %00000000_00000101_01011010_00000000
        LONG %00000010_10100101_01011010_00000000
        LONG %00000010_10100001_00001010_00000000
        LONG %00000010_10101011_10101010_00000000
        LONG %00000000_00101011_10101000_00000000



'34
        LONG %00000001_01010101_01010101_00000000
        LONG %00000001_01111111_01010101_00000000
        LONG %00000000_01111110_10000101_00000000
        LONG %00000000_00101111_10100000_00000000
        LONG %00000000_00101111_10100000_00000000
        LONG %00000000_00101011_11111000_00000000
        LONG %00000000_01111111_01111110_10000000
        LONG %00000001_01111111_01011110_10100000
        LONG %00001011_11111111_11111110_10100000
        LONG %00001011_11111010_10101010_10100000
        LONG %00001011_11111010_10101010_10100000
        LONG %00000001_01111010_00000101_00000000
        LONG %00000001_01111010_00000101_00000000
        LONG %00000001_01111111_01010101_00000000
        LONG %00000000_11111111_11010100_00000000
        LONG %00000000_10101010_10000000_00000000
        LONG %00000000_00000010_10101010_00000000
        LONG %00000000_00010111_11111111_00000000
        LONG %00000000_01010101_11111101_01000000
        LONG %00000000_01010000_10101101_01000000
        LONG %00000000_01010000_10101101_01000000
        LONG %00001010_10101010_10101111_11100000
        LONG %00001010_10101010_10101111_11100000
        LONG %00001010_10111111_11111111_11100000
        LONG %00001010_10110101_11111101_01000000
        LONG %00000010_10111101_11111101_00000000
        LONG %00000000_00101111_11101000_00000000
        LONG %00000000_00001010_11111000_00000000
        LONG %00000000_00001010_11111000_00000000
        LONG %00000000_01010010_10111101_00000000
        LONG %00000000_01010101_11111101_01000000
        LONG %00000000_01010101_01010101_01000000

'56
        LONG %00000001_01111111_11110101_00000000
        LONG %00000001_01111111_11110101_00000000
        LONG %00000001_01111111_11111101_00000000
        LONG %00000000_00000000_10111111_00000000
        LONG %00000000_00000000_00011111_00000000
        LONG %00000000_00101010_10111111_00000000
        LONG %00000000_01111111_11111111_00000000
        LONG %00000011_11111111_11111111_00000000
        LONG %00000011_11100000_00001010_00000000
        LONG %00000011_11100000_00001010_00000000
        LONG %00000011_11100000_00001010_00000000
        LONG %00000011_11100000_00011111_00000000
        LONG %00000011_11100000_00011111_00000000
        LONG %00000011_11111111_11111111_00000000
        LONG %00000011_11111111_11111111_00000000
        LONG %00000000_01111111_11111100_00000000
        LONG %00000000_00111111_11111101_00000000
        LONG %00000000_11111111_11111111_11000000
        LONG %00000000_11111111_11111111_11000000
        LONG %00000000_11110100_00001011_11000000
        LONG %00000000_11110100_00001011_11000000
        LONG %00000000_10100000_00001011_11000000
        LONG %00000000_10100000_00001011_11000000
        LONG %00000000_10100000_00001011_11000000
        LONG %00000000_11111111_11111111_11000000
        LONG %00000000_11111111_11111101_00000000
        LONG %00000000_11111110_10101000_00000000
        LONG %00000000_11110100_00000000_00000000
        LONG %00000000_11111110_00000000_00000000
        LONG %00000000_01111111_11111101_01000000
        LONG %00000000_01011111_11111101_01000000
        LONG %00000000_01011111_11111101_01000000

'78
        LONG %00000001_01111111_11111101_00000000
        LONG %00000001_01111111_11111101_00000000
        LONG %00000001_11111010_10101111_00000000
        LONG %00000000_11110100_00001010_00000000
        LONG %00000000_11110100_00001010_00000000
        LONG %00000000_11110100_00001010_00000000
        LONG %00000000_10100101_00001010_00000000
        LONG %00000000_00101111_10101000_00000000
        LONG %00000000_10101111_10101010_00000000
        LONG %00000000_10100101_00001010_00000000
        LONG %00000000_10100001_01011010_00000000
        LONG %00000000_10100001_01011010_00000000
        LONG %00000000_10100001_01011010_00000000
        LONG %00000000_10101011_11111010_00000000
        LONG %00000000_10101011_11111010_00000000
        LONG %00000000_00101010_10101000_00000000
        LONG %00000000_00101010_10101000_00000000
        LONG %00000000_10101111_11101010_00000000
        LONG %00000000_10101111_11101010_00000000
        LONG %00000000_10100101_01001010_00000000
        LONG %00000000_10100101_01001010_00000000
        LONG %00000000_10100101_01001010_00000000
        LONG %00000000_10100000_01011010_00000000
        LONG %00000000_10101010_11111010_00000000
        LONG %00000000_00101010_11111000_00000000
        LONG %00000000_10100000_01011010_00000000
        LONG %00000000_10100000_00011111_00000000
        LONG %00000000_10100000_00011111_00000000
        LONG %00000000_10100000_00011111_00000000
        LONG %00000000_11111010_10101111_01000000
        LONG %00000000_01111111_11111101_01000000
        LONG %00000000_01111111_11111101_01000000

'9T
        LONG %00000000_11111111_11010110_10100000
        LONG %00001010_11111111_11110110_10100000
        LONG %00001011_11111111_11110111_10100000
        LONG %00001011_11000000_10110111_10100000
        LONG %00001011_11000000_10110111_10100000
        LONG %00001011_11000000_10110111_10100000
        LONG %00001011_11000000_10110111_10100000
        LONG %00001011_11000000_10110111_10100000
        LONG %00001011_11010101_11110111_10100000
        LONG %00001011_11010101_11110110_10100000
        LONG %00001011_11010101_11110110_10100000
        LONG %00001011_11000000_10100010_10100000
        LONG %00001011_11010100_10100010_10100000
        LONG %00001010_11111111_11110110_10100000
        LONG %00000000_11111111_11010110_10100000
        LONG %00000000_10111111_11010110_10100000
        LONG %00001010_10010111_11111110_00000000
        LONG %00001010_10010111_11111111_00000000
        LONG %00001010_10011111_11111111_10100000
        LONG %00001010_10001010_00010111_11100000
        LONG %00001010_10001010_00000011_11100000
        LONG %00001010_10011111_01010111_11100000
        LONG %00001010_10011111_01010111_11100000
        LONG %00001010_11011111_01010111_11100000
        LONG %00001010_11011110_00000011_11100000
        LONG %00001010_11011110_00000011_11100000
        LONG %00001010_11011110_00000011_11100000
        LONG %00001010_11011110_00000011_11100000
        LONG %00001010_11011110_00000011_11100000
        LONG %00001010_11011111_11111111_11100000
        LONG %00001010_10011111_11111111_10100000
        LONG %00001010_10010111_11111111_00000000

'JQ
        LONG %00000000_01111111_11111000_00000000
        LONG %00000000_11111111_11111010_00000000
        LONG %00000000_10100101_00001010_00000000
        LONG %00000000_10100101_00001010_00000000
        LONG %00000000_10100101_00001010_00000000
        LONG %00000000_10100101_00001010_00000000
        LONG %00000000_10100101_00001010_00000000
        LONG %00000000_10100101_00001010_00000000
        LONG %00000000_10100101_00001010_00000000
        LONG %00000000_10100101_00001010_00000000
        LONG %00000000_10100101_00001011_01000000
        LONG %00000000_10100101_00001011_01000000
        LONG %00000000_10101111_10101011_01000000
        LONG %00000000_00101111_11111101_01000000
        LONG %00000010_10101001_01010101_00000000
        LONG %00000010_10100000_00000000_00000000
        LONG %00000000_00000000_00001010_10000000
        LONG %00000000_01010101_01101010_10000000
        LONG %00000001_01111111_11111000_00000000
        LONG %00000001_11101010_11111010_00000000
        LONG %00000001_11100000_01011010_00000000
        LONG %00000001_11100000_01011010_00000000
        LONG %00000000_10100000_01011010_00000000
        LONG %00000000_10100000_01011010_00000000
        LONG %00000000_10100000_01011010_00000000
        LONG %00000000_10100000_01011010_00000000
        LONG %00000000_10100000_01011010_00000000
        LONG %00000000_10100000_01011010_00000000
        LONG %00000000_10100000_01011010_00000000
        LONG %00000000_10100000_01011010_00000000
        LONG %00000000_10101111_11111111_00000000
        LONG %00000000_00101111_11111101_00000000

'K
        LONG %00001111_11111111_00111111_11110000
        LONG %00001111_11111111_00111111_11110000
        LONG %00001111_11111111_00111111_11110000
        LONG %00000000_00111111_00001111_11000000
        LONG %00000000_00001111_11001111_11000000
        LONG %00000000_00001111_11001111_11000000
        LONG %00000000_00000000_11111111_11000000
        LONG %00000000_00000000_00111111_11000000
        LONG %00000000_00000000_11111111_11000000
        LONG %00000000_00000000_11111111_11000000
        LONG %00000000_00001111_11001111_11000000
        LONG %00000000_00111111_00001111_11000000
        LONG %00000000_11110000_00001111_11000000
        LONG %00001111_11111111_00111111_11110000
        LONG %00001111_11111111_00111111_11110000
        LONG %00001111_11111111_00111111_11110000
        LONG %00001111_11111100_11111111_11110000
        LONG %00001111_11111100_11111111_11110000
        LONG %00001111_11111100_11111111_11110000
        LONG %00000011_11110000_00001111_00000000
        LONG %00000011_11110000_11111100_00000000
        LONG %00000011_11110011_11110000_00000000
        LONG %00000011_11111111_00000000_00000000
        LONG %00000011_11111111_00000000_00000000
        LONG %00000011_11111100_00000000_00000000
        LONG %00000011_11111111_00000000_00000000
        LONG %00000011_11110011_11110000_00000000
        LONG %00000011_11110011_11110000_00000000
        LONG %00000011_11110000_11111100_00000000
        LONG %00001111_11111100_11111111_11110000
        LONG %00001111_11111100_11111111_11110000
        LONG %00001111_11111100_11111111_11110000



CardEdges long
        file "edges.dat"  '3x3=9
BlankCardEdges long
        file "edgesblank.dat"  '3x3=9        
King long
        file "king.dat"  '3x4=12
Queen long
        file "queen.dat"  '3x4=12
Jack long
        file "jack.dat"   '3x4=12
Buttons long
        file "BJ_Buttons.dat" '42x2=84
        