' --------------------------------------------------------------------------------
' LameLCD.spin
' Version: 0.6.2-rc2
' Copyright (c) 2015-2016 LameStation LLC
' See end of file for terms of use.
' --------------------------------------------------------------------------------
OBJ
    pin  :   "LamePinout"

CON
    ' LCD pins
    {DI = pin#DI         ' data / instruction
    EN = pin#E          ' enable to send data
    
    DB = pin#D0         ' starting data bit
    
    CSA = pin#CSA       ' chip select a
    CSB = pin#CSB       ' chip select b
      
    LCDstart = DI
    LCDend   = CSB

    ' period
    SYNC_PERIOD = 80_000_000/73
    BUSY_PERIOD = 400}


  fntsc         = 3_579_545     'NTSC color frequency
  lntsc         = 3640          'NTSC color cycles per line * 16
  sntsc         = 624           'NTSC color cycles per sync * 16
  wntsc         = 5             'NTSC half-pixel width (for 256 pixel lines)

  fpal          = 4_433_618     'PAL60 color frequency
  lpal          = {4540}4508    'PAL60 color cycles per line * 16
  spal          = {848}773      'PAL60 color cycles per sync * 16
  wpal          = 6             'PAL60 half-pixel width (for 256 pixel lines)

  paramcount    = 29
    
    ' screen size
    SCREEN_W = 128
    SCREEN_H = 64
    
    ' frame rate
    TURBOSPEED = 80
    FULLSPEED = 40
    HALFSPEED = 20
    QUARTERSPEED = 10


    ' for internal use
    CMD_SETSCREEN     = $01F
    CMD_SETFRAMELIMIT = $022
    CMD_DRAWSCREEN    = $026
    CMD_INVERTSCREEN  = $036

PUB Start(buffer) 'must be long aligned
{{
    Initializes the LCD object.
    
    parameters
       buffer: source buffer (usually provided by LameGFX)
    
    result
       Aborts when any part of the initialization fails, otherwise returns
       the address of the screen buffer.
}}
    byte[$7fff] := 0
    _draw := buffer
    ifnot cognew(@screen, @insn) +1
      abort

    waitcnt(cnt+$2000)
    'Exec(CMD_SETSCREEN, 0)                             ' make sure cog is running
    longfill(@screen{0}, 0, 512)                        ' clear screen
    screenptr := @screen
    'Exec(CMD_SETSCREEN, @screen{0})                    ' activate screen

    return @screen{0}


PUB Draw
{{
    Copy render buffer to screen buffer.
}}
    repeat
    while framen & lookup(rate:%1,%11,%111,%1111)
    WaitForVerticalSync
    
    longmove(screenptr,_draw,512)
    'Exec(CMD_DRAWSCREEN, _draw)

PUB SetFrameLimit(frequency)
{{
    Set user-defined frame limit (0: off)
}}

    'rate := clkfreq / frequency                        ' division by 0 is 0 in SPIN
    'Exec(CMD_SETFRAMELIMIT, @rate)
    rate := lookdownz(frequency:TURBOSPEED,FULLSPEED,HALFSPEED,QUARTERSPEED)
    
PUB InvertScreen(enabled) ' boolean value
{{
    Invert black/white but leave gray untouched.
}}

    'Exec(CMD_INVERTSCREEN, enabled <> 0)
    invert := enabled
    
PUB WaitForVerticalSync
{{
    Block execution until vertical sync pulse starts.
}}
        repeat
        until sync.byte{0}
        repeat
        while sync.byte{0}                              ' 1/0 transition


PUB tvpal(tp)

  pal := tp

DAT                                                     ' DAT mailbox

_draw                   long    0
rate                    long    0

insn                    long    0                       ' screen[-4]
sync                    long    0                       ' screen[-3]
screenptr               long    0                       ' screen[-2]
pal                     long    $05_0C_07_02                       
invert                  long    0
framen                  long    0


'*******************************
'* Assembly language TV driver *
'*******************************

                        org
'
'
' Entry
'
screen                  mov     taskptr,#tasks          'reset tasks

                        mov     x,#10                   'perform task sections initially
:init                   jmpret  taskret,taskptr
                        djnz    x,#:init
'
'
' Superfield
'
superfield              mov     taskptr,#tasks          'reset tasks

                        test    _mode,#%0001    wc      'if ntsc, set phaseflip
        if_nc           mov     phaseflip,phasemask

                        test    _mode,#%0010    wz      'get interlace into nz
'
'
' Field
'
field                   mov     x,vinv                  'do invisible back porch lines
:black                  call    #hsync                  'do hsync
                        waitvid burst,sync_high2        'do black
                        jmpret  taskret,taskptr         'call task section (z undisturbed)
                        djnz    x,#:black               'another black line?

                        mov     t1,par
                        add     t1,#4
                        wrlong  visible,t1              'set status to visible

                        mov     x,vb                    'do visible back porch lines
                        call    #blank_lines

                        mov     screen,par              'point to first tile (upper-leftmost)
                        add     screen,#8
                        rdlong  line,screen
                        mov     y,#64                   'set vertical tiles
:line                   test    _wide,#1      wc        'get widescreen into c
        if_c            mov     vx,#3                   'set vertical expand
        if_nc           mov     vx,#2
                        
:vert   'if_z           xor     interlace,#1            'interlace skip?
        'if_z           tjz     interlace,#:skip
                        mov     screen,line

                        call    #hsync                  'do hsync

                        mov     vscl,hb                 'do visible back porch pixels
                        xor     tile,_palette
                        waitvid tile,borderpixels

                        mov     x,#8                    'set horizontal tiles
                        mov     vscl,hx                 'set horizontal expand

:tile                   rdlong  pixels,screen           'read tile
                        add     screen,#4               'point to next tile
                        mov     tile,phaseflip
:color                  xor     tile,_palette
                        waitvid tile,pixels             'pass colors and pixels to video
                        djnz    x,#:tile                'another tile?

                        sub     screen,hc2x             'repoint to first tile in same line

                        mov     vscl,hf                 'do visible front porch pixels
                        mov     tile,phaseflip
                        xor     tile,_palette
                        waitvid tile,borderpixels

:skip                   djnz    vx,#:vert               'vertical expand?
                        add     line,#32                'set next line
                        add     screen,hc2x             'point to first tile in next line
                        djnz    y,#:line                'another tile line?

        if_z            {x}or   interlace,#1    wz      'get interlace and field1 into z  

                        test    _mode,#%0001    wc      'do visible front porch lines
                        mov     x,vf
        if_nz_and_c     add     x,#1
                        call    #blank_lines

        if_nz           wrlong  invisible,t1            'unless interlace and field1, set status to invisible (t1 still points to sync)

        if_z_eq_c       call    #hsync                  'if required, do short line
        if_z_eq_c       mov     vscl,hrest
        if_z_eq_c       waitvid burst,sync_high2
        if_z_eq_c       xor     phaseflip,phasemask

                        call    #vsync_high             'do high vsync pulses

                        movs    vsync1,#sync_low1       'do low vsync pulses
                        movs    vsync2,#sync_low2
                        call    #vsync_low

                        call    #vsync_high             'do high vsync pulses

        if_nz           mov     vscl,hhalf              'if odd frame, do half line
        if_nz           waitvid burst,sync_high2

        if_z            jmp     #field                  'if interlace and field1, display field2
                        jmp     #superfield             'else, new superfield
'
'
' Blank lines
'
blank_lines             call    #hsync                  'do hsync

                        xor     tile,_palette           'do background
                        waitvid tile,borderpixels

                        djnz    x,#blank_lines

blank_lines_ret         ret
'
'
' Horizontal sync
'
hsync                   test    _mode,#%0001    wc      'if pal, toggle phaseflip
        if_c            xor     phaseflip,phasemask

                        mov     vscl,sync_scale1        'do hsync       
                        mov     tile,phaseflip
                        xor     tile,burst
                        waitvid tile,sync_normal

                        mov     vscl,hvis               'setup in case blank line
                        mov     tile,phaseflip

hsync_ret               ret
'
'
' Vertical sync
'
vsync_high              movs    vsync1,#sync_high1      'vertical sync
                        movs    vsync2,#sync_high2

vsync_low               mov     x,vrep

vsyncx                  mov     vscl,sync_scale1
vsync1                  waitvid burst,sync_high1

                        mov     vscl,sync_scale2
vsync2                  waitvid burst,sync_high2

                        djnz    x,#vsyncx
vsync_low_ret
vsync_high_ret          ret
'
'
' Tasks - performed in sections during invisible back porch lines
'
tasks                   {mov     t1,par                  'load parameters
                        movd    :par,#_enable           '(skip _status)
                        mov     t2,#paramcount - 1
:load                   add     t1,#4
:par                    rdlong  0,t1
                        add     :par,d0
                        djnz    t2,#:load               '+119}

                        mov     t1,par
                        add     t1,#20
                        wrlong  framecnt,t1
                        add     framecnt,#1

                        rdbyte  t1,haxx
                        test    t1,#1           wc
                        muxc    _wide,#1
                        test    t1,#2           wc
                        muxc    _mode,#1

                        mov     t1,_pins                'set video pins and directions
                        test    t1,#$08         wc
        if_nc           mov     t2,pins0
        if_c            mov     t2,pins1
                        test    t1,#$40         wc
                        shr     t1,#1
                        shl     t1,#3
                        shr     t2,t1
                        movs    vcfg,t2
                        shr     t1,#6
                        movd    vcfg,t1
                        shl     t1,#3
                        and     t2,#$FF
                        shl     t2,t1
        if_nc           mov     dira,t2
        if_nc           mov     dirb,#0
        if_c            mov     dira,#0
        if_c            mov     dirb,t2                 '+18

                        tjz     _enable,#disabled       '+2, disabled?

                        jmpret  taskptr,taskret         '+1=140, break and return later

                        movs    :rd,#wtab               'load ntsc/pal metrics from word table
                        movd    :wr,#hvis
                        mov     t1,#wtabx - wtab
                        test    _mode,#%0001    wc
:rd                     mov     t2,0
                        add     :rd,#1
        if_nc           shl     t2,#16
                        shr     t2,#16
:wr                     mov     0,t2
                        add     :wr,d0
                        djnz    t1,#:rd                 '+54

        if_nc           movs    :ltab,#ltab             'load ntsc/pal metrics from long table
        if_c            movs    :ltab,#ltab+1
                        movd    :ltab,#fcolor
                        mov     t1,#(ltabx - ltab) >> 1
:ltab                   mov     0,0
                        add     :ltab,d0s1
                        djnz    t1,#:ltab               '+17

                        {rdlong t1,#0                   'get CLKFREQ
                        shr     t1,#1                   'if CLKFREQ < 16MHz, cancel _broadcast
                        cmp     t1,m8           wc
        if_c            mov     _broadcast,#0
                        shr     t1,#1                   'if CLKFREQ < color frequency * 4, disable
                        cmp     t1,fcolor       wc
        if_c            jmp     #disabled               '+11 }

                        jmpret  taskptr,taskret         '+1=83, break and return later

                        mov     t1,fcolor               'set ctra pll to fcolor * 16
                        call    #divide                 'if ntsc, set vco to fcolor * 32 (114.5454 MHz)
                        test    _mode,#%0001    wc      'if pal, set vco to fcolor * 16 (70.9379 MHz)
        if_c            movi    ctra,#%00001_111        'select fcolor * 16 output (ntsc=/2, pal=/1)
        if_nc           movi    ctra,#%00001_110
        if_nc           shl     t2,#1
                        mov     frqa,t2                 '+147

                        jmpret  taskptr,taskret         '+1=148, break and return later

                        mov     t1,#0'_broadcast        'set ctrb pll to _broadcast
                        mov     t2,#0                   'if 0, turn off ctrb
                        tjz     t1,#:off
                        min     t1,m8                   'limit from 8MHz to 128MHz
                        max     t1,m128
                        mov     t2,#%00001_100          'adjust _broadcast to be within 4MHz-8MHz
:scale                  shr     t1,#1                   '(vco will be within 64MHz-128MHz)
                        cmp     m8,t1           wc
        if_c            add     t2,#%00000_001
        if_c            jmp     #:scale
:off                    movi    ctrb,t2
                        call    #divide
                        mov     frqb,t2                 '+165

                        jmpret  taskptr,taskret         '+1=166, break and return later

                        mov     t1,#%10100_000          'set video configuration
                        test    _pins,#$01      wc      '(swap broadcast/baseband output bits?)
        if_c            or      t1,#%01000_000
                        test    _mode,#%1000    wc      '(strip chroma from broadcast?)
        if_nc           or      t1,#%00010_000
                        test    _mode,#%0100    wc      '(strip chroma from baseband?)
        if_nc           or      t1,#%00001_000
                        'and    _auralcog,#%111         '(set aural cog)
                        or      t1,#0'_auralcog
                        movi    vcfg,t1                 '+10
                                        
                        mov     hx,_hx                  'compute horizontal metrics
                        shl     hx,#8
                        or      hx,_hx
                        shl     hx,#4

                        mov     hc2x,#8
                        shl     hc2x,#1

                        mov     t1,#8
                        mov     t2,_hx
                        call    #multiply
                        mov     hf,hvis
                        sub     hf,t1
                        shr     hf,#1           wc
                        mov     hb,_ho
                        addx    hb,hf
                        sub     hf,_ho                  '+52

                        mov     t1,#64/16               'compute vertical metrics
                        test    _wide,#1        wc
        if_nc           mov     t2,#2
        if_c            mov     t2,#3
                        call    #multiply
                        test    _mode,#%10000   wc      'consider tile size
                        muxc    linerot,#1
                        mov     lineadd,lineinc
        if_c            shr     lineadd,#1
        if_c            shl     t1,#1
      '                 test    _mode,#%0010    wc      'consider interlace
      ' if_c            shr     t1,#1
                        mov     vf,vvis
                        sub     vf,t1
                        shr     vf,#1           wc
                        neg     vb,_vo
                        addx    vb,vf
                        add     vf,_vo                  '+53

                        'xor    _mode,#%0010            '+1, flip interlace bit for display

:colors                 jmpret  taskptr,taskret         '+1=117/160, break and return later

                        mov     t1,par
                        add     t1,#12
                        rdlong  _palette,t1
                        add     t1,#4
                        rdlong  junk,t1
                        tjz     junk,#:noinvert
                        mov     t1,_palette
                        andn    _palette,thisthing
                        mov     t2,t1
                        and     t2,#$FF
                        shl     t2,#8
                        or      _palette,t2
                        shr     t1,#8
                        and     t1,#$FF 
                        or      _palette,t1
                        
:noinvert
                        

                        jmp     #:colors                '+1, keep loading colors
'
'
' Divide t1/CLKFREQ to get frqa or frqb value into t2
'
divide                  rdlong  m1,#0                   'get CLKFREQ

                        mov     m2,#32+1
:loop                   cmpsub  t1,m1           wc
                        rcl     t2,#1
                        shl     t1,#1
                        djnz    m2,#:loop

divide_ret              ret                             '+140
'
'
' Multiply t1 * t2 * 16 (t1, t2 = bytes)
'
multiply                shl     t2,#8+4-1

                        mov     m1,#8
:loop                   shr     t1,#1           wc
        if_c            add     t1,t2
                        djnz    m1,#:loop

multiply_ret            ret                             '+37
'
'
' Disabled - reset status, nap ~4ms, try again
'
disabled                mov     ctra,#0                 'reset ctra
                        mov     ctrb,#0                 'reset ctrb
                        mov     vcfg,#0                 'reset video

                        wrlong  outa,par                'set status to disabled

                        rdlong  t1,#0                   'get CLKFREQ
                        shr     t1,#8                   'nap for ~4ms
                        min     t1,#3
                        add     t1,cnt
                        waitcnt t1,#0

                        jmp     #screen                 'reload parameters


'
'
' Initialized data
'
m8                      long    8_000_000
m128                    long    128_000_000
d0                      long    1 << 9 << 0
d6                      long    1 << 9 << 6
d0s1                    long    1 << 9 << 0 + 1 << 1
interlace               long    0
invisible               long    0
visible                 long    -1
phaseflip               long    $00000000
phasemask               long    $F0F0F0F0
line                    long    $00060000
lineinc                 long    $10000000
linerot                 long    0
pins0                   long    %11110000_01110000_00001111_00000111
pins1                   long    %11111111_11110111_01111111_01110111
sync_high1              long    %0101010101010101010101_101010_0101
sync_high2              long    %01010101010101010101010101010101       'used for black
sync_low1               long    %1010101010101010101010101010_0101
sync_low2               long    %01_101010101010101010101010101010

borderpixels            long    $AAAAAAAA
haxx                    long    $7FFF
thisthing               long    $0000FFFF
'
'
' NTSC/PAL metrics tables
'                               ntsc                    pal
'                               ----------------------------------------------
wtab                    word    lntsc - sntsc,          lpal - spal     'hvis
                        word    lntsc / 2 - sntsc,      lpal / 2 - spal 'hrest
                        word    lntsc / 2,              lpal / 2        'hhalf
                        word    243,                    243{286}         'vvis
                        word    10,                     10{18}           'vinv
                        word    6,                      5{5}            'vrep
                        word    $02_7A,                 $02_AA          'burst
                        word    wntsc*4,                wpal*4          'hx
wtabx
ltab                    long    fntsc                                   'fcolor
                        long    fpal
                        long    sntsc >> 4 << 12 + sntsc                'sync_scale1
                        long    spal >> 4 << 12 + spal
                        long    67 << 12 + lntsc / 2 - sntsc            'sync_scale2
                        long    79 << 12 + lpal / 2 - spal
                        long    %0101_00000000_01_10101010101010_0101   'sync_normal
                        long    %010101_00000000_01_101010101010_0101
ltabx


'
'
' Parameter buffer
'
_enable                 long    1       '0/non-0        read-only
_pins                   long    pin#TV  '%pppmmmm       read-only
_mode                   long    0       '%tccip         read-only
_wide                   long    0
_palette                long    0
'_screen                res     1       '@word          read-only
'_colors                res     1       '@long          read-only
'_ht                    res     1       '1+             read-only
'_vt                    res     1       '1+             read-only
'_hx                    res     1       '4+             read-only
'_vx                    res     1       '1+             read-only
_ho                     long    0       '0+-            read-only
_vo                     long    0       '0+-            read-only
'_broadcast             res     1       '0+             read-only
'_auralcog              res     1       '0-7            read-only
EOD 
'
'
' Uninitialized data
'
taskptr                 res     1                       'tasks
taskret                 res     1
t1                      res     1
t2                      res     1
m1                      res     1
m2                      res     1
junk                    res     1
framecnt                res     1

x                       res     1                       'display
y                       res     1
hf                      res     1
hb                      res     1
vf                      res     1
vb                      res     1
hx                      res     1
vx                      res     1
hc2x                    res     1
'screen                 res     1
tile                    res     1
pixels                  res     1
lineadd                 res     1

hvis                    res     1                       'loaded from word table
hrest                   res     1
hhalf                   res     1
vvis                    res     1
vinv                    res     1
vrep                    res     1
burst                   res     1
_hx                     res     1

fcolor                  res     1                       'loaded from long table
sync_scale1             res     1
sync_scale2             res     1
sync_normal             res     1


                        ''fit     colortable              'fit underneath colortable ($180-$1BF)
{screen padding}        long    -1[0 #> (512 - (@EOD - @screen) / 4)]                        

DAT
' --------------------------------------------------------------------------------------------------------
' TERMS OF USE: MIT License
'
' Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
' associated documentation files (the "Software"), to deal in the Software without restriction, including
' without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
' copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
' following conditions:
'
' The above copyright notice and this permission notice shall be included in all copies or substantial
' portions of the Software.
'
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
' LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
' IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
' WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
' SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
' --------------------------------------------------------------------------------------------------------
DAT