'*********************************************************************
'* The Graphic driver for Gridrunner                                 *
'*                                                                   *
'* This just supports a basic Commodore 64 screen                    *
'*                                                                   *
'* The basic idea is to support a 40x25 display with two colors.     *
'* Each cell has it's own 8x8 character.                             *
'*                                                                   *
'*********************************************************************

' 0.1 Initial release, copied from the C64 demo.
' 0.2 Pass the pointer to the background so it can be changed on the fly. 


' Thanks to Baggers, CardboardGuru and Potatohead...

CON
  ' Set up the processor clock in the standard way for 80MHz
  _CLKMODE = xtal1 + pll16x
  _XINFREQ = 5_000_000 + 0000

PUB start(params) 

  'We don't do much here, but launch the driver and point it to it's parameter block, defined by
  'the calling program.
  
  cognew(@entry,params)

DAT                     org     0
entry                   jmp     #initialization         'Start here...

' ***************
' * NTSC primer *
' ***************

' There is no way you can write a Video Driver unless you have a grip of the NTSC signal.
' Open up the Hydra book and read Chapter 7, Composite NTSC/PAL Video Hardware, pages 103 to 114.

' The main things you need to grasp is,
' NTSC has 262 video lines, of those 244 are usable.
' Every video line is 63.5 us, with a Horizontal blanking period of 10.9 us and Active video of 52.6 us.
' The Vertical blanking uses up 18 lines which explains why only 244 are usable.
' The color Frequency of NTSC is 3.579545 MHz     

' *****************************************
' * Frequency generation and calculation. *
' *****************************************

' First you need a multiple of the NTSC color frequency (3.579545 MHz).
' Counter A needs to be set in a way to produce either this frequency or a multiple there of.
' Counter A is then used by the VSU to generate video.    
' If you read the Propeller Chip Architecture chapter in the Hydra book you should be able to figure it out... ;-)
' Otherwise read pages 213 to 232 over and over.    

' From the Hydra book, FRQx=(Fsmb*2^32)/Fm
' Where FRQx is the value for the Counter A Frequency register, Fsmb is the desired output Frequency and Fm is the system clock.
' In this example, FRQA = 3579545*4294967296/80000000 => FRQA = 192175359 ($B745CFF).
' 

' There is a standard way in assembler to calculate this so you only need to specify your Frequncy.

'                       mov     r1, NTSC_color_freq     ' r1: Color frequency in Hz (3.579_545MHz)
'                       rdlong   v_clkfreq, #0          ' copy system clock from main memory location 0. (80Mhz)
'                       mov     r2, v_clkfreq           ' r2: CLKFREQ (80MHz)
                        ' perform r3 = 2^32 * r1 / r2
'                       mov     r0,#32+1
':loop                  cmpsub  r1,r2           wc
'                       rcl     r3,#1
'                       shl     r1,#1
'                       djnz    r0,#:loop
'                       mov     v_freq, r3              ' v_freq now contains frqa.
'                       mov     FRQA, r3                ' set frequency for counter A

' Now that you have the frequency figured out, let's look at the control register.
' For the counter mode (CTRMODE field, bits 30..26), there is really only one setting that is interesting for video.
' %00001 is the PLL internal for video setting.
' For the PLL mode (PLLDIV field, bits 25..23) you want no division, which means %111.
' Look at page 216 in the Hydra book, the actual counter goes through a PLL that multiplies the frequency by 16.
' So even though you programmed the counter for 3.579545 MHz, the output is 16*3.579545MHz.
' If you really wanted 3.579545 MHz, you should program the PLL mode with %011

' In assembler there is a convinient way of setting these registers by using movi.
'                       movi    CTRA,#%00001_111        ' pll internal routed to Video, PHSx+=FRQx (mode 1) + pll(16x)

' You are now done programming the counter for an output Frequency of 57.27272 MHz with a period of 17.46 ns.

' ****************************
' * VSU Video Streaming Unit *
' ****************************

' This is the actual hardware that generates the video signal.
' If you haven't done so, read chapter 14, Video Cog Hardware, pages 233 to 243.

' This Video configuration is a standard setup for the Hydra and described in the Hydra book, not much to add...

'                       movs    VCFG, #%0000_0111       ' VCFG'S = pinmask (pin31: 0000_0111 : pin24)
'                       movd    VCFG, #3                ' VCFG'D = pingroup (grp. 3 i.e. pins 24-31)
'                       movi    VCFG, #%0_10_111_000    ' baseband video on bottom nibble, 2-bit color, enable chroma on broadcast & baseband
                                                        ' %0_xx_x_x_x_xxx : Not used
                                                        ' %x_10_x_x_x_xxx : Composite video to top nibble, broadcast to bottom nibble
                                                        ' %x_xx_1_x_x_xxx : 4 color mode
                                                        ' %x_xx_x_1_x_xxx : Enable chroma on broadcast
                                                        ' %x_xx_x_x_1_xxx : Enable chroma on baseband
                                                        ' %x_xx_x_x_x_000 : Broadcast Aural FM bits (don't care)

' Once the VSU is setup to output the video on the Hydra hardware, you also need to make the pins output.

'                       or      DIRA, tvport_mask       ' set DAC pins to output
' Video (TV) Registers
'tvport_mask            long                    %0000_0111<<24


' Now for the interesting part, how to generate the video line.
' In order to do that you need to set the Video Scale Register (VSCL) to shift out the bits at the correct speed.
' The VSCL requires two settings, Clocks per frame and Clocks per pixel.
' As an example you are going to let the VSU generate the Horizontal sync.
' The good news is that a sync can be represented as a single frame with 16 pixels in four color mode.
' Remember that a sync takes 10.9 us which gives us 10.9us/17.46ns => 624 clocks.
' The 17.46ns is the period of Counter A running at 57 MHz.
' This means that a frame should take 624 clocks which is the time it takes to plot 16 pixels.
' So the clocks per pixels will be 624/16 = 39.
' The VSCL register looks like this, 
' %xxxx_xxxx_xxxx_________________________ : Not used
' %_______________PPPP_PPPP_______________ : Clocks per pixel (CPP)
' %_________________________FFFF_FFFF_FFFF : Clocks per frame (CPF)

' Which can be represented by "39 << 12 + 624" or "$27_270" where $270 = 624 and $27 = 39.

' Now let's look at the sync signal.
'
' ───── Front porch, Synch tip, Breezeway, Colorburst and Back porch.

' In the 4 color palette you can represent black with $02, synch with $00 and colorburst with $8A.
' Look at table 14:2 on page 240, here you can see where these values comes from and the colorburst is actually the color "Yellow".
' So let's choose a palette of $00_00_02_8A and pixels of %%11_0000_1_2222222_11, where color 0 =$8A, 1 = $02 and 2 = $00.
' The pixels are shifted out to the right so it's revered compared to the sync signal.
' Or in other words, %%"Back porch"_"Colorburst"_"Breezeway"_"Synch tip"_"Front Porch."

' To generate a sync signal, all you need to do now is "waitvid $00_00_02_8a, %%11_0000_1_2222222_11".



' So you got the synch, what about the Active video of 52.6 us ?
'
' Here you need to sit down and think what you want to do.
' The two major questions are how many pixels do you want to squeeze in per line and how many colors.
' Keep in mind that in order to have clear defined pixels, try aim for 188 pixels as André explains on page 113.
' I want to overdrive the pixels to get a resolution of 320x200 which is the Commodore 64 resolution.
' The Commodore 64 setting I'm aiming for have 40x25 chars that are 8x8 pixels, each char have it's own background and color.

' The 52.6 us Active videoline have 52.6us/17.46ns => 3008 clocks. This was rounded to a multiple of 16 to make things easy for us.
' If I divide 3008 with 320 I get 9.4 clocks per pixel, round it down to 9.
' Now I know that my 320 pixels will use up 2880 clocks (9*320) and I have 3008-2880= 128 clocks to spare.
' This will be my "border" and I will divied them equally on each side of the pixels.
' I'm also going to need a top and bottom border so I want a border video line that is one color.

' To make a border line that is "blank" you could output 16 pixels in the same color over the entire line.
' So let's program the VSCL with clocks per frame to 3008 and clocks per pixels to 3008/16 = 188 or $BC_BC0.
' Then just use a "waitvid [border color], 0" for each blank row.

' The same goes for the left and right border, 128 clocks totals gives 64 clocks on each side.
' So let's program the VSCL with clocks per frame to 64 and clocks per pixels to 64/16 = 4 or $04_040.
' Then just use a "waitvid [border color], 0" on each side of the pixels.

' Now for our 320 pixels per line.
' Since I want two color mode, I need to set the VCFG to %0_10_011_000.
' Then set the VSCL to 9 clocks per pixels and pixels per frame to 9*8 = 72 or $09_048.
' All I need to do now is to output the byte data 40 times with a "waitvid [Char_color]_[Background_color], [Byte_data]".
' Since I programmed the pixels per frame to 8 times clocks per pixels, the waitvid will only output 8 pixels before waiting for more data.

' Now for the vertical blanking which is 18 sync lines...
' The first 6 lines are a sync signal and then high (black color level) for the rest of the line.
' Then 6 lines which are inverted and reversed, in other words, mostly sync level and then high for 4.7us at the end of the line.
' Finally 6 lines of sync signals and the rest high.

' If you want to re-use the sync settings for Horisontal sync, you can use the same setting for the VSCL and the palette.
' The pixels for generating the sync puls can be approximated with %%11111111111_222_11.
' So basically, set the VSCL to "$27_270" and then do a "waitvid $00_00_02_8a, %%11111111111_222_11".
' Since the rest of the line is high, you can use the same setting as the border line for the rest of the line.
' Set VSCL to "$BC_BC0" and then "waitvid $00_00_02_8a, %%1111111111111111".
' Repeat 6 times.
' Then for the "middle" 6 rows, let's do the same.  
' Set the VSCL to "$27_270" and then do a "waitvid $00_00_02_8a, %%22222222222222_11".
' Set VSCL to "$BC_BC0" and then "waitvid $00_00_02_8a, %%1_222222222222222".
' Repeat 6 times.
' Set the VSCL to "$27_270" and then do a "waitvid $00_00_02_8a, %%11111111111_222_11".
' Set VSCL to "$BC_BC0" and then "waitvid $00_00_02_8a, %%1111111111111111".
' Repeat 6 times.

' And then repeat for every frame.

' One last note about the actual video lines.
' There are 262 lines but 18 is used for Vertical sync leaving 244 for the user.
' This means that my border should use 22 lines on the top and 22 lines on the bottom.

' NTSC sync stuff, see above.
NTSC_color_freq                 long  3_579_545
NTSC_hsync_VSCL                 long  39 << 12 + 624
NTSC_active_VSCL                long  188 << 12 + 3008
NTSC_hsync_pixels               long  %%11_0000_1_2222222_11
NTSC_vsync_high_1               long  %%11111111111_222_11
NTSC_vsync_high_2               long  %%1111111111111111
NTSC_vsync_low_1                long  %%22222222222222_11
NTSC_vsync_low_2                long  %%1_222222222222222
NTSC_sync_signal_palette        long  $00_00_02_8A

' Note that I had to change a few values to fit my TV.
' I originally calculated 9 clocks per pixel, however this didn't work.
' The active video line was way to wide and I had no border.
' So I tried 8 clocks per pixel which worked.
' The Left and Right borders were then recalculated and adjusted to fit my TV and be centered.

NTSC_Left_Border_VSCL           long  (13 << 12) + 208  ' Changed from 64 clocks per frame to 208 
NTSC_Graphics_Pixels_VSCL       long  (8 << 12) + 64    ' Changed from 9 clocks per pixel to 8...
NTSC_Right_Border_VSCL          long  (15 << 12) + 240  ' Changed from 64 clocks per frame to 240

NTSC_Border_Color               long  $00_00_00_00      ' Set from the paramters 
NTSC_Border_Pixels              long  $00_00_00_00      ' All the same pixel
NTSC_Border_Color_ptr           long  0                 ' Pointer to border color
NTSC_Top_Border_Lines           long  27                ' Changed from 22 lines to 27
NTSC_Graphic_Lines              long  200               ' One of the few unchanged values...  ;-)
NTSC_Bottom_Border_Lines        long  17                ' Changed from 22 lines to 17

NTSC_Pixel_data                 long  0                 ' Initilized to zero
NTSC_User_Palette               long  $00_00_00_00      ' Set from colormap
NTSC_Tiles_Per_Line             long  40                ' Tiles per line.

VCFG_ColorMode                  long  |<28

' The bitmask for the Hydra Video output pins
tvport_mask                     long  %0000_0111<<24
tvport_mask_ptr                 long  0 ' Pointer used to get the actual value

' The pingroup to use.
VCFG_PinGroup                   long 3
VCFG_PinGroup_ptr               long 0 ' Pointer used to get the actual value

' The VCFG pingroup to use.
VCFG_PinMask                    long %0000_0111
VCFG_PinMask_ptr                long 0 ' Pointer used to get the actual value

' General Purpose Registers
r0                              long  $0        ' should typically equal 0
r1                              long  $0
r2                              long  $0
r3                              long  $0

' loop counters.
line_loop                       long  $0        ' Counter for each line, varies depending were we are on the screen
tile_loop                       long  $0        ' Tile counter, for 40 chars this is obiously 40.
font_line                       long  $0        ' Font line, syncs with the line loop to count 0-7 over and over.


' The char array
char_data                       long  $0        ' Points to the characters.
char_data_work                  long  $0        ' Points to the characters, work register.
current_char                    long  $0        ' Points to the current char.
color_data                      long  $0        ' Points to the colors.
color_data_work                 long  $0        ' Points to the colors, work register.

font_add                        long  $0        ' Font offset.        
font_shift                      long  $0        ' Font shift to get correct byte.



'***************************************************
'* The driver itself                               *
'***************************************************

initialization

                        ' Get parameters from parameter block, and pass them to COG code here

                        mov     r0, PAR                                         ' Get parameter block address
                        rdlong  char_data, r0                                   ' Get screen address

                        add     r0, #4                                          ' Get parameter block address
                        rdlong  color_data, r0                                  ' Get screen address

                        add     r0, #4                                          ' Get parameter block address
                        rdlong  NTSC_Border_Color_ptr, r0                       ' Get Border address

                        add     r0, #4                                          ' Get parameter block address
                        rdlong  VCFG_PinGroup_ptr, r0                           ' Get Pingroup address
                        rdlong  VCFG_PinGroup, VCFG_PinGroup_ptr                ' Get the pingroup.

                        add     r0, #4                                          ' Get parameter block address
                        rdlong  tvport_mask_ptr, r0                             ' Get PinMask address
                        rdlong  tvport_mask, tvport_mask_ptr                    ' Get PinMask

                        add     r0, #4                                          ' Get parameter block address
                        rdlong  VCFG_PinMask_ptr, r0                            ' Get VCFG PinMask address
                        rdlong  VCFG_PinMask, VCFG_PinMask_ptr                  ' Get VCFG PinMask


                        ' VCFG: setup Video Configuration register and 3-bit tv DAC pins to output
                        movs    VCFG, VCFG_PinMask                              ' VCFG'S = pinmask (pin31: 0000_0111 : pin24)
                        movd    VCFG, VCFG_PinGroup                             ' VCFG'D = pingroup (grp. 3 i.e. pins 24-31)
                        test    VCFG_PinMask,#1         wc
              if_c      movi    VCFG, #%0_10_101_000                            ' Baseband video on bottom nibble, 2-bit color, enable chroma on baseband
              if_nc     movi    VCFG, #%0_11_101_000                            ' Baseband video on top nibble, 2-bit color, enable chroma on baseband

                        or      DIRA, tvport_mask                               ' Set DAC pins to output

                        ' CTRA: setup Frequency to Drive Video
                        movi    CTRA,#%00001_110                                ' pll internal routed to Video, PHSx+=FRQx (mode 1) + pll(8x)
                        mov     r1, NTSC_color_freq                             ' r1: Color frequency in Hz (3.579_545MHz)
                        shl     r1, #1  ' * 2 so we can use pll8x and remain in-spec
                        rdlong  r2, #0                                          ' Copy system clock from main memory location 0. (80Mhz)
                        ' perform r3 = 2^32 * r1 / r2
                        mov     r0,#32+1
:loop                   cmpsub  r1,r2           wc
                        rcl     r3,#1
                        shl     r1,#1
                        djnz    r0,#:loop
                        mov     FRQA, r3                                        ' Set frequency for counter A


'-----------------------------------------------------------------------------
                        
frame_loop              ' Initilize font line to make it start at 0 on the top line.
                        mov     font_line, #0
                        mov     char_data_work, char_data                       ' Initilize work char pointer
                        mov     color_data_work, color_data                     ' Initilize work char pointer
                        rdbyte  NTSC_Border_Color, NTSC_Border_Color_ptr        ' Get background color
                        
                        mov     line_loop, NTSC_Top_Border_Lines                ' Load the line loop with the top rows, 26.
top_border              mov     VSCL, NTSC_hsync_VSCL                           ' Set VSCL for Horizontal sync.
                        waitvid NTSC_sync_signal_palette, NTSC_hsync_pixels     ' Generate Horizontal sync.
                        rdbyte  NTSC_Border_Color, NTSC_Border_Color_ptr        ' Get background color                         
                        mov     VSCL, NTSC_active_VSCL                          ' Set VSCL for rest of active line.
                        waitvid NTSC_Border_Color, NTSC_Border_Pixels           ' Draw 16 pixels as a border over the rest of the line.
                        djnz    line_loop, #top_border                          ' count down the top border.
'-----------------------------------------------------------------------------

                        mov     line_loop, NTSC_Graphic_Lines                   ' Load the line loop with user lines, 200

user_graphics_lines     mov     VSCL, NTSC_hsync_VSCL                           ' Generate sync.
                        waitvid NTSC_sync_signal_palette, NTSC_hsync_pixels
                        rdbyte  NTSC_Border_Color, NTSC_Border_Color_ptr        ' Get background color                        

                        mov     VSCL, NTSC_Left_Border_VSCL                     ' Set up VSCL for left border
                        waitvid NTSC_Border_Color, NTSC_Border_Pixels           ' Draw border pixels      

                        mov     VSCL, NTSC_Graphics_Pixels_VSCL                 ' Set up VSCL for user line, the 40 chars
                        andn    VCFG, VCFG_ColorMode                            ' Two color mode, same as 1 bit mode.                                                                                      
                        ' Note that when you switch from one color mode to another this should idealy be done just after the first waitvid for the new color mode.
                        ' Since the border is one color, it does not mater.

                        mov     tile_loop, NTSC_Tiles_Per_Line                  ' Set up the tile loop with 40 tiles.
                        ' Note this was taken from anohter 8x8 driver

                        ' Precalculate the font offset.
                        mov     font_add, font_line                             ' Get fontline.
                        and     font_add, #%100                                 ' Upper or lower char long?
                        shr     font_add, #2                                    ' Shift in correct offset.                                                                                                

                        ' Precalculate the byte shift.
                        mov     font_shift, font_line                           ' Get fontline.
                        and     font_shift, #%0011                              ' Mask off shift amounts to find correct byte in the char data. 
                        shl     font_shift, #3                                  ' Multiply by 8 since each byte is 8 bits.

user_tile_loop
                        rdbyte  current_char, char_data_work                    ' Get current char.
                        shl     current_char, #1                                ' Multiply by 2, for char offset                        
                        add     current_char, #Gridrunner_font                  ' Get address for the character to display
                        rdword  NTSC_User_Palette, color_data_work              ' Get current palette, two instructions in between HUB instructions not waste cycles.
                        add     current_char, font_add                          ' Add font offset.
                        movs    :pixel_pointer, current_char                    ' Modify the mov pixel instruction directly, set the Source field to the character address.
                        add     char_data_work, #1                              ' Wait for movs above to finish, in the mean time point to next char.
:pixel_pointer          mov     NTSC_Pixel_data, 0-0                            ' Get the correct pixel data.                 
                        shr     NTSC_Pixel_data, font_shift                     ' Shift desired pixels (byte) into position.
                        
                        waitvid NTSC_User_Palette, NTSC_Pixel_data              ' Draw the tile.
                        
                        add     color_data_work, #2                             ' Point to next palette
                        
                        djnz    tile_loop, #user_tile_loop                      ' loop throug the tiles.

                        mov     VSCL, NTSC_Right_Border_VSCL                    ' Set up right border.
                        ' Note that even if the border is 4 color mode, you cannot put "movi VCFG, #%0_10_101_000" here, it will mess up the last column of the user chars.
                        waitvid NTSC_Border_Color, NTSC_Border_Pixels           ' Draw right border.          
                        or      VCFG, VCFG_ColorMode                            ' Set back 4 color mode for the border.

                        add        font_line, #1                                ' Next font line.
                        and        font_line, #%0111     wz                     ' All eight lines of the char ?
                if_nz   sub        char_data_work, NTSC_Tiles_Per_Line          ' If we are not yet on the next line, subtract the number of tiles.                                     
                if_nz   sub        color_data_work, NTSC_Tiles_Per_Line         ' If we are not yet on the next line, subtract the number of colors.
                if_nz   sub        color_data_work, NTSC_Tiles_Per_Line         ' Do it twice since it's a word...   

                        djnz    line_loop, #user_graphics_lines                 ' Loop through the 200 user video lines.
'-----------------------------------------------------------------------------

                        ' Draw bottom border, see comment for top border.
                        mov     line_loop, NTSC_Bottom_Border_Lines
bottom_border           mov     VSCL, NTSC_hsync_VSCL
                        waitvid NTSC_sync_signal_palette, NTSC_hsync_pixels
                        rdbyte  NTSC_Border_Color, NTSC_Border_Color_ptr        ' Get background color                         
                        mov     VSCL, NTSC_active_VSCL
                        waitvid NTSC_Border_Color, NTSC_Border_Pixels
                        djnz    line_loop, #bottom_border
'-----------------------------------------------------------------------------


                        'The vertical sync, 3 sections of 6 lines each.
                        
                        mov     line_loop, #6                                   ' Line 244, start of first high sync.
vsync_higha             mov     VSCL, NTSC_hsync_VSCL
                        waitvid NTSC_sync_signal_palette, NTSC_vsync_high_1
                        mov     VSCL, NTSC_active_VSCL
                        waitvid NTSC_sync_signal_palette, NTSC_vsync_high_2
                        djnz    line_loop, #vsync_higha

                        mov     line_loop, #6                                   ' Line 250, start of the Seration pulses.
vsync_low               mov     VSCL, NTSC_hsync_VSCL
                        waitvid NTSC_sync_signal_palette, NTSC_vsync_low_1
                        mov     VSCL, NTSC_active_VSCL
                        waitvid NTSC_sync_signal_palette, NTSC_vsync_low_2
                        djnz    line_loop, #vsync_low

                        mov     line_loop, #6                                   ' Line 256, start of second high sync.
vsync_highb             mov     VSCL, NTSC_hsync_VSCL
                        waitvid NTSC_sync_signal_palette, NTSC_vsync_high_1
                        mov     VSCL, NTSC_active_VSCL
                        waitvid NTSC_sync_signal_palette, NTSC_vsync_high_2
                        djnz    line_loop, #vsync_highb

'-----------------------------------------------------------------------------

                        jmp     #frame_loop                                     ' And repeat for ever...


' These are not the exact colors, but a pretty good estimate.
Commodore_Palette       long  $02            ' 0 - Black
                        long  $07            ' 1 - White
                        long  $5A            ' 2 - Red
                        long  $DC            ' 3 - Cyan
                        long  $2B            ' 4 - Purple
                        long  $AB            ' 5 - Green
                        long  $0B            ' 6 - Blue
                        long  $8E            ' 7 - Yellow
                        long  $6C            ' 8 - Orange
                        long  $7A            ' 9 - Brown
                        long  $5D            ' A - Light red
                        long  $03            ' B - Dark grey
                        long  $04            ' C - Grey
                        long  $AE            ' D - Light green
                        long  $0D            ' E - Light blue
                        long  $05            ' F - Light grey


' The font from Gridrunner, got it from a memory dump from VICE... ;-)
Gridrunner_font
                        long %00011000_00011000_00011000_00011000
                        long %00011000_00011000_00011000_11111111
                        long %11111000_00001000_00000100_00001111
                        long %00001111_00000100_00001000_11111000
                        long %00011000_00011000_00011000_00011000
                        long %10000001_10000001_11000011_10111101
                        long %11000101_00000110_00000100_00000000
                        long %00000000_00000000_00001100_00110100
                        long %00010011_10100000_01000000_00000000
                        long %00000000_00000000_00000000_00001100
                        long %00000100_01111100_00100000_00010000
                        long %00010000_00010000_00001000_00001000
                        long %00001000_00001000_00010000_00010000
                        long %00100000_01000000_00100000_00010000
                        long %00011000_01100110_00111100_00011000
                        long %11000011_11100111_11111111_01111110
                        long %00111100_00011000_00011000_00011000
                        long %00111100_01111110_01111110_01111110
                        long %01111110_01111110_01111110_00111100
                        long %00011000_00011000_00011000_00111100
                        long %00111100_00100100_01100110_01000010
                        long %00011000_00111100_00011000_00011000
                        long %11111000_01001110_00000011_00000000
                        long %00000000_00000011_01001110_11111000
                        long %00011111_01110010_11000000_00000000
                        long %00000000_11000000_01110010_00011111
                        long %00011000_00000000_00000000_00000000
                        long %00000000_00000000_00000000_00011000
                        long %00111100_00011000_00000000_00000000
                        long %00000000_00000000_00011000_00111100
                        long %10111101_01011010_00100100_00011000
                        long %00011000_00100100_01011010_10111101
                        long %11111111_01011010_01111110_10011001
                        long %10011001_01111110_01011010_11111111
                        long %01011010_10111101_10011001_01100110
                        long %01100110_10011001_10111101_01011010
                        long %00000000_10100101_01000010_00100100
                        long %00100100_01000010_10100101_00000000
                        long %11111111_00010010_01100010_00001100
                        long %00110000_01000110_01001000_11111111
                        long %00011111_01001110_00111111_00000011
                        long %11000000_11111100_01110010_11111000
                        long %01111010_01110010_11110100_11010000
                        long %00001011_00101111_01001110_01011110
                        long %00000001_00010100_01100010_00000000
                        long %01000010_00000100_00010000_01101000
                        long %00000000_01100000_10000100_00000010
                        long %10000000_01000001_00000110_00000000
                        long %00000000_00000000_00000001_01000000
                        long %00000010_10000000_00000000_00000000
                        long %11001111_11011011_11001111_00000000
                        long %00000000_00000000_11000011_11000011
                        long %11000000_11000000_11000000_00000000
                        long %00000000_00000000_11011011_11000000
                        long %00111110_00000000_01100011_01100011
                        long %00011100_00011100_00011100_00011100
                        long %00111110_00000000_01100011_01100011
                        long %00011100_00011100_00011100_00011100
                        long %00111111_00110011_00110011_00000000
                        long %00000000_00000000_00110011_00110011
                        long %00000110_01100110_00001111_00000000
                        long %00000000_00000000_00001111_01100110
                        long %00111110_00010000_00001000_00000000
                        long %00000000_00000000_00001000_00010000
                        long %00000000_00000000_00000000_00000000
                        long %00000000_00000000_00000000_00000000
                        long %00000011_00000000_01111111_00111110
                        long %00111110_01100011_01100011_01111011
                        long %01100011_00000000_01111111_00111111
                        long %01100011_00010011_00001011_00111111
                        long %01100011_00000000_01100011_01100011
                        long %00111110_01111111_01100011_01100011
                        long %00001100_00000000_00111111_00111111
                        long %00111111_00111111_00001100_00001100
                        long %01100011_00000000_01111111_00111111
                        long %00111111_01111111_01100011_01100011
                        long %01100111_00000000_01100011_01100011
                        long %01100011_01110011_01111011_01101111
                        long %00001111_00000000_01111111_01111111
                        long %01111111_01111111_00000011_00001111
                        long %01100011_00000000_01111111_01111111
                        long %01100011_01100011_01100011_01111111
                        long %01100011_00000000_01111111_00111111
                        long %00111111_01100011_01100011_00111111
                        long %00001111_00000000_01111111_01111111
                        long %00000011_00000011_00000011_00001111
                        long %01100011_00000000_01100011_01100011
                        long %01100011_01100011_01100011_01111111
                        long %01100000_00000000_01100000_01100000
                        long %00111110_01111111_01100011_01100000
                        long %01111111_00000000_01110111_01100011
                        long %01100011_01100011_01100011_01100011
                        long %01100011_00000000_01111111_01111111
                        long %00000011_00000011_00000011_01111111
                        long %00000011_00000000_01111111_01111111
                        long %01111111_01100011_01100000_01111111
                        long %01100011_00000000_01111111_01111111
                        long %01111111_01111111_01100011_01100011
                        long %00011000_00000000_00011000_00011000
                        long %00011100_00011100_00011100_00011100
                        long %01100000_00000000_01111111_00111110
                        long %01111111_01111111_00011100_00110000
                        long %01111000_00000000_01111111_00111110
                        long %00111110_01111111_01100000_01111000
                        long %00110111_00000000_00000110_00000110
                        long %00110000_00110000_00110000_01111111
                        long %00000111_00000000_01111111_01111111
                        long %00111110_01111111_01100000_00111110
                        long %00000011_00000000_01111111_00111110
                        long %00111110_01100011_01100011_00111111
                        long %00110000_00000000_01111111_01111111
                        long %00001110_00001100_00001100_00011000
                        long %01100011_00000000_01111111_00111110
                        long %00111110_01100011_01100011_00111110
                        long %01100011_00000000_01111111_00111110
                        long %00111110_01111111_01100000_01111110
                        long %00001100_00000000_00111111_00111111
                        long %00001100_00001100_00001100_00001100
                        long %01100011_00000000_01100011_01100011
                        long %00011100_00011100_00110110_00110110
                        long %00110011_11100110_00001100_11111000
                        long %11111000_00001100_11100110_00110011
                        long %00001100_00000110_00000011_00000001
                        long %00000001_00000011_00000110_00001100
                        long %00000011_00000000_00000011_00000011
                        long %01111111_01111111_00000011_00000011
                        long %00011000_00011000_00011000_00011000
                        long %00011000_00011000_00011000_00011000
                         

' These are the ordinary chars of the Commodore font...

                        long  %01110110_01110110_01100110_00111100
                        long  %00000000_00111100_01000110_00000110
                        long  %01111110_01100110_00111100_00011000
                        long  %00000000_01100110_01100110_01100110
                        long  %00111110_01100110_01100110_00111110
                        long  %00000000_00111110_01100110_01100110
                        long  %00000110_00000110_01100110_00111100
                        long  %00000000_00111100_01100110_00000110
                        long  %01100110_01100110_00110110_00011110
                        long  %00000000_00011110_00110110_01100110
                        long  %00011110_00000110_00000110_01111110
                        long  %00000000_01111110_00000110_00000110
                        long  %00011110_00000110_00000110_01111110
                        long  %00000000_00000110_00000110_00000110
                        long  %01110110_00000110_01100110_00111100
                        long  %00000000_00111100_01100110_01100110
                        long  %01111110_01100110_01100110_01100110
                        long  %00000000_01100110_01100110_01100110
                        long  %00011000_00011000_00011000_00111100
                        long  %00000000_00111100_00011000_00011000
                        long  %00110000_00110000_00110000_01111000
                        long  %00000000_00011100_00110110_00110000
                        long  %00001110_00011110_00110110_01100110
                        long  %00000000_01100110_00110110_00011110
                        long  %00000110_00000110_00000110_00000110
                        long  %00000000_01111110_00000110_00000110
                        long  %11010110_11111110_11101110_11000110
                        long  %00000000_11000110_11000110_11000110
                        long  %01111110_01111110_01101110_01100110
                        long  %00000000_01100110_01100110_01110110
                        long  %01100110_01100110_01100110_00111100
                        long  %00000000_00111100_01100110_01100110
                        long  %00111110_01100110_01100110_00111110
                        long  %00000000_00000110_00000110_00000110
                        long  %01100110_01100110_01100110_00111100
                        long  %00000000_01110000_00111100_01100110
                        long  %00111110_01100110_01100110_00111110
                        long  %00000000_01100110_00110110_00011110
                        long  %00111100_00000110_01100110_00111100
                        long  %00000000_00111100_01100110_01100000
                        long  %00011000_00011000_00011000_01111110
                        long  %00000000_00011000_00011000_00011000
                        long  %01100110_01100110_01100110_01100110
                        long  %00000000_00111100_01100110_01100110
                        long  %01100110_01100110_01100110_01100110
                        long  %00000000_00011000_00111100_01100110
                        long  %11010110_11000110_11000110_11000110
                        long  %00000000_11000110_11101110_11111110
                        long  %00011000_00111100_01100110_01100110
                        long  %00000000_01100110_01100110_00111100
                        long  %00111100_01100110_01100110_01100110
                        long  %00000000_00011000_00011000_00011000
                        long  %00011000_00110000_01100000_01111110
                        long  %00000000_01111110_00000110_00001100
                        long  %00001100_00001100_00001100_00111100
                        long  %00000000_00111100_00001100_00001100
                        long  %00111110_00001100_01001000_00110000
                        long  %00000000_00111111_01000110_00001100
                        long  %00110000_00110000_00110000_00111100
                        long  %00000000_00111100_00110000_00110000
                        long  %01111110_00111100_00011000_00000000
                        long  %00011000_00011000_00011000_00011000
                        long  %11111110_00001100_00001000_00000000
                        long  %00000000_00001000_00001100_11111110
                        long  %00000000_00000000_00000000_00000000
                        long  %00000000_00000000_00000000_00000000
                        long  %00011000_00011000_00011000_00011000
                        long  %00000000_00011000_00000000_00000000
                        long  %00000000_01100110_01100110_01100110
                        long  %00000000_00000000_00000000_00000000
                        long  %01100110_11111111_01100110_01100110
                        long  %00000000_01100110_01100110_11111111
                        long  %00111100_00000110_01111100_00011000
                        long  %00000000_00011000_00111110_01100000
                        long  %00011000_00110000_01100110_01000110
                        long  %00000000_01100010_01100110_00001100
                        long  %00011100_00111100_01100110_00111100
                        long  %00000000_11111100_01100110_11100110
                        long  %00000000_00011000_00110000_01100000
                        long  %00000000_00000000_00000000_00000000
                        long  %00001100_00001100_00011000_00110000
                        long  %00000000_00110000_00011000_00001100
                        long  %00110000_00110000_00011000_00001100
                        long  %00000000_00001100_00011000_00110000
                        long  %11111111_00111100_01100110_00000000
                        long  %00000000_00000000_01100110_00111100
                        long  %01111110_00011000_00011000_00000000
                        long  %00000000_00000000_00011000_00011000
                        long  %00000000_00000000_00000000_00000000
                        long  %00001100_00011000_00011000_00000000
                        long  %01111110_00000000_00000000_00000000
                        long  %00000000_00000000_00000000_00000000
                        long  %00000000_00000000_00000000_00000000
                        long  %00000000_00011000_00011000_00000000
                        long  %00110000_01100000_11000000_00000000
                        long  %00000000_00000110_00001100_00011000
                        long  %01101110_01110110_01100110_00111100
                        long  %00000000_00111100_01100110_01100110
                        long  %00011000_00011100_00011000_00011000
                        long  %00000000_01111110_00011000_00011000
                        long  %00110000_01100000_01100110_00111100
                        long  %00000000_01111110_00000110_00001100
                        long  %00111000_01100000_01100110_00111100
                        long  %00000000_00111100_01100110_01100000
                        long  %01100110_01111000_01110000_01100000
                        long  %00000000_01100000_01100000_11111110
                        long  %01100000_00111110_00000110_01111110
                        long  %00000000_00111100_01100110_01100000
                        long  %00111110_00000110_01100110_00111100
                        long  %00000000_00111100_01100110_01100110
                        long  %00011000_00110000_01100110_01111110
                        long  %00000000_00011000_00011000_00011000
                        long  %00111100_01100110_01100110_00111100
                        long  %00000000_00111100_01100110_01100110
                        long  %01111100_01100110_01100110_00111100
                        long  %00000000_00111100_01100110_01100000
                        long  %00000000_00011000_00000000_00000000
                        long  %00000000_00000000_00011000_00000000
                        long  %00000000_00011000_00000000_00000000
                        long  %00001100_00011000_00011000_00000000
                        long  %00000110_00001100_00011000_01110000
                        long  %00000000_01110000_00011000_00001100
                        long  %00000000_01111110_00000000_00000000
                        long  %00000000_00000000_00000000_01111110
                        long  %01100000_00110000_00011000_00001110
                        long  %00000000_00001110_00011000_00110000
                        long  %00110000_01100000_01100110_00111100
                        long  %00000000_00011000_00000000_00011000
                         
                        fit 496
                                                   