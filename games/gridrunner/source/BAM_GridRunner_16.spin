'*********************************************************************
'* The Gridrunner game.                                              *
'*                                                                   *
'* Try to recreate the good old Gridrunner by Jeff Minter...         *
'*                                                                   *
'*********************************************************************

CON
 _clkmode = xtal1 + pll16x
 _xinfreq = 5_000_000

  ' What kind of system are we dealing with.
  TYPE_HYDRA  = 1
  TYPE_HYBRID = 2
  TYPE_DEMO   = 0

  ' Keyboard control.
  KBD_FIRE = $20                ' Fire with space key. 
  KBD_UP = $C2                  
  KBD_DOWN = $C3                                   
  KBD_LEFT = $C0                                   
  KBD_RIGHT = $C1          
  KBD_PAUSE = $CB

  ' Change this to 1_600_000 for faster or 2_000_00 for slower...
  GAME_SPEED = 1_800_000

  JOY_CLK = 16
  JOY_LCH = 17
  JOY_DATAOUT0 = 19
  JOY_DATAOUT1 = 18

  ' NES bit encodings for NES gamepad 0
  NES0_RIGHT    = %00000000_00000001
  NES0_LEFT     = %00000000_00000010
  NES0_DOWN     = %00000000_00000100
  NES0_UP       = %00000000_00001000
  NES0_START    = %00000000_00010000
  NES0_SELECT   = %00000000_00100000
  NES0_B        = %00000000_01000000
  NES0_A        = %00000000_10000000

  ' NES bit encodings for NES gamepad 1
  NES1_RIGHT    = %00000001_00000000
  NES1_LEFT     = %00000010_00000000
  NES1_DOWN     = %00000100_00000000
  NES1_UP       = %00001000_00000000
  NES1_START    = %00010000_00000000
  NES1_SELECT   = %00100000_00000000
  NES1_B        = %01000000_00000000
  NES1_A        = %10000000_00000000


  ' Modes for the Finite State Machine...
  MODE_INIT       = 1           ' Init game.     
  MODE_MENU       = 2           ' Display meny                                        
  MODE_START      = 3           ' Start screen     
  MODE_IN_GAME    = 4           ' Play the game       
  MODE_NEXT_LEVEL = 5           ' Next Level
  MODE_LOST_LIFE  = 7           ' Loos a life                                                  
  MODE_GAME_OVER  = 8           ' Game Over
  MODE_EXIT       = 9           ' Exit Game, probably not needed...     

  ' Events in game       
  GRID_MISSILE = %00000000_00000001   ' Gridrunner fired
  GRID_SIDE    = %00000000_00000010   ' Side swiper fired


  CHANNELS_AVAILABLE = 6        ' Number of avaialble sound channels.
  SOUND_GRID = 129              ' Sound effect for drawing grid, 1 + 128                              
  SOUND_SWIPE = 130             ' Sound effect for Side Swiper, 2 + 128                              
  SOUND_MISSILE = 131           ' Sound effect for Missile, 3 + 128                              
  SOUND_SHIP_EXPLODE = 132      ' Sound effect for exploding ship, 4 + 128
  SOUND_LEVEL_START = 133       ' Sound effect for "ENTERING GRID AREA", 5 + 128

  ' Channel CHANNELS_AVAILABLE reserved for SOUND_LEVEL_START  
  ' Channel CHANNELS_AVAILABLE - 1 reserved for SOUND_GRID  
  ' Channel CHANNELS_AVAILABLE - 2 reserved for SOUND_SHIP_EXPLODE  
  ' Channel CHANNELS_AVAILABLE - 3 reserved for SOUND_SWIPE  
                                

VAR

  byte  debug                   ' Skip long operations...

  byte  displayb[1000]          ' Allocate character buffer, 40x25 bytes
  word  colorb[1000]            ' Allocate color buffer, 40x25 words
  byte  backgroundCol           ' Background color
  long  pingroup                ' Pingroup
  long  pinmaskIO               ' mask for IO pins
  long  pinmaskVCFG             ' mask for VCFG pins
  long  soundpin                ' sound pin
  long  keyboardpin             ' keyboard pin

  byte  col                     ' Column used for print                
  byte  row                     ' Row used for Print 

  byte  fsm_mode                ' Finite State Machine mode.

  byte  ship_x                  ' Space ship coordinates
  byte  ship_y
  byte  ship_x_old              ' Old coordinates
  byte  ship_y_old

  byte  side_swiper_x           ' The swipers on the side of the grid 
  byte  side_swiper_y
  byte  side_swiper_x_fire      ' The positions of Side Swipe fire 
  byte  side_swiper_y_fire
  word  side_swiper_next_fire   ' Countdown until the next sideSwiper fires.
  byte  side_swiper_x_laser     ' The positions of bottom Side Swipe fire

  byte  missile_x               ' Missile fired
  byte  missile_y

  long score                    ' Current score
  long hiscore                  ' The High score
  byte shipsLeft                ' Ships left
  byte level                    ' Current Level

  word  Nes_Pad                 ' Result from reading the NES pad 

  word  Grid_Event              ' Events such as missile, sidswiper etc...


  ' Convert integer to Ascii
  byte sbuffer[10]


  long  params[6]               'The parameters for the Video driver. 
        '[0] screen memory                            passed to the TV driver.
        '[1] color memory                             passed to the TV driver.
        '[2] background color                         passed to the TV driver.
        '[3] pingroup                                 passed to the TV driver.
        '[4] pins for video                           passed to the TV driver.
        '[5] pinmask for video                        passed to the TV driver.


        ' The centipede is divided into 45 bytes,
        ' Byte 0, direction X 1, 0 or -1, if direction = 0, the centipede is dead.
        ' Byte 1, direction Y 1 or -1
        ' Byte 2, length 1-20
        ' Byte 3, Reserved
        ' Byte 4, Reserved
        ' Byte 5 to 44 position on screen, x and y coordinates * lenght...
        ' We are going to reserve 20 centipededs...
        ' Each Centipede can be 1 to 19 long...
  byte  centipedes[20 * 45]
        ' How many Centipedes are alive ?
  byte  activeCentipedes

        ' The bomb created by the side swipers or hitting the centipede.
        ' byte 0, x or 0 if not active.
        ' byte 1, y
        ' byte 2, curren state
        ' byte 3, countdown to next stage.  
  byte  bombs[100*4]

  long  cogon                                           ' Is the sound cog active ?
  long  cog                                             ' If so, which is the actual cog number 
  long  my_stack[500]                                   ' Reserve a stack for the sound cog.


  byte sound_playing[CHANNELS_AVAILABLE]                ' The sound playing on the channel, 0 

  long sound1_step1
  long sound1_step2

  long sound2_step1
  long sound2_step2

  long sound3_step1
  long sound3_step2

  long sound5_step1
  long sound5_step2  

  
OBJ
  Graphic : "BAM_GridRunner_Graphic_DRV_06"         ' The video driver
    snd   : "NS_sound_drv_052_22khz_16bit_stereo"   ' Sound driver
    kbd   : "Keyboard"                              ' Keyboard driver 
  
  
PUB start | temp{, type}

    ' Detect hardware and set clock frequency
    {type := HWDetect
    
    CASE type
      TYPE_HYDRA :
        pingroup    := 3               ' Set Pingroup used by TV driver.
        pinmaskIO   := %0000_0111<<24  ' Set Pinmaks for output pins.
        pinmaskVCFG := %0000_0111      ' Set Pinmaks for VCFG pins.
        soundpin    := 7               ' sound pin
        keyboardpin := 13              ' keyboard pin
      TYPE_HYBRID :
        pingroup    := 3               ' Set Pingroup used by TV driver.
        pinmaskIO   := %0000_0111<<24  ' Set Pinmaks for output pins.
        pinmaskVCFG := %0000_0111      ' Set Pinmaks for VCFG pins.
        soundpin    := 7               ' sound pin
        keyboardpin := 12              ' keyboard pin
      TYPE_DEMO :
        pingroup    := 1               ' Set Pingroup used by TV driver.
        pinmaskIO   := %0000_0111<<12  ' Set Pinmaks for output pins.
        pinmaskVCFG := %0111_0000      ' Set Pinmaks for VCFG pins.
        soundpin    := 10              ' sound pin
        keyboardpin := 26              ' keyboard pin
     }

  ' For Ventilator
  pingroup    := 1               ' Set Pingroup used by TV driver.
  pinmaskIO   := %0000_0111<<12  ' Set Pinmaks for output pins.
  pinmaskVCFG := %0111_0000      ' Set Pinmaks for VCFG pins.
  soundpin    := 10              ' sound pin
  keyboardpin := 8               ' keyboard pin


    'Debug or not ?
    debug := 0

    'Start FSM
    fsm_mode := MODE_INIT

    ' The meaning of life
    hiscore := 42
    score := 0
    shipsLeft := 0
    activeCentipedes := 0
    level := 1
    
    'Clear screen
    repeat temp from 0 to 999
      byte [@displayb] [temp] := 32

    ' Clear colors
    repeat temp from 0 to 999
      word [@colorb] [temp] := $0D_0B

    'start the tv cog & pass it the parameter block
    params[0] := @displayb
    params[1] := @colorb
    params[2] := @backgroundCol
    params[3] := @pingroup
    params[4] := @pinmaskIO
    params[5] := @pinmaskVCFG
    Graphic.start(@params)

  'start sound driver
  snd.start(soundpin)

  'start the keyboard driver
  kbd.startx(keyboardpin,keyboardpin+1, %0_000_100, 0)

    ' Reset the sound effects, note that sound effects are numbered 1-3 and to init a sound set the high bit (add 128 to sound effect) 
    repeat temp from 0 to CHANNELS_AVAILABLE
      sound_playing[temp] := 0              

    ' Start the sound cog.
    cogon := (cog := cognew(GridRunner_Sound, @my_stack)) > 0


' ********************************** Commodore 64 Intro ************************************

    if (debug == 0)

      ' C64 Blue 
      backgroundCol := $0D
    
      ' Print C64 boot screen    
      PrintC64(0,1,String("    ***** PARALLAX P1 SPIN V1 *****"))
      PrintC64(0,3,String(" 32K RAM SYSTEM "))
      PrintC64(16,3,DecStr(@result,5))
      PrintC64(21,3,String(" STACK BYTES FREE"))
      PrintC64(0,5,String("READY."))
       
       
      ' Blink cursor
      repeat temp from 0 to 3
        CursorC64(240)
       
      ' LOAD"GRIDRUNNER",8,1
      PrintC64Slow(0,6,@LOAD_STR1)
      ' LOADING
      PrintC64(0,7,@LOAD_STR2)
       
      ' Loading by flashing border...
      repeat temp from 0 to 10000
        waitcnt(34567 + cnt)
        backgroundCol := byte[@Commodore_Palette] [temp // 16]
      backgroundCol := $0D
       
      'READY.
      PrintC64(0,8,@LOAD_STR3)
      ' RUN
      PrintC64Slow(0,9,@LOAD_STR4)
      waitcnt(40_000_000 + cnt)

' ********************************** Commodore 64 Intro ************************************



'****************** Start of Finite State Machine, the main game loop **********************
       
    'Start the Finite State Machine. 
    repeat 

      'Speed setting
      waitcnt(cnt + GAME_SPEED)
      case fsm_mode
        '************************************************* MODE_INIT ******************************
        MODE_INIT: ' Initializes the game overall
        '
          fsm_mode := MODE_MENU
          ClearScreen
          PrintTopRow
          PrintMenu

        '************************************************* MODE_MENU ******************************           
        MODE_MENU: ' Dispaly menu and prepare for game.


           Nes_Pad := NES_Read_Gamepad
           ' Added hack for keyboard
           if kbd.key == KBD_FIRE
             Nes_Pad := Nes_Pad | NES0_START
            
            
           ' Start the game.  
           if (Nes_Pad & NES0_START) or (Nes_Pad & NES0_B) 
             fsm_mode := MODE_START
             ClearScreen
             PrintStart
             score := 0    
             shipsLeft := 4          
             PrintTopRow
             level := 1         
                                       
        '************************************************* MODE_START ******************************           
        MODE_START: ' Dispaly "Get ready..."

          ship_x :=20
          ship_Y :=23
          ship_x_old :=21
          ship_y_old :=23
          Grid_Event := 0   
          InitCentipedes
          InitBombs
           
          side_swiper_x := 1
          side_swiper_y := 2
          side_swiper_next_fire := 50

          
          if (debug == 0)
            
            ' Play this sound effect on a specific channel.
             sound_playing[CHANNELS_AVAILABLE] := SOUND_LEVEL_START                   
                                 
            repeat temp from 0 to 10000                                               
             byte[@colorb] [61+7*80] := byte[@Commodore_Palette] [(temp // 14) + 2]   
             byte[@colorb] [63+7*80] := byte[@Commodore_Palette] [(temp // 14) + 2]   
             waitcnt(cnt + 10_000)                                                    
          
          ' Enter game 
          fsm_mode := MODE_IN_GAME
          DrawGrid
          DrawShipEntering
          activeCentipedes := 0
          kbd.clearkeys

          ' Create Centipedes
          if level > 19
           ' Level 20 and higher are define as two centipedes 15 long 
            CreateCentipede(1, 1, 15, 20, 2)
            CreateCentipede(2, 1, 15, 15, 2)
          else
            'Get lenght of right centipede
             temp := byte[@Centipede1_Level] [level -1]
             if temp > 0
              CreateCentipede(1, 1, temp, 20, 2)
            'Get lenght of left centipede
             temp := byte[@Centipede2_Level] [level -1]
             if temp > 0
              CreateCentipede(2, 1, temp, 15, 2)
           
        '************************************************* MODE_IN_GAME ******************************           
        MODE_IN_GAME: ' In game

          'Count down side Swipe Fire
           side_swiper_next_fire := side_swiper_next_fire - 1
           ' Fire the side Swiper.
           if (side_swiper_next_fire == 0)
              ' Play side swiper sound
               sound_playing[CHANNELS_AVAILABLE - 3] := SOUND_SWIPE                 
           
            side_swiper_next_fire := 50
            Grid_Event := Grid_Event | GRID_SIDE
            side_swiper_x_fire := 1
            side_swiper_y_fire := side_swiper_y
            side_swiper_x_laser := side_swiper_x

           'Read pad       
           Nes_Pad := NES_Read_Gamepad


           'Did we go right
           if (Nes_Pad & NES0_RIGHT )
            if (ship_x < 38)
              ship_x := ship_x + 1
                      
           'Did we go left
           if (Nes_Pad & NES0_LEFT)
            if (ship_x > 1)
              ship_x := ship_x - 1
                      
           'Did we go up
           if (Nes_Pad & NES0_UP)
            if (ship_y > 16)
              ship_y := ship_y - 1

           'Did we go down              
           if (Nes_Pad & NES0_DOWN)
            if (ship_y < 23)
              ship_y := ship_y + 1

           ' Update game
           DrawShip
           UpdateSideSwipers
           UpdateSideSwiperLaser
           UpdateBombs
           DrawCentipedes
           DrawMissile
           PrintScore

           'Did we exit ?
           if (Nes_Pad & NES0_SELECT)
             fsm_mode := MODE_MENU
             ClearScreen
             PrintTopRow
             PrintMenu

           ' Fire missile
           if (Nes_Pad & NES0_B)
             if ((Grid_Event & GRID_MISSILE) == 0)
             ' Find a free a sound channel and play sound effect for missile.
               repeat temp from 0 to CHANNELS_AVAILABLE - 4 ' Reserve four channels...                      
                if( sound_playing[temp] == 0)               
                 sound_playing[temp] := SOUND_MISSILE            
               Grid_Event := Grid_Event | GRID_MISSILE
               missile_x := ship_x 
               missile_y := ship_y-1

           ' Did we got hit ?    
           if (displayb[ship_x+ship_y*40] <> 7)
            fsm_mode := MODE_LOST_LIFE

           if (activeCentipedes == 0)
             fsm_mode := MODE_NEXT_LEVEL

           ' Pause
           if (Nes_Pad & NES0_START)
             repeat while (Nes_Pad & NES0_START)
                waitcnt(cnt + GAME_SPEED)
                Nes_Pad := NES_Read_Gamepad
             repeat until (Nes_Pad & NES0_START)
                waitcnt(cnt + GAME_SPEED)
                Nes_Pad := NES_Read_Gamepad
             repeat while (Nes_Pad & NES0_START)
                waitcnt(cnt + GAME_SPEED)
                Nes_Pad := NES_Read_Gamepad

          
        '************************************************* MODE_LOST_LIFE ******************************           
        MODE_NEXT_LEVEL: ' Next Level
          level++                
          ClearScreen
          PrintTopRow
          PrintMenu
          fsm_mode := MODE_START
          ClearScreen
          PrintStart
          PrintTopRow




        '************************************************* MODE_LOST_LIFE ******************************           
        MODE_LOST_LIFE: ' Lost a life
          DrawExplodingShip
          ClearScreen
          PrintTopRow
          PrintMenu
          shipsLeft := shipsLeft - 1
          if (shipsLeft == 0)
            fsm_mode := MODE_MENU
             if (score > hiscore)
              hiscore := score  
             ClearScreen
             PrintTopRow
             PrintMenu
          else
            fsm_mode := MODE_START
             ClearScreen
             PrintStart
             PrintTopRow             


'****************** End of Finite State Machine, the main game loop **********************

PUB UpdateSideSwipers

    ' Clear old horizontal swiper
    colorb  [side_swiper_x+24*40] := $02_02
    side_swiper_x := side_swiper_x + 1
    ' Did we wrap around ?
    if (side_swiper_x > 38)
      side_swiper_x := 1
    ' Draw swiper  
    displayb[side_swiper_x+24*40] := 2
    colorb  [side_swiper_x+24*40] := $06_02

    ' Clear old vertical swiper
    colorb  [side_swiper_y*40] := $02_02
    side_swiper_y := side_swiper_y + 1
    ' Did we wrap around ?
    if (side_swiper_y > 23)
      side_swiper_y := 2
    ' Draw swiper  
    displayb[side_swiper_y*40] := 1
    colorb  [side_swiper_y*40] := $06_02

PUB UpdateSideSwiperLaser | temp

    'Did the SideSwiper fire ?
    if (Grid_Event & GRID_SIDE)
       'Did the side missile hit the vertical beam ?
       if (side_swiper_x_fire == side_swiper_x_laser)
        'Clear beam
        repeat temp from 2 to 23
          displayb[side_swiper_x_laser + temp*40] := 0
          colorb [side_swiper_x_laser + temp*40] := $7B_02
        ' Plant new bomb and quit Grid event.
        NewBomb(side_swiper_x_fire, side_swiper_y_fire)
        Grid_Event := Grid_Event & !GRID_SIDE
        side_swiper_x_laser :=0
      
       'Check to see if we still want to draw beam
       if (side_swiper_x_laser > 0)
          ' Draw beam
          repeat temp from 2 to 23

            if (displayb[side_swiper_x_laser + temp*40] == 19)
             SplitCentipede(side_swiper_x_laser, temp)
            if (displayb[side_swiper_x_laser + temp*40] == 20)
             SplitCentipede(side_swiper_x_laser, temp)
            if (displayb[side_swiper_x_laser + temp*40] == 21)
             SplitCentipede(side_swiper_x_laser, temp)
          
            displayb[side_swiper_x_laser + temp*40] := 5 + (side_swiper_x_fire // 2)
            colorb [side_swiper_x_laser + temp*40] := $07_02
        'Update side missile
        displayb[side_swiper_x_fire + side_swiper_y_fire*40] := 0
        colorb [side_swiper_x_fire + side_swiper_y_fire*40] := $7B_02
        side_swiper_x_fire := side_swiper_x_fire + 1
        displayb[side_swiper_x_fire + side_swiper_y_fire*40] := 3 + (side_swiper_x_fire // 2)
        colorb [side_swiper_x_fire + side_swiper_y_fire*40] := $07_02


PUB InitBombs | bombsCounter

  'Mark Bobms inactive
  repeat bombsCounter from 0 to 99
   'Set Bomb inactive
   bombs[4*bombsCounter] := 0

PUB NewBomb(bombX, bombY) | bombOffset, bombsCounter

  'Start at first bomb and loop through them
  bombsCounter := 0
  repeat while bombsCounter < 99
    bombOffset := 4*bombsCounter 
   'Is Bomb inactive
    if (bombs[bombOffset] == 0)
      'Create new bomb at coordinates.
      bombs[bombOffset + 0] := bombX
      bombs[bombOffset + 1] := bombY
      bombs[bombOffset + 2] := 15
      bombs[bombOffset + 3] := 20
      'Show bomb on screen
      displayb[bombs[bombOffset + 0] + bombs[bombOffset + 1]*40] := bombs[bombOffset + 2]
      colorb  [bombs[bombOffset + 0] + bombs[bombOffset + 1]*40] := $8E_02
      ' Exit loop      
      bombsCounter := 100

    'See if next bomb is active ?  
    bombsCounter := bombsCounter + 1

PUB ClearBomb(bombX, bombY) | bombOffset, bombsCounter

  'Start at first bomb and loop through them
  bombsCounter := 0
  repeat while bombsCounter < 99
    bombOffset := 4*bombsCounter 
   'Is Bomb active
    if (bombs[bombOffset] <> 0)
    
      'Is this the right bomb ?
      if (bombs[bombOffset + 0] == bombX and bombs[bombOffset + 1] == bombY)
        'Clear and exit loop
        bombs[bombOffset + 0] := 0       
        bombsCounter := 100   

    'See if next bomb is active ?  
    bombsCounter := bombsCounter + 1
       

PUB UpdateBombs | bombsCounter, temp
{{
Make the bombs planted by the side Swipers grow and then later fall down.
 }}
  repeat bombsCounter from 0 to 99
   'Is Bomb active ?
    if (bombs[4*bombsCounter] <> 0)
      'Countdown
      bombs[4*bombsCounter + 3] := bombs[4*bombsCounter + 3] - 1
      if (bombs[4*bombsCounter + 3] == 0)

      ''Bomb is dropping
       if (bombs[4*bombsCounter + 2] == 10)
        'Clear old bomb
        displayb[bombs[4*bombsCounter + 0] + bombs[4*bombsCounter + 1]*40] := 0
        colorb [bombs[4*bombsCounter + 0] + bombs[4*bombsCounter + 1]*40] := $7B_02
        'Stop if we are at bottom
        if (bombs[4*bombsCounter + 1] == 23)
          bombs[4*bombsCounter] := 0
        else
          'Otherwise, keep falling down
          bombs[4*bombsCounter + 1] := bombs[4*bombsCounter + 1] + 1
          displayb[bombs[4*bombsCounter + 0] + bombs[4*bombsCounter + 1]*40] := bombs[4*bombsCounter + 2]
          colorb [bombs[4*bombsCounter + 0] + bombs[4*bombsCounter + 1]*40] := $8E_02
          bombs[4*bombsCounter + 3] := 1

        'Is bomb "growing" ?
       if (bombs[4*bombsCounter + 2] > 10)
         'Do we grow to next stage
         if (bombs[4*bombsCounter + 2] < 18)
           bombs[4*bombsCounter + 2] := bombs[4*bombsCounter + 2] + 1
           bombs[4*bombsCounter + 3] := 20
           displayb[bombs[4*bombsCounter + 0] + bombs[4*bombsCounter + 1]*40] := bombs[4*bombsCounter + 2]
           colorb  [bombs[4*bombsCounter + 0] + bombs[4*bombsCounter + 1]*40] := $8E_02
          'Or does the bomb start dropping
         else
            bombs[4*bombsCounter + 2] := 10
            bombs[4*bombsCounter + 3] := 1
            displayb[bombs[4*bombsCounter + 0] + bombs[4*bombsCounter + 1]*40] := bombs[4*bombsCounter + 2]
            colorb [bombs[4*bombsCounter + 0] + bombs[4*bombsCounter + 1]*40] := $8E_02
           
PUB InitCentipedes | centipede

  'Mark Centipedes inactive
  repeat centipede from 0 to 19
   'Set Centipede inactive
   centipedes[45*centipede] := 0

PUB CreateCentipede(dirX, dirY, len, startX, startY) : newCentipede | centipede, centoffset, centlen

  activeCentipedes:=activeCentipedes + 1
  newCentipede := -1
  centipede := 0
  repeat while centipede < 19
    'Calculate offset  
    centoffset := 45 * centipede
    if (centipedes[centoffset + 0] ==0)
       newCentipede := centipede
      'Create centipede
      centipedes[centoffset + 0] := dirX  
      centipedes[centoffset + 1] := dirY  
      centipedes[centoffset + 2] := len
      repeat centlen from 0 to len   
        centipedes[centoffset + (centlen*2) + 5] := startX  
        centipedes[centoffset + (centlen*2) + 6] := startY
     'Exit   
     centipede := 20 

    'Next Centipede
    centipede := centipede + 1 


PUB SplitCentipede(cent_pos_x, cent_pos_y) | centipede, centoffset, centlen, len, newCentipede, newLen, oldLen, newDir, counter, cent_x, cent_y

  centipede := 0
  repeat while centipede < 19
    'Calculate offset  
    centoffset := 45 * centipede
    if (centipedes[centoffset + 0] <> 0)
      len := centipedes[centoffset + 2]
      repeat centlen from 0 to len
        if (cent_pos_x == centipedes[centoffset + (centlen*2) + 5] and cent_pos_y == centipedes[centoffset + (centlen*2) + 6])
          if (len <> 1)
            ' Did we hit las piece of the tail ?
            if ((len-centlen) < 2)
               'Shrink Centipede by one
               centipedes[centoffset + 2] := centipedes[centoffset + 2] - 1
               ' Clear end of centipede                                                                                   
               newLen := centipedes[centoffset + 2]                                                                       
               cent_x := centipedes[centoffset + (2 * newLen) + 5 ]                                                       
               cent_y := centipedes[centoffset + (2 * newLen) + 6 ]                                                       
               displayb[cent_x + cent_y*40] := 0                                                                          
               colorb  [cent_x + cent_y*40] := $7B_02                                                                     

            else
                                                                                                                          
               oldLen := centipedes[centoffset + 2]                                                                       
                                                                                                                          
               ' Clear end of centipede                                                                                   
               newLen := centipedes[centoffset + 2] - 1                                                                   
               cent_x := centipedes[centoffset + (2 * newLen) + 5 ]                                                       
               cent_y := centipedes[centoffset + (2 * newLen) + 6 ]                                                       
               displayb[cent_x + cent_y*40] := 0                                                                          
               colorb  [cent_x + cent_y*40] := $7B_02                                                                     
                                                                                                                          
               ' Clear current position                                                                                   
               centipedes[centoffset + 2] := centlen                                                                      
               displayb[cent_pos_x + cent_pos_y*40] := 0                                                                                                                                                                      
               colorb  [cent_pos_x + cent_pos_y*40] := $7B_02                                                             
                                                                                                                          
               'Check Direction                                                                                           
               if (centipedes[centoffset + 0] == 1)                                                                       
                 newDir := 2                                                                                              
                 cent_x := cent_pos_x - 1
                  if (cent_x < 1)                                                
                    newDir := 1                                                                                           
                    cent_x := cent_pos_x + 1
               else                                                                                                       
                 newDir := 1                                                                                              
                 cent_x := cent_pos_x + 1                                                                                 
                  if (cent_x > 38)                                               
                    newDir := 2                                                                                           
                    cent_x := cent_pos_x - 1

                                                                                                                          
               'Calculate new length                                                                                      
               newLen := oldLen - centlen - 1                                                                             
                                                                                                                          
               'New Centipede                                                                                             
               newCentipede := CreateCentipede(newDir, 1, newLen, cent_x, cent_pos_y + 1)                                 
                                                                                                                          
               if(newLen > 1)                                                                                             
                 oldLen--                                                                                                 
                 newLen := centlen + 1                                                                                    
                 newCentipede := newCentipede * 45                                                                        
                 repeat counter from newLen to oldLen                                                                     
                   centipedes[newCentipede + ((counter-newLen + 1)*2) + 5] := centipedes[centoffset + (counter*2) + 5]    
                   centipedes[newCentipede + ((counter-newLen + 1)*2) + 6] := centipedes[centoffset + (counter*2) + 6]    
                                                                                                                          
               if (centlen == 0)                                                                                          
                centipedes[centoffset + 0] := 0                                                                           
                activeCentipedes:=activeCentipedes - 1                                                                    
                AddScore(15)                                                                                       
          else                                                                                                            
            centipedes[centoffset + 0] := 0                                                                               
            activeCentipedes:=activeCentipedes - 1                                                                        
            AddScore(20)                                                                                           
            displayb[cent_pos_x + cent_pos_y*40] := 0                                                                     
            colorb  [cent_pos_x + cent_pos_y*40] := $7B_02                                                                
                                                                                                                          

                                                                                       
          'Exit   
          centipede := 20 

    'Next Centipede
    centipede := centipede + 1 

 
               
PUB DrawCentipedes | centipede, centoffset, centlen, cent_x, cent_y

  ' Loop through centipedes
  repeat centipede from 0 to 19
   'Calculate offset
   centoffset := 45 * centipede
   'Is the Centipede active ?
   if (centipedes[centoffset] <> 0)
     ' Clear end of centipede
     centlen := centipedes[centoffset + 2] - 1   
     cent_x := centipedes[centoffset + (2 * centlen) + 5 ]   
     cent_y := centipedes[centoffset + (2 * centlen) + 6 ]   
     displayb[cent_x + cent_y*40] := 0
     colorb  [cent_x + cent_y*40] := $7B_02

     ' Get current position...      
     cent_x := centipedes[centoffset + 5 ]   
     cent_y := centipedes[centoffset + 6 ]

     ' Move old positions.
        repeat centlen from (centipedes[centoffset + 2]-1) to 0
          centipedes[centoffset + (2 * centlen) + 7 ] := centipedes[centoffset + (2 * centlen) + 5 ]     
          centipedes[centoffset + (2 * centlen) + 8 ] := centipedes[centoffset + (2 * centlen) + 6 ]     

      'Left to right ?     
     if (centipedes[centoffset] == 1)
      cent_x := cent_x + 1

      'Did we hit the ship ?
      if (displayb[cent_x + cent_y*40] == 7)
        fsm_mode := MODE_LOST_LIFE
      
      ' Do we need to turn around ?
      if (cent_x > 38 or displayb[cent_x + cent_y*40] <> 0)
       cent_x := cent_x - 1
       centipedes[centoffset] := 2
       if (centipedes[centoffset + 1] == 1) 
         cent_y := cent_y + 1
       else
         cent_y := cent_y - 1

       ' Or right to left ?
     else
      cent_x := cent_x - 1
      'Did we hit the ship ?
      if (displayb[cent_x + cent_y*40] == 7)
        fsm_mode := MODE_LOST_LIFE

      ' Do we need to turn around ?
      if (cent_x < 1 or displayb[cent_x + cent_y*40] <> 0)
       cent_x := cent_x + 1
       centipedes[centoffset] := 1
       if (centipedes[centoffset + 1] == 1) 
         cent_y := cent_y + 1
       else
         cent_y := cent_y - 1

     ' Up to down 
     if (centipedes[centoffset + 1] == 1)
      if (cent_y > 23)
       cent_y := cent_y - 2
       centipedes[centoffset + 1] := 2
      ' Or down to up
     else
      if (cent_y < 12)
       cent_y := cent_y + 2
       centipedes[centoffset + 1] := 1
     

    ' Draw new head
    centipedes[centoffset + 5 ] := cent_x   
    centipedes[centoffset + 6 ] := cent_y
    
    displayb[cent_x + cent_y*40] := 19 + (cent_x // 3)
    colorb  [cent_x + cent_y*40] := $DC_02
    
    ' Is the centipede longer than 1 char ?
    if (centipedes[centoffset + 2] > 1)
     cent_x := centipedes[centoffset + 7]   
     cent_y := centipedes[centoffset + 8]   
     displayb[cent_x + cent_y*40] := 19
     colorb [cent_x + cent_y*40] := $DC_02
     
PUB DrawMissile

    ' Did we fire ?
    if (Grid_Event & GRID_MISSILE)
      'Is the missile drawn yet ?
      if (displayb[missile_x + missile_y*40] == 0)
        displayb[missile_x + missile_y*40] := 9         
        colorb  [missile_x + missile_y*40] := $07_02
      'Is the missile hit by something or not ?  
      if (displayb[missile_x + missile_y*40] == 9)
        'No the missile is not hit.
        displayb[missile_x + missile_y*40] := 0         
        colorb  [missile_x + missile_y*40] := $7B_02
        
        'Move missile
        missile_y := missile_y - 1
        'Did the Missile hit something ?
        if (displayb[missile_x + missile_y*40] == 0)
         displayb[missile_x + missile_y*40] := 9         
         colorb  [missile_x + missile_y*40] := $07_02
         'Yes, we hit something.
        else


          ' Did we hit the Centipede ?
          if (displayb[missile_x + missile_y*40] == 19)
            SplitCentipede(missile_x, missile_y)
            Grid_Event := Grid_Event & !GRID_MISSILE
          if (displayb[missile_x + missile_y*40] == 20)
            SplitCentipede(missile_x, missile_y)
            Grid_Event := Grid_Event & !GRID_MISSILE
          if (displayb[missile_x + missile_y*40] == 21)
            SplitCentipede(missile_x, missile_y)
            Grid_Event := Grid_Event & !GRID_MISSILE
                                                     
          'Did we hit a falling bomb?                       
          if (displayb[missile_x + missile_y*40] == 10)                           
            AddScore(10)                               
            ClearBomb( missile_x, missile_y)                                 
            displayb[missile_x + missile_y*40] := 0                          
            colorb  [missile_x + missile_y*40] := $7B_02                        
           Grid_Event := Grid_Event & !GRID_MISSILE                      

          'Did we hit a bomb in stage 1 to 4?                       
          if (displayb[missile_x + missile_y*40] == 15)                           
            AddScore(15)                               
            ClearBomb( missile_x, missile_y)                                 
            displayb[missile_x + missile_y*40] := 0                          
            colorb  [missile_x + missile_y*40] := $7B_02                        
           Grid_Event := Grid_Event & !GRID_MISSILE
          if (displayb[missile_x + missile_y*40] == 16)                           
            AddScore(15)                               
            ClearBomb( missile_x, missile_y)                                 
            displayb[missile_x + missile_y*40] := 0                          
            colorb  [missile_x + missile_y*40] := $7B_02                        
           Grid_Event := Grid_Event & !GRID_MISSILE
          if (displayb[missile_x + missile_y*40] == 17)                           
            AddScore(15)                               
            ClearBomb( missile_x, missile_y)                                 
            displayb[missile_x + missile_y*40] := 0                          
            colorb  [missile_x + missile_y*40] := $7B_02                        
           Grid_Event := Grid_Event & !GRID_MISSILE
          if (displayb[missile_x + missile_y*40] == 18)                           
            AddScore(15)                               
            ClearBomb( missile_x, missile_y)                                 
            displayb[missile_x + missile_y*40] := 0                          
            colorb  [missile_x + missile_y*40] := $7B_02                        
           Grid_Event := Grid_Event & !GRID_MISSILE
           
        'The missile got hit. 
      else
         'Hit by a falling bomb ?                                                 
         if (displayb[missile_x + missile_y*40] == 10)      
           AddScore(10)                                               
           ClearBomb( missile_x, missile_y)                                  
           displayb[missile_x + missile_y*40] := 0                              
           colorb  [missile_x + missile_y*40] := $7B_02
           Grid_Event := Grid_Event & !GRID_MISSILE                      

          ' Hit by the Centipede ?
          if (displayb[missile_x + missile_y*40] == 19)
            SplitCentipede(missile_x, missile_y)
            Grid_Event := Grid_Event & !GRID_MISSILE
          if (displayb[missile_x + missile_y*40] == 20)
            SplitCentipede(missile_x, missile_y)
            Grid_Event := Grid_Event & !GRID_MISSILE
          if (displayb[missile_x + missile_y*40] == 21)
            SplitCentipede(missile_x, missile_y)
            Grid_Event := Grid_Event & !GRID_MISSILE
                                 
      'Did it reache the top ?
      if (missile_y < 2)
          colorb  [missile_x + missile_y*40] := $02_02
          Grid_Event := Grid_Event & !GRID_MISSILE

    
PUB DrawShip
{{
Draw ship and clear old pisition if we moved. 
}}
    'Draw ship
    displayb[ship_x+ship_y*40] := 7
    colorb  [ship_x+ship_y*40] := $AD_02
    'Did we move ?
    if (ship_x_old <> ship_x)
      'Clear old ship position
      colorb  [ship_x_old+ship_y_old*40] := $7B_02
      displayb[ship_x_old+ship_y_old*40] := 0
    'Did we move ?
    if (ship_y_old <> ship_y)
      'Clear old ship position
      colorb  [ship_x_old+ship_y_old*40] := $7B_02
      displayb[ship_x_old+ship_y_old*40] := 0
    'Save old position
    ship_x_old := ship_x
    ship_y_old := ship_y


PUB DrawGrid | x,y
{{
Draw the grid for the playing field.
}}

    ' Use a specific channel for this sound effect...
     sound_playing[CHANNELS_AVAILABLE - 1] := SOUND_GRID                   

    'Draw the grid.
    repeat x from 1 to 38
      repeat y from 2 to 23
       displayb[x+y*40] := 63
       colorb[x+y*40] := $7B_02
      waitcnt(1_000_000 + cnt) 

    repeat y from 2 to 23
      repeat x from 1 to 38
       displayb[x+y*40] := 0
       colorb[x+y*40] := $7B_02
      waitcnt(1_000_000 + cnt) 
  

PUB DrawShipEntering | t
{{
Ship entering sequence...
}}

    repeat t from 1 to 17
      displayb[t+1+(t+1)*40] := 0
      colorb[t+1+(t+1)*40] := $7B_02
      displayb[t+2+((t+2)*40)] := (2- (t//3)) + 22
      colorb[t+2+((t+2)*40)] := $06_02

      displayb[39-t+(t+1)*40] := 0
      colorb[39-t+(t+1)*40] := $7B_02
      displayb[38-t+((t+2)*40)] := (2- (t//3)) + 22
      colorb[38-t+((t+2)*40)] := $06_02

      displayb[20+(t+1)*40] := 0
      colorb[20+(t+1)*40] := $7B_02
      displayb[20+((t+2)*40)] := (2- (t//3)) + 22
      colorb[20+((t+2)*40)] := $06_02

      displayb[t+1+23*40] := 0
      colorb[t+1+23*40] := $7B_02
      displayb[t+2+23*40] := (2- (t//3)) + 22
      colorb[t+2+23*40] := $06_02

      displayb[39-t+23*40] := 0
      colorb[39-t+23*40] := $7B_02
      displayb[38-t+23*40] := (2- (t//3)) + 22
      colorb[38-t+23*40] := $06_02
      waitcnt(2_000_000 + cnt)

    repeat t from 19 to 22
      displayb[19+(t*40)] := 0
      displayb[20+(t*40)] := 0
      displayb[21+(t*40)] := 0
      colorb[19+(t*40)] := $7B_02
      colorb[20+(t*40)] := $7B_02
      colorb[21+(t*40)] := $7B_02
      displayb[19+((t+1)*40)] := (2- (t//3)) + 22
      displayb[20+((t+1)*40)] := (2- (t//3)) + 22
      displayb[21+((t+1)*40)] := (2- (t//3)) + 22
      colorb[19+((t+1)*40)] := $06_02
      colorb[20+((t+1)*40)] := $06_02
      colorb[21+((t+1)*40)] := $06_02
      waitcnt(2_000_000 + cnt)

    displayb[19+(23*40)] := 0
    displayb[20+(23*40)] := 0
    displayb[21+(23*40)] := 0
    colorb[19+(23*40)] := $7B_02
    colorb[20+(23*40)] := $7B_02
    colorb[21+(23*40)] := $7B_02

PUB DrawExplodingShip | t, x, y
{{
Expole ship into a star...
}}

    ' Play sound effect.
    sound_playing[CHANNELS_AVAILABLE - 2] := SOUND_SHIP_EXPLODE                   

   repeat t from 0 to 40
    'Up
     x := ship_x         
     y := ship_y - t
     DrawExplodingShip_clear(x, y)
     DrawExplodingShip_explosion(x, y - 1)
    'Down
     x := ship_x         
     y := ship_y + t
     DrawExplodingShip_clear(x, y)
     DrawExplodingShip_explosion(x, y + 1)
    'Left
     x := ship_x - t     
     y := ship_y
     DrawExplodingShip_clear(x, y)
     DrawExplodingShip_explosion(x - 1, y)
    'Right
     x := ship_x + t     
     y := ship_y
     DrawExplodingShip_clear(x, y)
     DrawExplodingShip_explosion(x + 1, y)
    'Up + Left
     x := ship_x - t     
     y := ship_y - t
     DrawExplodingShip_clear(x, y)
     DrawExplodingShip_explosion(x - 1, y - 1)
    'Down + Left
     x := ship_x - t     
     y := ship_y + t
     DrawExplodingShip_clear(x, y)
     DrawExplodingShip_explosion(x - 1, y + 1)
    'Up + Right
     x := ship_x + t     
     y := ship_y - t
     DrawExplodingShip_clear(x, y)
     DrawExplodingShip_explosion(x + 1, y - 1)
    'Down + Right
     x := ship_x + t     
     y := ship_y + t
     DrawExplodingShip_clear(x, y)
     DrawExplodingShip_explosion(x + 1, y + 1)

      waitcnt(2_000_000 + cnt)
     
PUB DrawExplodingShip_clear(x, y)

   ' Clear ship if we are within boundary
   if (x>0 and x<39)
    if (y>1 and y<24)
     displayb[x+(y*40)] := 0
     colorb  [x+(y*40)] := $7B_02

PUB DrawExplodingShip_explosion(x, y)

   ' Draw explosion ship if we are within boundary
   if (x>0 and x<39)
    if (y>1 and y<24)
     displayb[x+(y*40)] :=  (2- ((x+y)//3)) + 22
     colorb  [x+(y*40)] := $06_02


PUB AddScore(scoreToAdd)
  score := score + scoreToAdd

PUB ClearScreen | index 

    ' Clear screen
    repeat index from 0 to 999
      displayb[index] := 32 

   ' Clear colors
    repeat index from 0 to 999
      colorb[index] := $8E_02 

    backgroundCol := $02

    
PUB PrintTopRow

    ' Print GRID
    PrintAt(0,0,String("ABDE"),$DC_02)
    ' Print RUNNER
    PrintAt(4,0,String("BCFFGB"),$2B_02)

    ' Print PL.1
    displayb[12] := 25
    displayb[13] := 26

    ' Print Score
    PrintScore
    
    ' Print HI:
    displayb[24] := 29
    displayb[25] := 30

    ' Print Highscore
    itoa7(hiscore, @sbuffer)
    PrintAt(27,0,@sbuffer,$2B_02)

    ' Print Ship
    displayb[36] := 7
    colorb[36] := $AD_02

    ' Ships left
    displayb[36] := shipsLeft + $30
    colorb[36] := $2B_02

PUB PrintScore

    ' Print Score
    itoa7(score, @sbuffer)
    PrintAt(15,0,@sbuffer,$DC_02)


PUB PrintMenu

    ' Print BY JEFF MINTER
    PrintAt(11,5,String("I  LGJJ MDF:GB"),$DC_02)
    displayb[12+5*40] := 27

    ' Print ENTER LEVEL 01
    PrintAt(11,7,String("GF:GB >G;G> 01"),$06_02)

    ' Print (C) 2008 BAM PRESS FIRE TO BEGIN
    PrintAt(3,9,String("<= 2008 IHM  NBGOO JDBG :0 IGADF"),$8E_02)


PUB PrintStart

    ' Print BATTLE STATIONS
    PrintAt(15,5,String("IH::>G O:H:D0FO"),$2B_02)

    ' Print ENTER GRID AREA 01
    PrintAt(14,7,String("GF:GB ABDE HBGH"),$06_02)

    ' Print Level
    itoa2(level, @sbuffer)
    PrintAt(30,7,@sbuffer,$06_02)
    

PUB Print(char, color)
    PrintAt(col, row, char, color)

PUB PrintAt(x,y,char, color) | i, daChar

    ' Print a string
    col:=x
    row:=y
    repeat i from 0 to strSize(char) - 1
      daChar:= byte[char][i]
      if byte[char][i] == 13
        row++
        next
      if byte[char][i] == 10
        col:=0
        next
      if byte[char][i] == 0
        abort

      if byte[char][i] > 63
       if  byte[char][i] < 91
        daChar:= byte[char][i]-32

      if byte[char][i] > 95
       if  byte[char][i] < 123
        daChar:= byte[char][i]-96
      
      displayb[col + row*40] := daChar
      colorb[col + row*40] := color
      col++

PUB NES_Read_Gamepad : nes_bits        |  i 
' //////////////////////////////////////////////////////////////////
' NES Game Paddle Read
' //////////////////////////////////////////////////////////////////       
' reads both gamepads in parallel encodes 8-bits for each in format
' right game pad #1 [15..8] : left game pad #0 [7..0]
'
' set I/O ports to proper direction
' P3 = JOY_CLK      (4)
' P4 = JOY_SH/LDn   (5) 
' P5 = JOY_DATAOUT0 (6)
' P6 = JOY_DATAOUT1 (7)
' NES Bit Encoding
'
' RIGHT  = %00000001
' LEFT   = %00000010
' DOWN   = %00000100
' UP     = %00001000
' START  = %00010000
' SELECT = %00100000
' B      = %01000000
' A      = %10000000

' step 1: set I/Os
DIRA [JOY_CLK] := 1 ' output
DIRA [JOY_LCH] := 1 ' output
DIRA [JOY_DATAOUT0] := 0 ' input
DIRA [JOY_DATAOUT1] := 0 ' input

' step 2: set clock and latch to 0
OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0
'Delay(1)

' step 3: set latch to 1
OUTA [JOY_LCH] := 1 ' JOY_SH/LDn = 1
'Delay(1)                            

' step 4: set latch to 0
OUTA [JOY_LCH] := 0 ' JOY_SH/LDn = 0

' data is now ready to shift out, clear storage
nes_bits := 0

' step 5: read 8 bits, 1st bits are already latched and ready, simply save and clock remaining bits
repeat i from 0 to 7

 nes_bits := (nes_bits << 1)
 nes_bits := nes_bits | INA[JOY_DATAOUT0] | (INA[JOY_DATAOUT1] << 8)

 OUTA [JOY_CLK] := 1 ' JOY_CLK = 1
 'Delay(1)             
 OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
 
 'Delay(1)             
' invert bits to make positive logic
nes_bits := (!nes_bits & $FFFF)


if kbd.keystate(KBD_FIRE)
  nes_bits |= NES0_B
if kbd.keystate(KBD_PAUSE)
  nes_bits |= NES0_START
if kbd.keystate(KBD_UP)
  nes_bits |= NES0_UP
if kbd.keystate(KBD_DOWN)
  nes_bits |= NES0_DOWN
if kbd.keystate(KBD_LEFT)
  nes_bits |= NES0_LEFT
if kbd.keystate(KBD_RIGHT)
  nes_bits |= NES0_RIGHT

' //////////////////////////////////////////////////////////////////
' End NES Game Paddle Read
' //////////////////////////////////////////////////////////////////       

' ////////////////////////////////////////////////////////////////////

PUB PrintC64(x,y,char) | i, daChar

    ' Print a string.
    col:=x
    row:=y
    repeat i from 0 to strSize(char) - 1
      daChar:= byte[char][i]

      if byte[char][i] > 95
       if  byte[char][i] < 123
        daChar:= byte[char][i]-32

      if byte[char][i] > 32
       if  byte[char][i] < 58
        daChar:= byte[char][i] + 64
      
      displayb[col + row*40] := daChar
      col++

PUB DecStr(n,digits) | i, str[12/3]

digits<#=11
result := @str+11
byte[result]:=0
repeat digits
  byte[--result] := "0"+n//10
  n/=10



PUB PrintC64Slow(x,y,char) | i, daChar

    ' Print a string slow as if someone typing it.
    col:=x
    row:=y
    repeat i from 0 to strSize(char) - 1
      daChar:= byte[char][i]

      if byte[char][i] > 95
       if  byte[char][i] < 123
        daChar:= byte[char][i]-32

      if byte[char][i] > 32
       if  byte[char][i] < 58
        daChar:= byte[char][i] + 64
      
      displayb[col + row*40] := daChar
      waitcnt(10_000_000 + cnt)
      col++

PUB CursorC64(pos)
      ' Flash cursor @ pos
      waitcnt(23_000_000 + cnt)
      word [@colorb] [pos] := $0B_0D
      waitcnt(23_000_000 + cnt)
      word [@colorb] [pos] := $0D_0B

PUB itoa7(value, string_ptr) | base, factor, i
' converts an integer into an ASCIIZ, up to 999_999
' string_ptr points to target storage to store converted string

base := 100_0000

repeat i from 7 to 2
  factor := value / base 
  byte[0][string_ptr++] := Dec_Table[ factor ]
  value -= factor*base
  base /= 10

' 1's position
byte[0][string_ptr++] := Dec_Table[ value ]

' NULL terminate
byte[0][string_ptr++] := 0

PUB itoa2(value, string_ptr) | factor, i
' converts an integer into an ASCIIZ, up to 999_999
' string_ptr points to target storage to store converted string

 if value > 99
  value := 99

factor := value / 10 
byte[0][string_ptr++] := Dec_Table[ factor ]
value -= factor*10

' 1's position
byte[0][string_ptr++] := Dec_Table[ value ]

' NULL terminate
byte[0][string_ptr++] := 0

{
Pub  HWDetect | HWPins, Type

' //////////////////////////////////////////////////////
' AUTHOR: Coley
' LAST MODIFIED: 23.06.08
' VERSION 1
' COMMENTS: Routine for checking Hardware configurations
'           of various Propeller based boards.             
' //////////////////////////////////////////////////////
'
'  Power on harware configurations for target boards
'
'                      Pin Number
'
'            00000000001111111111222222222233
'            01234567890123456789012345678901
'
'                                    vdac
' Hybrid     000000001111111100000000OOOO1111
'
'                        vdac
' Demoboard  000000000000OOOO0000000011110111
'
'                                    vdac
' Hydra      010001000101010111110000OOOO??11 
'
' For this application we will check the status
' of P12..P13
'
' %00 - Demo/Protoboard
' %01 - Hydra
' %11 - Hybrid
'
' //////////////////////////////////////////////////////

  clkset(%01101000,  12_000_000)                        ' Set internal oscillator to RCFast and set PLL to start
  waitcnt(cnt+120_000)                                  ' wait approx 10ms at 12mhz for PLL to 'warm up'
  
  HWPins := INA[12..13]                                 ' Check state of Pins 12-13

  CASE HWPins
    %00 : clkset( %01101111, 80_000_000 )               ' Demo Board   80MHz (5MHz PLLx16)  
          Type    := 0      
    %01 : clkset( %01110110, 80_000_000 )               ' Hydra        80MHz (10MHz PLLx8)
          Type    := 1
    %11 : clkset( %01101111, 96_000_000 )               ' Hybrid       96MHz (6MHz PLLx16)
          Type    := 2

 ' Modified to return Type of system.
 return Type
}
PRI GridRunner_Sound | i

{{
Main loop for playing sound effects, this will run in it's own cog...
}}
  repeat
    ' Standar delay
    waitcnt(cnt + 5_000)

    repeat i from 0 to CHANNELS_AVAILABLE

      if( sound_playing[i] > 128)
       sound_playing[i]:=sound_playing[i] - 128

       CASE sound_playing[i]
        1:
          Sound1(0, i)
        2:
          Sound2(0, i)
        3:
          Sound3(0, i)
        4:
          Sound4(0, i)
        5:
          Sound5(0, i)
       
      if( sound_playing[i] > 0)
       
       CASE sound_playing[i]
        1:
          sound_playing[i] := Sound1(1, i)
        2:
          sound_playing[i] := Sound2(1, i)
        3:
          sound_playing[i] := Sound3(1, i)
        4:
          sound_playing[i] := Sound4(1, i)
        5:
          sound_playing[i] := Sound5(1, i)
                    

PRI Sound1(param, channel) | retval

' Sound effect for drawing grid.
' SOUND_GRID

  retval := 1

  'If parameter = 0, then start sound.
  if (param == 0)
    sound1_step1 := 9
    sound1_step2 := 20001

    ' Play effect.
  else
    if(sound1_step2 > 2000)
    
      if(sound1_step1 < 21)
       sound1_step1++
       sound1_step2:= 100
       snd.PlaySoundFM(channel, snd#SHAPE_TRIANGLE, sound1_step2 + 1000*sound1_step1, CONSTANT( Round(Float(snd#SAMPLE_RATE) * 0.4)), 255, $3579_ADEF )

      else
       retval := 0

    else
    
      snd.SetFreq(channel,sound1_step2 + 1000*sound1_step1)
      sound1_step2 := sound1_step2 + 8         


  return retval


PRI Sound2(param, channel) | retval

' Sound effect for side swipers.
' SOUND_SWIPE

  retval := 2

  'If parameter = 0, then start sound.
  if (param == 0)

    ' Play effect.
  else
    snd.PlaySoundFM(channel, snd#SHAPE_NOISE, 2000, CONSTANT( Round(Float(snd#SAMPLE_RATE) * 0.4)), 155, $3579_ADEF )
    retval := 0


  return retval

PRI Sound3(param, channel) | retval

' Sound effect for missiles.
' SOUND_MISSILE

  retval := 3

  'If parameter = 0, then start sound.
  if (param == 0)
    sound3_step1 := 20
    sound3_step2 := 20001

    ' Play effect.
  else

    if(sound3_step2 > 2000)
    
      if(sound3_step1 < 21)
       sound3_step1++
       sound3_step2:= 400
       snd.PlaySoundFM(channel, snd#SHAPE_SQUARE, sound3_step2 + 2200, CONSTANT( Round(Float(snd#SAMPLE_RATE) * 0.4)), 255, $3579_ADEF )

      else
       retval := 0

    else
    
      snd.SetFreq(channel,sound3_step2 + 2200 )
      sound3_step2 := sound3_step2 + 5         


  return retval  

PRI Sound4(param, channel) | retval

' Exploding ship
' SOUND_SHIP_EXPLODE

  retval := 4

  if (param == 0)


   'Play sound.
  else

    snd.PlaySoundFM(channel, snd#SHAPE_NOISE, $100 , CONSTANT( snd#SAMPLE_RATE / 1 ), 110, $2468_ACEF)
    retval := 0

  return retval

PRI Sound5(param, channel) | retval

' Sound effect for new level.
' SOUND_LEVEL_START

  retval := 5

  'If parameter = 0, then start sound.
  if (param == 0)
    sound5_step1 := 50
    sound5_step2 := 20001

    ' Play effect.
  else

    if(sound5_step2 > 2000)
    
      if(sound5_step1 < 60)
       snd.PlaySoundFM(channel, snd#SHAPE_SINE, 1000, CONSTANT( Round(Float(snd#SAMPLE_RATE) * 0.2)), 255, $3579_ADEF )
       sound5_step1++
       sound5_step2:= 400

      else
       retval := 0

    else
    
      snd.SetFreq(channel,sound5_step2*20 + 5000 +  sound5_step1*1000 )
      sound5_step2 := sound5_step2 + 4         


  return retval  
  
DAT

' These are not the exact colors, but a pretty good estimate.
Commodore_Palette       byte  $02            ' 0 - Black
                        byte  $07            ' 1 - White
                        byte  $5A            ' 2 - Red
                        byte  $DC            ' 3 - Cyan
                        byte  $2B            ' 4 - Purple
                        byte  $AB            ' 5 - Green
                        byte  $0B            ' 6 - Blue
                        byte  $8E            ' 7 - Yellow
                        byte  $6C            ' 8 - Orange
                        byte  $7A            ' 9 - Brown
                        byte  $5D            ' A - Light red
                        byte  $03            ' B - Dark grey
                        byte  $04            ' C - Grey
                        byte  $AE            ' D - Light green
                        byte  $0D            ' E - Light blue
                        byte  $05            ' F - Light grey

LOAD_STR1               byte "LOAD", 34, "GRIDRUNNER", 34, ",8,1",0
LOAD_STR2               byte "LOADING",0
LOAD_STR3               byte "READY.",0
LOAD_STR4               byte "RUN",0

Dec_Table               byte "0123456789"

' The lenght of the centipede per level for the right Centipede.
Centipede1_Level        byte 5  ' Level 1
                        byte 6  ' Level 2
                        byte 7  ' Level 3
                        byte 8  ' Level 4
                        byte 9  ' Level 5
                        byte 10 ' Level 6
                        byte 7  ' Level 7
                        byte 7  ' Level 8
                        byte 8  ' Level 9
                        byte 8  ' Level 10
                        byte 9  ' Level 11
                        byte 9  ' Level 12
                        byte 10 ' Level 13
                        byte 10 ' Level 14
                        byte 11 ' Level 15
                        byte 12 ' Level 16
                        byte 13 ' Level 17
                        byte 14 ' Level 18
                        byte 15 ' Level 19

' The lenght of the centipede per level for the left Centipede.
Centipede2_Level        byte 0  ' Level 1
                        byte 0  ' Level 2
                        byte 0  ' Level 3
                        byte 0  ' Level 4
                        byte 0  ' Level 5
                        byte 0  ' Level 6
                        byte 3  ' Level 7
                        byte 4  ' Level 8
                        byte 5  ' Level 9
                        byte 6  ' Level 10
                        byte 7  ' Level 11
                        byte 8  ' Level 12
                        byte 9  ' Level 13
                        byte 10 ' Level 14
                        byte 11 ' Level 15
                        byte 12 ' Level 16
                        byte 13 ' Level 17
                        byte 14 ' Level 18
                        byte 15 ' Level 19
                                           