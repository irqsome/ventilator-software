{{
        Planetary Defense sound driver.
}}

OBJ
  snd   : "NS_sound_drv_052_11khz_16bit_stereo.spin" 'Sound driver

PUB Start
  snd.start(10)
  dCnt := clkfreq / 60
  nextCnt := dCnt + cnt

  UfoOff
  
PUB Stop
  snd.stop

VAR
  long  nextCnt       ' When cnt reaches this value, a time interval has elapsed.
  long  dCnt          ' The interval between nextCnts.

PUB Nudge 
' Some sounds have time-varying parameters. 
' Whenever the main program loop has a free moment it should call Nudge to make the parameters vary.
  if cnt - nextCnt < 0
    return

  nextCnt += dCnt

  SirenNudge
  FireANudge
  FireBNudge
  TahdahNudge
  NofireNudge
  UfoNudge
    
CON
  CH_SIREN = 0
  CH_FIREA = 0
  CH_FIREB = 0
  CH_BOOM = 1'..5
  CH_TAHDAH = 4'..7
  CH_THUP = 0
  CH_SCANNER = 0'..1
  CH_NOFIRE = 0
  CH_UFO = 1
    
PUB Siren
  durSiren := 120
  tmrSiren := -1

VAR
long  tmrSiren, durSiren
      
PRI SirenNudge | i
  if durSiren
    if ++tmrSiren < durSiren
      i := tmrSiren // 40
      snd.PlaySoundFM( CH_SIREN, snd#SHAPE_SQUARE, 350 + i*30, snd#SAMPLE_RATE/10, 128, snd#NO_ENVELOPE )
    else
      snd.StopSound( CH_SIREN )
      durSiren := 0
  
VAR
  long  tmrFireA, durFireA
  
PUB FireA
  durFireA := 11
  tmrFireA := -1

PRI FireANudge
  if durFireA
    if ++tmrFireA < durFireA
      snd.PlaySoundFM( CH_FIREA, snd#SHAPE_SAWTOOTH, 1200-tmrFireA*60, snd#SAMPLE_RATE/6, 228-tmrFireA*20, snd#NO_ENVELOPE )
    else
      snd.StopSound( CH_FIREA )
      durFireA := 0

VAR
  long  tmrFireB, durFireB
  
PUB FireB
  durFireB := 16
  tmrFireB := -1

PRI FireBNudge
  if durFireB      
    if ++tmrFireB < durFireB
      snd.PlaySoundFM( CH_FIREB, snd#SHAPE_TRIANGLE, 1200-tmrFireB*40, snd#SAMPLE_RATE/6, 228-tmrFireB*15, snd#NO_ENVELOPE )
    else
      snd.StopSound( CH_FIREB )
      durFireB := 0

PUB Boom | i
  repeat i from 0 to 4
    snd.PlaySoundFM( CH_BOOM + i, snd#SHAPE_NOISE, 150 + i * 57, snd#SAMPLE_RATE*3/2,  255, $1234_68eF)

VAR
  long  tmrTahdah, durTahdah
  
PUB Tahdah | i
  durTahdah := 60
  tmrTahdah := -1

PRI TahdahNudge | i
  if durTahdah
    if ++tmrTahdah < durTahdah
      if tmrTahdah == 0
        repeat i from 0 to 3
          snd.PlaySoundFM( CH_TAHDAH + i, snd#SHAPE_TRIANGLE, chord[i], snd#SAMPLE_RATE/8, 128, $1578_88cf )
      if tmrTahdah == 8
        repeat i from 0 to 3
          snd.PlaySoundFM( CH_TAHDAH + i, snd#SHAPE_TRIANGLE, chord[i], snd#SAMPLE_RATE, 128, $1578_88ff )
    else
      durTahdah := 0    

DAT
chord long    snd#NOTE_D4, snd#NOTE_Fs4, snd#NOTE_A4, snd#NOTE_D5

PUB Thup
  snd.PlaySoundFM( CH_THUP , snd#SHAPE_NOISE, 220, snd#SAMPLE_RATE/10, 228, $123468df )

PUB Scanner( i ) | r
  if i == 0  
    snd.PlaySoundFM( CH_SCANNER, snd#SHAPE_SINE, 1200, snd#SAMPLE_RATE/60, 128, snd#NO_ENVELOPE )
    snd.PlaySoundFM( CH_SCANNER+1, snd#SHAPE_SINE, 1215, snd#SAMPLE_RATE/60, 128, snd#NO_ENVELOPE )
  else
    snd.PlaySoundFM( CH_SCANNER, snd#SHAPE_SAWTOOTH, ?r & 255 + 1000, snd#SAMPLE_RATE/60, 128, snd#NO_ENVELOPE )

VAR
  long  tmrNofire, durNofire

PUB Nofire
  durNofire := 8
  tmrNofire := -1

PRI NofireNudge
  if durNoFire
    if ++tmrNofire < durNofire
      snd.PlaySoundFM( CH_NOFIRE, snd#SHAPE_SINE, 800+tmrNofire*100, snd#SAMPLE_RATE, 128-tmrNofire*16, snd#NO_ENVELOPE )
    else
      snd.StopSound( CH_NOFIRE )
      durNofire := 0

VAR
  long  tmrUfo

PUB UfoOn
  tmrUfo := 0

PUB UfoOff
  tmrUfo := -1
  snd.StopSound( CH_UFO )
    
PRI UfoNudge | i
  if tmrUfo < 0
    return
  i := ++tmrUfo // 10
  if i => 5
    i := 10 - i
  snd.PlaySoundFM( CH_UFO, snd#SHAPE_SQUARE, 300+i*100, snd#SAMPLE_RATE, 128, snd#NO_ENVELOPE )
  