{{
        Planetary Defense renderer.

        - Converts one line of the bitmap (2 bits per pixel) to high-color (8 bpp)
        - Overlays high-color (8 bpp) sprites that intersect that line
        - Communicates with rasterizer through status (probably could have chosen a better name):
                status>>1 is the scan line to be rendered.
                status&1 == 0 is the rasterizer telling a renderer to start rendering line (status>>1)
                status&1 == 1 is a renderer saying it is now rendering
        It takes about three scan lines' worth of time to render one scan line, so three cogs are used.
        One renders lines 0, 3, 6..., the next 1, 4, 7..., the third 2, 5, 8... 
}}        
{
  2007-07-23 begin modifying blah011_render to use high-color sprites instead of 2-bit palettized sprites.
}

CON
  sizeofSpriteData = 2 ' longs
  
PUB Start( params, id )
  long[params] := 0
  rendererId := id
  cog := cognew( @Renderer_Entry, params )
  repeat while long[params] == 0

PUB Stop
  cogstop( cog )

VAR
  long  cog
DAT
                        org $000
Renderer_Entry
                        mov     pPar, par
                        add     pPar, #4
                        rdlong  nRenderCogs, pPar       ' par[1]

                        add     pPar, #16
                        rdlong  nPixels, pPar           ' par[5]
                        add     pPar, #4
                        rdlong  nLines, pPar            ' par[6]
                        mov     columnOffset, nLines
                        shl     columnOffset, #2
          
                        mov     pPar, rendererId
                        add     pPar, #11
                        shl     pPar, #2
                        add     pPar, par
                        rdlong  pBuff, pPar             ' par[11+renId]
                        
                        mov     temp, #$1ff
                        wrlong  temp, par               ' set status to $1ff to inform hub that this renderer is initialized

:NewFrame               mov     pPar, par
                        add     pPar, #12
                        rdlong  pScreen, pPar           ' par[3]
                        mov     temp, rendererId
                        shl     temp, #2
                        add     pScreen, temp

                        add     pPar, #4
                        rdlong  pSpriteData, pPar       ' par[4]

                        mov     line, rendererId

:WaitForNextLine        rdlong  status, par
                        shr     status, #1
                        cmp     status, line    wz
              if_nz     jmp     #:WaitForNextLine

                        mov     pPar, par
                        add     pPar, #8                ' par[2]
                        rdlong  palette, pPar           ' 4 colors for bitmap

                        mov     pPar, par
                        add     pPar, #16               ' par[4]
                        rdlong  pSpriteData, pPar
                        rdlong  activeSprites, pSpriteData
                        add     pSpriteData, #4

                        shl     status, #1
                        or      status, #1      ' set LSB
                        wrlong  status, par

                        mov     r0, palette
                        mov     r1, r0
                        rol     r1, #8
                        movd    :store, #colorTable
                        mov     i, #4
:c0
                        mov     j, #4
:c1
                        mov     temp, r0
                        and     temp, #$ff
                        mov     l0ng, r1
                        and     l0ng, byte1mask
                        or      l0ng, temp
:store                  mov     0-0, l0ng
                        add     :store, IncrDst
                        ror     r0, #8
                        djnz    j, #:c1
                        ror     r1, #8
                        djnz    i, #:c0

                        movd    :toLinebuff, #linebuff
                        
                        mov     pSrc, pScreen
                        mov     temp, nRenderCogs
                        shl     temp, #2
                        add     pScreen, temp             ' Now points to next row
                        
                        mov     i, nPixels
                        shr     i, #4
:loop                   rdlong  pixels, pSrc            ' Get a long's worth of 2-bit pixels

                           
                        add     pSrc, columnOffset      ' Point pSrc to the next long
                        mov     j, #4                   ' Do the following 4 times
:byteToLong
                        mov     l0ng, #0
                        
                        mov     temp, pixels
                        and     temp, #$0f              ' 4 bits = 2 pixels
                        add     temp, #colorTable
                        movs    :readTable1, temp
                        ror     pixels, #4
:readTable1             or      l0ng, 0-0
                        ror     l0ng, #16
                        
                        mov     temp, pixels
                        and     temp, #$0f              ' 4 bits = 2 pixels
                        add     temp, #colorTable
                        movs    :readTable2, temp
                        ror     pixels, #4
:readTable2             or      l0ng, 0-0
                        ror     l0ng, #16

:toLinebuff             mov     0-0, l0ng
                        add     :toLinebuff, IncrDst
                        
                        djnz    j, #:byteToLong

                        djnz    i, #:loop

                        mov     i, #32
:z0
                        ror     activeSprites, #1       wc
              if_nc     add     pSpriteData, #sizeofSpriteData*4
              if_nc     jmp     #:z2

                        rdlong  currentSpriteData+0, pSpriteData
                        add     pSpriteData, #4
                        rdlong  pSpriteImageInfo, pSpriteData
                        add     pSpriteData, #4
                        rdlong  currentSpriteData+1, pSpriteImageInfo
                        add     pSpriteImageInfo, #4

                        call    #SpriteStuff

:z2
{                       rdlong  temp, par               ' get status
                        sar     temp, #1
                        sub     temp, line
                        cmp     temp, nRenderCogs       wc, wz
             if_nz}     djnz    i, #:z0


' Move linebuff to hub memory
                        movd    :fromLinebuff0, #linebuff
                        movd    :fromLinebuff1, #linebuff+1
                        movd    :fromLinebuff2, #linebuff+2
                        movd    :fromLinebuff3, #linebuff+3
                        mov     pDst, pBuff
              
                        mov     i, nPixels
                        shr     i, #4
:fromLinebuff0          wrlong  0-0, pDst
                        add     :fromLinebuff0, IncrDstBy4
                        add     pDst, #4
:fromLinebuff1          wrlong  0-0, pDst
                        add     :fromLinebuff1, IncrDstBy4
                        add     pDst, #4
:fromLinebuff2          wrlong  0-0, pDst
                        add     :fromLinebuff2, IncrDstBy4
                        add     pDst, #4
:fromLinebuff3          wrlong  0-0, pDst
                        add     :fromLinebuff3, IncrDstBy4
                        add     pDst, #4
                        djnz    i, #:fromLinebuff0

                        add     line, nRenderCogs
                        cmps    line, nLines    wc, wz
              if_b      jmp     #:WaitForNextLine

                        jmp     #:NewFrame


' Sprite stuff -----------------------------------------------------------

SpriteStuff
              ' Unpack spriteData struct[0]
                        mov     temp, currentSpriteData+0
                        mov     x, temp
                        shl     x, #16
                        sar     x, #16
                        mov     y, temp
                        sar     y, #16

                        mov     temp, currentSpriteData+1
                        mov     xHot, temp
                        and     xHot, #$ff
                        mov     yHot, temp
                        shr     yHot, #8
                        and     yHot, #$ff
                        mov     w, temp
                        shr     w, #16
                        and     w, #$ff
                        mov     h, temp
                        shr     h, #24
                        and     h, #$ff

'                     pSpriteInfo = currentSpriteData+2

                        sub     x, xHot
                        cmps    x, nPixels      wc, wz
              if_ae     jmp     #SpriteStuff_ret
                        neg     temp, w
                        cmps    x, temp         wc, wz  ''' room for improvement?
              if_b      jmp     #SpriteStuff_ret
              
                        sub     y, yHot
                        cmps    y, line         wc, wz
              if_a      jmp     #SpriteStuff_ret
                        mov     temp, y
                        add     temp, h
                        cmps    temp, line      wc, wz
              if_be     jmp     #SpriteStuff_ret

                        mov     temp, line
                        sub     temp, y
                        ' We want the temp'th line of spriteImageInfo, and each line
                        ' is (1 + w/4) longs long, so multiply temp by (1 + w/4) * 4 (to get byte address)
                        mov     Q, w
                        shr     Q, #2
                        add     Q, #1
                        shl     temp, #16
                        shr     Q, #1           wc
              if_c      add     Q, temp         wc
                        rcr     Q, #1           wc
              if_c      add     Q, temp         wc
                        rcr     Q, #1           wc
              if_c      add     Q, temp         wc
                        rcr     Q, #1           wc
              if_c      add     Q, temp        
                        shr     Q, #11
                        
                        add     pSpriteImageInfo, Q     ' pSpriteImageInfo is a pointer to hub memory; adjust it
                                                        '  to point to correct line of sprite image info.
                        
                        rdlong  mask0, pSpriteImageInfo
                        add     pSpriteImageInfo, #4    ' Increment to point to sprite bitmap
                        mov     temp, x                 ' x/4
                        sar     temp, #2                ' The long of interest is linebuff[x/4];
                        add     temp, #linebuff         '  it will contain the first 1 to 4 pixels.
                        movs    :read, temp
                        movd    :write, temp
                        mov     shift, x
                        and     shift, #3       


                        shl     mask0, shift
                        shl     shift, #3               ' now shift is 0, 8, 16, 24
                        neg     endmask0, #1            
                        shl     endmask0, shift         ' endmask0 = ffffffff, ffffff00, ffff0000, ff000000
                        neg     endmask1, endmask0
                        sub     endmask1, #1            ' endmask1 = 00000000, 000000ff, 0000ffff, 00ffffff
                        
                        mov     pixels0, #0
:loop                   
                        rdlong  pixels, pSpriteImageInfo
                        add     pSpriteImageInfo, #4
                        rol     pixels, shift
                        mov     data, pixels
                        and     data, endmask0
                        or      data, pixels0
                        
                        mov     temp, mask0
                        and     temp, #$0f
                        add     temp, #maskTable
                        movs    :readMask, temp
                        nop                             
:readMask               mov     mask, 0-0

:read                   mov     temp, 0-0
                        xor     temp, data
                        andn    temp, mask
                        xor     temp, data
:write                  mov     0-0, temp
                        add     :read, #1
                        add     :write, IncrDst

                        mov     pixels0, pixels         ' save previous pixel bits
                        and     pixels0, endmask1       ' well, just the relevant bits
                        shr     mask0, #4       wz
              if_nz     jmp     #:loop

SpriteStuff_ret         ret

nPixels                 long    0
nLines                  long    0
x                       long    0
y                       long    0
xHot                    long    0
yHot                    long    0
w                       long    0
h                       long    0
shift                   long    0
data                    long    0
mask                    long    0

r0                      long    0
r1                      long    0
status                  long    0
rendererId              long    0
nRenderCogs             long    0
pBuff                   long    0
pScreen                 long    0
pPar                    long    0
line                    long    0
i                       long    0
j                       long    0
pSrc                    long    0
pDst                    long    0
pixels                  long    0
pixels0                 long    0
pixels1                 long    0
columnOffset            long    0
byte1mask               long    $00_00_ff_00
IncrDst                 long    1<<9
IncrDstSrc              long    1<<9 + 1
IncrDstBy4              long    1<<11
maskTable               long    $00_00_00_00
                        long    $00_00_00_ff
                        long    $00_00_ff_00
                        long    $00_00_ff_ff
                        long    $00_ff_00_00
                        long    $00_ff_00_ff
                        long    $00_ff_ff_00
                        long    $00_ff_ff_ff
                        long    $ff_00_00_00
                        long    $ff_00_00_ff
                        long    $ff_00_ff_00
                        long    $ff_00_ff_ff
                        long    $ff_ff_00_00
                        long    $ff_ff_00_ff
                        long    $ff_ff_ff_00
                        long    $ff_ff_ff_ff
Q                       res     1
palette                 res     1
temp                    res     1
l0ng                    res     1
pSpriteData             res     1
currentSpriteData       res     2               ' These three
pSpriteImageInfo        res     1               '   longs make up currentSpriteData
colorTable              res     16
activeSprites           res     1

                        res     4
linebuff                res     256/4           ' Assume max nPixels is 256
                        res     4
mask0                   res     1
endmask0                res     1
endmask1                res     1
spriteData              res     0
                        fit
                        