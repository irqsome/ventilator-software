{
        PlanetaryDefense_cd 0.01

        Collision detection
        - sprite/sprite
        - sprite/playfield
        Also, sprite animation in between collision detection duties.
}

CON
  nSprites      = 32
  sizeofSpriteData = 2 ' longs
  {
    word x
    word y
    long pImageInfo
  }
  {
    spriteImageInfo struct:
        byte xHot
        byte yHot
        byte w (multiple of 4, max: 28)
        byte h
        long    lineMask         ┐
        long    lineData[w/4]    ┴─ repeated h times
   
    Per-line transparency masks are precomputed.
  }
  nExplosions   = 32
  sizeofExplosionData = 2 ' longs
  {
    word x
    word y
    long stuff
  }

  sizeofObjectData = 5 ' longs
  {
    objectData struct:
        long timer
        long x                  ' 16.16
        long y                  ' 16.16
        long dx                 ' 16.16        
        long dy                 ' 16.16  
  }
  
  missileAsStart = 0
  nMaxMissileAs = 7
  missileAsEnd = missileAsStart + nMaxMissileAs - 1
  
  missileBsStart = missileAsEnd + 1            
  nMaxMissileBs = 4
  missileBsEnd = missileBsStart + nMaxMissileBs - 1
  
  bombsStart = missileBsEnd + 1
  nMaxBombs = 8
  bombsEnd = bombsStart + nMaxBombs - 1

  UFO = bombsEnd + 1
  
  nObjects = nMaxMissileAs + nMaxMissileBs + nMaxBombs + 1 ' UFO
  SATELLITE = nObjects+0
  CROSSHAIRS = nObjects+1
  nAllObjects = nObjects + 2

  nCoordPairs = 310             ' Must be a multiple of 5.
  
PUB Start( p ) | _i
  p += 10 * 4
  long[@pVclock] := p
  p += 5 * 4
  long[@pSpriteData] := p
  p += (1 + nSprites*sizeofSpriteData) << 2
'  long[@pButtons] := p
  p += 4
  long[@pScreenBase] := long[p]
  p += 4
'  long[@pExplosionData] := p
  p += (nExplosions * sizeofExplosionData) * 4
  long[@pObjectData] := p
  p += (nAllObjects * sizeofObjectData) * 4
'  long[@pExplosionCoords] := long[p]
  p += 4
'  long[@pFontBase] := long[p]
  p += 4
'  long[@pScannerLine] := p
  p += 4
  long[@pStatusData] := p
  p += 16*4
  repeat _i from 0 to 3
    long[@pSatelliteSpriteInfos][_i] := long[p]
    p += 4
  repeat _i from 0 to 3
    long[@pUfoSpriteInfos][_i] := long[p]
    p += 4


  long[@pIndex0] := @index0
  long[@pIndex1] := @index1
  long[@pCtrl] := @ctrl
  ctrl := 0
    
  cog :=  cognew(@CdEntry, 0)

PUB Stop
  cogstop( cog )

PUB SSCD( _i0, _i1 )
  index0 := _i0
  index1 := _i1
  ctrl := CTRL_SSCD
  repeat while ctrl > 0
  return ctrl

PUB SPCD( _i0 )
  index0 := _i0
  ctrl := CTRL_SPCD
  repeat while ctrl > 0
  return ctrl

VAR
  long  cog
  long  index0, index1
  long  ctrl      '-1 => collision detected (set by cog)
CON               ' 0 => no collision (set by cog)       
  CTRL_SSCD =       1'=> start SSCD (set by Spin)
  CTRL_SPCD =       2'=> start SPCD (set by Spin)
  
DAT
                        ORG 0
CdEntry
                        rdlong  temp, pVclock
                        cmp     prevClock, temp         wz
              if_nz     mov     prevClock, temp
              if_nz     call    #AnimateSprites
                        rdlong  temp, pCtrl
                        cmp     temp, #CTRL_SSCD        wz
              if_z      jmp     #SpriteToSprite
                        cmp     temp, #CTRL_SPCD        wz
              if_z      jmp     #SpriteToPlayfield
                        jmp     #CdEntry

DAT
SpriteToSprite                                          ' Basically, compute intersection (bitwise AND) of the
                                                        ' two sprites' masks. Non-zero intersection => collision.
                        rdlong  i0, pIndex0
                        rdlong  i1, pIndex1

                        mov     pObject0, pObjectData
                        mov     temp, i0                ' i0
                        shl     temp, #2                '  *
                        add     temp, i0                '   5 
                        shl     temp, #2                '    * 4
                        add     pObject0, temp          ' pObject0 = pObjectData + i0 * sizeofObjectData * 4
                        
                        mov     pSprite0, pSpriteData
                        mov     temp, i0                ' (i0
                        shl     temp, #1                '   *2
                        add     temp, #1                '     +1
                        shl     temp, #2                '       )*4
                        add     pSprite0, temp          ' pSprite0 = pSpriteData + (1 + i0 * sizeofSpriteData) * 4
:loop
                        rdword  temp, pObject0 wz       ' Get object[i0] timer.
              if_z      jmp     #:noCollision           ' If it's inactive there can be no collision.

                        rdlong  x0, pSprite0            ' Get sprite[i0] coordinates into (x0,y0) 
                        mov     y0, x0
                        shl     x0, #16
                        sar     x0, #16
                        sar     y0, #16

                        add     pSprite0, #4            ' Now look at sprite[i0]'s spriteInfo
                        rdlong  pSpriteInfo0, pSprite0
                        sub     pSprite0, #4
                        
                        rdlong  spriteInfo0, pSpriteInfo0
                        mov     temp, spriteInfo0       ' x0 -= spriteInfo.xHot
                        and     temp, #$ff
                        sub     x0, temp
                        mov     temp, spriteInfo0       ' y0 -= spriteInfo.yHot
                        shr     temp, #8
                        and     temp, #$ff
                        sub     y0, temp
                        mov     w0, spriteInfo0         ' w0 = spriteInfo.w
                        shr     w0, #16
                        and     w0, #$ff
                        mov     h0, spriteInfo0         ' h0 = spriteInfo.h
                        shr     h0, #24
                        and     h0, #$ff
                        
                        mov     pObject1, pObjectData
                        mov     temp, i1                ' i1
                        shl     temp, #2                '  *
                        add     temp, i1                '   5 
                        shl     temp, #2                '    * 4
                        add     pObject1, temp          ' pObject1 = pObjectData + i1 * sizeofObjectData * 4
                        
                        mov     pSprite1, pSpriteData
                        mov     temp, i1                ' (i1
                        shl     temp, #1                '   *2
                        add     temp, #1                '     +1
                        shl     temp, #2                '       )*4
                        add     pSprite1, temp           ' pSprite1 = pSpriteData + (1 + i1 * sizeofSpriteData) * 4

                        rdword  temp, pObject1  wz      ' Get object[i1] timer.
              if_z      jmp     #:noCollision           ' If it's inactive there can be no collision.

                        ' Object[i0] and object[i1] are both active here...
                        
                        rdlong  x1, pSprite1            ' Get sprite[i1] coordinates into (x1,y1)   
                        mov     y1, x1
                        shl     x1, #16
                        sar     x1, #16
                        sar     y1, #16

                        add     pSprite1, #4            ' Now look at sprite[i1]'s spriteInfo      
                        rdlong  pSpriteInfo1, pSprite1
                        sub     pSprite1, #4

                        rdlong  spriteInfo1, pSpriteInfo1
                        mov     temp, spriteInfo1       ' x1 -= spriteInfo.xHot
                        and     temp, #$ff
                        sub     x1, temp
                        mov     temp, spriteInfo1       ' y1 -= spriteInfo.yHot
                        shr     temp, #8
                        and     temp, #$ff
                        sub     y1, temp
                        mov     w1, spriteInfo1         ' w1 = spriteInfo.w
                        shr     w1, #16
                        and     w1, #$ff
                        mov     h1, spriteInfo1         ' h1 = spriteInfo.h
                        shr     h1, #24
                        and     h1, #$ff
                      
                        mov     yStart, y0
                        mins    yStart, y1              ' yStart is the greater of y0 and y1.

                        mov     yEnd, y0
                        add     yEnd, h0
                        mov     temp, y1
                        add     temp, h1
                        maxs    yEnd, temp              ' yEnd is the lesser of y0+h0 and y1+h1.
                        
                        mov     dy, yEnd
                        sub     dy, yStart

                        cmps    dy, #0          wc, wz
              if_be     jmp     #:noCollision           ' If yEnd-yStart <= 0, the sprites do not overlap vertically.

                        mov     dx, x1
                        sub     dx, x0
                        cmps    dx, #0          wc, wz
              if_a      mov     :shiftInstr, rshiftInstr ' Choose left or right shift. 
              if_be     mov     :shiftInstr, lshiftInstr
              if_be     neg     dx, dx
                        cmp     dx, #32         wc, wz
              if_ae     jmp     #:noCollision           ' If we have to shift by more than 31, they don't overlap horizontally.

                        shr     w0, #2                  ' Convert w0 to the stride through spriteInfo0
                        add     w0, #1
                        shl     w0, #2

                        shr     w1, #2                  ' Convert w1 to the stride through spriteInfo1
                        add     w1, #1
                        shl     w1, #2

                        add     pSpriteInfo0, #4        ' Point both pSpriteInfos to the first line 
                        add     pSpriteInfo1, #4        '  of their sprites' masks.

                        mov     mulA, yStart
                        sub     mulA, y0
                        mov     mulB, w0
                        call    #Mult
                        add     pSpriteInfo0, mulRes    ' pSpriteInfo0 points to the (yStart0-y0)th line of data.

                        mov     mulA, yStart
                        sub     mulA, y1
                        mov     mulB, w1
                        call    #Mult
                        add     pSpriteInfo1, mulRes    ' pSpriteInfo1 points to the (yStart1-y1)th line of data.
:intersectLoop
                        rdlong  spriteInfo0, pSpriteInfo0
:shiftInstr             shl     spriteInfo0, 0-0        ' This whole instruction will be replaced at runtime.
                        rdlong  spriteInfo1, pSpriteInfo1
                        test    spriteInfo1, spriteInfo0 wz
              if_nz     jmp     #:collision
                        add     pSpriteInfo0, w0        ' Advance both pointers to next line of
                        add     pSpriteInfo1, w1        '  their sprite's mask.
                        djnz    dy, #:intersectLoop
                                                        ' If we exit the loop, there was no intersection (collision).               
:noCollision
                        mov     temp, #0
                        wrlong  temp, pCtrl             ' Set result = 0
                        jmp     #CdEntry

:collision
                        neg     temp, #1
                        wrlong  temp, pCtrl             ' Set result = -1
                        jmp     #CdEntry

lshiftInstr             shl     spriteInfo0, dx
rshiftInstr             shr     spriteInfo0, dx

DAT
SpriteToPlayfield                                       ' Intersect (AND) the sprite's mask and the pixels of the playfield
                                                        ' (bitmap). Each playfield pixel is two bits; ignore the low-order bit.
                                                        ' In other words, check for collision against colors %10 (planet) and
                                                        ' %11 (explosion), but not against %00 (background) or %01 (score).
                                                        ' N.B. This code only works for sprites of width <= 16. 
                        rdlong  i0, pIndex0

                        mov     pObject0, pObjectData
                        mov     temp, i0                ' i0
                        shl     temp, #2                '  *
                        add     temp, i0                '   5 
                        shl     temp, #2                '    * 4
                        add     pObject0, temp          ' pObject0 = pObjectData + i0 * sizeofObjectData * 4
                        
                        mov     pSprite0, pSpriteData
                        mov     temp, i0                ' (i0
                        shl     temp, #1                '   *2
                        add     temp, #1                '     +1
                        shl     temp, #2                '       )*4
                        add     pSprite0, temp          ' pSprite0 = pSpriteData + (1 + i0 * sizeofSpriteData) * 4
:loop
                        rdword  temp, pObject0 wz       ' Get object[i0] timer.
              if_z      jmp     #:noCollision           ' If it's inactive there can be no collision.

                        rdlong  x0, pSprite0            ' Get sprite[i0] coordinates into (x0,y0) 
                        mov     y0, x0
                        shl     x0, #16
                        sar     x0, #16
                        sar     y0, #16

                        add     pSprite0, #4            ' Now look at sprite[i0]'s spriteInfo
                        rdlong  pSpriteInfo0, pSprite0
                        sub     pSprite0, #4
                        
                        rdlong  spriteInfo0, pSpriteInfo0
                        mov     temp, spriteInfo0       ' x0 -= spriteInfo.xHot
                        and     temp, #$ff
                        sub     x0, temp
                        mov     temp, spriteInfo0       ' y0 -= spriteInfo.yHot
                        shr     temp, #8
                        and     temp, #$ff
                        sub     y0, temp
                        mov     w0, spriteInfo0         ' w0 = spriteInfo.w
                        shr     w0, #16
                        and     w0, #$ff
                        mov     h0, spriteInfo0         ' h0 = spriteInfo.h
                        shr     h0, #24
                        and     h0, #$ff

                        mov     yStart, y0
                        mins    yStart, #0

                        mov     yEnd, y0
                        add     yEnd, h0
                        maxs    yEnd, #192

                        mov     dy, yEnd
                        sub     dy, yStart
                        cmps    dy, #0          wc, wz  
              if_be     jmp     #:noCollision           ' Vertical miss (sprite would have to be totally off-screen for this to happen).   

                        mov     temp, x0
                        add     temp, w0
                        cmps    temp, #0        wc, wz
              if_be     jmp     #:noCollision           ' Horizontal miss (sprite totally off-screen horizontally). 
                        cmps    x0, #256        wc, wz
              if_ae     jmp     #:noCollision           ' Ditto.             

                        mov     iStart, x0
                        sar     iStart, #4              ' iStart := x0 ~> 4

                        mov     shift, x0
                        and     shift, #$0f             ' shift := x0 & $0f                                                              

                        mov     iEnd, iStart
                        add     iEnd, #1                ' iEnd := iStart + 1
                                                                                       
                        mins    iStart, #0              ' iStart has a lower limit of 0.
                        maxs    iEnd, #15               ' iEnd has an upper limit of 15.

                        shr     w0, #2                  ' Convert w0 to the stride through spriteInfo0
                        add     w0, #1
                        shl     w0, #2

                        add     pSpriteInfo0, #4        ' Point pSpriteInfo0 to the first line of sprite mask. 

                        mov     mulA, yStart
                        sub     mulA, y0
                        mov     mulB, w0
                        call    #Mult
                        add     pSpriteInfo0, mulRes    ' pSpriteInfo0 points to the (yStart0-y0)th line of data.

                        mov     pScreen, yStart
                        shl     pScreen, #2
                        add     pScreen, pScreenBase    ' pScreen := pScreenBase + yStart*4

                        mov     mulA, columnOffset
                        mov     mulB, iStart
                        call    #Mult
                        add     pScreen, mulRes         ' Point pScreen to the correct column.

:outerLoop
                        rdlong  spriteInfo0, pSpriteInfo0
                        shl     spriteInfo0, shift

                        mov     pScreenSave, pScreen
                        mov     i, iEnd
                        sub     i, iStart
                        add     i, #1
:middleLoop
                        mov     spriteMask, #0

                        mov     j, #4
:innerLoop
                        mov     temp, spriteInfo0
                        and     temp, #$0f
                        add     temp, #spriteMasks
                        movs    :readSpriteMask, temp
                        nop
:readSpriteMask         or      spriteMask, 0-0         ' spriteMask |= spriteMasks[spriteInfo0 & $0f]
                        ror     spriteMask, #8          ' Take the low 16 bits of spriteInfo0
                        shr     spriteInfo0, #4         '  and double the bits to make 32 bits of spriteMask.

                        djnz    j, #:innerLoop

                        rdlong  temp, pScreen
                        test    temp, spriteMask        wz
              if_nz     jmp     #:collision
                        add     pScreen, columnOffset                        
                        djnz    i, #:middleLoop

                        add     pSpriteInfo0, w0
                        mov     pScreen, pScreenSave
                        add     pScreen, #4
                        
                        djnz    dy, #:outerLoop
                                                        ' If we exit the loop, there was no intersection (collision).               
:noCollision
                        mov     temp, #0
                        wrlong  temp, pCtrl             ' Set result = 0
                        jmp     #CdEntry

:collision
                        neg     temp, #1
                        wrlong  temp, pCtrl             ' Set result = -1
                        jmp     #CdEntry

spriteMasks
                        long    %00000000  
                        long    %00000010  
                        long    %00001000  
                        long    %00001010  
                        long    %00100000  
                        long    %00100010  
                        long    %00101000  
                        long    %00101010  
                        long    %10000000  
                        long    %10000010  
                        long    %10001000  
                        long    %10001010  
                        long    %10100000  
                        long    %10100010  
                        long    %10101000  
                        long    %10101010  
                                                        
i                       long    0
j                       long    0
iStart                  long    0
iEnd                    long    0
shift                   long    0
pCtrl                   long    0
pIndex0                 long    0
pIndex1                 long    0
i0                      long    0
i1                      long    0
pObjectData             long    0
pObject0                long    0
pObject1                long    0
pSpriteData             long    0
pSprite0                long    0
pSprite1                long    0
pSpriteInfo0            long    0
pSpriteInfo1            long    0
spriteInfo0             long    0
spriteInfo1             long    0
spriteMask              long    0
x0                      long    0
y0                      long    0
x1                      long    0
y1                      long    0
w0                      long    0
w1                      long    0
h0                      long    0
h1                      long    0
yStart                  long    0
yEnd                    long    0
dx                      long    0
dy                      long    0
pScreenBase             long    0
pScreen                 long    0
pScreenSave             long    0
columnOffset            long    192*4

DAT
'---------------------- Multiply routine
'                       32 x 32 => lower 32 bits.
'                       mulRes = mulA x mulB
'                       mulB should be less than mulA for better performance.
Mult                    mov     mulRes, #0
:_0                     shr     mulB, #1        wc, wz
              if_c      add     mulRes, mulA
                        shl     mulA, #1
              if_nz     jmp     #:_0
Mult_ret                ret

mulA                    long    0
mulB                    long    0
mulRes                  long    0

temp                    long    0

DAT
AnimateSprites
                        mov     sprite, #SATELLITE
                        mov     pSpriteInfo, #pSatelliteSpriteInfos
                        call    #_Animate
                        mov     sprite, #UFO
                        mov     pSpriteInfo, #pUfoSpriteInfos
                        call    #_Animate

AnimateSprites_ret      ret

_Animate
                        mov     temp, prevClock
                        shr     temp, #3
                        and     temp, #3
                        add     temp, pSpriteInfo
                        movs    :getSpriteInfo, temp
                        nop
:getSpriteInfo          mov     pSpriteInfo, 0-0
                        mov     pSprite, pSpriteData
                        add     pSprite, #4
                        shl     sprite, #3                ' * sizeofSpriteData * 4
                        add     pSprite, sprite
                        add     pSprite, #4
                        wrlong  pSpriteInfo, pSprite                        
_Animate_ret            ret

pStatusData             long    0
pVclock                 long    0
prevClock               long    0
sprite                  long    0
pSprite                 long    0
pSpriteInfo             long    0
pSatelliteSpriteInfos   long    0
                        long    0
                        long    0
                        long    0
pUfoSpriteInfos         long    0
                        long    0
                        long    0
                        long    0

                        fit     