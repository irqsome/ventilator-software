{{
        Planetary Defense 0.02
        by Michael Park

        Based on Tom Hudson's Planetary Defense for the Atari 8-bit
        (ANALOG Computing Magazine No. 17, March 1984)

        I, Michael Park, place the following files into the public domain:
        - PlanetaryDefense_main_001.spin
        - PlanetaryDefense_raster_002.spin
        - PlanetaryDefense_render_001.spin
        - PlanetaryDefense_cd_001.spin
        - PlanetaryDefense_sound_001.spin
}}
{
        2007-08-07 Forked blah012_tv, blah012_render, blah011_tv
        2007-08-07 Can draw planet, use mouse
        2007-08-08 Started "vbi" cog. Had to modify mouse driver. Vbi handles satellite and crosshairs
        2007-08-11 Got explosions working. With color randomizer.
        2007-08-12 Started object movement in vbi.
        2007-08-13 Missile/Playfield collision detection working.
        2007-08-14 Added "Planetary Defense" lettering.
        2007-08-14 Changing missiles to sprites. No collision detection.
        2007-08-16 Point/point collision detection working. Had to move explosion coordinates into hub.
        2007-08-18 Ran out of vbi cog space. Moving collision detection into main Spin program.
        2007-08-19 Sprite/sprite collision detection working, but slooow.
        2007-08-20 Sprite/playfield collision detection working. Now to speed things up...
        2007-08-21 Moved crosshairs/satellite/explosions into rasterizer. Vbi cog now free to become...
        2007-08-21 ...collision detector. Sprite/sprite in a cog now working.
        2007-08-21 And now so is sprite/playfield. Speed looks good.
        2007-08-23 Animated sprites. Starting to make levels.
        2007-08-25 In PD_Render, turned off the check for early exit out of sprite processing
                   because crosshairs sprite wasn't displaying during title screen.
        2007-08-26 Discovered that a partially off-screen sprite could collide with an explosion         
                   and cause an explosion on the other side of the screen. Added limiting to Explode().
                   Realized that scoring for missileB hits is going to be difficult if not impossible.
                   That is, there's no way to tell if a bomb has collided with a missileB explosion
                   rather than some other explosion. Oh well.
        2007-08-28 Spent way too much time tracking down a RES-related bug. Moved sprite animation
                   into collision detection cog. Smaller planet and orbit. Bombs in different orientations.
                   Started researching sound.
                   That off-screen explosion bug from 8/26? Also forgot to sign-extend.
        2007-08-29 Planet scanning. Bonus scoring.
        2007-09-01 Adding sound.
                   Running low on memory; had to reclaim renderer's DAT space as part of screen buffer.
        2007-09-02 Added UFO.                                      
        2007-09-03 Very tight on space. Made up some levels. Not entirely satisfied with UFO sprites.
                   Otherwise the game is pretty much ready for playtesting.
        2007-09-03 Finally squashed sound bug. There were some places where sfx.Nudge was not being called
                   for significant periods of time (during planet drawing and scanning) which put nextCnt
                   way behind, but since Nudge was being called 60 times a second by Delay and dCnt is 1/60s,
                   nextCnt never had a chance to catch up (but the sounds still got nudged on time). But
                   eventually the difference between cnt and nextCnt was too great and caused an overflow
                   and temporary loss of sound.
        2007-09-06 Version 0.02: found typo in PD_raster.
                   
                   My high score: 132470.                                                                            
                           
        Screen is 256 x 192. (0,0) is upper-left corner.
        Screen bitmap is laid out in column-major order, so it is compatible with graphics_drv.

        Cog usage:
        1 Spin interpreter
        1 rasterizer
        3 renderers
        1 collision detection
        1 mouse
        1 sound
       ---
        8 total
        
        "Playfield" colors:
        %00 - background (black)
        %01 - score/status
        %10 - planet (rasterizer changes color every scanline)
        %11 - explosions (rasterizer changes color every vblank)

        Everything else on the screen is sprites.

        We have objects (with game-specific data such as position, velocity, lifetime) and the
        objects have corresponding sprites (strictly visual assets).

        Flying Objects          Sprite
        Missiles (type A)       sprDot
        Missiles (type B)       sprDot and sprSmallX
        Bombs                   sprBombA_* (determined by PlanetaryDefense_cd)
        UFO                     sprUFO_* (determined by PlanetaryDefense_cd)                    

        Type A missiles are the "charged-particle projectiles" of classic Planetary Defense.
        Type B missiles are like Missile Command missiles.
        
}
CON

  _clkmode = xtal1 + pll16x     ' Enable external clock range 5-10MHz and pll times 8.
  _xinfreq = 5_000_000 + 0000   ' Set frequency to 10 MHz.
  _stack   = 50                 ' Need lots of stack for some reason!

  NRENDERERS = 3

OBJ
  rasterizer:           "PlanetaryDefense_raster_002"
  renderer[NRENDERERS]: "PlanetaryDefense_render_001"
  cd:                   "PlanetaryDefense_cd_001"
  sfx:                  "PLanetaryDefense_sound_001"
  
PUB Start | i

  nPixels := 256
  nLines := 192
  plocksPerPixel := 9
  horizontalOffset := -30
  verticalOffset := -6
  basePalette := $07_05_0c_02

  nRendererCogs := NRENDERERS
  bitmapBase := rasterizer.BufferAddress
  pScreen := bitmapBase
  pBuff0 := @buffer0
  pBuff1 := @buffer1
  pBuff2 := @buffer2
   
  pScreenBase := bitmapBase
  SetSpriteData( @spriteData )
  pExplosionCoords := @coords
  pFontBase := @font    
  pSatelliteSpriteInfos[0] := @sprSatellite_up
  pSatelliteSpriteInfos[1] := @sprSatellite_left
  pSatelliteSpriteInfos[2] := @sprSatellite_down
  pSatelliteSpriteInfos[3] := @sprSatellite_right
  pUfoSpriteInfos[0] := @sprUfo_0
  pUfoSpriteInfos[1] := @sprUfo_1
  pUfoSpriteInfos[2] := @sprUfo_2
  pUfoSpriteInfos[3] := @sprUfo_3

  scannerLine := 666
    
  rasterizer.Start( @vstatus )
  longfill( bitmapBase, 0, 256 * 192 / 4 / 4 )
   
  repeat i from 0 to nRendererCogs-1
    renderer[i].Start( @vstatus, i )

  cd.Start( @vstatus )

  sfx.Start

  SetSpriteImageInfo( CROSSHAIRS, @sprCrosshairs )
  SetSpriteActive( CROSSHAIRS, 1 )

  repeat i from 0 to 63
    byte[@statusData][i] := CHAR_SPACE

  DrawPlanet( PLANETRADIUS )

  repeat
    Game

PRI Game | i

  Title
  StatusPrint( 41, string("CLICK TO START") )
  
  repeat until buttons
    sfx.Nudge
  
  if buttons&2
    repeat i from 0 to SATELLITE
      SetSpriteActive( i, 0 )
    Credits
    repeat while buttons
      sfx.Nudge
    repeat until buttons
      sfx.Nudge
    longfill( bitmapBase, 0, 192 * 16 )
  repeat while buttons
    sfx.Nudge

  rand := cnt
    
  repeat i from 32 to 63
    byte[@statusData][i] := CHAR_SPACE

  repeat i from 0 to SATELLITE
    SetObjectTimer( i, 0 )
    SetSpriteActive( i, 0 )

  basePalette := $07_05_0c_02
  DrawPlanet( PLANETRADIUS )

  satelliteStatus := SATELLITE_OFF
  tmrSatelliteDead := 1
  tmrSatelliteInvulnerable := -1
  
  nSatellites := 5
  satelliteBonusScore := SCORE_SATELLITEBONUS
  
  score := 0
  level := 0
  gameOver := false
  repeat
    InitLevel
    ++level
    PlayLevel
    if gameOver
      quit
    BonusScores

  nSatellites := 0
  nMissileBs := 0
  UpdateStatusDisplay

  Delay( 180 )

  repeat i from 32 to 63
    byte[@statusData][i] := CHAR_SPACE
    
VAR
  long  level
  long  score
  long  nSatellites
  long  nMissileBs  
  long  nBombs
  long  nBombsInFlight
  long  velBombMin
  long  velBombMax
  long  ufoProbability
  long  scoreMultiplier
  long  tmrSatelliteDead, tmrSatelliteInvulnerable, satelliteStatus
  long  satelliteBonusScore
  long  ufoCanFire
  long  gameOver
  long  vclockPrev
  long  bitmapBase
    
PRI InitLevel | p, c
  nMissileBs := 20

  p := @levelData + (level // nDefinedLevels) * sizeofLevelData * 4
  nBombs := long[p][0]
  nBombsInFlight := long[p][1]
  velBombMin := long[p][2] + level / nDefinedLevels
  velBombMax := long[p][3] + level / nDefinedLevels
  ufoProbability := long[p][4]
  scoreMultiplier := ((level >> 1) + 1) <# 10
    
  c := (level & $0f) << 12      ' c is chroma of colors 1
  c |= c << 8                   '  and 2.                                             
  basePalette := c | $0c02      ' Color 0 is black, color 3 tbd.
  
DAT
levelData     '  Bombs     Bombs      Bomb Velocity      Ufo     
              '          in flight    Min       Max      Prob.   
              '---------|---------|---------|---------|---------|
        long      10,       2,        2,        3,        0                     ' 1     
        long      20,       4,        2,        3,        5                     ' 2
        long      25,       4,        4,        6,        5                     ' 3
        long      20,       4,        6,        8,        5                     ' 4
        long      12,       1,        9,       11,        5                     ' 5
        long      10,    nMaxBombs,   3,        5,       10                     ' 6
        long      30,       4,        4,        6,       10                     ' 7
        long      30,       6,        5,        7,       15                     ' 8
        long      20,    nMaxBombs,   5,        7,       20                    '  9
        long      40,    nMaxBombs,   5,        7,       20                    ' 10
        
CON
  nDefinedLevels = 10
  sizeofLevelData = 5 ' longs

PRI Delay( n )
  repeat n
    repeat
      sfx.Nudge
    while vclockPrev == vclock
    vclockPrev := vclock
            
PRI PlayLevel | i, s, buttonsNow, buttonsDiff, continue, x4, ufoActive, tmrUfoShotClock

  repeat i from missileAsStart to missileBsEnd
    SetSpriteImageInfo( i, @sprDot )
  repeat i from 0 to nMaxMissileBs-1
    SetSpriteImageInfo( nAllObjects+i, @sprSmallX )
  ufoActive := false
  ufoCanFire := false
  tmrUfoShotClock := -1

  UpdateStatusDisplay

  StatusPrint( 38, string("LEVEL") )
  i := @statusData + 38 + 6
  i := StatusNumber( i, level )
  StatusPrint( i - @statusData, string(":") )
  i := StatusNumber( i+2, scoreMultiplier )
  StatusPrint( i - @statusData, string(" X POINTS") )
  
  byte[@statusData][i++] := CHAR_0 + level / 10
  byte[@statusData][i++] := CHAR_0 + level // 10

  sfx.Siren
  
  Delay( 120 )

  repeat i from 32 to 63
    byte[@statusData][i] := CHAR_SPACE    

  buttonsPrev := 0
  x4 := false
  
  repeat
    Delay( 1 )
    
    if Decrement(@tmrSatelliteDead) == 0
      if nSatellites
        --nSatellites   
        satelliteStatus := SATELLITE_FLASHING
        tmrSatelliteInvulnerable := 120
      else
        x4 := true
        nBombs := -1
        nBombsInFlight := nMaxBombs
        ufoProbability := 0
        
    if Decrement(@tmrSatelliteInvulnerable) == 0
      satelliteStatus := SATELLITE_ON
      SetSpriteActive( SATELLITE, 1 )
      SetObjectTimer( SATELLITE, -1 )

    if satelliteStatus == SATELLITE_FLASHING
      SetSpriteActive( SATELLITE, (vclock>>1)&1 )

    if satelliteStatus   
      FireMissiles

    if Decrement(@tmrUfoShotClock) == 0
      ufoCanFire := true
      tmrUfoShotClock := 120
                                                             
    MoveObjects
    if x4
      repeat 3
        sfx.Nudge
        MoveObjects
        
    UfoCheckBounds
            
    CollisionDetection

    if ufoActive and GetObjectTimer( UFO ) == 0
      ufoActive := false
      tmrUfoShotClock := -1
      ufoCanFire := false
      sfx.UfoOff
    
    if GetSpriteActive( SATELLITE ) == 0 and satelliteStatus == SATELLITE_ON
      tmrSatelliteDead := 100
      satelliteStatus := SATELLITE_OFF
      SetObjectTimer( SATELLITE, 0 )
      SetSpriteActive( SATELLITE, 0 )

    if nBombs
      repeat i from bombsStart to bombsStart+nBombsInFlight-1
        if GetObjectTimer( i ) == 0
          if GetObjectTimer( UFO ) == 0 and Randi( 100 ) < ufoProbability
            UfoInit
            ufoActive := true
            tmrUfoShotClock := 120
            sfx.UfoOn
          else
            BombInit( i )
            --nBombs
          quit

    if ufoCanFire
      UfoFire

    CheckGameOver      
    x4 or= gameOver
    
    UpdateStatusDisplay

    continue := satelliteStatus == SATELLITE_FLASHING
    repeat i from missileAsStart to UFO
      if GetObjectTimer( i )
        continue := true
        quit
  while continue

  repeat 70
    Delay( 1 )                  ' Gotta keep checking even after all objects are dead   
    CheckGameOver               '  because an explosion might still be in progress.
    
PRI CheckGameOver
  if GetPixel( 128, 96 ) <> 3
    gameOver := true
    nBombs := 0
    StatusPrint( 44, string("GAME OVER") )
    tmrSatelliteDead := -1
    satelliteStatus := SATELLITE_OFF
    SetObjectTimer( SATELLITE, 0 )
    SetSpriteActive( SATELLITE, 0 )
    
CON
  SATELLITE_OFF      = 0
  SATELLITE_ON       = 1
  SATELLITE_FLASHING = 2

  SCORE_SATELLITEBONUS          = 10000                 ' New satellite when score reaches a multiple of this.
  SCORE_BOMB                    = 10                    ' Points for a missile hitting a bomb.
  SCORE_UFO                     = 20                    ' Points for a missile hitting a UFO.
  SCORE_MISSILE                 = 10                    ' Bonus points for saving a missile.
  SCORE_PLANET                  = 1000                  ' Bonus points for area of planet saved.
    
PRI Decrement( p )
  long[p] := (long[p] - 1) #> -1
  return long[p]

PRI AddToScore( points )
  score += points * scoreMultiplier
  if score => satelliteBonusScore
    ++nSatellites
    satelliteBonusScore += SCORE_SATELLITEBONUS
    sfx.Tahdah
  UpdateStatusDisplay
  
PRI UpdateStatusDisplay | i, n, p
  p := @statusData
  repeat i from 0 to 9
    if i + 1 =< nSatellites
      byte[p] := CHAR_SATELLITE
    else
      byte[p] := CHAR_SPACE
    ++p
    
  n := score
  p := @statusData + 18
  repeat i from 0 to 5
    byte[p] := CHAR_0 + n // 10
    n /= 10
    --p

  n := nMissileBs
  p := @statusData + 27
  repeat 5
    i := n <# 4
    byte[p++] := CHAR_B0 + i
    n -= i

PRI BonusScores | x, y, n, t, p, s
  Delay( 120 )

  t := 0
  if nMissileBs
    PlotString( string("MISSILE BONUS POINTS"), 6, 145 )
    Delay( 60 )
    repeat while nMissileBs
      --nMissileBs
      UpdateStatusDisplay
      sfx.Thup
      t += SCORE_MISSILE
      StatusNumberCentered( @statusData + 48, t * scoreMultiplier )       
      Delay( 6 )
      
  Delay( 60 )

  AddToScore( t )
  bytefill( @statusData + 32, CHAR_SPACE, 32 )
  PlotString( string("                    "), 6, 145 )
  Delay( 30 )
                                                    
  n := 0
  PlotString( string("PLANET BONUS POINTS"), 6, 145 )
  Delay( 60 )
  repeat y from 96-PLANETRADIUS-10 to 95+PLANETRADIUS+10
    scannerLine := y
    s := 0
    repeat x from 127-PLANETRADIUS to 128+PLANETRADIUS
      if GetPixel(x,y)
        ++n
        t := n * SCORE_PLANET / PLANETAREA
        p := StatusNumberCentered( @statusData + 48, t * scoreMultiplier )
        s := 1
    sfx.Scanner( s )
    sfx.Nudge
    
  scannerLine := 666
  Delay( 60 )

  AddToScore( t )
  bytefill( @statusData + 32, CHAR_SPACE, 32 )
  PlotString( string("                    "), 6, 145 )
  Delay( 100 )

PRI StatusNumberD( p, n, d )
  p += d
  repeat d
    byte[--p] := CHAR_0 + n // 10
    n /= 10
  return p + d

PRI StatusNumber( p, n ) | d, t
  d := 1
  t := 10
  repeat while n => t
    t *= 10
    ++d
  return StatusNumberD( p, n, d )
  
PRI StatusNumberCentered( p, n ) | d, t
  d := 1
  t := 10
  repeat while n => t
    t *= 10
    ++d
  return StatusNumberD( p - d/2, n, d )
  
VAR
  long  buttonsPrev
  
PRI FireMissiles | buttonsNow, buttonsDiff, i, s, x0, y0, x1, y1, vx, vy, r
  buttonsNow := buttons
  buttonsDiff := buttonsNow ^ buttonsPrev
  buttonsPrev := buttonsNow
  buttonsNow &= buttonsDiff
  if buttonsNow
    GetSpritePos( SATELLITE, @x0, @y0 )
    GetSpritePos( CROSSHAIRS, @x1, @y1 )
    if buttonsNow & 1
      ComputeVector( velMissileA, x0, y0, x1, y1, @vx, @vy, @r )
      if r 
        repeat i from missileAsStart to missileAsEnd - 1
          if GetObjectTimer( i ) == 0
            SetObjectPos( i, x0<<16, y0<<16 )
            SetObjectVel( i, vx, vy )
            SetObjectTimer( i, -1 ) ' Infinite life.
            SetObjectMisc( i, 0 )
            SetSpriteActive( i, 1 )
            sfx.FireA
            quit
    else
      if nMissileBs == 0
        sfx.Nofire
        return
      ComputeVector( velMissileB, x0, y0, x1, y1, @vx, @vy, @r )
      if r 
        repeat i from missileBsStart to missileBsEnd
          if GetObjectTimer( i ) == 0
            --nMissileBs
            SetObjectPos( i, x0<<16, y0<<16 )
            SetObjectVel( i, vx, vy )
            SetObjectTimer( i, r )
            SetObjectMisc( i, 0 )
            SetSpriteActive( i, 1 )
            s := i - nMaxMissileAs + nAllObjects
            SetSpritePos( s, x1, y1 )
            SetSpriteActive( s, 1 )
            sfx.FireB
            quit

PRI ComputeVector( vel, x0, y0, x1, y1, pVx, pVy, pR ) | vx, vy, r
  vx := x1 - x0
  vy := y1 - y0
  if (||vx < 2) and (||vy < 2)
    long[pR] := 0
    return
  r := ^^( vx*vx + vy*vy ) << 5
  vx <<= 16
  vy <<= 16
  vx *= vel
  vx /= r
  vy *= vel
  vy /= r
  r += (vel>>1)
  r /= vel
  long[pVx] := vx
  long[pVy] := vy
  long[pR] := r
     
PRI MoveObjects | i, p, x, y  
  p := @objectData
  repeat i from 0 to nObjects-1
    if word[p][0] 
      x := (long[p][1] += long[p][3]) ~> 16
      y := (long[p][2] += long[p][4]) ~> 16
      
      if (i =< missileAsEnd) and ((x < 0) or (x > 255) or (y < 0) or (y > 191))
        ' Out of bounds. Applies only to missileAs. Other objects have on-screen end-of-life points
        ' (besides, some objects start out off-screen).
        word[p][0] := 0
        SetSpriteActive( i, 0 )
      elseif --word[p][0]                               ' Still alive.
        SetSpritePos( i, x, y )
      else                                              ' Expired.
        SetSpriteActive( i, 0 )
        if missileBsStart =< i and i =< missileBsEnd    ' For missileBs, turn off corresponding target sprite.
          SetSpriteActive( i-missileBsStart+nAllObjects, 0 )
          Explode( i-missileBsStart+nAllObjects )       ' Also use the target's position for the explosion
        else 
          Explode( i )
    p += sizeofObjectData << 2
    
PRI CollisionDetection | i, j, x, y
{
        Sprite-to-sprite collision matrix
        +----------------------------------------------------------------------------+
        ¦          ¦ missileA ¦ missileB ¦   bomb   ¦   UFO    ¦crosshairs¦satellite ¦
        +----------+----------+----------+----------+----------+----------+----------¦     
        ¦ missileA ¦    Y[1]  ¦    Y[1]  ¦    Y     ¦    Y[2]  ¦    N     ¦    N     ¦ 
        +----------+----------+----------+----------+----------+----------+----------¦     
        ¦ missileB ¦    -     ¦    Y[1]  ¦    Y     ¦    Y     ¦    N     ¦    N     ¦ 
        +----------+----------+----------+----------+----------+----------+----------¦     
        ¦   bomb   ¦    -     ¦    -     ¦    N     ¦    N     ¦    N     ¦    Y     ¦ 
        +----------+----------+----------+----------+----------+----------+----------¦     
        ¦   UFO    ¦    -     ¦    -     ¦    -     ¦    N     ¦    N     ¦    Y     ¦ 
        +----------+----------+----------+----------+----------+----------+----------¦     
        ¦crosshairs¦    -     ¦    -     ¦    -     ¦    -     ¦    N     ¦    N     ¦ 
        +----------+----------+----------+----------+----------+----------+----------¦     
        ¦satellite ¦    -     ¦    -     ¦    -     ¦    -     ¦    -     ¦    N     ¦ 
        +----------------------------------------------------------------------------+
        [1] Could use point/point collision detection if such were available.
        [2] Collisions between a UFO and the missileA that it fired should not count.
            missileAsEnd is reserved for UFO.
}
  repeat i from missileAsStart to missileBsEnd-1        ' Handle missile-to-missile collisions.
    repeat j from i+1 to missileBsEnd                   ' (Unlikely but possible.)
      if cd.SSCD( i, j )
        Deactivate( i )
        Deactivate( j )
        Explode( i )
        quit
        
  repeat i from missileAsStart to missileBsEnd          ' Missile-to-enemy-object collisions.
    repeat j from bombsStart to UFO
      if i == missileAsEnd and j == UFO                 ' UFO's missile 
        next
      if cd.SSCD( i, j )
        Deactivate( i )
        Deactivate( j )
        Explode( i )
        Explode( j )
        if i == missileAsEnd                            ' UFO's missile should not earn points.
          quit
        if j < UFO
          AddToScore( SCORE_BOMB )
        else
          AddToScore( SCORE_UFO )
        quit
            
  repeat i from bombsStart to UFO                       ' Enemy-object-to-satellite collisions.
    if cd.SSCD( i, SATELLITE )
      Deactivate( i )
      Deactivate( SATELLITE )
      Explode( i )
      Explode( SATELLITE )
      if i == UFO
        AddToScore( SCORE_UFO )
      else
        AddToScore( SCORE_BOMB )
      quit

  if cd.SSCD( missileAsEnd, SATELLITE )                 ' UFO's-missile-to-satellite collision.
    Deactivate( missileAsEnd )
    Deactivate( SATELLITE )
    Explode( missileAsEnd )
    Explode( SATELLITE )
   
  repeat i from missileAsStart to UFO
    if cd.SPCD( i )
      Deactivate( i )
      Explode( i )

PRI Deactivate( i )
  SetObjectTimer( i, 0 )
  SetSpriteActive( i, 0 )
  if missileBsStart =< i and i =< missileBsEnd          ' For missileBs, turn off corresponding target sprite.
    SetSpriteActive( i-missileBsStart+nAllObjects, 0 )

PRI Explode( i ) | x, y    
  GetSpritePos( i, @x, @y )
  x := 0 #> x <# 255
  y := 0 #> y <# 191
  StartExplosion( x, y )
  sfx.Boom
  
PRI BombInit( i ) | x0, y0, x1, y1, vx, vy, r
  if Randi(2) & 1
    if Randi(2) & 1
      x0 := 270
    else
      x0 := -15
    y0 := Randi( 192 )
  else
    if Randi(2) & 1
      y0 := 210
    else
      y0 := -15
    x0 := Randi( 256 )
  x1 := 128
  y1 := 96
  ComputeVector( velBombMin + Randi(velBombMax-velBombMin+1), x0, y0, x1, y1, @vx, @vy, @r )
  SetObjectPos( i, x0<<16, y0<<16 )
  SpriteInit( i, 1, x0, y0, ChooseBombSprite( vx, vy ) )
  SetObjectVel( i, vx, vy )
  SetObjectTimer( i, r )

PRI UfoInit | x0, y0, x1, y1, vx, vy, r      
  if Randi(2) & 1                                       ' UFO starts 8 pixels outside screen.
    if Randi(2) & 1                                     ' (UFO expires once it's more than 8 pixels offscreen.) 
      x0 := 263
      x1 := -666
    else
      x0 := -8
      x1 := 666
    y0 := y1 := Randi( 192 )
  else
    if Randi(2) & 1
      y0 := 199
      y1 := -666
    else
      y0 := -8
      y1 := 666
    x0 := x1 := Randi( 256 )
  ComputeVector( velBombMin + Randi(velBombMax-velBombMin+1), x0, y0, x1, y1, @vx, @vy, @r )
  SetObjectPos( UFO, x0<<16, y0<<16 )
  SetSpritePos( UFO, x0, y0 )
  SetSpriteActive( UFO, 1 )
  SetObjectVel( UFO, vx, vy )
  SetObjectTimer( UFO, r )

PRI UfoFire | x, y, vx, vy, r
  if GetObjectTimer( UFO ) == 0
    return
  GetSpritePos( UFO, @x, @y )
  if x < 0 or x > 255 or y < 0 or y > 191
    return
  if GetSpriteActive( missileAsEnd ) == 0
    ComputeVector( velMissileA, x, y, 128 - 40 + Randi(80), 96 - 40 + Randi(80), @vx, @vy, @r )
    SetObjectPos( missileAsEnd, x<<16, y<<16 )
    SetSpriteActive( missileAsEnd, 1 )
    SetObjectVel( missileAsEnd, vx, vy )
    SetObjectTimer( missileAsEnd, -1 )
    sfx.FireA
    ufoCanFire := false

PRI UfoCheckBounds | x, y
  if GetObjectTimer( UFO ) == 0
    return
  GetSpritePos( UFO, @x, @y )
  if x < -8 or x > 263 or y < -8 or y > 199
    Deactivate( UFO )

PRI ChooseBombSprite( vx, vy ) | ax, ay, q, a
{
A primitive "arctangent" calculation.
Reduces vx and vy to one of 8 directions, then returns the corresponding bomb sprite.

        4
        ¦       
  5     ¦      3
        ¦        
       2¦1
6 ------+-----? x 2
       3¦0
        ¦             x  y  q
  7     ?      1      +  +  0  0 + 012 -> 012
        y             +  -  1  4 - 012 => 4 3 2
        0             -  -  2  4 + 012 => 4 5 6
                      -  +  3  0 - 012 => 0 7 6
}
  if vy < 0
    q := 1
  else
    q := 0
    
  if vx < 0
    q := 3 - q
    
  ax := || vx
  ay := || vy
  if ay > ax + ax
    a := 0
  elseif ax > ay + ay
    a := 2
  else
    a := 1

  case q
'   0: do nothing
    1: a := 4 - a
    2: a := 4 + a
    3: a := (-a) & 7

  case a
    0: return @sprBombA_180     ' The angle a {0..7} is    
    1: return @sprBombA_135     ' the opposite of the compass     
    2: return @sprBombA_90      ' directions used to label     
    3: return @sprBombA_45      ' the various bomb sprites.                                                      
    4: return @sprBombA_0     
    5: return @sprBombA_315     
    6: return @sprBombA_270     
    7: return @sprBombA_225     
           
CON
  velMissileA = 55
  velMissileB = 90
  PLANETRADIUS = 40
  PLANETAREA = 5184
  
PRI DrawPlanet( rad ) | x, y
  repeat y from 0 to rad-1
    x := ^^( rad*rad - y*y )
    repeat while x => 0
      Plot4( x--, y )
    sfx.Nudge
  Plot( 128, 96, 3 )
  
PRI Plot4( x, y )
  Plot( 128+x, 96+y, 2 )
  Plot( 128+x, 95-y, 2 )
  Plot( 127-x, 96+y, 2 )
  Plot( 127-x, 95-y, 2 )

PRI Plot( x, y, c ) | p
  p := (y + ((x >> 4) * 12 << 4)) * 4 + bitmapBase
  long[p] &= !(3 << ( (x & 15) << 1))
  long[p] |= c << ( (x & 15) << 1)

PRI GetPixel( x, y ) | p
  p := (y + ((x >> 4) * 12 << 4)) * 4 + bitmapBase
  return (long[p] >> ( (x & 15) << 1)) & 3

PRI Title | i, j, s

  i := 68
  j := 0
  s := 88
  SpriteInit( j++, 1, i, s, @sprP )
  i += 15
  SpriteInit( j++, 1, i, s, @sprL )
  i += 15
  SpriteInit( j++, 1, i, s, @sprA )
  i += 15
  SpriteInit( j++, 1, i, s, @sprN )
  i += 15
  SpriteInit( j++, 1, i, s, @sprE )
  i += 15
  SpriteInit( j++, 1, i, s, @sprT )
  i += 15
  SpriteInit( j++, 1, i, s, @sprA )
  i += 15
  SpriteInit( j++, 1, i, s, @sprR )
  i += 15
  SpriteInit( j++, 1, i, s, @sprY )

  i := 76
  s := 110
  SpriteInit( j++, 1, i, s, @sprD )
  i += 17
  SpriteInit( j++, 1, i, s, @sprE )
  i += 17     
  SpriteInit( j++, 1, i, s, @sprF )
  i += 17     
  SpriteInit( j++, 1, i, s, @sprE )
  i += 17
  SpriteInit( j++, 1, i, s, @sprN )
  i += 17
  SpriteInit( j++, 1, i, s, @sprS )
  i += 17
  SpriteInit( j++, 1, i, s, @sprE )

PRI StatusPrint( i, s ) | ch
  repeat while ch := byte[s++]
    byte[@statusData][i++] := ConvertChar(ch)
    
PRI ConvertChar( ch )
' From ASCII to my little character set.
  case ch
    "A".."Z": return CHAR_A + ch - "A"
    "0".."9": return CHAR_0 + ch - "0"
    " ":      return CHAR_SPACE
    ".":      return CHAR_DOT
    "-":      return CHAR_HYPHEN
    "%":      return CHAR_PERCENT
    ":":      return CHAR_COLON
  return CHAR_DOT

PRI Credits | p, i, y
  p := @creditStrings
  repeat while y := byte[p++]
    i := (32 - strsize( p )) >> 1
    p := PlotString( p, i, y )
    
PRI PlotString( p, i, y ) | ch
  repeat while ch := byte[p++]
    PlotChar( ch, i++, y )
  return p

PRI PlotChar( ch, i, y ) | p, q, w, b
  p := bitmapBase + y * 4 + (i>>1) * 192 * 4 + (i&1) * 2
  q := @font + ConvertChar(ch) << 3

  word[p] &= !$ffff
  p += 4
  repeat 8
    b := byte[q++]
    repeat 8
      w <<= 2
      if b & $80
        w |= %01
      b <<= 1
    word[p] &= !$ffff
    word[p] |= w
    p += 4
  word[p] &= !$ffff
     
VAR
  long rand

PRI Randi(n)
  return (?rand & $7fffffff) // n

VAR
  long  vstatus                 ' vstatus<31..1> is line#, vstatus<0> = 0 => reading params from hub
                                '                                     = 1 => rendering (hub params are free to be set up for line#+1
  long  nRendererCogs           ' 1
  long  palette                 ' 2
  long  pScreen                 ' 3
  long  pSpriteData             ' 4
  long  nPixels                 ' 5
  long  nLines                  ' 6
  long  plocksPerPixel          ' 7
  long  horizontalOffset        ' 8
  long  verticalOffset          ' 9
  long  vclock                  ' 10
  long  pBuff0                  ' 11
  long  pBuff1                  ' 12
  long  pBuff2                  ' 13
  long  basePalette             ' 14
VAR                             ' 15 and on...
  long  spriteData[ 1 + nSprites * sizeofSpriteData ] ' Active-sprite bitmap followed by nSprites data
  long  buttons
  long  pScreenBase
  long  explosionData[ nExplosions * sizeofExplosionData ]
  long  objectData[ nAllObjects * sizeofObjectData ]
  long  pExplosionCoords
  long  pFontBase
  long  scannerLine
  long  statusData[16] ' Actually 64 bytes, but declared long to keep it together with the other longs here.
  long  pSatelliteSpriteInfos[4]
  long  pUfoSpriteInfos[4]
  
  long  buffer0[256/4], buffer1[256/4], buffer2[256/4]

CON
  nSprites      = 32
  sizeofSpriteData = 2 ' longs
  {
    spriteData struct:
        word x
        word y
        long pImageInfo
  }
  {
    spriteImageInfo struct:
        byte xHot
        byte yHot
        byte w (multiple of 4, max: 28)
        byte h
        long    lineMask         +
        long    lineData[w/4]    -- repeated h times
   
    Per-line transparency masks are precomputed.
  }
  nExplosions   = 32
  sizeofExplosionData = 2 ' longs
  {
    explosionData struct:
        long stuff
        word x
        word y
  }

  sizeofObjectData = 5 ' longs
  {
    objectData struct:
        word timer
        word misc
        long x                  ' 16.16
        long y                  ' 16.16
        long dx                 ' 16.16        
        long dy                 ' 16.16  
  }
  
  missileAsStart = 0
  nMaxMissileAs = 7
  missileAsEnd = missileAsStart + nMaxMissileAs - 1
  
  missileBsStart = missileAsEnd + 1            
  nMaxMissileBs = 4
  missileBsEnd = missileBsStart + nMaxMissileBs - 1
  
  bombsStart = missileBsEnd + 1
  nMaxBombs = 8
  bombsEnd = bombsStart + nMaxBombs - 1

  UFO = bombsEnd + 1
  
  nObjects = nMaxMissileAs + nMaxMissileBs + nMaxBombs + 1 ' UFO
  SATELLITE = nObjects + 0
  CROSSHAIRS = nObjects + 1
  nAllObjects = nObjects + 2

PUB GetObjectTimer( object ) | p
  p := @objectData + object * sizeofObjectData * 4
  return word[p][0]

PUB SetObjectTimer( object, t ) | p
  p := @objectData + object * sizeofObjectData * 4
  word[p][0] := t

PUB SetObjectMisc( object, t ) | p
  p := @objectData + object * sizeofObjectData * 4
  word[p][1] := t

PUB SetObjectPos( object, x, y ) | p
  p := @objectData + object * sizeofObjectData * 4
  long[p][1] := x
  long[p][2] := y
        
PUB SetObjectVel( object, dx, dy ) | p
  p := @objectData + object * sizeofObjectData * 4
  long[p][3] := dx
  long[p][4] := dy
        
PUB StartExplosion( x, y ) | i, p
  p := @explosionData
  repeat i from 0 to nExplosions-1
    if long[p] == 0
      word[p][2] := x
      word[p][3] := y
      long[p] := 1
      return
    p += sizeofExplosionData * 4

PUB InitSprites
  long[pSpriteData] := 0

PUB FindInactiveSprite | i
  repeat i from 0 to nSprites-1
    if (long[pSpriteData] & (|< i)) == 0
      return i
  return -1

PUB SetSpriteActive( sprite, active ) | b
' active = 0 or 1
  if active
    long[pSpriteData] |= |< sprite
  else
    long[pSpriteData] &= !|< sprite

PUB GetSpriteActive( sprite )
  return (long[pSpriteData] >> sprite) & 1
  
PUB SetSpritePos( sprite, x, y ) | p
  p := pSpriteData + 4 + sprite * sizeofSpriteData * 4
  word[p][0] := x
  word[p][1] := y

PUB GetSpritePos( sprite, px, py ) | p
  p := pSpriteData + 4 + sprite * sizeofSpriteData * 4
  long[px] := ~~word[p][0]
  long[py] := ~~word[p][1]
  
PUB SetSpriteImageInfo( sprite, pSpriteImageInfo ) | p
  p := pSpriteData + 4 + sprite * sizeofSpriteData * 4
  long[p][1] := pSpriteImageInfo

PUB SpriteInit( sprite, active, x, y, pSpriteImageInfo )
  SetSpriteActive( sprite, active )
  SetSpritePos( sprite, x, y )
  SetSpriteImageInfo( sprite, pSpriteImageInfo )

PUB SetSpriteData( p )
   pSpriteData := p
   
DAT
sprCrosshairs
        byte  4, 4, 12, 9
        long  %0000_0000_0011_1000, $02_00_00_00, $00_00_02_07, $00_00_00_00
        long  %0000_0000_0011_1000, $02_00_00_00, $00_00_02_07, $00_00_00_00
        long  %0000_0000_0011_1000, $02_00_00_00, $00_00_02_07, $00_00_00_00
        long  %0000_0001_1111_1111, $02_02_02_02, $02_02_02_07, $00_00_00_02
        long  %0000_0001_1111_1111, $07_07_07_07, $07_07_07_02, $00_00_00_07
        long  %0000_0001_1111_1111, $02_02_02_02, $02_02_02_07, $00_00_00_02
        long  %0000_0000_0011_1000, $02_00_00_00, $00_00_02_07, $00_00_00_00
        long  %0000_0000_0011_1000, $02_00_00_00, $00_00_02_07, $00_00_00_00
        long  %0000_0000_0011_1000, $02_00_00_00, $00_00_02_07, $00_00_00_00
sprSatellite_up
        byte  5, 6, 12, 12
        long  %0000_0000_0000_0000_0000_0000_0111_0000, $00_00_00_00, $00_04_04_04, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0001_1111_1100, $07_06_00_00, $07_07_07_07, $00_00_00_06
        long  %0000_0000_0000_0000_0000_0001_0010_0100, $00_07_00_00, $00_00_07_00, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0000_0010_0000, $00_00_00_00, $00_00_07_00, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0010_0000, $00_00_00_00, $00_00_07_00, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_0000, $00_00_00_00, $00_07_07_07, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0100_1111_1001, $07_00_00_06, $07_07_07_07, $00_06_00_00
        long  %0000_0000_0000_0000_0000_0100_0111_0001, $00_00_00_07, $00_07_07_07, $00_07_00_00
        long  %0000_0000_0000_0000_0000_0100_1010_1001, $07_00_00_07, $07_00_07_00, $00_07_00_00
        long  %0000_0000_0000_0000_0000_0111_0000_0111, $00_07_07_06, $00_00_00_00, $00_06_07_07
        long  %0000_0000_0000_0000_0000_0011_0000_0110, $00_07_07_00, $00_00_00_00, $00_00_07_07
        long  %0000_0000_0000_0000_0000_0001_1000_1100, $07_07_00_00, $07_00_00_00, $00_00_00_07
sprSatellite_down
        byte  5, 5, 12, 12
        long  %0000_0000_0000_0000_0000_0001_1000_1100, $07_07_00_00, $07_00_00_00, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0011_0000_0110, $00_07_07_00, $00_00_00_00, $00_00_07_07
        long  %0000_0000_0000_0000_0000_0111_0000_0111, $00_07_07_06, $00_00_00_00, $00_06_07_07
        long  %0000_0000_0000_0000_0000_0100_1010_1001, $07_00_00_07, $07_00_07_00, $00_07_00_00
        long  %0000_0000_0000_0000_0000_0100_0111_0001, $00_00_00_07, $00_07_07_07, $00_07_00_00
        long  %0000_0000_0000_0000_0000_0100_1111_1001, $07_00_00_06, $07_07_07_07, $00_06_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_0000, $00_00_00_00, $00_07_07_07, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0010_0000, $00_00_00_00, $00_00_07_00, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0010_0000, $00_00_00_00, $00_00_07_00, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0001_0010_0100, $00_07_00_00, $00_00_07_00, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0001_1111_1100, $07_06_00_00, $07_07_07_07, $00_00_00_06
        long  %0000_0000_0000_0000_0000_0000_0111_0000, $00_00_00_00, $00_04_04_04, $00_00_00_00
sprSatellite_left
        byte  5, 5, 12, 11
        long  %0000_0000_0000_0000_0000_0001_1110_0000, $00_00_00_00, $07_07_06_00, $00_00_00_06
        long  %0000_0000_0000_0000_0000_0011_0000_0000, $00_00_00_00, $00_00_00_00, $00_00_07_07
        long  %0000_0000_0000_0000_0000_0111_0000_0110, $00_06_06_00, $00_00_00_00, $00_07_07_07
        long  %0000_0000_0000_0000_0000_0100_1010_0011, $00_00_07_04, $07_00_07_00, $00_07_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_0011, $00_00_07_06, $00_07_07_07, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_1111_1111, $07_07_07_06, $07_07_07_07, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_0011, $00_00_07_06, $00_07_07_07, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0100_1010_0011, $00_00_07_04, $07_00_07_00, $00_07_00_00
        long  %0000_0000_0000_0000_0000_0111_0000_0110, $00_06_06_00, $00_00_00_00, $00_07_07_07
        long  %0000_0000_0000_0000_0000_0011_0000_0000, $00_00_00_00, $00_00_00_00, $00_00_07_07
        long  %0000_0000_0000_0000_0000_0001_1110_0000, $00_00_00_00, $07_07_06_00, $00_00_00_06
sprSatellite_right
        byte  5, 5, 12, 11
        long  %0000_0000_0000_0000_0000_0000_0011_1100, $07_06_00_00, $00_00_06_07, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0000_0110, $00_07_07_00, $00_00_00_00, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0011_0000_0111, $00_07_07_07, $00_00_00_00, $00_00_06_06
        long  %0000_0000_0000_0000_0000_0110_0010_1001, $07_00_00_07, $00_00_07_00, $00_04_07_00
        long  %0000_0000_0000_0000_0000_0110_0111_0000, $00_00_00_00, $00_07_07_07, $00_06_07_00
        long  %0000_0000_0000_0000_0000_0111_1111_1000, $07_00_00_00, $07_07_07_07, $00_06_07_07
        long  %0000_0000_0000_0000_0000_0110_0111_0000, $00_00_00_00, $00_07_07_07, $00_06_07_00
        long  %0000_0000_0000_0000_0000_0110_0010_1001, $07_00_00_07, $00_00_07_00, $00_04_07_00
        long  %0000_0000_0000_0000_0000_0011_0000_0111, $00_07_07_07, $00_00_00_00, $00_00_06_06
        long  %0000_0000_0000_0000_0000_0000_0000_0110, $00_07_07_00, $00_00_00_00, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0011_1100, $07_06_00_00, $00_00_06_07, $00_00_00_00
 
sprDot
        byte  0, 0, 4, 1
        long  %0000_0000_0000_0000_0000_0000_0000_0001, $00_00_00_fe

sprSmallX
        byte  1, 1, 4, 3
        long  %0000_0000_0000_0000_0000_0000_0000_0101, $00_7e_00_7e
        long  %0000_0000_0000_0000_0000_0000_0000_0010, $00_00_07_00
        long  %0000_0000_0000_0000_0000_0000_0000_0101, $00_7e_00_7e

sprUFO_0
        byte  8, 4, 16, 8
        long  %0000_0000_0000_0000_0000_0111_1110_0000, $00_00_00_00, $7e_7e_7c_00, $00_7c_7e_7e, $00_00_00_00
        long  %0000_0000_0000_0000_0000_1111_1111_0000, $00_00_00_00, $7e_7e_7d_04, $04_7d_7e_7e, $00_00_00_00
        long  %0000_0000_0000_0000_0000_1111_1111_0000, $00_00_00_00, $7e_7e_7e_7e, $7e_7e_7e_7e, $00_00_00_00
        long  %0000_0000_0000_0000_0011_1111_1111_1100, $7e_04_00_00, $7c_7c_7d_7d, $7d_7d_7c_7c, $00_00_04_7e
        long  %0000_0000_0000_0000_1111_1111_1111_1111, $7c_7c_7c_7c, $7c_7c_7c_7c, $7c_7c_7c_7c, $7c_7c_7c_7c
        long  %0000_0000_0000_0000_0001_1111_1111_1000, $04_00_00_00, $7c_7c_7c_7c, $7c_7c_7c_7c, $00_00_00_04
        long  %0000_0000_0000_0000_0000_0111_1110_0000, $00_00_00_00, $7c_7c_7c_00, $00_7c_7c_7c, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0011_1100_0000, $00_00_00_00, $7c_04_00_00, $00_00_04_7c, $00_00_00_00
sprUFO_1
        byte  8, 4, 16, 8
        long  %0000_0000_0000_0000_0000_0111_1110_0000, $00_00_00_00, $7e_7e_7c_00, $00_7c_7e_7e, $00_00_00_00
        long  %0000_0000_0000_0000_0000_1111_1111_0000, $00_00_00_00, $7e_7e_7d_04, $04_7d_7e_7e, $00_00_00_00
        long  %0000_0000_0000_0000_0000_1111_1111_0000, $00_00_00_00, $7e_7e_7e_7e, $7e_7e_7e_7e, $00_00_00_00
        long  %0000_0000_0000_0000_0011_1111_1111_1100, $7e_04_00_00, $7c_7c_7d_7d, $7d_7d_7c_7c, $00_00_04_7e
        long  %0000_0000_0000_0000_1111_1111_1111_1111, $07_07_07_07, $7c_7c_07_07, $7c_7c_7c_7c, $7c_7c_7c_7c
        long  %0000_0000_0000_0000_0001_1111_1111_1000, $04_00_00_00, $7c_7c_7c_7c, $7c_7c_7c_7c, $00_00_00_04
        long  %0000_0000_0000_0000_0000_0111_1110_0000, $00_00_00_00, $7c_7c_7c_00, $00_7c_7c_7c, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0011_1100_0000, $00_00_00_00, $7c_04_00_00, $00_00_04_7c, $00_00_00_00
sprUFO_2
        byte  8, 4, 16, 8
        long  %0000_0000_0000_0000_0000_0111_1110_0000, $00_00_00_00, $7e_7e_7c_00, $00_7c_7e_7e, $00_00_00_00
        long  %0000_0000_0000_0000_0000_1111_1111_0000, $00_00_00_00, $7e_7e_7d_04, $04_7d_7e_7e, $00_00_00_00
        long  %0000_0000_0000_0000_0000_1111_1111_0000, $00_00_00_00, $7e_7e_7e_7e, $7e_7e_7e_7e, $00_00_00_00
        long  %0000_0000_0000_0000_0011_1111_1111_1100, $7e_04_00_00, $07_07_06_7d, $7d_06_07_07, $00_00_04_7e
        long  %0000_0000_0000_0000_1111_1111_1111_1111, $07_7c_7c_7c, $7c_06_07_07, $07_07_06_7c, $7c_7c_7c_07
        long  %0000_0000_0000_0000_0001_1111_1111_1000, $04_00_00_00, $7c_7c_7c_7c, $7c_7c_7c_7c, $00_00_00_04
        long  %0000_0000_0000_0000_0000_0111_1110_0000, $00_00_00_00, $7c_7c_7c_00, $00_7c_7c_7c, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0011_1100_0000, $00_00_00_00, $7c_04_00_00, $00_00_04_7c, $00_00_00_00
sprUFO_3
        byte  8, 4, 16, 8
        long  %0000_0000_0000_0000_0000_0111_1110_0000, $00_00_00_00, $7e_7e_7c_00, $00_7c_7e_7e, $00_00_00_00
        long  %0000_0000_0000_0000_0000_1111_1111_0000, $00_00_00_00, $7e_7e_7d_04, $04_7d_7e_7e, $00_00_00_00
        long  %0000_0000_0000_0000_0000_1111_1111_0000, $00_00_00_00, $7e_7e_7e_7e, $7e_7e_7e_7e, $00_00_00_00
        long  %0000_0000_0000_0000_0011_1111_1111_1100, $7e_04_00_00, $7c_7c_7d_7d, $7d_7d_7c_7c, $00_00_04_7e
        long  %0000_0000_0000_0000_1111_1111_1111_1111, $7c_7c_7c_7c, $7c_7c_7c_7c, $07_07_7c_7c, $07_07_07_07
        long  %0000_0000_0000_0000_0001_1111_1111_1000, $04_00_00_00, $7c_7c_7c_7c, $7c_7c_7c_7c, $00_00_00_04
        long  %0000_0000_0000_0000_0000_0111_1110_0000, $00_00_00_00, $7c_7c_7c_00, $00_7c_7c_7c, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0011_1100_0000, $00_00_00_00, $7c_04_00_00, $00_00_04_7c, $00_00_00_00
{
sprUFO
        byte  8, 5, 16, 10
        long  %0000_0000_0000_0000_0000_0111_1110_0000, $00_00_00_00, $7d_7d_7c_00, $00_7c_7d_7d, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0111_1110_0000, $00_00_00_00, $7d_7d_7d_00, $00_7d_7d_7d, $00_00_00_00
        long  %0000_0000_0000_0000_0111_1111_1111_1110, $7c_7c_7c_00, $7c_7c_7c_7c, $7c_7c_7c_7c, $00_7c_7c_7c
        long  %0000_0000_0000_0000_0111_1111_1111_1110, $7d_7d_7d_00, $7d_7d_7d_7d, $7d_7d_7d_7d, $00_7d_7d_7d
        long  %0000_0000_0000_0000_1111_1111_1111_1111, $7d_7d_7d_7d, $7d_7d_7d_7d, $7d_7d_7d_7d, $7d_7d_7d_7d
        long  %0000_0000_0000_0000_1111_1111_1111_1111, $7d_7d_7d_7d, $7d_7d_7d_7d, $7d_7d_7d_7d, $7d_7d_7d_7d
        long  %0000_0000_0000_0000_0111_1111_1111_1110, $7d_7d_7d_00, $7d_7d_7d_7d, $7d_7d_7d_7d, $00_7d_7d_7d
        long  %0000_0000_0000_0000_0111_1111_1111_1110, $7c_7c_7c_00, $7c_7c_7c_7c, $7c_7c_7c_7c, $00_7c_7c_7c
        long  %0000_0000_0000_0000_0000_0111_1110_0000, $00_00_00_00, $7d_7d_7d_00, $00_7d_7d_7d, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0111_1110_0000, $00_00_00_00, $7d_7d_7c_00, $00_7c_7d_7d, $00_00_00_00
}

sprBombA_0
        byte  4, 2, 8, 12
        long  %0000_0000_0000_0000_0000_0000_0001_1000, $3c_00_00_00, $00_00_00_3c
        long  %0000_0000_0000_0000_0000_0000_0011_1100, $3c_3c_00_00, $00_00_3c_3c
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $3c_3c_4b_00, $00_4b_3c_3c
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $3c_3c_3c_00, $00_3c_3c_3c
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $3c_3c_3c_00, $00_3c_3c_3c
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $3c_3c_3c_00, $00_3c_3c_3c
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $3c_3c_3c_00, $00_3c_3c_3c
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $3c_3c_4c_00, $00_4c_3c_3c
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $3c_4c_4b_00, $00_4b_4c_3c
        long  %0000_0000_0000_0000_0000_0000_0011_1100, $3c_4c_00_00, $00_00_4c_3c
        long  %0000_0000_0000_0000_0000_0000_1111_1111, $4c_4b_3c_4b, $4b_3c_4b_4c
        long  %0000_0000_0000_0000_0000_0000_1100_0011, $00_00_3c_3c, $3c_3c_00_00
sprBombA_45
        byte  9, 2, 12, 12
        long  %0000_0000_0000_0000_0000_1111_1000_0000, $00_00_00_00, $3b_00_00_00, $3b_3c_3c_3b
        long  %0000_0000_0000_0000_0000_1111_1100_0000, $00_00_00_00, $3c_3b_00_00, $3c_3c_3c_3c
        long  %0000_0000_0000_0000_0000_1111_1110_0000, $00_00_00_00, $3c_3c_3b_00, $3c_3c_3c_3c
        long  %0000_0000_0000_0000_0000_1111_1111_0000, $00_00_00_00, $3c_3c_3c_3b, $3b_3c_3c_3c
        long  %0000_0000_0000_0000_0000_1111_1111_0110, $00_3b_3b_00, $3c_3c_3c_3b, $3b_3c_3c_3c
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $3c_3c_3c_00, $3c_3c_3c_3b, $00_3b_3c_3c
        long  %0000_0000_0000_0000_0000_0011_1111_1110, $3b_3c_3b_00, $3c_3c_3c_3c, $00_00_3b_3c
        long  %0000_0000_0000_0000_0000_0001_1111_0100, $00_3b_00_00, $3b_3b_3c_3b, $00_00_00_3b
        long  %0000_0000_0000_0000_0000_0000_0110_0000, $00_00_00_00, $00_3c_3b_00, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_1111_0000, $00_00_00_00, $3b_3c_3c_3b, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_1110_0000, $00_00_00_00, $3b_3c_3b_00, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0000_0000, $00_00_00_00, $00_00_00_00, $00_00_00_00
sprBombA_90
        byte  9, 4, 12, 8
        long  %0000_0000_0000_0000_0000_0000_0000_0011, $00_00_4b_3c, $00_00_00_00, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0011_1111_1011, $4b_00_3c_3c, $3c_3c_3c_4c, $00_00_4b_3c
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $4c_4c_4b_00, $3c_3c_3c_3c, $00_3c_3c_3c
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $3c_3c_4c_00, $3c_3c_3c_3c, $3c_3c_3c_3c
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $3c_3c_4c_00, $3c_3c_3c_3c, $3c_3c_3c_3c
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $4c_4c_4b_00, $3c_3c_3c_3c, $00_3c_3c_3c
        long  %0000_0000_0000_0000_0000_0011_1111_1011, $4b_00_3c_3c, $3c_3c_3c_4c, $00_00_4b_3c
        long  %0000_0000_0000_0000_0000_0000_0000_0011, $00_00_4b_3c, $00_00_00_00, $00_00_00_00
sprBombA_135
        byte  9, 9, 12, 12
        long  %0000_0000_0000_0000_0000_0000_0000_0000, $00_00_00_00, $00_00_00_00, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_1110_0000, $00_00_00_00, $3b_3c_3b_00, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_1111_0000, $00_00_00_00, $3b_3c_3c_3b, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0110_0000, $00_00_00_00, $00_3c_3b_00, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0001_1111_0100, $00_3b_00_00, $3b_3b_3c_3b, $00_00_00_3b
        long  %0000_0000_0000_0000_0000_0011_1111_1110, $3b_3c_3b_00, $3c_3c_3c_3c, $00_00_3b_3c
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $3c_3c_3c_00, $3c_3c_3c_3b, $00_3b_3c_3c
        long  %0000_0000_0000_0000_0000_1111_1111_0110, $00_3b_3b_00, $3c_3c_3c_3b, $3b_3c_3c_3c
        long  %0000_0000_0000_0000_0000_1111_1111_0000, $00_00_00_00, $3c_3c_3c_3b, $3b_3c_3c_3c
        long  %0000_0000_0000_0000_0000_1111_1110_0000, $00_00_00_00, $3c_3c_3b_00, $3c_3c_3c_3c
        long  %0000_0000_0000_0000_0000_1111_1100_0000, $00_00_00_00, $3c_3b_00_00, $3c_3c_3c_3c
        long  %0000_0000_0000_0000_0000_1111_1000_0000, $00_00_00_00, $3b_00_00_00, $3b_3c_3c_3b
sprBombA_180
        byte  3, 9, 8, 12
        long  %0000_0000_0000_0000_0000_0000_1100_0011, $00_00_3c_3c, $3c_3c_00_00
        long  %0000_0000_0000_0000_0000_0000_1111_1111, $4c_4b_3c_4b, $4b_3c_4b_4c
        long  %0000_0000_0000_0000_0000_0000_0011_1100, $3c_4c_00_00, $00_00_4c_3c
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $3c_4c_4b_00, $00_4b_4c_3c
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $3c_3c_4c_00, $00_4c_3c_3c
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $3c_3c_3c_00, $00_3c_3c_3c
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $3c_3c_3c_00, $00_3c_3c_3c
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $3c_3c_3c_00, $00_3c_3c_3c
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $3c_3c_3c_00, $00_3c_3c_3c
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $3c_3c_4b_00, $00_4b_3c_3c
        long  %0000_0000_0000_0000_0000_0000_0011_1100, $3c_3c_00_00, $00_00_3c_3c
        long  %0000_0000_0000_0000_0000_0000_0001_1000, $3c_00_00_00, $00_00_00_3c
sprBombA_225
        byte  2, 9, 12, 12
        long  %0000_0000_0000_0000_0000_0000_0000_0000, $00_00_00_00, $00_00_00_00, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_0000, $00_00_00_00, $00_3b_3c_3b, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_1111_0000, $00_00_00_00, $3b_3c_3c_3b, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0110_0000, $00_00_00_00, $00_3b_3c_00, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0010_1111_1000, $3b_00_00_00, $3b_3c_3b_3b, $00_00_3b_00
        long  %0000_0000_0000_0000_0000_0111_1111_1100, $3c_3b_00_00, $3c_3c_3c_3c, $00_3b_3c_3b
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $3c_3c_3b_00, $3b_3c_3c_3c, $00_3c_3c_3c
        long  %0000_0000_0000_0000_0000_0110_1111_1111, $3c_3c_3c_3b, $3b_3c_3c_3c, $00_3b_3b_00
        long  %0000_0000_0000_0000_0000_0000_1111_1111, $3c_3c_3c_3b, $3b_3c_3c_3c, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_1111, $3c_3c_3c_3c, $00_3b_3c_3c, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0011_1111, $3c_3c_3c_3c, $00_00_3b_3c, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0001_1111, $3b_3c_3c_3b, $00_00_00_3b, $00_00_00_00
sprBombA_270
        byte  2, 3, 12, 8
        long  %0000_0000_0000_0000_0000_1100_0000_0000, $00_00_00_00, $00_00_00_00, $3c_4b_00_00
        long  %0000_0000_0000_0000_0000_1101_1111_1100, $3c_4b_00_00, $4c_3c_3c_3c, $3c_3c_00_4b
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $3c_3c_3c_00, $3c_3c_3c_3c, $00_4b_4c_4c
        long  %0000_0000_0000_0000_0000_0111_1111_1111, $3c_3c_3c_3c, $3c_3c_3c_3c, $00_4c_3c_3c
        long  %0000_0000_0000_0000_0000_0111_1111_1111, $3c_3c_3c_3c, $3c_3c_3c_3c, $00_4c_3c_3c
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $3c_3c_3c_00, $3c_3c_3c_3c, $00_4b_4c_4c
        long  %0000_0000_0000_0000_0000_1101_1111_1100, $3c_4b_00_00, $4c_3c_3c_3c, $3c_3c_00_4b
        long  %0000_0000_0000_0000_0000_1100_0000_0000, $00_00_00_00, $00_00_00_00, $3c_4b_00_00
sprBombA_315
        byte  2, 2, 12, 12
        long  %0000_0000_0000_0000_0000_0000_0001_1111, $3b_3c_3c_3b, $00_00_00_3b, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0011_1111, $3c_3c_3c_3c, $00_00_3b_3c, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_1111, $3c_3c_3c_3c, $00_3b_3c_3c, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_1111_1111, $3c_3c_3c_3b, $3b_3c_3c_3c, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0110_1111_1111, $3c_3c_3c_3b, $3b_3c_3c_3c, $00_3b_3b_00
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $3c_3c_3b_00, $3b_3c_3c_3c, $00_3c_3c_3c
        long  %0000_0000_0000_0000_0000_0111_1111_1100, $3c_3b_00_00, $3c_3c_3c_3c, $00_3b_3c_3b
        long  %0000_0000_0000_0000_0000_0010_1111_1000, $3b_00_00_00, $3b_3c_3b_3b, $00_00_3b_00
        long  %0000_0000_0000_0000_0000_0000_0110_0000, $00_00_00_00, $00_3b_3c_00, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_1111_0000, $00_00_00_00, $3b_3c_3c_3b, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_0000, $00_00_00_00, $00_3b_3c_3b, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0000_0000, $00_00_00_00, $00_00_00_00, $00_00_00_00

DAT
sprA
        byte  6, 7, 12, 15
        long  %0000_0000_0000_0000_0000_0011_1111_0000, $00_00_00_00, $ae_ae_ae_ae, $00_00_ae_ae
        long  %0000_0000_0000_0000_0000_0011_1111_0000, $00_00_00_00, $aa_aa_aa_ae, $00_00_ae_ad
        long  %0000_0000_0000_0000_0000_0011_1111_0000, $00_00_00_00, $ab_ab_aa_ae, $00_00_ae_ca
        long  %0000_0000_0000_0000_0000_0011_1111_0000, $00_00_00_00, $ab_ab_ab_ae, $00_00_ae_aa
        long  %0000_0000_0000_0000_0000_0011_1111_1000, $ae_00_00_00, $ab_ab_aa_ac, $00_00_ae_ab
        long  %0000_0000_0000_0000_0000_0011_1111_1000, $ae_00_00_00, $ab_ab_ae_aa, $00_00_ae_ab
        long  %0000_0000_0000_0000_0000_0111_1111_1000, $ae_00_00_00, $ab_ab_ae_ab, $00_ae_ac_ab
        long  %0000_0000_0000_0000_0000_0111_1111_1000, $ae_00_00_00, $ab_ab_ae_aa, $00_ae_aa_ab
        long  %0000_0000_0000_0000_0000_0111_1111_1100, $ac_ae_00_00, $ab_aa_ae_ad, $00_ae_ab_ab
        long  %0000_0000_0000_0000_0000_0111_1111_1100, $aa_ae_00_00, $ab_ca_ae_ae, $00_ae_ab_ab
        long  %0000_0000_0000_0000_0000_0111_1111_1100, $ab_ae_00_00, $ab_aa_ab_ad, $00_ae_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1100, $aa_ae_00_00, $ab_ae_ae_ae, $ae_ac_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1101_1110, $ac_ac_ae_00, $ab_ae_00_ae, $ae_aa_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ab_ab_ac_ae, $ab_ad_ae_ae, $ad_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ae_ae_ae_ae, $ae_ae_ae_ae, $ae_ae_ae_ae

sprD
        byte  6, 7, 12, 15
        long  %0000_0000_0000_0000_0000_0001_1111_1111, $ae_ae_ae_ae, $ae_ae_ae_ae, $00_00_00_ae
        long  %0000_0000_0000_0000_0000_0111_1111_1111, $ab_ab_ac_ae, $aa_ae_ab_ab, $00_ae_ae_ae
        long  %0000_0000_0000_0000_0000_0111_1111_1111, $ab_ab_ae_ae, $ab_ae_ab_ab, $00_ae_ac_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_ab_ae_00, $ac_ae_ab_ab, $ae_ad_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_ab_ae_00, $ac_ae_ab_ab, $ae_ac_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_ab_ae_00, $ac_ae_ab_ab, $ad_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_ab_ae_00, $ac_ae_ab_ab, $ad_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_ab_ae_00, $ac_ae_ab_ab, $ad_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_ab_ae_00, $ac_ae_ab_ab, $ad_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_ab_ae_00, $ac_ae_ab_ab, $ad_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_ab_ae_00, $ac_ae_ab_ab, $ae_ac_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_ab_ae_00, $ac_ae_ab_ab, $ae_ad_ab_ab
        long  %0000_0000_0000_0000_0000_0111_1111_1111, $ab_ab_ae_ae, $ab_ae_ab_ab, $00_ae_ac_ab
        long  %0000_0000_0000_0000_0000_0111_1111_1111, $ab_ab_ac_ae, $ab_ae_ab_ab, $00_ae_ae_ae
        long  %0000_0000_0000_0000_0000_0001_1111_1111, $ae_ae_ae_ae, $ae_ae_ae_ae, $00_00_00_ae

sprE
        byte  6, 7, 12, 15
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ae_ae_ae_ae, $ae_ae_ae_ae, $ae_ae_ae_ae
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $aa_ab_ac_ae, $aa_ae_aa_aa, $ae_ab_aa_aa
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ab_ab_ae_ae, $ae_ae_ab_ab, $ae_ab_ab_ad
        long  %0000_0000_0000_0000_0000_1111_0111_1110, $ab_aa_ae_00, $00_ae_ab_ab, $ae_ab_ad_ae
        long  %0000_0000_0000_0000_0000_1111_0111_1110, $ab_aa_ae_00, $00_ae_ab_ab, $ae_ab_ae_ae
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_aa_ae_00, $ae_ae_ab_ab, $ae_ae_ae_ac
        long  %0000_0000_0000_0000_0000_0011_1111_1110, $ab_ab_ae_00, $ac_ae_ab_ab, $00_00_ae_ab
        long  %0000_0000_0000_0000_0000_0011_1111_1110, $ab_aa_ae_00, $ab_ae_ab_ab, $00_00_ae_aa
        long  %0000_0000_0000_0000_0000_0011_1111_1110, $ab_aa_ae_00, $ac_ae_ab_ab, $00_00_ae_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_aa_ae_00, $ae_ae_ab_ab, $ae_ae_ae_ac
        long  %0000_0000_0000_0000_0000_1111_0111_1110, $ab_ab_ae_00, $00_ae_ab_ab, $ae_ab_ae_ae
        long  %0000_0000_0000_0000_0000_1111_0111_1110, $ab_aa_ae_00, $00_ae_ab_ab, $ae_aa_ad_ae
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ab_aa_ae_ae, $ae_ae_ab_ab, $ae_ab_ab_ad
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ab_ab_ac_ae, $ab_ae_ab_ab, $ae_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ae_ae_ae_ae, $ae_ae_ae_ae, $ae_ae_ae_ae

sprF
        byte  6, 7, 12, 15
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ae_ae_ae_ae, $ae_ae_ae_ae, $ae_ae_ae_ae
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $aa_ab_ac_ae, $aa_ae_aa_aa, $ae_ab_aa_aa
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_ab_ae_00, $ae_ae_ab_ab, $ae_ab_ab_ad
        long  %0000_0000_0000_0000_0000_1111_0111_1110, $ab_ab_ae_00, $00_ae_ab_ab, $ae_ab_ad_ae
        long  %0000_0000_0000_0000_0000_1111_0111_1110, $ab_aa_ae_00, $00_ae_ab_ab, $ae_ab_ae_ae
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_aa_ae_00, $ae_ae_ab_ab, $ae_ae_ae_ac
        long  %0000_0000_0000_0000_0000_0011_1111_1110, $ab_aa_ae_00, $ac_ae_ab_ab, $00_00_ae_ab
        long  %0000_0000_0000_0000_0000_0011_1111_1110, $ab_ab_ae_00, $aa_ae_ab_ab, $00_00_ae_ab
        long  %0000_0000_0000_0000_0000_0011_1111_1110, $ab_aa_ae_00, $ac_ae_ab_ab, $00_00_ae_ab
        long  %0000_0000_0000_0000_0000_0011_1111_1110, $ab_aa_ae_00, $ae_ae_ab_ab, $00_00_ae_ac
        long  %0000_0000_0000_0000_0000_0011_1111_1110, $ab_aa_ae_00, $ae_ae_ab_ab, $00_00_ae_ae
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $ab_ab_ae_00, $00_ae_ab_ab, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $ab_aa_ae_00, $00_ae_ab_ab, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_1111_1111, $ab_ab_ac_ae, $ae_ac_ab_ab, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_1111_1111, $ae_ae_ae_ae, $ae_ae_ae_ae, $00_00_00_00
{
sprG
        byte  8, 7, 12, 15
        long  %0000_0000_0000_0000_0000_1101_1111_0000, $00_00_00_00, $07_07_07_07, $07_07_00_07
        long  %0000_0000_0000_0000_0000_1111_1111_1000, $07_00_00_00, $04_06_04_06, $07_04_07_06
        long  %0000_0000_0000_0000_0000_1111_1111_1100, $04_07_00_00, $04_07_6a_02, $07_02_02_6a
        long  %0000_0000_0000_0000_0000_1111_1011_1110, $02_04_07_00, $07_00_07_02, $07_02_05_07
        long  %0000_0000_0000_0000_0000_1110_0011_1110, $02_02_07_00, $00_00_07_02, $07_02_05_00
        long  %0000_0000_0000_0000_0000_1110_0011_1111, $02_02_04_07, $00_00_07_02, $07_6a_07_00
        long  %0000_0000_0000_0000_0000_0100_0011_1111, $02_02_03_07, $00_00_07_02, $00_07_00_00
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $02_02_6a_07, $07_07_07_02, $07_07_07_07
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $02_6a_ca_07, $6a_04_07_02, $06_02_02_02
        long  %0000_0000_0000_0000_0000_1111_1011_1111, $02_02_03_07, $06_00_07_02, $07_02_02_7a
        long  %0000_0000_0000_0000_0000_1111_1011_1111, $02_6a_04_07, $06_00_07_02, $07_02_02_02
        long  %0000_0000_0000_0000_0000_1111_1011_1110, $02_02_07_00, $06_00_07_02, $07_02_02_6a
        long  %0000_0000_0000_0000_0000_1111_1011_1110, $7a_05_07_00, $06_00_07_02, $07_05_02_02
        long  %0000_0000_0000_0000_0000_0111_1111_1100, $06_07_00_00, $6a_07_02_02, $00_07_04_02
        long  %0000_0000_0000_0000_0000_0011_1111_1000, $07_00_00_00, $07_07_07_07, $00_00_07_07

sprM
        byte  6, 7, 12, 15
        long  %0000_0000_0000_0000_0000_1111_0001_1111, $07_07_07_07, $00_00_00_07, $07_07_07_07
        long  %0000_0000_0000_0000_0000_1111_1011_1111, $03_03_03_07, $07_00_07_03, $06_03_03_03
        long  %0000_0000_0000_0000_0000_1111_1011_1110, $ca_6a_06_00, $07_00_07_6a, $07_ca_6a_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $02_03_07_00, $07_07_04_02, $07_6a_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $6a_06_07_00, $06_07_ca_02, $07_03_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $02_07_07_00, $06_05_6a_02, $07_6a_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $05_07_07_00, $06_6a_02_02, $07_ca_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $07_03_07_00, $06_03_02_02, $07_03_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $07_ca_07_00, $07_03_02_6a, $07_6a_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $07_03_07_00, $07_05_02_05, $07_ca_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $07_ca_07_00, $07_06_02_06, $07_03_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $07_03_07_00, $07_07_7a_07, $07_6a_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $07_6a_07_00, $07_07_03_07, $07_ca_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $05_7a_05_07, $05_07_05_07, $06_6a_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $07_07_07_07, $07_07_07_07, $07_07_07_07

sprO
        byte  6, 7, 12, 15
        long  %0000_0000_0000_0000_0000_0001_1111_0000, $00_00_00_00, $07_07_07_07, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0011_1111_1000, $07_00_00_00, $04_06_03_05, $00_00_07_06
        long  %0000_0000_0000_0000_0000_0111_1111_1100, $02_07_00_00, $02_07_05_02, $00_07_04_02
        long  %0000_0000_0000_0000_0000_0111_1011_1110, $02_03_07_00, $06_00_07_04, $00_07_02_02
        long  %0000_0000_0000_0000_0000_1111_1011_1111, $02_02_06_07, $06_00_07_04, $07_05_02_02
        long  %0000_0000_0000_0000_0000_1111_1011_1111, $02_02_03_07, $06_00_07_04, $07_02_02_02
        long  %0000_0000_0000_0000_0000_1111_1011_1111, $02_02_03_07, $06_00_07_04, $07_02_02_02
        long  %0000_0000_0000_0000_0000_1111_1011_1111, $02_02_6a_07, $06_00_07_04, $07_02_02_02
        long  %0000_0000_0000_0000_0000_1111_1011_1111, $02_02_ca_07, $06_00_07_04, $07_02_02_02
        long  %0000_0000_0000_0000_0000_1111_1011_1111, $02_02_03_07, $06_00_07_04, $07_02_02_02
        long  %0000_0000_0000_0000_0000_1111_1011_1111, $02_02_04_07, $06_00_07_04, $07_04_02_02
        long  %0000_0000_0000_0000_0000_0111_1011_1110, $02_02_07_00, $06_00_07_04, $00_07_02_02
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $02_04_07_00, $03_07_07_02, $00_07_02_02
        long  %0000_0000_0000_0000_0000_0011_1111_1100, $04_07_00_00, $6a_06_02_6a, $00_00_07_02
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $07_00_00_00, $07_07_07_07, $00_00_00_07

sprV
        byte  6, 7, 12, 15
        long  %0000_0000_0000_0000_0000_1111_0111_1111, $07_07_07_07, $00_07_07_07, $07_07_07_07
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $02_02_04_07, $07_04_02_02, $07_04_02_04
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $02_02_06_00, $07_06_02_02, $00_06_02_05
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $6a_ca_07_00, $07_07_02_02, $00_07_ca_07
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $02_04_07_00, $07_05_02_02, $00_07_04_05
        long  %0000_0000_0000_0000_0000_0011_1111_1110, $02_06_07_00, $07_03_02_02, $00_00_07_03
        long  %0000_0000_0000_0000_0000_0011_1111_1100, $02_07_00_00, $07_6a_02_02, $00_00_07_6a
        long  %0000_0000_0000_0000_0000_0011_1111_1100, $03_07_00_00, $07_02_02_02, $00_00_07_7a
        long  %0000_0000_0000_0000_0000_0011_1111_1100, $05_07_00_00, $07_02_02_02, $00_00_07_04
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $07_00_00_00, $07_02_02_02, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $07_00_00_00, $ca_02_02_02, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $07_00_00_00, $03_02_6a_ca, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $07_00_00_00, $05_02_6a_05, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0000_1111_0000, $00_00_00_00, $07_02_02_07, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0110_0000, $00_00_00_00, $00_07_07_00, $00_00_00_00

spr0
        byte  6, 7, 12, 15
        long  %0000_0000_0000_0000_0000_0001_1110_0000, $00_00_00_00, $07_07_07_00, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0011_1111_0000, $00_00_00_00, $03_06_04_07, $00_00_07_06
        long  %0000_0000_0000_0000_0000_0111_1111_1000, $07_00_00_00, $03_07_6a_02, $00_07_06_02
        long  %0000_0000_0000_0000_0000_0111_1111_1100, $03_07_00_00, $06_07_03_02, $00_07_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $02_06_07_00, $06_07_03_02, $07_05_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $02_04_07_00, $06_07_03_02, $07_03_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $02_03_07_00, $06_07_03_02, $07_6a_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $02_03_07_00, $06_07_03_02, $07_02_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $02_03_07_00, $06_07_03_02, $07_02_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $02_04_07_00, $06_07_03_02, $07_03_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $02_06_07_00, $06_07_03_02, $07_04_02_02
        long  %0000_0000_0000_0000_0000_0111_1111_1100, $02_07_00_00, $06_07_03_02, $00_07_02_02
        long  %0000_0000_0000_0000_0000_0111_1111_1100, $06_07_00_00, $06_07_ca_02, $00_07_04_02
        long  %0000_0000_0000_0000_0000_0011_1111_1000, $07_00_00_00, $02_06_02_06, $00_00_07_04
        long  %0000_0000_0000_0000_0000_0001_1111_0000, $00_00_00_00, $07_07_07_07, $00_00_00_07

spr1
        byte  6, 7, 12, 15
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $07_00_00_00, $07_07_07_07, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0001_1111_1100, $06_07_00_00, $04_04_04_04, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0001_1111_1100, $04_07_00_00, $02_02_02_6a, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $07_00_00_00, $02_02_02_04, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $07_00_00_00, $02_02_02_04, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $07_00_00_00, $02_02_02_04, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $07_00_00_00, $02_02_6a_04, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $07_00_00_00, $02_02_02_04, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $07_00_00_00, $02_02_02_04, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $07_00_00_00, $02_02_6a_04, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $07_00_00_00, $02_02_02_04, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $07_00_00_00, $02_02_02_04, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $07_00_00_00, $6a_02_02_04, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0011_1111_1100, $04_07_00_00, $02_02_02_02, $00_00_07_ca
        long  %0000_0000_0000_0000_0000_0011_1111_1100, $07_07_00_00, $07_07_07_07, $00_00_07_07

spr2
        byte  6, 7, 12, 15
        long  %0000_0000_0000_0000_0000_0000_1111_1000, $07_00_00_00, $07_07_07_07, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0011_1111_1100, $05_07_00_00, $04_03_07_04, $00_00_07_07
        long  %0000_0000_0000_0000_0000_0111_1111_1111, $02_02_07_07, $02_02_07_06, $00_07_05_02
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $02_02_03_07, $02_ca_07_ca, $07_06_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $02_02_03_07, $02_03_07_02, $07_05_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $02_6a_07_00, $02_ca_07_06, $07_05_02_02
        long  %0000_0000_0000_0000_0000_0111_1110_1100, $07_07_00_00, $02_02_07_00, $00_07_02_02
        long  %0000_0000_0000_0000_0000_0011_1111_0000, $00_00_00_00, $02_02_05_07, $00_00_07_03
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $07_00_00_00, $06_04_02_03, $00_00_00_07
        long  %0000_0000_0000_0000_0000_0100_0111_1100, $03_07_00_00, $00_07_07_06, $00_06_00_00
        long  %0000_0000_0000_0000_0000_1111_1111_1100, $06_07_00_00, $07_07_05_05, $07_03_07_07
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $02_6a_07_00, $02_02_02_02, $07_04_02_02
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $02_02_03_07, $02_02_02_02, $07_07_02_02
        long  %0000_0000_0000_0000_0000_0111_1111_1111, $07_07_04_07, $02_02_04_06, $00_07_07_03
        long  %0000_0000_0000_0000_0000_0011_1111_1111, $07_07_07_07, $07_07_07_07, $00_00_07_07
}
sprL
        byte  6, 7, 12, 15
        long  %0000_0000_0000_0000_0000_0000_1111_1111, $ae_ae_ae_ae, $ae_ae_ae_ae, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_1111_1111, $aa_ab_ac_ae, $ae_ab_aa_aa, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $ab_ab_ae_00, $00_ae_ab_ab, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $ab_ab_ae_00, $00_ae_ab_ab, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $ab_aa_ae_00, $00_ae_ab_ab, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $ab_aa_ae_00, $00_ae_ab_ab, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $ab_aa_ae_00, $00_ae_ab_ab, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $ab_ab_ae_00, $00_ae_ab_ab, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $ab_aa_ae_00, $00_ae_ab_ab, $00_00_00_00
        long  %0000_0000_0000_0000_0000_1100_0111_1110, $ab_aa_ae_00, $00_ae_ab_ab, $ae_ae_00_00
        long  %0000_0000_0000_0000_0000_1110_0111_1110, $ab_aa_ae_00, $00_ae_ab_ab, $ae_aa_ae_00
        long  %0000_0000_0000_0000_0000_1110_0111_1110, $ab_ab_ae_00, $00_ae_ab_ab, $ae_ab_ad_00
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_aa_ae_00, $ae_ae_ab_ab, $ae_ab_aa_ae
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ab_ab_ac_ae, $ad_ae_ab_ab, $ae_ab_ab_aa
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ae_ae_ae_ae, $ae_ae_ae_ae, $ae_ae_ae_ae

sprN
        byte  6, 7, 12, 15
        long  %0000_0000_0000_0000_0000_1111_1011_1111, $ae_ae_ae_ae, $ae_00_ae_ae, $ae_ae_ae_ae
        long  %0000_0000_0000_0000_0000_1111_1011_1111, $aa_aa_aa_ae, $ae_00_ae_ab, $ae_ac_aa_ac
        long  %0000_0000_0000_0000_0000_0111_0111_1110, $ab_ab_ae_00, $00_ae_ad_ab, $00_ad_ab_ad
        long  %0000_0000_0000_0000_0000_0111_0111_1110, $ab_ab_ae_00, $00_ae_ab_ab, $00_ae_ab_ae
        long  %0000_0000_0000_0000_0000_0111_0111_1110, $ab_ad_ae_00, $00_ae_ab_ab, $00_ae_aa_ae
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $ab_ae_ae_00, $ae_ab_ab_ab, $00_ae_aa_ae
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $ae_ad_ae_00, $ab_ab_ab_aa, $00_ae_aa_ae
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $ae_aa_ae_00, $ab_ab_ab_ab, $00_ae_ad_ae
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $ae_aa_ae_00, $ab_ab_ab_ae, $00_ae_ae_ab
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $ae_aa_ae_00, $ab_ab_ab_ae, $00_ae_ad_ab
        long  %0000_0000_0000_0000_0000_0111_1110_1110, $ae_aa_ae_00, $ab_ab_ae_00, $00_ae_ab_ab
        long  %0000_0000_0000_0000_0000_0111_1110_1110, $ae_aa_ae_00, $aa_aa_ae_00, $00_ae_ab_ab
        long  %0000_0000_0000_0000_0000_0111_1110_1110, $ad_aa_ad_00, $ab_ad_ae_00, $00_ae_ab_ab
        long  %0000_0000_0000_0000_0000_0111_1101_1111, $ac_ab_ac_ae, $ab_ae_00_ae, $00_ae_ab_ab
        long  %0000_0000_0000_0000_0000_0111_1101_1111, $ae_ae_ae_ae, $ae_ae_00_ae, $00_ae_ae_ae

sprP
        byte  6, 7, 12, 15
        long  %0000_0000_0000_0000_0000_0011_1111_1111, $ae_ae_ae_ae, $ae_ae_ae_ae, $00_00_ae_ae
        long  %0000_0000_0000_0000_0000_0111_1111_1111, $aa_aa_ac_ae, $aa_ae_aa_ab, $00_ae_ac_aa
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_ab_ae_00, $ab_ae_ab_ab, $ae_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_ab_ae_00, $ac_ae_ab_ab, $ad_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_aa_ae_00, $ac_ae_ab_ab, $ad_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_aa_ae_00, $ac_ae_ab_ab, $ad_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_aa_ae_00, $ac_ae_ab_ab, $ad_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_ab_ae_00, $ab_ae_ab_ab, $ae_ab_ab_ab
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $ab_aa_ae_00, $aa_ae_ab_ab, $00_ae_ac_ab
        long  %0000_0000_0000_0000_0000_0011_1111_1110, $ab_aa_ae_00, $ae_ae_ab_ab, $00_00_ae_ae
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $ab_aa_ae_00, $00_ae_ab_ab, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $ab_ab_ae_00, $00_ae_ab_ab, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_0111_1110, $ab_aa_ae_00, $00_ae_ab_ab, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_1111_1111, $ab_aa_ac_ae, $ae_ac_ab_ab, $00_00_00_00
        long  %0000_0000_0000_0000_0000_0000_1111_1111, $ae_ae_ae_ae, $ae_ae_ae_ae, $00_00_00_00

sprR
        byte  6, 7, 12, 15
        long  %0000_0000_0000_0000_0000_0011_1111_1111, $ae_ae_ae_ae, $ae_ae_ae_ae, $00_00_ae_ae
        long  %0000_0000_0000_0000_0000_0111_1111_1111, $aa_aa_ac_ae, $aa_aa_ae_ab, $00_ae_ad_aa
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $ab_ab_ae_00, $ab_ad_ae_ab, $00_ad_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_aa_ae_00, $ab_ae_ae_ab, $ae_ac_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_ab_ae_00, $aa_ae_ae_ab, $ae_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_aa_ae_00, $aa_ae_ae_ab, $ae_ad_ab_ab
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $ab_aa_ae_00, $aa_ae_ae_ab, $00_ae_ca_ab
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $ab_aa_ae_00, $ab_ca_ae_ab, $00_ae_ae_ad
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $ab_ab_ae_00, $ab_ab_ae_ab, $00_ae_aa_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_aa_ae_00, $aa_ae_ae_ab, $ae_ac_aa_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_aa_ae_00, $ab_ae_ae_ab, $ae_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_aa_ae_00, $aa_ae_ae_ab, $ae_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_ab_ae_00, $aa_ae_ae_ab, $ad_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ab_aa_ac_ae, $ad_ae_ac_ab, $ae_ab_ab_ab
        long  %0000_0000_0000_0000_0000_0111_1111_1111, $ae_ae_ae_ae, $ae_ae_ae_ae, $00_ae_ad_ae

sprS
        byte  6, 7, 12, 15
        long  %0000_0000_0000_0000_0000_1111_1111_1100, $ae_ae_00_00, $ae_ae_ae_ae, $ae_ae_ae_ae
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ac_ae_ae_00, $aa_ae_aa_ab, $ae_ca_ac_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ab_aa_ae_ae, $ae_ae_ae_ab, $ae_aa_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ab_ab_aa_ae, $ae_ae_ae_ab, $ae_ab_ab_ae
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ab_ab_ab_ae, $ae_ae_ae_ab, $ae_ab_aa_ae
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ab_ab_aa_ae, $ad_aa_ab_ab, $ae_ad_ae_ae
        long  %0000_0000_0000_0000_0000_0111_1111_1111, $ab_ab_ab_ae, $ab_ab_ab_ab, $00_ae_ae_ab
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $ab_ab_ae_00, $ab_ab_ab_ab, $00_ae_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_ad_ae_00, $ab_ab_ab_ab, $ae_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ae_ae_ad_ae, $ab_ab_ab_ab, $ae_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ae_ae_ab_ae, $ab_ad_ae_ae, $ae_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ae_ab_ab_ae, $ad_ae_ae_ae, $ae_ab_ab_ab
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $aa_ab_aa_ae, $ad_ae_ae_ae, $ae_ab_ab_ab
        long  %0000_0000_0000_0000_0000_0111_1111_1111, $ac_ad_aa_ae, $ab_ab_ae_ab, $00_ae_ab_ab
        long  %0000_0000_0000_0000_0000_0011_1111_1111, $ae_ae_ae_ae, $ae_ae_ae_ae, $00_00_ae_ae

sprT
        byte  6, 7, 12, 15
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ae_ae_ae_ae, $ae_ae_ae_ae, $ae_ae_ae_ae
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ad_aa_aa_ae, $ab_aa_aa_aa, $ae_aa_ab_ad
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ae_ab_aa_ae, $ab_ab_ab_ab, $ae_ab_ca_ae
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ae_ab_ab_ae, $ab_ab_ab_aa, $ae_aa_ab_ae
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ae_ae_aa_ae, $ab_ab_ab_aa, $ae_aa_ae_ae
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ae_ae_ac_ae, $ab_ab_ab_aa, $ae_ac_ae_ae
        long  %0000_0000_0000_0000_0000_1101_1111_1011, $ae_00_ae_ae, $ab_ab_ab_ab, $ae_ae_00_ae
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $ae_00_00_00, $ab_ab_ab_aa, $00_00_00_ae
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $ae_00_00_00, $ab_ab_ab_aa, $00_00_00_ae
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $ae_00_00_00, $ab_ab_ab_aa, $00_00_00_ae
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $ae_00_00_00, $ab_ab_ab_ab, $00_00_00_ae
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $ae_00_00_00, $ab_ab_ab_aa, $00_00_00_ae
        long  %0000_0000_0000_0000_0000_0011_1111_1100, $ae_ae_00_00, $ab_ab_ab_aa, $00_00_ae_ae
        long  %0000_0000_0000_0000_0000_0011_1111_1100, $ab_ae_00_00, $ab_ab_ab_ab, $00_00_ae_ab
        long  %0000_0000_0000_0000_0000_0011_1111_1100, $ae_ae_00_00, $ae_ae_ae_ae, $00_00_ae_ae

sprY
        byte  6, 7, 12, 15
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ae_ae_ae_ae, $ae_ae_ae_ae, $ae_ae_ae_ae
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $aa_aa_aa_ae, $ae_ab_ab_aa, $ae_aa_aa_aa
        long  %0000_0000_0000_0000_0000_1111_1111_1111, $ab_ab_ac_ae, $ae_ad_ab_ab, $ae_ac_ab_ad
        long  %0000_0000_0000_0000_0000_1111_1111_1110, $ab_ab_ae_00, $ae_ae_ab_ab, $ae_ae_aa_ae
        long  %0000_0000_0000_0000_0000_0111_1111_1110, $ab_ac_ae_00, $ae_ac_ab_ab, $00_ae_ab_ac
        long  %0000_0000_0000_0000_0000_0011_1111_1100, $aa_ae_00_00, $ae_ab_ab_ab, $00_00_ae_aa
        long  %0000_0000_0000_0000_0000_0011_1111_1100, $aa_ae_00_00, $ae_ab_ab_ab, $00_00_ae_ad
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $ae_00_00_00, $aa_ab_ab_ab, $00_00_00_ae
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $ae_00_00_00, $ab_ab_ab_ab, $00_00_00_ae
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $ae_00_00_00, $ab_ab_ab_aa, $00_00_00_ae
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $ae_00_00_00, $ab_ab_ab_aa, $00_00_00_ae
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $ae_00_00_00, $ab_ab_ab_aa, $00_00_00_ae
        long  %0000_0000_0000_0000_0000_0001_1111_1000, $ae_00_00_00, $ab_ab_ab_ab, $00_00_00_ae
        long  %0000_0000_0000_0000_0000_0011_1111_1100, $ac_ae_00_00, $ab_ab_ab_aa, $00_00_ae_ac
        long  %0000_0000_0000_0000_0000_0011_1111_1100, $ae_ae_00_00, $ae_ae_ae_ae, $00_00_ae_ae

CON
  CHAR_0         = 0
  CHAR_A         = 10
  CHAR_SPACE     = 36
  CHAR_B0        = 36
  CHAR_SATELLITE = 41
  CHAR_DOT       = 42
  CHAR_HYPHEN    = 43
  CHAR_PERCENT   = 44
  CHAR_COLON     = 45
  
DAT
font
        byte  %00111110
        byte  %01100011
        byte  %01100011
        byte  %01100011
        byte  %01100011
        byte  %01100011
        byte  %01100011
        byte  %00111110

        byte  %00011000
        byte  %00011100
        byte  %00011000
        byte  %00011000
        byte  %00011000
        byte  %00011000
        byte  %00011000
        byte  %00111100

        byte  %00111110
        byte  %01100011
        byte  %01100000
        byte  %00111100
        byte  %00000110
        byte  %00000011
        byte  %00000011
        byte  %01111111

        byte  %00111110
        byte  %01100011
        byte  %01100000
        byte  %00111100
        byte  %01100000
        byte  %01100000
        byte  %01100011
        byte  %00111110
        
        byte  %00110000
        byte  %00111000
        byte  %00101100
        byte  %00110110
        byte  %01111111
        byte  %00110000
        byte  %00110000
        byte  %00110000
        
        byte  %01111111
        byte  %00000011
        byte  %00000011
        byte  %00111111
        byte  %01100000
        byte  %01100000
        byte  %01100011
        byte  %00111110
        
        byte  %00111110
        byte  %01100011
        byte  %00000011
        byte  %00111111
        byte  %01100011
        byte  %01100011
        byte  %01100011
        byte  %00111110

        byte  %01111111
        byte  %01100000
        byte  %00110000
        byte  %00011000
        byte  %00001100
        byte  %00001100
        byte  %00001100
        byte  %00001100

        byte  %00111110
        byte  %01100011
        byte  %01100011
        byte  %00111110
        byte  %01100011
        byte  %01100011
        byte  %01100011
        byte  %00111110

        byte  %00111110
        byte  %01100011
        byte  %01100011
        byte  %01111110
        byte  %01100000
        byte  %01100000
        byte  %01100011
        byte  %00111110

        byte  %00111110         ' A
        byte  %01100011
        byte  %01100011
        byte  %01111111
        byte  %01100011
        byte  %01100011
        byte  %01100011
        byte  %01100011

        byte  %00111111         ' B
        byte  %01100011
        byte  %01100011
        byte  %00111111
        byte  %01100011
        byte  %01100011
        byte  %01100011
        byte  %00111111

        byte  %00111110         ' C
        byte  %01100011
        byte  %00000011
        byte  %00000011
        byte  %00000011
        byte  %00000011
        byte  %01100011
        byte  %00111110

        byte  %00111111         ' D
        byte  %01100011
        byte  %01100011
        byte  %01100011
        byte  %01100011
        byte  %01100011
        byte  %01100011
        byte  %00111111

        byte  %01111111         ' E
        byte  %00000011
        byte  %00000011
        byte  %00111111
        byte  %00000011
        byte  %00000011
        byte  %00000011
        byte  %01111111

        byte  %01111111         ' F
        byte  %00000011
        byte  %00000011
        byte  %00111111
        byte  %00000011
        byte  %00000011
        byte  %00000011
        byte  %00000011

        byte  %00111110         ' G
        byte  %01100011        
        byte  %00000011        
        byte  %01110011        
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        
        byte  %00111110        

        byte  %01100011         ' H
        byte  %01100011        
        byte  %01100011        
        byte  %01111111        
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        

        byte  %00011110         ' I
        byte  %00001100        
        byte  %00001100        
        byte  %00001100        
        byte  %00001100        
        byte  %00001100        
        byte  %00001100        
        byte  %00011110        

        byte  %01100000         ' J
        byte  %01100000        
        byte  %01100000        
        byte  %01100000        
        byte  %01100000        
        byte  %01100011        
        byte  %01100011        
        byte  %00111110        

        byte  %01100011         ' K
        byte  %00110011        
        byte  %00011011        
        byte  %00001111        
        byte  %00011011        
        byte  %00110011        
        byte  %01100011        
        byte  %01100011        

        byte  %00000011         ' L
        byte  %00000011        
        byte  %00000011        
        byte  %00000011        
        byte  %00000011        
        byte  %00000011        
        byte  %00000011        
        byte  %01111111        

        byte  %01100011         ' M
        byte  %01110111        
        byte  %01101111        
        byte  %01101011        
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        

        byte  %01100011         ' N
        byte  %01100111        
        byte  %01101111        
        byte  %01111011        
        byte  %01110011        
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        

        byte  %00111110         ' O
        byte  %01100011       
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        
        byte  %00111110        

        byte  %00111111         ' P
        byte  %01100011        
        byte  %01100011        
        byte  %00111111        
        byte  %00000011        
        byte  %00000011        
        byte  %00000011        
        byte  %00000011        

        byte  %00111110         ' Q
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        
        byte  %01101011        
        byte  %01110011        
        byte  %01111110        

        byte  %00111111         ' R
        byte  %01100011        
        byte  %01100011        
        byte  %00111111        
        byte  %00001111        
        byte  %00011011        
        byte  %00110011        
        byte  %01100011        

        byte  %00111110         ' S
        byte  %01100011        
        byte  %00000011        
        byte  %00111110        
        byte  %01100000        
        byte  %01100000        
        byte  %01100011        
        byte  %00111110        

        byte  %01111111         ' T
        byte  %00011000        
        byte  %00011000        
        byte  %00011000        
        byte  %00011000        
        byte  %00011000        
        byte  %00011000        
        byte  %00011000        

        byte  %01100011         ' U
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        
        byte  %00111110        

        byte  %01100011         ' V
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        
        byte  %00110110        
        byte  %00011100        

        byte  %01100011         ' W
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        
        byte  %01100011        
        byte  %01101011        
        byte  %01110111        
        byte  %01100011        

        byte  %01100011         ' X
        byte  %01100011        
        byte  %00110110        
        byte  %00011100        
        byte  %00011100        
        byte  %00110110        
        byte  %01100011        
        byte  %01100011        

        byte  %01100011         ' Y
        byte  %01100011        
        byte  %00111110        
        byte  %00011000        
        byte  %00011000        
        byte  %00011000        
        byte  %00011000        
        byte  %00011000        

        byte  %01111111         ' Z
        byte  %01100000        
        byte  %00110000        
        byte  %00011000        
        byte  %00001100        
        byte  %00000110        
        byte  %00000011        
        byte  %01111111        

        byte  %00000000         ' 36: CHAR_SPACE aka CHAR_B0
        byte  %00000000        
        byte  %00000000        
        byte  %00000000        
        byte  %00000000        
        byte  %00000000        
        byte  %00000000        
        byte  %00000000        

        byte  %00000000         ' 37: CHAR_B1
        byte  %00000110       
        byte  %00000110       
        byte  %00000000        
        byte  %00000000        
        byte  %00000000        
        byte  %00000000        
        byte  %00000000        

        byte  %00000000         ' 38: CHAR_B2        
        byte  %00000110       
        byte  %00000110     
        byte  %00000000        
        byte  %00000000        
        byte  %00000110       
        byte  %00000110       
        byte  %00000000        

        byte  %00000000         ' 39: CHAR_B3        
        byte  %01100110        
        byte  %01100110        
        byte  %00000000        
        byte  %00000000        
        byte  %00000110        
        byte  %00000110        
        byte  %00000000        

        byte  %00000000         ' 40: CHAR_B4        
        byte  %01100110        
        byte  %01100110        
        byte  %00000000        
        byte  %00000000        
        byte  %01100110        
        byte  %01100110        
        byte  %00000000        

        byte  %00001100         ' 41: CHAR_SATELLITE
        byte  %01100110
        byte  %11011010
        byte  %11111000
        byte  %11011010
        byte  %01100110
        byte  %00001100
        byte  %00000000

        byte  %00000000         ' 42: CHAR_DOT
        byte  %00000000
        byte  %00000000
        byte  %00000000
        byte  %00000000
        byte  %00000000
        byte  %00000000
        byte  %00001100
        
        byte  %00000000         ' 43 : CHAR_HYPHEN
        byte  %00000000
        byte  %00000000
        byte  %01111110
        byte  %00000000
        byte  %00000000
        byte  %00000000
        byte  %00000000

        byte  %00100011         ' 44 : CHAR_PERCENT
        byte  %00100011
        byte  %00010000
        byte  %00001000
        byte  %00000100
        byte  %00000010
        byte  %00110001
        byte  %00110001

        byte  %00000000         ' 45 : CHAR_COLON
        byte  %00000000
        byte  %00000000
        byte  %00001100
        byte  %00000000
        byte  %00000000
        byte  %00000000
        byte  %00001100
{        
        byte  %00110000         ' alternate satellites
        byte  %01100110
        byte  %01011011
        byte  %00011111
        byte  %01011011
        byte  %01100110
        byte  %00110000
        byte  %00000000

        byte  %00011100                 
        byte  %00110110        
        byte  %00001000        
        byte  %00011100        
        byte  %01011101        
        byte  %01100011        
        byte  %00110110        
        byte  %00000000        
}

DAT
coords                          ' explosion coordinates
                        byte    $ff,$ff,$0,$ff,$fe,$fe,$0,$0,$fe,$ff,$1,$ff,$1,$0,$1,$fe
                        byte    $ff,$0,$1,$fd,$2,$fc,$2,$0,$1,$1,$2,$1,$2,$fe,$fd,$ff
                        byte    $fd,$fe,$ff,$1,$fe,$1,$3,$1,$fc,$ff,$fe,$0,$0,$fe,$2,$fd
                        byte    $fc,$fd,$fc,$fe,$ff,$fe,$fe,$fd,$0,$1,$4,$1,$fd,$0,$2,$ff
                        byte    $2,$2,$fe,$2,$0,$2,$ff,$2,$fc,$1,$fd,$3,$3,$0,$4,$0
                        byte    $3,$ff,$fe,$fc,$0,$fd,$0,$fc,$1,$2,$3,$fd,$5,$0,$3,$fc
                        byte    $2,$fb,$1,$fc,$4,$ff,$3,$2,$3,$fb,$3,$fe,$ff,$3,$ff,$fd
                        byte    $fe,$3,$6,$0,$0,$fb,$0,$3,$fc,$0,$fd,$fc,$4,$fd,$fb,$fd
                        byte    $fd,$fd,$3,$3,$2,$3,$5,$1,$4,$4,$4,$fb,$fd,$4,$fb,$0
                        byte    $fb,$fe,$fc,$fc,$fd,$5,$4,$2,$5,$ff,$fe,$4,$6,$ff,$4,$fe
                        byte    $1,$fb,$4,$3,$5,$3,$fd,$fb,$fd,$6,$fb,$fc,$fd,$2,$fd,$1
                        byte    $fc,$6,$ff,$fc,$0,$4,$fd,$fa,$3,$fa,$6,$1,$1,$fa,$fa,$fd
                        byte    $7,$2,$fc,$2,$fc,$3,$ff,$4,$7,$ff,$0,$5,$8,$ff,$1,$4
                        byte    $ff,$fb,$ff,$fa,$2,$f9,$fa,$0,$4,$fa,$fb,$2,$7,$fe,$0,$6
                        byte    $ff,$5,$5,$2,$fd,$f9,$fb,$fb,$2,$fa,$f9,$0,$fb,$ff,$fe,$5
                        byte    $9,$ff,$5,$fe,$fc,$fb,$5,$fa,$5,$fc,$4,$fc,$1,$f9,$6,$3
                        byte    $7,$0,$fe,$6,$3,$f9,$1,$3,$1,$5,$2,$f8,$fb,$4,$5,$5
                        byte    $2,$4,$3,$f8,$6,$2,$3,$4,$3,$f7,$4,$5,$8,$0,$ff,$6
                        byte    $fc,$fa,$7,$1,$fa,$fc,$fa,$3,$9,$0,$2,$f7,$5,$6,$fc,$4
                        byte    $fa,$ff,$ff,$7,$fa,$fe,$fa,$fb,$f9,$fb,$7,$3,$5,$4,$ff,$8
                        byte    $fb,$fa,$6,$5,$fb,$3,$fa,$4,$fd,$7,$f9,$fe,$6,$4,$6,$6
                        byte    $f9,$ff,$5,$fd,$f9,$fc,$6,$fd,$fa,$1,$7,$4,$fa,$2,$8,$3
                        byte    $5,$fb,$6,$f9,$f8,$ff,$7,$6,$fb,$1,$6,$fc,$0,$7,$0,$fa
                        byte    $7,$fd,$fe,$fb,$2,$6,$f8,$fe,$1,$f8,$fc,$5,$6,$fe,$8,$2
                        byte    $0,$f9,$8,$fd,$5,$f9,$2,$5,$fb,$5,$3,$6,$fe,$f9,$4,$6
                        byte    $fb,$7,$f9,$1,$f9,$2,$6,$7,$fc,$f9,$fe,$f8,$8,$fe,$9,$fd
                        byte    $ff,$9,$8,$4,$9,$fe,$6,$fa,$fe,$fa,$f8,$1,$3,$5,$4,$f9
                        byte    $ff,$f9,$f9,$4,$f8,$0,$0,$8,$fc,$f8,$f8,$fb,$fe,$f7,$fc,$8
                        byte    $fe,$7,$7,$fc,$f9,$5,$2,$7,$9,$1,$4,$7,$f8,$fd,$0,$f8
                        byte    $1,$6,$fd,$8,$fa,$6,$9,$2,$fd,$9,$1,$f7,$1,$7,$8,$1
                        byte    $fa,$5,$f9,$3,$f7,$fd,$fb,$f9,$3,$7,$1,$8,$f8,$2,$fd,$f8
                        byte    $fb,$6,$fa,$fa,$fb,$f8,$fe,$8,$fc,$f7,$f9,$7,$2,$8,$2,$9
                        byte    $3,$8,$f7,$fe,$fd,$f7,$f7,$fb,$f6,$fd,$fd,$f6,$7,$f9,$7,$5
                        byte    $4,$8,$f9,$fa,$1,$9,$f9,$fd,$f8,$fc,$ff,$f8,$f9,$f9,$f8,$fa
                        byte    $6,$fb,$f7,$1,$0,$f7,$f6,$2,$fc,$7,$5,$f8,$f7,$ff,$1,$f6
                        byte    $fe,$f6,$4,$f8,$f9,$6,$f6,$fe,$0,$9,$fe,$9,$fb,$f7,$fa,$7
                        byte    $2,$f6,$6,$f8,$f8,$4,$fa,$f8,$ff,$f7,$0,$f6,$f7,$2,$7,$fb
                        byte    $8,$fb,$f8,$5,$7,$fa,$f7,$fc,$f6,$ff,$f7,$3,$f6,$1,$f8,$6
                        byte    $fb,$8,$f8,$3,$f9,$f8,$fa,$f9,$f7,$0,$8,$fc,$5,$7
                        word    0

creditStrings
        byte  16,"PLANETARY DEFENSE V0.02",0
        byte  30,"BY MICHAEL PARK",0                                                  
        byte  40,"SEPTEMBER 2007",0                                                    
        byte  70,"BASED ON PLANETARY DEFENSE",0                                         
        byte  80,"FOR THE ATARI 8-BIT",0                                                 
        byte  90,"BY TOM HUDSON IN",0                                                 
        byte 100,"ANALOG COMPUTING NO. 17",0                                           
        byte 110,"MARCH 1984",0                                                         
        byte 0                                                                           