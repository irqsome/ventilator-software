{{
  Planetary Defense rasterizer 0.01
  
        Shifts out the high-color scanline rendered by the renderers.
        
        Planetary Defense-specific stuff:
        Every scan line, it updates the palette (for planet color bands) 
        Does per-frame work during blank lines (sync and overscan):
        - randomize explosion color
        - update mouse variables and crosshairs position
        - update satellite position
        - explosions
        - update score/status display

}}
CON
  
  colorBurstFrq = 3_579_545.0                           ' NTSC color clock frequency in Hz
  plocksPerSecond = colorBurstFrq * 16.0                ' plocks = PLL clocks
  linePlocks    = trunc(0.000_063_5 * plocksPerSecond)  ' total scanline duration (in plocks)
  hsyncPlocks   = trunc(0.000_010_9 * plocksPerSecond)  ' hsync duration
  visiblePlocks = linePlocks - hsyncPlocks              ' duration of visible portion of line

  totalLines    = 262           ' non-interlaced
  syncLines     = 18
  visibleLines  = totalLines - syncLines

CON

  nSprites      = 32
  sizeofSpriteData = 2 ' longs
  {
    word x
    word y
    long pImageInfo
  }
  {
    spriteImageInfo struct:
        byte xHot
        byte yHot
        byte w (multiple of 4, max: 28)
        byte h
        long    lineMask         ┐
        long    lineData[w/4]    ┴─ repeated h times
   
    Per-line transparency masks are precomputed.
  }
  nExplosions   = 32
  sizeofExplosionData = 2 ' longs
  {
    word x
    word y
    long stuff
  }

  sizeofObjectData = 5 ' longs
  {
    objectData struct:
        long timer
        long x                  ' 16.16
        long y                  ' 16.16
        long dx                 ' 16.16        
        long dy                 ' 16.16  
  }
  
  missileAsStart = 0
  nMaxMissileAs = 7
  missileAsEnd = missileAsStart + nMaxMissileAs - 1
  
  missileBsStart = missileAsEnd + 1            
  nMaxMissileBs = 4
  missileBsEnd = missileBsStart + nMaxMissileBs - 1
  
  bombsStart = missileBsEnd + 1
  nMaxBombs = 8
  bombsEnd = bombsStart + nMaxBombs - 1

  UFO = bombsEnd + 1
  
  nObjects = nMaxMissileAs + nMaxMissileBs + nMaxBombs + 1 ' UFO
  SATELLITE = nObjects+0
  CROSSHAIRS = nObjects+1
  nAllObjects = nObjects + 2

  nCoordPairs = 310             ' Must be a multiple of 5.

  JOY_CLK = 16
  JOY_LCH = 17
  JOY_DATA0 = 19
  JOY_DATA1 = 18
  

OBJ
  mouse: "PlanetaryDefense_SNESmouse"

PUB Start( param ) | plocksPerPixel, screenW, screenH, horizontalOffset, VerticalOffset, leftBorderPlocks, rightBorderPlocks, p, _i

' Initialize video stuff.

  screenW        := long[param][5] ' must be divisible by 16
  screenH        := long[param][6]
  plocksPerPixel := long[param][7]
  horizontalOffset := long[param][8]
  verticalOffset   := long[param][9]
  leftBorderPlocks  := (visiblePlocks - screenW * plocksPerPixel) / 2 + horizontalOffset
  rightBorderPlocks := visiblePlocks - screenW * plocksPerPixel - leftBorderPlocks

  nPixels := screenW
  nLines := screenH
  
  sclHsync := ((hsyncPlocks >> 4) << 12) + hsyncPlocks
  sclVisible := visiblePlocks
  sclLeftBorder := leftBorderPlocks
  sclRightBorder := rightBorderPlocks
  scl4Pixels := (plocksPerPixel << 12) + 4 * plocksPerPixel
  overscanLines1 := (visibleLines - screenH) / 2 - verticalOffset
  overscanLines2 := visibleLines - screenH - overscanLines1

' Initialize renderer stuff.

  nRenderers := long[param][1]                   ' For Planetary Defense, nRenderers = 3.
  pBuff0 := long[param][11]                      ' So
  pBuff1 := long[param][12]                      '  three
  pBuff2 := long[param][13]                      '   buffers.  

' Initialize background task stuff.

  ' Start mouse
  mouse.start(JOY_CLK,JOY_LCH,JOY_DATA0,JOY_DATA1,60,2)
  pMouseParams := mouse.GetParamsPointer

  p := param + 15*4
  pSpriteData := p
  p += (1 + nSprites*sizeofSpriteData) << 2
  pButtons := p
  p += 4
  pScreenBase := long[p]
  p += 4
  pExplosionData := p
  p += (nExplosions * sizeofExplosionData) * 4
'  pObjectData := p
  p += (nAllObjects * sizeofObjectData) * 4
  pExplosionCoords := long[p]
  p += 4
  pFontBase := long[p]
  p += 4
  pScannerLine := p
  p += 4
  pStatusData := p
  p += 16*4
  repeat _i from 0 to 3
'    long[@pSatelliteSpriteInfos][_i] := long[p]
    p += 4
  repeat _i from 0 to 3
'    long[@pUfoSpriteInfos][_i] := long[p]
    p += 4
  
  cog :=  cognew(@Rasterizer_Entry, param)

PUB Stop
  cogstop( cog )

VAR
  long cog

PUB BufferAddress
  return @BufferStart
    
DAT
BufferStart
                        org $000
Rasterizer_Entry

             ' VCFG: setup Video Configuration register and 3-bit TV DAC pins to outputs
             
              movs      vcfg, #%0111_0000       ' pinmask  (pin31 ->0000_0111<-pin24), only want upper 3-bits
              movd      vcfg, #1                ' pingroup (Hydra uses group 3, pins 24-31)
              movi      vcfg, #%0_11_1_01_000   ' controls overall setting, we want baseband video on bottom
                                                '  nibble, 2-bit color, enable chroma on broadcast & baseband
              or        dira, tvport_mask       ' set DAC pins to output on pins 24, 25, 26
               
              ' CTRA: setup Frequency to Drive Video
                       
              movi      ctra,#%00001_111        ' pll internally routed to Video, PHSx+=FRQx (mode 1) + pll(16x)
                                                ' needn't set D,S fields since they set pin A/B I/Os,
                                                '  but mode 1 is internal, thus irrelvant
              mov       frqa, freq              ' FREQ such that the final counter output is NTSC and the PLL output is 16*NTSC 
              
Next_Frame
              mov       iBuff, #0
              mov       backgroundTask, #RandomizeExplosionColor
              
              call      #VsyncHigh
              call      #VsyncLow
              call      #VsyncHigh

              mov       status, nRenderers                  ' nrenderers - overscan - 1
              sub       status, overscanLines1
              sub       status, #1
              shl       status, #1              
              wrlong    status, par

              mov       i, overscanLines1
              call      #Overscan

              call      #Visible

              add       vclock, #1
              mov       pPar, par
              add       pPar, #40               ' par[10] is vclock
              wrlong    vclock, pPar

              mov       i, overscanLines2
              call      #Overscan

              jmp       #Next_Frame
              
' Horizontal Scanline Loop
Visible
              mov       i, nLines

:NextScanline
        { A scanline consists of the following parts:
              - hsync
              - left border
              - actual picture
              - right border
        }
              mov       vscl, sclHsync          ' set the video scale register such that 16 serial pixels take
                                                '  39 color clocks (the total time of the hsync and burst)
              waitvid   colorsHsync, pixelsHsync ' send out the pixels and the colors to create the hsync pulse

              call      #Hcolor
              
              rdlong    status, par
              add       status, #2
              andn      status, #1
              wrlong    status, par

              mov       vscl, sclLeftBorder
              waitvid   colorsBorder, #0

              mov       temp, iBuff
              add       temp, #pBuff0
              movs      :pointToBuff, temp
              nop
:pointToBuff  mov       ptr, 0-0                ' ptr = { pBuff0, pBuff1, ... } [ iBuff ]

              add       iBuff, #1
              cmp       iBuff, nRenderers       wz
        if_z  mov       iBuff, #0
        
              mov       vscl, scl4Pixels

              mov       j, nPixels
              sar       j, #2
:loop
              rdlong    colors4Pixels, ptr
              waitvid   colors4Pixels, #%%3210
              add       ptr, #4
              djnz      j, #:loop
              
              mov       vscl, sclRightBorder
              waitvid   colorsBorder, #0

              djnz      i, #:NextScanline

Visible_Ret   ret
                        
' Generate 'HIGH' vsync signal for 6 horizontal lines.
VsyncHigh               
              mov       i, #6
               
:loop         mov       vscl, sclHsync
              waitvid   colorsHsync, pixelsVsyncHigh1

              mov       vscl, sclVisible
              waitvid   colorsHsync, pixelsVsyncHigh2
              jmpret    foregroundTask, backgroundTask
              djnz      i, #:loop
VsyncHigh_Ret ret

' Generate 'LOW' vsync signal for 6 horizontal lines.
VsyncLow
              mov       i, #6
              
:loop         mov       vscl, sclHsync
              waitvid   colorsHsync, pixelsVsyncLow1

              mov       vscl, sclVisible
              waitvid   colorsHsync, pixelsVsyncLow2
              jmpret    foregroundTask, backgroundTask
              djnz      i, #:loop

VsyncLow_Ret ret


Overscan
'       do i times:
        
:loop         mov       vscl, sclHsync
              waitvid   colorsHsync, pixelsHsync

              call      #Hcolor

              rdlong    status, par
              add       status, #2
              or        status, #1
              wrlong    status, par

              mov       vscl, sclVisible
              waitvid   colorsBorder, #0

              jmpret    foregroundTask, backgroundTask
              djnz      i, #:loop
Overscan_Ret  ret                             

'------------ Do per-scanline palette processing.
Hcolor        
              mov       pPalette, par
              add       pPalette, #2*4          ' par[2] is palette
              rdlong    palette, pPalette
              mov       pBasePalette, par
              add       pBasePalette, #14*4     ' par[14] is basePalette
              rdlong    basePalette, pBasePalette

              mov       palette, basePalette
                           
              mov       lineNo, status
              shr       lineNo, #1            
              add       lineNo, #1              ' compute luma of palette color 2 (planet) for line # lineNo

              cmps      lineNo, #8              wc, wz
        if_b  jmp       #:statusColors
              cmps      lineNo, #192-32         wc, wz
        if_b  jmp       #:planetColors
:statusColors                                   ' First 8 and last 32 scanlines are status display.   
              mov       temp, palette           ' Copy color 3 (explosion) into color 2.
              ror       temp, #24
              and       temp, #$ff
              ror       palette, #16
              andn      palette, #$ff
              or        palette, temp
              rol       palette, #16
              jmp       #:_1
:planetColors              
              ror       basePalette, #16        ' Get base color 2 chroma into LSByte.
              and       basePalette, #$f0                           
              ror       palette, #16            ' Get color 2 into LSByte.
              andn      palette, #$ff
              or        palette, basePalette    ' Copy base color 2 chroma into palette.
              rol       palette, #16            ' Return palette to original order.

              mov       temp, lineNo
              shl       temp, #2
              add       temp, lineNo
              sar       temp, #2
              add       temp, #12
              shr       temp, #3
              and       temp, #3
              cmp       temp, #3                wz
        if_z  mov       temp, #1
              add       temp, #$0b              ' luma in [$0b, $0c, $0d ]
              shl       temp, #16
              add       palette, temp           ' Modify luma of palette color 2.
:_1
              rdlong    temp, pScannerLine
              cmp       temp, lineNo            wz
        if_z  wrlong    scannerPalette, pPalette
        if_nz wrlong    palette, pPalette

Hcolor_ret    ret              

DAT ' VARIABLE DECLARATIONS

i             long              $0
j             long              $0
pPar          long              $0
status        long              $0 
pBuff0        long              $0
pBuff1        long              $0
pBuff2        long              $0
pBuff3        long              $0
pBuff4        long              $0
iBuff         long              $0
ptr           long              $0
nRenderers    long              0
nPixels       long              0
nLines        long              0
overscanLines1 long             0
overscanLines2 long             0
vclock        long              0
temp          long              0
lineNo        long              0
palette       long              0
pPalette      long              0
basePalette   long              0
pBasePalette  long              0
scannerPalette long             $00_07_00_8b
xff0000ff     long              $ff0000ff

' tv output DAC port bit mask
tvport_mask   long              %0111_0000 << 8' Ventilator DAC is on bits 12,13,14

sclHsync      long              0
sclVisible    long              0
sclLeftBorder long              0
sclRightBorder long             0
scl4Pixels    long              0

colorsHsync   long              $00_00_02_8A    ' SYNC / SYNC / BLACKER THAN BLACK / COLOR BURST

' hsync pixels
                                ' BP  |BURST|BW|    SYNC      |FP| <- Key BP = Back Porch, Burst = Color Burst, BW = Breezway, FP = Front Porch
pixelsHsync   long              %%1_1_0_0_0_0_1_2_2_2_2_2_2_2_1_1

colors4Pixels long              $4c_0A_0B_0C   ' each 2-bit pixel below references one of these 4 colors, (msb) 3,2,1,0 (lsb)

colorsBorder  long              $00_00_00_02

pixelsVsyncHigh1        long    %%1_1_1_1_1_1_1_1_1_1_1_2_2_2_1_1  
pixelsVsyncHigh2        long    %%1_1_1_1_1_1_1_1_1_1_1_1_1_1_1_1

pixelsVsyncLow1         long    %%2_2_2_2_2_2_2_2_2_2_2_2_2_2_1_1
pixelsVsyncLow2         long    %%1_2_2_2_2_2_2_2_2_2_2_2_2_2_2_2
  
freq          long              192175359 ' colorBurstFrq / 80_000_000.0 * 2^20

DAT
foregroundTask          long    0
backgroundTask          long    0

RandomizeExplosionColor
                        mov     pPar, par
                        add     pPar, #14*4             ' par[14] is basePalette
                        rdlong  palette, pPar           
                         
                        ' Generate random number
                        rol     :randi, #16
                        mov     :i, #32
                        mov     temp, :randi
                        mov     :randi, #0
                        mov     :a, :a0
:loop
                        shr     :a, #1           wc, wz
              if_c      add     :randi, temp
                        shl     temp, #1
              if_nz     djnz    :i, #:loop
                          
                        add     :randi, :b
                        ror     :randi, #16

                        mov     temp, :randi            ' Update color #3 (explosion color).
                        shl     temp, #24
                        shl     palette, #8
                        shr     palette, #8
                        or      palette, temp
                        mov     temp, #$0c
                        shl     temp, #24
                        or      palette, temp
                        wrlong  palette, pPar           ' Write new basePalette.
                        
                        jmpret  backgroundTask, foregroundTask
                        jmp     #UpdateCrosshairs
                                                    
:randi                  long    0
:a                      long    0
:a0                     long    1664525
:b                      long    1013904223    
:i                      long    0
                         
'---------------------- Read mouse, update crosshairs sprite position
UpdateCrosshairs        
                        mov     pPar, pMouseParams
                        rdlong  temp, pPar              ' Get current mouse absolute x,
                        mov     mouseDX, temp           '  compute dx,
                        sub     mouseDX, oldMouseX
                        mov     oldMouseX, temp         '  and save x for next time.
                        and     mouseX, #255
                        add     pPar, #4                ' pMouseParams[1]
                        rdlong  temp, pPar              ' Get current mouse absolute y,
                        mov     mouseDY, temp           '  compute dy,
                        sub     mouseDY, oldMouseY
                        mov     oldMouseY, temp         '  and save y for next time.

                        add     pPar, #8                ' pMouseParams[3] is buttons.
                        rdlong  temp, pPar
                        wrlong  temp, pButtons
                        
                        add     mouseX, mouseDX
                        mins    mouseX, #0
                        maxs    mouseX, #255

                        sub     mouseY, mouseDY
                        mins    mouseY, #0
                        maxs    mouseY, #191

                        mov     sprite, #CROSSHAIRS
                        mov     spriteX, mouseX
                        mov     spriteY, mouseY
                        call    #SetSpritePos
                              
                        jmpret  backgroundTask, foregroundTask
                        jmp     #UpdateSatellite
                                                
pMouseParams            long    0
pButtons                long    0
oldMouseX               long    0
oldMouseY               long    0
mouseX                  long    128
mouseY                  long    96
mouseDX                 long    0
mouseDY                 long    0

DAT
'---------------------- Update satellite sprite position
UpdateSatellite
                        call    #ComputeSin             ' Compute "sine" of theta.
                        mov     satX, sin
                        sar     satX, #10
                        mov     temp, satX
                        sar     temp, #3                ' Reduce by 1/8.
                        sub     satX, temp
                        add     satX, #128

                        add     theta, deg90            ' Now compute "cosine".
                        call    #ComputeSin
                        mov     satY, sin
                        sar     satY, #10
                        mov     temp, satY
                        sar     temp, #3                ' Reduce by 1/8.
                        sub     satY, temp
                        add     satY, #96

                        sub     theta, deg90            ' Revert theta to original value.

                        add     theta, dTheta           ' dTheta is a constant, so we could combine the sub and add.

                        mov     sprite, #SATELLITE
                        mov     spriteX, satX
                        mov     spriteY, satY
                        call    #SetSpritePos
                                  
                        jmpret  backgroundTask, foregroundTask
                        jmp     #DoExplosions

satX                    long    0                       ' Satellite coordinates
satY                    long    0
theta                   long    0                       ' Satellite angular coordinate
dTheta                  long    -30                     ' Satellite angular velocity
deg90                   long    2048                    ' Various constants for ComputeSin
deg180                  long    2048*2
deg270                  long    2048*3
deg360                  long    2048*4
x1fff                   long    $1fff
sinTable                long    $e000                   ' Sin table in ROM
sin                     long    0                       ' Holds result of ComputeSin

ComputeSin
                        mov     temp, theta
                        and     temp, x1fff
                        cmp     temp, deg90             wc, wz
              if_b      jmp     #:lookup
                        cmp     temp, deg180            wc, wz
              if_ae     jmp     #:_1
                        neg     temp, temp
                        add     temp, deg180
                        jmp     #:lookup
:_1                     cmp     temp, deg270            wc, wz
              if_ae     jmp     #:_2
                        sub     temp, deg180
                        jmp     #:negLookup
:_2                     neg     temp, temp
                        add     temp, deg360
:negLookup              shl     temp, #1
                        add     temp, sinTable
                        rdword  sin, temp
                        neg     sin, sin
                        jmp     #ComputeSin_ret

:lookup                 shl     temp, #1
                        add     temp, sinTable
                        rdword  sin, temp
ComputeSin_ret          ret

DAT
SetSpritePos
                        mov     pSprite, sprite
                        shl     pSprite, #1
                        add     pSprite, #1
                        shl     pSprite, #2             ' pSprite = (1 + sprite*sizeofSpriteData)*4
                        add     pSprite, pSpriteData

                        mov     temp, spriteY
                        shl     temp, #16
                        or      temp, spriteX
                        wrlong  temp, pSprite
SetSpritePos_ret        ret

SetSpriteInactive
                        rdlong  active, pSpriteData
                        mov     temp, #1
                        shl     temp, sprite
                        andn    active, temp
                        wrlong  active, pSpriteData
SetSpriteInactive_ret   ret

pSpriteData             long    0
pSprite                 long    0
sprite                  long    0
active                  long    0
spriteX                 long    0
spriteY                 long    0

DAT
'---------------------- Explosion stuff
DoExplosions
                        movs    LoadPixel, #3           ' Explosion consists of color #3.
                        mov     pPar, pExplosionData
                        mov     :i, #nExplosions
:loop                   rdlong  expIndex, pPar          wz
                        ' expIndex is 0, 1..n, n+1..2n, 2n+1
                        ' If expIndex is 0 or 2n+1, this explosion is inactive.
              if_z      jmp     #:skip                  ' expIndex == 0? Skip this explosion.
                        cmp     expIndex, nCoordPairs2 wc, wz
              if_be     jmp     #:do                    
                        mov     expIndex, #0            ' If expIndex is past the end of coords then 
                        wrlong  expIndex, pPar          '  set it to 0 and skip.                     
                        jmp     #:skip                  
:do           
                        add     expIndex, #5            ' We'll be doing 5 points at a time.
                        wrlong  expIndex, pPar
                        sub     expIndex, #6            ' Now expIndex is [0..n-1], [n..2n-1] (0-based)
                        
                        add     pPar, #4
                        rdlong  temp, pPar              ' Get (x,y) pair
                        add     pPar, #4
                        mov     expX, temp
                        and     expX, #$00ff
                        mov     expY, temp
                        shr     expY, #16

                        cmp     expIndex, #nCoordPairs  wc, wz
              if_b      mov     OrAndnInstr, OrInstr
              if_ae     mov     OrAndnInstr, AndnInstr
              if_ae     sub     expIndex, #nCoordPairs  ' Shift expIndex from [n..2n-1] to [0..n-1]
              
                        mov     pCoords, expIndex
                        shl     pCoords, #1             ' Multiply by 2 (2 bytes per coordinate pair)
                        add     pCoords, pExplosionCoords
                        
                        ' Plot/erase 5 pixels
                        mov     :j, #5

:readCoords             rdword  plotX, pCoords
                        
                        mov     plotY, plotX            ' plotX has x in byte 0 (lsb), y in byte 1.         
                        shl     plotX, #24              ' Sign-extend x.
                        sar     plotX, #24
                        shl     plotY, #16              ' Sign-extend y.
                        sar     plotY, #24

                        add     plotX, expX
                        cmp     plotX, #256             wc, wz    ' Ensure plotX is in [0..255]
              if_ae     jmp     #:nextPlot
              
                        add     plotY, expY
                        cmp     plotY, #192             wc, wz    ' Ensure plotY is in [0..191]
              if_b      call    #PlotErase

:nextPlot               add     expIndex, #1
                        add     pCoords, #2
                        djnz    :j, #:readCoords

                        jmp     #:continue
              
:skip                   add     pPar, #sizeofExplosionData*4  ' Increment past index and (x,y).

:continue
                        jmpret  backgroundTask, foregroundTask
                        djnz    :i, #:loop

                        jmpret  backgroundTask, foregroundTask
                        jmp     #UpdateStatusDisplay

:i                      long    0
:j                      long    0
pExplosionData          long    0
pExplosion              long    0
pScreenBase             long    0               ' Points to base of screen bitmap.
pScreen                 long    0               ' Points to a long in screen bitmap.
pExplosionCoords        long    0               ' Points to table of explosion coordinates.
pCoords                 long    0               ' A pointer into the table of explosion coordinates.
expIndex                long    0               ' Index into coordinate pair table.
expX                    long    0
expY                    long    0
plotX                   long    0
plotY                   long    0
plotLong                long    0
shift                   long    0
nCoordPairs2            long    nCoordPairs * 2
expI                    long    0

PlotErase
                        mov     temp, plotX
                        shr     temp, #4
                        mov     pScreen, temp           ' Each column is 192*4=3*64 longs long.
                        add     pScreen, temp
                        add     pScreen, temp           ' pScreen = temp*3
                        shl     pScreen, #6             '                  << 6
                        add     pScreen, plotY          ' +y gives offset in longs.
                        shl     pScreen, #2             ' *4 to convert offset to bytes.
                        add     pScreen, pScreenBase    ' Now pScreen points to the long of interest.
                        rdlong  plotLong, pScreen
                        mov     shift, plotX
                        and     shift, #$0f
                        shl     shift, #1
LoadPixel               mov     temp, #0-0              ' 2-bit value set by caller.
                        shl     temp, shift
OrAndnInstr             nop                             ' Will be either OrInstr or AndnInstr (or XorInstr).
                        wrlong  plotLong, pScreen
PlotErase_ret           ret

OrInstr                 or      plotLong, temp
AndnInstr               andn    plotLong, temp
XorInstr                xor     plotLong, temp

DAT
UpdateStatusDisplay
                        mov     pScreen, pScreenBase
                        mov     pStatus, pStatusData
                        mov     colCount, #32
                        call    #UpdateStatusLine
                        mov     pScreen, pScreenBase
                        add     pScreen, :offsetTo2ndLine
                        mov     colCount, #32
                        call    #UpdateStatusLine

:done                   jmpret  backgroundTask, foregroundTask
                        jmp     #:done
                        
:offsetTo2ndLine        long    (192-32) * 4          

UpdateStatusLine                        
:loop
                        rdbyte  char, pStatus
                        add     pStatus, #1

                        call    #DisplayChar                        

                        test    colCount, #1    wz
              if_nz     add     pScreen, columnOffset
              if_z      add     pScreen, #2
              if_z      jmpret  backgroundTask, foregroundTask ' every other time through...

                        djnz    colCount, #:loop

UpdateStatusLine_ret    ret
                        
DisplayChar
                        mov     pFont, char
                        shl     pFont, #3
                        add     pFont, pFontBase        ' pFont := pFontBase + char * 8
                        
                        mov     rowCount, #8
:displayLoop
                        rdbyte  fontByte, pFont
                        add     pFont, #1

                        mov     temp, fontByte
                        and     temp, #$0f
                        add     temp, #expando
                        movs    :expand0, temp
                        nop
:expand0                mov     fontWord, 0-0           ' fontWord := expando[fontByte&15]

                        ror     fontWord, #8            ' Rotate byte 0 out of the way.

                        shr     fontByte, #4
                        add     fontByte, #expando
                        movs    :expand1, fontByte
                        nop
:expand1                or      fontWord, 0-0           ' fontWord |= expando[fontByte>>4]                                                   

                        rol     fontWord, #8            ' Rotate byte 0 back.

                        andn    fontWord, x0000AAAA

                        rdword  screenWord, pScreen
                        and     screenWord, x0000AAAA
                        or      screenWord, fontWord
                        wrword  screenWord, pScreen
                        add     pScreen, #4

                        djnz    rowCount, #:displayLoop

                        sub     pScreen, #8*4
displayChar_ret         ret

colCount                long    0
rowCount                long    0                
expandCount             long    0
char                    long    0                    
fontByte                long    0
fontWord                long    0
screenWord              long    0
pStatusData             long    0
pStatus                 long    0
pFontBase               long    0
pFont                   long    0
pScannerLine            long    0
columnOffset            long    192*4 - 2
x0000AAAA               long    $0000aaaa
expando                 long    %00000000
                        long    %00000011
                        long    %00001100
                        long    %00001111
                        long    %00110000
                        long    %00110011
                        long    %00111100
                        long    %00111111
                        long    %11000000
                        long    %11000011
                        long    %11001100
                        long    %11001111
                        long    %11110000
                        long    %11110011
                        long    %11111100
                        long    %11111111
RasterizerEnd
                        fit

                        long    $deadbeef[ 256 * 192 / 4 / 4 - (RasterizerEnd - BufferStart) ] 