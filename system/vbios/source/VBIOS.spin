''***************************************
''*  Graphics Demo                      *
''*  Author: Chip Gracey                *
''*  Copyright (c) 2005 Parallax, Inc.  *               
''*  See end of file for terms of use.  *               
''***************************************


CON

  _clkmode = xtal1 + pll16x
  _xinfreq = 5_000_000
  _stack = 128 + ((bitmap2_size)>>2)

  bitmap1_x_tiles = 12
  bitmap1_y_tiles = 12
  bitmap2_x_tiles = 32
  bitmap2_y_tiles = 6
  screen_width = 32
  screen_height = 24
  
  bitmap1_size = bitmap1_x_tiles*bitmap1_y_tiles*64
  bitmap2_size = bitmap2_x_tiles*bitmap2_y_tiles*64
  'display_base = $8000-bitmap_size
  'bitmap_base = display_base-bitmap_size
   bitmap1_base = $8000-bitmap1_size
   bitmap2_base = $8000-bitmap2_size

  LOGOSTACK_SIZE = 128


  spiDO     = 21
  spiClk    = 24
  spiDI     = 20
  spiCS     = 25
  

VAR
  long time,time2,booterror

  long  tv_status     '0/1/2 = off/visible/invisible           read-only
  long  tv_enable     '0/? = off/on                            write-only
  long  tv_pins       '%ppmmm = pins                           write-only
  long  tv_mode       '%ccinp = chroma,interlace,ntsc/pal,swap write-only
  long  tv_screen     'pointer to screen (words)               write-only
  long  tv_colors     'pointer to colors (longs)               write-only               
  long  tv_hc         'horizontal cells                        write-only
  long  tv_vc         'vertical cells                          write-only
  long  tv_hx         'horizontal cell expansion               write-only
  long  tv_vx         'vertical cell expansion                 write-only
  long  tv_ho         'horizontal offset                       write-only
  long  tv_vo         'vertical offset                         write-only
  long  tv_broadcast  'broadcast frequency (Hz)                write-only
  long  tv_auralcog   'aural fm cog                            write-only

  long  vga_status    '0/1/2 = off/visible/invisible      read-only
  long  vga_enable    '0/non-0 = off/on                   write-only
  long  vga_pins      '%pppttt = pins                     write-only
  long  vga_mode      '%tihv = tile,interlace,hpol,vpol   write-only
  long  vga_screen    'pointer to screen (words)          write-only
  long  vga_colors    'pointer to colors (longs)          write-only            
  long  vga_ht        'horizontal tiles                   write-only
  long  vga_vt        'vertical tiles                     write-only
  long  vga_hx        'horizontal tile expansion          write-only
  long  vga_vx        'vertical tile expansion            write-only
  long  vga_ho        'horizontal offset                  write-only
  long  vga_vo        'vertical offset                    write-only
  long  vga_hd        'horizontal display ticks           write-only
  long  vga_hf        'horizontal front porch ticks       write-only
  long  vga_hs        'horizontal sync ticks              write-only
  long  vga_hb        'horizontal back porch ticks        write-only
  long  vga_vd        'vertical display lines             write-only
  long  vga_vf        'vertical front porch lines         write-only
  long  vga_vs        'vertical sync lines                write-only
  long  vga_vb        'vertical back porch lines          write-only
  long  vga_rate      'tick rate (Hz)                     write-only

  long logostack[LOGOSTACK_SIZE]

  word  screen[screen_width * screen_height]

  byte  logocog,bootflag,bootflag2,bootflag3
  'long colors[64]

  

OBJ

  tv    : "tv"
  vga   : "vga"
  gr    : "graphics"
  gr2   : "graphics"
  kb    : "keyboard"
  fat   : "SD-MMC_FATEngine"


PUB start | i, j, k,l, dx, dy, dx2, dy2,dx3,dy3,dx4,dy4,sc,it,dx5,dy5,dx6,dy6

  kb.startx(8,9, %1_111_011, %01_01000)

  'start tv
  longmove(@tv_status, @tvparams, tv#paramcount)
  tv_screen := @screen
  tv_colors := @colorarr_tv
  tv.start(@tv_status)
  longmove(@vga_status, @vgaparams, vga#paramcount)
  vga_screen := @screen
  vga_colors := @colorarr_vga
  vga.start(@vga_status)

  'init colors
  setpalettes(0,0)
  
  colorarr_tv[60]:=$07BC07BC
  colorarr_tv[61]:=$0707BCBC
  colorarr_vga[60]:=$FFC0FFC0
  colorarr_vga[61]:=$FFFFC0C0
  
  colorarr_tv[62]:=$07020702
  colorarr_tv[63]:=$07070202
  colorarr_vga[62]:=$FF03FF03
  colorarr_vga[63]:=$FFFF0303
    

  'init tile screen
  repeat dx from 0 to screen_width - 1
    repeat dy from 0 to screen_width - 2 step 2
      putchar(dx,dy," ",62)
  repeat dx from 0 to constant(bitmap1_x_tiles - 1)
    repeat dy from 0 to constant(bitmap1_y_tiles - 1)
      screen[dy * screen_width + dx + constant(logopos_x+logopos_y*screen_width)] := bitmap1_base >> 6 + dy + dx * bitmap1_y_tiles + ((dy+16) << 10)
  repeat dx from 0 to constant(bitmap2_x_tiles - 1)
    repeat dy from 0 to constant(bitmap2_y_tiles - 1)
      screen[dy * screen_width + dx + constant(rasterpos_y*screen_width)] := bitmap2_base >> 6 + dy + dx * bitmap2_y_tiles + ((dy+28) << 10)

  putstr(0,0,@topstr,62)
  
  'start and setup graphics
  gr2.start
  logocog := cognew(logothread,@logostack)

  'time2 := $7FF0

  repeat

    repeat until tv_status == 1
      
    gr2.setup(bitmap2_x_tiles, bitmap2_y_tiles, bitmap2_x_tiles*8, bitmap2_y_tiles*16-1, bitmap2_base,%%2222222222222222)
    gr2.color(0)
    'gr2.clear
    gr2.tri(0,256,-1024,-100,1024,-100)
    gr2.colorwidth(2,0)
    gr2.plot(-256,0)
    gr2.line(256,0)
    dx4 := (time2*-8) & $FF
    dy4 := (time2*-25) & $FF
    repeat i from 0 to horzlines
      dy := (constant((bitmap2_y_tiles*16-1)<<8) / (((-((i<<8)+dy4))*constant((raster_horizon_z<<8)/horzlines)) )~> 8 ) + raster_horizon_y
      gr2.plot(-256,dy)
      gr2.line(256,dy)

    repeat i from constant(convlines/-2) to constant(convlines/2)
      dx := (((i<<8)+dx4)*constant((bitmap2_x_tiles*16*256)/convlines))
      gr2.plot(dx~>16,0)
      gr2.line(dx~>14,-96)
    
    ' palette changes
    case time2
      $00000: setpalettes(0,0)
      $04000: setpalettes(1,1)
      $0A000: setpalettes(0,0)
      $0C000: setpalettes(3,3)
      $0F000: setpalettes(0,0)
      $1FC00: setpalettes(2,2)
      
    ifnot time2&15
      ifnot time2&16 or not booterror
        putstrc(2,booterror,60)
      elseifnot time2&16 OR (kb.present or time2 < $80)
        putstr(0,2,@kberr,60)
      else
        putstr(0,2,@blankstr,62)
      
    
    case time2
      $00000: putstr(0,16,@crstr,62)
      $00001..$0006F: 'do nothing
      $04000..$043FF:
        case time2&127
          118: putstr(0,16,@blankstr,62)
          0: putstr2(0,16,@poemstrs+(33*((time2>>7)//4)),62)
      $08000..$083FF:
        case time2&127
          118: putstr(0,16,@blankstr,62)
          0: putstr2(0,16,@wlstrs+(33*((time2>>7)//2)),62)
      $0C000: putstr2(0,16,@trstr,62)
      $0C001..$0C175: 'do nothing
      $1FC00..$1FFFF:
        case time2&127
          118: putstr(0,16,@blankstr,62)
          0: putstr2(0,16,@poem2strs+(33*((time2>>7)//8)),62)
      other:
        case time2&127
          118: putstr(0,16,@blankstr,62)
          0: putstr(0,16,@instrstrs+(33*((time2>>7)//8)),62)

    case kb.key
      "T"," ",$0D: tryboot(string("OS.BIN"))
      "V": tryboot(string("OSVGA.BIN"))
      "X": tryboot(string("OSTERM.BIN"))
      

    if bootflag2 and not bootflag
      cogstop(logocog)
      longfill(@logostack,0,LOGOSTACK_SIZE)
      coginit(logocog,logothread,@logostack)
      bootflag2~

    ' print time
    'puthex(0,0,time2,5,62)
    if bootflag
      putchar (0,4,"1",60)
    else
      putchar (0,4," ",62)
    if bootflag2
      putchar (0,6,"2",60)
    else
      putchar (0,6," ",62)

    'increment counter that makes everything change
    time2++
    time2 &= $1FFFF ' a bit over an hour

PRI tryboot(fname)
ifnot bootflag or bootflag2
   bootflag~~
   bootflag2~~
   bootflag3~~
   booterror~
   
   'cogstop(logocog)
   repeat while bootflag3
   coginit(logocog,tryboot2(fname),@logostack)
   

PRI tryboot2(fname)
  booterror := \tryboot3(fname)
  fat.FATEngineStop
  bootflag~
  repeat

PRI tryboot3(fname)
  fat.FATEngineStart(spiDO,spiClk,spiDI,spiCS,-1,-1,-1,-1,-1)
  fat.mountPartition(0)
  fat.bootPartition(fname)
  return string("Unknown Error")
  
  
PRI setpalettes(p1,p2) : i | off
  repeat i from 0 to 15
    colorarr_tv[i] := $00001010 * (i+4) & $F + $2B060802
    colorarr_vga[i] := $00001010 * (i+4) & $F + $2B060802
  off := p1*12
  repeat i from 0 to 11
    colorarr_tv[i+16] := (byte[@colrlines_tv][i+off]*$1000100) + $00020002
    colorarr_vga[i+16] := (byte[@colrlines_vga][i+off]*$1000100) + $00030003
  off := p2*6
  repeat i from 0 to 5
    colorarr_tv[i+28] := (byte[@colrlines2_tv][i+off]*$1010000) + $0000202
    colorarr_vga[i+28] := (byte[@colrlines2_vga][i+off]*$1010000) + $0000303

PRI putchar(x,y,chr,color) : odd
  x <#= screen_width-1
  y <#= screen_height-2
  odd := chr&1
  chr -= odd
  chr += (odd+color)<<10
  y *= screen_width             
  screen[x+y] := constant($8000>>6)+chr
  screen[x+y+screen_width] := constant(1+($8000>>6))+chr

PRI putstr(x,y,str,color)
  x--
  repeat while result:=byte[str++]
    x++
    putchar(x,y,result,color)

PRI putstr2(x,y,str,color) | z
  x--
  z~~
  repeat while result:=byte[str++]
    putchar(++x,y,(((x[z]*%%10000)^(clkmode<-(posx~>%%131)))<<%%100)>>%%120,color)
PRI putstrml(x,y,str,color)
  x--
  repeat while result:=byte[str++]
    x++
    repeat while x => screen_width
      x -= screen_width
      y+=2
    putchar(x,y,result,color)
PRI putstrc(y,str,color) : x | len
  len := strsize(str) <# 32
  repeat (32-len)/2
    putchar(x++,y," ",color)
  putstr(x,y,str,color)
  x+=len
  repeat (33-len)/2
    putchar(x++,y," ",color)
  

PRI puthex(x,y,num,digits,color)
  x+=digits
  repeat digits 
    putchar(--x,y,lookupz(num&$F:"0".."9","A".."F"),color)
    num >>= 4

PRI cos(angle)
  return sin(angle+$0800)

PRI sin(angle) : s | z
                               'angle: 0..8192 = 360°
  z := angle & $1000
  if angle & $800
    angle := -angle
  angle |= $E000>>1
  angle <<= 1
  s := word[angle]
  if z
    s := -s                    ' return sin = -$FFFF..+$FFFF    

PRI logothread | i, j, k,l, dx, dy, dx2, dy2,dx3,dy3,dx4,dy4,sc,it,dx5,dy5,dx6,dy6
  gr.start
  repeat               
    'clear bitmap
    'gr.clear
    gr.setup(bitmap1_x_tiles, bitmap1_y_tiles, bitmap1_x_tiles*8, bitmap1_y_tiles*8, bitmap1_base,%%1111111111111111)

    
    'draw spinning triangles
    repeat i from 1 to 8
      'gr.vec(0, 0, (time & $7F) << 3 + i << 5, time << 6 + i << 8, @vecdef)

    'gr.vec(0,0,$180,k << 3,@vlogo_vec)
    repeat it from 0 to 15
      gr.colorwidth(1,0)
      j := ((it-1)<<9)-(time<<6)
      k := ((it)<<9)-(time<<6)
      sc := trunc(sinmax/(logorad*1.0))
      dx := sin(j)/sc
      dy := cos(j)/sc
      sc := trunc(sinmax/(logorad*1.12))
      dx2 := sin(k)/sc 
      dy2 := cos(k)/sc
      
      gr.quad(dx,dy,dx2,dy2,dx6,dy6,dx5,dy5)
      longmove(@dx5,@dx,4)
      'gr.tri(0,0,sin(i<<8)/1000,cos(i<<8)/1000,sin((i+1)<<8)/1000,cos((i+1)<<8)/1000)
    'repeat i from 0 to 7
      ifnot it&1
        i:=(it>>1)
        gr.colorwidth(i&1,0)
        repeat l from 0 to 3
          sc := l*bladebend
          j := ((i)<<10)+sc-(time<<6)
          k := ((i+1)<<10)+sc-(time<<6)
          sc := lookupz(l:trunc(sinmax/(logorad*0.25)),trunc(sinmax/(logorad*0.5)),trunc(sinmax/(logorad*0.75)),trunc(sinmax/(logorad*1.0)))
          dx := sin(j)/sc
          dy := cos(j)/sc
          dx2 := sin(k)/sc
          dy2 := cos(k)/sc               
          ifnot l
            gr.tri(0,0,dx,dy,dx2,dy2)
          else
            gr.quad(dx,dy,dx2,dy2,dx4,dy4,dx3,dy3)
          longmove(@dx3,@dx,4)

    time++
    if bootflag3
      bootflag3~
      repeat

  
CON

 PLOT = $4000
 LINE = $8000

 sinmax = float($ffff)
 logorad = 86.0
 bladebend = 171

 logopos_x = 10
 logopos_y = 4

 rasterpos_y = 18

 raster_horizon_z = 4
 raster_horizon_y = 24
 horzlines = 4

 convlines = 16
 

DAT

tvparams                long    0               'status
                        long    1               'enable
                        long    %001_0101       'pins
                        long    %0010           'mode
                        long    0               'screen
                        long    0               'colors
                        long    screen_width    'hc
                        long    screen_height   'vc
                        long    5               'hx
                        long    1               'vx
                        long    0               'ho
                        long    0               'vo
                        long    0               'broadcast
                        long    0               'auralcog


vgaparams               long    0               'status
                        long    1               'enable
                        long    %000_111        'pins
                        long    %011            'mode
                        long    0               'videobase
                        long    0               'colorbase
                        long    screen_width    'hc
                        long    screen_height   'vc
                        long    1               'hx
                        long    1               'vx
                        long    0               'ho
                        long    0               'vo
                        long    512             'hd
                        long    12              'hf
                        long    88              'hs
                        long    32              'hb
                        long    388             'vd
                        long    11              'vf
                        long    2               'vs
                        long    31              'vb
                        long    20_000_000      'rate
                        

colorarr_tv             long $07_05_03_02[64]
colorarr_vga            long $FF_AE_57_03[64]

colrlines_tv            byte $2B,$2B,$2B,$2B ' normal
                        byte $2B,$2B,$2B,$2B
                        byte $2B,$2B,$2B,$2B
                        
                        byte $AE,$AE,$AE,$BD ' sunrise
                        byte $BD,$BD,$CC,$CC
                        byte $CC,$DB,$DB,$CB

                        byte $03,$03,$03,$03 ' black
                        byte $03,$03,$03,$03
                        byte $03,$03,$03,$03

                        byte $DE,$DE,$DE,$3E 
                        byte $3E,$07,$07,$3E
                        byte $3E,$DE,$DE,$DE


colrlines_vga           byte $2B,$2B,$2B,$2B ' normal
                        byte $2B,$2B,$2B,$2B
                        byte $2B,$2B,$2B,$2B
                        
                        byte $FB,$FB,$FB,$D7  ' sunrise
                        byte $D7,$D7,$C7,$C7
                        byte $C7,$87,$87,$83

                        byte $57,$57,$57,$57  ' black
                        byte $57,$57,$57,$57
                        byte $57,$57,$57,$57

                        byte $C3,$C3,$E3,$E3
                        byte $F3,$F3,$23,$23
                        byte $0F,$0F,$47,$47


colrlines2_tv           byte $3B,$3C,$4C,$BC
                        byte $9D,$CC
                        
                        byte $CC,$9D,$BC,$4C
                        byte $3C,$3B

                        byte $07,$07,$07,$07
                        byte $06,$06

                        'byte $48,$48,$28,$28
                        'byte $9E,$9E
                        'byte $38,$AD,$9E,$E8 
                        'byte $88,$EC
                        byte $EC,$88,$E8,$9E 
                        byte $AD,$38
                        'byte $AD,$AD,$AD,$AD 
                        'byte $AD,$EC
                        'byte $07,$07,$3E,$3E 
                        'byte $DE,$DE


colrlines2_vga          byte $17,$6B,$2B,$97
                        byte $A7,$C7
                        
                        byte $87,$A7,$97,$2B
                        byte $6B,$17

                        byte $FF,$FF,$FF,$FF
                        byte $AB,$AB
                        
                        'byte $17,$6B,$2B,$97
                        'byte $A7,$87
                        
                        'byte $48,$48,$28,$28
                        'byte $9E,$9E
                        byte $CF,$3F,$FF,$3F
                        byte $CF,$CF  

{
vlogo_vec               'circle
                        word  PLOT+$0000
                        word  50
                        word  LINE+$0100
                        word  50
                        word  LINE+$0200
                        word  50
                        word  LINE+$0300
                        word  50
                        word  LINE+$0400
                        word  50
                        word  LINE+$0500
                        word  50
                        word  LINE+$0600
                        word  50
                        word  LINE+$0700
                        word  50
                        word  LINE+$0800
                        word  50
                        word  LINE+$0900
                        word  50
                        word  LINE+$0A00
                        word  50
                        word  LINE+$0B00
                        word  50
                        word  LINE+$0C00
                        word  50
                        word  LINE+$0D00
                        word  50
                        word  LINE+$0E00
                        word  50
                        word  LINE+$0F00
                        word  50
                        word  LINE+$1000
                        word  50
                        word  LINE+$1100
                        word  50
                        word  LINE+$1200
                        word  50
                        word  LINE+$1300
                        word  50
                        word  LINE+$1400
                        word  50
                        word  LINE+$1500
                        word  50
                        word  LINE+$1600
                        word  50
                        word  LINE+$1700
                        word  50
                        word  LINE+$1800
                        word  50
                        word  LINE+$1900
                        word  50
                        word  LINE+$1A00
                        word  50
                        word  LINE+$1B00
                        word  50
                        word  LINE+$1C00
                        word  50
                        word  LINE+$1D00
                        word  50
                        word  LINE+$1E00                                
                        word  50
                        word  LINE+$1F00
                        word  50
                        word  LINE+$0000
                        word  50

                        word  LINE+$1000
                        word  50
                        word  LINE+$0000
                        word  50
                        
                        word  0 }


topstr byte  "            V - BIOS            ",0

kberr  byte  "         Keyboard Error         ",0


blankstr
       byte  "                                ",0

instrstrs
crstr  byte  "    (C)2020 IRQsome Software    ",0
       byte  "     Press T to boot OS.BIN     ",0
       byte  "    Press V to boot OSVGA.BIN   ",0 
       byte  "   Press X to boot OSTERM.BIN   ",0
       'byte  "   Press F1..F4 to boot EEPROM  ",0
       byte  "      (EEPROM boot is NYI)      ",0 
       byte  "               ..               ",0 
       byte  "              ....              ",0 
       byte  "       V-BIOS Version 0.3       ",0  

wlstrs
       byte  "#########Glm$w#zlv#kbuf#########",0
       byte  "#####bmzwkjmd#afwwfq#wl#gl<#####",0

trstr
       byte  "#Wqbmp#qjdkwp#bqf#kvnbm#qjdkwp",34,0



poemstrs
       byte  "####Wkf#qjpjmd#pvm#`bpwp#btbz###",0
       byte  "##Wkf#pkbgltp#le#zfbqp#dlmf#az##",0
       byte  "#Wkf#tlqog#ojw#jm#mv`ofbq#ojdkw#",0
       byte  "#######QFBGZ-#wl#af#wbhfm-######",0

poem2strs
       byte  "Wkf#eolt#le#wjnf#jp#botbzp#`qvfo",0
       byte  "######Fufqzwkjmd#nvpw#fmg-######",0
       byte  "#####Ojef#vmgfq#b#aob`h#pvm/####",0
       byte  "#Mflm#eoltjmd#wkqlvdk#zlvq#ufjmp",0
       byte  "######Lmf#tjwk#wkf#nb`kjmf-#####",0
       byte  "#Pwbmgjmd#lm#wkf#kjoo-#Tbjwjmd-#",0
       byte  "########Wkfqf$p#ml#slfwqz#######",0 
       byte  "######jm#nffwjmd#jm#wkf#gbz-####",0

{{

┌──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
│                                                   TERMS OF USE: MIT License                                                  │                                                            
├──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤
│Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    │ 
│files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    │
│modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software│
│is furnished to do so, subject to the following conditions:                                                                   │
│                                                                                                                              │
│The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.│
│                                                                                                                              │
│THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          │
│WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         │
│COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   │
│ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         │
└──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
}}