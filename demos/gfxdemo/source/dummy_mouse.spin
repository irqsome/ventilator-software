'' Dummy mouse driver


PUB start(clk,data) : okay


  return true


PUB stop


PUB present : type

'' Check if mouse present - valid ~2s after start
'' returns mouse type:
''
''   3 = five-button scrollwheel mouse
''   2 = three-button scrollwheel mouse
''   1 = two-button or three-button mouse
''   0 = no mouse connected

  type := 0


PUB button(b) : state

'' Get the state of a particular button
'' returns t|f

  return false


PUB buttons : states

'' Get the states of all buttons
'' returns buttons:
''
''   bit4 = right-side button
''   bit3 = left-side button
''   bit2 = center/scrollwheel button
''   bit1 = right button
''   bit0 = left button

   return false  


PUB abs_x : ax

'' Get absolute-x

  ax := 0


PUB abs_y : ay

'' Get absolute-y

  ay := 0


PUB abs_z : az

'' Get absolute-z (scrollwheel)

  az := 0


PUB delta_x : dx        | newx

'' Get delta-x

  return 0


PUB delta_y : dy        | newy

'' Get delta-y

  return 0


PUB delta_z : dz        | newz

'' Get delta-z (scrollwheel)

  return 0


PUB  delta_reset

