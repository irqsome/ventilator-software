PUB melody
  return @MUSIC

DAT 
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
'                                     Music header
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
              word  1
MUSIC         word  0 
'──────────────────────────────────────────────────────────────────────────────────────────        
INS_INIT      word  @TRI_PERC      -@INS_INIT  
              word  @NOISE_PERC    -@INS_INIT
              word  @SQR_BEEP      -@INS_INIT
              word  @SAW_LEAD      -@INS_INIT
              word  @SAW_PIANO     -@INS_INIT 
              word  @SQR_STACC     -@INS_INIT 
              word  END                                  
'────────────────────────────────────────────────────────────────────────────────────────── 
SEQ_INIT      word  @PATTSEQ_BASS                 -@INS_INIT   ' Square wave channel 1
              word  0                                          ' Square wave channel 2
              word  @PATTSEQ_LEAD                 -@INS_INIT   ' Square wave channel 3 
              word  @PATTSEQ_COORD_VOICE1_1       -@INS_INIT   ' Saw wave channel 2 
              word  @PATTSEQ_COORD_VOICE2_1       -@INS_INIT   ' Saw wave channel 3 
              word  @PATTSEQ_COORD_VOICE3_1       -@INS_INIT   ' Saw wave channel 1  
              word  @PATTSEQ_PERCUSSION2_TRIANGLE -@INS_INIT   ' Triangle channel
              word  @PATTSEQ_PERCUSSION1_NOISE    -@INS_INIT   ' Noise channel
                         
DAT
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
'                               Pattern sequency data
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
  long 1

PATTSEQ_COORD_VOICE1_1
'{
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS      -@INS_INIT)    
  long (OCTAVE *6) | (NOTE *10)              | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)  
  long (OCTAVE *6) | (NOTE *8)               | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8)               | (INSTR *4) | (@PATT_COORD_TAPS      -@INS_INIT)    
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)  
  long (OCTAVE *6) | (NOTE *7)               | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS      -@INS_INIT)    
  long (OCTAVE *6) | (NOTE *10)              | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)  
  long (OCTAVE *6) | (NOTE *8)               | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8)               | (INSTR *4) | (@PATT_COORD_TAPS      -@INS_INIT)    
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)  
  long (OCTAVE *6) | (NOTE *7)               | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)
'}
'{
  ' Part 2
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
'}
  ' Part 3
'{
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
'}
  ' Part 4
'{
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS        -@INS_INIT)
  long (OCTAVE *6) | (NOTE *7 )              | (INSTR *4) | (@PATT_COORD_TAPS        -@INS_INIT)
  long (OCTAVE *6) | (NOTE *10)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS        -@INS_INIT)
  long (OCTAVE *6) | (NOTE *7 )              | (INSTR *4) | (@PATT_COORD_TAPS        -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
'}
  ' Part 5
'{
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8 )              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
'}
  long  END
  
PATTSEQ_COORD_VOICE2_1
'{
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_COORD_TAPS      -@INS_INIT)    
  long (OCTAVE *6) | (NOTE *3)               | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)  
  long (OCTAVE *6) | (NOTE *3)               | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_COORD_TAPS      -@INS_INIT)    
  long (OCTAVE *7) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)  
  long (OCTAVE *6) | (NOTE *3)               | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_COORD_TAPS      -@INS_INIT)    
  long (OCTAVE *6) | (NOTE *3)               | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)  
  long (OCTAVE *6) | (NOTE *3)               | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_COORD_TAPS      -@INS_INIT)    
  long (OCTAVE *7) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)  
  long (OCTAVE *6) | (NOTE *4)               | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)
'}
'{   
  ' Part 2 
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE  -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE  -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE  -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE  -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE2 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE2 -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE2 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE2 -@INS_INIT) 
'}
'{
  ' Part 3
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4 -@INS_INIT)   
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4 -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4 -@INS_INIT)   
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4 -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4 -@INS_INIT)
'}
'{
  ' Part 4
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4_SHORT     -@INS_INIT)   
  long (OCTAVE *6) | (NOTE *3)               | (INSTR *4) | (@PATT_LEAD4_SHORT_VAR -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4           -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4_SHORT     -@INS_INIT)   
  long (OCTAVE *6) | (NOTE *3)               | (INSTR *4) | (@PATT_LEAD4_SHORT_VAR -@INS_INIT)  
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4           -@INS_INIT) 
'}
'{
  ' Part 5
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4 -@INS_INIT)   
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4 -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4 -@INS_INIT)   
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4 -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_LEAD4 -@INS_INIT)
'}
  long  END
  
PATTSEQ_COORD_VOICE3_1
'{
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS      -@INS_INIT)    
  long (OCTAVE *6) | (NOTE *7)               | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)  
  long (OCTAVE *6) | (NOTE *10)              | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS      -@INS_INIT)    
  long (OCTAVE *6) | (NOTE *10)              | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)  
  long (OCTAVE *6) | (NOTE *10)              | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS      -@INS_INIT)    
  long (OCTAVE *6) | (NOTE *7)               | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)  
  long (OCTAVE *6) | (NOTE *10)              | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS      -@INS_INIT)    
  long (OCTAVE *6) | (NOTE *10)              | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)  
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_HALF -@INS_INIT)
'}
'{   
  ' Part 2  
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT) 
'}
  ' Part 3
'{
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
'}
  ' Part 4
'{
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS        -@INS_INIT)
  long (OCTAVE *6) | (NOTE *10)              | (INSTR *4) | (@PATT_COORD_TAPS        -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS        -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *10)              | (INSTR *4) | (@PATT_COORD_TAPS        -@INS_INIT)
  long (OCTAVE *5) | (NOTE *5)               | (INSTR *4) | (@PATT_COORD_TAPS        -@INS_INIT)  
  long (OCTAVE *5) | (NOTE *0)               | (INSTR *4) | (@PATT_COORD_TAPS        -@INS_INIT) 
'}
  ' Part 5
'{
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT) 
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
  long (OCTAVE *6) | (NOTE *12)              | (INSTR *4) | (@PATT_COORD_TAPS_DOUBLE -@INS_INIT)
'}
  long  END
DAT
PATTSEQ_PERCUSSION1_NOISE
'{
  long  SET_TEMPO|(((BPM*105)/8)&$FFF00000)  | (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)    
  long                                         (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)
'}
'{
  ' Part 2 
  long  SET_TEMPO|(((BPM*105)/8)&$FFF00000)  | (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)    
  long                                         (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)
  long                                         (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)    
  long                                         (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)
'}
  ' Part 3
'{
  long  SET_TEMPO|(((BPM*105)/8)&$FFF00000)  | (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)    
  long                                         (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)
  long                                         (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)    
  long                                         (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)
'}
  ' Part 4
'{
  long  SET_TEMPO|(((BPM*105)/8)&$FFF00000)  | (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)    
  long                                         (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)
  long                                         (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)    
  long                                         (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)
'}
  ' Part 5
'{
  long  SET_TEMPO|(((BPM*105)/8)&$FFF00000)  | (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)    
  long                                         (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)
  'long                                         (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)    
  'long                                         (INSTR *1) | (@PATT_PERCUSSION1_NOISE    -@INS_INIT)
'}
  long  END
DAT
PATTSEQ_PERCUSSION2_TRIANGLE
'{
  long  SET_TEMPO|(((BPM*105)/8)&$FFF00000)  | (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)    
  'long                                         (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)  
'}
  ' Part 2
'{
  long  SET_TEMPO|(((BPM*105)/8)&$FFF00000)  | (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)    
  long                                         (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)  
  'long                                         (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)    
  'long                                         (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)
'}
  ' Part 3
'{
  long  SET_TEMPO|(((BPM*105)/8)&$FFF00000)  | (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)    
  long                                         (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)  
  'long                                         (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)    
  'long                                         (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)
'}

  ' Part 4
'{
  long  SET_TEMPO|(((BPM*105)/8)&$FFF00000)  | (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)    
  long                                         (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)  
  'long                                         (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)    
  'long                                         (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)
'}
  ' Part 5
'{
  long  SET_TEMPO|(((BPM*105)/8)&$FFF00000)  | (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)    
  'long                                         (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)  
  'long                                         (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)    
  'long                                         (INSTR *0) | (@PATT_PERCUSSION2_TRIANGLE -@INS_INIT)
'}
  long  END

DAT
PATTSEQ_LEAD
'{
  long (OCTAVE *6) | (NOTE *7)               | (INSTR *3) | (@PATT_LEAD1 -@INS_INIT)    
  long (OCTAVE *6) | (NOTE *7)               | (INSTR *3) | (@PATT_LEAD1 -@INS_INIT)  
  long (OCTAVE *6) | (NOTE *7)               | (INSTR *3) | (@PATT_LEAD2 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *7)               | (INSTR *3) | (@PATT_LEAD3 -@INS_INIT)  
'}
  ' Part 2
'{
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *5) | (@PATT_LEAD4 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *5) | (@PATT_LEAD4 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *5) | (@PATT_LEAD4 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *5) | (@PATT_LEAD4 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *5) | (@PATT_LEAD4 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *5) | (@PATT_LEAD4 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *5) | (@PATT_LEAD4 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *5) | (@PATT_LEAD4 -@INS_INIT)
'}
  ' Part 3
'{
  long (OCTAVE *5) | (NOTE *5)               | (INSTR *3) | (@PATT_LEAD5 -@INS_INIT)
  long (OCTAVE *5) | (NOTE *0)               | (INSTR *3) | (@PATT_LEAD6 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *3) | (@PATT_LEAD7 -@INS_INIT)
  long (OCTAVE *5) | (NOTE *5)               | (INSTR *3) | (@PATT_LEAD5 -@INS_INIT)
  long (OCTAVE *5) | (NOTE *0)               | (INSTR *3) | (@PATT_LEAD8 -@INS_INIT)
  long (OCTAVE *5) | (NOTE *5)               | (INSTR *3) | (@PATT_LEAD7 -@INS_INIT)
'}
  ' Part 4
'{
  long (OCTAVE *6) | (NOTE *8)               | (INSTR *3) | (@PATT_LEAD9 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *8)               | (INSTR *3) | (@PATT_LEAD9 -@INS_INIT)
'}
  ' Part 5
'{
  long (OCTAVE *5) | (NOTE *5)               | (INSTR *3) | (@PATT_LEAD5 -@INS_INIT)
  long (OCTAVE *5) | (NOTE *0)               | (INSTR *3) | (@PATT_LEAD6 -@INS_INIT)
  long (OCTAVE *6) | (NOTE *5)               | (INSTR *3) | (@PATT_LEAD7 -@INS_INIT)
  long (OCTAVE *5) | (NOTE *5)               | (INSTR *3) | (@PATT_LEAD5 -@INS_INIT)
  long (OCTAVE *5) | (NOTE *0)               | (INSTR *3) | (@PATT_LEAD8 -@INS_INIT)
  long (OCTAVE *5) | (NOTE *5)               | (INSTR *3) | (@PATT_LEAD7 -@INS_INIT)
'}
  long END

DAT
PATTSEQ_BASS
'{
  long (OCTAVE *9) | (NOTE *5)               | (INSTR *2) | (@PATT_BASS -@INS_INIT)    
  long (OCTAVE *9) | (NOTE *3)               | (INSTR *2) | (@PATT_BASS -@INS_INIT)  
  long (OCTAVE *9) | (NOTE *1)               | (INSTR *2) | (@PATT_BASS -@INS_INIT)
  long (OCTAVE *9) | (NOTE *0)               | (INSTR *2) | (@PATT_BASS -@INS_INIT)  
  long (OCTAVE *9) | (NOTE *5)               | (INSTR *2) | (@PATT_BASS -@INS_INIT)    
  long (OCTAVE *9) | (NOTE *3)               | (INSTR *2) | (@PATT_BASS -@INS_INIT)  
  long (OCTAVE *9) | (NOTE *1)               | (INSTR *2) | (@PATT_BASS -@INS_INIT)
  long (OCTAVE *9) | (NOTE *0)               | (INSTR *2) | (@PATT_BASS -@INS_INIT)
'}
  ' Part 2    
'{
  long (OCTAVE *9) | (NOTE *5)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *1)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *0)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *5)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)
  long (OCTAVE *9) | (NOTE *5)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *1)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *0)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *5)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)
'}
  ' Part 3
'{   
  long (OCTAVE *9) | (NOTE *5)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *1)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *0)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *5)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)
  long (OCTAVE *9) | (NOTE *5)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *1)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *0)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *5)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)
'}
  ' Part 4
'{   
  long (OCTAVE *9) | (NOTE *1)               | (INSTR *2) | (@PATT_BASS -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *3)               | (INSTR *2) | (@PATT_BASS -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *5)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *1)               | (INSTR *2) | (@PATT_BASS -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *3)               | (INSTR *2) | (@PATT_BASS -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *5)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)
'}
  ' Part 5
'{   
  long (OCTAVE *9) | (NOTE *5)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *1)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *0)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *5)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)
  long (OCTAVE *9) | (NOTE *5)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *1)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *0)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)   
  long (OCTAVE *9) | (NOTE *5)               | (INSTR *2) | (@PATT_BASS_LONG -@INS_INIT)
'}
  'long END
  long RESTART

DAT
'──────────────────────────────────────────────────────────────────────────────────────────
'                                  Pattern data
'──────────────────────────────────────────────────────────────────────────────────────────
PATT_LEAD1               byte ___             | 3
                         byte ((12    ) << 3) | 1
                         byte ((12 + 1) << 3) | 1
                         byte ((12 - 1) << 3) | 3
                         byte ((12 - 2) << 3) | 1
                         byte ((12 + 7) << 3) | 7
                         byte ___             | 7
                         byte ___             | 5
                         byte ((12 - 5) << 3) | 1
                         byte ((12 + 1) << 3) | 1
                         byte ((12 - 1) << 3) | 3
                         byte ((12 - 2) << 3) | 1
                         byte ((12 + 7) << 3) | 5
                         byte ((12 - 2) << 3) | 3
                         byte ((12 - 2) << 3) | 3
                         byte ((12 - 1) << 3) | 3
                         byte END

PATT_LEAD2               byte ___             | 3
                         byte ((12    ) << 3) | 1
                         byte ((12 + 1) << 3) | 1
                         byte ((12 - 1) << 3) | 3
                         byte ((12 - 2) << 3) | 1
                         byte ((12 + 12)<< 3) | 7
                         byte ___             | 7
                         byte ___             | 5
                         byte ((12 - 10)<< 3) | 1
                         byte ((12 + 1) << 3) | 1
                         byte ((12 - 1) << 3) | 3
                         byte ((12 - 2) << 3) | 1
                         byte ((12 + 12)<< 3) | 5
                         byte ((12 + 2) << 3) | 3
                         byte ((12 - 4) << 3) | 7
                         byte END

PATT_LEAD3               byte ___             | 3
                         byte ((12    ) << 3) | 1
                         byte ((12 + 1) << 3) | 1
                         byte ((12 - 1) << 3) | 3
                         byte ((12 - 2) << 3) | 1
                         byte ((12 + 12)<< 3) | 3
                         byte ((12 + 2) << 3) | 3
                         byte ((12 - 2) << 3) | 5
                         byte ((12 - 1) << 3) | 3
                         byte ((12 + 1) << 3) | 5
                         byte ((12 + 0) << 3) | 3
                         byte ((12 + 0) << 3) | 5
                         byte ((12 - 1) << 3) | 5
                         byte ((12 - 4) << 3) | 5
                         byte ((12 - 2) << 3) | 3        
                         byte END

PATT_LEAD4               byte ___             | 5
                         byte ((12    ) << 3) | 5 
                         byte ((12 - 5) << 3) | 3
                         byte ((12 - 2) << 3) | 3
                         byte ((12 + 9) << 3) | 5  
                         byte ((12 + 1) << 3) | 3  
                         byte ((12 - 3) << 3) | 1
PATT_LEAD4_SHORT         byte ___             | 5
                         byte ((12    ) << 3) | 5 
                         byte ((12 - 5) << 3) | 3
                         byte ((12 - 2) << 3) | 3
                         byte ((12 + 9) << 3) | 5  
                         byte ((12 + 1) << 3) | 3  
                         byte ((12 - 3) << 3) | 1
                         byte END

PATT_LEAD4_SHORT_VAR     byte ___             | 5
                         byte ((12    ) << 3) | 5 
                         byte ((12 - 5) << 3) | 3
                         byte ((12 - 2) << 3) | 3
                         byte ((12 + 9) << 3) | 5  
                         byte ((12 + 3) << 3) | 3  
                         byte ((12 - 3) << 3) | 1
                         byte END

PATT_LEAD5               byte ((12    ) << 3) | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ((12 + 3) << 3) | 5
                         byte ((12 + 2) << 3) | 5
                         byte ((12 - 2) << 3) | 3  
                         byte ((12 - 1) << 3) | 5  
                         byte ((12 - 2) << 3) | 5
                         byte ((12 - 2) << 3) | 3 
                         byte ((12 + 2) << 3) | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ((12 - 4) << 3) | 0
                         byte ((12 - 1) << 3) | 1  
                         byte ((12 - 2) << 3) | 0  
                         byte ((12 - 2) << 3) | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ___             | 3 
                         byte END

PATT_LEAD6               byte ((12    ) << 3) | 5
                         byte ((12    ) << 3) | 5
                         byte ((12    ) << 3) | 3
                         byte ((12 - 2) << 3) | 5 
                         byte ((12 - 2) << 3) | 5
                         byte ((12 - 1) << 3) | 3
                         byte ((12 + 1) << 3) | 5  
                         byte ((12    ) << 3) | 5
                         byte ((12    ) << 3) | 3
                         byte ((12 - 1) << 3) | 5 
                         byte ((12 - 4) << 3) | 5
                         byte ((12 - 3) << 3) | 3
                         byte END

PATT_LEAD7               byte ((12    ) << 3) | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ___             | 7 
                         byte ___             | 7
                         byte ___             | 7
                         byte ((12    ) << 3) | 7
                         byte ___             | 7 
                         byte END

PATT_LEAD8               byte ((12    ) << 3) | 5
                         byte ((12    ) << 3) | 5
                         byte ((12    ) << 3) | 3
                         byte ((12 - 2) << 3) | 5 
                         byte ((12 - 2) << 3) | 5
                         byte ((12 - 1) << 3) | 3
                         byte ((12 + 1) << 3) | 5  
                         byte ((12 + 2) << 3) | 5
                         byte ((12 + 2) << 3) | 3
                         byte ((12 + 3) << 3) | 5 
                         byte ((12 + 5) << 3) | 5
                         byte ((12 - 5) << 3) | 3
                         byte END

PATT_LEAD9               byte ((12    ) << 3) | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ((12 + 2) << 3) | 5
                         byte ((12 - 3) << 3) | 5 
                         byte ((12 - 4) << 3) | 3
                         byte ((12 - 2) << 3) | 5
                         byte ((12 + 2) << 3) | 5  
                         byte ((12 + 4) << 3) | 3
                         byte ((12 + 1) << 3) | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte ((12 - 3) << 3) | 7 
                         byte ___             | 7
                         byte ___             | 7
                         byte ___             | 7
                         byte END
   
PATT_COORD_TAPS_DOUBLE2  byte ((12 + 0 ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 0 ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 10) << 3) | 1
                         byte ((12 + 2 ) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 0 ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 10) << 3) | 1
                         byte ((12 + 2 ) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 0 ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 10) << 3) | 1
                         byte ((12 + 2 ) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 0 ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 10) << 3) | 1
                         byte ((12 + 2 ) << 3) | 1
                         byte END
  
PATT_COORD_TAPS_DOUBLE   byte ((12 + 0 ) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 5
                         byte ((12 + 0 ) << 3) | 3
                         byte ((12 + 0 ) << 3) | 3
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 3
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 0 ) << 3) | 1 
                         byte ((12 + 12) << 3) | 3

PATT_COORD_TAPS          byte ((12 + 0 ) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 5
                         byte ((12 + 0 ) << 3) | 3
                         byte ((12 + 0 ) << 3) | 3
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 3
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 0 ) << 3) | 1 
                         byte ((12 + 12) << 3) | 3
                         byte END
                         
PATT_COORD_TAPS_HALF     byte ((12 + 0 ) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 5
                         byte ((12 + 0 ) << 3) | 3
                         byte ((12 + 0 ) << 3) | 1
                         byte END

PATT_BASS_LONG           byte ((12     ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1 
                         byte ((12 - 12) << 3) | 1
                         byte ((12     ) << 3) | 3 
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12     ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         
PATT_BASS                byte ((12     ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1 
                         byte ((12 - 12) << 3) | 1
                         byte ((12     ) << 3) | 3 
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12     ) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 + 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1
                         byte ((12 - 12) << 3) | 1                           
                         byte END
                         
PATT_PERCUSSION1_NOISE   byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 0
                         byte TRG_0 | 0
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 0
                         byte TRG_0 | 0
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 1
                         byte TRG_0 | 0
                         byte TRG_0 | 0
                         byte TRG_0 | 0
                         byte TRG_0 | 0
                         byte END

PATT_PERCUSSION2_TRIANGLE
                         byte TRG_0 | 7
                         byte TRG_1 | 5
                         byte TRG_0 | 1
                         byte TRG_0 | 3
                         byte TRG_0 | 3
                         byte TRG_1 | 3
                         byte TRG_0 | 3
                         byte TRG_0 | 7
                         byte TRG_1 | 5
                         byte TRG_0 | 1
                         byte TRG_0 | 3
                         byte TRG_0 | 3
                         byte TRG_1 | 3
                         byte TRG_0 | 3

                         byte TRG_0 | 7
                         byte TRG_1 | 5
                         byte TRG_0 | 1
                         byte TRG_0 | 3
                         byte TRG_0 | 3
                         byte TRG_1 | 3
                         byte TRG_0 | 3
                         byte TRG_0 | 7
                         byte TRG_1 | 5
                         byte TRG_0 | 1
                         byte TRG_0 | 3
                         byte TRG_0 | 3
                         byte TRG_1 | 3
                         byte TRG_0 | 3
                         
                         byte TRG_0 | 7
                         byte TRG_1 | 5
                         byte TRG_0 | 1
                         byte TRG_0 | 3
                         byte TRG_0 | 3
                         byte TRG_1 | 3
                         byte TRG_0 | 3
                         byte TRG_0 | 7
                         byte TRG_1 | 5
                         byte TRG_0 | 1
                         byte TRG_0 | 3
                         byte TRG_0 | 3
                         byte TRG_1 | 3
                         byte TRG_0 | 3

                         byte TRG_0 | 7
                         byte TRG_1 | 5
                         byte TRG_0 | 1
                         byte TRG_0 | 3
                         byte TRG_0 | 3
                         byte TRG_1 | 3
                         byte TRG_0 | 3
                         byte TRG_0 | 7
                         byte TRG_1 | 5
                         byte TRG_0 | 1
                         byte TRG_0 | 3
                         byte TRG_0 | 3
                         byte TRG_1 | 3
                         byte TRG_1 | 1
                         byte TRG_0 | 1
                         byte END

DAT
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────
'                                    Instrument data
'──────────────────────────────────────────────────────────────────────────────────────────
'──────────────────────────────────────────────────────────────────────────────────────────          
         long 1
NOISE_PERC
         long  SET|MODULATION                   , 2_895_977_631          
         long  SET|VOLUME                       , 165 *vol 
         long  SET|FREQUENCY                    , 49000 *Hz              
         long  SET|ENVELOPE| (5 *wait_ms)       ,( (ms/5) &ILSB9) | 7
         long  SET|ENVELOPE| (20 *wait_ms)      ,(-(ms/56) &ILSB9)| 1
         long  SET|MODULATION                   , 2_895_977_632 
         long  SET|ENVELOPE| (20 *wait_ms)      ,(-(ms/220) &ILSB9)| 1
         long  JUMP                             , -1 *STEPS              
'────────────────────────────────────────────────────────────────────────────────────────── 
TRI_PERC long  JUMP                             , 1 *STEPS
         long  JUMP                             , 11 *STEPS                                  '
         long  SET|MODULATION                   , 27 << 27          
         long  SET|VOLUME                       , 255 *vol 
         long  SET|FREQUENCY                    , 700 *Hz              
         long  SET|ENVELOPE                     ,( (ms/5) &ILSB9) | 7
         long  MODIFY|FREQUENCY | (repeats *6)  , -57_60_00
         long  SET|MODULATION                   , -1
         long  MODIFY|FREQUENCY | (repeats *30) , -2_00_00 
         long  MODIFY|FREQUENCY | (repeats *100), -0_40_00 
         long  SET|ENVELOPE                     ,(-(ms/38) &ILSB9) | 1
         long  JUMP                             , -1 *STEPS
                       
TRI_SNR  long  SET|MODULATION                   , -1          
         long  SET|VOLUME                       , 255 *vol 
         long  SET|FREQUENCY                    , 700 *Hz              
         long  SET|ENVELOPE                     ,( (ms/5) &ILSB9) | 7
         long  MODIFY|FREQUENCY | (repeats *4)  , -1_295_020_000
         
         long  SET|FREQUENCY                    , 1000 *Hz 
         long  MODIFY|FREQUENCY | (repeats *2)  , -1_363_000
         long  MODIFY|FREQUENCY | (repeats *9)  , -168_000 
         long  MODIFY|FREQUENCY | (repeats *10) , -7_000
         long  SET|VOLUME                       , 140 *vol 
         long  SET|ENVELOPE                     ,(-(ms/308) &ILSB9) | 1  
         long  MODIFY|FREQUENCY | (repeats *250), -1_295_020_000  
         long  JUMP                             , -1 *STEPS           
'────────────────────────────────────────────────────────────────────────────────────────── 
SQR_BEEP long  SET|MODULATION                   , 220                    
         long  SET|MODULATION                   , 45 << 9
         long  SET|VOLUME                       , 100 *vol                
         long  SET|ENVELOPE|(20 *wait_ms)       ,( (ms/20 )&ILSB9) | 7    
         long  SET|ENVELOPE|(8  *wait_ms)       ,(-(ms/190)&ILSB9) | 1  
         long  SET|ENVELOPE                     ,(-(ms/420)&ILSB9) | 1  
         long  JUMP                             , -1 *STEPS              
'────────────────────────────────────────────────────────────────────────────────────────── 
SAW_LEAD long  SET|VOLUME                       , 36 *vol                
         long  SET|ENVELOPE                     ,( (ms/6000) &ILSB9)| 7    
         long  SET|MODULATION                   , 58                    
         long  SET|MODULATION|(200 *wait_ms)    , 15  << 9
         long  SET|MODULATION|(300 *wait_ms)    , 205 << 9 
         long  SET|ENVELOPE  |(120 *wait_ms)    ,(-(ms/4900)&ILSB9)| 1  
         long  SET|ENVELOPE                     ,(-(ms/29000)&ILSB9)| 1
         long  MODIFY|FREQUENCY|(repeats *80)   , -1500
         long  MODIFY|FREQUENCY|(repeats *160)  ,  1500
         long  MODIFY|FREQUENCY|(repeats *160)  , -1500
         long  JUMP                             , -3 *STEPS              
'────────────────────────────────────────────────────────────────────────────────────────── 
SAW_PIANO
         long  SET|MODULATION                   , 218                    
         long  SET|MODULATION                   , 36 << 9
         long  SET|VOLUME                       , 77 *vol                
         long  SET|ENVELOPE|(20 *wait_ms)       ,( (ms/20 )&ILSB9) | 7    
         long  SET|ENVELOPE|(15 *wait_ms)       ,(-(ms/600)&ILSB9) | 1  
         long  SET|MODULATION                   , 100 << 9 
         long  SET|ENVELOPE|(100 *wait_ms)      ,(-(ms/700)&ILSB9) | 1  
         long  SET|MODULATION                   , 220 << 9 
         long  SET|ENVELOPE                     , 0 | 1 
         long  JUMP                             , -1 *STEPS

SQR_STACC
         long  SET|MODULATION                   , 133                    
         long  SET|MODULATION|(0 *wait_ms)      , 60 << 9 
         long  SET|VOLUME                       , 50 *vol                
         long  SET|ENVELOPE|(20 *wait_ms)       ,( (ms/5 )&ILSB9) | 7    
         long  SET|ENVELOPE|(8  *wait_ms)       ,(-(ms/150)&ILSB9) | 1
         long  SET|ENVELOPE|(20 *wait_ms)       ,(-(ms/2320)&ILSB9)| 1  
         long  SET|MODULATION|(0 *wait_ms)      , 305 << 9 
         long  JUMP                             , -1 *STEPS
                   
CON

  END         = $0
  OCTAVE      = $1000_0000
  NOTE        = $100_0000
  INSTR       = $1_0000
  RESTART     = -1
  SET_TEMPO   = 32768
  ___         = 25 << 3      ' Void note
  TRG_0       = 12 << 3
  TRG_1       = 26 << 3
  TRG_2       = 27 << 3  
  TRG_3       = 28 << 3  
  TRG_4       = 29 << 3  
  TRG_5       = 30 << 3  
  TRG_6       = 31 << 3  
  ms          = 55_063_683 
  Hz          = ms / 1000
  seconds     = Hz   
  repeats     = $10
  delta       = 12
  wait_ms     = repeats
  STEPS       = 8
  VOL         = 1 << 24
  BPM         = 9_691_208    ' (2^32 / 78_000 / 11) * 16 
  
  ' Instructions
  JUMP        = $0
  SET         = $4
  MODIFY      = $8
  WAIT        = $8
  
  ' Registers
  FREQUENCY   = $0
  ENVELOPE    = $1
  VOLUME      = $2
  MODULATION  = $3
  NONE        = $0

  ' Masks
  LSB9         = %00000000_00000000_00000001_11111111
  ILSB9        = %11111111_11111111_11111110_00000000
  