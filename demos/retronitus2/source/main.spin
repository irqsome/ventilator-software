CON
    _clkmode  = xtal1 | pll16x
    _xinfreq  = 5_000_000
    left_pin  = 11
    right_pin = 10

OBJ
    sound : "Retronitus"
    music : "NearlyThere"
    
PUB Main
    sound.start(left_pin, right_pin)
    sound.playMusic(music.melody)
        
    repeat
       waitcnt(cnt + 80_000_000)
 