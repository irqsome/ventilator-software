{{

Nyan Cat for Propeller  WIP Doug Dingus MIT License Nov, 27, 2011

Thanks to Ahle2 for Retronitus Sound.  

Works at 80 Mhz +

Default config = Demoboard @ 80

Edit Clock settings for your xtal below.  Edit start pin in PUB main.

This one fixed video sync issue, reduced cat speed some, and changed to black background.

Report display issues to doug@opengeek.org, or the forum!  Thanks.  Potatohead

}}
CON
  ' Set up the processor clock in the standard way for 80MHz
  _XINFREQ = 5_000_000 + 0000
  _CLKMODE = XTAL1 + PLL16X

CON

  LEFT_AUDIO  = 11
  RIGHT_AUDIO = 10
  SCREEN_WIDTH=160
  SCREEN_HEIGHT=96
  NUM_COLORS=256
  _STACK=1000

OBJ
   tv : "03_NTSC4x2"            'Eric ball 8bpp driver 
   gr : "09_8bppgraphics"       'Doug Dingus basic PASM graphics COG
   retronitus : "Retronitus"
   
PUB main | i, j, k, l, anim

  retronitus.start(LEFT_AUDIO, RIGHT_AUDIO)
  retronitus.playMusic(@tune)

  tv.start( @i_ptr )
  gr.start( @cmd )

  i_width := 40
  i_height := 96
  i_lines := 411                ' 4:3 = 144/7 * i_width
  i_pin := 12                   ' Demoboard
  i_tint := 0 
  i_ptr := @colors
  i_tint := 20

  setscreen(@colors, 80*96)  'This sets screen buffer and size for graphics cog.

  repeat
     'Clear screen right at the start of blanking!
     'We are at bottom of frame, invisible screen blanking time

      'Clear graphics screen to black --Some color fringing may appear on high contrast
      'color borders.  Use grey or lower contrast changes to marginalize this.
      clear($00000000)

      'backdrop for Nyan Cat!
      fill(44, 20, $91, 73, 58)


      drawrainbow(anim)

      drawstars(anim)
      'fill(0,0,66,2, 95)
      drawtail(anim)
      'fill(2,0,76,2, 95)
      drawfeet(anim)
      'fill(4,0,86,2, 95)
      drawtart(anim)
      'fill(8,0,26,2, 95)
      drawcat(anim)

      'repeat 2000                   'padding to see screen engine speed
      'drawlines
      
      'fill(10,0,06,2, 95)            'A measuring stick, shorter = less time to draw remains

    repeat while i_blanking == 0


'----Anything that can be done while screen is visible (maybe sound)
    k++  'update the animation key variable
    if k == 5  'Adjust your Nyan Speed Here!!!
      k := 0
      anim ++
    
    'anim := k >> 2

PUB  drawfeet(sa) | anim

        anim := sa // 11

        bitplot(66+byte[@feetx][anim], 55-byte[@feety][anim], $ff, @rfeet1size + (byte[@rfeetseq][anim]*52)) 'Rear
        
        bitplot(82+byte[@feetx][anim], 56-byte[@feety][anim], $ff, @ffeet1size + (byte[@ffeetseq][anim]*38)) 'Front

        
PUB  drawcat(sa)

     bitplot(82+byte[@catseqx][sa // 11], 44+byte[@catseqy][sa // 11], $91, @catsize)


PUB  drawtart(sa)

     bitplot(69, 40-byte[@tartseq][sa // 11], $FF, @tsize)

PUB  drawtail(sa) | i

      i := sa // 6
      
      bitplot(62, 46-byte[@tartseq][sa // 11], $FF, @tail1size + (byte[@tailseq][i]*74))
      

PUB  drawlines | i

     repeat i from 0 to 20 step 6
        line(159, 95, 159-i, 83, 89)
        
PUB  drawrainbow(sa) | anim, j, yoff, xoff

     yoff := 42
     xoff := 44

     anim := (sa>>2) & 1

     if (anim == 0)
     
        repeat j from 0 to 6
            bitplot(xoff+(j << 2), yoff+byte[@rainy0][j], $00, @rainbowsize)

     else

        repeat j from 0 to 6
            bitplot(xoff+(j << 2), yoff+byte[@rainy1][j], $00, @rainbowsize)
        


PUB   drawstars(sa) | row, anim, starptr, starsptr, offx, offy, starxptr, starx, stary, starpix, i

      offx := 42
      offy := 17

      repeat row from 0 to 5
      
          stary := rowydata[row] + offy
          
          starxptr := @rowdat0 + (rowseqnum[row] * 12)
          starsptr := @seqdat0 + (rowseqnum[row] * 12)

          anim := (sa + byte[@rowseqoff][row]) // 12
          
          starx := byte[starxptr][anim] + offx
          starpix := @_0size + (51 * byte[starsptr][anim])

          bitplot(starx, stary, $91, starpix)

      
      'Mask off extra graphics
      fill(41, 18, $00, 86, 2)
      fill(41, 20, $00, 3, 58)
      fill(117, 19, $00, 10, 59) 

PUB   setscreen(A, B)
      'This sets a couple of global variables related to the graphics screen
      scraddr := A
      scrsize := B
      
                                            '
PUB  clear(color)                   'Quick interface to the PASM COG
      arg1 := scraddr
      arg2 := scrsize
      arg3 := color
      cmd := 01
      repeat until cmd == 0

PUB   plot(x, y, color)
      arg1 := scraddr
      arg2 := x + (y << 8)
      arg3 := color
      cmd := 03
      repeat until cmd == 0

PUB   bitplot(x, y, trans, imagemap)
      arg1 := scraddr
      arg2 := x + (y << 8)
      arg3 := trans
      arg4 := imagemap
      cmd := 02
      repeat until cmd == 0

PUB   fill(x, y, color, xsize, ysize)
      arg1 := scraddr
      arg2 := x + (y << 8)
      arg3 := color
      arg4 := xsize + (ysize << 8)
      cmd := 04
      repeat until cmd == 0


PUB   line(x1, y1, x2, y2, color) | x, y, xinc1, yinc1, xinc2, yinc2, deltax, deltay, den, num, numadd, numpixels, curpixel

  'Needs PASM / C port 
  'C + PASM WIP  This does work, but does not do screen buffer clipping.  Raw function.
  'Shifts and adds are all that are needed!

  deltax := ||(x2 - x1)          
  deltay := ||(y2 - y1)         
  x := x1                       
  y := y1

  if (x2 => x1)    'x increasing
    xinc1 := 1
    xinc2 := 1                     
  else                        'x decreasing
    xinc1 := -1
    xinc2 := -1

  if (y2 => y1)    'y increasing
    yinc1 := 1
    yinc2 := 1                     
  else                        'y decreasing
    yinc1 := -1
    yinc2 := -1

  if (deltax => deltay)       'More x values?
    xinc1 := 0
    yinc2 := 0
    den := deltax
    num := deltax / 2
    numadd := deltay
    numpixels := deltax
  else
    xinc2 := 0
    yinc1 := 0
    den := deltay
    num := deltay / 2
    numadd := deltax
    numpixels := deltay

  'General purpose "draw the line"

  repeat curpixel from 0 to numpixels
    plot(x,y, color)
    num += numadd
    if (num => den)
        num -= den
        x += xinc1
        y += yinc1
    x += xinc2
    y += yinc2    
    
  

DAT
colors      BYTE  $05[160*96]      'The bitmap buffer


catsize byte 16, 13
cat     byte $91, $91, $00, $00, $91, $91, $91, $91, $91, $91, $91, $91, $00, $00, $91, $91
        byte $91, $00, $cc, $cc, $00, $91, $91, $91, $91, $91, $91, $00, $cc, $cc, $00, $91
        byte $91, $00, $cc, $cc, $cc, $00, $91, $91, $91, $91, $00, $cc, $cc, $cc, $00, $91
        byte $91, $00, $cc, $cc, $cc, $cc, $00, $00, $00, $00, $cc, $cc, $cc, $cc, $00, $91
        byte $91, $00, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $00, $91
        byte $00, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $00
        byte $00, $cc, $cc, $cc, $aa, $00, $cc, $cc, $cc, $cc, $cc, $aa, $00, $cc, $cc, $00
        byte $00, $cc, $cc, $cc, $00, $00, $cc, $cc, $cc, $00, $cc, $00, $00, $cc, $cc, $00
        byte $00, $cc, $bd, $bd, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $bd, $bd, $00
        byte $00, $cc, $bd, $bd, $cc, $00, $cc, $cc, $00, $cc, $cc, $00, $cc, $bd, $bd, $00
        byte $91, $00, $cc, $cc, $cc, $00, $00, $00, $00, $00, $00, $00, $cc, $cc, $00, $91
        byte $91, $91, $00, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $cc, $00, $91, $91
        byte $91, $91, $91, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $91, $91, $91


_0size  byte  $07, $07
star0   byte  $91, $91, $91, $91, $91, $91, $91
        byte  $91, $91, $91, $91, $91, $91, $91
        byte  $91, $91, $91, $91, $91, $91, $91
        byte  $91, $91, $91, $FF, $91, $91, $91
        byte  $91, $91, $91, $91, $91, $91, $91
        byte  $91, $91, $91, $91, $91, $91, $91
        byte  $91, $91, $91, $91, $91, $91, $91

_1size  byte  $07, $07
star1   byte  $91, $91, $91, $91, $91, $91, $91
        byte  $91, $91, $91, $91, $91, $91, $91
        byte  $91, $91, $91, $FF, $91, $91, $91
        byte  $91, $91, $FF, $91, $FF, $91, $91
        byte  $91, $91, $91, $FF, $91, $91, $91
        byte  $91, $91, $91, $91, $91, $91, $91
        byte  $91, $91, $91, $91, $91, $91, $91

_2size  byte  $07, $07
star2   byte  $91, $91, $91, $91, $91, $91, $91
        byte  $91, $91, $91, $FF, $91, $91, $91
        byte  $91, $91, $91, $FF, $91, $91, $91
        byte  $91, $FF, $FF, $91, $FF, $FF, $91
        byte  $91, $91, $91, $FF, $91, $91, $91
        byte  $91, $91, $91, $FF, $91, $91, $91
        byte  $91, $91, $91, $91, $91, $91, $91

_3size  byte  $07, $07
star3   byte  $91, $91, $91, $FF, $91, $91, $91
        byte  $91, $91, $91, $FF, $91, $91, $91
        byte  $91, $91, $91, $91, $91, $91, $91
        byte  $FF, $FF, $91, $FF, $91, $FF, $FF
        byte  $91, $91, $91, $91, $91, $91, $91
        byte  $91, $91, $91, $FF, $91, $91, $91
        byte  $91, $91, $91, $FF, $91, $91, $91

_4size  byte  $07, $07
star4   byte  $91, $91, $91, $FF, $91, $91, $91
        byte  $91, $FF, $91, $91, $91, $FF, $91
        byte  $91, $91, $91, $91, $91, $91, $91
        byte  $FF, $91, $91, $91, $91, $91, $FF
        byte  $91, $91, $91, $91, $91, $91, $91
        byte  $91, $FF, $91, $91, $91, $FF, $91
        byte  $91, $91, $91, $FF, $91, $91, $91

_5size  byte  $07, $07
star5   byte  $91, $91, $91, $FF, $91, $91, $91
        byte  $91, $91, $91, $91, $91, $91, $91
        byte  $91, $91, $91, $91, $91, $91, $91
        byte  $FF, $91, $91, $91, $91, $91, $FF
        byte  $91, $91, $91, $91, $91, $91, $91
        byte  $91, $91, $91, $91, $91, $91, $91
        byte  $91, $91, $91, $FF, $91, $91, $91
                                                                     
tsize   byte  23, 18
tart    byte  $FF, $FF, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $FF, $FF
        byte  $FF, $00, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $00, $FF
        byte  $00, $7f, $7f, $7f, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $7f, $7f, $7f, $00
        byte  $00, $7f, $7f, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $AC, $bd, $bd, $AC, $bd, $bd, $bd, $bd, $bd, $7f, $7f, $00
        byte  $00, $7f, $7f, $bd, $bd, $AC, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $7f, $7f, $00
        byte  $00, $7f, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $7f, $00
        byte  $00, $7f, $bd, $bd, $bd, $bd, $bd, $bd, $AC, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $AC, $bd, $bd, $bd, $bd, $7f, $00
        byte  $00, $7f, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $7f, $00
        byte  $00, $7f, $bd, $bd, $bd, $AC, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $7f, $00
        byte  $00, $7f, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $AC, $bd, $bd, $bd, $bd, $bd, $7f, $00
        byte  $00, $7f, $bd, $AC, $bd, $bd, $bd, $bd, $bd, $AC, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $7f, $00
        byte  $00, $7f, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $AC, $bd, $7f, $00
        byte  $00, $7f, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $7f, $00
        byte  $00, $7f, $bd, $bd, $bd, $bd, $bd, $AC, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $AC, $bd, $bd, $7f, $00
        byte  $00, $7f, $7f, $bd, $AC, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $7f, $7f, $00
        byte  $00, $7f, $7f, $7f, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $bd, $7f, $7f, $7f, $00
        byte  $FF, $00, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $7f, $00, $FF
        byte  $FF, $FF, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $FF, $FF
        
tail1size     byte 08, 09
tail1   byte  $FF, $00, $00, $00, $00, $FF, $FF, $FF
        byte  $FF, $00, $cc, $cc, $00, $00, $FF, $FF
        byte  $FF, $00, $00, $cc, $cc, $00, $00, $FF
        byte  $FF, $FF, $00, $00, $cc, $cc, $00, $00
        byte  $FF, $FF, $FF, $00, $00, $cc, $cc, $00
        byte  $FF, $FF, $FF, $FF, $00, $00, $00, $00
        byte  $FF, $FF, $FF, $FF, $FF, $FF, $00, $00
        byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
        byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
        
tail2size     byte 08, 09
tail2   byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
        byte  $FF, $FF, $00, $00, $FF, $FF, $FF, $FF
        byte  $FF, $00, $cc, $cc, $00, $FF, $FF, $FF
        byte  $FF, $00, $cc, $cc, $00, $00, $00, $00
        byte  $FF, $FF, $00, $cc, $cc, $cc, $cc, $00
        byte  $FF, $FF, $FF, $00, $00, $cc, $cc, $00
        byte  $FF, $FF, $FF, $FF, $FF, $00, $00, $00
        byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
        byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF

tail3size     byte 08, 09
tail3   byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
        byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
        byte  $FF, $00, $00, $00, $00, $FF, $FF, $FF
        byte  $00, $cc, $cc, $cc, $00, $00, $00, $00
        byte  $00, $00, $cc, $cc, $cc, $cc, $00, $00
        byte  $FF, $FF, $00, $00, $00, $00, $cc, $00
        byte  $FF, $FF, $FF, $FF, $FF, $00, $00, $00
        byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
        byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF

tail4size     byte 08, 09
tail4   byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
        byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
        byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
        byte  $FF, $FF, $FF, $FF, $FF, $FF, $00, $00
        byte  $FF, $FF, $FF, $00, $00, $00, $00, $00
        byte  $FF, $00, $00, $cc, $cc, $cc, $cc, $00
        byte  $FF, $00, $cc, $cc, $cc, $00, $00, $00
        byte  $FF, $FF, $00, $00, $00, $00, $FF, $FF
        byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF

tail5size     byte 08, 09
tail5   byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
        byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
        byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
        byte  $FF, $FF, $FF, $FF, $FF, $00, $00, $00
        byte  $FF, $FF, $FF, $00, $00, $cc, $cc, $00
        byte  $FF, $FF, $00, $cc, $cc, $cc, $cc, $00
        byte  $FF, $00, $cc, $cc, $00, $00, $00, $00
        byte  $FF, $00, $cc, $cc, $00, $FF, $FF, $FF
        byte  $FF, $FF, $00, $00, $FF, $FF, $FF, $FF

ffeet1size     byte      09, 04
ffeet1  byte  $00, $00, $00, $00, $FF, $00, $00, $00, $00        
        byte  $00, $cc, $cc, $00, $FF, $00, $cc, $cc, $00
        byte  $00, $cc, $cc, $00, $FF, $00, $cc, $cc, $00
        byte  $FF, $00, $00, $00, $FF, $FF, $00, $00, $FF

ffeet2size     byte      09, 04
ffeet2  byte  $00, $00, $00, $00, $FF, $00, $00, $00, $00
        byte  $00, $cc, $cc, $00, $FF, $00, $00, $cc, $00
        byte  $00, $cc, $cc, $00, $FF, $00, $cc, $cc, $00
        byte  $00, $00, $00, $FF, $FF, $FF, $00, $00, $00

ffeet3size     byte      09, 04
ffeet3  byte  $00, $00, $00, $00, $FF, $00, $00, $00, $00
        byte  $00, $cc, $cc, $00, $FF, $00, $00, $cc, $00
        byte  $00, $cc, $cc, $00, $FF, $00, $cc, $cc, $00
        byte  $ff, $00, $00, $00, $FF, $FF, $00, $00, $00

ffeet4size     byte      09, 04
ffeet4  byte  $00, $00, $00, $00, $FF, $00, $00, $00, $00        
        byte  $00, $cc, $cc, $00, $FF, $00, $cc, $cc, $00
        byte  $00, $cc, $cc, $00, $FF, $00, $cc, $cc, $00
        byte  $00, $00, $00, $ff, $FF, $FF, $00, $00, $00

rfeet1size    byte      10, 05
rfeet1  byte  $ff, $ff, $ff, $00, $00, $00, $00, $00, $ff, $ff
        byte  $ff, $00, $00, $cc, $cc, $cc, $cc, $cc, $00, $ff
        byte  $00, $cc, $cc, $cc, $00, $00, $00, $cc, $00, $00
        byte  $00, $cc, $cc, $00, $00, $ff, $00, $cc, $cc, $00
        byte  $00, $00, $00, $00, $ff, $ff, $00, $00, $00, $ff

rfeet2size    byte      10, 05
rfeet2  byte  $ff, $ff, $00, $00, $00, $00, $00, $ff, $ff, $ff
        byte  $ff, $00, $cc, $cc, $cc, $cc, $cc, $00, $ff, $ff
        byte  $00, $cc, $cc, $cc, $00, $00, $cc, $cc, $00, $ff
        byte  $00, $cc, $cc, $00, $ff, $00, $cc, $cc, $00, $ff
        byte  $00, $00, $00, $ff, $ff, $00, $00, $00, $ff, $ff

rfeet3size    byte      10, 05
rfeet3  byte  $ff, $ff, $00, $00, $00, $00, $00, $ff, $ff, $ff
        byte  $ff, $00, $00, $00, $00, $00, $00, $00, $ff, $ff
        byte  $00, $cc, $cc, $cc, $00, $00, $cc, $cc, $00, $ff
        byte  $00, $cc, $cc, $00, $ff, $00, $cc, $cc, $00, $ff
        byte  $00, $00, $00, $ff, $ff, $ff, $00, $00, $00, $ff

rfeet4size    byte      10, 05
rfeet4  byte  $ff, $ff, $00, $00, $00, $00, $00, $ff, $ff, $ff
        byte  $ff, $00, $cc, $cc, $cc, $cc, $cc, $00, $ff, $ff
        byte  $00, $cc, $cc, $cc, $00, $00, $cc, $cc, $00, $ff
        byte  $00, $cc, $cc, $00, $ff, $00, $cc, $cc, $00, $ff
        byte  $00, $00, $00, $ff, $ff, $00, $00, $00, $ff, $ff

rainbowsize   byte 4, 15
rbow0         byte $1c, $1c, $1c, $1c
              byte $1c, $1c, $1c, $1c
              byte $1c, $1c, $1c, $1c
              byte $2b, $2b, $2b, $2b
              byte $2b, $2b, $2b, $2b
              byte $2b, $2b, $2b, $2b
              byte $87, $87, $87, $87
              byte $87, $87, $87, $87
              byte $87, $87, $87, $87
              byte $f2, $f2, $f2, $f2
              byte $f2, $f2, $f2, $f2
              byte $f2, $f2, $f2, $f2
              byte $74, $74, $74, $74
              byte $74, $74, $74, $74
              byte $74, $74, $74, $74
              
'Starfield animation data, position 12 off screen

rowydata      byte      1, 10, 19, 37, 45, 54                     'Star Y offsets
rowseqnum     byte      0, 0, 0, 1, 2, 3                          'six rows four sequences
rowseqoff     byte      4, 0, 10, 7, 0, 4

rowdat0 byte  70, 61, 52, 46, 42, 36, 28, 19, 10, 4, 0, 78        'Star X offset  cyan
rowdat1 byte  71, 62, 53, 47, 43, 37, 29, 20, 11, 5, 1, 78        'orange
rowdat2 byte  72, 69, 62, 53, 48, 44, 36, 30, 26, 20, 11, 2, 78   'pink
rowdat3 byte  72, 63, 54, 48, 44, 38, 30, 21, 12, 6, 2, 78        'blue


seqdat0 byte  3, 4, 5, 0, 1, 2, 3, 4, 5, 0, 1, 2                  'Star image number
seqdat1 byte  3, 4, 5, 0, 1, 2, 3, 4, 5, 0, 1, 2                               
seqdat2 byte  1, 0, 5, 4, 3, 2, 1, 0, 5, 4, 3, 2                  'star chart.bmp
seqdat3 byte  3, 4, 5, 0, 1, 2, 3, 4, 5, 0, 1, 2  


'Rainbow data

rainy0        byte  0, 1, 1, 0, 0, 1, 1
rainy1        byte  1, 0, 0, 1, 1, 0, 0

'Tail

tailseq       byte 0, 1, 3, 4, 2, 1, 0

'Tart

tartseq       byte 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0 

'Cat

catseqx       byte 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0
catseqy       byte 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0

'Feet

feetx      byte  1, 2, 3, 2, 0, 0, 1, 2, 3, 2, 0, 0  'add

feety      byte  1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0  'subtract

rfeetseq   byte  0, 0, 1, 2, 2, 3, 0, 0, 1, 2, 2, 3

ffeetseq   byte  0, 2, 2, 2, 2, 3, 0, 2, 2, 2, 2, 3



                
DAT

cmd           long 0     'current command, 0 = done / ready
arg1          long 0     'mailbox parameters 
arg2          long 0
arg3          long 0
arg4          long 0

scraddr       long 0    'address of screen buffer target
scrsize       long 0    'size of screen buffer target

DAT

' NTSC4x input parameters - 6 contiguous longs, reloaded each frame
i_ptr          long 0   ' pointer to screen (bytes), 0 = inactive
i_width        long 0   ' number of longs (max 48)
i_height       long 0   ' number of rows (max i_lines, does not need to be integer multiple)
i_lines        long 0   ' number of active lines (max 481, 4:3 = 144/7 * i_width)
i_pin          long 0   ' i_pin = LSB, i_pin+2 = MSB, must be divisible by 4
i_tint         long 0   ' tint correction
i_blanking     long 0   ' Vertical blank flag for syncronized screen drawing  opengeek@gmail.com
                        ' Note: NTSC4x input parameters not validated, minimum clockspeed 35.8MHz -- a bit higher now with blank
                        ' flag addition.  40Mhz maybe.

dat
  tune file "NyanCat.rsf"
        