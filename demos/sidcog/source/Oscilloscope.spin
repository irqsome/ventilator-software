CON
  PROPELLER_FREQ   = 96_000_000.0

PUB start( screen, audio, s, ss )

  screenPtr   := screen 
  sound       := audio 
  vSync       := s
  scrollSpeed := ss
  
  cognew( @OSCILLOSCOPE, @parameters )

dat org 0
'
'                Assembly SID emulator                      
'
OSCILLOSCOPE  mov      tempValue, par                
              rdlong   screenBase, tempValue
              add      tempValue, #4 
              rdlong   audioIn, tempValue
              add      tempValue, #4
              rdlong   verticalSync, tempValue 
              add      tempValue, #4
              rdlong   sSpeed, tempValue
              
              mov      waitCounter, cnt                    'Setup loop counter
              add      waitCounter, sampleRate              

'----------------------------------------------------------- 
mainLoop      call     #renderOsc  
              cmp      oscPos, #255                     wz
  if_nz       waitcnt  waitCounter, sampleRate 
              cmp      oscPos, #255                     wz 
  if_z        call     #scroll
              jmp      #mainLoop
'-----------------------------------------------------------              
              
dat
'
'  
'
renderOsc     add      oscPos, #1
              and      oscPos, #255

              mov      arg1, oscPos
              mov      arg2, #0
              mov      arg3, #2
              mov      arg4, #104
              call     #clsVerticalLine 
 
              rdlong   newPos, audioIn
              shr      newPos, #25
              sub      newPos, #15                     wc
  if_c        mov      newPos, #0                      
              max      newPos, #100
 
              mov      arg4, oldPos 
              sub      arg4, newPos
              abs      arg4, arg4
              min      arg4, #1
              max      arg4, #30  
              add      arg4, #3
 
              mov      arg2, oldPos 
              max      arg2, newPos
  
              mov      arg3, #2
              mov      arg1, oscPos

              call     #verticalLine
              mov      oldPos, newPos 
 
renderOsc_ret ret 

dat
'
'  
'
scroll        rdlong   tempValue, verticalSync
              cmp      tempValue, #2                     wz
  if_z        jmp      #scroll

              mov      arg3, #17
              mov      arg1, screenBase
              'mov      arg1, #384/2


              rdlong   tempValue, sSpeed             
              cmp      tempValue, #0                     wz
  if_z        ret                        

              
scrollLoop1   add      arg1, #384
              mov      arg2, #96
scrollLoop2   add      arg1, V768    
              rdlong   tempValue2, arg1
              shl      tempValue2,#28
              sub      arg1, V768                 
              rdlong   tempValue, arg1
              shr      tempValue, #4
              or       tempValue, tempValue2                  
              wrlong   tempValue, arg1
              add      arg1, #4
              djnz     arg2, #scrollLoop2
              djnz     arg3, #scrollLoop1 

sync2         rdlong   tempValue, verticalSync
              cmp      tempValue, #1                     wz
  if_z        jmp      #sync2
              mov       waitCounter, cnt                
              add       waitCounter, sampleRate    
 
scroll_ret    ret 

dat
'
' verticalLine     x=arg1    y=arg2    C=arg3  height=arg4
'
verticalLine  shl       arg1, #1
              shl       arg3, arg1
              shr       arg1, #1
'-----------------------------------------------------------  
              mov       tempValue, screenBase
              shr       arg1, #4
              shl       arg1, #9
              add       tempValue, arg1
              shr       arg1, #1
              add       tempValue, arg1
              shl       arg2, #2
              add       tempValue, arg2
'-----------------------------------------------------------  
lineLoop      rdlong    tempValue2, tempValue 
              xor       tempValue2, arg3
              wrlong    tempValue2, tempValue
              add       tempValue, #4
              djnz      arg4, #lineLoop
verticalLine_ret ret 

dat
'
' verticalLine     x=arg1    y=arg2    C=arg3  height=arg4
'
clsVerticalLine
              shl       arg1, #1
              shl       arg3, arg1
              shr       arg1, #1
'-----------------------------------------------------------  
              mov       tempValue, screenBase
              shr       arg1, #4
              shl       arg1, #9
              add       tempValue, arg1
              shr       arg1, #1
              add       tempValue, arg1
              shl       arg2, #2
              add       tempValue, arg2
'-----------------------------------------------------------  
clearLoop     rdlong    tempValue2, tempValue 
              andn      tempValue2, arg3
              wrlong    tempValue2, tempValue
              add       tempValue, #4
              djnz      arg4, #clearLoop
clsVerticalLine_ret ret 
 

dat
' 
'        Variables, masks and reference values
'
          
V768                long 768

screenBase          long 0
audioIn             long 0
verticalSync        long 0
sSpeed              long 0

oldPos              long 100
newPos              long 100

vbl                 long trunc( PROPELLER_FREQ / 50.0 )
sampleRate          long 2560*2
'sampleRate          long 2320*2 
'sampleRate          long trunc( PROPELLER_FREQ / (C64_CLOCK_FREQ/96.0) )   'clocks between samples ( ~31.250 khz ) 
'sampleRate          long trunc( PROPELLER_FREQ / (C64_CLOCK_FREQ/32.0) )   'clocks between samples ( ~31.250 khz )   

'Setup parameters  
arg1                long 0
arg2                long 0
arg3                long 0
arg4                long 0 
tempValue           long 0
tempValue2          long 0
screenLongs         long $c00

'Working variables
oscPos              res  1
waitCounter         res  1
                    fit

dat parameters      
screenPtr           long   0
sound               long   0
vSync               long   0
scrollSpeed         long   0

       