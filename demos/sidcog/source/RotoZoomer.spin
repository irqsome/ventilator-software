PUB start( screen, s )

  screenPtr := screen 
  vSync     := s
  
  cognew( @ROTOZOOM, @parameters )

dat org 0
'
'                Assembly SID emulator                      
'
ROTOZOOM      mov      tempValue, par                
              rdlong   screenBase, tempValue
              add      tempValue, #4
              rdlong   verticalSync, tempValue 

'----------------------------------------------------------- 
mainLoop      call     #rotoZoomer  
              call     #sync
              jmp      #mainLoop
'-----------------------------------------------------------              
              
dat
'
'   Updates all 256x192 pixels 60 times per second
'
rotoZoomer    and       frameCounter, #255               wz 
 if_z         neg       zoomSpeed, zoomSpeed
'-----------------------------------------------------------
              add       xZoom, xPos                         '
              mov       xOffset, xZoom                      '
              shl       xOffset, #7                         '
              mov       startBit, xZoom                     '  Calculate bitmap x start position
              sub       startBit, #1                        '
              shr       startBit, #25                       '
              add       startBit, bitOffset                 '
              sub       xZoom, xPos                         '
'----------------------------------------------------------- 
              add       yZoom, yPos                         '
              mov       tempValue, yZoom                    '
              shl       tempValue, #5                       '
              mov       yOffset, tempValue                  '  Calculate bitmap y start position  
              shl       tempValue, #1                       '
              add       yOffset, tempValue                  '
              sub       yZoom, yPos                         '
'----------------------------------------------------------- 
              neg       bitmapPosYF, yOffset                '  Init render loop
              mov       screenPointer, screenBase           '
              mov       lineCounter, #192                   '
'-----------------------------------------------------------              
:frameLoop    mov       longCounter, #16                    '  line = 16 Longs 
              mov       bitPointer, #1                      '
              rol       bitPointer, startBit
              neg       bitmapPosXF, xOffset         
'-----------------------------------------------------------                
:lineLoop     mov       pixelCounter, #16                   '  long = 16 pixel
'-----------------------------------------------------------
:pixelLoop    add       bitmapPosXF, lineZoomX           wc '|
 if_c         ror       bitPointer, #1                      '|
:rowSelect    test      bitmap, bitPointer               wc '| Inner pixel loop
              rcr       longShift, #2                       '| 
              djnz      pixelCounter, #:pixelLoop           '| 
'-----------------------------------------------------------
              and       longShift, color1
              rdlong    tempValue, screenPointer
              and       tempValue, color2
              add       longShift, tempValue
              wrlong    longShift, screenPointer            ' Write long to video memory
              add       screenPointer, V768                 ' and continue to next long
              djnz      longCounter, #:lineLoop
'-----------------------------------------------------------
              sub       screenPointer, V12284
              call      #waveX
 
              add       bitmapPosYF, lineZoomY              '
              mov       tempValue2, bitmapPosYF             '
          
              shr       tempValue2, #27                     '
              mov       tempValue, #bitmap                  '
              add       tempValue, tempValue2               '
              movd      :rowSelect, tempValue               '
              
              djnz      lineCounter, #:frameLoop


              
'----------------------------------------------------------- 
              add       waveOffset1, #36  
              add       waveOffset2, #357
              'add       waveOffset3, #311
'----------------------------------------------------------- 
              add       yZoom, zoomSpeed                    '
              mov       tempValue, zoomSpeed                '  Zoom x and y
              shl       tempValue, #5                       '
              add       xZoom, tempValue                    '
'-----------------------------------------------------------  
              add       xPos, xSpeed                        '  Move x and y 
              add       yPos, ySpeed                        '  
'-----------------------------------------------------------


               
'-----------------------------------------------------------
rotoZoomer_ret ret

dat
waveX
              mov       sin, lineCounter
              shl       sin, #5
              add       sin, waveOffset1 
              call      #getSin
              mov       wave1, sin
              shr       wave1, #10

              mov       sin, lineCounter
              shl       sin, #8 
              add       sin, waveOffset2
              shr       sin, #3
              call      #getSin
              mov       wave2, sin
              shr       wave2, #8

              'mov       sin, lineCounter
              'shl       sin, #6 
              'add       sin, waveOffset3
              'shr       sin, #5
              'call      #getSin
              'mov       wave3, sin
              'shr       wave3, #7
               
'-----------------------------------------------------------
              mov       tempValue, wave1
              shl       tempValue, #19
              mov       lineZoomY, yZoom 
              add       lineZoomY, tempValue                
'-----------------------------------------------------------
              mov       tempValue, wave2
              shl       tempValue, #21
              mov       lineZoomX, xZoom 
              add       lineZoomX, tempValue                
'-----------------------------------------------------------
'              mov       tempValue, wave3
'              shl       tempValue, #24            
'              add       xOffset, tempValue               wc    ' xOffset
'  if_c        add       startBit, #1                           '
'-----------------------------------------------------------  
waveX_ret     ret



dat
'
'  
'
sync          rdlong   tempValue, verticalSync
              cmp      tempValue, #2                     wz
  if_z        jmp      #sync
              add      frameCounter, #1
sync_ret      ret 

dat
'
'  
'
getSin
              test     sin, sin_90                       wc
              test     sin, sin_180                      wz
              negc     sin, sin
              or       sin, sin_table
              shl      sin, #1
              rdword   sin, sin
              negnz    sin, sin

getSin_ret    ret 
 
 
dat
' 
'        Variables, masks and reference values
'

sin_90              long $0800
sin_180             long $1000
sin_table           long $E000 >> 1
sin                 long 0

rowCounter          long 0

V8191               long 0
         
V768                long 768
V12284              long 12284 
zero                long 0
color1              long $55555555
color2              long $AAAAAAAA
color3              long $ffffffff

tempOffset          long $0 
aValue              long $80000000   

wave1               long $0
wave2               long $0
wave3               long $0
waveOffset1         long $0
waveOffset2         long $0
waveOffset3         long $0

screenBase          long 0
verticalSync        long 0

startBit            long 1
bitPointer          long 1
bitOffset           long 0
  
xOffset             long 0
yOffset             long 0

xPos                long 0
yPos                long 0
xSpeed              long -$1300000
ySpeed              long $40000

'zoomSpeed           long $60000
zoomSpeed           long $30000  
xZoom               long $80000000
yZoom               long $4000000
lineZoomX           long 0
lineZoomY           long 0

zoomDiffX           long 0
zoomDiffY           long 0

bitmapPosXF         long 0 
bitmapPosYF         long 0 

screenPointer       long 0
longShift           long 0
lineCounter         long 0
longCounter         long 0
pixelCounter        long 0

frameCounter        long 1

tempValue           long 0
tempValue2          long 0
tempValue3          long 0
screenLongs         long $c00

 
bitmap              long %11111111_11111111_11111111_11111111
                    long %11111111_11111111_11111111_11111111
                    long %11111111_11111111_11111111_11111111
                    long %10000001_10000001_10000001_10000001
                    long %11111111_11111111_11111111_11111111
                    long %11111111_11111111_11111111_11111111
                    long %11111111_11111111_11111111_11111111
                    long %11111111_11111111_11111111_11111111
                    
                    long %11000011_11000011_10000111_11111111   
                    long %10011001_11100111_10010011_11111111  
                    long %10011111_11100111_10011001_11111111   
                    long %11000011_11100111_10011001_11111111  
                    long %11111001_11100111_10011001_11111111   
                    long %10011001_11100111_10010011_11111111  
                    long %11000011_11000011_10000111_11111111   
                    long %11111111_11111111_11111111_11111111

                    long %11111111_11111111_11111111_11111111
                    long %11111111_11111111_11111111_11111111
                    long %11000011_11000011_11000001_11111111
                    long %10011111_10011001_10011001_11111111                                      
                    long %10011111_10011001_10011001_11111111
                    long %10011111_10011001_11000001_11111111
                    long %11000011_11000011_11111001_11111111
                    long %11111111_11111111_10000011_11111111

                    long %11111111_11111111_11111111_11111111
                    long %11111111_11111111_11111111_11111111
                    long %11111111_11111111_11111111_11111111
                    long %10000001_10000001_10000001_10000001
                    long %11111111_11111111_11111111_11111111
                    long %11111111_11111111_11111111_11111111
                    long %11111111_11111111_11111111_11111111
                    long %11111111_11111111_11111111_11111111

                    fit

dat parameters      
screenPtr           long   0
vSync               long   0
       