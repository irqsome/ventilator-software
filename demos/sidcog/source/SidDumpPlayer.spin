CON _clkmode = xtal1 + pll16x
    _xinfreq = 5_000_000    

    x_tiles      = 16
    y_tiles      = 12
    paramcount   = 14   
    display_base = $4400
    'enablePAL    = false
    enablePAL    = true
    SDPins       = 0
    'SDPins       = 16
    NTSC_corr    = 1.0+(float(!enablePAL&1)*0.2)
                                                                       
VAR

  byte  buffer[25]
  byte  filename[18]
  byte  fileNumber
  long  waitTime

  long  scrollSpeed  
  long  frameCounter
  long  event
  
  long  tv_status     '0/1/2 = off/visible/invisible           read-only
  long  tv_enable     '0/? = off/on                            write-only
  long  tv_pins       '%ppmmm = pins                           write-only
  long  tv_mode       '%ccinp = chroma,interlace,ntsc/pal,swap write-only
  long  tv_screen     'pointer to screen (words)               write-only
  long  tv_colors     'pointer to colors (longs)               write-only               
  long  tv_hc         'horizontal cells                        write-only
  long  tv_vc         'vertical cells                          write-only
  long  tv_hx         'horizontal cell expansion               write-only
  long  tv_vx         'vertical cell expansion                 write-only
  long  tv_ho         'horizontal offset                       write-only
  long  tv_vo         'vertical offset                         write-only
  long  tv_broadcast  'broadcast frequency (Hz)                write-only
  long  tv_auralcog   'aural fm cog                            write-only

  word  screen[x_tiles * y_tiles]
  long  colors[64]
  long  scrollCounter
  long  textPointer
  
OBJ
  Roto  : "RotoZoomer"
  OSC   : "Oscilloscope"
  SID   : "SIDCog"
  SD    : "fsrw"
  tv    : "tv"


  'printString( 5, 32, string( "SIDCog" ), 3 )
 
PUB Main | pixelPointer, SIDRegisters, i, oldStatus, x, y, updateRate, wait, songNumber

  SIDRegisters := SID.start(10,11)       'Start the emulated SID in one cog

  sd.mount_explicit( 21,24,20,25)
  initTV
  OSC.start( display_base, SIDRegisters + 28, @tv_status, @scrollSpeed )
  Roto.start( display_base, @tv_status )
    
  fileNumber  := 0    
  oldStatus   := 123
  event       := 0
  updateRate  := 50
  scrollSpeed := 2
  frameCounter:= 0
  songNumber  := 0
  wait        := cnt

  if( sd.pread(@buffer, 25) ) < 0
    openNextFile

  'setFinalColors          
  'textPointer := (@message3 - @message)

  repeat
  
    if (cnt-wait)>0
      updateSID
      wait := cnt + (80000000/updateRate)

      if event == 10
        if byte[@buffer + 0] == "S" and byte[@buffer + 1] == "o" and byte[@buffer + 2] == "n" and byte[@buffer + 3] == "g"

          textPointer := (@message2 - @message)
          bytemove( @message2, @buffer, 24 )
          repeat i from 0 to 24
            if byte[@message2 + i] > 0
              byte[@message2 + i] := byte[@message2 + i]^182
 
          scrollSpeed := 2
          songNumber++

          if songNumber == 61
            textPointer := (@message3 - @message)
             
          ifnot byte[@buffer+24] == " "
            updateRate := byte[@buffer+25] 
     
    if oldStatus <> tv_status
      oldStatus := tv_status
              
      if tv_status == 2
        scroll

        if word[ @eventTimer + event ]-- == 0
          case event
            0:
              updateRate  := 150
              event += 2
            2:
              setFinalColors
              event += 2
            4:
              updateRate := 200
              event += 2
            6:
              updateRate := 50 
              event += 2             
            8:
              event += 2
                
PUB updateSID
    if( sd.pread(@buffer, 25) ) < 0
      'openNextFile
    SID.updateRegisters( @buffer )  
   
PUB openNextFile | i
    'sd.opendir
    'repeat i from 0 to fileNumber
    '  sd.nextfile( @filename )
    if sd.popen( string("ABSDF.NFS"), "r" ) == -1
      reboot
      
    'fileNumber++
    
    'if fileNumber == 61
    '  reboot
      
    waitTime := cnt + 80_000_000

PUB initTV | i, dx, dy
  longmove(@tv_status, @tvparams, paramcount)
  tv_screen := @screen
  tv_colors := @colors
  tv.start(@tv_status)
  repeat i from 0 to 18
    colors[i] := (2 | 0)     
    colors[i] |= (2 | 0)<<8 
    colors[i] |= (6 | 0)<<16
    colors[i] |= (6 | 0)<<24
  repeat dx from 0 to tv_hc - 1
    repeat dy from 0 to tv_vc - 1
      screen[dy * tv_hc + dx] := display_base >> 6 + dy + dx * tv_vc + ((dy & $3F) << 10)

PUB setFinalColors | i, color1, color2, color3, color4
  repeat i from 0 to 6
    colors[i] := ((4 | (0<<4) | 8)    ) 
    colors[i] |= ((3 | (1<<4) | 8)<<8 )
    colors[i] |= ((6 | (3<<4) | 8)<<16)
    colors[i] |= ((5 | (4<<4) | 8)<<24)
  repeat i from 7 to 10
    color1 := 15-((8+i)&15)
    color2 := 15-((10+i)&15)
    color3 := 14  
    color4 := 1 
    colors[i] := ((3 | (color1<<4) | 8)    ) 
    colors[i] |= ((2 | (color2<<4) | 8)<<8 )
    colors[i] |= ((6 | (color3<<4) | 8)<<16)
    colors[i] |= ((5 | (color4<<4) | 8)<<24)
  repeat i from 11 to 12
    colors[i] := ((4 | (0<<4) | 8)    ) 
    colors[i] |= ((3 | (1<<4) | 8)<<8 )
    colors[i] |= ((5 | (5<<4) | 8)<<16)
    colors[i] |= ((4 | (5<<4) | 8)<<24)
 
PUB scroll | char, x, y 
 
  char := 65
  x    := 16
  y    := 128 

  ifnot enablePAL or event == 10 
    frameCounter++
    frameCounter//=6
    if frameCounter==0
      scrollCounter-=scrollSpeed
      if scrollCounter&15 == 14
        textPointer--
          
  ifnot (scrollCounter+=scrollSpeed)&15 or scrollSpeed == 0
    printChar( x, y, byte[ @message + textPointer ]^182, 2 )
    textPointer++

    
    if byte[ @message + textPointer] == 0  
      scrollSpeed := 0
 
PUB printString( x, y, str, col ) | i
  i := 0
  repeat
    printChar( x+i, y, byte[ str + i ], col )
  while byte[ str + (++i) ] > 0
 
PUB printChar( x, y, char, col ) | i, adr
  x  *= 768
  y <<= 2
  longmove( display_base + y + x, 32768 + ((char<<6)&!127), 32 )
  adr := display_base + y + x  
  case char&1
      0:
        case col
          1:
            repeat i from 0 to 31
              adr += 4
              long[adr] := (long[adr]&$55555555)
          2:
            repeat i from 0 to 31
              adr += 4 
              long[adr] := (long[adr]&$55555555)<<1
          3:
            repeat i from 0 to 31
              adr += 4 
              long[adr] := (long[adr]&$55555555)
              long[adr] += long[adr]<<1
      1:
        case col
          1:
            repeat i from 0 to 31
              adr += 4 
              long[adr] := (long[adr]&$AAAAAAAA)>>1  
          2:
            repeat i from 0 to 31
              adr += 4 
              long[adr] := long[adr]&$AAAAAAAA
          3:
            repeat i from 0 to 31
              adr += 4 
              long[adr] := (long[adr]&$AAAAAAAA)
              long[adr] += long[adr]>>1  
               
DAT
tvparams                long    2               'status
                        long    1               'enable
                        long    %001_0101       'pins
                        long    enablePAL&1     'mode
                        long    0               'screen
                        long    0               'colors
                        long    x_tiles         'hc
                        long    y_tiles         'vc
                        long    9+(enablePAL&1)*2'hx
                        long    1               'vx
                        long    0               'ho
                        long    0               'vo
                        long    0               'broadcast
                        long    0               'auralcog
 
dat
eventTimer
        word  trunc(4235.0 * NTSC_corr)   ' Zelda theme  - 3X playback
        word  trunc(3175.0 * NTSC_corr)   ' Enable C64 mode
        word  trunc(995.0  * NTSC_corr)   ' Eskimonika   - 4X playback
        word  trunc(6035.0 * NTSC_corr)   ' Mr marvelous - 1X playback 
        word  trunc(10682.0 * NTSC_corr)  ' Tight NTSC letters.
                        
DAT
message file "scrollText" 
        byte  150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,0 

message2
        byte "                         "
        byte 0,"   "   

message3
        file "scrollText3"  
            