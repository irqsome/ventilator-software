CON
    _clkmode  = xtal1 | pll16x
    _xinfreq  = 5_000_000
    left_pin  = 11
    right_pin = 10

OBJ
    sound : "r1"
    music : "Alice"
    pst   : "parallax serial terminal"
    
PUB Main | b,p,p2,chans,count,i,w
    sound.start(right_pin,left_pin)
    sound.playMusic(music.melody)
    pst.start(115200)

    repeat
       pst.str(string("Step count:",13))
       p := music.melody + 2
       repeat while word[p]
         p+=2
       chans := p
       repeat i from 1 to 8
         count~                 
         p := word[chans][i] + music.melody + 2
         repeat until ((w:=word[p])&7)==%101
           p+=2
           ifnot (w&1)
             p2 := w>>1
             repeat while b:=byte[p2++]
               count += (b&7)+1
         pst.str(string("Channel "))
         pst.dec(i)
         pst.str(string(" has "))
         pst.dec(count)
         pst.str(string(" steps!",13))
       waitcnt(cnt + 80_000_000)
 