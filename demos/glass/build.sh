mkdir -p sdbins
cd source
rm ../sdbins/GLASDEMO1.BIN
rm ../sdbins/GLASDEMO2.BIN
bstc -Ocgru -e -o ../sdbins/GLASDEMO1 RGW_Glass1_.spin
mv ../sdbins/GLASDEMO1.eeprom ../sdbins/GLASDEMO1.BIN
bstc -Ocgru -e -o ../sdbins/GLASDEMO2 RGW_Glass2_.spin
mv ../sdbins/GLASDEMO2.eeprom ../sdbins/GLASDEMO2.BIN
cd ..
