SNES2COM
==========

## Info
- Type: Utility
- Author(s): Ada Gottensträter
- First release: 2019
- Video formats: None
- Inputs: Gamepad (duh)

## Description
Reads your SNES gamepads, converts their data into IBUS packets and sends them straight into your PC's COM port.

## Instructions
- Launch SNES2COM
- Set your COM port's buffer sizes to something low (like 64 bytes) (On Windows, this is somewhere in Device Manager, no clue on Linux)
- Download [VJoySerialFeeder](https://github.com/Cleric-K/vJoySerialFeeder) and set it up. (On Windows, this involves installing VJoy and creating a virtual joystick with 2 axes and 16 buttons. On Linux, you need to do some permission magic as explained on VJoySerialFeeder's page)
- Set protocol to IBUS
- Select your COM port
- open "Port Setup", select "Custom" and set the format to 230400 baud, 8 data bits, no parity, 1 stop bit
- press "Connect"
- Add one "Bit-mapped Button" and set it to Channel 1. Now map the bits to buttons. For me, it looks like this:
```
| 3| 4| 9|10|  |  |  |  | 2| 1| 5| 6|  |  |  |  |
```
- Add two axes. Set one to X and Channel 2, the other to Y and Channel 3. Then click "Setup" on each and go through the calibration process.
- Launch a game and map the controller as usual (some games will require you to set it as the primary controller in Windows' game controller settings)
- Profit?


## TODOs
- Figure out how to make this work with two pads (the data is sent, but routing it to two virtual devices is the isssue)
