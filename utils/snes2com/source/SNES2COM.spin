CON

_clkmode = xtal1 + pll16x
_xinfreq = 5_000_000      
_stack   = 128

'' gamepad pin configuration (adjust for your setup)
  JOY_CLK = 16
  JOY_LCH = 17
  JOY_DATAOUT0 = 19
  JOY_DATAOUT1 = 18

  NUM_PADS = 2
  IBUS_CHANNELS = NUM_PADS*3 'Three channels per pad - One for the buttons, two for directions

OBJ

  com           : "Parallax Serial Terminal"

VAR
  word checksum
  word pad[NUM_PADS]

  

PUB main | i,axis

  com.start(230_400)

  repeat
    read_pads
    ibus_start(IBUS_CHANNELS)
    repeat i from 0 to NUM_PADS-1
      ibus_channel(pad[i])
      ' X Axis
      axis := $8000
      if pad[i]&%0100_0000 'left?
        axis -= $7FFF
      if pad[i]&%1000_0000 'right?
        axis += $7FFF
      ibus_channel(axis)
      ' Y Axis
      axis := $8000
      if pad[i]&%0001_0000 'up?
        axis -= $7FFF
      if pad[i]&%0010_0000 'down?
        axis += $7FFF
      ibus_channel(axis)
    ibus_end
    
    
PRI ibus_start(numChannels) | length
  length := 4+numChannels*2
  checksum := $FFFF - length - $40
  com.char(length)
  com.char($40)

PRI ibus_channel(val) | b
  b := val & $FF
  com.char(b)
  checksum -= b
  b := (val>>8) & $FF
  com.char(b)
  checksum -= b

PRI ibus_end
  com.char(byte[@checksum+0])
  com.char(byte[@checksum+1])
  
PRI read_pads |i,p0,p1

DIRA [JOY_LCH] := 1 ' output
DIRA [JOY_CLK] := 1 ' output
DIRA [JOY_DATAOUT0] := 0 ' input
DIRA [JOY_DATAOUT1] := 0 ' input

OUTA [JOY_CLK] := 0
waitcnt(cnt+100)
OUTA [JOY_LCH] := 0
waitcnt(cnt+100)
OUTA [JOY_LCH] := 1
waitcnt(cnt+100)
OUTA [JOY_LCH] := 0
waitcnt(cnt+100)
p0 := INA[JOY_DATAOUT0]
p1 := INA[JOY_DATAOUT1]

repeat i from 0 to 14
  OUTA [JOY_CLK] := 1
  waitcnt(cnt+100)
  OUTA [JOY_CLK] := 0
  waitcnt(cnt+100)
  p0 <<= 1
  p0 |= INA[JOY_DATAOUT0]
  p1 <<= 1
  p1 |= INA[JOY_DATAOUT1]

pad[0] := !(p0><16)
pad[1] := !(p1><16)