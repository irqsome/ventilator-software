def main():
	from optparse import OptionParser
	parser = OptionParser("%prog inputfile outputfile")

	options, args = parser.parse_args()
	if len(args) != 2:
		parser.error("must specify input and output files");
	(infilen,outfilen) = args
	infile = open(infilen, "rb")
	outfile = open(outfilen, "wb")
	
	color = infile.read(1)
	while len(color):
		scr = infile.read(960)
		if len(scr) != 960:
			raise Exception('Screen too short')
		for i in range(0,960):
			outfile.write(scr[i:i+1])
			outfile.write(color)
		color = infile.read(1)
	outfile.close()
	infile.close()
	
main()