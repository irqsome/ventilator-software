'' **************************************
'' *  PhaseMod GeneralMIDI-Synthesizer  *  version 1.0
'' **************************************
'   HAXXORIZED FOR NCOWAV
'' (c)2009 Andy Schenk, www.insonix.ch/propeller ( see MIT license at end)
''
'' Multitimbral Synthesizer with GM1 soundset, and GM1 drum set on channel 10.
'' Generates 10 voices in 1 cog, or 20 voices in 2 cogs. 
''
'' Voice architecture:                                        Parameters: (10 bytes)
''                           ┌─────┐                          - volume              0..255
'' ┌──────┐  ┌───┐           │ Env │                          - panorama            0..127
'' │ Osc2 ├──┤├─┬────┐    └──┬──┘      pl                  - semitone osc1       0..255
'' └──────┘  └───┘ │        ┌──┴──┐    ┌── L DAC          - semitone osc2       0..255
''         pm┌──├─┘   (+)──┤ DCA ├─┬──┤                     - detune osc2         0..255
'' ┌──────┐    ┌───┐       └─────┘ │  └── R DAC          - PhaseMod intensity  7..0
'' │ Osc1 ├(+)─┤├───┴─┐pm      │    pr                  - Feedback intensity  7..0
'' └──────┘    └───┘               │                        - Envelope Attack     0..255
''           └─────├─────(+)──────┘                        - Envelope Decay&Release  0..255
''                 fb                                         - Envelope Sustain    0..255
''
''Legend:  = SawtoothTriangle Shaper, (+) = Mixer,  ├ = 2^n Attenuator (shifts)
'' Osc = Sawtooth Oscillator  (DDS)
'' Env = Envelope Generator (A D/R S)
'' DCA = Digital Attenuator (mult 8*32)
''

 #0, volp, pan, semi1, semi2, detune, pmi, fbi, eat, edy, esu   'synth parameter index enumeration

''Sound parameter tipps:
'' PM and FB are 2^n Attenuators, realized with a simple right shift (SAR instruction)
'' that is: 0 is no attenuation and 1 is 1/2 value 2=1/4 value and so on. values over 7
'' have no noticable effect.
'' if you set FB to 0 and PM to 0..2 you get noise sounds, because of the havy feedback.
''
'' One part of feedback modulation comes from the envelope modulated output, and the sound
'' is therefore not only volume modulated, but also the timbre changes with the envelope.
''
'' if you set semi1 or semi2 to 128..255 you get a low frequency from this oscillator. Combined
'' with feedback, noise sounds with no tonal parts are possible.
''
'' PM (phase modulation) synthesis works the same as FM for higher frequencies. On FM synthesizers, the
'' Oscillators produce sine waves, but this synth works with triangle waves. The sound generation
'' principals are the same: No PM or FB produces low harmonics, with PM or FM the harmonics
'' relate on the frequency ratio of the oscillators. With both osc at the same freq. the sound
'' is sawtooth like, with osc2=2*osc1 the sound is more square wave like. With odd ratios, you
'' get a lot of disharmonic sounds, like bells. 
'' You can set the frequency of the oscillators only in semitones, to get exact ratios this table can help:
'' 1*freq = 0, 2*freq=12, 4*freq=24 (3*freq≈19, 5*freq≈28)     

VAR
 long i, k
 long cog1, cog2, nVc, mVc
 
 long auxL, auxR, cmd2, cmnd                            'Assembly parameters
 long cmlp, cmrp, pinlr, rticks                         '(8 contiguous longs)
 
 long rte, inc1, inc2, env, mods                        'for voice parameter passing
 long rotvc                                             'for rotating voice allocation
 word vcstate[20]                                       'voice status
 byte vol[16], prog[16], panc[16]                       'channel values
 long pres_ptr,drset_ptr
 long asmptr
 long fqtab[128]                                        'Frequency values for notes


PUB start(Lptr,Rptr,Cogs,pptr,dptr,asm) : okay | v ''start Synth, starts 1..2 cogs
  asmptr := asm
  pres_ptr := pptr
  drset_ptr := dptr
'' Lpin, Rpin = Audio Out Pins (for mono: Rpin = -1)
  nVc := 20                                  '' Cogs = 2: 20 voices in 2 cogs
  if Cogs == 1
    nVc := 10                                '' Cogs = 1: 10 voices in 1 cog
  mVc := nVc-1
  
  repeat k from 9 to 0                       'generate Freq Table
    repeat  i from 0 to 11
      v := constant(1<<30 / 15974)  * word[i<<1+@octav]
      fqtab[k*12+i] := v >> (9-k)

  repeat i from 0 to 15                      'controller defaults
    vol[i] := 127
    prog[i] := 0
    panc[i] := 64

  rticks := {clkfreq/32000} 2504             'sample freq = 32kHz 'hardcoded because ncowav stuffs (0,16% out of tune...)
  cmlp := Lptr 
  cmrp := Rptr                                       
  cog1 := cognew(asmptr, @auxL)+1            'start 1.cog synth
  if cog1 and Cogs>1
    waitcnt(8192+cnt)
    byte[asmptr+4] := 8                      'modify assembly code for 2.cog
    cog2 := okay := cognew(asmptr, @auxL)+1  'start 2.cog synth
  allOff
  if okay
    okay := pres_ptr                         '' return pointer to sound parameters if OK


PUB stop                                     ''stop synth - frees up to 2 cogs
  if cog1
    byte[asmptr+4] := 12
    cogstop(cog1~ -1)
  if cog2
    cogstop(cog2~ -1)


PUB noteOn(key,chan,vel) | dy,pb1,v          ''start a note
  v := chan<<8 + key
  repeat i from 0 to mVc                     'search free voice
    rotvc := (rotvc + 1) // nVc
    if vcstate[rotvc]==0 or vcstate[rotvc]==v
      quit
  if chan == 9
    if key<35 or key>81
      return
    v := (key-35)*10 + drset_ptr             'Drum Set or..
  else
    v := prog[chan]*10 + pres_ptr            '..Instruments

  i := byte[v+semi1]                         'Pitch 1
  if i<128 
    inc1 := fqtab[i+key]                     'according note
  else
    inc1 := (i-128) << 14                    'low frequ if semi > 127

  i := byte[v+semi2]                         'Pitch 2
  if i<128 
    inc2 := byte[v+detune]<<14               'according note with detune
    inc2 += fqtab[i+key]
  else
    inc2 := (i-128) << 14                    'low frequ if semi > 127
    
  i := panc[chan]>>4<<1                      'Panorama 
  k := byte[@pantab+i]
  i := byte[@pantab+i+1]
  mods := i<<24 + k<<16 + byte[v+fbi]<<8 + byte[v+pmi]   'modulation values
  env := 0
  dy := byte[v+volp]                         'Volume
  if chan==9
    dy := (dy*15)>>3 ' Amplify percussion
  elseif key>70
    dy := (dy*(70+128-key)) >> 7              'Key scaling
  dy := (vol[chan]*vel*dy) / 69_666             'Velocity
  rte := byte[v+eat]<<24 + byte[v+edy]<<16 + byte[v+esu]<<8 + (dy<#255)   'Env rates
  k :=  @rte<<12 + 4<<9 + ((rotvc//10)*11)
  if rotvc < 10                                       
    repeat until cmnd==0                     'pass to assembly cog 1 or
    cmnd := k
  else
    repeat until cmd2==0                     'to assembly cog 2
    cmd2 := k

  vcstate[rotvc] := chan<<8+key              'set voice state


PUB noteOff(key,chan)  | st, v               ''release a note 
  st := chan<<8+key
  repeat i from 0 to mVc                     'search key and channel in voices
    if vcstate[i] == st
      v := prog[chan]*10 + pres_ptr          'set to release if found
      k := (i//10)*11 + 3
      if i < 10
        repeat until cmnd==0                 'in cog1
        cmnd := k
      else
        repeat until cmd2==0                 'in cog2
        cmd2 := k
      vcstate[i] := 0


PUB prgChange(num,chan)                      ''Program Change
  prog[chan] := num
  if chan==9
    panc[chan] := byte[num*10 + drset_ptr + pan]
  else
    panc[chan] := byte[num*10 + pres_ptr + pan]


PUB volContr(vo,chan)                        ''Volume Controller
  vol[chan] := vo

PUB panContr(pa,chan)                        ''Panorama Controller
  panc[chan] := pa


PUB allOff  | kn,v                          ''all Notes off
  repeat v from 0 to 9
    kn := v*11 + 3
    repeat until cmnd==0
    cmnd := kn
    cmd2 := kn
    vcstate[v] := 0
    vcstate[v+10] := 0


DAT
octav word 4186,4435,4699,4978,5274,5588,5920,6272,6645,7040,7459,7902



pantab
      byte 1,4, 1,3, 1,2, 1,1, 1,1, 2,1, 3,1, 4,1      '8 panorama steps (right-shifts)
'      byte 2,5, 2,4, 2,3, 2,2, 2,2, 3,2, 4,2, 5,2      '1/2 volume
'      byte 3,6, 3,5, 3,4, 3,3, 3,3, 4,3, 5,3, 6,3      '1/4 volume

''ASM code snipped

{{
 ───────────────────────────────────────────────────────────────────────────
                Terms of use: MIT License                                   
 ─────────────────────────────────────────────────────────────────────────── 
   Permission is hereby granted, free of charge, to any person obtaining a  
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation 
  the rights to use, copy, modify, merge, publish, distribute, sublicense,  
    and/or sell copies of the Software, and to permit persons to whom the   
    Software is furnished to do so, subject to the following conditions:    
                                                                            
   The above copyright notice and this permission notice shall be included  
           in all copies or substantial portions of the Software.           
                                                                            
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER   
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER     
                       DEALINGS IN THE SOFTWARE.                            
 ─────────────────────────────────────────────────────────────────────────── 
}}