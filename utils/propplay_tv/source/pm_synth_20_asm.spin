CON
#0, volp, pan, semi1, semi2, detune, pmi, fbi, eat, edy, esu   'synth parameter index enumeration
PUB hahano
DAT
'---- Assembly PhasModulation Synthesis ----
' the same code is used for both cogs, with 1 modification for the command register
' All voices are unrolled and registers are direct addressed to be as fast as possible

            org  0
entry       mov  cp,par
modify      add  cp,#12             'cog2: #8
            mov  pp,par
            mov  auxp1,pp
            mov  auxp2,pp
            add  auxp2,#4
            add  pp,#16
            
            test modify,#4   wz
     if_nz  rdlong leftptr,pp          'init Pins, counters and rate (cog1)
            add  pp,#4
     if_nz  rdlong rightptr,pp
            'add  pp,#4
     'if_nz  rdlong dira,pp
            add  pp,#{4}8
            rdlong rate,pp          'rate also for cog2
            sub  pp,#16
            mov  tm,cnt
            add  tm,rate

loop        rdlong cmd,cp     wz    'Audio Loop
     if_nz  call #copypar
     if_z   call #envsub
            mov  mixl,#0
            mov  mixr,#0
            call #voices            'calc 10 voices
            cmp  cp,pp        wz    'cog1 or 2 ?
     if_nz  jmp  #cogn2
            rdlong t1,auxp1         'cog1: add aux
            add  mixl,t1
            maxs mixl,maxout        'limiter
            rdlong t2,auxp2
            add  mixr,t2
            mins mixl,minout
            maxs mixr,maxout
            mins mixr,minout
            shl  mixl,#1
            shl  mixr,#1
            'add mixl,bit31         'to DAC
            'add mixr,bit31
            'mov  frqa,mixl
            'mov  frqb,mixr
loopend     waitcnt tm,rate         '32kHz periode
            wrlong mixl,leftptr
            wrlong mixr,rightptr
            jmp  #loop

cogn2       wrlong mixl,auxp1       'cog2: write output to aux
            wrlong mixr,auxp2
            jmp  #loopend

voices      add  phs02,inc02        'Osc 2   voice 0
            abs  t2,phs02
            add  phs01,inc01        'Osc 1
            sar  t2,pm0
            mov  t1,phs01
            add  t1,t2              'PM
            mov  t2,fbrg0
            sar  t2,fb0             'FB
            add  t1,t2
            add  t1,t2
            abs  t1,t1              'Mixer
            mov  fbrg0,t1
            sar  fbrg0,pm0
            sub  t1,middle
            addabs  t1,phs02
            mov  fakt,env0           'DCA
            call #mul8
            add  fbrg0,t1           'FB register
            mov  t2,t1
            sar  t1,pl0
            add  mixl,t1
            sar  t2,pr0
            add  mixr,t2            '/44*4 cycles 24 instr

            add  phs12,inc12        'voice 1
            abs  t2,phs12
            add  phs11,inc11
            sar  t2,pm1
            mov  t1,phs11
            add  t1,t2
            mov  t2,fbrg1
            sar  t2,fb1
            add  t1,t2
            add  t1,t2
            abs  t1,t1
            mov  fbrg1,t1
            sar  fbrg1,pm1
            sub  t1,middle
            addabs  t1,phs12
            mov  fakt,env1
            call #mul8
            add  fbrg1,t1
            mov  t2,t1
            sar  t1,pl1
            add  mixl,t1
            sar  t2,pr1
            add  mixr,t2

            add  phs22,inc22        'voice 2
            abs  t2,phs22
            add  phs21,inc21
            sar  t2,pm2
            mov  t1,phs21
            add  t1,t2
            mov  t2,fbrg2
            sar  t2,fb2
            add  t1,t2
            add  t1,t2
            abs  t1,t1
            mov  fbrg2,t1
            sar  fbrg2,pm2
            sub  t1,middle
            addabs  t1,phs22
            mov  fakt,env2
            call #mul8
            add  fbrg2,t1
            mov  t2,t1
            sar  t1,pl2
            add  mixl,t1
            sar  t2,pr2
            add  mixr,t2

            add  phs32,inc32        'voice 3
            abs  t2,phs32
            add  phs31,inc31
            sar  t2,pm3
            mov  t1,phs31
            add  t1,t2
            mov  t2,fbrg3
            sar  t2,fb3
            add  t1,t2
            add  t1,t2
            abs  t1,t1
            mov  fbrg3,t1
            sar  fbrg3,pm3
            sub  t1,middle
            addabs  t1,phs32
            mov  fakt,env3
            call #mul8
            add  fbrg3,t1
            mov  t2,t1
            sar  t1,pl3
            add  mixl,t1
            sar  t2,pr3
            add  mixr,t2

            add  phs42,inc42        'voice 4
            abs  t2,phs42
            add  phs41,inc41
            sar  t2,pm4
            mov  t1,phs41
            add  t1,t2
            mov  t2,fbrg4
            sar  t2,fb4
            add  t1,t2
            add  t1,t2
            abs  t1,t1
            mov  fbrg4,t1
            sar  fbrg4,pm4
            sub  t1,middle
            addabs  t1,phs42
            mov  fakt,env4
            call #mul8
            add  fbrg4,t1
            mov  t2,t1
            sar  t1,pl4
            add  mixl,t1
            sar  t2,pr4
            add  mixr,t2

            add  phs52,inc52        'voice 5
            abs  t2,phs52
            add  phs51,inc51
            sar  t2,pm5
            mov  t1,phs51
            add  t1,t2
            mov  t2,fbrg5
            sar  t2,fb5
            add  t1,t2
            add  t1,t2
            abs  t1,t1
            mov  fbrg5,t1
            sar  fbrg5,pm5
            sub  t1,middle
            addabs  t1,phs52
            mov  fakt,env5
            call #mul8
            add  fbrg5,t1
            mov  t2,t1
            sar  t1,pl5
            add  mixl,t1
            sar  t2,pr5
            add  mixr,t2

            add  phs62,inc62        'voice 6
            abs  t2,phs62
            add  phs61,inc61
            sar  t2,pm6
            mov  t1,phs61
            add  t1,t2
            mov  t2,fbrg6
            sar  t2,fb6
            add  t1,t2
            add  t1,t2
            abs  t1,t1
            mov  fbrg6,t1
            sar  fbrg6,pm6
            sub  t1,middle
            addabs  t1,phs62
            mov  fakt,env6
            call #mul8
            add  fbrg6,t1
            mov  t2,t1
            sar  t1,pl6
            add  mixl,t1
            sar  t2,pr6
            add  mixr,t2

            add  phs72,inc72        'voice 7
            abs  t2,phs72
            add  phs71,inc71
            sar  t2,pm7
            mov  t1,phs71
            add  t1,t2
            mov  t2,fbrg7
            sar  t2,fb7
            add  t1,t2
            add  t1,t2
            abs  t1,t1
            mov  fbrg7,t1
            sar  fbrg7,pm7
            sub  t1,middle
            addabs  t1,phs72
            mov  fakt,env7
            call #mul8
            add  fbrg7,t1
            mov  t2,t1
            sar  t1,pl7
            add  mixl,t1
            sar  t2,pr7
            add  mixr,t2

            add  phs82,inc82        'voice 8
            abs  t2,phs82
            add  phs81,inc81
            sar  t2,pm8
            mov  t1,phs81
            add  t1,t2
            mov  t2,fbrg8
            sar  t2,fb8
            add  t1,t2
            add  t1,t2
            abs  t1,t1
            mov  fbrg8,t1
            sar  fbrg8,pm8
            sub  t1,middle
            addabs  t1,phs82
            mov  fakt,env8
            call #mul8
            add  fbrg8,t1
            mov  t2,t1
            sar  t1,pl8
            add  mixl,t1
            sar  t2,pr8
            add  mixr,t2

            add  phs92,inc92        'voice 9
            abs  t2,phs92
            add  phs91,inc91
            sar  t2,pm9
            mov  t1,phs91
            add  t1,t2
            mov  t2,fbrg9
            sar  t2,fb9
            add  t1,t2
            add  t1,t2
            abs  t1,t1
            mov  fbrg9,t1
            sar  fbrg9,pm9
            sub  t1,middle
            addabs  t1,phs92
            mov  fakt,env9
            call #mul8
            add  fbrg9,t1
            mov  t2,t1
            sar  t1,pl9
            add  mixl,t1
            sar  t2,pr9
            add  mixr,t2
voices_ret  ret

envsub      movs rdenv,eptr         'calc 1 envelope every AudioLoop (=3.2kHz fs)
            movd wrenv,eptr
            sub  eptr,#3
            movs rdrt,eptr
rdenv       mov  envv,0-0           'envval [31..9] flags[8..0]
rdrt        mov  pcnt,0-0           'rates a[31..24] dr[23..16] s[15..8] vol[7..0]
            movs flgs,envv
            mov  t1,pcnt
            test flgs,#$180 wz      'flgs %00=attack
      if_nz jmp  #release
            shr  t1,#24
            shl  t1,#20
            add  envv,t1            'A
            max  envv,maxe  wc
      if_nc or   flgs,#$100
            jmp  #envmul
release     shl  t1,#8
            test flgs,#$80  wz
            mov  t2,t1
            shr  t1,#11             'D/R
            shl  t2,#8              'S
            cmp  envv,expe  wc      'exponential D/R with 2 segments
      if_ae shl  t1,#3
            cmp  envv,t2    wc
 if_nc_or_nz sub  envv,t1
            mins envv,#0
envmul      mov  fakt,pcnt
            mov  t1,envv
            call #mul8
            shr  t1,#24
'            and  t1,#$7F
            and  flgs,#$180
            or   flgs,t1
            movs envv,flgs
wrenv       mov  0-0,envv
            add  eptr,#3+11         'next Env
            cmp  eptr,#env0+110  wc
      if_ae mov  eptr,#env0
envsub_ret  ret                    '/51*4 with call

copypar     add  cmd,#rt0          'copy parameter to cogram
            movd copy,cmd          'cmd[25..9]=@pars cmd[8..0]=#vc*11
            movd shval,cmd
            shr  cmd,#9
            mov  pcnt,cmd
            and  pcnt,#7
            shr  cmd,#3     wz
      if_z  jmp  #noff
            cmp  pcnt,#4    wz
copy        rdlong 0-0,cmd
            add  cmd,#4
            add  copy,d_inc
            djnz pcnt,#copy
      if_nz jmp  #copend
            rdlong t1,cmd          '2mod/2pan shifts
            add  shval,d_offs
            mov  pcnt,#4
shval       mov  0-0,t1            '[31..24]=panR [23..16]=panL [15..8]=fb [7..0]=pm
            add  shval,d_inc
            shr  t1,#8
            djnz pcnt,#shval
copend      wrlong pcnt,cp
copypar_ret ret                     '/71*4 with call

noff        mov  t1,shval           'Note Off (flg1=1) if @pars=0
            shr  t1,#9
            movd clflg,t1
            mov  pcnt,#0
clflg       or   0-0,#$80           'set flag
            jmp  #copend

mul8        and  fakt,#$7F          'only 7 bits here
            movs t1,fakt            'Mult signed t1[31..8] by unsigned fakt[7..0]
            sar  t1,#1     wc       'signed result in t1[31..0]
            mov  fakt,t1
            andn fakt,#$FF
      if_nc sub  t1,fakt
            sar  t1,#1     wc
      if_c  add  t1,fakt
            sar  t1,#1     wc
      if_c  add  t1,fakt
            sar  t1,#1     wc
      if_c  add  t1,fakt
            sar  t1,#1     wc
      if_c  add  t1,fakt
            sar  t1,#1     wc
      if_c  add  t1,fakt
            sar  t1,#1     wc
      if_c  add  t1,fakt
mul8_ret    ret                     '20*4 cycles with call

'constants
bit31       long 1<<31
middle      long $8000_0000
rate        long 80_000 / 32        'sample rate
d_inc       long $200
d_offs      long $200*4
maxe        long $7FFFFFFE
expe        long $1FFF0000
maxout      long $3FFF8000
minout      long $C0001000
leftptr     long $8000 'point to ROM by default (cog 2 never overwrites these)
rightptr    long $8000

' registers
pp          long 0
t1          long 0
t2          long 0
fakt        long 0
flgs        long 0
tm          long 0
pcnt        long 0
mixl        long 0
mixr        long 0
envv        long 0
eptr        long env0
cmd         long 0
cp          long 0
auxp1       long 0
auxp2       long 0

' synth regs
phs01       long 0
phs02       long 0
fbrg0       long 0
rt0         long 0                  'bit[31..24]=a [23..16]=dr [15..8]=s [7..0]=vol
inc01       long 0
inc02       long 0
env0        long 0                  'bit[31..27]=ptr bit[26..8]=phs bit[7..0]=flgs
pm0         long 2                  '[8..0]
fb0         long 5                  '{8..0]
pl0         long 2                  '[8..0] Panorama left
pr0         long 2                  '[8..0] Panorama right

phs11       long 0
phs12       long 0
fbrg1       long 0
rt1         long 0
inc11       long 0
inc12       long 0
env1        long 0
pm1         long 2
fb1         long 5
pl1         long 2
pr1         long 2

phs21       long 0
phs22       long 0
fbrg2       long 0
rt2         long 0
inc21       long 0
inc22       long 0
env2        long 0
pm2         long 2
fb2         long 5
pl2         long 2
pr2         long 2

phs31       long 0
phs32       long 0
fbrg3       long 0
rt3         long 0
inc31       long 0
inc32       long 0
env3        long 0
pm3         long 2
fb3         long 5
pl3         long 2
pr3         long 2

phs41       long 0
phs42       long 0
fbrg4       long 0
rt4         long 0
inc41       long 0
inc42       long 0
env4        long 0
pm4         long 2
fb4         long 5
pl4         long 2
pr4         long 2

phs51       long 0
phs52       long 0
fbrg5       long 0
rt5         long 0
inc51       long 0
inc52       long 0
env5        long 0
pm5         long 2
fb5         long 5
pl5         long 2
pr5         long 2

phs61       long 0
phs62       long 0
fbrg6       long 0
rt6         long 0
inc61       long 0
inc62       long 0
env6        long 0
pm6         long 2
fb6         long 5
pl6         long 2
pr6         long 2

phs71       long 0
phs72       long 0
fbrg7       long 0
rt7         long 0
inc71       long 0
inc72       long 0
env7        long 0
pm7         long 2
fb7         long 5
pl7         long 2
pr7         long 2

phs81       long 0
phs82       long 0
fbrg8       long 0
rt8         long 0
inc81       long 0
inc82       long 0
env8        long 0
pm8         long 2
fb8         long 5
pl8         long 2
pr8         long 2

phs91       long 0
phs92       long 0
fbrg9       long 0
rt9         long 0
inc91       long 0
inc92       long 0
env9        long 0
pm9         long 2
fb9         long 5
pl9         long 2
pr9         long 2
            fit