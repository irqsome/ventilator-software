CON

'********************************************************************************************
' Propplay TV
' (c) 2012..2014 Piotr Kardasz pik33@o2.pl
' MIT license: see end of file
'********************************************************************************************


'****** this version is not suitable for overclocking because of hardcoded constants !!!!!!  **
'****** alpha - ugly hacks done to sidcog and ncowav.  ***************


  _clkmode = xtal1+pll16x
  _clkfreq = 80_000_000
  _stack = 128

  _SD_DO = 21
  _SD_CLK = 24
  _SD_DI = 20
  _SD_CS = 25
  _SD_WP = -1 ' -1 ifnot installed.
  _SD_CD = -1 ' -1 ifnot installed.



  rightPin = 10
  leftPin  = 11

  JOY_CLK = 16
  JOY_LCH = 17
  JOY_DATAOUT = 19 ' Only one controller

  KB_DATA = 8
  KB_CLOCK = 9


  HELP_SCREEN_SIZE = 1920 ' size of one help screen page

VAR

long pos
long stop
long time            
long dir_pos
long playrate
long il
long ild
long disp_pos
long disp_start
long dir_disp_pos
long dir_disp_start
long playcog
long playstack[128]
long wait
long dirs
long pause
long ends

byte cptr
byte buffer2[25]
byte namebuf[48]
byte namebuf_now[48]
byte fat4lock


OBJ

  kbm           : "Keyboard"
  '''vga        : "vn2034"
  tv            : "AiGeneric_Driver"
  fat           : "kyefat_lfn"
  fat2          : "kyefat_lfn"
  fat3          : "kyefat_lfn"
  fat4          : "kyefat_lfn"
  sid           : "sidcog"  
  pm_synth      : "pm_synth_20"
  wav           : "ncowav"
  'decomp1       : "decompress"

pub start | char, mx, my, dmx,dmy, buffer, i, code,s1,s2,g,j,l,x,y,w,tmpcnt

playcog:=-1

's2:=string("Scanning...")

' start components
kbm.startx(KB_DATA,KB_CLOCK, %0_000_100, %01_10000)
'''vga.start
tv.start(12)
'sid.start(rightPin, leftPin)
fat.FATEngineStart(_SD_DO, _SD_CLK, _SD_DI, _SD_CS, _SD_WP, _SD_CD, -1, -1, -1)
'''vga.cursoroff

'wait until they starts...

'waitcnt(clkfreq*3 + cnt)

'mount a SD

'''vga.writeln(string("Mounting SD card. Check SD card, reset your board if this text don't disappear"))
tv.str(string("Mounting SD card. Check SD card and reset the computer if this text doesn't disappear"))
i:=fat.mountpartition(0)
i:=fat2.mountpartition(0)
i:=fat3.mountpartition(0)
i:=fat4.mountpartition(0)
fat4.changeDirectory(string("PROPPLAY"))
fat4lock := locknew ' Just assume we got one....

'''vga.cls
tv.cls
'''vga.cursoroff



ild:=dirscan-1
il:=scan-1

' we have scanned directory here
fat2.openfile(string("dmplist.dat"),"R")
fat3.openfile(string("dirlist.dat"),"R")


init_display

pos:=0
disp_pos:=0
disp_start:=0

''display
''setdmpdisplay(0)

''dirdisplay
''setdirdisplay(0)
''dimdirdisplay


repeat i from 1 to 8
  '''vga.setfontcolor(23,i,3,3,0)
  '''vga.setfontcolor(25,i,3,3,0)
'stop:=1
'dirs:=0
setdirdisplay(0)
repeat
  displaytime(time)
  char:=getkey'kbm.key
  if char<>0 or ends==1
    if ends==1
      char:=13
      ends:=0
    if char==$6C9 'Ctrl-Alt-Del : Quit gracefully! (SD card gets modified!)
      if playcog>-1
        stop:=1
        repeat until stop==0
        waitcnt(cnt+clkfreq>>4)
      fat4.unmountPartition
      fat3.unmountPartition
      fat2.unmountPartition
      fat.unmountPartition
      fat.FATEngineStop
      reboot
    if char==$c3
      if dirs==0
        setdmpdisplay(1)
      else
        setdirdisplay(1)

    if char==$D0 ''F1
      'tv.outtextxy(0,1,@helpScreen)
      'decomp1.decomp_all(tv.GetBufferPtr+80,@helpCompressed)
      repeat while lockset(fat4lock)
      \fat4.openfile(string("HELP.SCR"),"r")
      repeat
        ifnot fat4.readData(tv.GetBufferPtr+80,HELP_SCREEN_SIZE)
          quit     
        kbm.clearkeys
        repeat until getkey
      fat4.closefile
      lockclr(fat4lock)
      tv.cls
      init_display
      if playcog>-1
        display_fileinfo(w)
      if dirs
        dirs~  ''force repaint
        setdirdisplay(0)
      else
        dirs~~ ''force repaint
        setdmpdisplay(0)
      
    
    if char==$c2
      if dirs==0
        setdmpdisplay(-1)
      else
        setdirdisplay(-1)

    if char==$c6
      if dirs==0
        setdmpdisplay(-10)
      else
        setdirdisplay(-10)

    if char==$c7
      if dirs==0
        setdmpdisplay(10)
      else
        setdirdisplay(10)

    if char==9
      if dirs==0
        setdirdisplay(0)

      else
        setdmpdisplay(0)

    if char=="+"
      if (playcog>-1) and (w==0)
        playrate+=10
        displayrate(playrate)

    if char=="-"
      if (playcog>-1) and (w==0)
        if playrate>10
          playrate-=10
          displayrate(playrate)


    if char=="S" or char=="s"
      stop:=1

    if char=="R" or char=="r"

      if playcog>-1
        stop:=1
        repeat until stop==0
        waitcnt(cnt+clkfreq>>4)
      setdmpdisplay(-100000)
      setdirdisplay(-100000)

      fat.deleteentry(string("dirlist.dat"))
      fat.deleteentry(string("dmplist.dat"))
      ild:=dirscan-1
      il:=scan-1


      fat2.openfile(string("dmplist.dat"),"R")
      fat3.openfile(string("dirlist.dat"),"R")

      pos:=0
      disp_pos:=0
      disp_start:=0
      dir_pos:=0
      dir_disp_pos:=0
      dir_disp_start:=0
      'display
      'setdmpdisplay(0)
      'dirdisplay
      setdirdisplay(0)

      'dirs:=0
      'dimdirdisplay
      setdmpdisplay(0)

    if char==" "
      pause:=not pause

    if char==13
      if dirs==0
        fat2.fileseek(pos*48)
        l:=fat2.readdata(@namebuf,48)
        if l==48
          playrate := 0
          ''free indent :)
            pause:=0
            if playcog<>-1
              stop:=1
              repeat until stop==0
              waitcnt(cnt+clkfreq>>2)
            ' determine file type
            l := strsize(@namebuf)
            if strcomp(@namebuf+l-4,string(".DMP"))
              w:=0
              playcog:=cognew(playcmd,@playstack)
            elseif strcomp(@namebuf+l-4,string(".WAV")) 'play a wave
              w:=1
              playcog:=cognew(wavplaycmd,@playstack)
            elseif strcomp(@namebuf+l-4,string(".MID")) 'play a wave
              w:=2
              playcog:=cognew(midplaycmd,@playstack)
            tmpcnt := cnt
            repeat until playrate <> 0 OR ((tmpcnt+clkfreq)-cnt)<1 'waste some cycles
            bytemove(@namebuf_now,@namebuf,48)
            display_fileinfo(w)
            setdmpdisplay(1)

      else
        if playcog>-1
          stop:=1
          repeat until stop==0
          waitcnt(cnt+clkfreq>>2)
        \fat3.fileseek(dir_pos*48)
        \fat3.readdata(@namebuf,48)
        \fat.changedirectory(@namebuf)
        \fat2.changedirectory(@namebuf)
        \fat3.changedirectory(@namebuf)
        setdmpdisplay(-100000)
        setdirdisplay(-100000)
        ild:=dirscan-1
        il:=scan-1


        fat2.openfile(string("dmplist.dat"),"R")
        fat3.openfile(string("dirlist.dat"),"R")

        pos:=0
        disp_pos:=0
        disp_start:=0
        dir_pos:=0
        dir_disp_pos:=0
        dir_disp_start:=0
        'display
        'setdmpdisplay(0)
        'dirdisplay
        setdmpdisplay(0)
        setdirdisplay(0)

        
        'dimdmpdisplay

pub displayrate(rate)

  tv.outtextxy(1,20,string("Playing frequency: "))
  tv.dec(rate)
  tv.str(string(" Hz          "))

pub display_fileinfo(type)

  tv.outtextxy(1,18,string("Name: "))
  tv.str(@namebuf_now+15)
  repeat 32-strsize(@namebuf_now+15)
    tv.out(" ")
  tv.goto(1,19)
  repeat 38
    tv.out(" ")
  tv.outtextxy(1,19,lookupz(type:string("Type: SID dump file"),string("Type: RIFF Wave file"),string("Type: MIDI (format 0)")))
  displayrate(playrate)

pub displaytime(time2) | secs,minss,hs

  secs:=(time2/100000)//60
  minss:=(time2/6000000)//60
  hs:=time2/360000000
  tv.goto(28,17)
  tv.out("")

             
  if hs<10
    
    tv.out("0")
    tv.out(48+hs)
  else
    tv.dec(hs)

  tv.out(":")
  
  if minss<10
    tv.out("0")
    tv.out(48+minss)
  else
    tv.dec(minss)

  tv.out(":")

  if secs<10
    tv.out("0")
    tv.out(48+secs)
  else
    tv.dec(secs)

  tv.out("")

pub init_display | s1
s1 := string("PropPlayTV v. 0.22 - 2019.12.14")
tv.goto(20-(strsize(s1)>>1),0)
tv.str(s1)
tv.box(0,17,39,24)
tv.putchar(1,17,"┴")
tv.putchar(38,17,"┴")
tv.outtextxy(1,16,string("┣────── Press F1 for Help! ──────┫"))
tv.outtextxy(5,17,string(" Now playing "))
tv.outtextxy(28,17,string("        ")) 
pub dimdmpdisplay | x,y,i

x:=11
y:=disp_pos+4
repeat i from 0 to 7
  '''vga.setfontcolor(y,i+x,0,0,2)
  '''vga.setbackcolor(y,i+x,2,2,2)

pub dimdirdisplay | x,y,i

x:=1
y:=dir_disp_pos+4
repeat i from 0 to 7
  '''vga.setfontcolor(y,i+x,0,0,2)
  '''vga.setbackcolor(y,i+x,2,2,2)


pub setdmpdisplay(a)|  i,x,y,did_display

x:=1
y:=disp_pos+2
tv.putchar(x,y," ")


if (pos+a)>il
  a:=il-pos
if (pos+a)<0
  a:=-pos

pos+=a 'set new list position

did_display := false

if (disp_pos+a)>-1 and (disp_pos+a)<13
  disp_pos+=a

else
  if disp_pos+a>12
    disp_start:=pos-12
    display
    disp_pos:=12
    did_display:=true

  if disp_pos+a<0
    disp_start:=pos
    display
    disp_pos:=0
    did_display:=true

if dirs AND !did_display
  display
dirs:=false

x:=1
y:=disp_pos+2
tv.putchar(x,y,"▶")



pub display |l,x,y,i

tv.box(0,1,39,15)
tv.outtextxy(5,1,string(" Media files "))
tv.putchar(1,15,"┬")
tv.putchar(38,15,"┬")

'fat2.openfile(string("dmplist.dat"),"R")
repeat i from disp_start to disp_start+12
  fat2.fileseek(48*i)
  l:=fat2.readdata(@namebuf,48)
  if l==48
    y:=i+2-disp_start
    tv.goto(1,y)
    repeat 40-2
      tv.out(" ")
    tv.outtextxy(3,y,@namebuf+15)
  else
    y:=i+2-disp_start                                                 
    tv.goto(1,y)
    repeat 40-2
      tv.out(" ")


pub setdirdisplay(a)|  i,x,y,did_display

x:=1
y:=dir_disp_pos+2
tv.putchar(x,y," ")


if (dir_pos+a)>ild
  a:=ild-dir_pos
if (dir_pos+a)<0
  a:=-dir_pos

dir_pos+=a 'set new list position

did_display := false

if (dir_disp_pos+a)>-1 and (dir_disp_pos+a)<13
  dir_disp_pos+=a

else
  if dir_disp_pos+a>12
    dir_disp_start:=pos-12
    dirdisplay
    dir_disp_pos:=13
    did_display:=true

  if dir_disp_pos+a<0
    dir_disp_start:=pos
    dirdisplay
    dir_disp_pos:=0
    did_display:=true

if !dirs AND !did_display
  dirdisplay
dirs:=true

x:=1
y:=dir_disp_pos+2
tv.putchar(x,y,"▶") 



pub dirdisplay |l,x,y,i

tv.box(0,1,39,15)
tv.outtextxy(5,1,string(" Directories "))
tv.putchar(1,15,"┬")
tv.putchar(38,15,"┬")
'fat2.openfile(string("dirlist.dat"),"R")
repeat i from dir_disp_start to dir_disp_start+12
  fat3.fileseek(48*i)
  l:=fat3.readdata(@namebuf,48)
  if l==48
    y:=i+2-dir_disp_start
    tv.goto(1,y) 
    repeat 40-2
      tv.out(" ")
    tv.outtextxy(3,y,@namebuf+15)
  else
    y:=i+2-dir_disp_start
    tv.goto(1,y)
    repeat 40-2
      tv.out(" ")



pub playcmd |c,i,w,s

repeat while lockset(fat4lock)
stop:=0
\fat4.openfile(string("SID.COG"),"r") 'Load SID.COG into ncowav's buffer (not used in synth mode)
fat4.readData(wav.getbuf,constant(512*4))

c:=sid.start(wav.getbuf,wav.getbuf) 'ASM gets overwritten by COMBWAVE.DAT
waitcnt(cnt+constant(496*16))
\fat4.openfile(string("COMBWAVE.DAT"),"r")
fat4.readData(wav.getbuf,1024)
\fat4.closefile
lockclr(fat4lock)

s:=sid.getsidsample

wav.start(leftPin,rightPin,1,s)

playrate:=namebuf[13]+256*namebuf[14] 
\fat.openfile(@namebuf,"r")
w:=clkfreq/playrate
c:=fat.readdata(@buffer2,25)
if c<>25
  '''vga.outtextxy(4,22,string("File not found"))
  return

time:=0
wait:=cnt
repeat
  ifnot pause
    w:=clkfreq/playrate
    wait:=wait+w
    ifnot (cnt>0) and (wait<0)
      repeat until cnt>wait
    else
      repeat until cnt<0
      repeat until cnt>wait
    c:=fat.readdata(@buffer2,25)
    sid.updateRegisters(@buffer2)
'    '''vga.outtextxy(0,0,'''vga.inttostr(long[s]))
    time:=time+100000/playrate
  else
    w:=clkfreq/playrate
    wait:=wait+w
    ifnot (cnt>0) and (wait<0)
      repeat until cnt>wait
    else
      repeat until cnt<0
      repeat until cnt>wait
    bytefill(@buffer2,0,25)
    sid.updateregisters(@buffer2)
until (c<>25) or (stop==1)
fat.closefile
if c<>25
  ends:=1
bytefill(@buffer2,0,25)
sid.updateRegisters(@buffer2)
waitcnt(cnt+clkfreq>>2)
stop:=0
c:=playcog
playcog:=-1
sid.stop
wav.stop
cogstop(c)

pub wavplaycmd |i,w,buf,buf2,bufnum,c,cog

playrate := 44100''TODO
stop:=0
time:=0
\fat.openfile(@namebuf,"r")
buf:=wav.getbuf
bufnum:=wav.getbufnum
longfill(buf,0,512)
cog:=wav.start(leftPin,rightPin,0,0)
fat.readdata(buf+1900,44)
fat.readdata(buf,2048)
repeat
  repeat until long[bufnum]>1024
  if pause
    longfill(buf,0,256)
  else
    i:=fat.readdata(buf,1024)
  repeat until long[bufnum]<1024
  if pause
    longfill(buf+1024,0,256)
  else
    i:=fat.readdata(buf+1024,1024)
    time:=time+1161
until (i<>1024) or (stop==1)
if i<>1024
  ends:=1
longfill(buf,0,512)
waitcnt(cnt+clkfreq>>2)
wav.stop
c:=\fat.closefile
stop:=0
c:=playcog
playcog:=-1
cogstop(c)


PUB midplaycmd |c,left,right 'yes, sample buffers on the Spin stack

playrate := 1
stop:=0
time:=0

repeat while lockset(fat4lock)

\fat4.openfile(string("pmsynth.cog"),"r")
fat4.readData(wav.getbuf,constant(512*4))
pm_synth.start(@left,@right,2,wav.getbuf,wav.getbuf+constant(128*10),wav.getbuf)
'pm_synth.start(@left,@right,2,@pres,@drset,wav.getbuf)
waitcnt(cnt+constant(512*16))
\fat4.openfile(string("GENMIDI.dat"),"r") 'Load GENMIDI.DAT into buffer
fat4.readData(wav.getbuf,fat4.filesize <# 4096)
\fat4.closefile
lockclr(fat4lock)

\fat.openfile(@namebuf,"r")  

wav.start(leftPin,rightPin,2,@left)
midiplay

wav.stop
pm_synth.allOff
waitcnt(cnt+clkfreq) 'give the notes some time to release
pm_synth.stop

c:=\fat.closefile
  

stop:=0 
c:=playcog
playcog:=-1
cogstop(c)

pub  dirscan|i,buffer,c,freq,l

'fat.deleteentry(string("dirlist.dat"))
i:=0
c:=\fat.listentry(string("dirlist.dat"))
if fat.partitionerror<>0
  c:=\fat2.newfile(string("dirlist.dat"))
  fat2.openfile(string("dirlist.dat"),"W")
  fat.listEntries("W")
  repeat while(buffer := \fat.listEntries("N"))
    l:=11
    repeat while byte[buffer+l]==32
      byte[buffer+l]:=0
      l-=1
    l:=strsize(buffer)
    if fat.listIsDirectory
      bytefill(@namebuf,0,48)
      bytemove(@namebuf,buffer,13)

      bytemove(@namebuf+15,buffer,l)
      if fat.islfn

        bytemove(@namebuf+15,fat.getlfn,32)
      i:=i+1
      fat2.writedata(@namebuf,48)

  fat2.closefile

fat2.openfile(string("dirlist.dat"),"R")
i:=fat2.filesize/48
fat2.closefile
return i



pub  scan|i,buffer,c,freq,l,m,n,w,v

i:=0
'fat.deleteentry(string("dmplist.dat"))
c:=\fat.listentry(string("dmplist.dat"))
if fat.partitionerror<>0
  c:=\fat2.newfile(string("dmplist.dat"))
  fat2.openfile(string("dmplist.dat"),"W")
  fat.listEntries("W")
  repeat while(buffer := \fat.listEntries("N"))

    l:=0
    repeat l from 0 to 11
      if byte[buffer+l]==32
        byte[buffer+l]:=0
    l:=strsize(buffer)
    if (l>4)
      v := true
      if (strcomp(buffer+l-4,string(".WAV")))
        w:=1
      elseif (strcomp(buffer+l-4,string(".DMP")))
        w:=0
      elseif (strcomp(buffer+l-4,string(".MID")))
        w:=2
      else
        v:= false
      if v
        bytefill(@namebuf,0,48)
        bytemove(@namebuf,buffer,13)
        \fat3.openfile(buffer,"r")



        c:=\fat3.readdata(@buffer2,25)
        if c==25
          if (buffer2[0]=="S") and (buffer2[1]=="D") and (buffer2[2]=="M") and (buffer2[3]=="P")
            freq:=buffer2[4]
            namebuf[13]:=freq 'lo
            freq:=buffer2[5]
            namebuf[14]:=freq 'hi
            bytemove(@namebuf+15,@buffer2+8,16)
            if fat.islfn
               striplfn
               bytemove(@namebuf+15,fat.getlfn,32)
          else
            byte[buffer+l-4]:=0
            bytemove(@namebuf+15,buffer,l)
            if fat.islfn
              striplfn
              bytemove(@namebuf+15,fat.getlfn,32)

            namebuf[13]:=50
            namebuf[14]:=0
            if w==1
              namebuf[13]:=68
              namebuf[14]:=172

          i:=i+1
          fat2.writedata(@namebuf,48)
          '''vga.outtextxy(45,1,'''vga.inttostr(i))

  fat2.closefile

fat2.openfile(string("dmplist.dat"),"R")
i:=fat2.filesize/48
fat2.closefile

'''vga.outtextxy(34,1, string("               "))
result:=i


pub striplfn |m,n

m:=strsize(fat.getlfn)
repeat n from m-4 to m
  byte[fat.getlfn+n]:=0
repeat n from 0 to m
  if byte[fat.getlfn+n]=="_"
    byte[fat.getlfn+n]:=" "


DAT

'helpCompressed file "help.exc"
{{

┌──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
│                                                   TERMS OF USE: MIT License                                                  │
├──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤
│Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    │
│files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    │
│modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software│
│is furnished to do so, subject to the following conditions:                                                                   │
│                                                                                                                              │
│The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.│
│                                                                                                                              │
│THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          │
│WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         │
│COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   │
│ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         │
└──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
}}

'''''''''''''''''''''''''''''
''' SNES pad gunk starts here
'''''''''''''''''''''''''''''
CON
  SNES0_RIGHT    = %00000000_10000000
  SNES0_LEFT     = %00000000_01000000
  SNES0_DOWN     = %00000000_00100000
  SNES0_UP       = %00000000_00010000
  SNES0_START    = %00000000_00001000
  SNES0_SELECT   = %00000000_00000100
  SNES0_Y        = %00000000_00000010
  SNES0_B        = %00000000_00000001
  SNES0_A        = %00000001_00000000
  SNES0_X        = %00000010_00000000
  SNES0_L        = %00000100_00000000
  SNES0_R        = %00001000_00000000

  SNES_QUIT      = SNES0_START+SNES0_L+SNES0_R

PUB NES_Read_Gamepad : nes_bits    |  i
' //////////////////////////////////////////////////////////////////
' SNES Game Pad Read
' //////////////////////////////////////////////////////////////////

' step 1: set I/Os
DIRA [JOY_CLK]~~ ' output
DIRA [JOY_LCH]~~ ' output
DIRA [JOY_DATAOUT]~ ' input

' step 2: set clock and latch to 0
OUTA [JOY_CLK]~
OUTA [JOY_LCH]~
'Delay(1)

' step 3: set latch to 1
OUTA [JOY_LCH]~~     

' step 4: set latch to 0
OUTA [JOY_LCH]~

' step 5: read 16 bits, 1st bits are already latched and ready, simply save and clock remaining bits
repeat i from 0 to 15

 nes_bits := (nes_bits << 1) | INA[JOY_DATAOUT]

 OUTA [JOY_CLK] := 1 ' JOY_CLK = 1     
 OUTA [JOY_CLK] := 0 ' JOY_CLK = 0
                      

' invert bits to make positive logic
nes_bits := (!(nes_bits><16) & $0FFF)
if nes_bits == $0FFF
  nes_bits~ 

VAR
  word pad_laststate,pad_lastread


PUB getkey | pad_state
ifnot result := kbm.key
  ifnot (||(cnt-pad_lastread))>(clkfreq/60) ' Not longer than 1/60th second since last poll?
    return
  pad_lastread := cnt
  pad_state := NES_Read_Gamepad
  if (pad_state & SNES_QUIT) == SNES_QUIT
    return $6C9 'emulate Ctrl-Alt-Del
  'get new key bit and lookup keycode to emulate            B   Y  Select Start Up  Down Left Right  A   X   L   R
  result := lookup(>|((pad_state^pad_laststate)&pad_state):" ","-", "R"  , "S" ,$C2,$C3 ,$C0 ,$C1  ,$0D,"+",$D0,$09)
  pad_laststate := pad_state
       
  

DAT
''''''''''''''''''''''''''''''''''''''''''
'''COPY-PASTE FROM MIDI PLAYER STARTS HERE
''''''''''''''''''''''''''''''''''''''''''
VAR    
  long  time3, tq, ts, us
PUB midiplay   : lg | dt, b, v, st, d1, d2, ch, tt
  us := clkfreq / 1_000_000                             'ticks / microsecond
  b := st := d1 := d2 := v := 0
  'ifnot sd.popen(name,"r")
    fat.readData(@v,4)
    if v == constant("M" + "T"<<8 + "h"<<16 + "d"<<24)  'Header ID
      fat.readData(@v,4)                                'Header Len
      v~
      fat.readData(@v,2)                                'Format
      if v <> 0
        'disp.str(string("only Format 0 supported"))
        return ''TODO: Add error handling
      else
        fat.readData(@v,2)                              'Tracks
        fat.readData(@v,2)                              'Ticks/Quarter
        tq := v>>8 + (v&255)<<8
        ts := 500_000 / tq                              'default Tempo 120 BPM
        fat.readData(@v,4)                              '"MTrk"
        fat.readData(@v,4)                              'Track length
        lg := v>>24 + (v>>16&255)<<8 + (v>>8&255)<<16
        time3 := cnt
        repeat
          
            fat.readData(@b,1)                            'get delta time       
            lg--
            dt := b & $7F
            repeat while b>127
              fat.readData(@b,1)
              lg--
              dt := dt<<7 + (b & $7F)
            tt := dt*ts
            time3 := time3 + tt*us
            repeat until time3-cnt < 0 AND (stop or !pause) 'wait deltatime
              if pause
                repeat until (stop or !pause)
                time3 := cnt
            time += tt/10                               'advance timer display
            b~
            fat.readData(@b,1)                            'MIDI byte
            if b > 127
              st := b                                     'new status byte
            elseif st==$FF
              st~
            ch := st & $0F
            d1~
            d2~
            case st & $F0                                 'decode MIDI Event
              $90: fat.readData(@d1,1)                    'Note 
                   fat.readData(@d2,1)                    'Velocity
                   lg -= 3
                   if d2{>0}
                     pm_synth.noteOn(d1,ch,d2)            'Note On
                    ''' show(15,ch+1,string(127),-1)
                   else
                     pm_synth.noteOff(d1,ch)              'Note Off if Vel=0
                    ''' show(15,ch+1,string("."),-1)
              $80: fat.readData(@d1,1)                    'Note Off 
                   fat.readData(@d2,1)                    'Velocity
                   lg -= 3
                   pm_synth.noteOff(d1,ch)
                  ''' show(15,ch+1,string("."),-1)
                   '''if ch==9
                    ''' show(20,ch+1,string(">"),d1)
              $A0: fat.readData(@d1,1)                    'Poly AfterTouch 
                   fat.readData(@d2,1)                    '(not supported)
                   lg -= 3
              $C0: fat.readData(@d1,1)                    'Prg Change 
                   lg -= 2
                   pm_synth.prgChange(d1,ch)
                  ''' show(0,ch+1,string("Ch:"),ch+1)
                  ''' show(6,ch+1,string("Prg:"),d1)
              $D0: fat.readData(@d1,1)                    'Mono AfterTouch 
                   lg -= 2                                '(not supported)
              $B0: fat.readData(@d1,1)                    'Controller (nr) 
                   fat.readData(@d2,1)                    'value
                   lg -= 3
                   if d1==7
                     pm_synth.volContr(d2,ch)             '7=Vol
                   if d1==10
                     pm_synth.panContr(d2,ch)             '10=Pan
              $E0: fat.readData(@d1,1)                    'Pitch Bender 
                   fat.readData(@d2,1)                    '(not supported!)
                   lg -= 3
              $F0: fat.readData(@d1,1)                    'Meta Event
                   v~
                   if st==$F0 or st==$F7                  'SysEx
                     repeat
                       fat.readData(@v,1)
                       lg--
                       d1--
                     until v==$F7 or b=< 0
                     d1~
                   else
                     lg -= 2
                     fat.readData(@b,1)                   'Len
                     repeat while b{>0}                   'read meta data
                       fat.readData(@d2,1)
                       v := v<<8 + d2
                       b--
                       lg--
                     if d1==81                            'Tempo
                       ts := v / tq
              0: fat.readData(@d1,1)                      'Meta Event Running status
                 lg -= 2
                 fat.readData(@b,1)                     'Len
                 repeat while b{>0}                     'read meta data
                   fat.readData(@d2,1)
                   v := v<<8 + d2
                   b--
                   lg--
          
        until lg < 1 or stop
        if lg < 1
          ends:=1
    else
      ''disp.str(string("No MIDI file")
      return ''TODO: add error handling