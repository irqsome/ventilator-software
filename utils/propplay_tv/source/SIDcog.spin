' This is version modified for propplay 0.18 - use original sidcog in your projects !!! 
{
┌──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
│                            SIDcog - SID/MOS8580 emulator v1.3 (C) 2012 Johannes Ahlebrand                                    │                                                            
├──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤
│                                    TERMS OF USE: Parallax Object Exchange License                                            │                                                            
├──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤
│Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    │ 
│files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    │
│modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software│
│is furnished to do so, subject to the following conditions:                                                                   │
│                                                                                                                              │
│The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.│
│                                                                                                                              │
│THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          │
│WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         │
│COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   │
│ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         │
└──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘


Revision History

v0.7 (December 2009) - Initial release

v0.8 (February 2010) - Added support for "combined waveforms"
                     - Optimized code
                     - Fixed bugs
                       
v1.0 (May 2011)      - First OBEX release
                     - Added convenient API methods

v1.2 (August 2011)   - Increased ADSR accuracy  (Almost perfect now; The famous ADSR bug isn't implemented though)
                     - Increased Noise accuracy (As close as we will get without decreasing the sample rate of SIDcog)
                     - Fixed a bug when no waveform was selected
                     - Decreased the "max cutoff frequency" a bit to fix aliasing issues
                     - Made some small optimizations 

v1.3 (April 2012)    - Fixed a bug when noise + any other waveform was selected at the same time
                     - Calibrated the cutoff frequency to better match a real 8580
                     - Cycle optimized code to "make room" for the point below 
                     - Increased resonance accuracy (replaced "4 step logaritmic lookup table" with "16 step bit linear multiplication")
                     - Increased ADSR accuracy a little bit more (the ADSR bug is still not implemented)

} 
CON  PAL = 985248.0, NTSC = 1022727.0, MAXF = 1031000.0, TRIANGLE = 16, SAW = 32, SQUARE = 64, NOISE = 128   

  C64_CLOCK_FREQ   = PAL
  '                                  ___ 
  RESONANCE_OFFSET = 6'                 │
  RESONANCE_FACTOR = 5'                 │
  CUTOFF_LIMIT     = 1100'              │
  LP_MAX_CUTOFF    = 11'                │ Don't alter these constants unless you know what you are doing! 
  BP_MAX_CUTOFF    = 10'                │
  FILTER_OFFSET    = 12'                │
  START_LOG_LEVEL  = $5d5d5d5d'         │
  DECAY_DIVIDE_REF = $6C6C6C6C'         │   
  ENV_CAL_FACTOR   = 545014038.181330'  │ ENV_CAL_FACTOR = (MaxUint32  / SIDcogSampleFreq) / (1 / SidADSR_1secRomValue)   
  NOISE_ADD        = %1010_1010_101<<23'│ ENV_CAL_FACTOR = (4294967295 / 30789           ) / (1 / 3907                ) = 545014038,181330
  NOISE_TAP        = %100001 << 8'   ___│


pub getsidsample

return @sidsample


PUB start(cogptr,combinedWaveforms)
' ┌──────────────────────────────────────────────────────────────┐
' │               Starts SIDcog in a single cog                  │
' ├──────────────────────────────────────────────────────────────┤
' │ Returns a pointer to the first SID register in hub memory    │
' │ on success; otherwise returns 0.                             │
' │                                                              │ 
' │ right - The pin to output the right channel to. 0 = Not used │
' │                                                              │
' │ left - The pin to output the left channel to. 0 = Not used   │
' └──────────────────────────────────────────────────────────────┘
  


  long[cogptr+4] := combinedWaveforms
  long[cogptr+8] := getsidsample
  cog := cognew(cogptr, @ch1_frequencyLo) + 1

  if cog
    return cog '@ch1_frequencyLo
  else
    return 0

PUB stop
' ┌──────────────────────────────────────────────────────────────┐
' │                        Stops SIDcog                          │
' └──────────────────────────────────────────────────────────────┘
  if cog
    cogstop(cog~ -1)
    cog := 0

PUB setRegister(reg, val) 
' ┌──────────────────────────────────────────────────────────────┐
' │           Sets a single SID register to a value              │
' ├──────────────────────────────────────────────────────────────┤
' │ reg - The SID register to set.                               │
' │                                                              │
' │ val - The value to set the register to.                      │
' └──────────────────────────────────────────────────────────────┘
  byte[@ch1_frequencyLo + (reg + (reg/7))] := val
  
PUB updateRegisters(source)
' ┌──────────────────────────────────────────────────────────────┐
' │                 Update all 25 SID registers                  │
' ├──────────────────────────────────────────────────────────────┤
' │ source - A pointer to an array containing 25 bytes to update │
' │          the 25 SID registers with.                          │
' └──────────────────────────────────────────────────────────────┘
  bytemove(@ch1_frequencyLo, source, 7) 
  bytemove(@ch1_frequencyLo + 8 , source + 7 , 7)
  bytemove(@ch1_frequencyLo + 16, source + 14, 7)
  bytemove(@ch1_frequencyLo + 24, source + 21, 4)

PUB resetRegisters
' ┌──────────────────────────────────────────────────────────────┐
' │                 Reset all 25 SID registers                   │
' └──────────────────────────────────────────────────────────────┘
  bytefill(@ch1_frequencyLo, 0, 25)
  
PUB setVolume(volumeValue)
' ┌──────────────────────────────────────────────────────────────┐
' │                    Sets the main volume                      │
' ├──────────────────────────────────────────────────────────────┤
' │ value - A value betwen 0 and 15.                             │
' └──────────────────────────────────────────────────────────────┘
  byte[@Volume] := (byte[@Volume]&$F0) | (volumeValue&$0F)  

PUB play(channel, freq, waveform, attack, decay, sustain, release) | offs
' ┌──────────────────────────────────────────────────────────────┐
' │                Plays a tone in a SID channel.                │
' ├──────────────────────────────────────────────────────────────┤
' │ channel - The SID channel to use. (0 - 2)                    │
' │                                                              │
' │ freq - The 16 bit frequency value use. (0 - 65535)           │
' │ (The SID can output tone frequencies from 0 - 3.9 kHz)       │
' │                                                              │
' │ waveform - The waveform combination to use.                  │
' │ e.g. sid.play(x, x, sid#SQUARE | sid#SAW, x, x, x, x)        │
' │                                                              │
' │ attack - The attack value. (0 - 15)                          │
' │                                                              │
' │ decay - The decay value. (0 - 15)                            │
' │                                                              │
' │ sustain - The sustain value. (0 - 15)                        │
' │                                                              │
' │ release - The release value. (0 - 15)                        │
' ├──────────────────────────────────────────────────────────────┤
' │ - When calling this method, the envelope generator enters the│
' │ "attack - decay - sustain" phase. Don't forget to call       │
' │ "noteOff" before using it so the envelope is in release phase│
' └──────────────────────────────────────────────────────────────┘

  offs := channel<<3
  word[@ch1_frequencyLo + offs] := freq   
  byte[@ch1_attackDecay + offs] := (decay&$F) | ((attack&$F)<<4)
  byte[@ch1_sustainRelease + offs] := (release&$F) | ((sustain&$F)<<4) 
  byte[@ch1_controlRegister + offs] := (byte[@ch1_controlRegister + offs]&$0F) | waveform | 1

PUB noteOn(channel, freq) | offs
' ┌──────────────────────────────────────────────────────────────┐
' │                Plays a tone in a SID channel                 │
' ├──────────────────────────────────────────────────────────────┤
' │ channel - The SID channel to use. (0 - 2)                    │
' │                                                              │
' │ freq - The 16 bit frequency value use. (0 - 65535)           │
' │ (The SID can output tone frequencies from 0 - 3.9 kHz)       │
' ├──────────────────────────────────────────────────────────────┤
' │ - Don't forget to set the envelope values for the channel    │
' │ before using this method.                                    │ 
' │                                                              │
' │ - Make sure you have set the waveform for the channel before │
' │ using this method.                                           │
' │                                                              │ 
' │ - When calling this method, the envelope generator enters the│
' │ "attack - decay - sustain" phase. Don't forget to call       │
' │ "noteOff" before calling this method to set the envelope to  │
' │ release phase.                                               │
' └──────────────────────────────────────────────────────────────┘
  offs := channel<<3
  byte[@ch1_controlRegister + offs] := (byte[@ch1_controlRegister+offs]&$FE) | 1 
  word[@ch1_frequencyLo + offs] := freq
  
PUB noteOff(channel)
' ┌──────────────────────────────────────────────────────────────┐
' │  Sets the envelope generator of a channel to release phase   │
' ├──────────────────────────────────────────────────────────────┤
' │ channel - The SID channel to use. (0 - 2)                    │
' └──────────────────────────────────────────────────────────────┘
  byte[@ch1_controlRegister + (channel<<3)] &= $FE

PUB setFreq(channel, freq)
' ┌──────────────────────────────────────────────────────────────┐
' │             Sets the frequency of a SID channel              │
' ├──────────────────────────────────────────────────────────────┤
' │ channel - The SID channel to use. (0 - 2)                    │
' │                                                              │ 
' │ freq - The 16 bit frequency value. (0 - 65535)               │
' │ (The SID can output tone frequencies from 0 - 3.9 kHz)       │
' └──────────────────────────────────────────────────────────────┘
  word[@ch1_frequencyLo + (channel<<3)] := freq

PUB setWaveform(channel, waveform) | offs  
' ┌──────────────────────────────────────────────────────────────┐
' │             Sets the waveform of a SID channel               │
' ├──────────────────────────────────────────────────────────────┤
' │ channel - The SID channel to use. (0 - 2)                    │
' │                                                              │ 
' │ waveform - The waveform combination to use.                  │
' │ e.g. sid.setWaveform(x, sid#SQUARE | sid#SAW)                │
' └──────────────────────────────────────────────────────────────┘ 
  offs := channel<<3
  byte[@ch1_controlRegister+offs] := (byte[@ch1_controlRegister + offs]&$0F) | waveform 
      
PUB setPulseWidth(channel, pulseWidth)
' ┌──────────────────────────────────────────────────────────────┐
' │           Sets the pulse width of a SID channel              │
' ├──────────────────────────────────────────────────────────────┤
' │ channel - The SID channel to use. (0 - 2)                    │
' │                                                              │ 
' │ pulseWidth - The 12 bit pulse width value to use. (0 - 4095) │
' │ e.g. sid.setWaveform(x, sid#SQUARE | sid#SAW)                │
' ├──────────────────────────────────────────────────────────────┤  
' │ - The pulse width value affects square waves ONLY.           │
' └──────────────────────────────────────────────────────────────┘ 
  word[@ch1_pulseWidthLo + (channel<<3)] := pulseWidth 
     
PUB setADSR(channel, attack, decay, sustain, release) | offs
' ┌──────────────────────────────────────────────────────────────┐
' │         Sets the envelope values of a SID channel            │
' ├──────────────────────────────────────────────────────────────┤
' │ channel - The SID channel to use. (0 - 2)                    │
' │                                                              │
' │ attack - The attack value. (0 - 15)                          │
' │                                                              │
' │ decay - The decay value. (0 - 15)                            │
' │                                                              │
' │ sustain - The sustain value. (0 - 15)                        │
' │                                                              │
' │ release - The release value. (0 - 15)                        │
' └──────────────────────────────────────────────────────────────┘
  offs := channel<<3
  byte[@ch1_attackDecay + offs] := (decay&$F) | ((attack&$F)<<4)
  byte[@ch1_sustainRelease + offs] := (release&$F) | ((sustain&$F)<<4) 

PUB setResonance(resonanceValue)
' ┌──────────────────────────────────────────────────────────────┐
' │           Sets the resonance value of the filter             │
' ├──────────────────────────────────────────────────────────────┤
' │ resonanceValue - The resonance value to use. (0 - 15)        │
' └──────────────────────────────────────────────────────────────┘
  byte[@Filter3] := (byte[@Filter3]&$0F) | (resonanceValue<<4)
  
PUB setCutoff(cutoffValue)
' ┌──────────────────────────────────────────────────────────────┐
' │          Sets the cutoff frequency of the filter             │
' ├──────────────────────────────────────────────────────────────┤
' │ cutoffValue - The 12 bit cutoff frequency value to use.        │
' └──────────────────────────────────────────────────────────────┘
  byte[@Filter1] := cutoffValue&$07
  byte[@Filter2] := (cutoffValue&$07F8)>>3

PUB setFilterMask(ch1, ch2, ch3)
' ┌──────────────────────────────────────────────────────────────┐
' │             Enable/Disable filtering on channels             │
' ├──────────────────────────────────────────────────────────────┤
' │ ch1 - Enable/Disable filter on channel 1. (True/False)       │
' │                                                              │
' │ ch2 - Enable/Disable filter on channel 2. (True/False)       │   
' │                                                              │
' │ ch3 - Enable/Disable filter on channel 3. (True/False)       │   
' └──────────────────────────────────────────────────────────────┘
  byte[@Filter3] := (byte[@Filter3]&$F0) | (ch1&1) | (ch2&2) | (ch3&4)

PUB setFilterType(lp, bp, hp)
' ┌──────────────────────────────────────────────────────────────┐
' │                 Enable/Disable filter types                  │
' ├──────────────────────────────────────────────────────────────┤
' │ lp - Enable/Disable lowpass filter. (True/False)             │
' │                                                              │
' │ bp - Enable/Disable bandpass filter. (True/False)            │   
' │                                                              │
' │ hp - Enable/Disable highpass filter. (True/False)            │   
' └──────────────────────────────────────────────────────────────┘
  byte[@volume] := (byte[@volume]&$0F) | (lp&16) | (bp&32) | (hp&64) 

PUB enableRingmod(ch1, ch2, ch3)
' ┌──────────────────────────────────────────────────────────────┐
' │          Enable/Disable ring modulation on channels          │
' ├──────────────────────────────────────────────────────────────┤
' │ ch1 - Enable/Disable ring modulation on ch 1. (True/False)   │
' │                                                              │
' │ ch2 - Enable/Disable ring modulation on ch 2. (True/False)   │  
' │                                                              │
' │ ch3 - Enable/Disable ring modulation on ch 3. (True/False)   │   
' ├──────────────────────────────────────────────────────────────┤ 
' │- Channel 3 modulates channel 1                               │
' │                                                              │
' │- Channel 1 modulates channel 2                               │
' │                                                              │ 
' │- Channel 2 modulates channel 3                               │  
' └──────────────────────────────────────────────────────────────┘
  byte[@ch1_controlRegister] := (byte[@ch1_controlRegister]&$FB) | (ch1&4)
  byte[@ch2_controlRegister] := (byte[@ch2_controlRegister]&$FB) | (ch2&4)
  byte[@ch3_controlRegister] := (byte[@ch3_controlRegister]&$FB) | (ch3&4)

PUB enableSynchronization(ch1, ch2, ch3)
' ┌──────────────────────────────────────────────────────────────┐
' │    Enable/Disable oscillator synchronization on channels     │
' ├──────────────────────────────────────────────────────────────┤
' │ ch1 - Enable/Disable synchronization on ch 1. (True/False)   │
' │                                                              │
' │ ch2 - Enable/Disable synchronization on ch 2. (True/False)   │  
' │                                                              │
' │ ch3 - Enable/Disable synchronization on ch 3. (True/False)   │   
' ├──────────────────────────────────────────────────────────────┤
' │- Channel 3 synchronizes channel 1                            │
' │                                                              │
' │- Channel 1 synchronizes channel 2                            │
' │                                                              │ 
' │- Channel 2 synchronizes channel 3                            │  
' └──────────────────────────────────────────────────────────────┘ 
  byte[@ch1_controlRegister] := (byte[@ch1_controlRegister]&$FD) | (ch1&2)
  byte[@ch2_controlRegister] := (byte[@ch2_controlRegister]&$FD) | (ch2&2)
  byte[@ch3_controlRegister] := (byte[@ch3_controlRegister]&$FD) | (ch3&2)
    
DAT
  ''assembly code snipped                     

DAT
  ''no

VAR
  byte ch1_frequencyLo       
  byte ch1_frequencyHi     
  byte ch1_pulseWidthLo     
  byte ch1_pulseWidthHi    
  byte ch1_controlRegister 
  byte ch1_attackDecay     
  byte ch1_sustainRelease   
  byte ch1_dummy            
  byte ch2_frequencyLo     
  byte ch2_frequencyHi     
  byte ch2_pulseWidthLo     
  byte ch2_pulseWidthHi    
  byte ch2_controlRegister 
  byte ch2_attackDecay      
  byte ch2_sustainRelease   
  byte ch2_dummy            
  byte ch3_frequencyLo      
  byte ch3_frequencyHi    
  byte ch3_pulseWidthLo     
  byte ch3_pulseWidthHi    
  byte ch3_controlRegister  
  byte ch3_attackDecay      
  byte ch3_sustainRelease  
  byte ch3_dummy           
  byte Filter1              
  byte Filter2             
  byte Filter3             
  byte Volume
  byte oldVolume
  long SIDSample
  long cog
                                