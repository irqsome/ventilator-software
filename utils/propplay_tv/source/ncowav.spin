{{

This is ncowav version for using with a propplay. Get a full version of this from its topic on the propeller forum
}}


  _clkmode = xtal1+pll16x
  _clkfreq = 80_000_000

  _SD_DO = 0
  _SD_CLK = 1
  _SD_DI = 2
  _SD_CS = 3
  _SD_WP = -1 ' -1 ifnot installed.
  _SD_CD = -1 ' -1 ifnot installed.

VAR

'  long bufnum
  long cog
  long buf[1024]
  long bufnum


pub getbuf
return @buf

pub getbufnum
return @bufnum


PUB start(left, right, mode, addr)

smode:=mode
case mode
  0: ''Streaming audio (Wave)
    bufptr:=@buf
    overval:=7
    delay:=259
  1: '' SID
    bufptr:=addr
    overval:=9
    delay:=288
    stereo:=0
  2: '' MIDI
    bufptr:=addr
    overval:=8
    delay:=313
    stereo:=4
stop

leftcounter := ((left & $1F) + constant(%00100000 << 23))
rightcounter := ((right & $1F) + constant(%00100000 << 23))
outputmask := |<left | |<right

'coginit (7,@init,@bufnum)
cog:=1+cognew(@init, @bufnum)
return cog
  

PUB stop

if cog>0
  cogstop(cog-1)
cog := 0
  

DAT
                        org     0                         'initialization

init                    mov     ctra,leftcounter         ' nco mode
                        mov     ctrb,rightcounter
                        mov     frqa,#1                  ' frq=1 for pwm mode
                        mov     frqb,#1
                        mov     dira,outputmask          ' enable output on selected pins
                        mov     bufptr2,par
                        mov     time,cnt
                        add     time,delay

loop                    cmp     smode,#0                 wz
               if_nz    jmp     #p3
                        mov     ptr,bufptr               ' compute pointer to sample
                        add     ptr,bufcnt
                        rdword  lsample,ptr              ' get left
                        shl     lsample,#16
                        add     ptr, #2
                        rdword  rsample,ptr              ' get right
                        sar     lsample,#16
                        shl     rsample,#16              ' extend sign to 32 bits
                        sar     rsample,#16
                        add     bufcnt,#4
                        and     bufcnt,bufmask
p4                      mov     over,overval
                        wrlong  bufcnt,bufptr2           ' write actual sample number for main program
                        jmp     #p2

p3                      mov     ptr,bufptr
                        rdlong  lsample,ptr
                        sar     lsample,#16
                        mov     tmp, lsample
                        shl     tmp,#1
                        add     lsample, tmp
                        shl     tmp,#1
                        add     lsample, tmp
                        sar     lsample,#3

                        mov     stereo,stereo wz
              if_z      jmp     #mono
              
                        add     ptr,stereo
                        rdlong  rsample,ptr
                        sar     rsample,#16
                        mov     tmp, rsample
                        shl     tmp,#1
                        add     rsample, tmp
                        shl     tmp,#1
                        add     rsample, tmp
                        sar     rsample,#3
                        
                        jmp     #p4

mono                    mov     rsample,lsample
                        jmp     #p4

p2                      add     i1l,lsample              ' noise shape left
                        add     i2l,i1l
                        mov     topl,i2l
                        sar     topl,#8
                        mov     fbl,topl
                        shl     fbl,#8
                        sub     i1l,fbl
                        sub     i2l,fbl

                        add     i1r,rsample              ' noise shape right
                        add     i2r,i1r
                        mov     topr,i2r
                        sar     topr,#8
                        mov     fbr,topr
                        shl     fbr,#8
                        sub     i1r,fbr
                        sub     i2r,fbr


                        maxs    topr, maxval           ' clip max to avoid clicks
                        mins    topr, minval
                        add     topr, #$80             ' convert to 8 bit unsigned
                        and     topr, #$FF
                        maxs    topl, maxval
                        mins    topl, minval
                        add     topl, #$80
                        and     topl, #$FF

                        waitcnt time, delay
                        neg     phsa, topr            ' Output.
                        neg     phsb, topl            ' Output.

                        djnz    over,#p2
                        jmp    #loop

leftcounter             long    0
rightcounter            long    0
outputmask              long    0
delay                   long    259
bufmask                 long    %0000_0000_0000_0000_0000_0111_1111_1111
overval                 long    7
maxval                  long    127
minval                  long   -127
bufptr                  long    0
bufptr2                 long    0
bufcnt                  long    0
smode                   long    0
stereo                  long    0

over                    res 1
lsample                 res 1
rsample                 res 1
tmp                     res 1
time                    res 1
topl                    res 1
topr                    res 1
fbl                     res 1
fbr                     res 1
i1r                     res 1
i1l                     res 1
i2l                     res 1
i2r                     res 1
ptr                     res 1



                        fit     496

{{

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                  TERMS OF USE: MIT License
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}}