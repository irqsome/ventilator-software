pub haxx 
DAT                 


' Instrument parameters for GM1 sound set   (don't expect to much correspondence with sound name :-)
'         vol pan s1 s2 dt pm fb at dr su
pres
  byte 200,64, 12,0,7, 13,2, 255,19,38  '1 Grand Piano
  byte 200,64, 0,0,7, 13,2, 255,19,38   '2 Bright Piano
  byte 200,64, 0,12,7, 13,2, 255,19,38  '3 Electric Piano
  byte 200,64, 12,0,16, 4,1, 255,57,25  '4 Honkytonk
  byte 200,64, 12,0,09, 4,2, 255,33,40  '6 E-Piano 1
  byte 200,64, 0,12,17, 1,6, 255,39,22  '5 E-Piano 2
  byte 200,64, 12,0,6, 4,1, 255,57,25   '7 Hapsichord
  byte 200,64, 0,12,17, 1,5, 255,39,22  '8 Clavi

  byte 200,32, 12,12,3, 1,7, 255,37,0   '9  Celesta
  byte 200,32, 12,17,3, 1,7, 255,37,0   '10 Glockenspiel
  byte 200,32, 12,24,13, 1,7, 255,37,0  '11 MusicBox
  byte 200,32, 24,0,13, 5,3, 255,37,0   '12 Vibraphone
  byte 200,32, 0,12,13, 1,5, 255,57,0   '13 Marimba
  byte 200,32, 24,0,13, 5,3, 255,57,0   '14 Xylophon
  byte 200,32, 24,7,13, 3,3, 255,12,0   '15 Tubular Bells
  byte 200,32, 12,3,3, 1,7, 255,37,0    '16 Dulcimer

  byte 120,64, 0,24,19, 3,3, 18,79,113  '17 Organ
  byte 120,64, 0,12,19, 5,2, 200,49,80  '18 Perc Organ
  byte 100,64, 12,0,19, 3,2, 18,59,85   '19 Rock Organ 
  byte 150,64, 0,24,19, 3,5, 28,69,113  '20 Church Organ
  byte 120,64, 12,0,19, 5,2, 18,69,113  '21 Reed Organ
  byte 150,64, 12,0,19, 5,2, 18,69,113  '22 Accordion 
  byte 150,64, 0,12,19, 5,2, 18,69,99   '23 Harmonica
  byte 150,64, 0,12,19, 7,1, 18,69,129  '24 Tango Accordion

  byte 200,64, 24,12,6, 4,3, 255,47,25  '25 Guitar
  byte 200,64, 24,12,6, 4,1, 255,47,25  '26 Steel Guitar
  byte 200,64, 12,24,16, 2,3, 255,37,25 '27 Jazz Guitar
  byte 200,64, 24,12,6, 4,1, 255,37,25  '28 E-Guitar
  byte 200,64, 24,12,6, 4,2, 255,67,25  '29 E-Guitar muted
  byte 200,64, 24,12,6, 2,1, 255,47,89  '30 OverdriveGuitar
  byte 200,64, 24,12,6, 1,3, 199,57,125 '31 DistortionGuitar
  byte 140,64, 12,24,6, 1,4, 255,57,25  '32 Guitar harmonics

  byte 250,64, 12,12,07, 3,2, 255,47,40  '33 Acoustic Bass
  byte 250,64, 24,12,07, 4,2, 255,47,40  '34 Electric Bass
  byte 250,64, 24,12,07, 5,1, 255,67,0   '35 Pick Bass
  byte 250,64, 12,12,07, 5,1, 255,67,0   '36 Fretless Bass
  byte 250,64, 12,12,0, 2,1, 255,57,10   '37 Slap Bass1
  byte 250,64, 12,12,0, 3,1, 255,57,10   '38 Slap Bass2
  byte 250,64, 12,12,07, 3,1, 255,47,40  '39 Synth Bass1
  byte 250,64, 24,12,0, 3,1, 255,47,10   '40 Synth Bass2

  byte 120,64, 12,0,0, 6,2, 9,29,180    '41 Violine
  byte 120,64, 12,0,3, 6,2, 12,33,180   '42 Viola
  byte 120,64, 0,12,5, 1,4, 7,27,170    '43 Cello
  byte 120,64, 12,12,0, 3,2, 7,27,170   '44 Contrabass
  byte 150,64, 12,0,9, 2,4, 9,29,130    '45 TremoloStrings
  byte 200,64, 12,0,0, 3,5, 255,70,0    '46 Pizzicato
  byte 200,64, 12,0,0, 3,5, 255,50,10   '47 Harp
  byte 200,40, 0,12,42, 5,0, 255,57,0   '48 Timpani

  byte 150,64, 12,0,9, 1,4, 9,29,130    '49 String Ensemble1
  byte 100,64, 12,12,11, 1,4, 15,19,190 '50 String Ensemble2
  byte 120,64, 12,0,9, 2,4, 9,25,130    '51 Synth Strings1
  byte 120,64, 0,12,9, 1,4, 9,29,130    '52 Synth Strings2
  byte 150,64, 12,0,9, 2,5, 12,29,180   '53 Ahh
  byte 150,64, 12,0,9, 6,2, 12,29,180   '54 Ohh
  byte 150,64, 12,0,9, 6,1, 12,39,150   '55 Synth Voice
  byte 250,64, 12,0,19, 6,2, 50,29,70   '56 Orchester Hit

  byte 200,64, 12,24,19, 3,1, 15,89,98  '57 Trumpet
  byte 200,64, 12,24,19, 3,1, 15,89,78  '58 Trombone
  byte 200,64, 0,0,0, 2,1, 15,89,98     '59 Tuba
  byte 200,64, 24,12,0, 4,1, 15,99,92   '60 muted Trumpet
  byte 200,96, 0,24,19, 3,1, 18,139,118 '61 French Horn
  byte 200,64, 0,12,23, 4,1, 20,89,118  '62 Brass section
  byte 200,64, 24,12,13, 4,1, 20,89,121 '63 Synth Brass 1
  byte 200,64, 12,12,13, 4,1, 15,89,138 '64 Synth Brass 2

  byte 200,64, 0,0,0, 2,1, 15,89,98     '65 Soprano Sax
  byte 200,64, 12,0,0, 2,1, 15,89,113   '66 Alto Sax
  byte 200,64, 0,12,0, 2,1, 15,89,103   '67 Tenor Sax
  byte 200,64, 0,0,0, 1,2, 15,89,153    '68 Bariton Sax
  byte 200,96, 0,24,19, 3,1, 18,139,88  '69 Oboe
  byte 200,96, 0,24,2, 2,1, 18,79,118   '70 English Horn
  byte 200,96, 0,12,2, 3,1, 18,79,118   '71 Bassoon
  byte 200,96, 12,0,5, 1,4, 18,139,198  '72 Clarinet

  byte 200,96, 0,0,0, 5,5, 58,139,128   '73 Piccolo
  byte 200,16, 0,0,0, 5,4, 38,139,188   '74 Flute
  byte 200,96, 0,0,0, 5,5, 58,139,128   '75 Recorder
  byte 250,96, 12,0,0, 4,0, 18,139,78   '76 PanFlute
  byte 250,96, 12,0,0, 4,0, 18,139,78   '77 BlownBottle
  byte 200,96, 12,0,3, 2,0, 18,139,68   '78 Shakuhachi
  byte 150,96, 128,12,0, 1,0, 13,139,55 '79 Whistle
  byte 200,96, 12,0,3, 0,4, 44,139,55   '80 Ocarina

  byte 160,64, 0,12,9, 4,1, 75,89,78    '81 SquareSynth
  byte 180,64, 0,0,9, 5,1, 75,89,98     '82 SawSynth
  byte 170,96, 0,12,27, 1,1, 19,139,103 '83 Calliope
  byte 150,64, 0,12,9, 3,1, 75,89,118   '84 Chiff
  byte 150,64, 0,12,9, 3,0, 75,89,90    '85 Charang
  byte 130,64, 12,0,9, 6,2, 12,29,180   '86 Voice
  byte 150,64, 0,5,0, 4,3, 23,19,130    '87 Fifth
  byte 150,64, 12,0,7, 3,1, 255,47,90   '88 Bass&Lead

  byte 100,64, 0,12,9, 5,1, 9,19,130    '89 Synth Pad
  byte 100,64, 0,12,9, 6,2, 9,19,130    '90 warm Pad
  byte 100,64, 12,0,9, 1,4, 19,19,120   '91 PolySynth Pad
  byte 100,64, 12,0,9, 2,5, 12,29,180   '92 Choir Pad
  byte 100,64, 12,0,9, 1,4, 9,19,130    '93 bowed Pad
  byte 130,64, 0,24,9, 1,4, 9,19,90     '94 metal Pad
  byte 100,64, 12,0,9, 1,4, 9,19,130    '95 halo Pad
  byte 130,64, 0,12,9, 5,1, 255,129,78  '96 sweep

  byte 200,64, 128,219,77, 1,0, 1,11,90 '97 Rain
  byte 200,32, 12,17,3, 1,7, 255,37,0   '98 Soundtrack
  byte 150,64, 12,0,9, 1,4, 9,19,130    '99 crystal
  byte 150,64, 12,0,6, 4,3, 255,39,130  '100 Atmosphere
  byte 150,64, 0,0,9, 4,4, 2,19,130     '101 Brightness
  byte 150,64, 12,0,9, 5,3, 2,19,130    '102 Goblins
  byte 200,64, 12,0,22, 1,2, 255,19,30  '103 Echoes
  byte 200,64, 12,134,22, 1,2, 255,19,30 '104 SciFi

  byte 200,64, 0,132,22, 0,2, 255,23,10 '105 Sitar
  byte 250,64, 12,0,16, 3,1, 255,47,20  '106 Banjo
  byte 150,64, 12,0,16, 2,2, 255,47,60  '107 Shamisen
  byte 250,64, 12,0,12, 5,0, 255,77,30  '108 Koto
  byte 250,64, 0,12,12, 3,0, 255,166,10 '109 Kalimba
  byte 150,96, 0,12,5, 2,1, 18,79,168   '110 BagPipe  
  byte 150,64, 12,0,3, 6,1, 19,49,180   '111 Fiddle
  byte 200,32, 0,19,13, 1,5, 255,57,0   '112 Shanai

  byte 220,64, 0,17,42, 3,1, 255,77,0   '113 Tinkle Bell
  byte 200,32, 19,0,53, 5,3, 255,37,0   '114 Agogo Bell
  byte 250,32, 17,0,53, 4,3, 255,37,20  '115 Steel Drum
  byte 200,32, 25,0,53, 4,3, 255,137,0  '116 Wood Blocks
  byte 250,32, 19,0,53, 5,3, 255,37,0   '117 Taiko Drum
  byte 200,64, 128,0,0, 1,1, 255,47,0   '118 melodic Tom
  byte 200,32, 0,0,0, 7,0, 255,47,0     '119 Synth Drum
  byte 150,64, 128,128,0, 3,0, 1,221,0  '120 Reverse Cymbal

  byte 160,64, 128,128,0, 2,1, 9,221,0  '121 Guitar Noise
  byte 160,64, 128,128,0, 1,1, 12,55,0  '122 Breath Noise
  byte 200,64, 128,128,0, 0,1, 2,15,60  '123 Seashore
  byte 200,64, 12,193,0, 2,6, 19,15,60  '124 Bird Tweet
  byte 200,64, 12,193,0, 0,7, 19,15,60  '125 Telephone
  byte 200,64, 128,189,0, 0,1, 2,15,170 '126 Helicopter
  byte 100,64, 128,149,0, 0,0, 2,15,170 '127 Applause
  byte 200,64, 128,144,0, 0,0, 255,19,0 '128 Gunshot

' Drum Set
drset
  byte 250,64, 20,128,0, 3,1, 255,137,40 '35 Bass Drum 2
  byte 250,64, 20,128,0, 3,1, 255,137,40 '36 Bass Drum 1
  byte 200,16, 50,10,115, 5,1,255,167,0  '37 SideStick
  byte 200,64, 24,0,0, 2,0, 255,87,10    '38 SnareDrum 1
  byte 200,96, 180,40,0, 0,1, 255,97,0   '39 Hand Clap
  byte 200,64, 18,0,0, 2,0, 255,87,10    '40 SnareDrum 2
  byte 200,24, 128,12,0, 1,1, 255,67,0   '41 Low Tom 2
  byte 200,16, 128,128,0, 3,0, 255,77,0  '42 Closed HH
  byte 200,24, 128,12,0, 1,1, 255,67,0   '43 Low Tom 1
  byte 200,64, 128,128,0, 2,0, 255,31,0  '44 Pedal HH
  byte 200,64, 128,15,0, 1,1, 255,67,0   '45 Mid Tom 2
  byte 200,64, 128,128,0, 3,0, 255,31,0  '46 Open HH
  byte 200,64, 128,13,0, 1,1, 255,67,0   '47 Mid Tom 1
  byte 200,80, 128,20,0, 1,1, 255,67,0   '48 High Tom 2
  byte 200,64, 128,177,0, 2,0, 255,21,0  '49 Crash Cymbal
  byte 200,80, 128,18,0, 1,1, 255,67,0   '50 High Tom 1
  byte 150,64, 128,80,0, 0,1, 255,33,0   '51 Ride Cymbal
  byte 200,64, 128,199,0, 0,1, 255,23,0  '52 Chinese Cymb
  byte 200,64, 128,83,0, 0,1, 255,43,0   '53 Ride Bell
  byte 200,80, 20,128,0, 4,0, 197,77,0   '54 Tambourine
  byte 200,96, 128,212,0, 1,0, 255,33,0  '55 Splash Cymbal
  byte 100,112,21,36,13, 5,2, 255,77,0   '56 Cowbell
  byte 200,96, 128,212,0, 1,0, 255,25,0  '57 Crash Cymbal 2
  byte 200,112,210,16,0, 2,2, 255,37,0   '58 Vibra Slap
  byte 200,96, 128,212,0, 0,1, 255,33,0  '59 Ride Cymbal 2
  byte 200,40, 20,13,33, 5,2,255,87,0    '60 High Bongo
  byte 200,70, 7,0,33, 5,2, 255,87,0     '61 Low Bongo
  byte 200,36, 22,15,33, 5,2, 255,57,0   '62 High Conga
  byte 200,32, 21,14,33, 5,2, 255,37,0   '63 Open Hi Conga 
  byte 200,32, 17,10,33, 5,2, 255,57,0   '64 Open Conga 
  byte 200,96, 14,7,33, 7,1, 255,57,0    '65 High Timbal
  byte 200,80, 9,2,33, 7,1, 255,57,0     '66 Low Timbal
  byte 200,16, 31,12,53, 5,3, 255,47,0   '67 High Agogo
  byte 200,24, 23,4,53, 5,3, 255,37,0    '68 Low Agogo
  byte 200,80, 128,177,0, 3,0, 4,221,0   '69 Cabasa
  byte 200,80, 129,211,0, 2,0, 4,51,0    '70 Maracas
  byte 200,96, 128,25,0, 1,0, 13,139,0   '71 Short Whistle
  byte 200,96, 128,24,0, 1,0, 13,59,0    '72 Long Whistle
  byte 200,48, 129,211,0, 0,1, 4,91,0    '73 Short Guiro
  byte 200,48, 129,211,0, 0,1, 4,51,0    '74 Long Guiro
  byte 200,16, 129,128,0, 0,1, 8,51,0    '75 Claves
  byte 200,32, 25,1,53, 4,3, 255,137,0   '76 High Wood
  byte 200,32, 13,0,53, 4,3, 255,137,0   '77 Low Wood
  byte 200,96, 129,211,0, 0,1, 4,91,0    '78 Mute Cuica
  byte 200,96, 129,211,0, 0,1, 4,61,0    '79 Open Cuica
  byte 120,32, 59,39,43, 1,3, 255,127,0  '80 Mute Triangle
  byte 120,32, 58,38,43, 1,3, 255,87,0   '81 Open Triangle

  byte 100,96, 128,212,0, 0,1, 255,123,0 '82.. Short Ride for higher notes