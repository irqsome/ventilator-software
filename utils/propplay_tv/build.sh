mkdir -p sdbins
mkdir -p sdbins/PROPPLAY
cd source
bstc -c help.spin
python screen.py help.dat help.scr
cp help.scr "../sdbins/propplay/HELP.SCR"

bstc -c -o "../sdbins/propplay/sid" SIDcog_asm.spin
mv "../sdbins/propplay/sid.dat" "../sdbins/propplay/SID.COG"

bstc -c -o "../sdbins/propplay/pmsynth" pm_synth_20_asm.spin
mv "../sdbins/propplay/pmsynth.dat" "../sdbins/propplay/PMSYNTH.COG"

bstc  -Ocgru -e -o "../sdbins/propplay" propplay-018-20140820.spin
mv "../sdbins/propplay.eeprom" "../sdbins/PROPPLAY.BIN"

cp CombinedWaveforms.bin "../sdbins/propplay/COMBWAVE.DAT"

bstc -c -o "../sdbins/propplay/GENMIDI" genmidi.spin

cd ..