PropPlayTV
==========

## Info
- Type: Music Player
- Author(s): Piotr Kardasz, Ada Gottensträter
- First release: 2014 or 2019, your choice :)
- Improved version: **Yes, very much**
- Special requirements: SD card (duh)
- Video formats: NTSC (altough it's mostly monochrome,anyways...)
- Inputs: Keyboard

## Description
A powerful music player. Plays WAV (44.1kHz stereo only!), DMP (= SID dump) and MIDI (format 0 only!) files.

## Controls
Just press F1 to let it speak for itself :)

## TODOs
- Add HDMF playback
- Add some kind of visualizer
- Add proper RIFF parsing and support for more sampling rates
- Improve MIDI sound set (found in genmidi.spin)
- Find a better SID dumping tool (see below)

## Misc
- The `extras` directory contains a tool for creating DMP files from SID files. Its CPU emulation is quite buggy though. LFT's tunes in particular sound very, very wrong
